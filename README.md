## START REACT APP IN DEVELOPMENT:

1. unzip `node_modules.7z` or run `npm install`

2. Update dls package by run `npm run dls`

3. Start React App `npm start`

## START MOCK API SERVER

run `npm run server`

## BUILD REACT APP

1. config environment variables in .env.production
   PUBLIC_URL: where your react app domain/url.
   REACT_APP_API: where your API Server domain/url

2. npm run build

3. copy all files in build folder to server

## SENSITIVE KEY WORD REPLACEMENT

1. Key --> \*ky (recKy, rowKy, etc)
2. Password --> gateKy (for "gate key")
3. Social Security Number --> soSecNbr
4. Card Number --> crdNbr
5. Full Card --> fullCrd
6. Taxpayer Identification Number (TaxId) --> txIdNbr
7. Account Number --> accNbr
8. Authorization Code --> authCd
9. User Data --> usrData
10. User Name --> usrName
11. Business Account Number --> busAccNbr
12. User ID --> uId
13. Customer ID --> cId
14. Card --> crd
15. Number --> nbr
16. User --> usr
17. Customer --> cus
18. Account --> acc
19. Business --> bus
