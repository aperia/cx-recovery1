import { API_ERROR } from 'app/constants';
import { apiErrorDetailActions } from 'pages/__commons/ApiErrorDetail/_redux/reducer';
import { Middleware } from 'redux';

export const catchErrorMiddleWare: Middleware = store => next => action => {
  if (action?.payload?.errorMessage === API_ERROR) {
    const dispatch = store.dispatch;
    const storeId = action.payload.others?.storeId;
    const request = action.payload.response?.response?.config?.data as string;
    const response = JSON.stringify(action.payload.response?.response?.data);

    dispatch(
      apiErrorDetailActions.setErrValue({
        id: storeId ? `${storeId}-${action.type}` : action.type,
        value: `${response}\n\n${request}`
      })
    );
  }
  next(action);
};
