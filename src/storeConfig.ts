import { combineReducers } from 'redux';
import { caches } from 'app/_libraries/_dof/core/redux/reducers';
import { reducer as tab } from 'pages/__commons/TabBar/_redux';
import { reducer as mapping } from 'pages/__commons/MappingProvider/_redux/reducers';
import { reducer as i18next } from 'pages/__commons/I18nextProvider/_redux/reducers';
import { reducer as agencyMaintenance } from 'pages/AgencyManagement/AgencyMaintenance/_redux';
import { reducer as agencyDetails } from 'pages/AgencyDetails/_redux';
import { reducer as monitorMaintenance } from 'pages/__commons/MonitorMaintenance/_redux';
import { reducer as returnCriteriaForm } from 'pages/__commons/ReturnMaintenance/_redux/reducers';
import { reducer as form } from 'redux-form';
import { reducer as rootModal } from 'pages/__commons/RootModal/_redux/reducer';
import { reducer as toastNotifications } from 'pages/__commons/ToastNotifications/_redux';
import { reducer as agencyForm } from 'pages/__commons/Agency/AgencyForm/_redux/reducer';
import { refDataApi } from 'pages/__commons/RefData/refDataQuery';
import { reducer as apiErrorDetail } from 'pages/__commons/ApiErrorDetail/_redux/reducer';
import { reducer as agencyManagement } from 'pages/AgencyManagement/_redux';
import { reducer as accountManagement } from 'pages/AccountManagement/_redux';
import { reducer as agencyAssignRequest } from 'pages/AgencyManagement/AgencyAssignRequest/_redux';
import { reducer as agencyRecallRequest } from 'pages/AgencyManagement/AgencyRecallRequest/_redux';
import { reducer as noteCode } from 'pages/__commons/NoteCode/_redux';
import { reducer as accountDetail } from 'pages/AccountDetail/_redux';
import { reducer as accountDetailOverview } from 'pages/AccountDetail/Overview/_redux';
import { reducer as bankruptcyDeceaseRecord } from 'pages/AccountDetail/BankruptcyDeceaseRecord/_redux';
import { reducer as financialInformation } from 'pages/AccountDetail/FinacialInfomation/_redux';
import { reducer as suitInformation } from 'pages/AccountDetail/SuitInformation/_redux';
import { reducer as accountGroup } from 'pages/AccountGroup/_redux';
import { reducer as accountActivity } from 'pages/AccountActivity/_redux';
import { reducer as queueDetail } from 'pages/QueueDetail/_redux';
import { reducer as batchManagement } from 'pages/BatchManagement/_redux';
import { reducer as infoBar } from 'app/components/InfoBar/_redux';
import { reducer as payout } from 'pages/Payout/_redux';
import { reducer as ledger } from 'pages/AccountDetail/Ledger/_redux';
import { reducer as dashboard } from 'pages/Home/_redux';
import { reducer as workDateSchedule } from 'pages/WorkDateSchedule/_redux';

export const reducerMappingList = {
  accountDetailOverview,
  accountDetail,
  form,
  tab,
  i18next,
  mapping,
  agencyMaintenance,
  monitorMaintenance,
  returnCriteriaForm,
  toastNotifications,
  rootModal,
  apiErrorDetail,
  agencyDetails,
  agencyForm,
  agencyManagement,
  accountManagement,
  agencyAssignRequest,
  agencyRecallRequest,
  noteCode,
  bankruptcyDeceaseRecord,
  financialInformation,
  suitInformation,
  accountGroup,
  accountActivity,
  queueDetail,
  batchManagement,
  infoBar,
  payout,
  ledger,
  dashboard,
  workDateSchedule,
  [refDataApi.reducerPath]: refDataApi.reducer
};

export const rootReducer = combineReducers(reducerMappingList);
export type AppState = ReturnType<typeof rootReducer>;

// DOF AppState
export const dofRootReducer = combineReducers({
  ...reducerMappingList,
  caches
});

export type DOFAppState = ReturnType<typeof dofRootReducer>;
