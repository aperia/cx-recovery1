import { controlRegistry } from 'app/_libraries/_dof/core';

// controls
import {
  HeadingControlProps,
  TextBoxControlProps,
  ComboBoxControlProps,
  DropDownControlProps,
  DatePickerControlProps,
  NumericControlProps,
  GroupRadioControlProps,
  MultiSelectControlProps
} from 'app/components/_dof_controls/controls';
import { TextViewControlProps } from 'app/components/_dof_controls/controls/TextViewControl';
import { TextViewCustomControlProps } from 'app/components/_dof_controls/controls/TextViewCustomControl';
import { TagLineControlProps } from 'app/components/_dof_controls/controls/TagLineControl';
import { TextValueControlProps } from 'app/components/_dof_controls/controls/TextValueControl';
import { BadgeControlProps } from 'app/components/_dof_controls/controls/BadgeControl';
import { AccNbrControlProps } from 'app/components/_dof_controls/controls/AccNbrControl';
import { TagControlProps } from 'app/components/_dof_controls/controls/TagControl';
import { DividerControlProps } from 'app/components/_dof_controls/controls/DividerControl';

/**
 * Onchange funcs registries
 */
// import 'app/components/_dof_controls/custom-functions';

/**
 * Custom Validations registries
 */
// import 'app/components/_dof_controls/custom-validations';

/**
 * Control registries
 */
controlRegistry.registerControl<HeadingControlProps>('HeadingControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/HeadingControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<DatePickerControlProps>('DatePickerControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/DatePickerControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<ComboBoxControlProps>('ComboBoxControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/ComboBoxControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<DropDownControlProps>('DropDownControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/DropDownControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<TextBoxControlProps>('TextBoxControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/TextBoxControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<NumericControlProps>('NumericControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/NumericControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<GroupRadioControlProps>('GroupRadioControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/GroupRadioControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<MultiSelectControlProps>('MultiSelectControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/MultiSelectControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<TextViewControlProps>('TextViewControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/TextViewControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<TextViewCustomControlProps>(
  'TextViewCustomControl',
  {
    promise: () =>
      import(
        'app/components/_dof_controls/controls/TextViewCustomControl'
      ).then(imports => imports.default)
  }
);

controlRegistry.registerControl<TagLineControlProps>('TagLineControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/TagLineControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<TextValueControlProps>('TextValueControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/TextValueControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<BadgeControlProps>('BadgeControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/BadgeControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<AccNbrControlProps>('AccNbrControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/AccNbrControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<TagControlProps>('TagControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/TagControl').then(
      imports => imports.default
    )
});

controlRegistry.registerControl<DividerControlProps>('DividerControl', {
  promise: () =>
    import('app/components/_dof_controls/controls/DividerControl').then(
      imports => imports.default
    )
});
