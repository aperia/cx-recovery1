import {
  handleRequestFailure,
  handleRequestSuccess,
  handleResponseFailure,
  handleResponseSuccess
} from 'app/helpers/setupInterceptor';
import Axios, {
  AxiosInstance,
  AxiosRequestConfig,
  AxiosResponse,
  AxiosError
} from 'axios';

export interface ServiceSingletonInstance extends AxiosInstance {
  setupAxiosInterceptors?: <T = any>(
    interceptors?: InterceptorsInstance<T>
  ) => void;
}

export interface InterceptorsInstance<T = any> {
  request: {
    success?: (
      value: AxiosRequestConfig & T
    ) => AxiosRequestConfig | Promise<AxiosRequestConfig>;
    failure?: (error: AxiosError) => Promise<Error>;
  };
  response: {
    success?: (
      value: AxiosResponse & T
    ) => AxiosResponse | Promise<AxiosResponse>;
    failure?: (error: AxiosError) => Promise<Error>;
  };
}

export class ServiceSingleton {
  public static instance: ServiceSingletonInstance;
  private static interceptorsRequestNumber: number;
  private static interceptorsResponseNumber: number;
  /**
   * Singleton's constructor must be private
   */
  private constructor() {}

  public static getInstance(): ServiceSingletonInstance {
    if (ServiceSingleton.instance) return ServiceSingleton.instance;

    ServiceSingleton.instance = Axios.create({
      withCredentials: true
    });

    ServiceSingleton.instance.setupAxiosInterceptors =
      ServiceSingleton.setupAxiosInterceptors;

    // Setup interceptor by default
    ServiceSingleton.setupAxiosInterceptors();

    return ServiceSingleton.instance;
  }

  private static setupAxiosInterceptors(interceptors?: InterceptorsInstance) {
    if (interceptors) {
      // Eject request/response interceptors
      ServiceSingleton.instance.interceptors.request.eject(
        ServiceSingleton.interceptorsRequestNumber
      );
      ServiceSingleton.instance.interceptors.response.eject(
        ServiceSingleton.interceptorsResponseNumber
      );

      /**
       * Try to set the handler to empty array,
       * cuz the handler will be set null after eject
       */
      (
        ServiceSingleton.instance.interceptors.request as MagicKeyValue
      ).handlers = [];
      (
        ServiceSingleton.instance.interceptors.response as MagicKeyValue
      ).handlers = [];

      // Inject request/response interceptors
      ServiceSingleton.interceptorsRequestNumber =
        ServiceSingleton.instance.interceptors.request.use(
          interceptors.request.success?.bind(ServiceSingleton),
          interceptors.request.failure
        );
      ServiceSingleton.interceptorsResponseNumber =
        ServiceSingleton.instance.interceptors.response.use(
          interceptors.response.success,
          interceptors.response.failure
        );

      return;
    }

    ServiceSingleton.interceptorsRequestNumber =
      ServiceSingleton.instance.interceptors.request.use(
        handleRequestSuccess.bind(this),
        handleRequestFailure
      );

    ServiceSingleton.interceptorsResponseNumber =
      ServiceSingleton.instance.interceptors.response.use(
        handleResponseSuccess,
        handleResponseFailure
      );
  }
}

export type ServiceSingletonType = typeof ServiceSingleton;

export const apiService = ServiceSingleton.getInstance();
