export interface IRepositoryResponse<T> {
  data: T;
  extraData?: any;
}

export interface IGetByPageConfig {
  page?: number;
  itemsPerPage?: number;
}

export interface IWrite<T> {
  add(item: T): IRepositoryResponse<T[]>;
  update(id: string, item: T): IRepositoryResponse<T[]>;
  delete(id: string): IRepositoryResponse<T[]>;
}

export interface IRead<T> {
  getByPage(config: IGetByPageConfig): IRepositoryResponse<T[]>;
  getDetail(id: string): IRepositoryResponse<T>;
  getAll(): IRepositoryResponse<T[]>;
}
