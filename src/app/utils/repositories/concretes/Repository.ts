import {
  IGetByPageConfig,
  IRead,
  IRepositoryResponse,
  IWrite
} from '../interfaces';

/**
 * at NoteController.ts
 *
 * const noteRepository = new Repository<INoteCode>(JSON.parse(JSON.stringify(noteCodesJson))
 *
    // add
    const { data } = noteRepository.add(addData);

    // update
    const { data } = noteRepository.update(id, updateData);

    // delete
    const { data } = noteRepository.delete(id);

    // find by configs (lazy load, pagination)
    const {data, extraData} = noteRepository.getByPage({page: 1, itemsPerPage: 10});
    const { totalItem, currentPage } = extraData;

    // get detail
    noteRepository.getDetail(id);

    // get all
    noteRepository.getAll();

    // Usage Example:

    let noteCodeData = [] as INoteCode[];

    try {
      noteCodeData = JSON.parse(JSON.stringify(noteCodesJson))
        .noteCodes as INoteCode[];
    } catch (error) {}

    const TIME_DELAY = 600;
    const isHaveError = window.location.href.includes('error');
    const isHaveEmpty = window.location.href.includes('empty');
    const noteRepository = new Repository<INoteCode>(JSON.parse(JSON.stringify(noteCodesJson))

    export const noteCodeController = {
      getNoteCodes: ({ page }: IGetNoteCodeArgs) => {
        if (isHaveError) {
          return Promise.reject({
            data: {
              success: false,
              message: I18N_TEXT.LOAD_FAIL
            }
          });
        }

        const { data } = noteRepository.getByPage({ page }); // default ITEMS_PER_PAGE = 10

        return new Promise<IPayloadGetNoteCode>(function (resolve, reject) {
          setTimeout(() => {
            resolve({
              success: true,
              data: isHaveEmpty ? [] : data,
              totalItem,
              currentPage
            });
          }, TIME_DELAY);
        });
      }
    }
**/

function* idMaker(initValue?: number): IterableIterator<number> {
  let id = initValue || 0;
  while (true) {
    yield ++id;
  }
}

export class Repository<
  T extends {
    id?: string;
  }
> implements IWrite<T>, IRead<T>
{
  static get PAGE_DEFAULT() {
    return 1;
  }
  static get ITEMS_PER_PAGE() {
    return 10;
  }
  static get NOT_FOUND_ERROR_MSG() {
    return 'NOT_FOUND';
  }

  private data = [] as T[];
  private generateAutoIncrementId: IterableIterator<number>;

  constructor(initialList: T[]) {
    if (Array.isArray(initialList)) {
      this.data = initialList;

      // Handle get lastId from initialList
      let lastId = '0';
      if (initialList.length) {
        const sortedCopiedData = [...initialList].sort((a: T, b: T) => {
          if (!a?.id || !b?.id) return 0;
          return +b.id - +a.id;
        });
        lastId = sortedCopiedData[0]?.id || '0';
      }

      this.generateAutoIncrementId = idMaker(+lastId);
      return;
    }

    // the FIRST generated lastId will be 1
    this.generateAutoIncrementId = idMaker();
  }

  add(item: T): IRepositoryResponse<T[]> {
    const lastId = this.generateAutoIncrementId.next().value;

    const newItem = {
      ...item,
      id: `${lastId}`
    } as T;

    this.data = this.data.concat([newItem]);

    return { data: this.data };
  }

  update(id: string, item: T): IRepositoryResponse<T[]> {
    const itemIndex = this.data.findIndex(item => item.id === id);
    if (itemIndex === -1) {
      throw new Error(Repository.NOT_FOUND_ERROR_MSG);
    }

    const updateItem = {
      ...this.data[itemIndex],
      ...item
    };

    this.data = [
      ...this.data.slice(0, itemIndex),
      updateItem,
      ...this.data.slice(itemIndex + 1)
    ];

    return { data: this.data };
  }

  updates(items: T[]): IRepositoryResponse<T[]> {
    const _data = this.data.map((_data: T) => {
      const itemIndex = items.findIndex((item: T) => _data.id === item.id);
      if (itemIndex === -1) {
        return _data;
      }
      return items[itemIndex];
    });
    this.data = _data;

    return { data: this.data };
  }

  delete(id: string): IRepositoryResponse<T[]> {
    this.data = this.data.filter((item: T) => item.id !== id);
    return { data: this.data };
  }

  getByPage({
    page = Repository.PAGE_DEFAULT,
    itemsPerPage = Repository.ITEMS_PER_PAGE
  }: IGetByPageConfig): IRepositoryResponse<T[]> {
    const totalItem = this.data.length;
    const totalPages = Math.ceil(totalItem / itemsPerPage);

    const startIndex = (page - 1) * itemsPerPage;
    const endIndex = Math.min(startIndex + (itemsPerPage - 1), totalItem);

    const currentPage = Math.ceil((startIndex - 1) / itemsPerPage + 1);

    if (currentPage >= totalPages) {
      return {
        data: [],
        extraData: {
          totalItem,
          currentPage: page
        }
      };
    }

    return {
      data: this.data.slice(startIndex, endIndex + 1),
      extraData: {
        totalItem,
        currentPage
      }
    };
  }

  getDetail(id: string): IRepositoryResponse<T> {
    const item = this.data.find((item: T) => item.id === id);
    if (!item) throw new Error(Repository.NOT_FOUND_ERROR_MSG);

    return { data: item };
  }

  getAll(): IRepositoryResponse<T[]> {
    return { data: this.data };
  }
}
