export interface ChunkType {
  start: number;
  end: number;
}

export interface chunksAllProps {
  searchWord: string | RegExp;
  parentString: string;
  regexOption?: string;
}

export const chunksAll = ({
  searchWord,
  parentString,
  regexOption = 'gi'
}: chunksAllProps): ChunkType[] => {
  const regex = new RegExp(searchWord, regexOption);
  let match;
  const chunks = [];
  while ((match = regex.exec(parentString))) {
    const start = match.index;
    const end = regex.lastIndex;
    (end > start) && chunks.push({ start, end });
  }
  return chunks;
};
