import { AxiosError, AxiosRequestConfig, AxiosResponse } from 'axios';

// Config & Type
import { ServiceSingletonType } from 'app/utils/api.service';

export const mockAPIRegex = /mockApi/i;

export const isMockAPI = (url: string) => {
  return mockAPIRegex.test(url);
};

const isGettingRefData = (url: string) => {
  return /refData/i.test(url);
};

export const handleResponseSuccess = (
  response: AxiosResponse
): Promise<AxiosResponse> => {
  return Promise.resolve(response);
};

export const handleResponseFailure = (
  error: AxiosError
): Promise<AxiosError> => {
  return Promise.reject({
    errorMessage: error.message,
    response: error.response || error
  });
};

export function handleRequestSuccess(
  this: ServiceSingletonType,
  config: AxiosRequestConfig
): Promise<AxiosRequestConfig> {
  const { url = '' } = config;
  if (isMockAPI(url)) {
    config.baseURL = process.env.REACT_APP_MOCK_API;
  } else if (isGettingRefData(url)) {
    config.baseURL = process.env.PUBLIC_URL || '/';
  } else {
    config.baseURL = process.env.REACT_APP_API;
  }
  return Promise.resolve(config);
}

export const handleRequestFailure = (error: AxiosError): Promise<Error> => {
  return Promise.reject(error.request);
};
