export const stringValidate = (value = '') => {
  return {
    isAlphabetical: () => {
      const regex = new RegExp(
        atob('WzAtOWB+IUAjJCVeJiooKTw+Py8sXC1fLis9OjsnIlxcfFtcXXt9XHNd')
      );
      return !regex.test(value);
    },
    isAlphanumericPunctuationSpecial: () => {
      return !new RegExp(/[^ -~]+/g).test(value);
    },
    isSpecialAlphabetical: () => {
      return !new RegExp(/[0-9]/).test(value);
    },
    isAlphanumeric: () => {
      const regex = new RegExp(
        atob('W2B+IUAjJCVeJiooKTw+Py8sLis9OjsnIlxcfFtcXXt9IF0=')
      );
      return !regex.test(value);
    },
    isAlphanumericAndSpace: () => {
      return new RegExp(/^[a-zA-Z0-9 ]*$/gm).test(value);
    },
    isSpecialAlphanumeric: () => {
      const regex = new RegExp(
        atob('W2B+IUAjJCVeJiooKTw+Py8sLlwtXys9OjsnIlxcfFtcXXt9XHNd')
      );
      return !regex.test(value);
    },
    isNumber: () => {
      return new RegExp(/^[0-9]*$/gm).test(value);
    },
    isZip: () => {
      return new RegExp(/^[0-9\-]*$/gm).test(value);
    },
    maxLength: (length: number) => {
      return value.length <= length;
    },
    minLength: (length: number) => {
      return value.length >= length;
    },
    isRequire: () => {
      return !!value;
    },
    isNoneASCIICharacters: () => {
      return new RegExp(/[^\x00-\x7F]/).test(value);
    }
  };
};
