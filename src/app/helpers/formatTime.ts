import dateTime from 'date-and-time';

// Enum
import { FormatTime } from 'app/constants/enums';
import { isDate, isString, isUndefined } from '.';
import { TimePickerValue } from 'app/_libraries/_dls/components/TimePicker';

export const isValidDate = (date: string | Date | number) => {
  const newDate = new Date(date);
  return isDate(newDate) && !isNaN(newDate.valueOf());
};

/**
 * convert string date API return to string date view
 * '2020-12-10', 'YYYY-M-D, 'M/D/YYYY' => 12/10/2020
 * @param time : string date
 * @param inputFormat : string format
 * @param outFormat : string format
 */

export const convertAPIDateToView = (
  time: string,
  inputFormat = 'YYYY-M-D',
  outFormat = 'MM/DD/YYYY'
) => {
  if (/^-?\d+$/.test(time) || isUndefined(time)) return '';

  if (!time || !inputFormat || !outFormat) return time.replace(/-/gi, '/');

  const result = (dateTime as any).transform(time, inputFormat, outFormat);
  if (!result || result.includes('NaN')) {
    return time.replace(/-/gi, '/');
  }

  return result;
};

/**
 * - Convert Local Date to UTC Date
 * @param {Date} date
 * @returns {Date}
 */
export const convertLocalDateToUTCDate = (date: Date) => {
  return new Date(date.getTime() + date.getTimezoneOffset() * 60000);
};

export const convertToDD_MM_YYYY = (date: string): string => {
  const XX_YYYY_XX = new RegExp('^[0-9]{2}(/|-)[0-9]{4}(/|-)[0-9]{2}$');

  let month, day, year;
  if (XX_YYYY_XX.test(date)) {
    const operator = date.includes('/') ? '/' : '-';

    const dateParts = date.split(operator);

    // "openDate": "{openDayDate}-{openDate}" <=> dd-mm-yyyy
    // "openDate": "{openDayDate}-{openDate}" <=> dd-yyyy-mm
    // Re-assign year value if format is dd_yyyy_mm then return dd_mm_yyyy
    day = +dateParts[0];
    year = +dateParts[1];
    month = +dateParts[2];

    return [
      day.toString().padStart(2, '0'),
      month.toString().padStart(2, '0'),
      year
    ].join(operator);
  }

  return date;
};

/**
 * - ...
 *  'DD_MM_YYYY' <=> DD/MM/YYYY or DD-MM-YYYY
 * - input: time: '10/23/2020', format: 'MM_DD_YYYY'
 * - output: '10/23/2020', format: 'MM/DD/YYYY'
 * @param {string} time
 * @param {string} outputFormat
 * @returns {Date}
 */
export const prepareDateStringFormat = (
  date: string,
  outputFormat = 'MM_DD_YYYY'
): string => {
  const XX_XX_YYYY = new RegExp('^[0-9]{2}(/|-)[0-9]{2}(/|-)[0-9]{4}$');
  const YYYY_XX_XX = new RegExp('^[0-9]{4}(/|-)[0-9]{2}(/|-)[0-9]{2}$');

  const operator = date.includes('/') ? '/' : '-';

  const dateParts = date.split(operator);
  if (dateParts.length !== 3) return '';

  let month, day, year;
  // Assume format is mm_dd_yyyy
  month = +dateParts[0];
  day = +dateParts[1];
  year = +dateParts[2];

  // Handle input format

  if (XX_XX_YYYY.test(date)) {
    if (!(month >= 1 && month <= 12)) {
      // Re-assign month value if format is dd_mm_yyyy
      month = +dateParts[1];
      day = +dateParts[0];
    }
  }

  if (YYYY_XX_XX.test(date)) {
    year = +dateParts[0];
    month = +dateParts[1];
    day = +dateParts[2];
  }
  // handle output format
  if (outputFormat === 'YYYY_MM_DD') {
    return [
      year.toString().padStart(4, '0'),
      month.toString().padStart(2, '0'),
      day.toString().padStart(2, '0')
    ].join(operator);
  } else {
    return [
      month.toString().padStart(2, '0'),
      day.toString().padStart(2, '0'),
      year.toString().padStart(4, '0')
    ].join(operator);
  }
};

/**
 * - ...
 * - input: time: '10/16/2020', format: 'YYYY'
 * - output: '2020'
 * @param {string} time
 * @param {string} outputFormat
 * @param {string} inputFormat
 * @returns {Date}
 */
export const formatTimeDefault = (
  date: string | Date | null,
  outputFormat: FormatTime,
  inputFormat?: string
) => {
  if (!date) return;
  // Check date format is just have 0, ex: 000000
  const invalidFormatRegex = new RegExp('^[^1-9]+$');
  const validDateFormat = new RegExp('^[0-9]{4}(/|-)[0-9]{2}(/|-)[0-9]{2}$');
  const validDateFormat1 = new RegExp('^[0-9]{2}(/|-)[0-9]{2}(/|-)[0-9]{4}$');

  if (typeof date === 'string' && invalidFormatRegex.test(date)) {
    return undefined;
  }

  if (isString(date) && validDateFormat1.test(date)) {
    date = prepareDateStringFormat(date);
  }

  let newDate =
    isString(date) && validDateFormat.test(date)
      ? convertLocalDateToUTCDate(new Date(date))
      : new Date(date);

  if (
    isString(date) &&
    (validDateFormat.test(date) || validDateFormat1.test(date))
  ) {
    return dateTime.format(newDate, outputFormat);
  }

  // date: string Date
  //     || number string
  if (isValidDate(date)) {
    return dateTime.format(newDate, outputFormat);
  }

  newDate = isString(date)
    ? dateTime.parse(date, inputFormat || outputFormat.replace(/\//g, '-'))
    : date;

  // date: MM-YYYY => MM/YYYY
  if (newDate && isValidDate(newDate)) {
    return dateTime.format(newDate, outputFormat);
  }

  // date: MMDDYYYY
  const parsedDateValue =
    isString(date) && dateTime.parse(date, FormatTime.DefaultPostDate);

  if (parsedDateValue && !isNaN(parsedDateValue?.valueOf()))
    return dateTime.format(parsedDateValue, outputFormat);

  return undefined;
};

/**
 * ...
 * @param {string} time
 * @param {string} format
 * @param {boolean} isConvertLocal
 * @param {boolean} isConvertUTCToLocal
 * @returns {Date}
 */
export const formatTimeConvert =
  (
    time: string,
    format: FormatTime,
    isConvertLocal = false,
    isConvertUTCToLocal = false
  ) =>
  () => {
    if (isConvertLocal) time = time ? `${time}Z` : time;
    if (isConvertUTCToLocal) time = time.replace('Z', '');

    return formatTimeDefault(time, format);
  };

/**
 * - Common format time
 * @param {string} time
 * @returns {object}
 */
export const formatTime = (time: string) => {
  return {
    get year() {
      return formatTimeDefault(time, FormatTime.Year);
    },
    get date() {
      return formatTimeDefault(time, FormatTime.Date);
    },
    get datetime() {
      return formatTimeDefault(time, FormatTime.Datetime);
    },
    get monthDate() {
      return formatTimeDefault(time, FormatTime.MonthDate);
    },
    get shortYearMonth() {
      return formatTimeDefault(time, FormatTime.ShortYearMonth);
    },
    get shortMonthShortYear() {
      return formatTimeDefault(time, FormatTime.ShortMonthShortYear);
    },
    get allDatetime() {
      return formatTimeDefault(time, FormatTime.AllDatetime);
    },
    get fullTimeMeridiem() {
      return formatTimeDefault(time, FormatTime.FullTimeMeridiem);
    },
    get monthYear() {
      return formatTimeDefault(time, FormatTime.MonthYear);
    },
    get shortMonthYear() {
      return formatTimeDefault(time, FormatTime.ShortMonthYear);
    },
    get fullYearMonthDay() {
      return formatTimeDefault(time, FormatTime.FullYearMonthDay);
    },
    get allDateTimeToUTC() {
      return formatTimeConvert(time, FormatTime.AllDatetime, false, true);
    },
    get fullTime() {
      return formatTimeConvert(time, FormatTime.FullTime, false, false);
    },
    get fullTime24Hours() {
      return formatTimeConvert(time, FormatTime.FullTime24Hours, false, false);
    },
    get fullTimeMeridiemToUTC() {
      return formatTimeConvert(time, FormatTime.FullTimeMeridiem, false, true);
    },
    get dateWithHyphen() {
      return formatTimeConvert(time, FormatTime.DateWithHyphen, false, true);
    },
    get shortYearShortMonthShortDate() {
      return formatTimeDefault(time, FormatTime.ShortYearShortMonthShortDate);
    },
    get yearMonthDayWithHyphen() {
      return formatTimeDefault(time, FormatTime.YearMonthDayWithHyphen);
    },
    get shortTimeMeridiem() {
      return formatTimeDefault(time, FormatTime.ShortTimeMeridiem);
    },
    get utcDate() {
      return formatTimeDefault(time, FormatTime.UTCDate);
    }
  };
};

/**
 * - Check if input date is the same as today
 * @param {string} date
 * @returns {boolean}
 */
export const isToday = (date: string) =>
  dateTime.isSameDay(new Date(), new Date(date));

/**
 * - Check if input date is the same as yesterday
 * @param {string} date
 * @returns {boolean}
 */
export const isYesterday = (date: string) =>
  dateTime.isSameDay(dateTime.addDays(new Date(), -1), new Date(date));

/**
 * - Convert UTC Date to Local Date
 * - input: Thu Oct 15 2020 00:00:00 GMT+0700 (Indochina Time)
 * - output: Wed Oct 14 2020 17:00:00 GMT+0700 (Indochina Time)
 * @param {Date} date
 * @returns {Date}
 */
export const convertUTCDateToLocalDate = (date: Date) => {
  return new Date(date.getTime() + date.getTimezoneOffset() * 60000);
};

/**
 * - Get start of Day
 * - input: Date with time
 * - output: Date with at start of day (0, 0, 0)
 * @param {Date} date
 * @returns {string}
 */
export const getStartOfDay = (
  date: Date | undefined,
  format: FormatTime = FormatTime.AllDatetime,
  isUTC?: boolean
) => {
  if (!date) return '';
  date.setHours(0, 0, 0, 0);
  return dateTime.format(date, format, !!isUTC);
};

/**
 * - Get End of Day
 * - input: Date with time
 * - output: Date with at end of day (23, 59, 59)
 * @param {Date} date
 * @returns {string}
 */
export const getEndOfDay = (
  date: Date | undefined,
  format: FormatTime = FormatTime.AllDatetime,
  isUTC?: boolean
) => {
  if (!date) return '';
  date.setHours(23, 59, 59, 999);
  return dateTime.format(date, format, !!isUTC);
};

/**
 * ...
 * - input: Thu Oct 15 2020 00:00:00 GMT+0700 (Indochina Time)
 * - output: output: 2020-10-14T10:00:00.000Z
 * @param {Date} date
 * @returns {Date}
 */
export const parseDateWithoutTimezone = (date: Date) => {
  return new Date(convertUTCDateToLocalDate(date)).toISOString();
};

/**
 * - Reset to midnight
 * - input:  Wed Aug 05 2020 11:58:00 GMT+0700 (Indochina Time)
 * - output: Wed Aug 05 2020 00:00:00 GMT+0700 (Indochina Time)
 * @param {Date} date
 * @returns {Date}
 */
export const convertToGMTTimes = (date: Date) =>
  new Date(date.setHours(0, 0, 0, 0));

/**
 * ...
 * - input: Thu Oct 15 2020 00:00:00 GMT+0700 (Indochina Time)
 * - output: 20201015
 * @param {Date} date
 * @returns {String}
 */
export const formatDateToSubmit = (date: Date | string) => {
  if (!date) return '00000000';

  if (isDate(date)) {
    const year = date.getFullYear();
    let month = '' + (date.getMonth() + 1);
    let day = '' + date.getDate();
    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;
    return `${year}${month}${day}`;
  }

  // TO-DO: need to update here

  if (date.includes('GMT')) {
    return dateTime.format(new Date(date), FormatTime.FullYearMonthDay);
  }

  if (date.includes('-')) {
    return date.replace(/[-]/g, '').slice(0, 8);
  }
  const isValid = isValidDate(date);
  if (isValid) {
    return dateTime.format(new Date(date), FormatTime.FullYearMonthDay);
  }
};

export const getFormattedDate: (
  date: Date | string | undefined | null
) => string = date => {
  if (!date) return '00000000';
  if (typeof date === 'string') return date;
  return date.toISOString();
};

/**
 * ...
 * - input: 030733
 * - output: 03:07:33
 * @param {String} time
 * @returns {String}
 */
export const timeSplit: (time: string) => string = time => {
  if (!time) return '';
  if (time.includes(':')) return time;
  if (time.length !== 6) return '';
  return (time.match(/\d{2}/g) as string[]).join(':');
};

/**
 * combine date and hours,min,sec into one date instance
 * @param date example '2021-04-07'
 * @param time example '092806'
 * @returns {Date} example Wed Apr 07 2021 09:28:06 GMT+0700 (Indochina Time)
 */
export const combineDateAndTime = (
  date?: string,
  time?: string
): Date | undefined => {
  if (!date || !time) return undefined;

  const hours = Number(time.substr(0, 2));
  const min = Number(time.substr(2, 2));
  const sec = Number(time.substr(4, 2));

  const dateInstance = new Date(date);

  return new Date(dateInstance.setHours(hours, min, sec));
};

export const getTimePickerValue = (
  date?: Date | TimePickerValue
): TimePickerValue => {
  if (!date) return {};

  if (date instanceof Date) {
    const timeStr = formatTime(date.toString()).shortTimeMeridiem || '';

    const [times, meridiem] = timeStr.split(' ') || [];
    const [hour, minute] = times.split(':');

    return {
      hour,
      minute,
      meridiem
    };
  }

  return date;
};
