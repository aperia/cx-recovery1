import { FormikTouched } from 'formik';
import { isBoolean, isEmpty } from 'lodash';

export const isArrayHasValue = (data: Array<MagicKeyValue>): boolean => {
  return data && data.length ? true : false;
};

/* 
  Function checkEmpty cover 2 case:
  !A ? B : A
  !A ? B : C

  variableCompare: variable need to empty ("", {}, [], [{..}], false)

  return:
  _ variableCompare: if arg[0] isn't Empty and arg[2] not pass
  _ fallbackFalse: if arg[0] is Empty
  _ fallbackTrue: if arg[0] is Empty and arg[2] passed
*/

const handleCheckEmpty = <F, S, T>(first: F, second: S, third: T) => {
  if (isBoolean(first)) {
    return !first ? second : third;
  }
  return isEmpty(first) ? second : third;
};

export function checkEmpty<F, S>(arg: [F, S]): F | S;
export function checkEmpty<F, S, T>(arg: [F, S, T]): S | T;

export function checkEmpty<F, S, T>(arg: [F, S, T] | [F, S]) {
  if (arg.length === 3) {
    return handleCheckEmpty(arg[0], arg[1], arg[2]);
  } else {
    return handleCheckEmpty(arg[0], arg[1], arg[0]);
  }
}

export const DATA_SOURCE = {
  getMonitorCriteriaCalendarCode: {
    url: 'refData/monitorCriteriaCalendarCode.json',
    customRequest: 'getMonitorCriteriaCalendarCode'
  },
  getMonitorCriteriaNoteCodes: {
    url: 'refData/monitorCriteriaNoteCodes.json',
    customRequest: 'getMonitorCriteriaNoteCodes'
  },
  getMonitorCriteriaIntervalUnit: {
    url: 'refData/monitorCriteriaIntervalUnit.json',
    customRequest: 'getMonitorCriteriaIntervalUnit'
  }
};

export const getRandomIntInclusive = (min: number, max: number) => {
  min = Math.ceil(min);
  max = Math.floor(max);

  return Math.floor(Math.random() * (max - min + 1) + min);
};

export const isTouchedForm = (touched: FormikTouched<any>) => {
  return Object.keys(touched).length;
};

export const formatCurrency = (text: string, fractionDigits = 2) => {
  if (isNaN(Number(text))) return text;

  let bracket = false;
  let amount = Number(text);

  if (amount < 0) {
    bracket = true;
    amount = Number(text) * -1;
  }

  const val = `$${amount
    .toFixed(fractionDigits)
    .replace(/(\d)(?=(\d\d\d)+(?=\.))/g, '$1,')}`;

  return bracket ? `(${val})` : val;
};

const regexFormatQuantity = new RegExp(
  atob('XGQoPz0oPzpcZHszfSkrKD8hXGQpKQ==')
);
export const formatQuantity = (text: string) => {
  if (isNaN(Number(text))) return text;
  return text.toString().replace(regexFormatQuantity, (a: string) => a + ',');
};