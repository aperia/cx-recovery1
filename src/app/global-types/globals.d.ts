import { AppState } from 'storeConfig';
import { SortType } from 'app/_libraries/_dls/components';
import { DropdownBaseChangeEvent as EventDropdown } from 'app/_libraries/_dls/components';
import { ParametricSelector } from '@reduxjs/toolkit';
declare global {
  interface DropdownBaseChangeEvent extends EventDropdown {}

  interface RootState extends AppState {}

  interface ISortType extends SortType {}

  type AppThunk = ActionCreator<ThunkAction<void, AppState, T, Action<string>>>;

  type OutputSelectorFunction<R> = ParametricSelector<RootState, string, R>;
  interface MagicKeyValue {
    [ky: string]: any;
  }

  type RejectValue = {
    errorMessage?: string;
    response?: Partial<AxiosError>;
    others?: any;
  };

  type ThunkAPIConfig = {
    rejectValue: RejectValue;
    state: RootState;
  };

  interface Window {
    appConfig: AppConfiguration;
  }

  interface RefDataValue {
    value: string;
    description: string;
    show?: boolean;
  }

  type FormType = 'ADD' | 'EDIT' | '';
  interface RefDataValue {
    value: string;
    description: string;
    show?: boolean;
    isActive?: boolean;
    isDisable?: boolean;
    group?: {
      label: string;
      data: Array;
    }
  }

  interface HttpResponseSuccess {
    success: true;
  }

  interface HttpResponseError {}

  type HttpResponse = HttpResponseSuccess | HttpResponseError;
  interface AppConfiguration {
    api: {
      agencyManagement: {
        getSections: string;
      };
      agencyMaintenance: {
        getAgencies: string;
        addAgency: string;
        deleteAgency: string;
        agencyDetails: string;
        deleteReturn: string;
        deleteMonitor: string;
      };
      assignRequest: {
        getAssignRequests: string;
        getSaveNumbers: string;
      };
      recallRequest: {
        getRecallRequests: string;
      };
      monitor: {
        getMonitors: string;
      };
      return: {
        getReturns: string;
      };
      noteCode: {
        getNoteCodes: string;
        saveNoteCode: string;
        deleteNoteCode: string;
      };
    };
    mappingUrl: string;
  }

  interface DropdownProxyEvent {
    name: string;
    value: unknown;
  }

  interface StoreIdPayload {
    storeId: string;
  }
}
