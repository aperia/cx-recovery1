import { useCallback, useEffect, useState } from 'react';

const useMatches = (mql: MediaQueryList): boolean => {
  const [matches, setMatches] = useState(mql.matches);

  const updateMatches = useCallback(() => {
    setMatches(mql.matches);
  }, [mql]);

  useEffect(() => {
    mql.addEventListener('change', updateMatches);
    updateMatches();

    return () => {
      mql.removeEventListener('change', updateMatches);
    };
  }, [mql, updateMatches]);

  return matches;
};

export default useMatches;
