import React, { useContext } from 'react';

interface AccountDetailContextProps {
  storeId: string;
  accNumber: string;
}

const AccountDetailContext = React.createContext<AccountDetailContextProps>({
  storeId: '',
  accNumber: ''
});

export const useAccountDetail = () => {
  return useContext<AccountDetailContextProps>(AccountDetailContext);
};

export const AccountDetailProvider = ({
  children,
  value
}: {
  children: React.ReactNode;
  value: AccountDetailContextProps;
}) => {
  return (
    <AccountDetailContext.Provider value={value}>
      {children}
    </AccountDetailContext.Provider>
  );
};
