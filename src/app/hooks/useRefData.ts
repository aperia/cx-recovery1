import { useMemo } from 'react';

// hooks
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';

const useRefData = (fileName = '') => {
  //   generate ref data options
  const { data } = useGetRefDataQuery(fileName);

  const getRecords = (
    key: string,
    data?: Record<string, RefDataValue[]>
  ): RefDataValue[] => {
    if (!data) return [];
    return data[key];
  };

  const records: RefDataValue[] = useMemo(() => {
    return getRecords(fileName, data);
  }, [data, fileName]);

  return records;
};

export default useRefData;
