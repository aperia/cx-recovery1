import { useEffect } from 'react';

// hooks
import useMatches from 'app/hooks/useMatches';
import useIsMounted from 'app/hooks/useIsMounted';

// helpers
import { isFunction } from 'app/helpers';
export interface UseMediaQueryProps {
  query?: string;
  onMediaQueryChange?: (matches: boolean) => void;
}
const useMediaQuery = ({
  query = '',
  onMediaQueryChange
}: UseMediaQueryProps): boolean => {
  const mediaQueryList: MediaQueryList = window.matchMedia(query);

  const matches = useMatches(mediaQueryList);
  const isMounted = useIsMounted();

  useEffect(() => {
    if (isMounted && isFunction(onMediaQueryChange))
      onMediaQueryChange(matches);
  }, [isMounted, onMediaQueryChange, matches]);

  return matches;
};

export default useMediaQuery;
