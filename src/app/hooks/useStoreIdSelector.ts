import { useSelector as useSelectorRedux } from 'react-redux';
import { useAccountDetail } from './';

export const useSelectorId = <T extends unknown>(
  selector: (state: RootState, storeId: string) => T
): T => {
  const { storeId: accountDetailStoreId } = useAccountDetail();
  return useSelectorRedux<RootState, T>(states =>
    selector(states, accountDetailStoreId)
  );
};

export const useSelector = <T extends unknown>(
  selector: (state: RootState) => T
): T => {
  return useSelectorRedux<RootState, T>(states => selector(states));
};
