import { MutableRefObject, useRef, useCallback } from 'react';

export const useSmoothScroll = (): [MutableRefObject<any>, () => void] => {
  const ref = useRef(null);
  const smoothScroll = useCallback(() => {
    if (ref.current) {
      (ref.current as unknown as HTMLDivElement).scrollIntoView({
        behavior: 'smooth',
        block: 'start'
      });
    }
  }, []);

  return [ref, smoothScroll];
};
