export const STEP_SEQUENCE = 25;

export const PAGE_SIZE_COMMON: number[] = [10, 25, 50];

export const PAGE_SIZE_SMALL: number[] = [5, 25, 50];

export const REGEX_NON_SPECIAL_CHARACTERS = /[^A-Za-z0-9]/;

export const ALPHA_REPLACE_REGEX = /[^a-z]/gi;

export const ALPHA_SPACE_REPLACE_REGEX = /[^a-z\s]/gi;

export const NUMERIC_REPLACE_REGEX = /[^0-9]/gi;
export const DECIMAL_REPLACE_REGEX = /[^0-9.]/gi;
export const EMAIL_REPLACE_REGEX = /[^\w@.]/gi;

export const ALPHA_NUMERIC_REPLACE_REGEX = /[^a-z0-9]/gi;

export const ALPHA_NUMERIC_SPACE_REPLACE_REGEX = /[^a-z0-9\s]/gi;

export const PHONE_NUMBER_REGEX_MATCH =
  /^([(]\d{3}[)]\s\d{3}\-\d{4}$)|(\d{10})/;

export const EMPTY_STRING = '';

export const API_ERROR = 'API_ERROR';

export const PLACE_HOLDER_SSN = '___-__-____';

export const SSN_MASK_INPUT_REGEX = [
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
  /\d/
];
export const PLACE_HOLDER_MM_DD_YYYY = 'mm/dd/yyyy';
export const PLACE_HOLDER_PHONE_NUMBER = '(___) ___-____';
export const PHONE_NUMBER_MASK_INPUT_REGEX = [
  '(',
  /\d/,
  /\d/,
  /\d/,
  ')',
  ' ',
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
  /\d/
];

export const MAX_LENGTH_2 = 2;
export const MAX_LENGTH_3 = 3;
export const MAX_LENGTH_4 = 4;
export const MAX_LENGTH_5 = 5;
export const MAX_LENGTH_6 = 6;
export const MAX_LENGTH_8 = 8;
export const MAX_LENGTH_10 = 10;
export const MAX_LENGTH_12 = 12;
export const MAX_LENGTH_13 = 13;
export const MAX_LENGTH_14 = 14;
export const MAX_LENGTH_15 = 15;
export const MAX_LENGTH_16 = 16;
export const MAX_LENGTH_17 = 17;
export const MAX_LENGTH_19 = 19;
export const MAX_LENGTH_20 = 20;
export const MAX_LENGTH_25 = 25;
export const MAX_LENGTH_30 = 30;
export const MAX_LENGTH_35 = 35;
export const MAX_LENGTH_40 = 40;
export const MAX_LENGTH_50 = 50;
export const MAX_LENGTH_70 = 70;
export const MAX_LENGTH_140 = 140;
export const MAX_LENGTH_300 = 300;
export const MAX_LENGTH_400 = 400;

export const ordersDropdown = [
  {
    description: 'Ascending',
    value: 'asc'
  },
  {
    description: 'Descending',
    value: 'desc'
  }
];

export const dropdownSortAccounts = [
  {
    description: 'Account Number',
    value: 'accountNumber'
  },
  { description: 'Debtor Name', value: 'debtorName' },
  { description: 'SSN', value: 'ssn' },
  {
    description: 'Original Loan Number',
    value: 'originalLoanNumber'
  }
];

export const dropdownSortBatch = [
  {
    description: 'Batch Date',
    value: 'batchDate',
    show: true
  },
  { description: 'Batch Number', value: 'batchNumber' },
  { description: 'Batch Item', value: 'batchItem' },
  {
    description: 'Batch Amount',
    value: 'batchAmount'
  },
  {
    description: 'Current Item',
    value: 'currentItem'
  },
  {
    description: 'Current Amount',
    value: 'currentAmount'
  }
];

export const DROPDOWN_SORT_ITEMS_PAYMENT = [
  { description: 'Item Amount', value: 'itemAmount' },
  { description: 'Item Number', value: 'itemNumber' },
  { description: 'Transaction Date', value: 'transactionDate' }
];

export const BREAK_POINT_VIEWPORT = {
  BR_768PX: 768,
  BR_1024PX: 1024,
  BR_1028PX: 1028,
  BR_1280PX: 1280,
  BR_1688PX: 1688
};

export const CUSTOM = 'custom';
