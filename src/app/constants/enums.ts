export enum TextViewControlFormat {
  Text = 'text',
  PhoneNumber = 'phoneNumber',
  Amount = 'amount',
  LessMoreActions = 'lessMoreActions',
  ShowInformation = 'showInformation'
}

export enum FormatTime {
  Year = 'YYYY',
  Date = 'MM/DD/YYYY',
  DateWithHyphen = 'MM-DD-YYYY',
  Datetime = 'MM/DD/YYYY hh:mm A',
  MonthDate = 'MM/DD',
  ShortYearMonth = 'YY/MM',
  ShortMonthShortYear = 'MM/YY',
  ShortYearShortMonthShortDate = 'YYMMDD',
  AllDatetime = 'MM/DD/YYYY hh:mm:ss A',
  FullTime = 'hh:mm:ss',
  FullTime24Hours = 'HH:mm:ss',
  FullTimeMeridiem = 'hh:mm:ss A',
  MonthYear = 'MMMM YYYY',
  ShortMonthYear = 'MM/YYYY',
  YearMonthDay = 'YYYY/MM/DD',
  YearMonthDayWithHyphen = 'YYYY-MM-DD',
  UTCDate = 'YYYY-MM-DD',
  UTCAllDatetime = 'YYYY-MM-DD HH:mm:ss',
  DefaultPostDate = 'MMDDYYYY',
  DefaultShortPostDate = 'MMDDYY',
  FullYearMonthDay = 'YYYYMMDD',
  ShortTimeMeridiem = 'hh:mm A'
}

export enum FormType {
  ADD = 0,
  EDIT = 1,
  DELETE = 2
}

export enum RelationType {
  L = 0,
  S,
  U,
  A
}

export const ColorBadge: MagicKeyValue = {
  O: 'purple', //Open
  C: 'grey', //Close
  R: 'cyan', // Ready To Post
  E: 'red' //Error
};
