import className from './className';
import classes from './classes';
import { get } from '../lodash';

describe('test class', () => {
  it('className', () => {
    Object.keys(className).forEach(kyComponent => {
      Object.keys(get(className, kyComponent)).forEach(kyComponentClassName => {
        get(className, [kyComponent, kyComponentClassName]);
      });
    });
    expect(true).toBeTruthy();
  });

  it('classes', () => {
    Object.keys(classes).forEach(kyComponent => {
      Object.keys(get(classes, kyComponent)).forEach(kyComponentClassName => {
        get(classes, [kyComponent, kyComponentClassName]);
      });
    });
    expect(true).toBeTruthy();
  });
});
