import genAmtId from './genAmtId';

const nodeEnv = process.env.NODE_ENV;

afterAll(() => (process.env.NODE_ENV = nodeEnv));

describe('test genAmtId', () => {
  it('should return undefined', () => {
    const result = genAmtId('', '', '');
    expect(result).toBeUndefined();
  });

  it('should return undefined and run console.log', () => {
    process.env.NODE_ENV = 'development';
    Object.defineProperty(window, 'location', {
      value: {
        hash: '#data-testid'
      }
    });

    const result = genAmtId('', '', '');
    expect(result).toBeUndefined();
  });

  it('should return id', () => {
    const result = genAmtId('test-id', '', '');
    expect(result).toEqual('test-id');
  });

  it('should return id with suffix', () => {
    const result = genAmtId('test-id', 'suffix', '');
    expect(result).toEqual('test-id_suffix');
  });
});
