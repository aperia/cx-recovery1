import reactChildrenCount from './reactChildrenCount';

describe('test reactChildrenCount', () => {
  it('', () => {
    reactChildrenCount(null);
    const count = reactChildrenCount([
      { props: { children: 'children' } },
      { props: { children: [] } }
    ]);
    expect(count).toEqual(1);
  });
});
