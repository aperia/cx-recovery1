import groupOrderFlatBy from './groupOrderFlatBy';

describe('test groupOrderFlatBy', () => {
  it('', () => {
    const result = groupOrderFlatBy(undefined, 'prop1', 'prop2');
    expect(result).toEqual([]);
  });

  it('', () => {
    const result = groupOrderFlatBy(
      [
        {
          prop1: 'prop1',
          prop2: 2
        },
        {
          prop1: 'prop11',
          prop2: 'prop22'
        }
      ],
      'prop1',
      'prop2'
    );
    expect(result).toEqual([
      { prop1: 'prop1', prop2: 2 },
      { prop1: 'prop11', prop2: 'prop22' }
    ]);
  });

  it('', () => {
    const result = groupOrderFlatBy(
      [
        {
          prop1: 'prop1',
          prop2: 2
        },
        {
          prop1: 'prop11',
          prop2: 'prop22'
        }
      ],
      'prop1',
      'prop2',
      'asc'
    );
    expect(result).toEqual([
      { prop1: 'prop1', prop2: 2 },
      { prop1: 'prop11', prop2: 'prop22' }
    ]);
  });
});
