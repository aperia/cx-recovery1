import { isEqual } from 'lodash';
import truncateTooltip from './truncateTooltip';

import '../test-utils/mocks/mockCanvas';

describe('test truncateTooltip', () => {
  Object.defineProperty(window, 'getComputedStyle', {
    value: (element) => {
      if (isEqual(element, { noPaddingLeft: true })) {
        return {
          getPropertyValue: () => {
            return 'apx';
          }
        };
      }

      if (isEqual(element, { noPaddingRight: true })) {
        return {
          getPropertyValue: (ky: string) => {
            return ky === 'padding-right' ? 'apx' : '0px';
          }
        };
      }

      return {
        getPropertyValue: () => {
          return '0px';
        }
      };
    }
  });

  it('should run with element is undefined and awaitNextTick is undefined', () => {
    truncateTooltip();
  });

  it('should run with awaitNextTick', () => {
    jest.useFakeTimers();
    truncateTooltip({}, true);
    jest.runAllTimers();
  });

  it('should run with element noPaddingLeft', () => {
    truncateTooltip({ noPaddingLeft: true });
  });

  it('should run with element noPaddingRight', () => {
    truncateTooltip({ noPaddingRight: true });
  });

  it('should run without context', () => {
    HTMLCanvasElement.prototype.getContext = () => undefined;
    truncateTooltip({});
  });

  it('should run with context and condition 1', () => {
    HTMLCanvasElement.prototype.getContext = () => ({
      font: '',
      measureText: () => ({
        width: 2
      })
    });
    truncateTooltip({
      offsetWidth: 1,
      innerText: { replace: () => '' }
    });
  });

  it('should run with context and condition 2', () => {
    HTMLCanvasElement.prototype.getContext = () => ({
      font: '',
      measureText: () => ({
        width: 1
      })
    });
    truncateTooltip({ offsetWidth: 2 });
  });
});
