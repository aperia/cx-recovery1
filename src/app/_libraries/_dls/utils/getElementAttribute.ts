/**
 * get padding element
 * @param {HTMLElement | null} element
 * @returns {object}
 */
const getPadding = (element: HTMLElement | Element | null) => {
  if (!element) return null;
  const {
    paddingTop = '0',
    paddingRight = '0',
    paddingLeft = '0',
    paddingBottom = '0'
  } = window.getComputedStyle(element);

  return {
    top: parseFloat(paddingTop),
    right: parseFloat(paddingRight),
    left: parseFloat(paddingLeft),
    bottom: parseFloat(paddingBottom)
  };
};

/**
 * get margin element
 * @param {HTMLElement | null} element
 * @returns {object}
 */
const getMargin = (element: HTMLElement | Element | null) => {
  if (!element) return null;
  const {
    marginTop = '0',
    marginRight = '0',
    marginLeft = '0',
    marginBottom = '0'
  } = window.getComputedStyle(element);

  return {
    top: parseFloat(marginTop),
    right: parseFloat(marginRight),
    left: parseFloat(marginLeft),
    bottom: parseFloat(marginBottom)
  };
};

/**
 * get border element
 * @param {HTMLElement | null} element
 * @returns {object}
 */
const getBorder = (element: HTMLElement | Element | null) => {
  if (!element) return null;
  const {
    borderTop = '0',
    borderRight = '0',
    borderLeft = '0',
    borderBottom = '0'
  } = window.getComputedStyle(element);

  return {
    top: parseFloat(borderTop),
    right: parseFloat(borderRight),
    left: parseFloat(borderLeft),
    bottom: parseFloat(borderBottom)
  };
};

/**
 * get widths of scrollBar
 * @param {HTMLElement | null} element
 * @returns {number}
 */
const getScrollBarWidth = (element: HTMLElement | null) => {
  if (!element)
    return {
      vertical: 0,
      horizontal: 0
    };

  const border = getBorder(element)!;
  const { clientWidth, offsetWidth, clientHeight, offsetHeight } = element;

  return {
    vertical: offsetWidth - clientWidth - border.left - border.right,
    horizontal: offsetHeight - clientHeight - border.top - border.bottom
  };
};

/**
 * get width is available for children
 * @param {HTMLElement | null} element
 * @returns {number}
 */
const getAvailableWidth = (element: HTMLElement | null) => {
  if (!element) return 0;
  const { clientWidth } = element;

  const padding = getPadding(element)!;

  return clientWidth - padding.left - padding.right;
};

export {
  getPadding,
  getMargin,
  getBorder,
  getScrollBarWidth,
  getAvailableWidth
};
