import canvasTextWidth from './canvasTextWidth';

describe('test canvasTextWidth', () => {
  it('should call callback fn', () => {
    HTMLCanvasElement.prototype.getContext = () =>
      ({
        measureText: (text: string) => ({ width: text.length * 50 })
      } as any);
    const { width } = canvasTextWidth('Text...')!;
    const { width: width2 } = canvasTextWidth('Text.....')!;

    HTMLCanvasElement.prototype.getContext = () => null;
    canvasTextWidth('Text......')!;

    expect(width).toEqual(350);
    expect(width2).toEqual(450);
  });
});
