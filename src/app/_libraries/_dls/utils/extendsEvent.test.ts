import extendsEvent from './extendsEvent';

describe('test extendsEvent', () => {
  it('should extends event', () => {
    const event = extendsEvent({} as any, '', {
      value: 1
    });
    expect(event).toEqual({ value: 1 });
  });
});
