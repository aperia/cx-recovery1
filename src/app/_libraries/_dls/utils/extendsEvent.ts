import { isUndefined, set } from '../lodash';

const extendsEvent = <T extends React.SyntheticEvent>(
  event: T,
  basePath: string | undefined | null,
  propertiesNeedExtends: Record<string, any>
): T => {
  const entries = Object.entries(propertiesNeedExtends);
  entries.forEach(([ky, value]) => {
    const path = [basePath, ky].filter((segments) => segments) as string[];

    !isUndefined(value) && set(event, path, value);
  });

  return event;
};

export default extendsEvent;
