export enum OS {
  Window,
  MacOS,
  IOS,
  Android,
  Linux
}

const windowPlatforms: Record<string, OS> = {
  Win32: OS.Window,
  Win64: OS.Window,
  Windows: OS.Window,
  WinCE: OS.Window
};
const macOSPlatforms: Record<string, OS> = {
  Macintosh: OS.MacOS,
  MacIntel: OS.MacOS,
  MacPPC: OS.MacOS,
  Mac68K: OS.MacOS
};
const iosPlatforms: Record<string, OS> = {
  iPhone: OS.IOS,
  iPad: OS.IOS,
  iPod: OS.IOS
};
const androidPlatform = /Android/;
const linuxPlatform = /Linux/;

const getOS = () => {
  const { userAgent, platform } = navigator;
  return (
    windowPlatforms[platform] ||
    macOSPlatforms[platform] ||
    iosPlatforms[platform] ||
    androidPlatform.test(userAgent) ||
    linuxPlatform.test(platform)
  );
};

const isMacOrIOS = () => {
  const os = getOS();
  return os === OS.MacOS || os === OS.IOS;
};

export { isMacOrIOS };
export default getOS;
