import checkPlatform from './checkPlatform';

describe('test checkPlatform', () => {
  it('should call callback fn', () => {
    Object.defineProperty(window, 'navigator', {
      value: {
        platform: 'Win32'
      }
    });

    const result = checkPlatform();
    expect(result).toBeTruthy();
  });
});
