import { get, isFunction, isNumber } from '../lodash';
import keycode from './keycode';

describe('test keycode', () => {
  it('should run with properties', () => {
    Object.keys(keycode).forEach(kyKeyCode => {
      let _vr = -1;
      let _fn = false;
      const result = get(keycode, kyKeyCode);
      if (isNumber(result)) {
        _vr = result;
      }
      if (isFunction(result)) {
        let keyCodeArg;
        switch (kyKeyCode) {
          case 'isSelectAll':
            keyCodeArg = keycode.A;
            break;
          case 'isCopy':
            keyCodeArg = keycode.C;
            break;
          case 'isTab':
            keyCodeArg = keycode.V;
            break;
          case 'isShiftTab':
            keyCodeArg = keycode.TAB;
            break;
          default:
            keyCodeArg = -1;
            break;
        }

        _fn = result.apply(keycode, [true, keyCodeArg]);
      }

      _vr;
      _fn;
    });
    expect(true).toBeTruthy();
  });
});
