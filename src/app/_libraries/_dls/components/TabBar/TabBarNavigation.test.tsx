import React, { MutableRefObject } from 'react';
import { fireEvent, render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

// components
import TabBarNavigation from './TabBarNavigation';
import TabBarTab from './TabBarTab';

// utils
import { queryByClass } from '../../test-utils/queryHelpers';
import { act } from 'react-dom/test-utils';

const mockRef: MutableRefObject<HTMLDivElement | null> = { current: null };

const tabs = [
  {
    id: 'home',
    title: 'Home',
    children: 'Content Home Tab'
  },
  {
    id: 'tab1',
    title: 'Tab Name 1',
    children: 'Content Tab 01'
  },
  {
    id: 'tab2',
    title: 'Tab Name 2',
    children: 'Content Tab 02'
  },
  {
    id: 'tab3',
    title: 'Tab Name 3',
    children: 'Content Tab 03'
  },
  {
    id: 'tab4',
    title: 'Tab Name 4',
    children: 'Content Tab 04'
  },
  {
    id: 'tab5',
    title: 'Tab Name 5',
    children: 'Content Tab 05'
  }
];

const scrollLeft = 120;
const clientWidth = 250;
const scrollWidth = 400;
const minAllowWidth = 88;
const arrowWidth = 34;

beforeEach(() => {
  Element.prototype.scrollTo = jest.fn();

  // mock Tab width
  Object.defineProperty(Element.prototype, 'scrollLeft', {
    value: scrollLeft
  });
  Object.defineProperty(Element.prototype, 'clientWidth', {
    value: clientWidth
  });
  Object.defineProperty(Element.prototype, 'scrollWidth', {
    value: scrollWidth
  });
});

const renderComponent = (props: {
  selected?: number;
  onSelect?: jest.Mock;
  numberOfTab?: number;
}) => {
  const { selected = 0, onSelect = jest.fn(), numberOfTab } = props || {};

  const wrapper = render(
    <TabBarNavigation ref={mockRef} selected={selected} onSelect={onSelect}>
      {tabs.slice(0, numberOfTab).map(({ id, title, ...tabProps }) => {
        return (
          <TabBarTab
            key={id}
            title={
              <React.Fragment>
                <span className="tab-title" id={id}>
                  {title}
                </span>
              </React.Fragment>
            }
            hoverTitle={title}
            {...tabProps}
          />
        );
      })}
    </TabBarNavigation>
  );

  return { wrapper, baseElement: wrapper.baseElement as HTMLElement };
};

describe('Render', () => {
  it('should active first tab', () => {
    const { baseElement } = renderComponent({ selected: 1 });

    const activeTab = queryByClass(baseElement, 'dls-item state-active');

    expect(activeTab?.textContent).toEqual('Tab Name 1');
    expect(queryByClass(baseElement, /dls-tabbar-nav/)).toBeInTheDocument();
  });

  it('should active last tab', () => {
    const { baseElement } = renderComponent({ selected: tabs.length - 1 });

    const activeTab = queryByClass(baseElement, 'dls-item state-active');

    expect(activeTab?.textContent).toEqual('Tab Name 5');
    expect(queryByClass(baseElement, /dls-tabbar-nav/)).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('onSelect', () => {
    const onSelect = jest.fn();

    renderComponent({ selected: 3, onSelect });

    screen.getByText('Tab Name 1').click();

    expect(onSelect).toBeCalledWith(1);
  });

  describe('handleOnScroll', () => {
    it('when arrow buttons are showed', () => {
      renderComponent({});

      fireEvent.scroll(screen.getByRole('tablist'));
    });

    it('when arrow buttons are not showed', () => {
      renderComponent({ numberOfTab: 1 });

      fireEvent.scroll(screen.getByRole('tablist'));
    });
  });

  it('should remove arrow buttons when user resize window from 250 to 1024', () => {
    const { baseElement } = renderComponent({});

    expect(
      queryByClass(baseElement, /dls-arrow arrow-left/)
    ).toBeInTheDocument();
    expect(
      queryByClass(baseElement, /dls-arrow arrow-right/)
    ).toBeInTheDocument();

    act(() => {
      // mock clientWidth 250 -> 1024
      Object.defineProperty(Element.prototype, 'clientWidth', {
        value: 1024
      });
      // trigger resize
      window.dispatchEvent(new Event('resize'));
    });

    expect(queryByClass(baseElement, /dls-arrow arrow-left/)).toBeNull();
    expect(queryByClass(baseElement, /dls-arrow arrow-right/)).toBeNull();
  });

  describe('click arrrow buttons', () => {
    it('left -> right', () => {
      const mockScrollTo = jest.fn();
      Element.prototype.scrollTo = mockScrollTo;

      const { baseElement } = renderComponent({});

      let scrollLeft = 120;
      Object.defineProperty(Element.prototype, 'scrollLeft', {
        value: scrollLeft
      });

      // click arrow left button
      queryByClass(baseElement, /dls-arrow arrow-left/)!.click();
      expect(mockScrollTo).toBeCalledWith({ behavior: 'smooth', left: 0 });
      mockScrollTo.mockClear();

      // click arrow right button
      queryByClass(baseElement, /dls-arrow arrow-right/)!.click();
      let alignLeft = (scrollLeft + clientWidth - arrowWidth) / minAllowWidth;
      expect(mockScrollTo).toBeCalledWith({
        behavior: 'smooth',
        left: scrollLeft + (2 - (alignLeft % 1)) * minAllowWidth
      });
      mockScrollTo.mockClear();

      // scrollLeft 120 -> 400
      scrollLeft = 400;
      Object.defineProperty(Element.prototype, 'scrollLeft', {
        value: scrollLeft
      });

      // click arrow left button
      queryByClass(baseElement, /dls-arrow arrow-left/)!.click();
      alignLeft = (scrollLeft + arrowWidth) / minAllowWidth;
      expect(mockScrollTo).toBeCalledWith({
        behavior: 'smooth',
        left: scrollLeft - ((alignLeft % 1) + 1) * minAllowWidth
      });
      mockScrollTo.mockClear();

      // click arrow right button
      queryByClass(baseElement, /dls-arrow arrow-right/)!.click();
      expect(mockScrollTo).toBeCalledWith({
        behavior: 'smooth',
        left: scrollLeft + minAllowWidth
      });
      mockScrollTo.mockClear();
    });

    it('right -> left', () => {
      const mockScrollTo = jest.fn();
      Element.prototype.scrollTo = mockScrollTo;

      const { baseElement } = renderComponent({});

      let scrollLeft = 400;
      Object.defineProperty(Element.prototype, 'scrollLeft', {
        value: scrollLeft
      });

      // click arrow right button
      queryByClass(baseElement, /dls-arrow arrow-right/)!.click();
      expect(mockScrollTo).toBeCalledWith({
        behavior: 'smooth',
        left: scrollLeft + minAllowWidth
      });
      mockScrollTo.mockClear();

      // click arrow left button
      queryByClass(baseElement, /dls-arrow arrow-left/)!.click();
      let alignLeft = (scrollLeft + arrowWidth) / minAllowWidth;
      expect(mockScrollTo).toBeCalledWith({
        behavior: 'smooth',
        left: scrollLeft - ((alignLeft % 1) + 1) * minAllowWidth
      });
      mockScrollTo.mockClear();

      // scrollLeft 120 -> 400
      scrollLeft = 120;
      Object.defineProperty(Element.prototype, 'scrollLeft', {
        value: scrollLeft
      });

      // click arrow right button
      queryByClass(baseElement, /dls-arrow arrow-right/)!.click();
      alignLeft = (scrollLeft + clientWidth - arrowWidth) / minAllowWidth;
      expect(mockScrollTo).toBeCalledWith({
        behavior: 'smooth',
        left: scrollLeft + (2 - (alignLeft % 1)) * minAllowWidth
      });
      mockScrollTo.mockClear();

      // click arrow left button
      queryByClass(baseElement, /dls-arrow arrow-left/)!.click();
      expect(mockScrollTo).toBeCalledWith({ behavior: 'smooth', left: 0 });
      mockScrollTo.mockClear();
    });
  });
});
