import React from 'react';

// components
import DateRangePicker, {
  DateRangePickerProps,
  DateRangePickerRef
} from '../../../DateRangePicker';

// utils
import classnames from 'classnames';

export interface DateRangePickerControlProps
  extends DateRangePickerProps,
    DLSId {
  itemKey?: string;
  onRemoveField: (config: {
    byPosition?: number;
    byFieldKey?: string;
    byAction?: 'click' | 'keyboard';
  }) => void;
  onNeedFocusInputFilter: () => void;
  onSearch: () => void;
  /** 4 required properties (above) must be handled separately for each component type  */
  allowFieldAutoFocus?: boolean;
}

const DateRangePickerControl: React.RefForwardingComponent<
  DateRangePickerRef,
  DateRangePickerControlProps
> = (
  {
    itemKey,
    onRemoveField,

    allowFieldAutoFocus,
    placeholder,
    children,
    className: classNameProp,

    dataTestId,
    ...props
  },
  ref
) => {
  return (
    <div className={classnames(classNameProp, 'date-range-picker-control')}>
      <DateRangePicker
        popupBaseProps={{
          popupBaseClassName: classnames('w-auto', 'dls-control-popup')
        }}
        autoFocus={allowFieldAutoFocus}
        placeholder={placeholder}
        dataTestId={dataTestId}
        {...props}
      >
        {children}
      </DateRangePicker>
    </div>
  );
};

export default React.forwardRef<
  DateRangePickerRef,
  DateRangePickerControlProps
>(DateRangePickerControl);
