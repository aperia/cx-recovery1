import { render, RenderResult } from '@testing-library/react';
import React from 'react';
import '../../../../test-utils/mocks/mockCanvas';
import MultiSelectControl, { MultiSelectControlProps } from './';

describe('Test AttributeSearch controls > DateRangePickerControl', () => {
  const mockDatePickerProp = {
    placeholder: 'mm/dd/yyyy',
    id: 'mock-dp-id'
  } as MultiSelectControlProps;

  const renderMultiSelectControl = (
    props?: MultiSelectControlProps
  ): RenderResult => {
    return render(
      <div>
        <MultiSelectControl {...mockDatePickerProp} {...props} />
      </div>
    );
  };

  it('should not render MultiSelectControl UI with placeholder', () => {
    const wrapper = renderMultiSelectControl();
    const input: HTMLInputElement | null = wrapper.baseElement.querySelector(
      '.text-field-container > input'
    );

    expect(input?.getAttribute('placeholder')).toEqual(undefined);
  });
});
