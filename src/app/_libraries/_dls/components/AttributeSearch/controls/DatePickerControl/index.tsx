import React, { FC, useEffect, useRef, useState } from 'react';

// components
import DatePicker, {
  DatePickerProps,
  DatePickerRef
} from '../../../DatePicker';

// utils
import get from 'lodash.get';
import isFunction from 'lodash.isfunction';
import { classes } from '../../../../utils';

// hooks
import { useResizeElementFitText } from '../../../../hooks';

export interface DatePickerControlProps extends DatePickerProps, DLSId {
  allowFieldAutoFocus: boolean;
  value: any;
  onFocus?: () => void;
}

const DatePickerControl: FC<DatePickerControlProps> = ({
  allowFieldAutoFocus,
  onBlur,
  placeholder = 'mm/dd/yyyy',
  popupBaseProps,
  dataTestId,
  ...props
}) => {
  // refs
  const [datePickerElement, setDatePickerElement] =
    useState<DatePickerRef | null>(null);
  const shouldFocusInput = useRef(false);
  const keepRef = useRef<Record<string, any>>({});
  const [dateValue, setDateValue] = useState(placeholder);

  const handleOnBlur = (event: React.FocusEvent<Element>) => {
    if (shouldFocusInput.current) {
      datePickerElement?.maskedTextBox?.inputElement?.focus();
      return;
    }

    isFunction(onBlur) && onBlur(event);
  };

  const handlePopupShown = () => {
    const popupElement = datePickerElement?.popupBaseRef?.current?.element;

    const handleMouseDown = (event: MouseEvent) => {
      if (popupElement?.contains(event.target as Node)) {
        shouldFocusInput.current = true;
      } else {
        shouldFocusInput.current = false;
      }
    };

    keepRef.current.handleMouseDown = handleMouseDown;
    window.addEventListener('mousedown', handleMouseDown);
  };

  const handlePopupClosed = () => {
    shouldFocusInput.current = false;

    const { handleMouseDown } = keepRef.current;
    window.removeEventListener('mousedown', handleMouseDown);
  };

  useResizeElementFitText({
    element: datePickerElement?.maskedTextBox?.inputElement,
    text: dateValue || placeholder,
    adjust: (element, width) => {
      element.style.width = `${width + 4}px`;
    }
  });

  // watching input value change to adjust width of input
  useEffect(() => {
    if (!datePickerElement) return;
    const inputElement = datePickerElement?.maskedTextBox
      ?.inputElement as HTMLInputElement;

    const mutation = new MutationObserver((records) => {
      const target = get(records, ['0', 'target']) as HTMLInputElement;
      setDateValue(target.value);
    });

    mutation.observe(inputElement, { attributeFilter: ['value'] });

    return () => mutation.disconnect();
  }, [datePickerElement]);

  return (
    <div className={classes.datePicker.control}>
      <DatePicker
        ref={setDatePickerElement}
        autoFocus={allowFieldAutoFocus}
        hideIcon
        onBlur={handleOnBlur}
        onPopupShown={handlePopupShown}
        onPopupClosed={handlePopupClosed}
        placeholder={placeholder}
        popupBaseProps={{
          popupBaseClassName: 'dls-control-popup',
          ...popupBaseProps
        }}
        dataTestId={dataTestId}
        {...props}
      />
    </div>
  );
};

export default DatePickerControl;
