import React, { useState, useEffect, useRef } from 'react';

// components
import InputControl, { InputControlProps } from '../InputControl';
import DropdownList, {
  DropdownListRef,
  DropdownListProps
} from '../../../DropdownList';
import { DropdownBaseChangeEvent } from '../../../DropdownBase';

// utils
import { keycode, className, genAmtId } from '../../../../utils';
import isFunction from 'lodash.isfunction';
import isBoolean from 'lodash.isboolean';
import classnames from 'classnames';

export interface MainSubConditionLevel {
  key: string;
  text?: string;
}

export interface MainSubConditionValue {
  level?: MainSubConditionLevel;
  keyword?: string;
}

export interface MainSubConditionChangeEvent {
  (event: { value?: MainSubConditionValue }): void;
}

export interface MainSubConditionProps extends DLSId {
  itemKey?: string;
  onRemoveField: (config: {
    byPosition?: number | undefined;
    byFieldKey?: string | undefined;
    byAction?: 'click' | 'keyboard' | undefined;
  }) => void;
  onNeedFocusInputFilter: () => void;
  onSearch: () => void;
  /** 4 required properties (above) must be handled separately for each component type  */
  value: MainSubConditionValue;
  onChange: MainSubConditionChangeEvent;
  onFocus?: () => void;
  onBlur?: () => void;
  anchor?: HTMLElement | null;
  allowFieldAutoFocus?: boolean;
  autoFocusDropdownList?: boolean;
  autoFocusInput?: boolean;
  className?: string;

  data?: MainSubConditionLevel[];
  dropdownListProps?: DropdownListProps;
  inputProps?: InputControlProps;
}

const defaultData: MainSubConditionLevel[] = [
  { key: '0', text: 'Moderate' },
  { key: '1', text: 'Low' },
  { key: '2', text: 'Exact' }
];

const MainSubConditionControl: React.FC<MainSubConditionProps> = ({
  itemKey,
  onRemoveField,
  /** 4 required properties (above) must be handled separately for each component type  */
  value: valueProp,
  onChange: onChangeProp,
  onFocus: onFocusProp,
  onBlur: onBlurProp,
  allowFieldAutoFocus,
  autoFocusDropdownList,
  autoFocusInput,

  data = defaultData,
  dropdownListProps,
  inputProps,

  dataTestId
}) => {
  const { level, keyword } = valueProp || {};
  let allowFieldAutoFocusDropdownList = !keyword;
  let allowFieldAutoFocusInput = !!keyword;

  if (isBoolean(autoFocusDropdownList)) {
    allowFieldAutoFocusDropdownList = autoFocusDropdownList;
  }

  if (isBoolean(autoFocusInput)) {
    allowFieldAutoFocusInput = autoFocusInput;
  }

  if (allowFieldAutoFocus === false) {
    allowFieldAutoFocusDropdownList = allowFieldAutoFocus;
    allowFieldAutoFocusInput = allowFieldAutoFocus;
  }

  // refs
  const dropdownListRef = useRef<DropdownListRef | null>(null);
  const inputRef = useRef<HTMLInputElement | null>(null);
  const keepSelectionStartRef = useRef(-1);
  const isMouseDownRef = useRef(false);

  // states
  const [dropdownListValue, setDropdownListValue] = useState(level);
  const [inputValue, setInputValue] = useState(keyword);
  const [opened, setOpened] = useState(allowFieldAutoFocusDropdownList);

  // handle onChange DropdownList
  const handleOnChangeDropdownList = (event: DropdownBaseChangeEvent) => {
    if (!isFunction(onChangeProp)) return;

    onChangeProp({ value: { level: event.value, keyword: inputValue } });
  };

  // handle onChange Input
  const handleOnChangeInput = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { selectionStart } = event.target;

    // just keep selectionStart to use later
    keepSelectionStartRef.current = selectionStart!;

    if (!isFunction(onChangeProp)) return;

    onChangeProp({
      value: { level: dropdownListValue, keyword: event.target.value }
    });
  };

  const handleOnVisibilityChange = (nextOpened: boolean) => {
    setOpened(nextOpened);
  };

  // set props
  useEffect(() => setDropdownListValue(level), [level]);
  useEffect(() => setInputValue(keyword), [keyword]);

  // handle adjust caret selection start
  useEffect(() => {
    const selectionStart = keepSelectionStartRef.current;
    if (selectionStart === -1 || !inputRef.current) return;

    inputRef.current.setSelectionRange(selectionStart, selectionStart);
  }, [inputValue]);

  // handle AttributeSearch keyboards: backspace/delete to remove field
  useEffect(() => {
    const inputElement = inputRef.current;

    // handle keydown Input
    const handleKeydownInput = (event: KeyboardEvent) => {
      const { keyCode } = event;

      // backspace/delete to remove field
      if (
        !inputElement?.value &&
        (keyCode === keycode.BACKSPACE || keyCode === keycode.DELETE)
      ) {
        isFunction(onRemoveField) && onRemoveField({ byFieldKey: itemKey });
      }
    };

    inputElement?.addEventListener('keydown', handleKeydownInput);
    return () => {
      inputElement?.removeEventListener('keydown', handleKeydownInput);
    };
  }, [itemKey, onRemoveField]);

  // if document.activeElement is DropdownList input element
  // and keydown is Enter, we will focus to inputRef
  useEffect(() => {
    if (!opened) return;

    const handleKeydown = (event: KeyboardEvent) => {
      if (event.keyCode !== keycode.ENTER) return;

      /* istanbul ignore next */
      if (
        document.activeElement &&
        document.activeElement === dropdownListRef.current?.inputElement
      ) {
        // prevent event bubble to DropdownList keydown
        event.stopPropagation();

        inputRef.current?.focus();
      }
    };

    window.addEventListener('keydown', handleKeydown, true);
    return () => window.removeEventListener('keydown', handleKeydown, true);
  }, [opened]);

  // prevent trigger internal behavior of the DropdownList's popup element
  useEffect(() => {
    if (!opened) return;

    process.nextTick(() => {
      const popupElement =
        dropdownListRef.current?.popupBaseRef?.current?.element;

      const handleMouseDown = (event: MouseEvent) => {
        const target = event.target as HTMLElement;

        if (target.closest(`.${className.dropdownBase.ITEM}`)) {
          isMouseDownRef.current = true;
        }
      };

      popupElement?.addEventListener('mousedown', handleMouseDown, true);
    });
  }, [opened]);

  // handle mouse down on DropdownList's input element
  useEffect(() => {
    const dropdownListInputElement = dropdownListRef.current?.inputElement;

    const handleMouseDown = (event: MouseEvent) => {
      // prevent bubble event mousedown to DropdownList's input element
      event.stopPropagation();

      const nextOpened = !opened;
      setOpened(nextOpened);

      if (!nextOpened) {
        event.preventDefault();
        inputRef.current?.focus();
      }
    };

    dropdownListInputElement?.addEventListener(
      'mousedown',
      handleMouseDown,
      true
    );
    return () => {
      dropdownListInputElement?.removeEventListener(
        'mousedown',
        handleMouseDown,
        true
      );
    };
  }, [opened]);

  // handle blur on both DropdownList and Input
  useEffect(() => {
    const dropdownListInputElement = dropdownListRef.current?.inputElement;
    const inputElement = inputRef.current;

    const handleBlur = () => {
      process.nextTick(() => {
        if (isMouseDownRef.current) {
          isMouseDownRef.current = false;
          inputRef.current?.focus();
          return;
        }

        if (
          document.activeElement === dropdownListInputElement ||
          document.activeElement === inputElement
        ) {
          return;
        }

        isFunction(onBlurProp) && onBlurProp();
      });
    };

    dropdownListInputElement?.addEventListener('blur', handleBlur);
    inputElement?.addEventListener('blur', handleBlur);
    return () => {
      dropdownListInputElement?.removeEventListener('blur', handleBlur);
      inputElement?.removeEventListener('blur', handleBlur);
    };
  }, [onBlurProp]);

  return (
    <div
      className={classnames(
        'main-sub-condition-control',
        dropdownListValue && 'exists-value-dropdown-list'
      )}
      data-testid={genAmtId(
        dataTestId,
        'dls-main-sub-condition-control',
        'MainSubConditionControl'
      )}
    >
      <DropdownList
        ref={dropdownListRef}
        value={dropdownListValue}
        onChange={handleOnChangeDropdownList}
        onFocus={onFocusProp}
        opened={opened}
        onVisibilityChange={handleOnVisibilityChange}
        textField="text"
        autoFocus={allowFieldAutoFocusDropdownList}
        popupBaseProps={{
          popupBaseClassName: classnames(
            'dls-control-popup',
            'dls-main-sub-condition-popup'
          )
        }}
        allowCallOnChangeByUpDown
        valueType="single"
        dataTestId={dataTestId}
        {...dropdownListProps}
      >
        {data.map((item) => (
          <DropdownList.Item key={item.key} label={item.text} value={item} />
        ))}
      </DropdownList>
      <InputControl
        ref={inputRef}
        value={inputValue}
        onChange={handleOnChangeInput}
        onFocus={onFocusProp}
        autoFocus={allowFieldAutoFocusInput}
        dataTestId={dataTestId}
        {...(inputProps as any)}
      ></InputControl>
    </div>
  );
};

export default MainSubConditionControl;
