import React, { useState } from 'react';

import AmountControl, { AmountControlProps } from '.';

const MockAmountControlWrapper = (props: Partial<AmountControlProps>) => {
  const [anchorElement, setAnchorElement] =
    useState<HTMLDivElement | null>(null);

  return (
    <div className="field-detail">
      <div ref={setAnchorElement}>
        <button className="ds-attr-clear-button"></button>
      </div>
      <AmountControl
        ref={{ current: null }}
        anchor={anchorElement}
        {...props}
      />
      <button className="close-button" />
    </div>
  );
};

export default MockAmountControlWrapper;
