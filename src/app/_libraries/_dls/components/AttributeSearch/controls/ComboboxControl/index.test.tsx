import React from 'react';
import '@testing-library/jest-dom';

// components
import ComboBoxControl, { ComboBoxControlProps } from '.';

import {
  queryByRole,
  render,
  RenderResult,
  screen
} from '@testing-library/react';
import { queryByClass } from '../../../../test-utils/queryHelpers';

// mocks
import '../../../../test-utils/mocks/mockCanvas';
import userEvent from '@testing-library/user-event';

// utils
import * as canvasTextWidth from '../../../../utils/canvasTextWidth';
import { DropdownBase } from '../../..';

let wrapper: RenderResult;

const data = [
  {
    FieldMetadataID: 5,
    FieldID: 'ALL',
    FieldValue: 'All '
  },
  {
    FieldMetadataID: 5,
    FieldID: 'NER',
    FieldValue: 'NIGER'
  }
];

const renderComponent = (props: Partial<ComboBoxControlProps>) => {
  jest.useFakeTimers();

  wrapper = render(
    <ComboBoxControl ref={{ current: null }} textField="FieldValue" {...props}>
      {data.slice(0, 100).map((item, index) => (
        <DropdownBase.Item
          key={item.FieldID + index}
          label={item.FieldValue}
          value={item}
        />
      ))}
    </ComboBoxControl>
  );

  jest.runAllTimers();
};

const getNode = {
  get trigger() {
    return queryByClass(wrapper.container, /dls-popper-trigger/)!;
  },
  get textContainer() {
    return queryByClass(wrapper.container, /text-field-container/)!;
  },
  get input() {
    return queryByRole(wrapper.container, 'textbox')! as HTMLInputElement;
  }
};

const userSelect = (text: string) => {
  userEvent.click(getNode.input);
  jest.runAllTimers();
  userEvent.click(screen.getByText(new RegExp(text)));
  jest.runAllTimers();
  userEvent.click(document.body);
};

describe('Render', () => {
  it('should render', () => {
    renderComponent({ value: data[0] });

    expect(getNode.trigger).toBeInTheDocument();
    expect(getNode.input.value).toEqual(data[0].FieldValue);
  });

  it('should render when text is null', () => {
    const spy = jest
      .spyOn(canvasTextWidth, 'default')
      .mockImplementation(() => null);

    renderComponent({});

    expect(getNode.trigger).toBeInTheDocument();
    expect(getNode.input.value).toEqual('');

    spy.mockReset();
    spy.mockRestore();
  });
});

describe('Actions', () => {
  describe('handleOnChange', () => {
    it('click on item', () => {
      const onBlur = jest.fn();
      const onChange = jest.fn();

      renderComponent({ onChange, onBlur });

      userSelect(data[1].FieldValue);

      expect(onChange).toBeCalledWith(
        expect.objectContaining({ value: data[1] })
      );
      expect(onBlur).toHaveBeenCalled();
    });

    it('click on dropdown container', () => {
      const onBlur = jest.fn();
      const onChange = jest.fn();

      renderComponent({ onChange, onBlur });

      userEvent.click(getNode.input);
      jest.runAllTimers();

      userEvent.click(
        queryByClass(
          wrapper.baseElement as HTMLElement,
          /dls-dropdown-base-scroll-container/
        )!
      );

      expect(onChange).not.toBeCalled();
      expect(onBlur).not.toBeCalled();
    });
  });

  it('when press backspace', () => {
    const itemKey = 'itemKey';
    const onRemoveField = jest.fn();

    renderComponent({ onRemoveField, itemKey });

    userEvent.click(getNode.input);
    userEvent.type(getNode.input, '{backspace}');

    expect(onRemoveField).toBeCalledWith({ byFieldKey: itemKey });
  });

  it('when press delete', () => {
    const itemKey = 'itemKey';
    const onRemoveField = jest.fn();

    renderComponent({ onRemoveField, itemKey });

    userEvent.click(getNode.input);
    userEvent.type(getNode.input, '{delete}');

    expect(onRemoveField).toBeCalledWith({ byFieldKey: itemKey });
  });

  it('when press enter', () => {
    const itemKey = 'itemKey';
    const onRemoveField = jest.fn();

    renderComponent({ onRemoveField, itemKey });

    userEvent.click(getNode.input);
    userEvent.type(getNode.input, '{enter}');

    expect(onRemoveField).not.toBeCalled();
  });

  it('when filter', () => {
    const itemKey = 'itemKey';
    const onRemoveField = jest.fn();

    renderComponent({ onRemoveField, itemKey });

    userEvent.click(getNode.input);
    expect(
      screen.getByText(new RegExp(data[1].FieldValue))
    ).toBeInTheDocument();

    userEvent.type(getNode.input, data[0].FieldValue);
    expect(screen.queryByText(new RegExp(data[1].FieldValue))).toBeNull();
  });
});
