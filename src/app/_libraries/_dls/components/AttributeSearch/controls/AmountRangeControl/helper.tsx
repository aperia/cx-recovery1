import { keycode } from '../../../../utils';

export const DEFAULT_VALUE = [undefined, undefined];
export const DEFAULT_SET_MIN_VALUE = 0.01;
export const DEFAULT_SET_MAX_VALUE = 99999999999.99;

export const formatValue = (value: any) => {
  return value === '.' ? value.replace('.', '0') : value;
};

export const haveValue = (value: any, minValue: number) => {
  const nextValue = formatValue(value);
  return !!nextValue && parseFloat(nextValue) >= minValue;
};

export const getNewValue = (
  value: any,
  defaultValue: React.ReactText = DEFAULT_SET_MIN_VALUE,
  minValue = DEFAULT_SET_MIN_VALUE
) => {
  return haveValue(value, minValue) ? formatValue(value) : defaultValue;
};

export const isKeyNumber = (key: string) => '0' <= key && key <= '9';

export const isKeyAction = (keyCode: number) =>
  keyCode === keycode.BACKSPACE ||
  keyCode === keycode.DELETE ||
  keyCode === keycode.TAB ||
  // fix control move left - right
  keyCode === keycode.LEFT ||
  keyCode === keycode.RIGHT ||
  // fix ctrl + a && ctrl && c
  keyCode === keycode.CTRL ||
  keyCode === keycode.A ||
  keyCode === keycode.C ||
  keyCode === keycode.V;

export const isKeyDot = (key: string) => key === '.';

export const isKeyValid = ({
  key,
  keyCode
}: React.KeyboardEvent<HTMLInputElement>) => {
  return isKeyNumber(key) || isKeyDot(key) || isKeyAction(keyCode);
};

export const haveDot = (value: string) => value.indexOf('.') >= 0;
