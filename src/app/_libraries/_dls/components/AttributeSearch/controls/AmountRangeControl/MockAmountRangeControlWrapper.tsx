import React, { useState } from 'react';

import AmountRangeControl, { AmountRangeControlProps } from '.';

export interface MockAmountControlWrapperProps
  extends Omit<AmountRangeControlProps, 'anchor' | 'value'> {
  value?: any[] | string[] | number[];
}

const MockAmountControlWrapper = ({
  value: valueProp,
  onChange,
  ...props
}: MockAmountControlWrapperProps) => {
  const [value, setValue] = useState(valueProp);
  const [anchorElement, setAnchorElement] =
    useState<HTMLDivElement | null>(null);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const value = event.target.value as unknown as string[];
    setValue(value);
    onChange(value);
  };

  return (
    <div className="field-detail">
      <div ref={setAnchorElement}>
        <button className="ds-attr-clear-button"></button>
        <button className="dls-attribute-search-button"></button>
      </div>
      <AmountRangeControl
        value={(valueProp ? value : undefined) as unknown as string[]}
        ref={{ current: null }}
        anchor={anchorElement!}
        onChange={onChange ? handleChange : undefined}
        {...props}
      />
      <button className="close-button" />
    </div>
  );
};

export default MockAmountControlWrapper;
