import { useMemo } from 'react';

import { MousetrapInstance } from 'mousetrap';

export interface MouseTrapHook {
  (element?: HTMLElement): MousetrapInstance;
}

const MockUseMouseTrap: React.FC<any> = (element) => {
  const mousetrap = useMemo(() => {
    const instance: any = { unbind: () => {} };

    const addEvent = (code: number, callback: Function) =>
      document.addEventListener('keydown', (event: any) => {
        event.keyCode === code && callback(event);
      });

    instance.bind = (code: string, callback: Function) => {
      if (code === 'up') addEvent(38, callback);
      if (code === 'down') addEvent(40, callback);
      if (code === 'tab') addEvent(9, callback);
      if (code === 'backspace') addEvent(8, callback);
      if (code === 'enter') addEvent(13, callback);
    };

    return instance;
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [element]);
  return mousetrap;
};

export default MockUseMouseTrap;
