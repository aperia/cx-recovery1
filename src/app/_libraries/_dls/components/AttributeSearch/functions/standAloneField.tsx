import {
  AttributeSearchDetailData,
  NoCombineField,
  AttributeSearchField
} from '..';
import { fieldsMap } from '../utils';

const standAloneField = (
  fields: AttributeSearchField[],
  noCombineFields: NoCombineField[],
  currentDataFiltered: any,
  setNoFilterResultText: (value: React.SetStateAction<string>) => void,
  noFilterResultTextProp: string
) => {
  if (!Array.isArray(noCombineFields)) return currentDataFiltered;

  const mappedFields = fieldsMap(fields) as Record<string, any>;
  let hasNoCombinedField = false;
  noCombineFields.forEach(noCombineField => {
    if (mappedFields[noCombineField.field]) {
      hasNoCombinedField = true;
      setNoFilterResultText(noCombineField.filterMessage);
    }
  });

  if (hasNoCombinedField) {
    return [];
  } else {
    setNoFilterResultText(noFilterResultTextProp);
  }

  if (fields.length && currentDataFiltered.length) {
    const noCombineFieldsKey = noCombineFields.map(item => item.field);

    return currentDataFiltered.filter((field: AttributeSearchDetailData) => {
      return !noCombineFieldsKey.includes(field.key);
    });
  }

  return currentDataFiltered;
};

export { standAloneField };
