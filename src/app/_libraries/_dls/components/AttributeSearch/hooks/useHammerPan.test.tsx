import React from 'react';
import { render, screen } from '@testing-library/react';

import useHammerPan from './useHammerPan';

jest.mock('@egjs/hammerjs', () =>
  jest.requireActual('../../../test-utils/mocks/MockHammer')
);

const mockGetBoundingClientRect = ({
  left = 0,
  width = 0,
  height = 0,
  x = 0,
  y = 0
}) => {
  Element.prototype.getBoundingClientRect = () => {
    return {
      bottom: 0,
      height,
      left,
      right: 0,
      top: 0,
      width,
      x,
      y,
      toJSON: () => undefined
    };
  };
};

const Comp = ({
  hammerRef: hammerRefProp
}: {
  hammerRef?: React.MutableRefObject<HTMLDivElement | null>;
}) => {
  const hammerRef = useHammerPan();

  return (
    <div id="wrapper-element">
      <div ref={hammerRefProp || hammerRef} data-testid="useHammerPan" />
    </div>
  );
};

describe('useHammerPan', () => {
  it('do not use hammerRef', () => {
    render(<Comp hammerRef={{ current: null }} />);

    const hammerElement = screen.getByTestId('useHammerPan');

    expect(hammerElement.scrollLeft).toEqual(0);
  });

  describe('use hammerRef', () => {
    it("when active element is not hammer's children", () => {
      Element.prototype.contains = () => false;
      Element.prototype.closest = () => undefined;
      mockGetBoundingClientRect({ height: 2 });

      render(<Comp />);

      const hammerElement = screen.getByTestId('useHammerPan');

      expect(hammerElement.scrollLeft).toEqual(12);
    });

    describe("when active element is hammer's children", () => {
      it('search popup is not appeared', () => {
        Element.prototype.contains = () => true;
        Element.prototype.closest = () => undefined;
        mockGetBoundingClientRect({ left: 0 });

        render(<Comp />);

        const hammerElement = screen.getByTestId('useHammerPan');

        expect(hammerElement.scrollLeft).toEqual(0);
      });

      describe('search popup is not appeared', () => {
        it('limit pan right', () => {
          Element.prototype.contains = () => true;
          Element.prototype.closest = () => ({
            getAttribute: () => 'wrapper-element'
          });
          mockGetBoundingClientRect({ left: 0 });

          render(<Comp />);

          const hammerElement = screen.getByTestId('useHammerPan');

          expect(hammerElement.scrollLeft).toEqual(0);
        });

        it('limit pan left', () => {
          Element.prototype.contains = () => true;
          Element.prototype.closest = () => ({
            getAttribute: () => 'wrapper-element'
          });
          mockGetBoundingClientRect({ left: 0, width: 1000 });

          render(<Comp />);

          const hammerElement = screen.getByTestId('useHammerPan');

          expect(hammerElement.scrollLeft).toEqual(0);
        });

        it('no limit pan', () => {
          Element.prototype.contains = () => true;
          Element.prototype.closest = () => ({
            getAttribute: () => 'wrapper-element'
          });
          mockGetBoundingClientRect({ left: 0, width: 1000, y: 1 });

          render(<Comp />);

          const hammerElement = screen.getByTestId('useHammerPan');

          expect(hammerElement.scrollLeft).toEqual(-12);
        });
      });
    });
  });
});
