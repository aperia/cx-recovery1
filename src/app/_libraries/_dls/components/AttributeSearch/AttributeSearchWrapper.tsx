import React, { useRef, useState } from 'react';

import AttributeSearch, {
  AttributeSearchEvent,
  AttributeSearchProps,
  AttributeSearchRef,
  AttributeSearchValue
} from '.';

const AttributeSearchWrapper = ({
  value: valueProp,
  onChange,
  ...props
}: AttributeSearchProps) => {
  const containerRef = useRef<AttributeSearchRef>(null);

  const [value, setValue] =
    useState<AttributeSearchValue[] | undefined>(valueProp);

  const handleChange = (event: AttributeSearchEvent) => {
    setValue(event.value);
    onChange && onChange(event);
  };

  return (
    <div>
      <AttributeSearch
        ref={containerRef}
        value={valueProp ? value : undefined}
        onChange={onChange ? handleChange : undefined}
        {...props}
      />
      <input
        data-testid="ref-setError"
        onChange={(e: any) => {
          containerRef.current?.setError(e.target.optionalError);
        }}
      />
      <input
        data-testid="ref-setData"
        onChange={(e: any) => {
          containerRef.current?.setData(e.target.optionalData);
        }}
      />
      <input
        data-testid="ref-setValue"
        onChange={(e: any) => {
          containerRef.current?.setValue(e.target.optionalFields);
        }}
      />
      <input
        data-testid="ref-setShow"
        onChange={(e: any) => {
          containerRef.current?.setShow!(e.target.show);
        }}
      />
    </div>
  );
};

export default AttributeSearchWrapper;
