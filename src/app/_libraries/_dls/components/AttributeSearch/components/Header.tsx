import React from 'react';

import classnames from 'classnames';
import { genAmtId } from '../../../utils';

export interface AttributeSearchHeaderProps extends DLSId {
  name?: string;
  className?: string | any;
}

const Header: React.FC<AttributeSearchHeaderProps> = ({
  name,
  className,
  children,

  dataTestId,
  ...props
}) => {
  return (
    <div
      className={classnames('ds-attr-search-header', className)}
      data-testid={genAmtId(dataTestId, 'header', 'AttributeSearch.Header')}
      {...props}
    >
      {name || children}
    </div>
  );
};

export default Header;
