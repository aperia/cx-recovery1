import React from 'react';

// components
import ReactHighLightWords from 'react-highlight-words';

// utils
import classnames from 'classnames';
import { genAmtId, replaceSpecialCharacter } from '../../../utils';
import unset from 'lodash.unset';

// hooks
import { useTranslation } from '../../../hooks';

export interface AttributeSearchItem
  extends React.DetailedHTMLProps<
      React.HTMLAttributes<HTMLDivElement>,
      HTMLDivElement
    >,
    DLSId {
  name?: string;
  valueInputFilter?: string;
  description?: string;
  className?: string | any;
}

const Item = React.forwardRef<HTMLDivElement, AttributeSearchItem>(
  (
    {
      name,
      valueInputFilter = '',
      description,
      className,
      dataTestId,
      ...props
    },
    ref
  ) => {
    // handle eslint no-unused-var, can't destructor params above
    unset(props, 'children');

    const { t } = useTranslation();

    return (
      <div
        ref={ref}
        className={classnames('ds-attr-search-item', className)}
        data-testid={genAmtId(dataTestId, name, 'AttributeSearch.Item')}
        {...props}
      >
        <span className="ds-attr-search-item-name">
          <ReactHighLightWords
            searchWords={replaceSpecialCharacter(valueInputFilter).split(' ')}
            textToHighlight={name as string}
            highlightClassName="dls-mark"
          />
        </span>
        <span className="ds-attr-search-item-description">{description}</span>
        <span className="ds-attr-search-item-suggestion">
          <div className="ds-attr-search-item-press">
            {t('txt_press', 'Press')}
          </div>
          <div className="ds-attr-search-item-tag">{t('txt_tab', 'Tab')}</div>
        </span>
      </div>
    );
  }
);

Item.displayName = 'Item';

export default Item;
