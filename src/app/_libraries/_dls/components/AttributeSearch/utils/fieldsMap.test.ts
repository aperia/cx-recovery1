import fieldsMap from './fieldsMap';

import { AttributeSearchDetailData } from '..';

it('fieldsMap', () => {
  const fields: AttributeSearchDetailData[] = [
    { key: 'key_1', name: 'name_1', component: () => undefined as any },
    { key: 'key_2', name: 'name_2', component: () => undefined as any }
  ];
  const fieldsMapped = fieldsMap(fields);

  expect(fieldsMapped).toEqual({
    [fields[0].key]: fields[0],
    [fields[1].key]: fields[1]
  });
});
