import { AttributeSearchDetailData } from '..';

const fieldsMap = (fields: AttributeSearchDetailData[]) => {
  const fieldsMapped = fields.reduce((prev, curr) => {
    prev = {
      ...prev,
      [curr.key]: curr
    };

    return prev;
  }, {});

  return fieldsMapped;
};

export default fieldsMap;
