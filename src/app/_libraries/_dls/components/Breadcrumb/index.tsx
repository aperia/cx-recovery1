import React, {
  forwardRef,
  useRef,
  ForwardRefRenderFunction,
  ForwardRefExoticComponent,
  RefAttributes,
  useImperativeHandle,
  useEffect,
  useState,
  useMemo,
  useLayoutEffect
} from 'react';

// components
import DropdownButton, {
  DropdownButtonSelectEvent
} from '../../components/DropdownButton';
import Item, { ItemProps } from './Item';
import Icon from '../Icon';

// utils
import classNames from 'classnames';
import isString from 'lodash.isstring';

import { className, getAvailableWidth } from '../../utils';
import { ScreenSize } from '../../hooks/useScreenType/types';

// observe
import ResizeObserver from 'resize-observer-polyfill';

export interface BreadcrumbChangeEvent
  extends React.ChangeEvent<typeof Breadcrumb> {
  target: EventTarget &
    typeof Breadcrumb & { id?: string; name?: string; value?: any };
  value?: any;
}

export interface BreadcrumbProps extends DLSId {
  id?: string;
  name?: string;

  children:
    | React.ReactElement<ItemProps, typeof Item>
    | React.ReactElement<ItemProps, typeof Item>[];

  activeCrumb?: string;
  onChange?: (event: BreadcrumbChangeEvent) => void;

  className?: string;
}

export interface ExtraStaticProp
  extends ForwardRefExoticComponent<
    BreadcrumbProps & RefAttributes<HTMLDivElement>
  > {
  Item: typeof Item;
}

/**
 * @param {number} parentWidth The parent's width of component
 * @returns {boolean} True if parent's width > 768 & < 1024
 */
const checkTabletView = (parentWidth: number) => {
  const isTabletView =
    parentWidth < ScreenSize.TABLET_LANDSCAPE.width &&
    parentWidth > ScreenSize.TABLET_PORTRAIT.width;
  return isTabletView;
};

const Breadcrumb: ForwardRefRenderFunction<HTMLDivElement, BreadcrumbProps> = (
  {
    id,
    name,
    children: childrenProp,
    activeCrumb,
    onChange,
    className: classNameProp,

    dataTestId
  },
  ref
) => {
  // refs
  const coreRef = useRef<Record<string, any>>({});
  const containerRef = useRef<HTMLDivElement | null>(null);

  // provide ref
  useImperativeHandle(ref, () => containerRef.current!);

  // states
  const [shouldShowThreeDot, setShouldShowThreeDot] = useState(false);
  const [countLevelWithThreeDot, setCountLevelWithThreeDot] = useState(0);
  const [children, setChildren] = useState(childrenProp);

  // handle function
  const handleUpdateChildren = (value: any) => {
    const childrenArray = React.Children.toArray(children);

    const currentIndex = childrenArray.findIndex(
      (item: any) => item.props.value === value
    );

    const newChildren = childrenArray.filter(
      (_, index) => index <= currentIndex
    ) as typeof childrenProp;

    setChildren(newChildren);
  };

  const handleCountLevel = (parentWidth: number) => {
    const isTablet = checkTabletView(parentWidth);
    setCountLevelWithThreeDot(isTablet ? 2 : 1);
  };

  const handleShowThreeDot = (parentWidth: number) => {
    setShouldShowThreeDot(parentWidth < ScreenSize.TABLET_LANDSCAPE.width);
    handleCountLevel(parentWidth);
  };

  const handleOnChange = (selectedItem: Record<string, string>) => {
    const { onChange, handleUpdateChildren } = coreRef.current;

    const nextEvent = {
      target: {
        value: selectedItem,
        id,
        name
      },
      value: selectedItem
    };

    onChange?.(nextEvent);

    // controlled
    if (activeCrumb) return;

    // uncontrolled
    handleUpdateChildren(selectedItem);
  };

  const handleCloneElement = (child: any, index: number) => {
    const { handleOnChange } = coreRef.current;
    const childProps = child.props as any;

    let itemActive = false;

    // uncontrolled
    itemActive = !Array.isArray(children)
      ? true
      : index === children?.length - 1;

    // controlled
    if (isString(activeCrumb)) {
      itemActive = childProps.active;
    }

    return React.cloneElement(child, {
      ...childProps,
      active: itemActive,
      onClick: handleOnChange
    });
  };

  // keep reference
  coreRef.current.onChange = onChange;
  coreRef.current.handleOnChange = handleOnChange;
  coreRef.current.handleUpdateChildren = handleUpdateChildren;
  coreRef.current.handleShowThreeDot = handleShowThreeDot;
  coreRef.current.handleCloneElement = handleCloneElement;

  const handleSelectItemDropdown = (event: DropdownButtonSelectEvent) => {
    handleOnChange(event.target.value.props.value);
  };

  // variables
  const nextChildren = useMemo(() => {
    return React.Children.map(
      children as React.ReactElement[],
      (child, index) => {
        const { handleCloneElement } = coreRef.current;
        const elements = handleCloneElement(child, index);
        return elements;
      }
    );
  }, [children]);

  const listLevelItems = useMemo(() => {
    if (!Array.isArray(children)) {
      return <DropdownButton.Item value={null} />;
    }

    return children
      .slice(0, children?.length - countLevelWithThreeDot)
      .map((item: any, index: number) => (
        <DropdownButton.Item
          key={index}
          label={item.props.label}
          value={item}
        />
      ));
  }, [children, countLevelWithThreeDot]);

  // Choose those levels to show outside three dot
  const elements = useMemo(() => {
    if (!Array.isArray(children)) return children;

    return React.Children.map(
      children as React.ReactElement[],
      (child, index) => {
        if (index >= children.length - countLevelWithThreeDot) {
          const { handleCloneElement } = coreRef.current;
          const elements = handleCloneElement(child, index);
          return elements;
        }
      }
    );
  }, [children, countLevelWithThreeDot]);

  const isShowThreeDot = useMemo(() => {
    if (!Array.isArray(children)) return false;

    const { parentWidth } = coreRef.current;

    const isTablet = checkTabletView(parentWidth);

    // The minimum level to show three dot
    const totalLevelNeed = isTablet ? 4 : 3;

    return shouldShowThreeDot && children?.length >= totalLevelNeed;
  }, [children, shouldShowThreeDot]);

  // effects
  useLayoutEffect(() => {
    const { parentElement } = containerRef.current as HTMLDivElement;

    const { handleShowThreeDot } = coreRef.current;

    // Observable parent's width change
    const resizeObserver = new ResizeObserver(() => {
      const availableWidth = getAvailableWidth(parentElement);

      coreRef.current.parentWidth = availableWidth;

      handleShowThreeDot(availableWidth);
    });

    resizeObserver.observe(parentElement!);

    return () => resizeObserver.disconnect();
  }, []);

  useEffect(() => {
    // Controlled: update list breadcrumb base on behavior of dev
    setChildren(childrenProp);
  }, [childrenProp]);

  return (
    <nav
      ref={containerRef}
      className={classNames(className.breadcrumb.CONTAINER, classNameProp)}
      aria-label="breadcrumb"
      data-testid={dataTestId}
    >
      {isShowThreeDot ? (
        <ol className="d-flex pl-0">
          <li className="dls-breadcrumb-item dls-breadcrumb-item-icon d-flex mr-0">
            <DropdownButton
              buttonProps={{
                children: <Icon name="more" />,
                variant: 'outline-primary'
              }}
              onSelect={handleSelectItemDropdown}
            >
              {listLevelItems}
            </DropdownButton>
          </li>
          {elements}
        </ol>
      ) : (
        <ol
          className={classNames(
            className.breadcrumb.LIST,
            'd-flex pl-0',
            classNameProp
          )}
        >
          {nextChildren}
        </ol>
      )}
    </nav>
  );
};

const ExtraStaticBreadcrumb = forwardRef<HTMLDivElement, BreadcrumbProps>(
  Breadcrumb
) as ExtraStaticProp;
ExtraStaticBreadcrumb.Item = Item;

export default ExtraStaticBreadcrumb;
