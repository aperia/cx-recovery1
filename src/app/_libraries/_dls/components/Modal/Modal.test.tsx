import React, { MutableRefObject } from 'react';
import '@testing-library/jest-dom';
import { render, screen } from '@testing-library/react';

import Modal from './Modal';
import { queryByClass } from '../../test-utils/queryHelpers';

const mockRef: MutableRefObject<HTMLElement | null> = { current: null };

describe('Render', () => {
  it('modal should not show', () => {
    render(<Modal ref={mockRef} show={false} />);

    expect(screen.queryByRole('dialog')).toBeNull();
  });

  it('modal should show', () => {
    render(<Modal ref={mockRef} show />);

    expect(screen.getByRole('dialog')).toBeInTheDocument();
  });

  it('modal should has className', () => {
    const { baseElement } = render(
      <Modal ref={mockRef} show className="class-name" />
    );

    expect(
      queryByClass(baseElement as HTMLElement, /class-name/)
    ).toBeInTheDocument();
  });

  describe('modal position', () => {
    it('modal should in the center screen', () => {
      const { baseElement } = render(<Modal ref={mockRef} show lt />);

      expect(
        queryByClass(baseElement as HTMLElement, /modal-dialog-centered/)
      ).toBeInTheDocument();
    });

    it('modal should in the left screen', () => {
      const { baseElement } = render(<Modal ref={mockRef} show lt />);

      expect(
        queryByClass(baseElement as HTMLElement, /modal-left/)
      ).toBeInTheDocument();
    });

    it('modal should in the right screen', () => {
      const { baseElement } = render(<Modal ref={mockRef} show rt />);

      expect(
        queryByClass(baseElement as HTMLElement, /modal-right/)
      ).toBeInTheDocument();
    });
  });

  describe('modal size', () => {
    it('modal full screen', () => {
      const { baseElement } = render(<Modal ref={mockRef} show full />);

      expect(
        queryByClass(baseElement as HTMLElement, /modal-full/)
      ).toBeInTheDocument();
    });

    it('modal small size', () => {
      const { baseElement } = render(<Modal ref={mockRef} show sm />);

      expect(
        queryByClass(baseElement as HTMLElement, /window-sm/)
      ).toBeInTheDocument();
    });

    it('modal middle size', () => {
      const { baseElement } = render(<Modal ref={mockRef} show md />);

      expect(
        queryByClass(baseElement as HTMLElement, /window-md/)
      ).toBeInTheDocument();
    });

    it('modal large size', () => {
      const { baseElement } = render(<Modal ref={mockRef} show lg />);

      expect(
        queryByClass(baseElement as HTMLElement, /window-lg/)
      ).toBeInTheDocument();
    });

    it('modal extremely small size', () => {
      const { baseElement } = render(<Modal ref={mockRef} show xs />);

      expect(
        queryByClass(baseElement as HTMLElement, /window-xs/)
      ).toBeInTheDocument();
    });
  });

  describe('modal placement', () => {
    it('modal should place in center', () => {
      const { baseElement } = render(<Modal ref={mockRef} show />);

      expect(
        queryByClass(baseElement as HTMLElement, /modal-dialog-centered/)
      ).toBeInTheDocument();
    });

    it('modal should place in top', () => {
      const { baseElement } = render(
        <Modal ref={mockRef} show placement={'top'} />
      );

      expect(
        queryByClass(baseElement as HTMLElement, /placement-top/)
      ).toBeInTheDocument();
    });
  });

  it('modal should loading', () => {
    const { baseElement } = render(<Modal ref={mockRef} show loading />);

    expect(
      queryByClass(baseElement as HTMLElement, /loading/)
    ).toBeInTheDocument();
  });

  it('modal should has animation', () => {
    const { baseElement } = render(
      <Modal ref={mockRef} show animationDuration={1000} />
    );

    expect(
      queryByClass(baseElement as HTMLElement, /modal-backdrop/)
    ).toHaveStyle('animation-duration: 1000ms;');

    expect(
      queryByClass(baseElement as HTMLElement, /dls-modal-dialog/)
    ).toHaveStyle('animation-duration: 1000ms;');
  });
});

describe('Actions', () => {
  it('on hide', () => {
    const onHide = jest.fn();

    const { baseElement } = render(
      <Modal ref={mockRef} show onHide={onHide} />
    );

    // click outside of Modal to trigger onHide
    queryByClass(baseElement as HTMLElement, /modal-backdrop/)!.click();

    expect(onHide).toBeCalled();
  });
});
