(Math as any).easeInOutQuad = (
  currentTime: number,
  start: number,
  change: number,
  duration: number
) => {
  currentTime = currentTime / (duration / 2);
  if (currentTime < 1) return (change / 2) * currentTime * currentTime + start;
  currentTime--;
  return (-change / 2) * (currentTime * (currentTime - 2) - 1) + start;
};

const scrollTo = (element: HTMLElement, to: number, duration: number) => {
  const start = element.scrollTop;
  const change = to - start;
  let currentTime = 0;
  const increment = 20;

  const animateScroll = () => {
    currentTime += increment;
    const value = (Math as any).easeInOutQuad(
      currentTime,
      start,
      change,
      duration
    );
    element.scrollTop = value;
    if (currentTime < duration) setTimeout(animateScroll, increment);
  };

  animateScroll();
};

export default scrollTo;
