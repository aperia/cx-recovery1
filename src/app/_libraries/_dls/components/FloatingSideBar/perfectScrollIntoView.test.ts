import perfectScrollIntoView from './perfectScrollIntoView';

describe('perfectScrollIntoView', () => {
  it('call without element', () => {
    const element = null as unknown as HTMLDivElement;
    const prevBtnElement = null as unknown as HTMLDivElement;
    const nextBtnElement = null as unknown as HTMLDivElement;
    const containerElement = null as unknown as HTMLDivElement;

    perfectScrollIntoView(
      element,
      prevBtnElement,
      nextBtnElement,
      containerElement
    );
  });

  const scrollBy = jest.fn();

  const prevBtnElement = {
    getBoundingClientRect: () => ({ height: 100, y: 100 })
  } as unknown as HTMLDivElement;
  const nextBtnElement = {
    getBoundingClientRect: () => ({ height: 100, y: 900 })
  } as unknown as HTMLDivElement;
  const containerElement = { scrollBy } as unknown as HTMLDivElement;

  it('call with getBoundingClientRect return null', () => {
    scrollBy.mockClear();
    const scrollIntoView = jest.fn();

    const element = {
      scrollIntoView,
      getBoundingClientRect: () => ({ height: 300, y: 300 }),
      nextSibling: {}
    } as unknown as HTMLDivElement;
    const prevBtnElement = {
      getBoundingClientRect: () => null
    } as unknown as HTMLDivElement;
    const nextBtnElement = {
      getBoundingClientRect: () => null
    } as unknown as HTMLDivElement;

    perfectScrollIntoView(
      element,
      prevBtnElement,
      nextBtnElement,
      containerElement
    );

    expect(scrollIntoView).toBeCalledWith({
      behavior: 'smooth',
      block: 'nearest'
    });
    expect(scrollBy).not.toBeCalled();
  });

  it('call with perfect view', () => {
    scrollBy.mockClear();
    const scrollIntoView = jest.fn();

    const element = {
      scrollIntoView,
      getBoundingClientRect: () => ({ height: 300, y: 300 }),
      nextSibling: {}
    } as unknown as HTMLDivElement;

    perfectScrollIntoView(
      element,
      prevBtnElement,
      nextBtnElement,
      containerElement
    );

    expect(scrollIntoView).toBeCalledWith({
      behavior: 'smooth',
      block: 'nearest'
    });
    expect(scrollBy).not.toBeCalled();
  });

  describe('call with view replaced by prev button', () => {
    it('element has previousSibling', () => {
      const scrollIntoView = jest.fn();

      const element = {
        scrollIntoView,
        getBoundingClientRect: () => ({ height: 300, y: 100 }),
        previousSibling: {}
      } as unknown as HTMLDivElement;

      perfectScrollIntoView(
        element,
        prevBtnElement,
        nextBtnElement,
        containerElement
      );

      expect(scrollIntoView).toBeCalledWith({
        behavior: 'smooth',
        block: 'nearest'
      });
      expect(scrollBy).toBeCalledWith(0, -100);
    });

    it('element has no previousSibling', () => {
      const scrollIntoView = jest.fn();

      const element = {
        scrollIntoView,
        getBoundingClientRect: () => ({ height: 300, y: 100 })
      } as unknown as HTMLDivElement;

      perfectScrollIntoView(
        element,
        prevBtnElement,
        nextBtnElement,
        containerElement
      );

      expect(scrollIntoView).toBeCalledWith({
        behavior: 'smooth',
        block: 'nearest'
      });
      expect(scrollBy).toBeCalledWith(0, -104);
    });
  });

  describe('call with view replaced by next button', () => {
    it('element has nextSibling', () => {
      const scrollIntoView = jest.fn();

      const element = {
        scrollIntoView,
        getBoundingClientRect: () => ({ height: 300, y: 800 }),
        nextSibling: {}
      } as unknown as HTMLDivElement;

      perfectScrollIntoView(
        element,
        prevBtnElement,
        nextBtnElement,
        containerElement
      );

      expect(scrollIntoView).toBeCalledWith({
        behavior: 'smooth',
        block: 'nearest'
      });
      expect(scrollBy).toBeCalledWith(0, 200);
    });

    it('element has no nextSibling', () => {
      const scrollIntoView = jest.fn();

      const element = {
        scrollIntoView,
        getBoundingClientRect: () => ({ height: 300, y: 800 })
      } as unknown as HTMLDivElement;

      perfectScrollIntoView(
        element,
        prevBtnElement,
        nextBtnElement,
        containerElement
      );

      expect(scrollIntoView).toBeCalledWith({
        behavior: 'smooth',
        block: 'nearest'
      });
      expect(scrollBy).toBeCalledWith(0, 204);
    });
  });
});
