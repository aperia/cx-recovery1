import React from 'react';
import { render, RenderResult, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

import FloatingSideBarTabItem, {
  FloatingSideBarItem,
  FloatingSideBarTabItemProps
} from './FloatingSideBarTabItem';
import { queryByClass } from '../../test-utils/queryHelpers';

let wrapper: RenderResult;

interface RenderComponentProps
  extends Omit<FloatingSideBarTabItemProps, 'children'> {}

const item = {
  key: 100000520,
  text: '100000520',
  actived: false
};

const renderComponent = (props: RenderComponentProps) => {
  wrapper = render(
    <FloatingSideBarTabItem ref={{ current: null }} {...props} />
  );

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLDivElement,
    rerender: wrapper.rerender
  };
};

describe('Render', () => {
  it('should render', () => {
    const { baseElement } = renderComponent({
      item,
      className: 'class-name',
      groupActived: true,
      refs: {}
    });

    expect(queryByClass(baseElement, /class-name/)).toBeInTheDocument();
  });

  it('should render without item', () => {
    const { baseElement } = renderComponent({
      item: undefined as unknown as FloatingSideBarItem,
      className: 'class-name',
      groupActived: true,
      refs: {}
    });

    expect(queryByClass(baseElement, /class-name/)).toBeInTheDocument();
  });

  it('should render with active item', () => {
    const { baseElement } = renderComponent({
      className: 'class-name',
      groupActived: true,
      item: { ...item, actived: true },
      refs: {}
    });

    expect(queryByClass(baseElement, /class-name/)).toBeInTheDocument();
  });

  it('should render with refs', () => {
    const { baseElement } = renderComponent({
      item,
      className: 'class-name',
      refs: { hammerRef: {} }
    });

    expect(queryByClass(baseElement, /class-name/)).toBeInTheDocument();
  });
});

describe('Actions', () => {
  describe('handleOnClickItem', () => {
    it('controlled', () => {
      const onSelectItem = jest.fn();

      renderComponent({ item, onSelectItem });

      screen.getByText(item.text).click();

      expect(onSelectItem).toBeCalledWith({ value: item });
    });

    it('uncontrolled', () => {
      renderComponent({ item });

      screen.getByText(item.text).click();
    });
  });

  describe('handleOnClickIconClose', () => {
    it('controlled', () => {
      const onCloseItem = jest.fn();

      const { baseElement } = renderComponent({ item, onCloseItem });

      queryByClass(baseElement, /tab-item-wrap-icon/)!.click();

      expect(onCloseItem).toBeCalledWith({ value: item });
    });

    it('uncontrolled', () => {
      const { baseElement } = renderComponent({ item });

      queryByClass(baseElement, /tab-item-wrap-icon/)!.click();
    });
  });
});
