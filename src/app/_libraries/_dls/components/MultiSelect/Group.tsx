import React, { useEffect, useRef } from 'react';

// utils
import {
  classes,
  extendsEvent,
  keycode,
  reactChildrenCount,
  truncateTooltip
} from '../../utils';
import classnames from 'classnames';
import { get, isEmpty, isFunction, isString, pick } from '../../lodash';

// types
import { MultiSelectProps } from './types';
import { DropdownBaseRef } from '../DropdownBase';

// components
import Icon from '../Icon';
import Label from '../Label';

// hooks
import { useAfterCloseOnScroll } from './hooks';

export interface GroupProps
  extends Pick<
    MultiSelectProps,
    | 'label'
    | 'placeholder'
    | 'value'
    | 'textField'
    | 'disabled'
    | 'required'
    | 'error'
    | 'id'
    | 'name'
    | 'children'
    | 'onFocus'
    | 'onBlur'
    | 'opened'
    | 'variant'
    | 'autoFocus'
    | 'groupFormatInputText'
  > {
  handleOpenedMode: (nextOpened: boolean) => void;
  baseChildren: MultiSelectProps['children'];
  replacer?: DropdownBaseRef['searchBarElement'];

  openedControlled?: boolean;
}

const Group: React.FC<GroupProps> = ({
  label,
  placeholder,
  value,
  textField = '',
  disabled,
  required,
  error,
  id,
  name,
  onFocus,
  onBlur,
  handleOpenedMode,
  opened,
  variant,

  baseChildren,
  replacer,

  autoFocus,
  openedControlled,

  groupFormatInputText
}) => {
  const groupRef = useRef<HTMLDivElement | null>(null);
  const textFieldRef = useRef<HTMLDivElement | null>(null);
  const floatingLabelRef = useRef<HTMLSpanElement | null>(null);
  const navigateByKeyboardRef = useRef<'tab' | 'shift-tab' | 'none'>('none');
  const autoFocusRef = useRef(autoFocus);
  const openedControlledRef = useRef(openedControlled);

  // handle mousedown on MultiSelect element
  const handleOnMouseDown = (
    event?: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    const triggerElement = replacer || groupRef.current;

    if (triggerElement && document.activeElement === triggerElement) {
      // triggerElement.blur() will trigger blur, but focus will be trigger also, and blur will be trigger again
      // this is not behavior we want, so just preventDefault focus/blur event.
      event?.preventDefault();

      triggerElement.blur();
    }
  };

  // handle focus on MultiSelect element
  const handleOnFocus = () => {
    const isTab = navigateByKeyboardRef.current === 'tab';
    const isShiftTab = navigateByKeyboardRef.current === 'shift-tab';

    if (isTab || isShiftTab) {
      navigateByKeyboardRef.current = 'none';

      document.dispatchEvent(
        new KeyboardEvent('keydown', {
          keyCode: keycode.TAB,
          shiftKey: isShiftTab
        })
      );
      return;
    }

    let nextEvent = extendsEvent({} as any, 'target', { id, name, value });
    nextEvent = extendsEvent(nextEvent, null, { value });

    isFunction(onFocus) && onFocus(nextEvent);
    handleOpenedMode(true);
  };

  // handle blur on MultiSelect element
  const handleOnBlur: React.FocusEventHandler<HTMLDivElement> = (event) => {
    // if replacer is exists, we know that
    // MultiSelect has search bar in dropdown, and search bar is focusing
    // search bar will handle blur
    if (replacer) return;

    // what if the user clicks on something that is not in the browser?
    // example: desktop, taskbar, developer tools...
    // by default, if the user clicks on those thing, the browser will keeps
    // the previous state
    // So the next time, the user clicks (mousedown) on the screen (valid container DOM browser),
    // onFocus will be called and onMouseDown will be not called
    // we don't want that behavior
    // so this code block to prevent that behavior
    if (groupRef.current && document.activeElement === groupRef.current) {
      groupRef.current.blur();
    }

    let nextEvent = extendsEvent({} as any, 'target', { id, name, value });
    nextEvent = extendsEvent(nextEvent, null, { value });

    isFunction(onBlur) && onBlur(nextEvent);
    handleOpenedMode(false);
  };

  // handle mousedown on icon
  const handleOnMouseDownIcon = (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>
  ) => {
    if (disabled) return;

    // if we call groupRef.current.focus() inside mousedown icon,
    // groupRef.current will be focus, and call blur also...
    // this is not behavior we want, so we need to stop default behavior browser (blur/focus)
    event.preventDefault();

    const triggerElement = replacer || groupRef.current;

    if (triggerElement && document.activeElement === triggerElement) {
      triggerElement.blur();
      return;
    }

    triggerElement?.focus({ preventScroll: true });
  };

  // handle truncate tooltip label
  useEffect(() => truncateTooltip(floatingLabelRef.current!), [label]);

  // handle truncate tooltip value
  useEffect(() => truncateTooltip(textFieldRef.current!), [value]);

  // scroll auto close will trigger opened to false but onBlur is not triggered
  // we need trigger handleBlur manually
  useAfterCloseOnScroll({
    opened,
    blurElement: groupRef.current!
  });

  // adjust previous and next focus element on tab/shifttab
  useEffect(() => {
    if (!replacer) return;

    const handleKeydown = (event: KeyboardEvent) => {
      const { keyCode, shiftKey } = event;

      if (keyCode !== keycode.TAB) return;

      const isShiftTab = shiftKey;

      navigateByKeyboardRef.current = isShiftTab ? 'shift-tab' : 'tab';
      groupRef.current?.focus();
    };

    window.addEventListener('keydown', handleKeydown);
    return () => window.removeEventListener('keydown', handleKeydown);
  }, [replacer]);

  // handle auto focus
  useEffect(() => {
    if (openedControlledRef.current || !autoFocusRef.current) {
      return;
    }

    // check details: useEffect(() => setOpened(!!openedProp), [openedProp]); 238 MultiSelect index.tsx
    process.nextTick(() => groupRef.current?.focus());
  }, []);

  const formatInputText = () => {
    const baseChildrenLength = reactChildrenCount(baseChildren);

    const inputText = groupFormatInputText?.(
      isEmpty(value) ? 0 : value.length,
      baseChildrenLength,
      value,
      textField
    );

    if (isString(inputText)) return inputText;

    if (isEmpty(value)) {
      return variant === 'group' ? '' : 'No items selected';
    }

    if (value.length === 1 && isString(textField)) {
      return get(value, ['0', textField]);
    }

    if (value.length === baseChildrenLength) {
      return 'All items selected';
    }

    return `${value.length} of ${baseChildrenLength} selected`;
  };

  // check should show error or not
  const { status } = pick(error, 'status');

  // check should show placeholder or not
  const showPlaceholder = placeholder;

  const inputText = formatInputText();

  return (
    <>
      <div
        ref={groupRef}
        onMouseDown={handleOnMouseDown}
        onFocus={handleOnFocus}
        onBlur={handleOnBlur}
        tabIndex={disabled ? -1 : 0}
        className={classes.inputContainer.container}
      >
        <div
          ref={textFieldRef}
          className={classnames({
            [classes.inputContainer.input]: true,
            [classes.inputContainer.placeholder]: inputText
              ? false
              : showPlaceholder
          })}
        >
          {inputText || (showPlaceholder && placeholder)}
        </div>
        {label && (
          <Label ref={floatingLabelRef} asterisk={required} error={status}>
            <span>{label}</span>
          </Label>
        )}
      </div>
      <Icon onMouseDown={handleOnMouseDownIcon} size="3x" name="chevron-down" />
    </>
  );
};

export default Group;
