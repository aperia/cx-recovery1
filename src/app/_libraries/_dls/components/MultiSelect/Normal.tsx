import React, {
  DOMAttributes,
  useEffect,
  useLayoutEffect,
  useRef
} from 'react';

// utils
import { isEmpty, isFunction, pick } from '../../lodash';
import {
  canvasTextWidth,
  classes,
  extendsEvent,
  keycode,
  truncateTooltip
} from '../../utils';
import { responsiveFilterElement } from './helpers';

// types
import { DropdownBaseChangeEvent } from '../DropdownBase';
import { MultiSelectProps } from './types';

// components
import Icon from '../Icon';
import Label from '../Label';

export interface NormalProps
  extends Pick<
    MultiSelectProps,
    | 'label'
    | 'placeholder'
    | 'value'
    | 'textField'
    | 'required'
    | 'error'
    | 'id'
    | 'name'
    | 'children'
    | 'onFocus'
    | 'onBlur'
    | 'opened'
    | 'readOnly'
    | 'disabled'
  > {
  handleOpenedMode: (nextOpened: boolean) => void;
  handleValueMode: (event: DropdownBaseChangeEvent) => void;
  filter: string;
  handleFilterMode: (nextFilter: string) => void;
}

const Normal: React.FC<NormalProps> = ({
  label,
  readOnly,
  disabled,
  required,
  error,
  id,
  name,
  onFocus,
  onBlur,
  textField = '',
  opened,
  handleOpenedMode,
  filter,
  handleFilterMode,
  value,
  handleValueMode,
  placeholder
}) => {
  // refs
  const groupRef = useRef<HTMLDivElement | null>(null);
  const listRef = useRef<HTMLDivElement | null>(null);
  const inputRef = useRef<HTMLInputElement | null>(null);
  const floatingLabelRef = useRef<HTMLSpanElement | null>(null);
  const coreRef = useRef<Record<string, any>>({});

  // handle mousedown on MultiSelect element
  const handleOnMouseDown = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    if (disabled) return;

    // sequence event is:
    // - mousedown
    // - blur
    // - mouseup
    // - click
    // if we don't have preventDefault, by default after mousedown run successfully
    // blur event will be trigger
    // so we will lost focus input element
    event.preventDefault();

    const inputElement = inputRef.current;
    inputElement?.focus();
  };

  // handle focus on MultiSelect element
  const handleOnFocus = () => {
    let nextEvent = extendsEvent({} as any, 'target', { id, name, value });
    nextEvent = extendsEvent(nextEvent, null, { value });

    isFunction(onFocus) && onFocus(nextEvent);
    handleOpenedMode(true);
  };

  // handle blur on MultiSelect element
  const handleOnBlur = () => {
    // what if the user clicks on something that is not in the browser?
    // example: desktop, taskbar, developer tools...
    // by default, if the user clicks on those thing, the browser will keeps
    // the previous state
    // So the next time, the user clicks (mousedown) on the screen (valid container DOM browser),
    // onFocus will be called and onMouseDown will be not called
    // we don't want that behavior
    // so this code block to prevent that behavior
    if (inputRef.current && document.activeElement === inputRef.current) {
      inputRef.current.blur();
    }

    responsiveFilterElement({
      containerElement: listRef.current!,
      filterElement: inputRef.current!.parentElement!,
      callback: () => handleFilterMode('')
    });

    let nextEvent = extendsEvent({} as any, 'target', { id, name, value });
    nextEvent = extendsEvent(nextEvent, null, { value });

    isFunction(onBlur) && onBlur(nextEvent);
    handleOpenedMode(false);
  };

  // stop bubbling mousedown from icon to container
  const handleOnMouseDownIcon: DOMAttributes<HTMLElement>['onMouseDown'] = (
    event
  ) => {
    event.stopPropagation();

    // if stop propagation (bubbling) -> lost focus behavior on container
    // use preventDefault to prevent blur event from occurring
    // and keep focus (if exists) on container
    event.preventDefault();
  };

  const handleOnClickIcon = (
    event: React.MouseEvent<HTMLSpanElement, MouseEvent>,
    itemNeedRemove: any
  ) => {
    // trigger remove;
    const nextValueMultiple = value?.filter(
      (item: any) => item !== itemNeedRemove
    );
    let nextEvent = extendsEvent({} as any, 'target', {
      id,
      name,
      value: nextValueMultiple
    });
    nextEvent = extendsEvent(nextEvent, null, { value: nextValueMultiple });

    handleValueMode(nextEvent);

    // if there is no item in nextValueMultiple
    // trigger opened -> true
    isEmpty(nextValueMultiple) && handleOpenedMode(true);
  };

  // handle keydown backspace to delete last item
  const handleOnKeydown: DOMAttributes<HTMLInputElement>['onKeyDown'] = (
    event
  ) => {
    const { handleValueMode, value, filter, id, name } = coreRef.current;
    if (
      event.keyCode === keycode.BACKSPACE &&
      isEmpty(filter) &&
      !isEmpty(value)
    ) {
      const nextValueMultiple = value.slice(0, value.length - 1);
      let nextEvent = extendsEvent({} as any, 'target', {
        id,
        name,
        value: nextValueMultiple
      });
      nextEvent = extendsEvent(nextEvent, null, { value: nextValueMultiple });

      handleValueMode(nextEvent);
    }
  };

  const handleOnChangeFilter = (event: React.ChangeEvent<HTMLInputElement>) => {
    if (readOnly || disabled) return;
    handleFilterMode(event.target.value);
  };

  // core ref
  coreRef.current.handleValueMode = handleValueMode;
  coreRef.current.value = value;
  coreRef.current.filter = filter;
  coreRef.current.id = id;
  coreRef.current.name = name;

  // handle dynamic input width
  useLayoutEffect(() => {
    const extraWidth = filter === ' ' ? 16 : 10;

    const textMetrics = canvasTextWidth(
      filter || (isEmpty(value) ? placeholder : '') || ''
    );
    if (!textMetrics) return;

    inputRef.current!.style.width = textMetrics.width + extraWidth + 'px';
  }, [filter, placeholder, value, textField]);

  // handle truncate tooltip label
  useEffect(() => truncateTooltip(floatingLabelRef.current!), [label]);

  // close on scroll will trigger opened to false but onBlur is not triggered
  // we need trigger handleBlur manually
  useEffect(() => {
    if (opened) return;

    if (inputRef.current && document.activeElement === inputRef.current) {
      inputRef.current.blur();
    }
  }, [opened]);

  // if opened is controlled, input filter not focused -> focus input filter
  useEffect(() => {
    if (!opened || document.activeElement === inputRef.current) return;

    inputRef.current?.focus();
  }, [opened]);

  // check should show placeholder or not
  const showPlaceholder = isEmpty(value);

  // check should show error or not
  const { status } = pick(error, 'status');

  return (
    <div
      ref={groupRef}
      onMouseDown={handleOnMouseDown}
      className={classes.inputContainer.container}
    >
      <div ref={listRef} className="list">
        {value?.map((item: any, index: number) => {
          return (
            <div key={index} className="item">
              <span className="label">{item[textField]}</span>
              <Icon
                onMouseDown={handleOnMouseDownIcon}
                onClick={(event) => handleOnClickIcon(event, item)}
                size="3x"
                name="close"
              />
            </div>
          );
        })}
        <div className="filter">
          <input
            ref={inputRef}
            onFocus={handleOnFocus}
            onBlur={handleOnBlur}
            onKeyDown={handleOnKeydown}
            value={filter}
            onChange={handleOnChangeFilter}
            placeholder={showPlaceholder ? placeholder : undefined}
            type="text"
            autoComplete="off"
          />
        </div>
      </div>
      {label && (
        <Label ref={floatingLabelRef} asterisk={required} error={status}>
          <span>{label}</span>
        </Label>
      )}
    </div>
  );
};

export default Normal;
