// testing library
import '@testing-library/jest-dom';

// helpers
import responsiveFilterElement from './responsiveFilterElement';

describe('test responsiveFilterElement', () => {
  it('responsive', () => {
    responsiveFilterElement({
      containerElement: {
        getBoundingClientRect: () => ({
          width: 28
        })
      },
      filterElement: {
        getBoundingClientRect: () => ({
          width: 1
        }),
        previousElementSibling: {
          getBoundingClientRect: () => ({
            width: 1
          })
        },
        style: {
          width: 0
        }
      },
      callback: () => undefined
    } as any);
    expect(true).toBeTruthy();
  });

  it('not responsive', () => {
    responsiveFilterElement({
      containerElement: {
        getBoundingClientRect: () => ({
          width: 30
        })
      },
      filterElement: {
        getBoundingClientRect: () => ({
          width: 1
        }),
        previousElementSibling: {
          getBoundingClientRect: () => ({
            width: undefined
          })
        },
        style: {
          width: 0
        }
      },
      callback: () => undefined
    } as any);
  });
  expect(true).toBeTruthy();
});
