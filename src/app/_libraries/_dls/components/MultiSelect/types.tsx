import {
  DropdownBaseProps,
  DropdownBaseRef,
  DropdownBaseItemProps,
  DropdownBaseGroupProps,
  DropdownBaseItemRef
} from '../DropdownBase';
import { PopupBaseRef, PopupBaseProps } from '../PopupBase';
import { TooltipProps } from '../Tooltip';

export interface MultiSelectRef {
  popupBaseRef?: React.MutableRefObject<PopupBaseRef | null>;
  dropdownBaseRef?: React.MutableRefObject<DropdownBaseRef | null>;
}

export interface MultiSelectProps
  extends Omit<
      DropdownBaseProps,
      | 'selected'
      | 'onSelect'
      | 'allowKeydown'
      | 'reference'
      | 'scrollContainerProps'
      | 'filter'
      | 'onFilter'
      | 'checkAll'
      | 'checkAllLength'
      | 'baseChildren'
    >,
    DLSId {
  label?: React.ReactNode;
  textField?: string;

  onBlur?: (event: React.FocusEvent) => void;
  onFocus?: (event: React.FocusEvent) => void;

  opened?: boolean;
  onVisibilityChange?: (nextOpened: boolean) => void;

  // For easy maintenance, controlled filter is not supported
  // Just provides a mechanism to watch the filter
  onFilterChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  maxLengthFilter?: number;

  // Should show checkAll option or not
  checkAll?: boolean;
  // Edit checkAll text
  checkAllLabel?: string;
  // Should show searchBar or not
  searchBar?: boolean;
  // Edit placeholder of the searchBar
  searchBarPlaceholder?: string;

  readOnly?: boolean;
  disabled?: boolean;
  placeholder?: string;

  required?: boolean;
  error?: {
    message?: string;
    status?: boolean;
  };
  errorTooltipProps?: TooltipProps;

  popupBaseProps?: Omit<
    PopupBaseProps,
    'opened' | 'onVisibilityChange' | 'reference'
  >;
  autoFocus?: boolean;

  variant?: 'group' | 'no-border';

  groupFormatInputText?: (
    itemsSelected: number,
    itemsLength: number,
    value?: any,
    textField?: string
  ) => string;
}

export interface MultiSelectItemRef extends DropdownBaseItemRef {}

export interface MultiSelectItemProps extends DropdownBaseItemProps {}

export interface MultiSelectGroupProps extends DropdownBaseGroupProps {}
