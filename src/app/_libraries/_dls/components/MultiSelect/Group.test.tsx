import React from 'react';
import {
  fireEvent,
  render,
  RenderResult,
  screen
} from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

// components
import MultiSelect from '.';
import Group, { GroupProps } from './Group';

// mocks
import '../../test-utils/mocks/mockCanvas';

// utils
import { queryByClass } from '../../test-utils/queryHelpers';
import { MultiSelectProps } from './types';
import Button from '../Button';
import { keycode } from '../../utils';

const data = [
  { FieldID: 'ALL', FieldValue: 'ALL' },
  { FieldID: 'NER', FieldValue: 'NIGER' },
  { FieldID: 'BMU', FieldValue: 'BERMUDA' }
];
let wrapper: RenderResult;

interface RenderComponentProps
  extends Omit<GroupProps, 'children' | 'handleOpenedMode' | 'baseChildren'> {
  handleOpenedMode?: jest.Mock;
  baseChildren?: MultiSelectProps['children'];
}

const renderComponent = ({
  baseChildren: baseChildrenProp,
  ...props
}: RenderComponentProps) => {
  const baseChildren =
    baseChildrenProp ||
    data.map((item, index) => (
      <MultiSelect.Item
        key={item.FieldID + index}
        label={item.FieldValue}
        value={item}
        variant="checkbox"
      />
    ));

  jest.useFakeTimers();

  wrapper = render(
    <div>
      <Group
        label="Label"
        placeholder="My MultiSelect"
        textField="FieldID"
        handleOpenedMode={jest.fn()}
        baseChildren={baseChildren}
        {...props}
      >
        {data.map((item, index) => (
          <MultiSelect.Item
            key={item.FieldID + index}
            label={item.FieldValue}
            value={item}
            variant="checkbox"
          />
        ))}
      </Group>
    </div>
  );

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

const rerenderComponent = ({
  baseChildren: baseChildrenProp,
  ...props
}: RenderComponentProps) => {
  const baseChildren =
    baseChildrenProp ||
    data.map((item, index) => (
      <MultiSelect.Item
        key={item.FieldID + index}
        label={item.FieldValue}
        value={item}
        variant="checkbox"
      />
    ));

  jest.useFakeTimers();

  wrapper.rerender(
    <div>
      <Group
        label="Label"
        placeholder="My MultiSelect"
        textField="FieldID"
        handleOpenedMode={jest.fn()}
        baseChildren={baseChildren}
        {...props}
      >
        {data.map((item, index) => (
          <MultiSelect.Item
            key={item.FieldID + index}
            label={item.FieldValue}
            value={item}
            variant="checkbox"
          />
        ))}
      </Group>
    </div>
  );

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

afterEach(() => {
  wrapper?.unmount();
});

describe('Render', () => {
  it('should render when opened', () => {
    const { baseElement } = renderComponent({
      value: [],
      opened: true,
      groupFormatInputText: () => 'something...'
    });

    rerenderComponent({
      value: [{}],
      opened: true,
      groupFormatInputText: () => 'something...'
    });

    queryByClass(baseElement, /text-field-container/)!.focus();
    rerenderComponent({ value: [], opened: false });
    expect(true).toBeTruthy();
  });

  describe('should render when have 1 value selected', () => {
    it('with textField', () => {
      renderComponent({ value: [data[0]] });

      expect(screen.getByText(data[0].FieldValue)).toBeInTheDocument();
    });

    it('without textField', () => {
      renderComponent({
        value: [data[0]],
        textField: undefined
      });

      expect(screen.queryByText(data[0].FieldValue)).toBeNull();
    });
  });

  it('should render when have 2 values selected', () => {
    renderComponent({ value: [data[0], data[1]] });
    expect(
      screen.getByText(`2 of ${data.length} selected`)
    ).toBeInTheDocument();
  });

  it('should render when all values selected', () => {
    renderComponent({ value: data });

    expect(screen.getByText('All items selected')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  describe('handleOnMouseDownIcon', () => {
    it('when enabled', () => {
      const onFocus = jest.fn();
      const onBlur = jest.fn();

      const { baseElement } = renderComponent({
        onFocus,
        onBlur,
        value: []
      });

      // show dropdown
      userEvent.click(queryByClass(baseElement, /icon-chevron-down/)!);

      expect(onBlur).not.toBeCalled();
      expect(onFocus).toBeCalled();
      onBlur.mockClear();
      onFocus.mockClear();

      // hide dropdown
      userEvent.click(queryByClass(baseElement, /icon-chevron-down/)!);

      expect(onBlur).toBeCalled();
      expect(onFocus).not.toBeCalled();
    });

    it('when disabled', () => {
      const onFocus = jest.fn();
      const onBlur = jest.fn();

      const { baseElement } = renderComponent({
        onFocus,
        onBlur,
        value: [],
        disabled: true
      });

      // show dropdown
      userEvent.click(queryByClass(baseElement, /icon-chevron-down/)!);

      expect(onBlur).not.toBeCalled();
      expect(onFocus).not.toBeCalled();
    });
  });

  describe('handleOnMouseDown', () => {
    it('when has replacer', () => {
      const onFocus = jest.fn();
      const onBlur = jest.fn();

      const { baseElement } = renderComponent({
        onFocus,
        onBlur,
        value: [],
        replacer: {} as unknown as HTMLElement
      });

      userEvent.click(queryByClass(baseElement, /text-field-container/)!);

      expect(onBlur).not.toBeCalled();
      expect(onFocus).toBeCalled();
      onBlur.mockClear();
      onFocus.mockClear();

      // hide dropdown
      userEvent.click(queryByClass(baseElement, /text-field-container/)!);

      expect(onBlur).not.toBeCalled();
      expect(onFocus).not.toBeCalled();

      userEvent.click(document.body);

      expect(onBlur).not.toBeCalled();
      expect(onFocus).not.toBeCalled();
    });

    it('when no has replacer', () => {
      const onFocus = jest.fn();
      const onBlur = jest.fn();

      const { baseElement } = renderComponent({
        onFocus,
        onBlur,
        value: []
      });

      // show dropdown
      userEvent.click(queryByClass(baseElement, /text-field-container/)!);

      expect(onBlur).not.toBeCalled();
      expect(onFocus).toBeCalled();
      onBlur.mockClear();
      onFocus.mockClear();

      // hide dropdown
      userEvent.click(queryByClass(baseElement, /text-field-container/)!);

      expect(onBlur).toBeCalled();
      expect(onFocus).not.toBeCalled();
      onBlur.mockClear();
      onFocus.mockClear();

      // show dropdown
      userEvent.click(queryByClass(baseElement, /text-field-container/)!);

      expect(onBlur).not.toBeCalled();
      expect(onFocus).toBeCalled();
      onBlur.mockClear();
      onFocus.mockClear();

      fireEvent.blur(queryByClass(baseElement, /text-field-container/)!);

      expect(onBlur).toBeCalled();
      expect(onFocus).not.toBeCalled();
    });
  });

  it('should cover handlePopupClosed and apply next tabIndex is fakeFocusRef.current', () => {
    jest.useFakeTimers();
    const { container, queryByText } = render(
      <div>
        <MultiSelect
          variant="group"
          label="Label"
          placeholder="My MultiSelect"
          textField="FieldID"
        >
          {data.map((item, index) => (
            <MultiSelect.Item
              key={item.FieldID + index}
              label={item.FieldValue}
              value={item}
              variant="checkbox"
            />
          ))}
        </MultiSelect>
        <Button autoBlur={false}>Test handleOnPopupClosed</Button>
      </div>
    );

    const textFieldContainer = container.querySelector(
      '.text-field-container'
    )!;

    const buttonElement = queryByText('Test handleOnPopupClosed')!;
    userEvent.click(textFieldContainer);
    jest.runAllTimers();

    userEvent.click(textFieldContainer);
    jest.runAllTimers();

    userEvent.click(textFieldContainer);
    jest.runAllTimers();

    userEvent.click(buttonElement);
    jest.runAllTimers();

    expect(true).toBeTruthy();
  });

  it('should cover handleOnFocus tab/shiftTab', () => {
    renderComponent({
      value: data.slice(0, 5),
      replacer: null
    });

    renderComponent({
      value: data.slice(0, 5),
      replacer: <div>replacer</div>
    });

    // if
    window.dispatchEvent(
      new KeyboardEvent('keydown', { keyCode: keycode.UP, shiftKey: true })
    );
    // otherwise
    window.dispatchEvent(
      new KeyboardEvent('keydown', { keyCode: keycode.TAB })
    );
    window.dispatchEvent(
      new KeyboardEvent('keydown', { keyCode: keycode.TAB, shiftKey: true })
    );

    expect(true).toBeTruthy();
  });

  it('should cover autoFocus', () => {
    jest.useFakeTimers();
    renderComponent({
      value: data.slice(0, 5),
      replacer: null,
      autoFocus: true,
      openedControlled: false
    });
    jest.runAllTimers();

    expect(true).toBeTruthy();
  });
});
