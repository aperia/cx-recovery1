import React, {
  RefForwardingComponent,
  forwardRef,
  useReducer,
  useEffect,
  useCallback,
  useState,
  useRef
} from 'react';

import isEqual from 'react-fast-compare';

// types
import { GridRef, GridProps, GridReducer } from './types';

// components
import GridBase from './GridBase';
import { GridProvider, initState, gridReducer } from './GridContext';
import { genAmtId } from '../../utils';
import { nanoid } from 'nanoid';
import { useDeepValue } from '../../hooks';
import { get, isEmpty, isFunction, isUndefined, orderBy } from '../../lodash';

const Grid: RefForwardingComponent<GridRef, GridProps> = (props, ref) => {
  const {
    variant: variantProp,
    data,
    className,
    classes,

    dataItemKey = 'id',

    checkedList: checkedListPropNotStable,
    readOnlyCheckbox: readOnlyCheckboxProp,
    checkboxTooltipPropsList: checkboxTooltipPropsListProp,

    toggleButtonConfigList: toggleButtonConfigListProp,

    onCheck,
    checkAllControlled: checkAllControlledProp,

    expandedItemKey = 'id',
    expandedList: expandedListProp,
    onExpand,

    dnd,
    dndReadOnly,
    onDataChange,
    dndButtonConfigList: dndButtonConfigListProp,

    id,
    dataTestId
  } = props;

  const checkedListProp = useDeepValue(checkedListPropNotStable);
  const variant = useDeepValue(variantProp);
  const checkboxTooltipPropsList = useDeepValue(checkboxTooltipPropsListProp);
  const toggleButtonConfigList = useDeepValue(toggleButtonConfigListProp);
  const dndButtonConfigList = useDeepValue(dndButtonConfigListProp);

  // refs
  const coreRef = useRef<Record<string, any>>({});
  const dndIdRef = useRef(nanoid());
  coreRef.current.onCheck = onCheck;

  // context
  const [state, dispatch] = useReducer<GridReducer>(gridReducer, initState);

  // states
  const { checkedList, expandedList, readOnlyCheckbox } = state;
  const [dataKeyList, setDataKeyList] = useState<string[]>([]);

  // handle expand rows on Grid Body
  const handleOnExpand = useCallback(
    (isExpanded: boolean, item: Record<string, any>) => {
      const dataKey = get(item, expandedItemKey);
      const newExpandedList = isExpanded
        ? [...(expandedList || []), dataKey]
        : expandedList!.filter((key) => key !== dataKey);

      onExpand?.(newExpandedList);

      // controlled
      if (!isUndefined(expandedListProp)) return;

      // uncontrolled
      dispatch({ type: 'SET_PROPS', expandedList: newExpandedList });
    },
    [expandedItemKey, expandedList, expandedListProp, onExpand]
  );

  const handleOnCheckRadioRowBody = useCallback(
    (item: Record<string, any>) => {
      const dataKey = get(item, dataItemKey);
      const newCheckedList = [dataKey];

      coreRef.current.onCheck?.(newCheckedList);

      // controlled
      if (!isUndefined(checkedListProp)) return;

      // uncontrolled
      dispatch({ type: 'SET_PROPS', checkedList: newCheckedList });
    },
    [checkedListProp, dataItemKey]
  );

  // handle checkbox change on Grid Body
  const handleOnCheckRowBody = useCallback(
    (isChecked: boolean, item: Record<string, any>) => {
      const dataKey = get(item, dataItemKey);
      const newCheckedList = isChecked
        ? [...(checkedList || []), dataKey]
        : checkedList!.filter((key) => key !== dataKey);

      coreRef.current.onCheck?.(newCheckedList);

      // controlled
      if (!isUndefined(checkedListProp)) return;

      // uncontrolled
      dispatch({ type: 'SET_PROPS', checkedList: newCheckedList });
    },
    [dataItemKey, checkedList, checkedListProp]
  );

  const handleOnCheckRowHeader = useCallback(
    (isChecked: boolean) => {
      const nextCheckedList = isChecked ? dataKeyList : [];
      coreRef.current.onCheck?.(nextCheckedList);

      // controlled
      if (!isUndefined(checkedListProp)) return;

      // uncontrolled
      dispatch({
        type: 'SET_PROPS',
        checkedList: nextCheckedList
      });
    },
    [checkedListProp, dataKeyList]
  );

  const handleOnRowMove = (
    dragIndex: number,
    hoverIndex: number,
    type: 'hover' | 'drop'
  ) => {
    const nextData = [...data];
    const dragRecord = nextData[dragIndex];

    nextData.splice(dragIndex, 1);
    nextData.splice(hoverIndex, 0, dragRecord);

    if (type === 'hover') {
      dispatch({ type: 'SET_PROPS', data: nextData });
    }

    if (type === 'drop') {
      coreRef.current.onDataChange?.(nextData);
    }
  };

  // core ref
  coreRef.current.handleOnRowMove = handleOnRowMove;
  coreRef.current.onDataChange = onDataChange;
  coreRef.current.checkAllControlled = checkAllControlledProp;

  useEffect(() => {
    setDataKeyList(
      data
        .map((item) => get(item, dataItemKey))
        .filter((key) => !readOnlyCheckboxProp?.includes(key))
    );
  }, [data, dataItemKey, readOnlyCheckboxProp]);

  // update checkAll/indeterminate for checkbox thead when checkedList changed
  useEffect(() => {
    // if checkAll of the checkbox on thead is controlled
    const checkAllControlled = coreRef.current
      .checkAllControlled as GridProps['checkAllControlled'];
    if (isFunction(checkAllControlled)) {
      const checkAllResult = checkAllControlled(checkedList!);
      const nextCheckAll = checkAllResult === 'checked';
      const nextIndeterminate =
        checkAllResult === 'indeterminate' ? true : undefined;

      return dispatch({
        type: 'SET_PROPS',
        checkAll: nextCheckAll,
        indeterminate: nextIndeterminate
      });
    }

    // when checkAll is changed, [indeterminate] will always be undefined
    let nextCheckAll = false;
    if (!isEmpty(checkedList) && !isEmpty(dataKeyList)) {
      nextCheckAll = isEqual(orderBy(checkedList), orderBy(dataKeyList));
    }

    const nextIndeterminate =
      nextCheckAll || isEmpty(checkedList) ? undefined : true;

    dispatch({
      type: 'SET_PROPS',
      indeterminate: nextIndeterminate,
      checkAll: nextCheckAll
    });
  }, [checkedList, dataKeyList]);

  // update th readonly checkbox (for variant type is checkbox)
  useEffect(() => {
    if (isEmpty(data) || variant?.type !== 'checkbox') {
      return;
    }

    const itemKys = data.map((item) => {
      const itemKy = get(item, dataItemKey);
      return itemKy;
    });

    const readOnly = isEqual(itemKys, readOnlyCheckbox);
    dispatch({ type: 'SET_PROPS', readOnly });
  }, [variant, data, readOnlyCheckbox, dataItemKey]);

  // set props to context State
  useEffect(() => {
    dispatch({
      type: 'SET_PROPS',

      // refactoring dependencies
      ...props,
      onCheckRowBody: handleOnCheckRowBody,
      onCheckRowHeader: handleOnCheckRowHeader,
      onExpand: handleOnExpand,
      onCheckRadioRowBody: handleOnCheckRadioRowBody,

      // stable
      onCheck: coreRef.current.onCheck,
      checkedList: checkedListProp,
      variant,
      checkboxTooltipPropsList,
      toggleButtonConfigList,
      dndId: dndIdRef.current,
      dnd,
      dndReadOnly,
      dndButtonConfigList,
      onRowMove: coreRef.current.handleOnRowMove
    });
  }, [
    // 04/01/2022: WIP - refactor dependencies
    props,
    handleOnCheckRowBody,
    handleOnCheckRowHeader,
    handleOnExpand,
    handleOnCheckRadioRowBody,

    // stable
    checkedListProp,
    variant,
    checkboxTooltipPropsList,
    toggleButtonConfigList,
    dnd,
    dndReadOnly,
    dndButtonConfigList
  ]);

  return (
    // 04/01/2022: WIP - refactor value
    <GridProvider value={{ state, dispatch }}>
      <GridBase
        ref={ref}
        className={className}
        classes={classes}
        id={id}
        dataTestId={genAmtId(dataTestId, 'dls-grid', 'Grid')}
      />
    </GridProvider>
  );
};

export default forwardRef<GridRef, GridProps>(Grid);
