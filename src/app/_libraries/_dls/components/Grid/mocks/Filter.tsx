import React, { FC, useState, useRef } from 'react';

// components
import Button from '../../Button';
import TextBox from '../../TextBox';
import DropdownList from '../../DropdownList';

const filterAction = [
  // { id: 'notContains', value: 'Does not contains' },
  // { id: 'equals', value: 'Is equal to' },
  // { id: 'notEquals', value: 'Is not equal to' },
  { id: 'startWidth', value: 'Starts with' },
  // { id: 'endsWidth', value: 'Ends with' },
  // { id: 'isNull', value: 'Is null' },
  // { id: 'isNotNull', value: 'Is not null' },
  // { id: 'empty', value: 'Is empty' },
  // { id: 'notEmpty', value: 'Is not empty' },
  { id: 'contains', value: 'Contains' }
];

export type FilterType = {
  id: string;
  value?: string;
  filterType?: // | 'notContains'
  // | 'equals'
  // | 'notEquals'
  | 'startWidth'
    // | 'endsWidth'
    // | 'isNull'
    // | 'isNotNull'
    // | 'empty'
    // | 'notEmpty'
    | 'contains';
};

export interface FilterProps {
  columnId: string;
  filterBy: FilterType[];
  onFilter?: (filter: FilterType) => void;
}

const Filter: FC<FilterProps> = ({ columnId, filterBy, onFilter }) => {
  const dropdownAnchorRef = useRef<HTMLDivElement | null>(null);

  const [value] = useState('');
  const [filterType] = useState({
    id: 'startWidth',
    value: 'Starts with'
  });

  // const handleClearFilter = () => {
  //   if (!isFunction(onFilter)) return;

  //   onFilter({ id: columnId, value: '', filterType: 'startWidth' });
  // };

  // const handleFilter = () => {
  //   isFunction(onFilter) &&
  //     onFilter({ id: columnId, filterType: filterType.id as any, value });
  // };

  // useEffect(() => {
  //   const filterVal = filterBy.find(item => item.id === columnId);
  //   const { filterType: filterType, value } = filterVal || {};

  //   const filtered = filterAction.find(item => item.id === filterType);

  //   setValue(value || '');
  //   setFilterType(filtered || { id: 'startsWidth', value: 'Starts with' });
  // }, [columnId, filterBy]);

  return (
    <div className="dls-grid-filter">
      <h5 className="filter-header">Filter by</h5>

      <div ref={dropdownAnchorRef} className="dls-grid-filter-select">
        <DropdownList
          value={filterType}
          textField="value"
          // onChange={({ value }) => setFilterType(value)}
          // popupBaseProps={{ anchor: dropdownAnchorRef.current! }}
        >
          {filterAction.map(({ id, value }) => (
            <DropdownList.Item key={id} label={value} value={{ id, value }} />
          ))}
        </DropdownList>
      </div>

      <div className="dls-grid-filter-value">
        <TextBox
          value={value}
          // onChange={({ target }) => setValue(target.value || '')}
        />
      </div>

      <div
        className="dls-gclear
      rid-filter-action mt-8"
      >
        <Button size="sm" variant="secondary">
          Clear
        </Button>
        <Button size="sm" variant="primary">
          Filter
        </Button>
      </div>
    </div>
  );
};

export default Filter;
