import React from 'react';

import { Row as ReactTableRow } from 'react-table';
import { ColumnType, DndButtonConfig, ToggleButtonConfig } from './types';
import Button, { ButtonProps } from '../Button';
import Icon from '../Icon';
import CheckBox from '../CheckBox';
import Radio from '../Radio';
import Tooltip, { TooltipProps } from '../Tooltip';
import { classes, className, genAmtId, isProduction } from '../../utils';
import { get, isString } from '../../lodash';
import classnames from 'classnames';

export interface RowProps extends DLSId {
  row?: ReactTableRow;

  readonly?: boolean;
  checked?: boolean;
  checkboxTooltipProps?: TooltipProps;
  expanded?: boolean;

  horizontalScroll?: boolean;
  onExpand?: any;
  onCheck?: any;

  expandedText?: string;
  collapsedText?: string;
  toggleButtonConfig?: ToggleButtonConfig;

  className?: string;

  dropRef?: React.MutableRefObject<HTMLTableRowElement | null>;
  dragRef?: React.MutableRefObject<HTMLTableRowElement | null>;
  dndId?: string;
  dndReadOnly?: boolean;
  dndButtonConfig?: DndButtonConfig;
  onRowMove?: (
    dragIndex: number,
    hoverIndex: number,
    type: 'hover' | 'drop'
  ) => void;
}

const generateCellProps = (cell: any, hasHorizontalScroll?: boolean) => {
  const { column } = cell;
  const {
    cellProps,
    cellBodyProps,
    width: widthBase,
    className,
    style,
    autoWidth
  } = column;

  let width = widthBase;

  // if table isn't scrolling, this cell's width will be removed
  if (autoWidth && !hasHorizontalScroll) {
    width = undefined;
  }

  const extraProps = [
    { ...cellProps },
    { width, className, style },
    { ...cellBodyProps }
  ];

  const { key: cellKey, ...mergedCellProps } = cell.getCellProps(extraProps);

  return { cellKey, mergedCellProps };
};

const Row: React.FC<RowProps> = ({
  row,

  readonly,
  checked,
  checkboxTooltipProps,
  expanded,

  horizontalScroll,
  onExpand,
  onCheck,

  expandedText = 'Expand',
  collapsedText = 'Collapse',
  toggleButtonConfig,

  className: classNameProp,
  dropRef,
  dragRef,

  dndId,
  dndReadOnly = false,
  dndButtonConfig,
  onRowMove,

  dataTestId,
  ...rowProps
}) => {
  return (
    <tr
      ref={dropRef}
      className={classnames(classNameProp, {
        [classes.state.selected]: checked
      })}
      {...rowProps}
    >
      {row?.cells.map((cell) => {
        const column = cell.column as ColumnType;
        const { type: columnType, Header } = column;
        const { cellKey, mergedCellProps } = generateCellProps(
          cell,
          horizontalScroll
        );

        const rowTestId = genAmtId(
          dataTestId,
          `${isString(Header) ? Header : ''}`,
          'Grid.Row'
        );

        if (columnType === 'expand') {
          !isProduction() &&
            (expandedText || collapsedText) &&
            console.log(
              "[DLS] - Grid's expandedText/collapsedText have been deprecated, please use toggleButtonConfigList instead"
            );

          const expandedTooltipProps = get(
            toggleButtonConfig,
            'expandedTooltipProps'
          );
          const collapsedTooltipProps = get(
            toggleButtonConfig,
            'collapsedTooltipProps'
          );
          const buttonProps = get(
            toggleButtonConfig,
            'buttonProps'
          ) as ButtonProps;

          const displayTooltip = Boolean(
            (expandedText && collapsedText) ||
              (expandedTooltipProps && collapsedTooltipProps)
          );

          const buttonElement = (
            <Button
              size="sm"
              variant="icon-secondary"
              onClick={() => onExpand?.(!expanded, row.original)}
              {...buttonProps}
              dataTestId={genAmtId(rowTestId, 'expand-btn', '')}
            >
              <Icon name={expanded ? 'minus' : 'plus'} />
            </Button>
          );

          const tooltipProps: TooltipProps = {
            variant: 'primary',
            // collapsedText/expandedText will be deprecated
            element: expanded ? collapsedText : expandedText,
            ...(expanded ? collapsedTooltipProps : expandedTooltipProps),
            dataTestId: genAmtId(rowTestId, 'expand', '')
          };
          const nextElement: any = displayTooltip ? (
            <Tooltip {...tooltipProps}>{buttonElement}</Tooltip>
          ) : (
            buttonElement
          );

          return (
            <td key={cellKey} {...mergedCellProps}>
              {nextElement}
            </td>
          );
        }

        if (columnType === 'checkbox') {
          const hasTooltip = checkboxTooltipProps?.element;
          const checkBoxInput = (
            <CheckBox.Input
              checked={checked}
              readOnly={readonly}
              onChange={(event) =>
                onCheck?.(event.target.checked, row.original)
              }
            />
          );

          return (
            <td key={cellKey} {...mergedCellProps}>
              <CheckBox dataTestId={genAmtId(rowTestId, 'checkbox', '')}>
                {hasTooltip ? (
                  <Tooltip {...checkboxTooltipProps}>{checkBoxInput}</Tooltip>
                ) : (
                  checkBoxInput
                )}
              </CheckBox>
            </td>
          );
        }

        if (columnType === 'radio' || columnType === 'radio-check') {
          return (
            <td key={cellKey} {...mergedCellProps}>
              <Radio dataTestId={genAmtId(rowTestId, 'radio', '')}>
                <Radio.Input
                  className={
                    columnType === 'radio-check'
                      ? className.grid.GRID_CHECKED_STYLE
                      : ''
                  }
                  checked={checked}
                  readOnly={readonly}
                  onChange={(event) =>
                    onCheck?.(event.target.checked, row.original, true)
                  }
                />
              </Radio>
            </td>
          );
        }

        if (columnType === 'dnd') {
          const buttonProps = get(
            dndButtonConfig,
            'buttonProps'
          ) as ButtonProps;
          const tooltipProps = get(
            dndButtonConfig,
            'tooltipProps'
          ) as TooltipProps;
          const hasTooltip = tooltipProps?.element;
          const buttonElement = (
            <Button
              disabled={dndReadOnly}
              variant="icon-secondary"
              size="sm"
              {...buttonProps}
              dataTestId={genAmtId(rowTestId, 'drag-btn', '')}
            >
              <Icon name="drag" />
            </Button>
          );

          return (
            <td
              key={cellKey}
              {...mergedCellProps}
              className={classnames(
                mergedCellProps.className,
                !dndReadOnly && 'draggable'
              )}
              ref={dragRef}
            >
              {hasTooltip ? (
                <Tooltip {...tooltipProps}>{buttonElement}</Tooltip>
              ) : (
                buttonElement
              )}
            </td>
          );
        }

        return (
          <td key={cellKey} data-testid={rowTestId} {...mergedCellProps}>
            {cell.render('Cell')}
          </td>
        );
      })}
    </tr>
  );
};

export default Row;
