import React, {
  UIEvent,
  useRef,
  useImperativeHandle,
  RefForwardingComponent,
  forwardRef,
  useContext,
  Fragment,
  useMemo
} from 'react';
import { Row as ReactTableRow } from 'react-table';

// Utils
import { className as classConst, genAmtId } from '../../utils';
import { get, isFunction } from '../../lodash';
import classnames from 'classnames';

// Contexts
import { GridContext } from './GridContext';

// Types
import {
  CheckBoxTooltipProps,
  DndButtonConfig,
  GridBodyProps,
  ToggleButtonConfig
} from './types';

// Components
import Row from './Row';
import RowDnD from './RowDnD';

const GridBody: RefForwardingComponent<
  HTMLTableSectionElement,
  GridBodyProps
> = ({ className, dataTestId }, ref) => {
  const bodyRef = useRef<HTMLTableSectionElement | null>(null);

  const {
    state: {
      rowKey,

      totalWidth,
      scrollable,
      loadElement,
      dataItemKey,
      checkedList,
      readOnlyCheckbox,
      checkboxTooltipPropsList,
      toggleButtonConfigList,
      onCheckRowBody,
      onLoadMore,
      onCheckRadioRowBody,
      gridInstance: { rows, prepareRow, getTableBodyProps },
      subRow,
      expandedItemKey = 'id',
      expandedList,
      onExpand,

      expandedText,
      collapsedText,

      hasHorizontalScroll,
      maxBodyHeight,

      dndId,
      dnd,
      dndReadOnly,
      dndButtonConfigList,
      onRowMove
    }
  } = useContext(GridContext);

  const checkboxTooltipPropsListObj = useMemo(() => {
    const listObj = checkboxTooltipPropsList?.reduce((prev, current) => {
      return {
        ...prev,
        [current.id]: current
      };
    }, {});

    return listObj;
  }, [checkboxTooltipPropsList]);

  const toggleButtonConfigListObj = useMemo(() => {
    const listObj = toggleButtonConfigList?.reduce((prev, current) => {
      return {
        ...prev,
        [current.id]: current
      };
    }, {});

    return listObj;
  }, [toggleButtonConfigList]);

  const dndButtonConfigListObj = useMemo(() => {
    const listObj = dndButtonConfigList?.reduce((prev, current) => {
      return {
        ...prev,
        [current.id]: current
      };
    }, {});

    return listObj;
  }, [dndButtonConfigList]);

  // Set scroll table header when scroll table body is changing
  const handleScroll = (event: UIEvent<HTMLTableSectionElement>) => {
    const grid = event.currentTarget.closest(`.${classConst.grid.GRID}`);
    const header = grid?.getElementsByClassName(classConst.grid.GRID_HEAD)[0];

    // if (!header) return;

    header!.scrollLeft = event.currentTarget.scrollLeft;

    // handle load more
    const { scrollTop, clientHeight, scrollHeight } = event.currentTarget;

    // prevent load more when grid unscrollable
    if (!scrollable) return;

    if (scrollTop + clientHeight >= scrollHeight && onLoadMore) {
      onLoadMore();
    }
  };

  // handle checkbox radio change
  const handleOnCheck = (
    isChecked: boolean,
    original: Record<string, any>,
    isRadio = false
  ) => {
    if (isRadio) return onCheckRadioRowBody?.(original);

    onCheckRowBody?.(isChecked, original);
  };

  // handle expand columns
  const handleOnExpand = (isExpand: boolean, original: Record<string, any>) => {
    isFunction(onExpand) && onExpand(isExpand, original);
  };

  useImperativeHandle(ref, () => bodyRef.current!);

  const renderRow = (row: ReactTableRow, index: number) => {
    prepareRow(row);
    const { original } = row;

    // checkbox
    const dataKey = get(original, dataItemKey!);
    const isReadOnly = Boolean(readOnlyCheckbox?.includes(dataKey));
    const isChecked = isReadOnly ? false : checkedList?.includes(dataKey);
    const checkboxTooltipProps = get(
      checkboxTooltipPropsListObj,
      dataKey
    )! as CheckBoxTooltipProps;
    const toggleButtonConfig = get(
      toggleButtonConfigListObj,
      dataKey
    )! as ToggleButtonConfig;
    const dndButtonConfig = get(
      dndButtonConfigListObj,
      dataKey
    )! as DndButtonConfig;

    // expended row
    const isExpanded = expandedList?.includes(get(original, expandedItemKey));

    const { key: rowInnerKey, ...rowProps } = row.getRowProps({
      className: classnames(isChecked && classConst.grid.SELECTED)
    });

    const RowComponent = dnd ? RowDnD : Row;

    return (
      <Fragment key={get(original, rowKey!) || rowInnerKey}>
        <RowComponent
          row={row}
          // checkbox, radio readonly or not
          readonly={isReadOnly}
          // checkbox
          checked={isChecked}
          // checkbox tooltip's configurator
          checkboxTooltipProps={checkboxTooltipProps}
          // checkbox, radio callback
          onCheck={handleOnCheck}
          // drag and drop readonly or not
          dndReadOnly={dndReadOnly}
          // row expanded or not
          expanded={isExpanded}
          // row expand callback
          onExpand={handleOnExpand}
          // row expanded/collapsed tooltip text
          expandedText={expandedText}
          collapsedText={collapsedText}
          // grid horizontal scroll or not
          horizontalScroll={hasHorizontalScroll}
          // drag and drop id, used to distingish from other grids
          dndId={dndId}
          // callbacked called on row moved
          onRowMove={onRowMove}
          // config dnd button
          dndButtonConfig={dndButtonConfig}
          // config toggle button
          toggleButtonConfig={toggleButtonConfig}
          // data test id
          dataTestId={genAmtId(dataTestId, `row-${index}`, 'GridBody')}
        />
        {/* no support for subRow case yet (DnD) */}
        {isExpanded && (
          <>
            {/*
              When expand column, css background for rows (odd and even rows)
              will be not correct. adding or removing this row to correct this case.
            */}
            <tr />
            <tr {...rowProps}>
              <td
                className={classConst.grid.GRID_EXPAND_ROW}
                width={totalWidth}
              >
                {subRow(row)}
              </td>
            </tr>
          </>
        )}
      </Fragment>
    );
  };

  return (
    <tbody
      ref={bodyRef}
      {...getTableBodyProps([
        { className: classnames(classConst.grid.GRID_BODY, className) }
      ])}
      onScroll={handleScroll}
      style={{ height: maxBodyHeight }}
    >
      {rows.map(renderRow)}
      {scrollable && loadElement}
    </tbody>
  );
};

export default forwardRef<HTMLTableSectionElement, GridBodyProps>(GridBody);
