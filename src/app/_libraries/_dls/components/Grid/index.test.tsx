import React, { ReactElement } from 'react';
import {
  fireEvent,
  getByRole,
  getByText,
  queryAllByRole,
  queryByText,
  render,
  RenderResult,
  screen
} from '@testing-library/react';
import '@testing-library/jest-dom';

// components
import Grid from '.';
import { RenderDropdownActions } from './mocks/TransactionList';
import { SubGridNoHeader } from './mocks/PaymentHistoryExpandDetail';
import Filter from './mocks/Filter';

// data
import { paymentHistory, payments, transactionList } from './mocks/data';
import { data, dataTotalFooter } from './mocks/TotalFooter';

// types
import { GridProps } from './types';

// utils
import { queryByClass, queryAllByClass } from '../../test-utils/queryHelpers';
import userEvent from '@testing-library/user-event';
import MockGridWrapper from './MockGridWrapper';
import * as isWindowPlatform from '../../utils/isWindowPlatform';
import { act } from 'react-dom/test-utils';
import * as getElementAttribute from '../../utils/getElementAttribute';

// mocks
import '../../test-utils/mocks/mockCanvas';

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('../../test-utils/mocks/MockResizeObserver')
);

let wrapper: RenderResult;
let baseElement: HTMLElement;

const renderComponent = (props: GridProps) => {
  wrapper = render(<MockGridWrapper {...props} />);
  baseElement = wrapper.baseElement as HTMLElement;
};

const renderElement = (element: ReactElement) => {
  wrapper = render(element);
  baseElement = wrapper.baseElement as HTMLElement;
};

const getNode = {
  get table() {
    return screen.getByRole('table');
  },
  get header() {
    return queryByClass(wrapper.baseElement as HTMLElement, /dls-grid-head/)!;
  },
  get headerRows() {
    const headElement = queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-grid-head/
    )!;
    return queryAllByRole(headElement, 'row')!;
  },
  get body() {
    return queryByClass(wrapper.baseElement as HTMLElement, /dls-grid-body/)!;
  },
  get bodyRows() {
    const bodyElement = queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-grid-body/
    )!;
    return queryAllByRole(bodyElement, 'row')!;
  },
  get bodyCells() {
    return queryAllByRole(wrapper.baseElement as HTMLElement, 'cell')!;
  },
  getBodyCellFrom: (element: HTMLElement) => queryAllByRole(element, 'cell')!,
  get footer() {
    return queryByClass(wrapper.baseElement as HTMLElement, /dls-grid-footer/)!;
  },
  get triggerDropdownButton() {
    return queryByClass(
      wrapper.baseElement as HTMLElement,
      'dls-dropdown-button'
    )!;
  }
};

const expectByText = (id: string | number, element?: HTMLElement) => {
  const newId = id.toString().replace('$', `\\$`);

  return {
    toBeInTheDocument: () =>
      expect(
        queryByText(element || baseElement, new RegExp(newId))
      ).toBeInTheDocument(),
    toBeNull: () =>
      expect(queryByText(element || baseElement, new RegExp(newId))).toBeNull()
  };
};

Object.defineProperties(window.HTMLTableSectionElement.prototype, {
  offsetHeight: { get: () => 100 }
});

beforeEach(() => {
  jest.useFakeTimers();
});

const mockPrototype = ({ offsetHeight = 0 }) => {
  return Object.defineProperty(HTMLElement.prototype, 'clientHeight', {
    writable: true,
    value: offsetHeight
  });
};

describe('Render', () => {
  it('should render with footer', () => {
    const spy = jest
      .spyOn(getElementAttribute, 'getScrollBarWidth')
      .mockImplementation(() => ({ vertical: 0, horizontal: 100 }));

    // TotalFooter.tsx
    const columns = [
      {
        id: 'rowHeader',
        Header: 'MERCHANT',
        Footer: 'Total',
        accessor: 'rowHeader',
        width: 100,
        cellHeaderProps: { className: 'row-header' },
        cellBodyProps: { className: 'row-header' }
      },
      {
        id: 'amount',
        Header: 'AMOUNT',
        accessor: 'amount',
        width: 220,
        cellProps: { className: 'text-right' }
      },
      {
        id: 'number',
        Header: 'NUMBER',
        accessor: 'number'
      }
    ];

    renderComponent({
      columns,
      data,
      dataFooter: dataTotalFooter,
      classes: { body: 'grid-body' }
    });

    expect(getNode.table).toBeInTheDocument();

    // expect header
    expect(getNode.header).toBeInTheDocument();
    expectByText(columns[0].Header).toBeInTheDocument();
    expectByText(columns[1].Header).toBeInTheDocument();
    expectByText(columns[2].Header).toBeInTheDocument();

    expect(getNode.body).toBeInTheDocument();
    expectByText(data[0].rowHeader).toBeInTheDocument();
    expectByText(data[1].rowHeader).toBeInTheDocument();
    expectByText(data[0].amount).toBeInTheDocument();
    expectByText(data[1].amount).toBeInTheDocument();
    expectByText(data[0].number).toBeInTheDocument();
    expectByText(data[1].number).toBeInTheDocument();

    // expect footer
    expect(getNode.footer).toBeInTheDocument();
    expectByText(dataTotalFooter.rowHeader).toBeInTheDocument();
    expectByText(dataTotalFooter.amount).toBeInTheDocument();
    expectByText(dataTotalFooter.number).toBeInTheDocument();

    spy.mockReset();
    spy.mockRestore();
  });

  it('should render with date header', () => {
    const columns = [
      { id: 'column_1', accessor: 'column_1', isFixedLeft: true, width: 190 },
      { id: 'column_2', accessor: 'column_2', width: 170 }
    ];

    const dataHeader = { column_1: 'Statement Date', column_2: '27/07/2020' };

    const data = [
      { column_1: 'New Balance', column_2: '$3,550.82' },
      { column_1: 'Min Pay Due', column_2: '$1,031.85' }
    ];

    renderComponent({
      columns,
      data,
      dataFooter: dataTotalFooter,
      classes: { body: 'grid-body' },
      dataHeader
    });

    expect(getNode.table).toBeInTheDocument();
    const row = getNode.headerRows[0];

    expect(getByText(row, dataHeader.column_1)).toBeInTheDocument();
    expect(getByText(row, dataHeader.column_2)).toBeInTheDocument();
  });

  it('should render with Component cell', () => {
    // TransactionList.tsx
    const columns = [
      {
        id: 'transDate',
        Header: 'TRANS DATE',
        accessor: 'transDate',
        isSort: true,
        width: 125
      },
      {
        id: 'actions',
        Header: 'ACTIONS',
        accessor: RenderDropdownActions,
        width: 92,
        cellProps: { className: 'table-cell__action-more' }
      }
    ];

    const data = transactionList.slice(0, 1);

    renderElement(
      <Grid
        ref={{ current: null }}
        columns={columns}
        data={data}
        sortBy={[]}
        onSortChange={jest.fn()}
        onLoadMore={jest.fn()}
        loadElement={
          <tr className="row-loading">
            <td>
              <span className="loading" />
              <span>Loading more transactions...</span>
            </td>
          </tr>
        }
      />
    );

    expect(getNode.table).toBeInTheDocument();

    // expect header
    expect(getNode.header).toBeInTheDocument();
    expectByText(columns[0].Header).toBeInTheDocument();
    expectByText(columns[1].Header).toBeInTheDocument();

    expect(getNode.body).toBeInTheDocument();
    expectByText(data[0].transDate).toBeInTheDocument();
    expect(screen.getByRole('button')).toBeInTheDocument();
  });

  it('should render with fixed columns', () => {
    // TransactionList.tsx
    const columns = [
      {
        id: 'transDate',
        Header: 'TRANS DATE',
        accessor: 'transDate',
        isFixedLeft: true
      },
      {
        id: 'postingDate',
        Header: 'POSTING DATE',
        accessor: 'postingDate',
        width: 125
      },
      {
        id: 'actions',
        Header: 'ACTIONS',
        accessor: RenderDropdownActions,
        isFixedRight: true,
        cellProps: { className: 'table-cell__action-more' }
      }
    ];

    const data = transactionList.slice(0, 1);

    renderElement(
      <Grid ref={{ current: null }} columns={columns} data={data} />
    );

    const row_0 = getNode.bodyRows[0];
    expect(queryByClass(row_0, /fixed fixed-last-left/)).toBeInTheDocument();
    expect(queryByClass(row_0, /fixed fixed-first-right/)).toBeInTheDocument();
  });

  it('should render with load more', () => {
    mockPrototype({ offsetHeight: 100 });
    // TransactionList.tsx
    const columns = [
      {
        id: 'transDate',
        Header: 'TRANS DATE',
        accessor: 'transDate',
        width: 125
      },
      {
        id: 'actions',
        Header: 'ACTIONS',
        accessor: RenderDropdownActions,
        width: 92,
        cellProps: { className: 'table-cell__action-more' }
      }
    ];

    const data = transactionList.slice(0, 1);

    renderElement(
      <Grid
        maxHeight={50}
        ref={{ current: null }}
        columns={columns}
        data={data}
        loadElement={<div>load element</div>}
      />
    );
    // expect(screen.getByText('load element')).toBeInTheDocument();
  });

  it('should render in Window Platform', () => {
    const spy = jest
      .spyOn(isWindowPlatform, 'default')
      .mockImplementation(() => true);

    // TransactionList.tsx
    const columns = [
      {
        id: 'transDate',
        Header: 'TRANS DATE',
        accessor: 'transDate',
        width: 125
      },
      {
        id: 'actions',
        Header: 'ACTIONS',
        accessor: RenderDropdownActions,
        width: 92,
        cellProps: { className: 'table-cell__action-more' }
      }
    ];

    const data = transactionList.slice(0, 1);

    renderElement(
      <Grid ref={{ current: null }} columns={columns} data={data} />
    );

    expect(queryByClass(baseElement, /grid-for-window/)).toBeInTheDocument();

    spy.mockReset();
    spy.mockRestore();
  });

  describe('should render with sub headers', () => {
    // TransactionList.tsx
    const columns = [
      {
        id: 'transDate',
        Header: 'TRANS DATE',
        accessor: 'transDate',
        isFixedLeft: true,
        width: 125
      },
      {
        id: 'mainColumn',
        Header: 'Main Column',
        accessor: 'mainColumn',
        columns: [
          {
            id: 'postingDate',
            Header: 'POSTING DATE',
            accessor: 'postingDate',
            width: 125
          },
          {
            id: 'actions',
            Header: 'ACTIONS',
            accessor: RenderDropdownActions,
            cellProps: { className: 'table-cell__action-more' }
          }
        ]
      }
    ];

    const data = transactionList.slice(0, 1);

    const renderSubHeaderGrid = (staticHeaderId?: string) => {
      renderElement(
        <Grid
          ref={{ current: null }}
          columns={columns}
          data={data}
          staticHeaderId={staticHeaderId}
        />
      );
    };

    it('with staticHeaderId', () => {
      // render with staticHeaderId
      renderSubHeaderGrid();

      const row_0 = getNode.headerRows[0];
      const row_1 = getNode.headerRows[1];

      expect(queryByClass(baseElement, 'static-header-cell')).toBeNull();
      expect(getNode.headerRows.length).toEqual(2);
      expect(getByText(row_0, new RegExp(columns[0].Header))).toHaveStyle({
        color: 'transparent'
      });
      expect(
        getByText(row_0, new RegExp(columns[1].Header))
      ).toBeInTheDocument();
      expect(
        getByText(row_1, new RegExp(columns[0].Header))
      ).toBeInTheDocument();
      expect(
        getByText(row_1, new RegExp(columns[1].columns![0].Header))
      ).toBeInTheDocument();
      expect(
        getByText(row_1, new RegExp(columns[1].columns![1].Header))
      ).toBeInTheDocument();
    });

    it('without staticHeaderId', () => {
      // render without staticHeaderId
      renderSubHeaderGrid('mainColumn');

      expect(
        queryByClass(baseElement, 'static-header-cell')
      ).toBeInTheDocument();
    });
  });

  it('should render with checkbox', () => {
    const columns = [
      {
        id: 'scheduledDate',
        Header: 'scheduled Date',
        accessor: 'scheduledDate',
        isSort: true,
        width: 142
      },
      {
        id: 'confirmationNumber',
        Header: 'confirmation Number',
        accessor: 'confirmationNumber',
        width: 184,
        autoWidth: true
      }
    ];

    const data = [
      {
        id: 'data-1',
        scheduledDate: '09/20/2020',
        status: 0,
        amount: '$100.00',
        accountNumber: '****1234',
        confirmationNumber: 'HDKSL-348KDS'
      },
      {
        id: 'data-2',
        scheduledDate: '09/19/2020',
        status: 2,
        amount: '$60.00',
        accountNumber: '****1534',
        confirmationNumber: 'HDKSL-348KDS'
      }
    ];

    render(
      <MockGridWrapper
        columns={columns}
        data={data}
        // checkbox
        variant={{
          id: 'checkbox_id',
          type: 'checkbox',
          isFixedLeft: true
        }}
        dataItemKey="id"
        checkboxTooltipPropsList={[
          {
            id: data[0].id,
            element: 'Tooltip',
            displayForCallback: () => true
          }
        ]}
        readOnlyCheckbox={[data[0].id]}
        togglable
      />
    );

    expect(queryAllByClass(getNode.body, /dls-checkbox-input/).length).toEqual(
      data.length
    );
    expect(
      queryAllByClass(getNode.header, /dls-checkbox-input/).length
    ).toEqual(1);
  });

  it('should render with checkbox 2', () => {
    const columns = [
      {
        id: 'scheduledDate',
        Header: 'scheduled Date',
        accessor: 'scheduledDate',
        isSort: true,
        width: 142
      },
      {
        id: 'confirmationNumber',
        Header: 'confirmation Number',
        accessor: 'confirmationNumber',
        width: 184,
        autoWidth: true
      }
    ];

    const data = [
      {
        id: 'data-1',
        scheduledDate: '09/20/2020',
        status: 0,
        amount: '$100.00',
        accountNumber: '****1234',
        confirmationNumber: 'HDKSL-348KDS'
      },
      {
        id: 'data-2',
        scheduledDate: '09/19/2020',
        status: 2,
        amount: '$60.00',
        accountNumber: '****1534',
        confirmationNumber: 'HDKSL-348KDS'
      }
    ];

    render(
      <MockGridWrapper
        columns={columns}
        data={data}
        // checkbox
        variant={{
          id: 'checkbox_id',
          type: 'checkbox',
          isFixedLeft: true
        }}
        dataItemKey="id"
        checkboxTooltipPropsList={[
          {
            id: data[0].id,
            element: 'Tooltip'
          }
        ]}
        readOnlyCheckbox={[data[0].id]}
        togglable
      />
    );

    expect(queryAllByClass(getNode.body, /dls-checkbox-input/).length).toEqual(
      data.length
    );
    expect(
      queryAllByClass(getNode.header, /dls-checkbox-input/).length
    ).toEqual(1);
  });

  it('should render with checkbox 3', () => {
    const columns = [
      {
        id: 'scheduledDate',
        Header: 'scheduled Date',
        accessor: 'scheduledDate',
        isSort: true,
        width: 142
      },
      {
        id: 'confirmationNumber',
        Header: 'confirmation Number',
        accessor: 'confirmationNumber',
        width: 184,
        autoWidth: true
      }
    ];

    const data = [
      {
        id: 'data-1',
        scheduledDate: '09/20/2020',
        status: 0,
        amount: '$100.00',
        accountNumber: '****1234',
        confirmationNumber: 'HDKSL-348KDS'
      },
      {
        id: 'data-2',
        scheduledDate: '09/19/2020',
        status: 2,
        amount: '$60.00',
        accountNumber: '****1534',
        confirmationNumber: 'HDKSL-348KDS'
      }
    ];

    render(
      <MockGridWrapper
        columns={columns}
        data={data}
        // checkbox
        variant={{
          id: 'checkbox_id',
          type: 'checkbox',
          isFixedLeft: true
        }}
        dataItemKey="id"
        checkboxTooltipPropsList={[
          {
            id: data[0].id,
            element: 'Tooltip',
            displayFor: 'read-only'
          }
        ]}
        readOnlyCheckbox={[data[0].id]}
        togglable
      />
    );

    expect(queryAllByClass(getNode.body, /dls-checkbox-input/).length).toEqual(
      data.length
    );
    expect(
      queryAllByClass(getNode.header, /dls-checkbox-input/).length
    ).toEqual(1);
  });

  it('should render with filter', () => {
    const columns = [
      {
        id: 'merchantId',
        Header: 'MERCHANT ID',
        accessor: 'merchantId',
        width: 150,
        filter: <Filter filterBy={[]} columnId="merchantId" />
      },
      {
        id: 'merchantName',
        Header: 'MERCHANT NAME',
        accessor: 'merchantName',
        width: 150
      }
    ];

    const data = [
      {
        merchantId: '4250562991',
        merchantName: 'Esther Griffin'
      },
      {
        merchantId: '8376244831',
        merchantName: 'Ricky Burke'
      }
    ];

    renderComponent({ columns, data });

    expect(queryByClass(baseElement, /icon-filter/)).toBeInTheDocument();
  });

  it('should render with radio checkbox', () => {
    const columns = [
      {
        id: 'scheduledDate',
        Header: 'scheduled Date',
        accessor: 'scheduledDate',
        isSort: true,
        width: 142
      },
      {
        id: 'confirmationNumber',
        Header: 'confirmation Number',
        accessor: 'confirmationNumber',
        width: 184,
        autoWidth: true
      }
    ];

    render(
      <MockGridWrapper
        columns={columns}
        data={data}
        // checkbox
        variant={{
          id: 'checkbox_id',
          type: 'radio-check',
          isFixedLeft: true
        }}
        dataItemKey={'id.name'} // define [key value] in data
      />
    );

    expect(queryAllByClass(baseElement, /dls-radio-input/).length).toEqual(
      data.length
    );
  });

  it('should render with radio', () => {
    const columns = [
      {
        id: 'scheduledDate',
        Header: 'scheduled Date',
        accessor: 'scheduledDate',
        isSort: true,
        width: 142
      },
      {
        id: 'confirmationNumber',
        Header: 'confirmation Number',
        accessor: 'confirmationNumber',
        width: 184,
        autoWidth: true
      }
    ];

    render(
      <MockGridWrapper
        columns={columns}
        data={data}
        // checkbox
        variant={{
          id: 'checkbox_id',
          type: 'radio',
          isFixedLeft: true
        }}
        dataItemKey={'id.name'} // define [key value] in data
      />
    );

    expect(queryAllByClass(baseElement, /dls-radio-input/).length).toEqual(
      data.length
    );
  });

  it('should render with expand 1', () => {
    jest.useFakeTimers();

    const columns = [
      {
        id: 'scheduledDate',
        Header: 'scheduled Date',
        accessor: 'scheduledDate',
        isSort: true,
        width: 142,
        cellProps: { className: 'text-right' }
      },
      {
        id: 'confirmationNumber',
        Header: 'confirmation Number',
        accessor: 'confirmationNumber',
        width: 184
      }
    ];

    const data = paymentHistory.slice(0, 1);

    render(
      <MockGridWrapper
        columns={columns}
        data={data}
        // checkbox
        subRow={(row: any) => <SubGridNoHeader row={row} />}
        expand={{ id: 'expand' }}
        expandedItemKey={'cycleDate'}
        expandedList={['01/02/2021']}
      />
    );

    jest.runAllTimers();

    expect(queryByClass(baseElement, /sub-row/)).toBeInTheDocument();
  });

  it('should render with expand 2', () => {
    jest.useFakeTimers();

    const columns = [
      {
        id: 'scheduledDate',
        Header: 'scheduled Date',
        accessor: 'scheduledDate',
        isSort: true,
        width: 142,
        cellProps: { className: 'text-right' }
      },
      {
        id: 'confirmationNumber',
        Header: 'confirmation Number',
        accessor: 'confirmationNumber',
        width: 184
      }
    ];

    const data = paymentHistory.slice(0, 1);

    render(
      <MockGridWrapper
        columns={columns}
        data={data}
        // checkbox
        subRow={(row: any) => <SubGridNoHeader row={row} />}
        expand={{ id: 'expand' }}
        expandedItemKey={'cycleDate'}
        expandedList={['01/02/2021']}
        expandedText={null as any}
        collapsedText={null as any}
        dataItemKey="cycleDate"
        toggleButtonConfigList={[
          {
            id: data[0].cycleDate,
            expandedTooltipProps: {},
            collapsedTooltipProps: {}
          }
        ]}
      />
    );

    jest.runAllTimers();

    expect(queryByClass(baseElement, /sub-row/)).toBeInTheDocument();
  });

  it('should render with required', () => {
    const columns = [
      {
        id: 'scheduledDate',
        Header: 'scheduled Date',
        accessor: 'scheduledDate',
        isSort: true,
        width: 142,
        required: true,
        info: {
          iconProps: {},
          tooltipProps: {
            element: 'Info'
          }
        }
      },
      {
        id: 'confirmationNumber',
        Header: 'confirmation Number',
        accessor: 'confirmationNumber',
        width: 184,
        autoWidth: true
      }
    ];

    render(
      <MockGridWrapper
        columns={columns}
        data={data}
        dataItemKey={'id.name'} // define [key value] in data
      />
    );

    expect(baseElement.querySelector('.asterisk')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  describe('click sort data', () => {
    const columns = [
      {
        id: 'transDate',
        Header: 'TRANS DATE',
        accessor: 'transDate',
        isSort: true,
        width: 125
      },
      {
        id: 'actions',
        Header: 'ACTIONS',
        accessor: RenderDropdownActions,
        width: 92,
        isSort: true,
        cellProps: { className: 'table-cell__action-more' }
      }
    ];

    const data = transactionList.slice(0, 2);
    const onSortChange = jest.fn();

    const renderSortGrid = (isSortMulti?: boolean) => {
      renderElement(
        <MockGridWrapper
          columns={columns}
          data={data}
          onSortChange={onSortChange}
          onLoadMore={jest.fn()}
          isSortMulti={isSortMulti}
          loadElement={
            <tr className="row-loading">
              <td>
                <span className="loading" />
                <span>Loading more transactions...</span>
              </td>
            </tr>
          }
        />
      );
    };

    it('when single sort', () => {
      renderSortGrid();

      // click sort
      userEvent.click(screen.getByText(columns[0].Header));

      expect(onSortChange).toBeCalledWith({ id: 'transDate', order: 'asc' });
      onSortChange.mockClear();

      // click sort
      userEvent.click(screen.getByText(columns[0].Header));

      expect(onSortChange).toBeCalledWith({ id: 'transDate', order: 'desc' });
      onSortChange.mockClear();

      // click sort in another cell
      userEvent.click(screen.getByText(columns[1].Header));

      expect(onSortChange).toBeCalledWith({ id: 'actions', order: 'asc' });
      onSortChange.mockClear();
    });

    it('when multible sort', () => {
      renderSortGrid(true);

      // click sort
      userEvent.click(screen.getByText(columns[0].Header));

      expect(onSortChange).toBeCalledWith({ id: 'transDate', order: 'asc' });
      onSortChange.mockClear();

      // click sort
      userEvent.click(screen.getByText(columns[0].Header));

      expect(onSortChange).toBeCalledWith({ id: 'transDate', order: 'desc' });
      onSortChange.mockClear();

      // click sort in another cell
      userEvent.click(screen.getByText(columns[1].Header));

      expect(onSortChange).toBeCalledWith({ id: 'actions', order: 'asc' });
      onSortChange.mockClear();
    });
  });

  describe('click expand and collapse sub row', () => {
    const columns = [
      {
        id: 'scheduledDate',
        Header: 'scheduled Date',
        accessor: 'scheduledDate',
        isSort: true,
        width: 142,
        cellProps: { className: 'text-right' }
      },
      {
        id: 'confirmationNumber',
        Header: 'confirmation Number',
        accessor: 'confirmationNumber',
        width: 184
      }
    ];

    const data = paymentHistory.slice(0, 1);

    const subRow = (row: any) => (
      <SubGridNoHeader isSetTimeOut={false} row={row} />
    );

    const renderExpandGrid = (onExpand: jest.Mock, expandedList?: string[]) => {
      render(
        <MockGridWrapper
          columns={columns}
          data={data}
          subRow={subRow}
          expand={{ id: 'expand' }}
          expandedItemKey={'cycleDate'}
          expandedList={expandedList}
          onExpand={onExpand}
          expandedText={null as any}
          collapsedText="collapsedText"
        />
      );
    };

    it('expand', () => {
      const onExpand = jest.fn();

      renderExpandGrid(onExpand);

      // expand
      userEvent.click(getByRole(getNode.body, 'button'));

      expect(queryByClass(baseElement, /sub-row/)).toBeInTheDocument();
      expect(onExpand).toBeCalledWith(['01/02/2021']);
    });

    it('collapse', () => {
      const onExpand = jest.fn();

      renderExpandGrid(onExpand, ['01/02/2021']);

      expect(queryByClass(baseElement, /sub-row/)).toBeInTheDocument();

      // collapse
      userEvent.click(getByRole(getNode.body, 'button'));

      expect(queryByClass(baseElement, /sub-row/)).toBeNull();
      expect(onExpand).toBeCalledWith([]);
    });
  });

  describe('click checkbox', () => {
    const columns = [
      {
        id: 'scheduledDate',
        Header: 'scheduled Date',
        accessor: 'scheduledDate',
        isSort: true,
        width: 142
      },
      {
        id: 'confirmationNumber',
        Header: 'confirmation Number',
        accessor: 'confirmationNumber',
        width: 184,
        autoWidth: true
      }
    ];

    const data = payments.slice(0, 2);

    const renderCheckboxGrid = (
      onCheck: jest.Mock,
      readOnlyCheckbox?: string[],
      checkAllControlled?: () => 'checked' | 'indeterminate'
    ) => {
      render(
        <MockGridWrapper
          columns={columns}
          data={data}
          variant={{
            id: 'checkbox_id',
            type: 'checkbox',
            isFixedLeft: true
          }}
          dataItemKey={'id.name'}
          readOnlyCheckbox={readOnlyCheckbox}
          onCheck={onCheck}
          checkAllControlled={checkAllControlled}
        />
      );
    };

    it('when all checkbox is enabled', () => {
      const onCheck = jest.fn();

      renderCheckboxGrid(onCheck, ['data-1']);

      const checkAllInput = queryByClass(
        getNode.header,
        /dls-checkbox-input/
      )! as HTMLInputElement;
      const checkInputs = queryAllByClass(
        getNode.body,
        /dls-checkbox-input/
      ) as HTMLInputElement[];

      expect(checkAllInput.checked).toBeFalsy();
      expect(checkInputs[0].checked).toBeFalsy();
      expect(checkInputs[1].checked).toBeFalsy();

      // check all
      act(() => {
        userEvent.click(checkAllInput);
      });

      expect(onCheck).toBeCalled();
      expect(checkAllInput.checked).toBeTruthy();
      expect(checkInputs[0].checked).toBeFalsy();
      expect(checkInputs[1].checked).toBeTruthy();
    });

    it('when data empty and checkAll', () => {
      const onCheck = jest.fn();

      render(
        <MockGridWrapper
          columns={columns}
          data={[]}
          variant={{
            id: 'checkbox_id',
            type: 'checkbox',
            isFixedLeft: true,
            tooltipProps: {
              element: 'Tooltip',
              displayForCallback: () => true
            }
          }}
          dataItemKey={'id.name'}
          onCheck={onCheck}
        />
      );

      const checkAllInput = queryByClass(
        getNode.header,
        /dls-checkbox-input/
      )! as HTMLInputElement;

      // check all
      act(() => {
        userEvent.click(checkAllInput);
      });

      expect(onCheck).toBeCalledWith([]);
    });

    describe('when all checkbox is enabled', () => {
      it('click check All', () => {
        const onCheck = jest.fn();

        renderCheckboxGrid(onCheck);

        const checkAllInput = queryByClass(
          getNode.header,
          /dls-checkbox-input/
        )! as HTMLInputElement;
        const checkInputs = queryAllByClass(
          getNode.body,
          /dls-checkbox-input/
        ) as HTMLInputElement[];

        expect(checkAllInput.checked).toBeFalsy();
        expect(checkInputs[0].checked).toBeFalsy();
        expect(checkInputs[1].checked).toBeFalsy();

        // check all
        act(() => {
          userEvent.click(checkAllInput);
        });

        expect(onCheck).toBeCalledWith(['data-1', 'data-2']);
        expect(checkAllInput.checked).toBeTruthy();
        expect(checkInputs[0].checked).toBeTruthy();
        expect(checkInputs[1].checked).toBeTruthy();

        // un-check all
        act(() => {
          userEvent.click(checkAllInput);
        });

        expect(onCheck).toBeCalledWith([]);
        expect(checkAllInput.checked).toBeFalsy();
        expect(checkInputs[0].checked).toBeFalsy();
        expect(checkInputs[1].checked).toBeFalsy();
      });

      it('click checkbox', () => {
        const onCheck = jest.fn();

        renderCheckboxGrid(onCheck);

        const checkAllInput = queryByClass(
          getNode.header,
          /dls-checkbox-input/
        )! as HTMLInputElement;
        const checkInputs = queryAllByClass(
          getNode.body,
          /dls-checkbox-input/
        ) as HTMLInputElement[];

        expect(checkAllInput.checked).toBeFalsy();
        expect(checkInputs[0].checked).toBeFalsy();
        expect(checkInputs[1].checked).toBeFalsy();

        // check
        act(() => {
          userEvent.click(checkInputs[0]);
        });
        expect(onCheck).toBeCalledWith(['data-1']);

        act(() => {
          userEvent.click(checkInputs[1]);
        });
        expect(onCheck).toBeCalledWith(['data-1', 'data-2']);

        expect(checkInputs[0].checked).toBeTruthy();
        expect(checkInputs[1].checked).toBeTruthy();
        expect(checkAllInput.checked).toBeTruthy();

        // un-check
        act(() => {
          userEvent.click(checkInputs[0]);
        });
        expect(onCheck).toBeCalledWith(['data-2']);

        expect(checkInputs[0].checked).toBeFalsy();
        expect(checkInputs[1].checked).toBeTruthy();
        expect(checkAllInput.checked).toBeFalsy();
      });

      it('click checkbox with checkAllControlled checked', () => {
        const onCheck = jest.fn();

        renderCheckboxGrid(onCheck, [], () => 'checked');

        const checkAllInput = queryByClass(
          getNode.header,
          /dls-checkbox-input/
        )! as HTMLInputElement;

        expect(checkAllInput.checked).toBeTruthy();
      });

      it('click checkbox with checkAllControlled indeterminate', () => {
        const onCheck = jest.fn();

        renderCheckboxGrid(onCheck, [], () => 'indeterminate');

        const checkAllInput = queryByClass(
          getNode.header,
          /dls-checkbox-input/
        )! as HTMLInputElement;

        expect(checkAllInput.checked).toBeFalsy();
      });
    });
  });

  it('click checkbox radio', () => {
    const onCheck = jest.fn();

    const columns = [
      {
        id: 'scheduledDate',
        Header: 'scheduled Date',
        accessor: 'scheduledDate',
        isSort: true,
        width: 142
      },
      {
        id: 'confirmationNumber',
        Header: 'confirmation Number',
        accessor: 'confirmationNumber',
        width: 184,
        autoWidth: true
      }
    ];

    const data = payments.slice(0, 2);

    render(
      <MockGridWrapper
        columns={columns}
        data={data}
        variant={{
          id: 'checkbox_id',
          type: 'radio-check',
          isFixedLeft: true,
          tooltipProps: {
            displayFor: 'default'
          }
        }}
        dataItemKey={'id.name'}
        onCheck={onCheck}
      />
    );

    const checkInputs = screen.getAllByRole('radio')! as HTMLInputElement[];

    // check radio 1
    act(() => {
      userEvent.click(checkInputs[0]);
    });

    expect(onCheck).toBeCalledWith(['data-1']);
    expect(checkInputs[0].checked).toBeTruthy();
    expect(checkInputs[1].checked).toBeFalsy();

    // check radio 2
    act(() => {
      userEvent.click(checkInputs[1]);
    });

    expect(onCheck).toBeCalledWith(['data-2']);
    expect(checkInputs[0].checked).toBeFalsy();
    expect(checkInputs[1].checked).toBeTruthy();
  });

  it('toggle show/hide filter', () => {
    const columns = [
      {
        id: 'merchantId',
        Header: 'MERCHANT ID',
        accessor: 'merchantId',
        width: 150,
        filter: <Filter filterBy={[]} columnId="merchantId" />
      },
      {
        id: 'merchantName',
        Header: 'MERCHANT NAME',
        accessor: 'merchantName',
        width: 150
      }
    ];

    const data = [
      {
        merchantId: '4250562991',
        merchantName: 'Esther Griffin'
      },
      {
        merchantId: '8376244831',
        merchantName: 'Ricky Burke'
      }
    ];

    renderComponent({ columns, data });

    // show filter popup
    act(() => {
      userEvent.click(queryByClass(baseElement, /icon-filter/)!);
      jest.runAllTimers();
    });

    expect(queryByClass(baseElement, 'dls-grid-filter')!).toBeInTheDocument();

    // show filter popup
    act(() => {
      userEvent.click(queryByClass(baseElement, /icon-filter/)!);
      jest.runAllTimers();
    });

    expect(queryByClass(baseElement, 'dls-grid-filter')!).toBeNull();
  });

  describe('scroll body trigger loadmore', () => {
    it('when grid height is greater than allowed height', () => {
      const mockLoadMore = jest.fn();

      // TransactionList.tsx
      const columns = [
        {
          id: 'transDate',
          Header: 'TRANS DATE',
          accessor: 'transDate',
          width: 125
        },
        {
          id: 'actions',
          Header: 'ACTIONS',
          accessor: RenderDropdownActions,
          width: 92,
          cellProps: { className: 'table-cell__action-more' }
        }
      ];

      const data = transactionList.slice(0, 1);

      renderElement(
        <Grid
          maxHeight={150}
          ref={{ current: null }}
          columns={columns}
          data={data}
          loadElement={<div>load element</div>}
          onLoadMore={mockLoadMore}
        />
      );

      fireEvent.scroll(getNode.body, {
        target: {},
        currentTarget: { scrollTop: 1000 }
      });

      // expect(mockLoadMore).toBeCalled();
      // expect(screen.queryByText(/load element/)).toBeNull();
    });

    describe('when grid height is less than allowed height', () => {
      it('controlled', () => {
        const mockLoadMore = jest.fn();

        // TransactionList.tsx
        const columns = [
          {
            id: 'transDate',
            Header: 'TRANS DATE',
            accessor: 'transDate',
            width: 125
          },
          {
            id: 'actions',
            Header: 'ACTIONS',
            accessor: RenderDropdownActions,
            width: 92,
            cellProps: { className: 'table-cell__action-more' }
          }
        ];

        const data = transactionList.slice(0, 1);

        renderElement(
          <Grid
            maxHeight={50}
            ref={{ current: null }}
            columns={columns}
            data={data}
            loadElement={<div>load element</div>}
            onLoadMore={mockLoadMore}
          />
        );

        fireEvent.scroll(getNode.body, {
          target: {},
          currentTarget: { scrollTop: 1000 }
        });

        expect(mockLoadMore).toBeCalled();
        // expect(screen.getByText(/load element/)).toBeInTheDocument();
      });

      it('uncontrolled', () => {
        // TransactionList.tsx
        const columns = [
          {
            id: 'transDate',
            Header: 'TRANS DATE',
            accessor: 'transDate',
            width: 125
          },
          {
            id: 'actions',
            Header: 'ACTIONS',
            accessor: RenderDropdownActions,
            width: 92,
            cellProps: { className: 'table-cell__action-more' }
          }
        ];

        const data = transactionList.slice(0, 1);

        renderElement(
          <Grid
            maxHeight={50}
            ref={{ current: null }}
            columns={columns}
            data={data}
            loadElement={<div>load element</div>}
          />
        );

        fireEvent.scroll(getNode.body, {
          target: {},
          currentTarget: { scrollTop: 1000 }
        });

        // expect(screen.getByText(/load element/)).toBeInTheDocument();
      });
    });
  });

  describe('drag and drop', () => {
    it('should render dnd grid', () => {
      // TransactionList.tsx
      const columns = [
        {
          id: 'transDate',
          Header: 'TRANS DATE',
          accessor: 'transDate',
          width: 125
        },
        {
          id: 'amount',
          Header: 'AMOUNT',
          accessor: 'amount',
          width: 125
        },
        {
          id: 'actions',
          Header: 'ACTIONS',
          accessor: RenderDropdownActions,
          width: 92,
          cellProps: { className: 'table-cell__action-more' }
        }
      ];

      const data = transactionList.slice(0, 3);

      renderElement(
        <>
          <Grid
            columns={columns}
            data={data}
            variant={{
              id: 'something',
              type: 'dnd',
              tooltipProps: {
                displayFor: 'read-only'
              }
            }}
            dnd
          />
          <div className="other-element p-40">other element</div>
        </>
      );

      const dragElement1 = wrapper.container.querySelector(
        'tbody tr:first-child .draggable'
      )!;
      const dragElement2 = dragElement1.nextSibling!;

      fireEvent.dragStart(dragElement1);
      fireEvent.drop(dragElement2);

      expect(
        wrapper.baseElement.querySelector('.dls-drag-layer')
      ).toBeInTheDocument();
    });
  });
});
