import React, { useRef } from 'react';

import Row, { RowProps } from './Row';
import {
  useDrop,
  useDrag,
  DragObjectWithType,
  DropTargetMonitor
} from 'react-dnd';
import { get } from '../../lodash';
import classnames from 'classnames';
import { getEmptyImage } from 'react-dnd-html5-backend';

export interface RowDnDProps extends Omit<RowProps, 'dropRef' | 'dragRef'> {}

const RowDnD: React.FC<RowDnDProps> = ({
  dndId,
  dndReadOnly = false,
  onRowMove,
  row,
  className,
  ...props
}) => {
  const dropRef = useRef<HTMLTableRowElement | null>(null);
  const dragRef = useRef<HTMLTableRowElement | null>(null);

  const handleHover = (
    item: DragObjectWithType,
    monitor: DropTargetMonitor,
    type: 'hover' | 'drop'
  ) => {
    const dragIndex = get(item, 'index') as number;
    const hoverIndex = get(row, 'index') as number;

    onRowMove?.(dragIndex, hoverIndex, type);
  };

  const [, drop] = useDrop({
    accept: dndId!,
    hover: (item, monitor) => handleHover(item, monitor, 'hover'),
    drop: (item, monitor) => handleHover(item, monitor, 'drop')
  });

  const [{ isDragging }, drag, preview] = useDrag({
    canDrag: !dndReadOnly,
    item: {
      type: dndId!,
      index: row?.index,
      template: <Row row={row} {...props} />,
      width: dragRef.current?.closest('tr')?.clientWidth,
      backgroundColor: dragRef.current
        ? getComputedStyle(dragRef.current).backgroundColor
        : ''
    },
    collect: (monitor) => {
      return {
        isDragging: monitor.isDragging()
      };
    },
    end: (item: any, monitor) => {
      const { index } = item;
      const didProp = monitor.didDrop();
      !didProp && onRowMove?.(index, index, 'drop');
    }
  });

  preview(getEmptyImage(), { captureDraggingState: true });
  drop(dropRef);
  drag(dragRef);

  return (
    <Row
      dropRef={dropRef}
      dragRef={dragRef}
      row={row}
      className={classnames(className, {
        dragging: isDragging
      })}
      dndReadOnly={dndReadOnly}
      {...props}
    />
  );
};

export default RowDnD;
