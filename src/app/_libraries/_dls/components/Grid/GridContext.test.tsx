import React, { render, screen } from '@testing-library/react';
import { useContext } from 'react';
import { Row } from 'react-table';

import { initState, gridReducer, GridContext } from './GridContext';
import { DispatchProps, GridStateProps } from './types';

const Comp = () => {
  const { dispatch } = useContext(GridContext);

  const dispatchReturned = dispatch({ type: 'SET_PROPS', checkAll: true });

  return (
    <div>
      <div data-testid="state.dispatch">{JSON.stringify(dispatchReturned)}</div>
    </div>
  );
};

describe('GridContext', () => {
  it('test dispatch init', () => {
    render(<Comp />);

    expect(screen.getByTestId('state.dispatch').textContent).toEqual('');
  });

  it('initState', () => {
    const { gridInstance } = initState;
    const row = { id: 'row' } as unknown as Row;
    expect(gridInstance.prepareRow(row)).toEqual(row);
    expect(gridInstance.getTableBodyProps()).toEqual({});
  });

  describe('gridReducer', () => {
    it('update grid instance', () => {
      const stateStored = { id: '1' } as unknown as GridStateProps;
      const action = {
        type: 'SET_GRID_INSTANCE',
        id: '2'
      } as unknown as DispatchProps;

      const state = gridReducer(stateStored, action);

      expect(state).toEqual({ id: '2' });
    });

    it('update grid props', () => {
      const stateStored = { id: '1' } as unknown as GridStateProps;
      const action = { type: 'SET_PROPS', id: '2' } as unknown as DispatchProps;

      const state = gridReducer(stateStored, action);

      expect(state).toEqual({ id: '2' });
    });

    it('update nothing', () => {
      const stateStored = { id: '1' } as unknown as GridStateProps;
      const action = { type: '', id: '2' } as unknown as DispatchProps;

      const state = gridReducer(stateStored, action);

      expect(state).toEqual(stateStored);
    });
  });
});
