import React, { FC, useState } from 'react';
import { GridProps, SortType } from './types';

// lodash
import sortByLodash from 'lodash.sortby';

import Grid from '.';

const MockGridWrapper: FC<GridProps> = ({
  sortBy: sortByProp,
  data: dataProp,
  expandedList: expandedListProp,
  checkedList: checkedListProp,
  onSortChange,
  onLoadMore,
  onCheck,
  onExpand,
  ...props
}) => {
  const [sortBy, setSortBy] = useState<SortType[]>(sortByProp || []);
  const [data, setData] = useState(dataProp);
  const [checkedList, setCheckedList] =
    useState<string[] | undefined>(checkedListProp);
  const [expandedList, setExpandedList] =
    useState<string[] | undefined>(expandedListProp);

  const sortData = (dataSort: any, sortVal: SortType) => {
    const newData = sortByLodash(dataSort, [(item: any) => item[sortVal.id]]);

    if (sortVal.order === 'desc') newData.reverse();

    return newData;
  };

  const handleSortChange = (sortVal: SortType) => {
    setSortBy([sortVal]);
    setData((data) => sortData(data, sortVal));
    onSortChange && onSortChange(sortVal);
  };

  const handleOnCheck = (dataKeyList: string[]) => {
    setCheckedList(dataKeyList);
    onCheck && onCheck(dataKeyList);
  };

  const handleOnExpand = (dataKeyList: string[]) => {
    setExpandedList(dataKeyList);
    onExpand && onExpand(dataKeyList);
  };

  return (
    <Grid
      ref={{ current: null }}
      data={data}
      sortBy={sortBy}
      onSortChange={handleSortChange}
      checkedList={checkedList}
      // readOnlyCheckbox={readOnlyCheckbox} // control readOnly checkbox
      expandedList={expandedList}
      onCheck={handleOnCheck}
      onExpand={handleOnExpand}
      {...props}
    />
  );
};

export default MockGridWrapper;
