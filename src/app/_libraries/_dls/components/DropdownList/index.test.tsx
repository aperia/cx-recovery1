import React from 'react';

// mock libs
import {
  render,
  RenderResult,
  queryByText,
  fireEvent
} from '@testing-library/react';
import userEvent from '@testing-library/user-event';
import '../../test-utils/mocks/mockCanvas';
import '@testing-library/jest-dom';
import {
  clearMockMutationObserver,
  createMockMutationObserver
} from '../../test-utils/mocks/mockMutationObserver';

// components
import DropdownList from './index';

// types
import { DropdownListProps } from './types';
import Button from '../Button';

const mockData = [
  {
    FieldID: 'ALL',
    FieldValue: 'All'
  },
  {
    FieldID: 'NER',
    FieldValue: 'NIGER'
  },
  {
    FieldID: 'BMU',
    FieldValue: 'BERMUDA'
  }
];

afterAll(clearMockMutationObserver);
createMockMutationObserver([]);

const dropdownListItem = () => {
  return mockData.map((item, index) => (
    <DropdownList.Item
      key={item.FieldID + index}
      label={item.FieldValue}
      value={item}
    />
  ));
};

const renderDropdownList = (
  props: DropdownListProps,
  renderChild?: any
): RenderResult => {
  return render(
    <div>
      <DropdownList ref={{ current: null }} {...props}>
        {renderChild ? renderChild() : []}
      </DropdownList>
      <Button autoBlur={false}>Test handleOnPopupClosed</Button>
    </div>
  );
};

const mockProps: any = {
  name: 'ddlName',
  textField: 'FieldValue',
  label: 'DropdownList Label Name',
  placeholder: 'My DropdownList Placeholder'
};

describe('render', () => {
  it('should render DropdownList', () => {
    const { container } = renderDropdownList({ ...mockProps });
    const dropdownListElement = container.querySelector('.dls-dropdown-list');

    expect(dropdownListElement).toBeInTheDocument();
  });

  it('should render DropdownList has optional item', () => {
    const { container } = renderDropdownList({
      ...mockProps,
      value: null,
      optional: true
    });
    const dropdownListElement = container.querySelector('.dls-dropdown-list');

    expect(dropdownListElement).toBeInTheDocument();
  });

  it('should render with other variant', () => {
    const { container } = renderDropdownList({
      ...mockProps,
      variant: 'no-border'
    });

    const dropdownListElement = container.querySelector('.dls-dropdown-list');
    expect(dropdownListElement?.className).toContain('no-border');
  });

  it('should render with textFieldRender', () => {
    const textFieldRender = jest.fn();

    const { baseElement, container } = renderDropdownList(
      {
        ...mockProps,

        textFieldRender
      },
      dropdownListItem
    );

    const dropdownListElement = container.querySelector('.dls-dropdown-list');
    const iconElement = dropdownListElement?.querySelector('.icon');
    userEvent.click(iconElement!);

    const popupElement = baseElement.querySelector('.dls-popup');
    expect(popupElement).toBeInTheDocument();

    const item = queryByText(popupElement as HTMLElement, /NIGER/);
    userEvent.click(item!);
    expect(textFieldRender).toBeCalledWith({
      FieldID: 'NER',
      FieldValue: 'NIGER'
    });
  });

  it('should render with disabled true', () => {
    const { container } = renderDropdownList({
      ...mockProps,
      disabled: true
    });

    const dropdownListElement = container.querySelector('.dls-dropdown-list');

    expect(dropdownListElement?.className).toContain('dls-disabled');
  });

  it('should render with small size', () => {
    const { container } = renderDropdownList({
      ...mockProps,
      size: 'small'
    });

    const dropdownListElement = container.querySelector('.dls-dropdown-list');

    expect(dropdownListElement?.className).toContain('dls-small');
  });

  it('should render with required', () => {
    const { container } = renderDropdownList({
      ...mockProps,
      required: true,
      readOnly: false,
      disabled: false
    });

    expect(container.querySelector('.asterisk')).toBeInTheDocument();
  });

  it('should render with bubble', () => {
    const { container } = renderDropdownList({
      ...mockProps,
      bubble: true,
      value: mockData[0]
    });

    expect(container.querySelector('.dls-bubble')).toBeInTheDocument();
  });

  it('should render DropdownList with data (in popup)', () => {
    const wrapper = renderDropdownList(
      {
        ...mockProps
      },
      dropdownListItem
    );
    const { container, baseElement, getByText } = wrapper;
    const iconDropdownListElement = container.querySelector(
      '.icon.icon-chevron-down'
    );
    userEvent.click(iconDropdownListElement!);

    // render popup with children items
    const popup = baseElement.querySelector('.dls-popup');

    expect(popup).toBeInTheDocument();
    expect(getByText(/NIGER/i)).toBeInTheDocument();
  });

  it('should render Tooltip error if have hasError and opened', () => {
    const { baseElement } = renderDropdownList({
      ...mockProps,
      error: {
        message: 'Tooltip test error message',
        status: true
      },
      opened: true
    });

    const tooltipElement = queryByText(
      baseElement as HTMLElement,
      /Tooltip test error message/
    );
    expect(tooltipElement).toBeInTheDocument();
  });
});

describe('actions', () => {
  it('should call onFocus if autoFocus is true', () => {
    const onFocus = jest.fn();

    renderDropdownList({
      ...mockProps,
      onFocus,
      autoFocus: true
    });

    expect(onFocus).toBeCalled();
  });

  it('should call onChange (unControlled)', () => {
    jest.useFakeTimers();
    const onChange = jest.fn();

    const wrapper = renderDropdownList(
      {
        ...mockProps,
        onChange
      },
      dropdownListItem
    );
    const { container, baseElement, getByText } = wrapper;
    const iconDropdownListElement = container.querySelector(
      '.icon.icon-chevron-down'
    );
    userEvent.click(iconDropdownListElement!);

    // render popup with children items
    const popup = baseElement.querySelector('.dls-popup');
    const item = getByText(/NIGER/i);

    expect(popup).toBeInTheDocument();
    expect(item).toBeInTheDocument();

    userEvent.click(item);
    jest.runAllTimers();

    expect(onChange).toBeCalledWith({
      bubbles: false,
      cancelable: false,
      changeBy: 'click',
      isTrusted: true,
      target: {
        id: undefined,
        name: 'ddlName',
        value: { FieldID: 'NER', FieldValue: 'NIGER' }
      },
      value: { FieldID: 'NER', FieldValue: 'NIGER' }
    });
  });

  it('should call onChange (controlled)', () => {
    jest.useFakeTimers();
    const onChange = jest.fn();

    const wrapper = renderDropdownList(
      {
        ...mockProps,
        onChange,
        value: { FieldID: 'ALL', FieldValue: 'All' }
      },
      dropdownListItem
    );
    const { container, baseElement, getByText } = wrapper;
    const iconDropdownListElement = container.querySelector(
      '.icon.icon-chevron-down'
    );
    userEvent.click(iconDropdownListElement!);

    // render popup with children items
    const popup = baseElement.querySelector('.dls-popup')!;
    const item = getByText(/NIGER/i);

    expect(popup).toBeInTheDocument();
    expect(item).toBeInTheDocument();

    fireEvent.keyDown(popup, { keyCode: 40 });
    fireEvent.keyDown(popup, { keyCode: 13 });
    jest.runAllTimers();

    expect(onChange).toBeCalledWith({
      bubbles: false,
      cancelable: false,
      changeBy: 'enter',
      isTrusted: true,
      target: {
        id: undefined,
        name: 'ddlName',
        value: { FieldID: 'ALL', FieldValue: 'All' }
      },
      value: { FieldID: 'ALL', FieldValue: 'All' }
    });
  });

  it('should call onChange valueType single (controlled)', () => {
    jest.useFakeTimers();
    const onChange = jest.fn();

    const wrapper = renderDropdownList(
      {
        ...mockProps,
        onChange,
        value: { FieldID: 'ALL', FieldValue: 'All' },
        searchBar: true
      },
      dropdownListItem
    );
    const { container, baseElement, getByText } = wrapper;
    const iconDropdownListElement = container.querySelector(
      '.icon.icon-chevron-down'
    );
    userEvent.click(iconDropdownListElement!);

    // render popup with children items
    const popup = baseElement.querySelector('.dls-popup')!;
    const item = getByText(/NIGER/i);

    expect(popup).toBeInTheDocument();
    expect(item).toBeInTheDocument();

    fireEvent.keyDown(popup, { keyCode: 40 });
    jest.runAllTimers();

    expect(onChange).toBeCalledWith({
      bubbles: false,
      cancelable: false,
      changeBy: 'up/down',
      isTrusted: true,
      target: {
        id: undefined,
        name: 'ddlName',
        value: { FieldID: 'ALL', FieldValue: 'All' }
      },
      value: { FieldID: 'ALL', FieldValue: 'All' }
    });
  });

  it('should call onVisibilityChange (uncontrolled)', () => {
    const onVisibilityChange = jest.fn();

    const { container } = renderDropdownList(
      {
        ...mockProps,
        onVisibilityChange
      },
      dropdownListItem
    );
    const iconDropdownListElement = container.querySelector(
      '.icon.icon-chevron-down'
    );
    userEvent.click(iconDropdownListElement!);

    expect(onVisibilityChange).toBeCalledWith(true);
  });

  it('should call onVisibilityChange (controlled)', () => {
    const onVisibilityChange = jest.fn();

    const { container } = renderDropdownList(
      {
        ...mockProps,
        onVisibilityChange,
        opened: true
      },
      dropdownListItem
    );
    const iconDropdownListElement = container.querySelector(
      '.icon.icon-chevron-down'
    );
    userEvent.click(iconDropdownListElement!);

    expect(onVisibilityChange).toBeCalledWith(true);
  });

  it('should call onBlur if double click on DropdownList', () => {
    const onBlur = jest.fn();
    const { container } = renderDropdownList(
      {
        ...mockProps,
        onBlur
      },
      dropdownListItem
    );

    const inputElement = container.querySelector('.text-field-container');
    userEvent.click(inputElement!);
    userEvent.click(inputElement!);

    expect(onBlur).toBeCalled();
  });

  it('if popup shown, mousedown on popup element will prevent all of next events', () => {
    jest.useFakeTimers();

    const { baseElement } = renderDropdownList({
      ...mockProps,
      opened: true
    });

    jest.runAllTimers();

    const popupElement = baseElement.querySelector('.dls-popup');
    userEvent.click(popupElement!);
  });

  it('should return if user click mousedown icon when DropdownList is disabled', () => {
    jest.useFakeTimers();

    const { baseElement, container } = renderDropdownList({
      ...mockProps,
      disabled: true
    });

    const iconElement = container.querySelector('.icon');
    userEvent.click(iconElement!);

    jest.runAllTimers();

    const popupElement = baseElement.querySelector('.dls-popup');
    expect(popupElement).not.toBeInTheDocument();
  });

  it('should return if user click mousedown DropdownList when DropdownList is disabled', () => {
    jest.useFakeTimers();

    const { baseElement, container } = renderDropdownList({
      ...mockProps,
      disabled: true
    });

    const iconElement = container.querySelector('.text-field-container');
    userEvent.click(iconElement!);

    jest.runAllTimers();

    const popupElement = baseElement.querySelector('.dls-popup');
    expect(popupElement).not.toBeInTheDocument();
  });

  it('should blur if user click mousedown icon when DropdownList is opened', () => {
    jest.useFakeTimers();

    const { baseElement, container } = renderDropdownList({
      ...mockProps
    });

    const iconElement = container.querySelector('.icon');
    userEvent.click(iconElement!);
    userEvent.click(iconElement!);

    jest.runAllTimers();

    const popupElement = baseElement.querySelector('.dls-popup');
    expect(popupElement).not.toBeInTheDocument();
  });

  it('should blur if user click body when DropdownList is opened', () => {
    const onBlur = jest.fn();

    const { container } = renderDropdownList({
      ...mockProps,
      onBlur
    });

    const inputElement = container.querySelector(
      '.text-field-container'
    ) as HTMLInputElement;
    userEvent.click(inputElement);

    // must use fire event
    fireEvent.blur(inputElement);

    expect(onBlur).toBeCalled();
  });

  it('should cover handlePopupClosed and apply next tabIndex is fakeFocusRef.current', () => {
    const onFilterChange = jest.fn();

    jest.useFakeTimers();
    const { container, baseElement, queryByText } = renderDropdownList({
      ...mockProps,
      searchBar: true,
      onFilterChange
    });

    const inputElement = container.querySelector(
      '.text-field-container'
    ) as HTMLInputElement;
    const buttonElement = queryByText('Test handleOnPopupClosed')!;
    userEvent.click(inputElement);
    jest.runAllTimers();

    userEvent.click(inputElement);
    jest.runAllTimers();

    userEvent.click(inputElement);
    jest.runAllTimers();

    const inputFilterElement = baseElement.querySelector('input')!;
    userEvent.type(inputFilterElement, 'nig');

    userEvent.click(buttonElement);
    jest.runAllTimers();

    expect(onFilterChange).toBeCalled();
  });

  it('should call handleFilterMode', () => {
    jest.useFakeTimers();
    const onFilterChange = jest.fn();

    const wrapper = renderDropdownList(
      {
        ...mockProps,
        onFilterChange,
        value: { FieldID: 'ALL', FieldValue: 'All' },
        searchBar: true
      },
      dropdownListItem
    );
    const { container } = wrapper;
    const iconDropdownListElement = container.querySelector(
      '.icon.icon-chevron-down'
    );
    userEvent.click(iconDropdownListElement!);
    jest.runAllTimers();

    const inputFilterElement = wrapper.baseElement.querySelector(
      '.dls-dropdown-base-search-bar input'
    )!;
    userEvent.type(inputFilterElement, 'nig');

    expect(onFilterChange).toBeCalled();
  });
});
