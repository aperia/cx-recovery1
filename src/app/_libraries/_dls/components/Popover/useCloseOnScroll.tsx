import React, { useEffect } from 'react';
import { StorageContextArgs } from '../../providers';

export interface CloseOnScrollHook {
  (
    opened: boolean | undefined,
    storage: StorageContextArgs,
    popoverInnerRef: React.MutableRefObject<HTMLDivElement | null>,
    keepRef: React.MutableRefObject<Record<string, any>>
  ): void;
}

const useCloseOnScroll: CloseOnScrollHook = (
  opened,
  storage,
  popoverInnerRef,
  keepRef
) => {
  useEffect(() => {
    if (!opened) return;

    const handleOnScroll = (event: Event) => {
      const target = event.target as HTMLElement;

      // if horizontal scroll or vertical scroll >> return
      if (target && popoverInnerRef.current?.contains(target)) {
        return;
      }
      if (target && target.scrollTop === 0 && target.scrollLeft > 0) {
        return;
      }

      // elements should be ignored
      const shouldIgnoreElement = storage.values?.find((element: HTMLElement) =>
        element.contains(target)
      );
      if (shouldIgnoreElement) return;

      keepRef.current?.handleOpenedMode(false);
    };

    window.addEventListener('scroll', handleOnScroll, true);
    return () => {
      window.removeEventListener('scroll', handleOnScroll, true);
    };
  }, [opened, storage, popoverInnerRef, keepRef]);
};

export default useCloseOnScroll;
