import React from 'react';
import '@testing-library/jest-dom';
import { fireEvent, render } from '@testing-library/react';
import useCloseOnScroll, { CloseOnScrollHook } from './useCloseOnScroll';

const MockComponent: React.FC<{ args: Parameters<CloseOnScrollHook> }> = ({
  args
}) => {
  useCloseOnScroll(...args);
  return null;
};

describe('test close on scroll', () => {
  it('should return when opened is false', () => {
    const handleOpenedMode = jest.fn();
    render(
      <MockComponent
        args={[false, null, null, { current: { handleOpenedMode } }]}
      />
    );
    expect(handleOpenedMode).not.toBeCalled();
  });

  it('should call scroll event', () => {
    // should ignore if horizontal scroll or vertical scroll
    render(
      <MockComponent
        args={[true, null, { current: { contains: () => true } }, {}]}
      />
    );
    fireEvent.scroll(document.body, { target: { scrollY: 100 } });
    render(<MockComponent args={[true, {}, {}, {}]} />);
    fireEvent.scroll(document.body, {
      target: { scrollY: 101, scrollTop: 0, scrollLeft: 1 }
    });

    // should ignore if storage has popup element
    render(
      <MockComponent
        args={[true, { values: [{ contains: () => true }] }, {}, {}]}
      />
    );
    fireEvent.scroll(document.body, {
      target: { scrollY: 102, scrollTop: 1 }
    });

    // should close popover
    const handleOpenedMode = jest.fn();
    render(
      <MockComponent args={[true, {}, {}, { current: { handleOpenedMode } }]} />
    );
    fireEvent.scroll(document.body, {
      target: { scrollY: 103 }
    });
    expect(handleOpenedMode).toBeCalledWith(false);
  });
});
