import React, { useEffect } from 'react';

import { PopperRef } from '../Popper';
import { StorageContextArgs } from '../../providers';

export interface MouseDownOutsideHook {
  (
    opened: boolean | undefined,
    storage: StorageContextArgs,
    popperRef: React.MutableRefObject<PopperRef | null>,
    popoverInnerRef: React.MutableRefObject<HTMLDivElement | null>,
    keepRef: React.MutableRefObject<Record<string, any>>
  ): void;
}

const useMouseDownOutside: MouseDownOutsideHook = (
  opened,
  storage,
  popperRef,
  popoverInnerRef,
  keepRef
) => {
  useEffect(() => {
    if (!opened) return;

    const handleMouseDownOutside = (event: MouseEvent) => {
      const target = event.target as HTMLElement;
      const triggerElement = popperRef.current?.triggerElement;

      if (
        popoverInnerRef.current?.contains(target) ||
        triggerElement?.contains(target)
      ) {
        return;
      }

      // elements should be ignored
      const shouldIgnoreElement = storage.values?.find((element: HTMLElement) =>
        element.contains(target)
      );
      if (shouldIgnoreElement) return;

      keepRef.current?.handleOpenedMode(false);
    };

    window.addEventListener('mousedown', handleMouseDownOutside);
    return () => {
      window.removeEventListener('mousedown', handleMouseDownOutside);
    };
  }, [opened, storage, popperRef, popoverInnerRef, keepRef]);
};

export default useMouseDownOutside;
