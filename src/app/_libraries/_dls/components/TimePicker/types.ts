import { ReactText } from 'react';
import { TooltipProps } from '../Tooltip';

export interface TimePickerValue {
  hour?: string;
  minute?: string;
  meridiem?: string;
  second?: string;
}

export interface TimePickerChangeEvent
  extends React.ChangeEvent<TimePickerValue> {
  target: EventTarget & { value?: TimePickerValue; id?: string; name?: string };
  value?: TimePickerValue;
}

export interface ErrorType {
  message?: string;
  status?: boolean;
}

export interface TimePickerProps extends DLSId {
  data?: {
    hours?: ReactText[];
    minutes?: ReactText[];
    meridiem?: ReactText[];
    seconds?: ReactText[];
  };
  placeholder?: string;
  onChange?: (event: TimePickerChangeEvent) => void;
  value?: TimePickerValue;
  label?: string;
  required?: boolean;
  error?: ErrorType;

  readOnly?: boolean;
  disabled?: boolean;
  className?: string;
  errorTooltipProps?: TooltipProps;
  id?: string;
  name?: string;
  onBlur?: (event: React.FocusEvent) => void;
  onFocus?: (event: React.FocusEvent) => void;
  small?: boolean;
  showSecond?: boolean;
  mask?: (string | RegExp)[];

  timeLabels?: {
    hour: string;
    minute: string;
    second?: string;
    meridiem?: string;
  };
}

export interface TimeColumnChangeEvent {
  value?: string | number;
  name: string;
  shouldBlur?: boolean;
}
export interface TimeColumnProp {
  label: string;
  data: (string | number)[];
  value?: string;
  name: string;
  onChange: (value: TimeColumnChangeEvent) => void;
}

export enum TimeName {
  hour = 'hour',
  minute = 'minute',
  meridiem = 'meridiem',
  second = 'second'
}
