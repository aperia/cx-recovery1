import React from 'react';
import TimeColumn from './TimeColumn';
import { act, fireEvent, render, screen } from '@testing-library/react';
import { queryAllByClass, queryByClass } from '../../test-utils';
import userEvent from '@testing-library/user-event';

Element.prototype.scrollTo = jest.fn();

describe('Test TimeColumn component', () => {
  const hoursData = Array(13)
    .fill('')
    .map((item, index) => (index < 10 ? `0${index}` : `${index}`));

  it('render component', () => {
    jest.useFakeTimers();

    const wrapper = render(
      <TimeColumn
        label="Label"
        name="Name"
        data={hoursData}
        value="05"
        onChange={jest.fn()}
      />
    );

    jest.runAllTimers();

    expect(
      queryByClass(wrapper.container, 'dls-time-picker__label')?.innerHTML
    ).toEqual('Label');
    expect(queryAllByClass(wrapper.container, /time-element/)?.length).toEqual(
      14
    );
  });

  it('has onChange props', () => {
    const onChangeSpy = jest.fn();

    jest.useFakeTimers();

    render(
      <TimeColumn
        label="Label"
        name="Name"
        data={hoursData}
        value="05"
        onChange={onChangeSpy}
      />
    );

    jest.runAllTimers();

    const item = screen.getByText('01');
    act(() => {
      userEvent.click(item);
    });

    expect(onChangeSpy).toBeCalled();
  });

  it('trigger wheel event > call onChange props', () => {
    const onChangeSpy = jest.fn();
    jest.useFakeTimers();

    const wrapper = render(
      <TimeColumn
        label="Label"
        name="Name"
        data={hoursData}
        value="05"
        onChange={onChangeSpy}
      />
    );

    jest.runAllTimers();

    const item = queryByClass(
      wrapper.container,
      'dls-time-picker__scroll-container'
    );
    fireEvent.scroll(item!, { deltaY: 0 });

    expect(onChangeSpy).toBeCalled();
  });

  it('trigger wheel event with value is undefined > call onChange props', () => {
    const onChangeSpy = jest.fn();
    jest.useFakeTimers();

    const wrapper = render(
      <TimeColumn
        label="Label"
        name="Name"
        data={hoursData}
        onChange={onChangeSpy}
      />
    );

    jest.runAllTimers();

    const item = queryByClass(
      wrapper.container,
      'dls-time-picker__scroll-container'
    );
    fireEvent.scroll(item!, { deltaY: 0 });

    expect(onChangeSpy).not.toBeCalled();
  });

  it('trigger wheel event > not call onChange props', () => {
    const onChangeSpy = jest.fn();
    jest.useFakeTimers();

    const wrapper = render(
      <TimeColumn
        label="Label"
        name="Name"
        data={hoursData}
        onChange={onChangeSpy}
      />
    );

    jest.runAllTimers();

    const item = queryByClass(
      wrapper.container,
      'dls-time-picker__scroll-container'
    );
    fireEvent.wheel(item!, { deltaY: -1024 });

    expect(onChangeSpy).not.toBeCalled();
  });
});
