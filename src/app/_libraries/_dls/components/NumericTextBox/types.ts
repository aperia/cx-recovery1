import { InputHTMLAttributes } from 'react';
import { TooltipProps } from '../Tooltip';

export interface SelectionRangeProp {
  start: number;
  end: number;
}

export interface NumericProps
  extends Omit<
      InputHTMLAttributes<HTMLInputElement>,
      'size' | 'onChange' | 'value'
    >,
    DLSId {
  value?: React.ReactText;
  format?: string;
  label?: string;
  error?: {
    message?: string;
    status?: boolean;
  };
  min?: number;
  size?: 'sm' | '' | string;
  max?: number;
  errorTooltipProps?: TooltipProps;
  autoFill?: boolean;
  /**
   * - If false, step will not be displayed
   */
  showStep?: boolean;
  onChange?: (event: OnChangeNumericEvent) => void;
  step?: string;
  negative?: boolean;

  prefix?: string;
  suffix?: string;
}

export interface OnChangeNumericEvent {
  value: React.ReactText;
  target: {
    value: React.ReactText;
    id?: string;
    name?: string;
  };
  nativeEvent: any;
}
