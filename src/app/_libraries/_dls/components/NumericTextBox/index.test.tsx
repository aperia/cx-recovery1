import React from 'react';
import '@testing-library/jest-dom';
import {
  render,
  RenderResult,
  act,
  fireEvent,
  createEvent
} from '@testing-library/react';
import { queryByClass } from '../../test-utils/queryHelpers';
// mocks
import '../../test-utils/mocks/mockCanvas';

import NumericTextBox from './index';
import { NumericProps } from './types';
import userEvent from '@testing-library/user-event';

let wrapper: RenderResult;
let baseElement: HTMLElement;

const renderComponent = (props: NumericProps | null = null) => {
  wrapper = render(
    <div>
      <NumericTextBox id="test" ref={{ current: null }} {...props} />
    </div>
  );

  baseElement = wrapper.baseElement as HTMLElement;
};

const getNode = {
  get container() {
    return queryByClass(baseElement, /dls-numreric-container/)!;
  },
  get input() {
    return baseElement.querySelector(
      '.dls-numreric-container > input'
    )! as HTMLElement;
  },
  get dlsPopper() {
    return baseElement.querySelector('.dls-popper-container')! as HTMLElement;
  },
  get increaseBtn() {
    return baseElement.querySelector(
      '.dls-numreric-container .increase'
    ) as HTMLElement;
  },
  get decreaseBtn() {
    return baseElement.querySelector(
      '.dls-numreric-container .decrease'
    ) as HTMLElement;
  },
  get prefix() {
    return baseElement.querySelector(
      '.dls-numreric-container .text-box-prefix'
    );
  },
  get suffix() {
    return baseElement.querySelector(
      '.dls-numreric-container .text-box-suffix'
    );
  }
};

const expectInputValue = (inputValue: string, expectValue: string) => {
  userEvent.type(getNode.input, inputValue);
  expect(getNode.input).toHaveValue(expectValue);
};

describe('Render component', () => {
  it('Should render component', () => {
    renderComponent();

    expect(getNode.container).toBeInTheDocument();
    expect(getNode.input).toBeInTheDocument();
  });

  it('Should render component with small size', () => {
    renderComponent({ size: 'sm' });

    expect(getNode.container).toBeInTheDocument();
    expect(getNode.input).toBeInTheDocument();
  });

  it('Should render component disabled', () => {
    renderComponent({ disabled: true });

    expect(getNode.container).toBeInTheDocument();
    expect(getNode.input).toBeInTheDocument();
    expect(getNode.input).toBeDisabled();
  });

  it('Should render component readOnly', () => {
    renderComponent({ readOnly: true });

    expect(getNode.container).toBeInTheDocument();
    expect(getNode.input).toBeInTheDocument();
    expect(getNode.input).toHaveAttribute('readonly');
  });

  it('Should render component readOnly with value', () => {
    renderComponent({ readOnly: true, value: '12', label: 'Label' });

    expect(queryByClass(getNode.container, /dls-floating/)).toBeInTheDocument();
    expect(getNode.container).toBeInTheDocument();
    expect(getNode.input).toBeInTheDocument();
    expect(getNode.input).toHaveAttribute('readonly');
  });

  it('Should render component with maxLength', () => {
    renderComponent({ maxLength: 4 });

    expect(getNode.container).toBeInTheDocument();
    expect(getNode.input).toBeInTheDocument();

    expectInputValue('11111', '11.11');
    userEvent.clear(getNode.input);

    expectInputValue('111', '11.00');
  });

  it('Should render input currency', () => {
    renderComponent({ format: 'c2' });

    expect(
      queryByClass(getNode.container, /text-box-prefix/)
    ).toBeInTheDocument();

    getNode.input.focus();
    expect(
      queryByClass(getNode.container, /text-box-prefix/)
    ).toBeInTheDocument();
    expect(
      queryByClass(getNode.container, /text-box-prefix/)
    ).toHaveTextContent('$');
  });

  it('Should render input percent', () => {
    renderComponent({ format: 'p2' });

    expect(
      queryByClass(getNode.container, /text-box-suffix/)
    ).toBeInTheDocument();

    getNode.input.focus();
    expect(
      queryByClass(getNode.container, /text-box-suffix/)
    ).toBeInTheDocument();
    expect(
      queryByClass(getNode.container, /text-box-suffix/)
    ).toHaveTextContent('%');
  });

  it('Should render error message', () => {
    renderComponent({
      label: 'label',
      required: true,
      error: { message: 'required', status: true }
    });

    expect(getNode.container).toHaveClass('dls-error');
    expect(
      queryByClass(getNode.container, /asterisk color-danger/)
    ).not.toBeInTheDocument();

    act(() => {
      getNode.input.focus();
    });
    expect(getNode.dlsPopper).toBeInTheDocument();

    act(() => {
      getNode.input.blur();
    });
    expect(getNode.dlsPopper).not.toBeInTheDocument();
  });

  it('uncontrolled component', () => {
    renderComponent({});

    expect(getNode.container).toBeInTheDocument();
    expect(getNode.input).toBeInTheDocument();

    expectInputValue('111', '111.00');

    userEvent.clear(getNode.input);
    expectInputValue('111.555', '111.55');

    userEvent.clear(getNode.input);
    expectInputValue('1', '1.00');
    expectInputValue('{backspace}', '');
  });

  it('controlled component', () => {
    renderComponent({ value: 100 });

    expect(getNode.container).toBeInTheDocument();
    expect(getNode.input).toBeInTheDocument();

    expectInputValue('111', '100.00');
    expectInputValue('{backspace}', '100.00');
  });

  it('controlled component with maxLength', () => {
    renderComponent({ value: 100, maxLength: 2 });

    expect(getNode.container).toBeInTheDocument();
    expect(getNode.input).toBeInTheDocument();
    expect(getNode.input).toHaveValue('');
  });

  it('validation required', () => {
    renderComponent({ label: 'label', required: true });

    expect(getNode.container).not.toHaveClass('dls-floating');
    expect(
      queryByClass(getNode.container, /asterisk color-danger/)
    ).not.toBeInTheDocument();

    getNode.input.focus();
    expect(getNode.container).toHaveClass('dls-floating');
  });

  it('click on label', () => {
    renderComponent({ label: 'label', required: true });

    const labelElement = queryByClass(getNode.container, /dls-floating-label/);
    labelElement?.click();
    expect(getNode.input).toHaveFocus();
  });
});

describe('Test increase/decrease', () => {
  it('click on increase/decrease', () => {
    renderComponent({ negative: true });

    userEvent.type(getNode.input, '-1.00');
    userEvent.click(getNode.increaseBtn!);
    expect(getNode.input).toHaveValue('');
    userEvent.click(getNode.increaseBtn!);
    expect(getNode.input).toHaveValue('1.00');

    userEvent.click(getNode.decreaseBtn!);
    expect(getNode.input).toHaveValue('');
    userEvent.click(getNode.decreaseBtn!);
    expect(getNode.input).toHaveValue('-1.00');
  });

  it('click on increase/decrease with disabled', () => {
    const onChangeFn = jest.fn();
    renderComponent({ onChange: onChangeFn, disabled: true });

    userEvent.click(getNode.increaseBtn!);
    expect(onChangeFn).not.toBeCalled();

    userEvent.click(getNode.decreaseBtn!);
    expect(onChangeFn).not.toBeCalled();
  });
});

describe('Test prefix/suffix', () => {
  it('render with prefix/suffix', () => {
    renderComponent({ prefix: 'XXX', readOnly: true, value: '1.00' });
    expect(getNode.prefix).toBeInTheDocument();

    renderComponent({ suffix: 'XXX', readOnly: true, value: '1.00' });
    expect(getNode.suffix).toBeInTheDocument();
  });
});

describe('Test function', () => {
  // TODO
  describe('onPaste', () => {
    it('valid value', () => {
      const onPasteFn = jest.fn();
      renderComponent({ maxLength: 4, autoFill: true, onPaste: onPasteFn });

      const event = createEvent.paste(getNode.input, {
        clipboardData: {
          types: ['text/plain'],
          getData: () => '123'
        }
      });
      fireEvent(getNode.input, event);
      expect(onPasteFn).toBeCalledTimes(1);
      // bypass after event paste
      userEvent.type(getNode.input, '123');
    });

    it('invalid value', () => {
      const onPasteFn = jest.fn();
      renderComponent({ maxLength: 4, autoFill: true, onPaste: onPasteFn });

      const event = createEvent.paste(getNode.input, {
        clipboardData: {
          types: ['text/plain'],
          getData: () => 'abc'
        }
      });
      fireEvent(getNode.input, event);
      expect(onPasteFn).toBeCalledTimes(1);
    });
  });

  it('handleKeyDown ctrl c v', () => {
    const onPasteFn = jest.fn();
    renderComponent({ maxLength: 4, onPaste: onPasteFn });

    Object.assign(navigator, { clipboard: { readText: () => '123' } });
    const commonEvent = { ctrlKey: true, bubbles: true, metaKey: true };
    document.dispatchEvent(
      new KeyboardEvent('keydown', { key: 'Control', ...commonEvent })
    );
    document.dispatchEvent(
      new KeyboardEvent('keydown', { key: 'a', ...commonEvent })
    );
    document.dispatchEvent(
      new KeyboardEvent('keydown', { key: 'c', ...commonEvent })
    );
    document.dispatchEvent(
      new KeyboardEvent('keydown', { key: 'v', ...commonEvent })
    );

    userEvent.click(getNode.input);
    userEvent.type(getNode.input, '{ctrl}a');
    userEvent.click(getNode.container);
    userEvent.click(getNode.input);
    expectInputValue('1', '1.00');
  });

  describe('effect value', () => {
    it('valid value', () => {
      renderComponent({ autoFill: true, autoFocus: true });
      jest.useFakeTimers();
      userEvent.type(getNode.input, '123');
      act(() => {
        userEvent.click(getNode.container);
        wrapper.rerender(
          <div>
            <NumericTextBox
              id="test"
              ref={{ current: null }}
              autoFill
              autoFocus={false}
            />
          </div>
        );
        jest.runAllTimers();
      });
      jest.runAllTimers();

      expect(getNode.input).toHaveValue('123');
    });

    it('invalid value', () => {
      renderComponent({ autoFill: true, autoFocus: true });
      jest.useFakeTimers();
      userEvent.type(getNode.input, '');
      act(() => {
        userEvent.click(getNode.container);
        wrapper.rerender(
          <div>
            <NumericTextBox
              id="test"
              ref={{ current: null }}
              autoFill
              autoFocus={false}
            />
          </div>
        );
        jest.runAllTimers();
      });
      jest.runAllTimers();

      expect(getNode.input).toHaveValue('');
    });
  });

  it('function onChange', () => {
    const onChangeFn = jest.fn();

    renderComponent({ maxLength: 4, onChange: onChangeFn });
    expectInputValue('1', '1.00');
    expect(onChangeFn).toBeCalledTimes(1);
    expect(onChangeFn).toHaveBeenCalledWith(
      expect.objectContaining({ value: '1.00' })
    );

    expectInputValue('{backspace}', '');
    expect(onChangeFn).toBeCalledTimes(2);

    expectInputValue('1.', '1.00');
    expectInputValue('{backspace}', '1.00');
    expect(onChangeFn).toHaveBeenLastCalledWith(
      expect.objectContaining({ value: '1.00' })
    );

    expectInputValue('11111', '11.11');
    expect(onChangeFn).toHaveBeenLastCalledWith(
      expect.objectContaining({ value: '11.11' })
    );
  });

  it('function onChange with restrict value', () => {
    const onChangeFn = jest.fn();

    renderComponent({ onChange: onChangeFn });
    expectInputValue('!~`@#$%^&*()_+=:;"-?', '');
    expectInputValue('abcdefABCDEF', '');

    expect(onChangeFn).toBeCalledTimes(0);
    expectInputValue('1', '1.00');
    expect(onChangeFn).toBeCalledTimes(1);
  });

  it('function onChange with negative', () => {
    const onChangeFn = jest.fn();

    renderComponent({ onChange: onChangeFn, negative: true });

    expectInputValue('-0', '-0.00');
    userEvent.type(getNode.input, '{backspace}');
    expect(onChangeFn).toBeCalledTimes(2);
  });

  it('function onFocus', () => {
    const onFocusFn = jest.fn();
    renderComponent({ onFocus: onFocusFn });

    userEvent.click(getNode.input);
    expect(onFocusFn).toBeCalledTimes(1);
  });

  it('function onBlur', () => {
    const onBlurFn = jest.fn();
    renderComponent({ onBlur: onBlurFn });

    userEvent.click(getNode.input);
    getNode.input.blur();
    expect(onBlurFn).toBeCalledTimes(1);

    userEvent.type(getNode.input, '123');
    getNode.input.blur();
    expect(getNode.input).toHaveValue('123.00');
  });

  it('function onBlur with min, max value', () => {
    const onBlurFn = jest.fn();
    renderComponent({ min: 10, max: 100, onBlur: onBlurFn });

    userEvent.click(getNode.input);
    getNode.input.blur();
    expect(onBlurFn).toBeCalledTimes(1);

    userEvent.type(getNode.input, '2');
    getNode.input.blur();
    expect(getNode.input).toHaveValue('10.00');

    userEvent.clear(getNode.input);
    userEvent.type(getNode.input, '123');
    getNode.input.blur();
    expect(getNode.input).toHaveValue('100.00');
  });

  it('function onBlur with negative', () => {
    const onBlurFn = jest.fn();
    renderComponent({ onBlur: onBlurFn, negative: true });

    userEvent.click(getNode.input);

    userEvent.type(getNode.input, '-');
    getNode.input.blur();
    expect(getNode.input).toHaveValue('');
  });
});
