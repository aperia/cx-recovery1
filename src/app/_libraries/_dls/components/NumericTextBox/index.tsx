import React, {
  useEffect,
  useImperativeHandle,
  useMemo,
  useRef,
  useState
} from 'react';

// utils/ types
import isUndefined from 'lodash.isundefined';
import isEmpty from 'lodash.isempty';
import pick from 'lodash.pick';
import unset from 'lodash.unset';
import classNames from 'classnames';
import { classes, className, genAmtId, truncateTooltip } from '../../utils';
import { NumericProps, SelectionRangeProp } from './types';
import { nanoid } from 'nanoid';

// constants
import {
  PERCENT,
  CURRENCY,
  DEFAULT_CURRENCY,
  DEFAULT_MAX_LENGTH,
  DEFAULT_PERCENT,
  DEFAULT_FORMAT
} from './constant';

// services
import {
  getDeference,
  getFormat,
  convertDataViewFloat,
  convertValueToDataView,
  getNewPosition,
  getLengthDataView,
  isValidPasteInput,
  isNumberFloat,
  isNumber,
  isValidStep
} from './service';

// components
import Tooltip from '../../components/Tooltip';
import Label from '../Label';

// hooks
import { useBlurOnScroll } from '../../hooks';
import Big from 'big.js';
import Icon from '../Icon';
import { useAutoFill } from './hooks';

const NumericTextBox: React.RefForwardingComponent<
  HTMLInputElement,
  NumericProps
> = (
  {
    id,
    name,
    maxLength = DEFAULT_MAX_LENGTH,
    value: valueProp,
    format = DEFAULT_FORMAT,
    min,
    max,
    error,
    label,
    required,
    disabled,
    readOnly,
    errorTooltipProps,
    size,
    autoFill,

    negative,
    step = '1',
    prefix,
    suffix,
    showStep = true,

    onPaste,
    onChange,
    onBlur,
    onFocus,

    dataTestId,
    ...props
  },
  ref
) => {
  // handle eslint no-unused-var, can't destructor params above
  unset(props, 'size');

  // Refs
  const inputRef = useRef<HTMLInputElement | null>(null);
  const keepCurrentCursorRef = useRef<typeof currentCursor>();
  const floatingLabelRef = useRef<HTMLSpanElement | null>(null);
  const isPasteValue = useRef(false);

  const [value, setValue] = useState(convertValueToDataView(valueProp, format));
  const [isFocus, setFocus] = useState(false);
  const [currentCursor, setCurrentCursor] = useState<SelectionRangeProp>({
    start: 0,
    end: 0
  });

  keepCurrentCursorRef.current = currentCursor;

  const { message, status } = pick(error, 'message', 'status');
  const isOpenedTooltip = Boolean(status && message && isFocus);
  const isLabelFocus = readOnly && !value ? false : isFocus || !!value;

  const { type } = useMemo(() => getFormat(format), [format]);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    let nextValue = event.target.value;

    // if nagative and nextValue is sign, internal value is allowed
    // but not onChange
    if (negative && nextValue === '-') return setValue(nextValue);

    if (isPasteValue.current) {
      nextValue = parseFloat(nextValue.replace(/,/g, '')) + '';
      isPasteValue.current = false;
    }

    // Get current mouse
    const dotPosition = event.target.value.indexOf('.');
    const signPosition = event.target.value.indexOf('-');

    // ex .00 or undefine will format to ''
    const isFirstDotPosition = dotPosition === 0;
    const isFirstNegativeSign = signPosition === 0 && dotPosition === 1;

    if (
      !event.target.value ||
      ((isFirstDotPosition || isFirstNegativeSign) &&
        !Number(event.target.value.split('.')[1]))
    ) {
      setValue('');
      onChange?.({
        value: '',
        target: { value: '', id, name },
        nativeEvent: event
      });
      return;
    }

    // Handle decimal path, selectionStart will be undefined on click increase/decrease
    const selectionStart = event.target.selectionStart!;

    const arrWithChange = getDeference(value, nextValue);
    const hasRemoveDot = arrWithChange.includes('.');

    if (hasRemoveDot && !isUndefined(selectionStart)) {
      nextValue =
        nextValue.substring(0, selectionStart) +
        '.' +
        nextValue.substring(selectionStart);
    }

    // First typing or value input = value state = 0 will be format with format property,
    // else is default format (n0)
    const raw = convertValueToDataView(nextValue, format);

    const position = getNewPosition(
      value,
      nextValue,
      format,
      selectionStart || inputRef.current?.selectionStart || nextValue.length
    );

    setCurrentCursor({ start: position, end: position });

    if (!negative && Big(convertDataViewFloat(raw, format)).lt(0)) return;

    // do nothing if input length reach maxLength
    if (getLengthDataView(raw) > maxLength) {
      return;
    }

    // uncontrolled
    isUndefined(valueProp) && setValue(raw);

    const floatValue = convertDataViewFloat(raw, format);

    onChange?.({
      value: floatValue,
      target: { value: floatValue, id, name },
      nativeEvent: event
    });
  };

  const handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    setFocus(false);

    if (isUndefined(value) || isEmpty(value)) {
      onBlur?.({ ...event, target: { ...event.target, id: id!, name: name! } });
      return;
    }

    let rawValue = value;
    const valueFloat = convertDataViewFloat(value, format);

    // handle remove sign, set internal value, but not onChange
    if (negative && value === '-') {
      setValue('');
    }

    // Auto set min max
    if (min) {
      const rawMin = convertDataViewFloat(min, format);
      if (Number(valueFloat) < Number(rawMin)) {
        setValue(rawMin);
        rawValue = rawMin;
      }
    }

    if (max) {
      const rawMax = convertDataViewFloat(max, format);
      if (Number(valueFloat) > Number(rawMax)) {
        setValue(rawMax);
        rawValue = rawMax;
      }
    }

    const floatValue = convertDataViewFloat(rawValue, format);
    onBlur?.({
      ...event,
      target: { ...event.target, value: floatValue, id: id!, name: name! }
    });
  };

  const handleFocus = (event: React.FocusEvent<HTMLInputElement>) => {
    onFocus?.({
      ...event,
      target: { ...event.target, id: id!, name: name! }
    });
    setFocus(true);
  };

  const handleKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    const restrictKey = /^[A-Za-z!~`@#$%^&*()_+=:;"\-'><,\]?[/|\\\{}}]$/;

    if (event.ctrlKey) return;

    // move cursor when user typing '.'
    if (event.key === '.') {
      event.preventDefault();
      setCurrentCursor({
        start: currentCursor.start + 1,
        end: currentCursor.end + 1
      });
    }

    if (negative && value === '' && event.key === '-') return;

    if (restrictKey.test(event.key)) event.preventDefault();
  };

  const handlePaste = (event: React.ClipboardEvent<HTMLInputElement>) => {
    const valuePaste = event.clipboardData.getData('Text');
    const isValid =
      isNumberFloat(valuePaste) ||
      isValidPasteInput(valuePaste) ||
      isNumber(valuePaste);
    isPasteValue.current = false;

    if (!valuePaste || !isValid) {
      event.preventDefault();
      event.stopPropagation();
    } else {
      isPasteValue.current = true;
    }

    onPaste?.(event);
  };

  const handleClickIncrease = (event: React.MouseEvent) => {
    event.preventDefault();

    if (!isValidStep(step, format) || readOnly || disabled) return;

    const currentValue = new Big(convertDataViewFloat(value, format) || '0');
    const nextValue = currentValue.plus(step);

    const nextValueDataView = convertValueToDataView(
      nextValue.toString(),
      format
    );

    handleChange({ target: { value: nextValueDataView } } as any);
  };

  const handleClickDecrease = (event: React.MouseEvent) => {
    event.preventDefault();

    if (!isValidStep(step, format) || readOnly || disabled) return;

    const currentValue = new Big(convertDataViewFloat(value, format) || '0');
    const nextValue = currentValue.minus(step);

    const nextValueDataView = convertValueToDataView(
      nextValue.toString(),
      format
    );

    handleChange({ target: { value: nextValueDataView } } as any);
  };

  useImperativeHandle(ref, () => inputRef.current!);

  // add blur on scroll if Tooltip is opened && error && scrolled
  useBlurOnScroll(inputRef.current!, Boolean(status && isFocus));

  // Update valueProp
  useEffect(() => {
    setCurrentCursor({
      start: keepCurrentCursorRef.current!.start,
      end: keepCurrentCursorRef.current!.end
    });
    const formatData = convertValueToDataView(valueProp, format);
    getLengthDataView(formatData) > maxLength
      ? setValue('')
      : setValue(formatData);
  }, [valueProp, format, maxLength]);

  // Update correct caret when user typing
  useEffect(() => {
    inputRef.current?.setSelectionRange(currentCursor.start, currentCursor.end);
  }, [currentCursor]);

  // handle autoFill
  // example:
  // 12.00 > out focus > 12
  // 0.00 > out focus > 0
  useAutoFill({
    autoFill,
    focused: isFocus,
    disabled,
    readOnly,
    value,
    valueProp,
    inputElement: inputRef.current!
  });

  // handle truncate tooltip label
  useEffect(() => truncateTooltip(floatingLabelRef.current!), [label]);

  const isSmall = size === 'sm';
  const hasLabel = !isSmall && !!label;
  const condition =
    !hasLabel || (readOnly && value) || (!readOnly && (value || isFocus));

  const hasDefaultPreFix = format && type === CURRENCY && condition;
  const hasDefaultSuffix = format && type === PERCENT && condition;
  const defaultPrefixElement = hasDefaultPreFix ? (
    <span className={className.textBox.PREFIX}>{DEFAULT_CURRENCY}</span>
  ) : null;
  const defaultSuffixElement = hasDefaultSuffix ? (
    <span className={className.textBox.SUFFIX}>{DEFAULT_PERCENT}</span>
  ) : null;

  const hasPrefix = prefix && condition;
  const hasSuffix = suffix && condition;
  const prefixElement = hasPrefix ? (
    <span className={className.textBox.PREFIX}>{prefix}</span>
  ) : null;
  const suffixElement = hasSuffix ? (
    <span className={className.textBox.SUFFIX}>{suffix}</span>
  ) : null;

  return (
    <Tooltip
      opened={isOpenedTooltip}
      element={message}
      variant="error"
      placement="top-start"
      triggerClassName="d-block"
      dataTestId={`${dataTestId}-numeric-text-box-error`}
      {...errorTooltipProps}
    >
      <div
        className={classNames('dls-numreric-container', size, {
          [className.state.REQUIRED]: !!required,
          [className.state.FLOATING]: isLabelFocus,
          [className.state.FOCUSED]: isFocus,
          [className.state.DISABLED]: disabled,
          [className.state.ERROR]: status,
          [className.input.SUFFIX]: hasSuffix || hasDefaultSuffix,
          [className.input.PREFIX]: hasPrefix || hasDefaultPreFix,
          [className.state.READ_ONLY]: readOnly,
          [className.input.LABEL]: hasLabel,
          [classes.numericTextBox.onlySuffix]:
            (hasSuffix || hasDefaultSuffix) && !showStep,
          [classes.numericTextBox.noSuffix]:
            !hasSuffix && !hasDefaultSuffix && !showStep
        })}
        data-testid={genAmtId(
          dataTestId,
          'dls-numeric-textbox',
          'NumericTextBox'
        )}
      >
        {prefixElement || defaultPrefixElement}
        <input
          ref={inputRef}
          readOnly={readOnly}
          disabled={disabled}
          required={required}
          value={value}
          onChange={handleChange}
          onKeyDown={handleKeyDown}
          onBlur={handleBlur}
          onFocus={handleFocus}
          onPaste={handlePaste}
          autoComplete={nanoid()}
          data-testid={genAmtId(
            dataTestId,
            'dls-numeric-textbox-input',
            'NumericTextBox'
          )}
          {...(props as React.DetailedHTMLProps<
            React.InputHTMLAttributes<HTMLInputElement>,
            HTMLInputElement
          >)}
        />
        {hasLabel && (
          <Label
            ref={floatingLabelRef}
            asterisk={required}
            error={status}
            onClick={() => inputRef.current?.focus()}
            dataTestId={genAmtId(
              dataTestId,
              'dls-numeric-textbox-label',
              'NumericTextBox'
            )}
          >
            <span>{label}</span>
          </Label>
        )}
        {suffixElement || defaultSuffixElement}
        {showStep && (
          <div className="dls-numeric-step">
            <div onMouseDown={handleClickIncrease} className="step increase">
              <Icon size={isSmall ? '2x' : '3x'} name="chevron-up" />
            </div>
            <div onMouseDown={handleClickDecrease} className="step decrease">
              <Icon size={isSmall ? '2x' : '3x'} name="chevron-down" />
            </div>
          </div>
        )}
      </div>
    </Tooltip>
  );
};

export default React.forwardRef<HTMLInputElement, NumericProps>(NumericTextBox);
