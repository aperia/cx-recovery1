import React from 'react';
import Switch from '../Switch';
import '@testing-library/jest-dom';
import { fireEvent, queryByText, render } from '@testing-library/react';
import { mockUseRef } from '../../test-utils/hooks';
import { Form } from 'react-bootstrap';
import { queryBy } from '../../test-utils/queryHelpers';

Form.Switch = Switch as any;

describe('Test Switch', () => {
  const spyRef = mockUseRef();
  it('Should have default and uncontrolled', () => {
    const { container } = render(<Form.Switch ref={spyRef} />);

    const queryDivClass = queryBy(
      container,
      'class',
      'dls-custom-control dls-custom-switch'
    );

    const queryInputClass = queryBy(
      container,
      'class',
      'dls-custom-control-input'
    );

    const queryLabelClass = queryBy(
      container,
      'class',
      'dls-custom-control-label'
    );

    const queryInput = queryBy(container, 'class', 'dls-custom-control-input');

    const queryType = queryBy(container, 'type', 'checkbox');

    fireEvent.click(queryInput!);

    expect(queryDivClass).toBeInTheDocument();
    expect(queryInputClass).toBeInTheDocument();
    expect(queryLabelClass).toBeInTheDocument();
    expect(queryType).toBeInTheDocument();
  });

  it('Should have checkProp', () => {
    const { container } = render(<Form.Switch checked={true} />);

    const queryChecked = queryBy(container, 'checked', '');

    expect(queryChecked).toBeInTheDocument();
  });

  it('Should have onChange with controlled', () => {
    const onChange = jest.fn();
    const { container } = render(
      <Form.Switch onChange={onChange} checked={true} />
    );

    const queryInput = queryBy(container, 'class', 'dls-custom-control-input');

    fireEvent.click(queryInput!);

    expect(onChange).toHaveBeenCalled();
  });

  it('Should have disabled', () => {
    const { container } = render(<Form.Switch disabled={true} ref={spyRef} />);

    const queryClass = queryBy(container, 'disabled', '');

    expect(queryClass).toBeInTheDocument();
  });

  it('Should have classNameProps', () => {
    const labelClassName = 'labelClassName';
    const inputClassName = 'inputClassName';
    const containerClassName = 'containerClassName';

    const { container } = render(
      <Form.Switch
        labelClassName={labelClassName}
        inputClassName={inputClassName}
        containerClassName={containerClassName}
        ref={spyRef}
      />
    );

    const queryLabelClassName = queryBy(container, 'class', /labelClassName/);

    const queryInputClassName = queryBy(container, 'class', /inputClassName/);

    const queryContainerClassName = queryBy(
      container,
      'class',
      /containerClassName/
    );

    expect(queryLabelClassName).toBeInTheDocument();
    expect(queryInputClassName).toBeInTheDocument();
    expect(queryContainerClassName).toBeInTheDocument();
  });

  it('Should have Label', () => {
    const label = 'label';
    const { container } = render(<Form.Switch label={label} />);

    const queryText = queryByText(container, label);

    expect(queryText?.textContent).toEqual(label);
  });
});
