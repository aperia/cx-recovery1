import React, {
  ReactNode,
  useEffect,
  useImperativeHandle,
  useRef,
  useState
} from 'react';

// utils
import classNames from 'classnames';
import { className, genAmtId } from '../../utils';
import isBoolean from 'lodash.isboolean';
import isFunction from 'lodash.isfunction';

// components
import { Form } from 'react-bootstrap';

type SwitchPropsBS = typeof Form.Switch;

export interface SwitchProps extends SwitchPropsBS, DLSId {
  label?: ReactNode;
  disabled?: boolean;
  name?: string;
  id?: string;
  checked?: boolean;
  onChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  containerClassName?: string;
  inputClassName?: string;
  labelClassName?: string;
}

export interface SwitchRef {}

const Switch: React.ForwardRefRenderFunction<SwitchRef, SwitchProps> = (
  {
    label,
    id,
    checked: checkedProp,
    onChange: onChangeProp,
    name,
    disabled,
    containerClassName,
    inputClassName,
    labelClassName,

    dataTestId,
    ...props
  },
  reference
) => {
  const ref = useRef<HTMLInputElement | null>(null);
  useImperativeHandle(reference, () => ref.current!);

  const [checked, setChecked] = useState(isBoolean(checkedProp) && checkedProp);

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    isFunction(onChangeProp) && onChangeProp(event);

    // uncontrolled
    !isBoolean(checkedProp) && setChecked((state) => !state);
  };

  useEffect(() => {
    if (!isBoolean(checkedProp)) return;

    setChecked(checkedProp);
  }, [checkedProp]);

  return (
    <div
      className={classNames(className.switch.CONTAINER, containerClassName)}
      data-testid={genAmtId(dataTestId, 'dls-switch', 'Switch')}
    >
      <span className={classNames(className.switch.LABEL_TEXT)}>{label}</span>
      <input
        type="checkbox"
        id={id}
        name={name}
        className={classNames(className.switch.INPUT, inputClassName)}
        onChange={handleChange}
        checked={checked}
        disabled={disabled}
        ref={ref}
        {...props}
      />
      <label
        htmlFor={id}
        className={classNames(className.switch.LABEL, labelClassName)}
      ></label>
    </div>
  );
};

Form.Switch = Switch as SwitchProps;

export default React.forwardRef<SwitchRef, SwitchProps>(Switch);
