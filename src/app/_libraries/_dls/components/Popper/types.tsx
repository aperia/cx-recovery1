import {
  Placement,
  Modifier,
  State,
  PositioningStrategy,
  Instance
} from '@popperjs/core';

export interface PopperProps extends DLSId {
  /**
   * - Append/Remove Popper container element to DOM body
   */
  opened?: boolean;
  /**
   * - Element will be rendered in Popper container element
   * - Important: this element don't rerender/mount/unmount when "opened" change
   * - If you want to mount/unmount this element, please update "keepMount" to true
   */
  element?: React.ReactNode;
  /**
   * - If set to true, component in "component" prop will be mount/unmount when "opened" prop change
   * - default: false
   */
  keepMount?: boolean;
  onPlacementChange?: (nextPlacement: Placement) => void;
  placement?: Placement;
  /**
   * - https://popper.js.org/docs/v2/modifiers/
   */
  modifiers?: Partial<Modifier<any, any>>[];
  onFirstUpdate?: (arg0: Partial<State>) => void;
  strategy?: PositioningStrategy;
  triggerClassName?: string;
  containerClassName?: string;
  arrowClassName?: string;
  children?: React.ReactNode;
}

export interface PopperRef {
  instance: Instance;
  triggerElement: HTMLSpanElement;
}
