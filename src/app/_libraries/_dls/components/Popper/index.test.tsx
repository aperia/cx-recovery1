import React from 'react';
import { render, RenderResult } from '@testing-library/react';
import '@testing-library/jest-dom';

import Popper from '.';
import { queryByClass } from '../../test-utils/queryHelpers';
import { PopperProps } from './types';

let wrapper: RenderResult;

interface RenderComponentProps extends Omit<PopperProps, 'children'> {}

const reset = () => {
  const containerElement = document.getElementsByClassName(
    'dls-popper-container'
  );

  for (let i = 0; i < containerElement.length; i++) {
    containerElement[i].remove();
  }
};

const renderComponent = (props: RenderComponentProps) => {
  const element = <div />;

  wrapper = render(
    <div>
      <Popper ref={{ current: null }} element={element} {...props}>
        Popper
      </Popper>
    </div>
  );

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement,
    rerender: (newProps: RenderComponentProps) => {
      wrapper.rerender(
        <div>
          <Popper ref={{ current: null }} element={element} {...newProps} />
        </div>
      );
    }
  };
};

const getNode = {
  get container() {
    return queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-popper-container/
    );
  },
  get trigger() {
    return queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-popper-trigger/
    );
  }
};

beforeEach(() => jest.useFakeTimers());

afterEach(() => reset());

describe('Render', () => {
  it('test render', () => {
    renderComponent({});

    expect(getNode.trigger).toBeInTheDocument();
  });

  it('test render when oppend', () => {
    renderComponent({ opened: true, containerClassName: 'class-name' });

    expect(getNode.trigger).toBeInTheDocument();
    expect(getNode.container).toBeInTheDocument();
  });

  it('test render when add class after render', () => {
    const { baseElement, rerender } = renderComponent({
      opened: true,
      containerClassName: 'class-1 class-1'
    });

    expect(queryByClass(baseElement, /class-1/)).toBeInTheDocument();

    rerender({
      opened: true,
      containerClassName: 'class-1 class-1 class-2'
    });

    expect(queryByClass(baseElement, /class-1 class-1 class-2/)).toBeNull();
    expect(queryByClass(baseElement, /class-1 class-2/)).toBeInTheDocument();
    expect(getNode.trigger).toBeInTheDocument();
  });

  it('test render when add class after render', () => {
    const { baseElement, rerender } = renderComponent({
      opened: true,
      arrowClassName: 'class-1'
    });

    expect(queryByClass(baseElement, /class-1/)).toBeInTheDocument();

    rerender({ opened: true, arrowClassName: 'class-2 class-1' });

    expect(queryByClass(baseElement, /class-1 class-2/)).toBeInTheDocument();
    expect(getNode.trigger).toBeInTheDocument();
  });

  it('test render toggle open', () => {
    const { rerender } = renderComponent({ opened: true });

    expect(getNode.trigger).toBeInTheDocument();
    expect(getNode.container).toBeInTheDocument();

    rerender({ opened: false });

    expect(getNode.trigger).toBeInTheDocument();
    expect(getNode.container).toBeNull();

    rerender({ opened: true });

    expect(getNode.trigger).toBeInTheDocument();
    expect(getNode.container).toBeInTheDocument();
  });

  it('test render toggle open', () => {
    // const { rerender } = renderComponent({ opened: true });
  });
});
