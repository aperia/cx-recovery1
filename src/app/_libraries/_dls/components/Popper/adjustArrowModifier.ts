import { detectOverflow, Placement } from '@popperjs/core';
import get from 'lodash.get';

// Popper arrow is always centered against the trigger element
// For IconButton, we will custom this arrow by using css
// for the remaining cases, we will custom this arrow by using js
const adjustArrowModifier = {
  name: 'adjust-arrow',
  enabled: true,
  phase: 'write',
  fn: ({ state }: any) => {
    const { placement, rects } = state;
    const popper = get(state.elements, 'popper') as HTMLElement;
    const arrow = get(state.elements, 'arrow') as HTMLElement;
    const heightPopper = get(rects, ['popper', 'height'], 0) as number;
    const widthPopper = get(rects, ['popper', 'width'], 0) as number;
    const distanceX = 22;
    const distanceY = heightPopper > 38 ? 22 : 18;

    const arrowStyles = getComputedStyle(arrow);
    const popperStyles = getComputedStyle(popper);
    const { m41: popperX, m42: popperY } = new DOMMatrix(
      popperStyles.transform
    );
    const { m41: arrowX, m42: arrowY } = new DOMMatrix(arrowStyles.transform);

    if (
      popper.classList.contains('dls-tooltip-container') &&
      popper.classList.contains('error')
    ) {
      switch (placement) {
        case 'top-start':
        case 'bottom-start':
          arrow.style.transform = 'unset';
          arrow.style.left = '20px';
          break;
        case 'top-end':
        case 'bottom-end':
          arrow.style.transform = 'unset';
          arrow.style.left = 'calc(100% - 20px)';
          break;
      }
      return state;
    }

    switch (placement) {
      case 'top':
      case 'bottom':
        if (arrowX < distanceX) {
          const adjust = arrowX === 0;
          let left = 0;

          if (adjust) {
            left = detectOverflow(state, {
              placement: `${placement}-start` as Placement,
              padding: { left: 8, right: 8 }
            }).left;
          }

          const nextArrowX = distanceX;
          const nextPopperX = popperX - (adjust ? left : distanceX - arrowX);

          arrow.style.transform = `translate(${nextArrowX}px, ${arrowY}px)`;
          popper.style.transform = `translate(${nextPopperX}px, ${popperY}px)`;
          break;
        }
        if (widthPopper - arrowX < distanceX) {
          const adjust = Math.floor(widthPopper) - (arrowX + 2) <= 0;
          let right = 0;

          if (adjust) {
            right = detectOverflow(state, {
              placement: `${placement}-end` as Placement,
              padding: { left: 8, right: 8 }
            }).right;
          }

          const nextArrowX = widthPopper - distanceX;
          const nextPopperX = popperX + (adjust ? right : arrowX - nextArrowX);

          arrow.style.transform = `translate(${nextArrowX}px, ${arrowY}px)`;
          popper.style.transform = `translate(${nextPopperX}px, ${popperY}px)`;
          break;
        }
        break;
      case 'right':
      case 'left':
        if (arrowY < distanceY) {
          const adjust = arrowY === 0;
          let top = 0;

          if (adjust) {
            top = detectOverflow(state, {
              placement: `${placement}-start` as Placement,
              padding: { top: 8, bottom: 8 }
            }).top;
          }

          const nextArrowY = distanceY;
          const nextPopperY = popperY - (adjust ? top : distanceY - arrowY);

          arrow.style.transform = `translate(0px, ${nextArrowY}px)`;
          popper.style.transform = `translate(${popperX}px, ${nextPopperY}px)`;
          break;
        }
        if (heightPopper - arrowY < distanceY) {
          const adjust = Math.floor(heightPopper) - (arrowY + 2) <= 0;
          let bottom = 0;

          if (adjust) {
            bottom = detectOverflow(state, {
              placement: `${placement}-end` as Placement,
              padding: { top: 8, bottom: 8 }
            }).bottom;
          }

          const nextArrowY = heightPopper - distanceY;
          const nextPopperY = popperY + (adjust ? bottom : arrowY - nextArrowY);

          arrow.style.transform = `translate(0px, ${nextArrowY}px)`;
          popper.style.transform = `translate(${popperX}px, ${nextPopperY}px)`;
        }
    }

    return state;
  }
};

export default adjustArrowModifier;
