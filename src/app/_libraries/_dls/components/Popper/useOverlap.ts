import { useEffect } from 'react';

import { Instance } from '@popperjs/core';

export interface OverlapOptions {
  popperContainer: HTMLElement;
  popperInstance: Instance;
  opened: boolean;
}

export interface OverlapHook {
  (overlapOptions: OverlapOptions): void;
}

const useOverlap: OverlapHook = ({
  popperContainer,
  popperInstance,
  opened
}) => {
  useEffect(() => {
    if (!opened) return;

    const isTooltip =
      popperContainer?.className.indexOf('dls-tooltip-container') !== -1;
    if (!isTooltip) return;

    const beforePopupElement = popperContainer?.previousSibling as HTMLElement;
    if (!beforePopupElement) return;

    const mutationObserver = new MutationObserver(() => {
      const beforePlacement = beforePopupElement
        .getAttribute('data-popper-placement')
        ?.replace('-start', '')
        .replace('-end', '');

      const nextPopperPlacement =
        beforePlacement === 'top' ? 'bottom-start' : 'top-start';
      popperInstance.setOptions({ placement: nextPopperPlacement });
    });

    mutationObserver.observe(beforePopupElement, { attributes: true });
    return () => mutationObserver.disconnect();
  }, [opened, popperContainer, popperInstance]);
};

export default useOverlap;
