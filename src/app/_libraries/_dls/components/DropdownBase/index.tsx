import React, {
  useRef,
  useImperativeHandle,
  useState,
  useEffect,
  useMemo,
  isValidElement
} from 'react';

// types
import {
  DropdownBaseRef,
  DropdownBaseProps,
  DropdownBaseItemProps,
  DropdownBaseItemDetailProps,
  DropdownBaseGroupProps,
  DropdownBaseChangeEvent,
  DropdownBaseDirection,
  DropdownBaseCore,
  DropdownBaseValueSingle,
  DropdownBaseValueMultiple,
  DropdownBaseChangeBy,
  DropdownBaseHoverValue,
  DropdownBaseItemInfo
} from './types';

// utils
import classnames from 'classnames';
import { isFunction, isEmpty, isUndefined, isNumber, get } from '../../lodash';
import { className, genAmtId, JSONStringify, keycode } from '../../utils';

// hooks
import { useScrollOnNavigation } from '../../hooks';
import { useInfinityScroll } from './hooks';
import useNaiveVirtualScroll from '../VirtualScroll/hooks/useNaiveVirtualScroll';

// components
import Item from './Item';
import Group from './Group';
import ItemAll from './ItemAll';
import SearchBar, { SearchBarRef } from './SearchBar';

const DropdownBase: React.RefForwardingComponent<
  DropdownBaseRef,
  DropdownBaseProps
> = (
  {
    id,
    name,

    value: valueProp,
    onChange,
    allowCallOnChangeByUpDown = false,
    allowCallOnChangeByClick = false,
    allowCallOnChangeByEnter = false,

    valueType = 'single',

    checkAll,
    checkAllLabel = 'All',
    searchBar,
    searchBarPlaceholder,
    maxLengthFilter,
    filter = '',
    onFilter,
    baseChildren,
    onFocus,
    onBlur,
    handleOpenedMode,

    className: classNameProp,
    margin = 0,
    marginByItems = 0,
    defaultDirection = 'bottom',
    allowKeydown = false,
    highlightTextItem = false,
    truncateTooltip = false,
    noResult = 'No results found',

    virtualScroll,
    onInfinityScroll,
    loadingInfinityScroll,

    children: childrenProp,

    dataTestId
  },
  ref
) => {
  // refs
  const containerRef = useRef<HTMLDivElement | null>(null);
  const scrollContainerRef = useRef<HTMLDivElement | null>(null);
  const searchBarRef = useRef<SearchBarRef | null>(null);
  const directionRef = useRef<DropdownBaseDirection>(defaultDirection);
  const coreRef = useRef<DropdownBaseCore>({} as DropdownBaseCore);

  // states
  const [valueSingle, setValueSingle] =
    useState<DropdownBaseValueSingle>(valueProp);
  const [valueMultiple, setValueMultiple] =
    useState<DropdownBaseValueMultiple>(valueProp);
  const [hoverValue, setHoverValue] = useState<DropdownBaseHoverValue>(() => {
    if (valueType !== 'new-behavior') return;
    return valueProp;
  });

  // get index available, supports for keyboard up/down for disabled item
  const getIndexAvailable = (
    index: number,
    items: DropdownBaseCore['items'],
    direction: DropdownBaseDirection
  ): number => {
    let item = items[index];
    const step = direction === 'bottom' ? 1 : -1;

    while (!item || item.props.disabled || item.props.hidden) {
      index = index + 1 * step;
      item = items[index];

      if (index > items.length - 1) {
        index = items.length - 1;
        break;
      }

      if (index < 0) {
        index = 0;
        break;
      }
    }

    item = items[index];
    if (!item || item.props.disabled || item.props.hidden) {
      return -1;
    }

    return index;
  };

  const shouldShowCheckAll = () => {
    if (checkAll && !filter) return true;

    if (
      checkAll &&
      checkAllLabel.toLowerCase().indexOf(filter.toLowerCase()) !== -1
    ) {
      return true;
    }

    return false;
  };

  // handle onChange uncontrolled/controlled mode single
  const handleOnChangeModeSingle = (
    nextValueSingle: DropdownBaseValueSingle,
    changeBy?: DropdownBaseChangeBy,
    itemInfo?: DropdownBaseItemInfo
  ) => {
    const nextEvent = {
      isTrusted: true,
      bubbles: false,
      cancelable: false,
      changeBy,
      target: {
        value: nextValueSingle,
        name,
        id
      },
      value: nextValueSingle,
      itemInfo
    } as DropdownBaseChangeEvent;

    const shouldBecomeControlled =
      (changeBy === 'up/down' && allowCallOnChangeByUpDown) ||
      (changeBy === 'click' && allowCallOnChangeByClick) ||
      (changeBy === 'enter' && allowCallOnChangeByEnter);

    // controlled mode
    if (shouldBecomeControlled) {
      onChange?.(nextEvent);
      if (!isUndefined(valueProp)) return;
    }

    // uncontrolled mode
    setValueSingle(nextValueSingle);
  };

  // handle onChange uncontrolled/controlled mode multiple
  const handleOnChangeModeMultiple = (
    nextValueMultiple: DropdownBaseValueMultiple
  ) => {
    const nextEvent = {
      isTrusted: true,
      bubbles: false,
      cancelable: false,
      target: {
        value: nextValueMultiple,
        name,
        id
      },
      value: nextValueMultiple
    } as DropdownBaseChangeEvent;

    isFunction(onChange) && onChange(nextEvent);

    // controlled mode
    if (!isUndefined(valueProp)) return;

    // uncontrolled mode
    setValueMultiple(nextValueMultiple);
  };

  // keep references
  coreRef.current.handleOnChangeModeSingle = handleOnChangeModeSingle;
  coreRef.current.handleOnChangeModeMultiple = handleOnChangeModeMultiple;
  coreRef.current.valueSingle = valueSingle;
  coreRef.current.valueMultiple = valueMultiple;
  coreRef.current.valueType = valueType;
  coreRef.current.hoverValue = hoverValue;
  coreRef.current.setHoverValue = setHoverValue;
  coreRef.current.checkAll = checkAll;
  coreRef.current.baseChildren = baseChildren;
  coreRef.current.shouldShowCheckAll = shouldShowCheckAll;

  // enhance DropdownBase.Item
  const enhanceItemElements = (
    originItemElements:
      | React.ReactElement<DropdownBaseItemProps, typeof Item>
      | React.ReactElement<DropdownBaseItemProps, typeof Item>[],
    originValueSingle: DropdownBaseValueSingle,
    originValueMultiple: DropdownBaseValueMultiple,
    originHoverValue: DropdownBaseHoverValue,
    originCheckAll?: boolean,
    originCheckAllLength?: number
  ) => {
    const itemElements = React.Children.map(originItemElements, (child) => {
      // extract item props
      const { className: itemClassName, ...childProps } = child.props;

      let isSelected, isHover;
      let indeterminate;

      // valueType new-behavior
      if (valueType === 'new-behavior') {
        isSelected =
          childProps.value === originValueSingle &&
          !childProps.disabled &&
          !childProps.hidden;
        isHover =
          childProps.value === originHoverValue &&
          !childProps.disabled &&
          !childProps.hidden;
      }

      // valueType single
      if (valueType === 'single') {
        isSelected =
          childProps.value === originValueSingle &&
          !childProps.disabled &&
          !childProps.hidden;
      }

      // valueType multiple
      if (valueType === 'multiple') {
        // we need Boolean constructor here,
        // because isSelected can become undefined
        // checked from undefined to boolean (true or false) will
        // trigger react warning should not uncontrolled to controlled
        isSelected = Boolean(
          originValueMultiple?.some((value) => value === childProps.value)
        );
        isHover =
          childProps.value === originHoverValue &&
          !childProps.disabled &&
          !childProps.hidden;
      }

      // valueType multiple and have checkAll
      if (
        originCheckAll &&
        isNumber(originCheckAllLength) &&
        valueType === 'multiple'
      ) {
        if (
          originValueMultiple?.length &&
          originCheckAllLength !== originValueMultiple?.length
        ) {
          indeterminate = true;
        }

        isSelected = Boolean(
          originCheckAllLength === originValueMultiple?.length
        );
      }

      // make new item props
      const nextProps = {
        className: classnames(
          isSelected && className.state.SELECTED,
          isHover && className.state.HOVER,
          itemClassName
        ),
        coreRef,
        highlightTextItem,
        truncateTooltip,
        checked: isSelected,
        indeterminate,
        dataTestId: genAmtId(
          `${childProps.label}-${dataTestId}`,
          'dropdown-base-item',
          'DropdownBase.Item'
        ),
        ...childProps
      } as DropdownBaseItemDetailProps;

      // clone item with new item props
      return React.cloneElement(child, nextProps);
    });

    return itemElements;
  };

  // enhance DropdownBase.Group
  const enhanceGroupElement = (
    originGroupElement: React.ReactElement<
      DropdownBaseGroupProps,
      typeof Group
    >,
    originValueSingleType: any,
    originValueMultipleType: any[],
    originHoverValue: any
  ) => {
    // collector item elements for each group element
    const itemElements: React.FunctionComponentElement<DropdownBaseItemProps>[] =
      [];

    // we need convert groupElement to [groupElement], so we use React.Children.map
    const groupElements = React.Children.map(originGroupElement, (child) => {
      const nextItemElements = enhanceItemElements(
        child.props.children as
          | React.ReactElement<DropdownBaseItemProps, typeof Item>
          | React.ReactElement<DropdownBaseItemProps, typeof Item>[],
        originValueSingleType,
        originValueMultipleType,
        originHoverValue
      );

      itemElements.push(...nextItemElements);

      const nextProps = {
        ...child.props,
        children: nextItemElements
      } as typeof child.props;

      return React.cloneElement(child, nextProps);
    });

    return { groupElements, itemElements };
  };

  // map children
  const children = useMemo(() => {
    // reset list for new cycle
    coreRef.current.items = [];

    // if we have checkAll, we will prepend checkAll element at the first list
    let checkAllElement: any;
    if (coreRef.current.shouldShowCheckAll()) {
      checkAllElement = enhanceItemElements(
        (
          <ItemAll
            key="PP-M-CheckAll"
            label={checkAllLabel}
            value="PP-M-CheckAll"
            variant="checkbox"
            dataTestId={genAmtId(
              `All-${dataTestId}`,
              'dropdown-base-item',
              'DropdownBase.Item'
            )}
          />
        ) as React.ReactElement<DropdownBaseItemProps, typeof Item>,
        valueSingle,
        valueMultiple,
        hoverValue,
        checkAll,
        React.Children.count(baseChildren)
      );
      coreRef.current.items.push(...checkAllElement);
    }

    const childList = React.Children.map(childrenProp, (child, index) => {
      if (!React.isValidElement(child)) return child;

      if (child.type !== Group && child.type !== Item) {
        throw Error(
          '[DLS] - children should be of type inherit from "DropdownBase.Group" or "DropdownBase.Item"'
        );
      }

      // enhance DropdownBase.Group and collect itemElements
      if (child.type === Group) {
        const {
          groupElements,
          // sonar force avoid duplicate name
          itemElements: itemElementsRenamed
        } = enhanceGroupElement(
          child as React.ReactElement<DropdownBaseGroupProps, typeof Group>,
          valueSingle,
          valueMultiple,
          hoverValue
        );
        coreRef.current.items.push(...itemElementsRenamed);

        return index === 0 && checkAllElement
          ? [checkAllElement, ...groupElements]
          : groupElements;
      }

      // enhance DropdownBase.Item and collect itemElement
      const itemElements = enhanceItemElements(
        child as React.ReactElement<DropdownBaseItemProps, typeof Item>,
        valueSingle,
        valueMultiple,
        hoverValue
      );
      coreRef.current.items.push(...itemElements);
      return index === 0 && checkAllElement
        ? [checkAllElement, ...itemElements]
        : itemElements;
    });

    // if childList.length is 0, we need double check again with checkAll
    if (childList.length === 0 && checkAllElement) {
      return [checkAllElement];
    }

    return childList;
    // update 04/27/2021, we need hoverValue to trigger
    // re-render enhanceGroupElement/enhanceItemElements
    // ----
    // eslint is wrong, we don't need enhanceGroupElement/enhanceItemElements as dependencies
    // if you are updating eslint and eslint react hook version,
    // please remove this eslint disabled and try again
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [
    childrenProp,
    valueSingle,
    valueMultiple,
    hoverValue,
    checkAll,
    checkAllLabel,
    baseChildren
  ]);

  // use virtual scroll
  const [scrollableRef, totalHeight, from, to, translateY] =
    useNaiveVirtualScroll(children.length, 36, 15);

  const scrollRef = virtualScroll ? scrollableRef : scrollContainerRef;

  // provide features to ref
  useImperativeHandle(
    ref,
    () => ({
      containerElement: containerRef.current,
      scrollContainerElement: scrollRef.current,
      searchBarElement: searchBarRef.current as HTMLElement
    }),
    [scrollRef]
  );

  // set props
  useEffect(() => {
    switch (valueType) {
      case 'single':
        setValueSingle(valueProp);
        break;
      case 'multiple':
        setValueMultiple(valueProp);
        break;
      case 'new-behavior':
        setValueSingle(valueProp);
        setHoverValue(valueProp);
        break;
    }
  }, [valueProp, valueType]);

  // handle provides keyboard Up/Down to DropdownBase (valueType single)
  useEffect(() => {
    if (valueType !== 'single') return;

    const handleOnKeyDown = (event: KeyboardEvent) => {
      if (!allowKeydown) return;

      const { keyCode } = event;
      if (keyCode !== keycode.DOWN && keyCode !== keycode.UP) return;

      // we need to know index of the current value in itemsCollectorRef.current
      const index = coreRef.current.items.findIndex((item) => {
        return item.props.value === valueSingle;
      });

      // we need to calculate the next index
      let nextIndex;

      // keyboard Up/Down default will trigger scroll browser
      // prevent it
      event.preventDefault();

      switch (keyCode) {
        case keycode.DOWN:
          directionRef.current = 'bottom';
          nextIndex = getIndexAvailable(
            index + 1,
            coreRef.current.items,
            directionRef.current
          );
          break;
        case keycode.UP:
        default:
          directionRef.current = 'top';
          nextIndex = getIndexAvailable(
            index - 1,
            coreRef.current.items,
            directionRef.current
          );
      }

      const nextItem = coreRef.current.items[nextIndex];

      // itemsCollectorRef.current will be empty array if we don't have any items matching the filter
      if (!nextItem) return;

      coreRef.current.handleOnChangeModeSingle(
        nextItem.props.value,
        'up/down',
        {
          variant: nextItem.props.variant,
          badgeProps: nextItem.props.badgeProps,
          textBadgeProps: nextItem.props.textBadgeProps,
          bubbleProps: nextItem.props.bubbleProps,
          textProps: nextItem.props.textProps
        }
      );
    };

    window.addEventListener('keydown', handleOnKeyDown);
    return () => window.removeEventListener('keydown', handleOnKeyDown);
  }, [allowKeydown, valueSingle, valueType]);

  // handle provides keyboard Up/Down to DropdownBase (valueType multiple)
  useEffect(() => {
    if (valueType !== 'multiple') return;

    const handleOnKeyDown = (event: KeyboardEvent) => {
      if (!allowKeydown) return;

      const { keyCode } = event;
      if (keyCode !== keycode.DOWN && keyCode !== keycode.UP) return;

      // we need to know index of the current value in itemsCollectorRef.current
      const index = coreRef.current.items.findIndex((item) => {
        return item.props.value === hoverValue;
      });

      // we need to calculate the next index
      let nextIndex;

      // keyboard Up/Down default will be trigger scroll browser
      // prevent it
      event.preventDefault();

      switch (keyCode) {
        case keycode.DOWN:
          directionRef.current = 'bottom';
          nextIndex = getIndexAvailable(
            index + 1,
            coreRef.current.items,
            directionRef.current
          );
          break;
        case keycode.UP:
        default:
          directionRef.current = 'top';
          nextIndex = getIndexAvailable(
            index - 1,
            coreRef.current.items,
            directionRef.current
          );
      }

      const nextItem = coreRef.current.items[nextIndex];

      // itemsCollectorRef.current will be empty array if we don't have any items matching the filter
      if (!nextItem) return;

      setHoverValue(nextItem.props.value);
    };

    window.addEventListener('keydown', handleOnKeyDown);
    return () => window.removeEventListener('keydown', handleOnKeyDown);
  }, [allowKeydown, hoverValue, valueType]);

  // handle provides keyboard Up/Down to DropdownBase (valueType new-behavior)
  useEffect(() => {
    if (valueType !== 'new-behavior') return;

    const handleOnKeyDown = (event: KeyboardEvent) => {
      if (!allowKeydown) return;

      const { keyCode } = event;
      if (keyCode !== keycode.DOWN && keyCode !== keycode.UP) return;

      // we need to know index of the hover value in itemsCollectorRef.current
      const index = coreRef.current.items.findIndex((item) => {
        return item.props.value === hoverValue;
      });

      // we need to calculate the next index
      let nextIndex;

      // keyboard Up/Down default will trigger scroll browser
      // prevent it
      event.preventDefault();

      switch (keyCode) {
        case keycode.DOWN:
          directionRef.current = 'bottom';
          nextIndex = getIndexAvailable(
            index + 1,
            coreRef.current.items,
            directionRef.current
          );
          break;
        case keycode.UP:
        default:
          directionRef.current = 'top';
          nextIndex = getIndexAvailable(
            index - 1,
            coreRef.current.items,
            directionRef.current
          );
      }

      const nextItem = coreRef.current.items[nextIndex];

      // itemsCollectorRef.current will be empty array if we don't have any items matching the filter
      nextItem && setHoverValue(nextItem.props.value);
    };

    window.addEventListener('keydown', handleOnKeyDown);
    return () => window.removeEventListener('keydown', handleOnKeyDown);
  }, [allowKeydown, hoverValue, valueType]);

  // handle provides keyboard Enter to DropdownBase (valueType single)
  useEffect(() => {
    if (valueType !== 'single') return;

    const handleKeydownEnter = (event: KeyboardEvent) => {
      if (!allowKeydown) return;

      const { keyCode } = event;
      if (keyCode !== keycode.ENTER) return;

      const index = coreRef.current.items.findIndex((item) => {
        return item.props.value === valueSingle;
      });

      const item = coreRef.current.items[index];

      coreRef.current.handleOnChangeModeSingle(valueSingle, 'enter', {
        variant: item.props.variant,
        badgeProps: item.props.badgeProps,
        textBadgeProps: item.props.textBadgeProps,
        bubbleProps: item.props.bubbleProps,
        textProps: item.props.textProps
      });
    };

    window.addEventListener('keydown', handleKeydownEnter);
    return () => window.removeEventListener('keydown', handleKeydownEnter);
  }, [allowKeydown, valueSingle, valueType]);

  // handle provides keyboard Enter to DropdownBase (valueType multiple)
  useEffect(() => {
    if (valueType !== 'multiple') return;

    const handleKeydownEnter = (event: KeyboardEvent) => {
      if (!allowKeydown) return;

      const { keyCode } = event;
      if (keyCode !== keycode.ENTER) return;

      // if hoverValue is not exists, we don't have anything to check
      if (!hoverValue) return;

      // if hoverValue is not contained in coreRef.current.items, we don't have anything to check also
      const shouldContinue = coreRef.current.items.some((item) => {
        const value = get(item, ['props', 'value']);
        return value === hoverValue;
      });
      if (!shouldContinue) return;

      if (checkAll && hoverValue === 'PP-M-CheckAll') {
        const baseChildrenLength = React.Children.count(baseChildren);
        const isSelectedAll = baseChildrenLength === valueMultiple?.length;

        // TODO
        // WILL WRONG IF WE HAVE OTHER GROUP ELEMENT
        const nextAllValues = React.Children.map(baseChildren, (child) => {
          return get(child, ['props', 'value']);
        });

        coreRef.current.handleOnChangeModeMultiple(
          isSelectedAll ? [] : nextAllValues!
        );
        return;
      }

      // calculate next valueMultiple based on current valueMultiple and hoverValue
      let nextValues;
      const isExists = valueMultiple?.some((value) => value === hoverValue);
      if (isExists) {
        nextValues = valueMultiple.filter((value) => value !== hoverValue);
      } else {
        nextValues = [...(valueMultiple || []), hoverValue];
      }

      coreRef.current.handleOnChangeModeMultiple(nextValues);
    };

    window.addEventListener('keydown', handleKeydownEnter);
    return () => window.removeEventListener('keydown', handleKeydownEnter);
  }, [
    allowKeydown,
    valueMultiple,
    hoverValue,
    valueType,
    baseChildren,
    checkAll
  ]);

  // handle provides keyboard Enter to DropdownBase (valueType new-behavior)
  useEffect(() => {
    if (valueType !== 'new-behavior') return;

    const handleKeydownEnter = (event: KeyboardEvent) => {
      if (!allowKeydown) return;

      const { keyCode } = event;
      if (keyCode !== keycode.ENTER) return;

      const index = coreRef.current.items.findIndex((item) => {
        return item.props.value === hoverValue;
      });

      const item = coreRef.current.items[index];

      coreRef.current.handleOnChangeModeSingle(hoverValue, 'enter', {
        variant: item.props.variant,
        badgeProps: item.props.badgeProps,
        textBadgeProps: item.props.textBadgeProps,
        bubbleProps: item.props.bubbleProps,
        textProps: item.props.textProps
      });
    };

    window.addEventListener('keydown', handleKeydownEnter);
    return () => window.removeEventListener('keydown', handleKeydownEnter);
  }, [allowKeydown, hoverValue, valueType]);

  // handle provides new-behavior (set hoverValue to the item matching first character)
  useEffect(() => {
    if (valueType !== 'new-behavior') return;

    const search = (character: string) => {
      const { hoverValue } = coreRef.current;

      const matchedItems = coreRef.current.items.filter((item) => {
        const firstCharacter = get(item, ['props', 'label', '0'], '') as string;

        return character.toLowerCase() === firstCharacter.toLowerCase();
      });

      if (isEmpty(matchedItems)) return;

      const currentIndex = matchedItems
        .map((item) => item.props.value)
        .indexOf(hoverValue);
      const isLast = currentIndex === matchedItems.length - 1;

      const nextIndex = isLast ? 0 : currentIndex + 1;

      setHoverValue(matchedItems[nextIndex].props.value);
    };

    const handleKeydown = (event: KeyboardEvent) => {
      const { key } = event;
      if (key.length > 1) return;

      search(key);
    };

    window.addEventListener('keydown', handleKeydown);
    return () => window.removeEventListener('keydown', handleKeydown);
  }, [valueType]);

  // add scroll on navigation feature to scrollRef
  useScrollOnNavigation(
    scrollRef,
    directionRef,
    (target) => {
      let lookupClassName;
      switch (valueType) {
        case 'single':
          lookupClassName = className.state.SELECTED;
          break;
        case 'multiple':
          lookupClassName = className.state.HOVER;
          break;
        case 'new-behavior':
          lookupClassName = className.state.HOVER;
          break;
      }

      return target.classList.contains(lookupClassName);
    },
    margin,
    marginByItems
  );

  const dropdownbaseScrollContainer = () => {
    const noResultElement = isValidElement(noResult) ? (
      noResult
    ) : (
      <span
        className="dls-no-result-found"
        data-testid={genAmtId(
          dataTestId,
          'dls-dropdown-base-no-result',
          'DropdownBase.NoResult'
        )}
      >
        {noResult}
      </span>
    );

    const loadingInfinityScrollElement = loadingInfinityScroll ? (
      <div className="d-flex align-items-center justify-content-center fs-14 py-8">
        <span className="loading loading-md mr-24" />
        <span>Loading more...</span>
      </div>
    ) : null;

    const virtualScrollElement = (
      <div style={{ height: `${totalHeight}px` }}>
        <div
          style={{
            transform: `translateY(${translateY}px)`
          }}
        >
          {children.slice(from, to)}
          {loadingInfinityScrollElement}
        </div>
      </div>
    );

    const scrollContainerElement = (
      <div
        ref={scrollRef}
        className={classnames(className.dropdownBase.SCROLL_CONTAINER)}
        tabIndex={-1}
        popup-base-direction={directionRef.current}
      >
        {!coreRef.current.shouldShowCheckAll() && isEmpty(children)
          ? noResultElement
          : virtualScroll
          ? virtualScrollElement
          : children}
        {!virtualScroll ? loadingInfinityScrollElement : null}
      </div>
    );

    return scrollContainerElement;
  };

  // use infinity scroll
  useInfinityScroll(children, scrollRef, onInfinityScroll);

  const valuePropStringify = JSONStringify(valueProp);

  return (
    <div
      ref={containerRef}
      className={classnames(className.dropdownBase.CONTAINER, classNameProp)}
      controlled-item={valuePropStringify}
      data-testid={genAmtId(
        dataTestId,
        'dls-dropdown-base-container',
        'DropdownBase.Container'
      )}
    >
      {searchBar ? (
        <SearchBar
          ref={searchBarRef}
          onChange={(event) => onFilter?.(event.target.value)}
          valueDropdownBase={valueProp}
          onFocus={onFocus}
          onBlur={onBlur}
          handleOpenedMode={handleOpenedMode}
          maxLengthFilter={maxLengthFilter}
          placeholder={searchBarPlaceholder}
          id={id}
          name={name}
          data-testid={genAmtId(
            dataTestId,
            'dls-dropdown-base-search-bar',
            'DropdownBase.SearchBar'
          )}
        />
      ) : null}
      {dropdownbaseScrollContainer()}
    </div>
  );
};

const DropdownBaseForwardedRef = React.forwardRef<
  DropdownBaseRef,
  DropdownBaseProps
>(DropdownBase);

const DropdownBaseExtraStaticProp = React.memo(
  DropdownBaseForwardedRef
) as React.MemoExoticComponent<
  React.ForwardRefExoticComponent<
    DropdownBaseProps & React.RefAttributes<DropdownBaseRef>
  >
> & {
  Group: typeof Group;
  Item: typeof Item;
};

DropdownBaseExtraStaticProp.Group = Group;
DropdownBaseExtraStaticProp.Item = Item;

export * from './types';
export * from './contexts';
export { Item as DropdownBaseItem, Group as DropdownBaseGroup };
export default DropdownBaseExtraStaticProp;
