import React from 'react';
import {
  fireEvent,
  render,
  RenderResult,
  screen
} from '@testing-library/react';
import '@testing-library/jest-dom';

import SearchBar, { SearchBarProps } from './SearchBar';
import { queryByClass } from '../../test-utils/queryHelpers';
import { act } from 'react-dom/test-utils';
import userEvent from '@testing-library/user-event';
import { set } from '../../lodash';

let wrapper: RenderResult;

const getNode = {
  get wrapper() {
    return queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-dropdown-base-search-bar/
    );
  },
  get input() {
    return screen.getByRole('textbox');
  }
};

const renderComponent = (props: SearchBarProps) => {
  jest.useFakeTimers();

  wrapper = render(
    <div className="dls-popup" data-testid="dls-popup">
      <SearchBar ref={{ current: null }} {...props} />
    </div>
  );

  jest.runAllTimers();
};

const userClick = (element: HTMLElement) => {
  act(() => {
    userEvent.click(element);
    jest.runAllTimers();
  });
};

describe('render', () => {
  it('should render', () => {
    renderComponent({
      valueDropdownBase: [],
      handleOpenedMode: jest.fn()
    });

    expect(getNode.wrapper).toBeInTheDocument();
  });
});

describe('Actions', () => {
  it('onChange', () => {
    const onChange = jest.fn();

    renderComponent({
      valueDropdownBase: [],
      handleOpenedMode: jest.fn(),
      onChange
    });

    fireEvent.change(getNode.input, { target: { value: 'value' } });

    userClick(document.body);

    expect(onChange).toBeCalled();
  });

  it('onChange with maxLengthFilter', () => {
    const onChange = jest.fn();

    renderComponent({
      valueDropdownBase: [],
      handleOpenedMode: jest.fn(),
      onChange,
      maxLengthFilter: 6
    });

    fireEvent.change(getNode.input, { target: { value: 'value' } });
    userClick(document.body);
    expect(onChange).toBeCalled();
    onChange.mockReset();

    fireEvent.change(getNode.input, { target: { value: 'value123' } });
    expect(onChange).not.toBeCalled();
  });

  it('onPaste with maxLengthFilter', () => {
    const onPaste = jest.fn();

    const pasteEvent = new Event('paste', {
      bubbles: true,
      cancelable: true,
      composed: true
    });
    set(pasteEvent, 'clipboardData', {
      getData: () => 'value'
    });

    renderComponent({
      valueDropdownBase: [],
      handleOpenedMode: jest.fn(),
      onPaste,
      maxLengthFilter: 6
    });

    getNode.input.dispatchEvent(pasteEvent);
    expect(onPaste).toBeCalled();
    onPaste.mockReset();

    set(pasteEvent, 'clipboardData', {
      getData: () => 'value123'
    });
    getNode.input.dispatchEvent(pasteEvent);
    expect(onPaste).not.toBeCalled();
  });

  it('onBlur', () => {
    const onFocus = jest.fn();
    const onBlur = jest.fn();

    renderComponent({
      valueDropdownBase: [],
      handleOpenedMode: jest.fn(),
      onFocus,
      onBlur
    });

    userClick(getNode.input);

    fireEvent.blur(getNode.input);

    expect(onBlur).toBeCalled();
  });

  it('click on popup', () => {
    const onFocus = jest.fn();
    const onBlur = jest.fn();

    renderComponent({
      valueDropdownBase: [],
      handleOpenedMode: jest.fn(),
      onFocus,
      onBlur
    });

    userClick(getNode.input);

    userClick(screen.getByTestId('dls-popup'));

    expect(onBlur).not.toBeCalled();
  });
});
