import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

import HighLightWords from './HighLightWords';

describe('render', () => {
  it('should render', () => {
    const textToHighlight = 'text to hight light';
    render(<HighLightWords textToHighlight={textToHighlight} />);

    expect(screen.getByText(textToHighlight)).toBeInTheDocument();
  });
});
