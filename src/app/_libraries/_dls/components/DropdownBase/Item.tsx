import React, {
  useMemo,
  useRef,
  useImperativeHandle,
  RefForwardingComponent,
  forwardRef,
  useEffect,
  Fragment
} from 'react';

// utils
import classnames from 'classnames';
import { classes, className, genAmtId, truncateTooltip } from '../../utils';

// components/types
import Bubble from '../Bubble';
import Badge from '../Badge';
import HighLightWords from './HighLightWords';
import CheckBox from '../CheckBox';
import {
  DropdownBaseItemRef,
  DropdownBaseItemProps,
  DropdownBaseItemDetailProps
} from './types';
import Tooltip from '../Tooltip';

// hooks
import { useDeepValue } from '../../hooks';

const Item: RefForwardingComponent<
  DropdownBaseItemRef,
  DropdownBaseItemProps
> = (
  {
    variant,
    disabled,
    hidden,
    label,
    className: classNameProp,
    badgeProps,
    textBadgeProps,
    bubbleProps,
    textProps,
    tooltipProps,
    value,
    children,

    // internal props
    coreRef,
    highlightTextItem,
    truncateTooltip: truncateTooltipProp,
    checked,
    indeterminate,

    dataTestId
  }: DropdownBaseItemDetailProps,
  ref
) => {
  // refs
  const itemRef = useRef<HTMLDivElement | null>(null);

  const nextBubbleProps = useDeepValue(bubbleProps);
  const nextBadgeProps = useDeepValue(badgeProps);
  const nextTextBadgeProps = useDeepValue(textBadgeProps);
  const nextTextProps = useDeepValue(textProps);

  // provide ref
  useImperativeHandle(
    ref,
    () => ({ element: itemRef.current as HTMLElement }),
    []
  );

  // handle render variant
  const variantOfElement = useMemo(() => {
    const labelShouldRendered = highlightTextItem ? (
      <HighLightWords
        textToHighlight={label!}
        className="label"
        dataTestId={dataTestId}
      />
    ) : (
      <span
        className={classnames('label', classes.utils.ellipsis)}
        data-testid={genAmtId(dataTestId, 'text', '')}
      >
        {label}
      </span>
    );

    switch (variant) {
      case 'text-badge':
        return {
          element: (
            <>
              {labelShouldRendered}
              <Badge dataTestId={dataTestId} {...nextTextBadgeProps} />
            </>
          ),
          className: 'item-text-badge'
        };
      case 'badge':
        return {
          element: <Badge dataTestId={dataTestId} {...nextBadgeProps} />,
          className: 'item-badge'
        };
      case 'bubble':
        return {
          element: (
            <>
              <Bubble
                name={label}
                dataTestId={dataTestId}
                {...nextBubbleProps}
              />
              {labelShouldRendered}
            </>
          ),
          className: 'item-bubble'
        };
      case 'text':
        return {
          element: (
            <>
              {labelShouldRendered}
              <span {...nextTextProps} />
            </>
          ),
          className: 'item-text'
        };
      case 'checkbox':
        return {
          element: (
            <CheckBox dataTestId={dataTestId} autoGenId={false}>
              <CheckBox.Input
                indeterminate={indeterminate}
                checked={checked}
                onChange={() => null}
                disabled={disabled}
              />
              <CheckBox.Label>{labelShouldRendered}</CheckBox.Label>
            </CheckBox>
          ),
          className: 'item-checkbox'
        };
      case 'custom':
        return {
          element: children
        };
      default:
        return {
          element: labelShouldRendered,
          className: ''
        };
    }
    // risk dependencies (inline object will create new reference for each render on the parentElement,
    // we will received new reference on this
    // so this useMemo will rerunning every time)
    // need refactor
  }, [
    highlightTextItem,
    label,
    variant,
    nextBadgeProps,
    nextTextBadgeProps,
    nextBubbleProps,
    nextTextProps,
    indeterminate,
    checked,
    disabled,
    children,
    dataTestId
  ]);

  const handleOnClickItem = (event: React.MouseEvent) => {
    event.stopPropagation();

    if (disabled || hidden) return;

    const {
      valueType,
      valueMultiple,
      setHoverValue,
      handleOnChangeModeSingle,
      handleOnChangeModeMultiple,
      checkAll,
      baseChildren
    } = coreRef!.current;

    if (valueType === 'single') {
      handleOnChangeModeSingle(value, 'click', {
        variant,
        badgeProps,
        textBadgeProps,
        bubbleProps,
        textProps
      });
    }

    if (valueType === 'multiple') {
      if (checkAll && value === 'PP-M-CheckAll') {
        const baseChildrenLength = React.Children.count(baseChildren);
        const isAllValuesSelected =
          baseChildrenLength === valueMultiple?.length;

        // WILL WRONG IF WE HAVE OTHER GROUP ELEMENT
        const nextAllValues = React.Children.map(baseChildren, (child) => {
          return child.props.value;
        });

        setHoverValue(value);
        handleOnChangeModeMultiple(isAllValuesSelected ? [] : nextAllValues);
        return;
      }

      const isExists = valueMultiple?.some(
        (valueFromRef) => valueFromRef === value
      );

      const nextValues = isExists
        ? valueMultiple?.filter((valueFromRef) => valueFromRef !== value)
        : [...(valueMultiple || []), value];

      setHoverValue(value);
      handleOnChangeModeMultiple(nextValues);
    }

    if (valueType === 'new-behavior') {
      handleOnChangeModeSingle(value, 'click', {
        variant,
        badgeProps,
        textBadgeProps,
        bubbleProps,
        textProps
      });
      setHoverValue(value);
    }
  };

  // handle truncate tooltip
  useEffect(() => {
    if (!truncateTooltipProp || variant === 'custom') return;

    truncateTooltip(itemRef.current!, true);
  }, [truncateTooltipProp, variant]);

  const wrapper: { Component?: React.ReactType; props?: any } = {};
  if (tooltipProps) {
    wrapper.Component = Tooltip;
    wrapper.props = {
      triggerClassName: 'd-block',
      ...tooltipProps
    };
  } else {
    wrapper.Component = Fragment;
  }

  return (
    <wrapper.Component {...wrapper.props}>
      <div
        ref={itemRef}
        onClick={handleOnClickItem}
        className={classnames(
          {
            [className.dropdownBase.ITEM]: true,
            [className.state.DISABLED]: disabled,
            [className.state.HIDDEN]: hidden
          },
          variantOfElement.className,
          classNameProp
        )}
      >
        {variantOfElement?.element}
      </div>
    </wrapper.Component>
  );
};

const ItemForwardedRef = forwardRef<DropdownBaseItemRef, DropdownBaseItemProps>(
  Item
);

ItemForwardedRef.displayName = 'DropdownBase.Item';
export default React.memo(ItemForwardedRef);
