import { useEffect, useRef } from 'react';

// utils
import { classes } from '../../../utils';

export interface InfinityScrollHook {
  (
    children: React.ReactNode,
    scrollRef: React.MutableRefObject<HTMLElement | null>,
    callback?: () => void
  ): void;
}

const useInfinityScroll: InfinityScrollHook = (
  children,
  scrollRef,
  callback
) => {
  const callbackRef = useRef<typeof callback>();
  callbackRef.current = callback;

  useEffect(() => {
    const scrollContainerElement = scrollRef.current!;
    const parent = scrollContainerElement.closest(
      `.${classes.popupBase.container}`
    ) as HTMLElement;

    // set display to default so the browser can calculate scrollHeight/clientHeight
    const previousVisibility = parent.style.visibility;
    const previousDisplay = parent.style.display;
    parent.style.visibility = 'hidden';
    parent.style.display = '';
    const scrollHeight = scrollContainerElement.scrollHeight;
    const clientHeight = scrollContainerElement.clientHeight;
    parent.style.visibility = previousVisibility;
    parent.style.display = previousDisplay;

    // no vertical scroll
    if (scrollHeight <= clientHeight) return;

    const lastElement = scrollContainerElement.lastChild as HTMLElement;
    if (!lastElement) return;

    const handleIntersection: IntersectionObserverCallback = (records) => {
      const { isIntersecting } = records[0];
      if (!isIntersecting) return;

      callbackRef.current?.();
      intersectionObserver.unobserve(lastElement);
    };

    const intersectionObserver = new IntersectionObserver(handleIntersection, {
      root: scrollContainerElement
    });

    intersectionObserver.observe(lastElement);
    return () => intersectionObserver.disconnect();
  }, [children, scrollRef]);
};

export default useInfinityScroll;
