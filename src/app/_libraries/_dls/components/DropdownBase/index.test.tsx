import React, { Fragment, useMemo } from 'react';
import {
  fireEvent,
  getByRole,
  queryByText,
  render,
  screen
} from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';
import { queryAllByClass, queryByClass } from '../../test-utils/queryHelpers';
import { act } from 'react-dom/test-utils';
import { keycode } from '../../utils';

// components
import DropdownBase, { DropdownBaseProps } from '.';
import MultiSelect from '../MultiSelect';

// mocks
jest.mock('./hooks/useInfinityScroll', () => () => undefined);

const data = [
  {
    FieldMetadataID: 5,
    FieldID: 'AAA',
    FieldValue: 'AAA',
    SequenceNumber: 0,
    RoleID: 0,
    groupName: 'checkAll',
    tooltip: 'AAA'
  },
  {
    FieldMetadataID: 5,
    FieldID: 'BBB',
    FieldValue: 'BBB',
    SequenceNumber: 0,
    RoleID: 0
  },
  {
    FieldMetadataID: 5,
    FieldID: 'CCC',
    FieldValue: 'CCC',
    SequenceNumber: 0,
    RoleID: 0
  }
];

const RenderMultiSelect = () => {
  const dropdownListItem = useMemo(() => {
    return data
      .slice(0, 7)
      .map((item, index) => (
        <MultiSelect.Item
          key={item.FieldID + index}
          label={item.FieldValue}
          value={item}
          variant="checkbox"
        />
      ));
  }, []);

  return (
    <div>
      <MultiSelect searchBar checkAll variant="group">
        {dropdownListItem}
      </MultiSelect>
    </div>
  );
};

const dataItem = [
  { value: 1, label: 'label 1' },
  { value: 2, label: 'label 2' },
  { value: 3, label: 'label 3', disabled: true },
  { value: 4, label: 'label 4' }
];

const renderComponent = (props: Omit<DropdownBaseProps, 'children'>) => {
  jest.useFakeTimers();
  const wrapper = render(
    <DropdownBase ref={{ current: null }} {...props}>
      {dataItem.map(({ value, label, disabled }) => {
        return (
          <DropdownBase.Item
            key={value}
            value={value}
            variant="checkbox"
            label={label}
            disabled={disabled}
          />
        );
      })}
    </DropdownBase>
  );

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement,
    unmount: wrapper.unmount
  };
};

const pressKey = (keyCode: number) => {
  act(() => {
    window.dispatchEvent(new KeyboardEvent('keydown', { keyCode }));
    jest.runAllTimers();
  });
};

describe('Render', () => {
  it('should single select', () => {
    const { baseElement } = renderComponent({});

    expect(
      queryByClass(baseElement, /dls-dropdown-base-container/)
    ).toBeInTheDocument();
  });

  it('should multiple select with filter and checkall', () => {
    const { baseElement } = renderComponent({
      valueType: 'multiple',
      checkAll: true,
      filter: 'All'
    });

    expect(
      queryByClass(baseElement, /dls-dropdown-base-container/)
    ).toBeInTheDocument();
  });

  it('should multiple select with checkall and value', () => {
    const { baseElement } = renderComponent({
      valueType: 'multiple',
      checkAll: true,
      value: [{}]
    });

    expect(
      queryByClass(baseElement, /dls-dropdown-base-container/)
    ).toBeInTheDocument();
  });

  it('should render null children', () => {
    const { baseElement } = render(
      <DropdownBase noResult="noResult text">
        {dataItem.map(() => {
          return null as any;
        })}
      </DropdownBase>
    );

    expect(
      queryByClass(baseElement as HTMLElement, /dls-dropdown-base-container/)
    ).toBeInTheDocument();
    expect(
      queryByClass(baseElement as HTMLElement, /dls-no-result-found/)
    ).toBeInTheDocument();
    expect(screen.getByText('noResult text')).toBeInTheDocument();
  });

  it('should render Fragment children', () => {
    try {
      render(
        <DropdownBase noResult="noResult text">
          {dataItem.map((_, index) => (
            <Fragment key={index}></Fragment>
          ))}
        </DropdownBase>
      );
    } catch (err) {
      expect(err.message).toEqual(
        '[DLS] - children should be of type inherit from "DropdownBase.Group" or "DropdownBase.Item"'
      );
    }
  });

  it('should render empty children and noResult is a text', () => {
    const { baseElement } = render(
      <DropdownBase noResult="noResult text">{[]}</DropdownBase>
    );

    expect(
      queryByClass(baseElement as HTMLElement, /dls-dropdown-base-container/)
    ).toBeInTheDocument();
    expect(
      queryByClass(baseElement as HTMLElement, /dls-no-result-found/)
    ).toBeInTheDocument();
    expect(screen.getByText('noResult text')).toBeInTheDocument();
  });

  it('should rende with empty children and check all', () => {
    const { baseElement } = render(<DropdownBase checkAll>{[]}</DropdownBase>);

    expect(
      queryByClass(baseElement as HTMLElement, /dls-dropdown-base-container/)
    ).toBeInTheDocument();
    expect(
      queryByClass(baseElement as HTMLElement, /dls-no-result-found/)
    ).toBeNull();
    expect(screen.getByText('All')).toBeInTheDocument();
  });

  it('should render empty children and noResult is element', () => {
    const { baseElement } = render(
      <DropdownBase noResult={<div>noResult element</div>}>{[]}</DropdownBase>
    );

    expect(
      queryByClass(baseElement as HTMLElement, /dls-dropdown-base-container/)
    ).toBeInTheDocument();
    expect(
      queryByClass(baseElement as HTMLElement, /dls-no-result-found/)
    ).toBeNull();
    expect(screen.getByText('noResult element')).toBeInTheDocument();
  });

  it('should render with Group and check all', () => {
    const { baseElement } = render(
      <DropdownBase checkAll>
        <DropdownBase.Group label="Group One">
          {dataItem.map(({ value, label }) => {
            return (
              <DropdownBase.Item
                key={value}
                value={value}
                variant="checkbox"
                label={label}
              />
            );
          })}
        </DropdownBase.Group>
        <DropdownBase.Group label="Group two">
          <DropdownBase.Item
            key={3}
            value={3}
            variant="checkbox"
            label={'label 3'}
          />
        </DropdownBase.Group>
      </DropdownBase>
    );

    expect(
      queryByClass(baseElement as HTMLElement, /dls-dropdown-base-container/)
    ).toBeInTheDocument();
    expect(
      queryAllByClass(baseElement as HTMLElement, /start-group/).length
    ).toEqual(3);
  });

  it('should render with virtual scroll', () => {
    const { baseElement } = renderComponent({
      virtualScroll: true,
      loadingInfinityScroll: true
    });

    expect(queryByText(baseElement, 'Loading more...')).toBeInTheDocument();
  });
});

describe('Actions', () => {
  describe('onChange when allow multiple select', () => {
    describe('press Enter key', () => {
      it('controlled', () => {
        const onChange = jest.fn();
        renderComponent({
          valueType: 'multiple',
          allowKeydown: true,
          onChange,
          value: []
        });
        pressKey(keycode.DOWN);
        pressKey(keycode.ENTER);
        expect(onChange).toBeCalledWith(
          expect.objectContaining({ value: [1] })
        );
      });

      it('controlled valueType new-behavior', () => {
        const onChange = jest.fn();
        renderComponent({
          valueType: 'new-behavior',
          allowKeydown: true,
          onChange,
          allowCallOnChangeByEnter: true
        });
        pressKey(keycode.DOWN);
        pressKey(keycode.UP);
        pressKey(keycode.ENTER);
        expect(onChange).toBeCalled();
      });

      it('uncontrolled', () => {
        const onChange = jest.fn();
        renderComponent({
          valueType: 'multiple',
          allowKeydown: true,
          onChange
        });
        pressKey(keycode.DOWN);
        pressKey(keycode.ENTER);
        expect(onChange).toBeCalledWith(
          expect.objectContaining({ value: [1] })
        );
        onChange.mockClear();
        // pess enter again to unselect
        pressKey(keycode.ENTER);
        expect(onChange).toBeCalledWith(expect.objectContaining({ value: [] }));
      });
    });

    it('press key down and up', () => {
      const onChange = jest.fn();
      renderComponent({
        valueType: 'multiple',
        allowKeydown: true,
        onChange
      });

      // select item [0]
      pressKey(keycode.DOWN);
      pressKey(keycode.ENTER);
      expect(onChange).toBeCalledWith(expect.objectContaining({ value: [1] }));
      onChange.mockClear();

      // select item [1]
      pressKey(keycode.DOWN);
      pressKey(keycode.ENTER);
      expect(onChange).toBeCalledWith(
        expect.objectContaining({ value: [1, 2] })
      );
      onChange.mockClear();

      // select item [3] because item [2] is disabled
      pressKey(keycode.DOWN);
      pressKey(keycode.ENTER);
      expect(onChange).toBeCalledWith(
        expect.objectContaining({ value: [1, 2, 4] })
      );
      onChange.mockClear();

      // un-select item [2]
      pressKey(keycode.DOWN);
      pressKey(keycode.ENTER);
      expect(onChange).toBeCalledWith(
        expect.objectContaining({ value: [1, 2] })
      );
      onChange.mockClear();

      // un-select item [1]
      pressKey(keycode.UP);
      pressKey(keycode.ENTER);
      expect(onChange).toBeCalledWith(expect.objectContaining({ value: [1] }));
      onChange.mockClear();

      // un-select item [0]
      pressKey(keycode.UP);
      pressKey(keycode.ENTER);
      expect(onChange).toBeCalledWith(expect.objectContaining({ value: [] }));
      onChange.mockClear();

      // select item [0]
      pressKey(keycode.UP);
      pressKey(keycode.ENTER);
      expect(onChange).toBeCalledWith(expect.objectContaining({ value: [1] }));
      onChange.mockClear();
    });

    it('press key down and up valueType new-behavior', () => {
      const onChange = jest.fn();
      renderComponent({
        valueType: 'new-behavior',
        allowKeydown: false,
        onChange
      });
      pressKey(keycode.DOWN);
      pressKey(keycode.ENTER);
      expect(onChange).not.toBeCalled();
    });

    it('press key down when children is null', () => {
      const onChange = jest.fn();

      render(
        <DropdownBase valueType="multiple" allowKeydown onChange={onChange}>
          {dataItem.map(() => {
            return null as any;
          })}
        </DropdownBase>
      );

      pressKey(keycode.DOWN);
      pressKey(keycode.ENTER);

      expect(onChange).not.toBeCalled();
    });

    it('press another key', () => {
      const onChange = jest.fn();
      renderComponent({ valueType: 'multiple', allowKeydown: true, onChange });
      pressKey(keycode.C);
      pressKey(keycode.ENTER);

      expect(onChange).not.toBeCalled();
    });

    it('press key down but not allow call onChange', () => {
      const onChange = jest.fn();
      renderComponent({ valueType: 'multiple', allowKeydown: false, onChange });
      pressKey(keycode.DOWN);
      pressKey(keycode.ENTER);
      expect(onChange).not.toBeCalled();
    });

    it('click to select', () => {
      const onChange = jest.fn();
      renderComponent({
        valueType: 'multiple',
        allowCallOnChangeByClick: true,
        onChange
      });
      userEvent.click(screen.getByText('label 1'));
      expect(onChange).toBeCalledWith(expect.objectContaining({ value: [1] }));
    });

    it('should call search first character if valueType is new-behavior', () => {
      jest.useFakeTimers();
      const { wrapper } = renderComponent({
        valueType: 'new-behavior',
        allowKeydown: true
      });
      fireEvent.keyDown(wrapper.baseElement, { key: 'Shift' });
      fireEvent.keyDown(wrapper.baseElement, { key: 'L' });
      fireEvent.keyDown(wrapper.baseElement, { key: 'L' });
      fireEvent.keyDown(wrapper.baseElement, { key: 'L' });
      fireEvent.keyDown(wrapper.baseElement, { key: 'L' });
      fireEvent.keyDown(wrapper.baseElement, { key: 'L' });
      jest.runAllTimers();

      expect(true).toBeTruthy();
    });
  });

  describe('onChange when allow single select', () => {
    describe('press Enter key', () => {
      it('controlled', () => {
        const onChange = jest.fn();

        renderComponent({
          valueType: 'single',
          allowKeydown: true,
          allowCallOnChangeByEnter: true,
          onChange,
          value: []
        });

        pressKey(keycode.ENTER);

        expect(onChange).toBeCalledWith(expect.objectContaining({ value: [] }));
      });

      it('uncontrolled', () => {
        const onChange = jest.fn();

        renderComponent({
          valueType: 'single',
          allowKeydown: true,
          allowCallOnChangeByEnter: true,
          onChange
        });

        pressKey(keycode.ENTER);

        expect(onChange).toBeCalledWith(
          expect.objectContaining({ value: undefined })
        );
      });
    });

    it('press key up', () => {
      const onChange = jest.fn();
      renderComponent({
        valueType: 'single',
        allowKeydown: true,
        allowCallOnChangeByUpDown: true,
        onChange
      });

      pressKey(keycode.UP);

      expect(onChange).toBeCalledWith(expect.objectContaining({ value: 1 }));
    });

    it('press key down', () => {
      const onChange = jest.fn();

      renderComponent({
        valueType: 'single',
        allowKeydown: true,
        allowCallOnChangeByUpDown: true,
        onChange
      });

      pressKey(keycode.DOWN);
      pressKey(keycode.ENTER);

      expect(onChange).toBeCalledWith(expect.objectContaining({ value: 1 }));
    });

    it('press key down when children is null', () => {
      const onChange = jest.fn();

      render(
        <DropdownBase
          valueType="single"
          allowKeydown
          allowCallOnChangeByUpDown
          onChange={onChange}
        >
          {dataItem.map(() => {
            return null as any;
          })}
        </DropdownBase>
      );

      pressKey(keycode.DOWN);
      pressKey(keycode.ENTER);

      expect(onChange).not.toBeCalled();
    });

    it('press key down but not allow call onChange', () => {
      const onChange = jest.fn();
      renderComponent({
        valueType: 'single',
        allowKeydown: true,
        allowCallOnChangeByUpDown: false,
        onChange
      });

      pressKey(keycode.DOWN);

      expect(onChange).not.toBeCalled();
    });

    it('click select when allow single select', () => {
      const onChange = jest.fn();

      renderComponent({
        valueType: 'single',
        allowCallOnChangeByClick: true,
        onChange
      });

      userEvent.click(screen.getByText('label 1'));

      expect(onChange).toBeCalledWith(expect.objectContaining({ value: 1 }));
    });
  });

  it('should not invoke onChange when not allow', () => {
    const onChange = jest.fn();

    renderComponent({ allowKeydown: false, onChange });

    pressKey(keycode.UP);
    pressKey(keycode.ENTER);

    expect(onChange).not.toBeCalled();
  });

  it('onFilter', () => {
    const onFilter = jest.fn();
    const searchValue = 'value';

    const { baseElement } = renderComponent({
      valueType: 'multiple',
      searchBar: true,
      onFilter
    });

    const searchBar = queryByClass(
      baseElement,
      /dls-dropdown-base-search-bar/
    )!;
    const inputSearch = getByRole(searchBar, 'textbox');

    fireEvent.change(inputSearch, { target: { value: searchValue } });

    expect(onFilter).toBeCalledWith(searchValue);
  });

  describe('press keyboard support for MultiSelect', () => {
    it('press enter in item check all 2 times', () => {
      const { baseElement, container } = render(<RenderMultiSelect />);

      // show dropdown
      userEvent.click(container.querySelector('.text-field-container')!);

      fireEvent.keyDown(baseElement, {
        key: 'ArrowDown',
        keyCode: keycode.DOWN
      });

      expect(
        baseElement
          .querySelectorAll('.item.item-checkbox')[0]
          .classList.contains('dls-hover')
      ).toBeTruthy();

      fireEvent.keyDown(baseElement, {
        key: 'ArrowDown',
        keyCode: keycode.UP
      });

      expect(
        baseElement
          .querySelectorAll('.item.item-checkbox')[0]
          .classList.contains('dls-hover')
      ).toBeTruthy();

      fireEvent.keyDown(baseElement, { key: 'Enter', keyCode: keycode.ENTER });

      expect(baseElement.querySelectorAll('.dls-selected').length).toEqual(4);

      expect(
        baseElement
          .querySelectorAll('.item.item-checkbox')[0]
          .classList.contains('dls-hover')
      ).toBeTruthy();

      fireEvent.keyDown(baseElement, { key: 'Enter', keyCode: keycode.ENTER });
      expect(baseElement.querySelectorAll('.dls-selected').length).toEqual(0);
      expect(
        baseElement
          .querySelectorAll('.item.item-checkbox')[0]
          .classList.contains('dls-hover')
      ).toBeTruthy();
    });

    it('press arrow down and search item but not correct', () => {
      const { baseElement, container } = render(<RenderMultiSelect />);

      // show dropdown
      userEvent.click(container.querySelector('.text-field-container')!);

      fireEvent.keyDown(baseElement, { key: 'Enter', keyCode: keycode.ENTER });

      expect(baseElement.querySelectorAll('.dls-hover').length).toEqual(0);

      fireEvent.keyDown(baseElement, {
        key: 'ArrowDown',
        keyCode: keycode.DOWN
      });

      expect(
        baseElement
          .querySelectorAll('.item.item-checkbox')[0]
          .classList.contains('dls-hover')
      ).toBeTruthy();

      const searchBar = queryByClass(
        baseElement as HTMLElement,
        /dls-dropdown-base-search-bar/
      )!;
      const inputSearch = getByRole(searchBar, 'textbox');

      userEvent.type(inputSearch, 'D');

      expect(screen.getByText('No results found')).toBeInTheDocument();

      fireEvent.keyDown(baseElement, { key: 'Enter', keyCode: keycode.ENTER });

      expect(screen.getByText('No results found')).toBeInTheDocument();
    });
  });
});
