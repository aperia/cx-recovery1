import React, { useEffect, useRef, useState } from 'react';
import classNames from 'classnames';
// components/types
import {
  BubbleDataPoint,
  Chart,
  ChartTypeRegistry,
  ScatterDataPoint
} from 'chart.js';

// hooks
import {
  useDoughnutCenterCanvas,
  useDoughnutLegend,
  useRegister
} from './hooks';

// utils
import { nanoid } from 'nanoid';
import { ChartData } from 'chart.js';
import { useDeepValue } from '../../hooks';
import classnames from 'classnames';
import { set } from '../../lodash';
import { genAmtId } from '../../utils';

export interface DoughnutChartData extends ChartData<'doughnut', any> {
  center?: {
    value?: string;
    label?: string;
  };
  size?: 'small';
}

export interface DoughnutChartProps extends DLSId {
  chartData: DoughnutChartData;
  legendValueRender?: (value: number) => string;
  legendLabelRender?: (label: string) => string;
  allowToggleLegend?: boolean;
}

const Doughnut: React.FC<DoughnutChartProps> = ({
  chartData,
  legendValueRender,
  legendLabelRender,
  allowToggleLegend = false,

  id,
  dataTestId
}) => {
  const nextChartData = useDeepValue(chartData);

  // refs
  const canvasRef = useRef<HTMLCanvasElement | null>(null);
  const legendPluginIdRef = useRef(nanoid());
  const centerCanvasPluginIdRef = useRef(nanoid());
  const chartRef =
    useRef<
      Chart<
        keyof ChartTypeRegistry,
        (number | ScatterDataPoint | BubbleDataPoint | null)[],
        unknown
      >
    >();
  const [isHiddenCanvas, setIsHiddenCanvas] = useState(false);

  // chartJs register
  useRegister();

  const isSmall = chartData?.size === 'small';

  const [legendPlugin, legendElements] = useDoughnutLegend(
    legendPluginIdRef.current,
    allowToggleLegend,
    legendValueRender,
    legendLabelRender
  );
  const centerCanvasPlugin = useDoughnutCenterCanvas(
    centerCanvasPluginIdRef.current
  );

  useEffect(() => {
    const canvas = canvasRef.current!;
    const ctx = canvas.getContext('2d')!;

    const datasets = nextChartData.datasets;
    const max = Math.max(...datasets[0].data);
    const min = Math.min(...datasets[0].data);
    const nextData = datasets[0].data.map((datasetValue: number) => {
      if (datasetValue === max) return max - min;
      return datasetValue;
    });
    const fakeChartData = JSON.parse(JSON.stringify(nextChartData));
    set(fakeChartData, ['datasets', '0', 'data'], nextData);
    const pluginOptions = {
      plugins: {
        legend: {
          display: false
        },
        tooltip: {
          enabled: false
        }
      },
      radius: '100%',
      cutout: isSmall ? '70%' : '65%',
      borderWidth: 1,
      rotation: 0
    } as any;

    if (!chartRef.current) {
      const doughnutChart = new Chart(ctx, {
        type: 'doughnut',
        data: fakeChartData,
        options: pluginOptions,
        plugins: [legendPlugin, centerCanvasPlugin]
      } as any);

      chartRef.current = doughnutChart;
    }

    chartRef.current.data = fakeChartData;
    chartRef.current.options = pluginOptions;

    chartRef.current.update();
  }, [nextChartData, legendPlugin, centerCanvasPlugin, isSmall]);

  // destroy chart
  useEffect(() => {
    return () => chartRef.current?.destroy();
  }, []);

  useEffect(() => {
    process.nextTick(() => {
      const isHidden = nextChartData.datasets[0].data.every(
        (item: any) => isNaN(item) || item == 0
      );
      setIsHiddenCanvas(isHidden);
    });
  }, [nextChartData]);

  return (
    <div
      className={classnames('dls-chart dls-doughnut-chart', { small: isSmall })}
      id={id}
      data-testid={genAmtId(dataTestId, 'dls-doughnut-chart', 'DoughnutChart')}
    >
      <div
        className={classNames('doughnut-chart-canvas', {
          'd-none': isHiddenCanvas
        })}
        style={{ width: isSmall ? '59px' : '150px' }}
      >
        <canvas ref={canvasRef} />
      </div>
      <div className="doughnut-chart-legend ml-24">{legendElements}</div>
    </div>
  );
};

export default Doughnut;
