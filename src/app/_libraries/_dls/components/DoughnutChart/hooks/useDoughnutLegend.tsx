import React, { useMemo, useRef, useState } from 'react';

import { Plugin } from 'chart.js';
import { get, isFunction } from '../../../lodash';
import classnames from 'classnames';
import { DoughnutChartProps } from '..';

export interface DoughnutLegendHook {
  (
    pluginId: string,
    allowToggleLegend: boolean,
    legendValueRender: DoughnutChartProps['legendValueRender'],
    legendLabelRender: DoughnutChartProps['legendLabelRender']
  ): [Plugin<'doughnut', any>, React.ReactElement[] | null];
}

// this hook will return htmlLegendPlugin + react legend portal
// to custom DoughnutChart html legend
const useDoughnutLegend: DoughnutLegendHook = (
  pluginId,
  allowToggleLegend,
  legendValueRender,
  legendLabelRender
) => {
  const [elements, setElements] = useState<React.ReactElement[] | null>(null);

  const legendValueRenderRef = useRef(legendValueRender);
  const legendLabelRenderRef = useRef(legendLabelRender);

  const plugin = useMemo<Plugin<'doughnut', any>>(() => {
    return {
      id: pluginId,
      afterUpdate: chart => {
        if (!chart.data) return;

        const data = get(chart.data, ['datasets', '0', 'data'], []);
        const isHiddenItemPoint = data.every(
          (item: any) => isNaN(item) || item == 0
        );
        const generateLabels =
          chart?.options?.plugins?.legend?.labels?.generateLabels;
        const labels = chart.data.labels as string[];
        const legendItems = isFunction(generateLabels)
          ? generateLabels(chart)
          : null;

        if (!Array.isArray(labels) || !Array.isArray(legendItems)) return;

        // handle click on legend
        const handleOnClickLegend = (toggleIndex: number) => {
          // should toggle legend or not (allowToggleLegend, default: false)
          if (!allowToggleLegend) return;

          chart.toggleDataVisibility(toggleIndex);
          chart.update();
        };

        const legendElements = labels.map((labelItem, index) => {
          const legendItem = get(legendItems, index);
          const dataItem = get(chart.data, [
            'datasets',
            '0',
            'data',
            index
          ]) as number;
          const backgroundColorItem = get(chart.data, [
            'datasets',
            '0',
            'backgroundColor',
            index
          ]) as string;
          const ky = `${dataItem}-${backgroundColorItem}-${index}`;

          return (
            <div
              key={ky}
              onClick={() => handleOnClickLegend(index)}
              className={classnames('doughnut-chart-legend-item', {
                'item-removed': legendItem.hidden
              })}
            >
              <div className="item-value color-grey-d20">
                {isFunction(legendValueRenderRef.current)
                  ? legendValueRenderRef.current(dataItem)
                  : dataItem}
              </div>
              <div className="item-label doughnut-chart-text-label color-grey">
                {!isHiddenItemPoint && (
                  <span
                    className="item-point"
                    style={{ backgroundColor: backgroundColorItem }}
                  />
                )}
                <span>
                  {isFunction(legendLabelRenderRef.current)
                    ? legendLabelRenderRef.current(labelItem)
                    : labelItem}
                </span>
              </div>
            </div>
          );
        });

        // after this plugin run, legendElements has been generated
        // we will set legendElements to elements to use outside this hook
        setElements(legendElements);
      }
    };
  }, [allowToggleLegend, pluginId]);

  return [plugin, elements];
};

export default useDoughnutLegend;
