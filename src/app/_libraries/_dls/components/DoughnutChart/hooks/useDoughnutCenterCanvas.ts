import { useMemo } from 'react';

import { Plugin } from 'chart.js';
import { get } from '../../../lodash';
import { canvasTextWidth } from '../../../utils';

export interface DoughnutCenterCanvasHook {
  (pluginId: string): Plugin<'doughnut', any>;
}

const useDoughnutCenterCanvas: DoughnutCenterCanvasHook = pluginId => {
  const plugin = useMemo<Plugin<'doughnut', any>>(() => {
    return {
      id: pluginId,
      beforeDraw: chart => {
        const context = get(chart, 'ctx');
        const width = get(chart, 'width', 0);
        const height = get(chart, 'height', 0);
        const value = get(chart.data, ['center', 'value']) as string;
        const label = get(chart.data, ['center', 'label']) as string;

        const x = width / 2;
        const y = height / 2;

        // only show value, always ignore label
        if (get(chart.data, 'size') === 'small') {
          context.font = 'bold 11px Open Sans';
          context.fillStyle = '#333333';

          const smallValueTextMetrics = canvasTextWidth(value, context.font)!;
          context.fillText(value, x - smallValueTextMetrics.width / 2, y + 4);
          return;
        }

        context.font = 'bold 16px Open Sans';
        context.fillStyle = '#333';
        const valueTextMetrics = canvasTextWidth(value, context.font)!;
        context.fillText(value, x - valueTextMetrics.width / 2, y - 2);

        context.font = '10px Open Sans';
        context.fillStyle = '#666';
        const labelTextMetrics = canvasTextWidth(label, context.font)!;
        context.fillText(label, x - labelTextMetrics.width / 2, y + 20);
      }
    };
  }, [pluginId]);

  return plugin;
};

export default useDoughnutCenterCanvas;
