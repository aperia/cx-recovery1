import React from 'react';
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import DoughnutChart, { DoughnutChartData } from '.';

jest.mock('chart.js', () => {
  class Chart {
    constructor(ctx: any, options: any) {}

    public static register = (list: any) => undefined;

    update = jest.fn();

    destroy = () => undefined;
  }

  return {
    Chart,
    registerables: []
  };
});

const data = {
  labels: ['inbound calls', 'outbound calls'],
  datasets: [
    {
      label: 'Demo',
      data: [12000, 10000],
      backgroundColor: ['#D977C4', '#7777D9']
    }
  ],
  center: {
    label: 'Total',
    value: '757'
  }
} as DoughnutChartData;

describe('Doughnut chart', () => {
  it('should render with size small', () => {
    render(<DoughnutChart size="small" chartData={data} />);

    // temp
    expect(true).toBeTruthy();
  });

  it('should render with origin deg', () => {
    render(
      <DoughnutChart
        size="small"
        chartData={
          {
            ...data,
            datasets: [
              {
                ...data.datasets,
                data: [12000, 5000]
              }
            ]
          } as any
        }
      />
    );

    // temp
    expect(true).toBeTruthy();
  });

  it('should render with data [0,0]', () => {
    jest.useFakeTimers();
    render(
      <DoughnutChart
        size="small"
        chartData={
          {
            ...data,
            datasets: [{ ...data.datasets, data: [0, 0] }]
          } as any
        }
      />
    );
    jest.runAllTicks();

    // temp
    expect(true).toBeTruthy();
  });

  it('should cover 100%', () => {
    const result = render(<DoughnutChart chartData={data} />);
    result.rerender(
      <DoughnutChart chartData={{ ...data, labels: ['outbound calls'] }} />
    );
    // temp
    expect(true).toBeTruthy();
  });
});
