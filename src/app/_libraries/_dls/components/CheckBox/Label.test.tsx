import React from 'react';
import '@testing-library/jest-dom';
import Label from './Label';
import { render } from '@testing-library/react';
import { queryByClass } from '../../test-utils';

describe('Test Label CheckBox', () => {
  it('Render Label', () => {
    const { container } = render(<Label />);
    const queryClassLabel = queryByClass(container, /dls-checkbox-label/)!;
    expect(queryClassLabel).toBeInTheDocument();
  });
});
