import { ButtonProps } from '../Button';
import {
  DropdownBaseProps,
  DropdownBaseRef,
  DropdownBaseItemProps,
  DropdownBaseGroupProps,
  DropdownBaseItemRef
} from '../DropdownBase';
import { PopupBaseProps } from '../PopupBase';
import DropdownButton from '.';

export interface DropdownButtonSelectEvent
  extends React.ChangeEvent<typeof DropdownButton> {
  target: EventTarget &
    typeof DropdownButton & { value?: any; id?: string; name?: string };
  value?: any;
}

export interface DropdownButtonRef {
  popupBaseElement?: HTMLElement;
  dropdownBaseRef?: React.MutableRefObject<DropdownBaseRef | null>;
  buttonElement?: HTMLButtonElement | null;
}

export interface ButtonPropsOmitted
  extends Omit<ButtonProps, 'children' | 'onChange' | 'value'> {}

export interface DropdownButtonProps
  extends Omit<
      DropdownBaseProps,
      | 'selected'
      | 'onSelect'
      | 'allowKeydown'
      | 'reference'
      | 'scrollContainerProps'
      | 'value'
      | 'onChange'
      | 'allowCallOnChangeByUpDown'
      | 'allowCallOnChangeByEnter'
      | 'allowCallOnChangeByClick'
    >,
    DLSId {
  onBlur?: (event: React.FocusEvent) => void;
  onFocus?: (event: React.FocusEvent) => void;

  opened?: boolean;
  onVisibilityChange?: (nextOpened: boolean) => void;

  onSelect?: (event: DropdownButtonSelectEvent) => void;

  popupBaseProps?: Omit<
    PopupBaseProps,
    'opened' | 'onVisibilityChange' | 'reference'
  >;
  autoFocus?: boolean;

  buttonProps?: ButtonProps;
}

export interface DropdownButtonItemRef extends DropdownBaseItemRef {}

export interface DropdownButtonItemProps extends DropdownBaseItemProps {}

export interface DropdownButtonGroupProps extends DropdownBaseGroupProps {}
