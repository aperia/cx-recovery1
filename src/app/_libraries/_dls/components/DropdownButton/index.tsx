import React, { useEffect, useImperativeHandle, useRef, useState } from 'react';

// components/types
import DropdownBase, {
  DropdownBaseChangeEvent,
  DropdownBaseRef,
  DropdownBaseGroup,
  DropdownBaseItem
} from '../DropdownBase';
import PopupBase, { PopupBaseRef } from '../PopupBase';
import Button from '../Button';
import { DropdownButtonRef, DropdownButtonProps } from './types';

// utils
import classnames from 'classnames';
import { classes,  isMacOrIOS } from '../../utils';
import { isBoolean, isFunction } from '../../lodash';

const DropdownButton: React.ForwardRefRenderFunction<
  DropdownButtonRef,
  DropdownButtonProps
> = (
  {
    onSelect,

    onFocus,
    onBlur,
    opened: openedProp,
    onVisibilityChange,

    id,
    name,

    popupBaseProps,
    autoFocus = false,
    className: classNameProp,
    children,

    buttonProps,

    dataTestId,
    ...props
  },
  ref
) => {
  // refs
  const buttonRef = useRef<HTMLButtonElement | null>(null);
  const dropdownBaseRef = useRef<DropdownBaseRef | null>(null);
  const autoFocusRef = useRef(autoFocus);
  const [popupBaseElement, setPopupBaseElement] = useState<PopupBaseRef | null>(
    null
  );
  const keepRef = useRef<Record<string, any>>({});

  // states
  const [opened, setOpened] = useState(!!openedProp);

  // handle mousedown on DropdownButton element
  const handleOnMouseDown = (
    event?: React.MouseEvent<HTMLButtonElement, MouseEvent>
  ) => {
    if (isMacOrIOS() && !opened) {
      event?.stopPropagation();
      handleOpenedMode(true);
      return;
    }

    const buttonElement = buttonRef.current;

    if (document.activeElement && document.activeElement === buttonElement) {
      // if button is focused right now, we don't want focus/blur of the button element is triggered
      // use preventDefault() to prevent it
      event?.preventDefault();

      buttonElement.blur();
    }
  };

  useEffect(() => {
    if (!isMacOrIOS() || !popupBaseElement?.element) {
      return;
    }

    const handleOnClick = (event: MouseEvent) => {
      if (popupBaseElement.element?.contains(event.target as HTMLElement)) {
        return;
      }

      keepRef.current.handleOpenedMode(false);
    };

    window.addEventListener('mousedown', handleOnClick);
    return () => window.removeEventListener('mousedown', handleOnClick);
  }, [popupBaseElement?.element]);

  // handle opened uncontrolled/controlled
  const handleOpenedMode = (nextOpened: boolean) => {
    isFunction(onVisibilityChange) && onVisibilityChange(nextOpened);

    // controlled
    if (isBoolean(openedProp)) return;

    // uncontrolled
    setOpened(nextOpened);
  };

  // handle focus on DropdownButton element
  const handleOnFocus = (event: React.FocusEvent) => {
    isFunction(onFocus) && onFocus(event);
    handleOpenedMode(true);
  };

  // handle blur on DropdownButton element
  const handleOnBlur = (event: React.FocusEvent) => {
    // what if the user clicks on something that is not in the browser?
    // example: desktop, taskbar, developer tools...
    // by default, if the user clicks on those thing, the browser will keeps
    // the previous state
    // So the next time, the user clicks (mousedown) on the screen (valid container DOM browser),
    // onFocus will be called and onMouseDown will be not called
    // we don't want that behavior
    // so this code block to prevent that behavior
    if (
      document.activeElement &&
      document.activeElement === buttonRef.current
    ) {
      buttonRef.current?.blur();
    }

    isFunction(onBlur) && onBlur(event);
    handleOpenedMode(false);
  };

  // if the popup shown, we want the button element to stay in focus even if we
  // click on the popup, read more details in side this function
  const handleOnPopupShown = () => {
    const popupElement = popupBaseElement?.element;

    // handle mousedown on Popup's DropdownButton
    // mousedown on Popup's DropdownButton will trigger onBlur default of the DropdownButton
    // that is not the behavior we want, so we need to preventDefault it
    const handleMouseDown = (event: MouseEvent) => {
      // stop browser events
      event.preventDefault();
    };

    // no need to remove event later, because popup element will be remove from DOM
    popupElement?.addEventListener('mousedown', handleMouseDown);
  };

  // handle onChange DropdownBase
  // we know that: handleOnChange just called by keyboard enter and mouse click
  // because we are used two props: allowCallOnChangeByEnter, allowCallOnChangeByClick
  const handleOnChange = (event: DropdownBaseChangeEvent) => {
    isFunction(onSelect) && onSelect(event as any);

    if (isMacOrIOS()) {
      handleOpenedMode(false);
      return;
    }

    buttonRef.current?.blur();
  };

  keepRef.current.handleOpenedMode = handleOpenedMode;

  // provide ref
  const publicRef = useRef<DropdownButtonRef | null>();
  useImperativeHandle(
    ref,
    () => {
      if (!publicRef.current) publicRef.current = {};

      publicRef.current.popupBaseElement = popupBaseElement?.element;
      publicRef.current.dropdownBaseRef = dropdownBaseRef;
      publicRef.current.buttonElement = buttonRef.current;

      return publicRef.current;
    },
    [popupBaseElement?.element]
  );

  // set props
  useEffect(() => setOpened(!!openedProp), [openedProp]);

  // handle autofocus first time render
  useEffect(() => {
    if (!autoFocusRef.current) return;

    autoFocusRef.current = false;
    buttonRef.current?.focus();
  }, []);

  const { popupBaseClassName, ...popupProps } = popupBaseProps || {};
  const {
    className: buttonClassName,
    autoBlur = false,
    ...restButtonProps
  } = buttonProps || {};

  return (
    <>
      <Button
        ref={buttonRef}
        onMouseDown={handleOnMouseDown}
        onFocus={handleOnFocus}
        onBlur={handleOnBlur}
        className={classnames(
          classes.dropdownButton.container,
          opened && 'active',
          classNameProp,
          buttonClassName
        )}
        autoBlur={autoBlur}
        id={id}
        dataTestId={dataTestId}
        {...restButtonProps}
      />
      <PopupBase
        ref={setPopupBaseElement}
        reference={buttonRef.current!}
        opened={opened}
        onVisibilityChange={handleOpenedMode}
        onPopupShown={handleOnPopupShown}
        popupBaseClassName={classnames(
          classes.popupBase.container,
          popupBaseClassName
        )}
        dataTestId={`${dataTestId}-popup-dropdown-button`}
        {...popupProps}
      >
        <DropdownBase
          ref={dropdownBaseRef}
          allowKeydown={opened}
          allowCallOnChangeByEnter
          allowCallOnChangeByClick
          value={null}
          onChange={handleOnChange}
          name={name}
          id={id}
          dataTestId={dataTestId}
          {...props}
        >
          {children}
        </DropdownBase>
      </PopupBase>
    </>
  );
};

const DropdownButtonExtraStaticProp = React.forwardRef<
  DropdownButtonRef,
  DropdownButtonProps
>(DropdownButton) as React.ForwardRefExoticComponent<
  DropdownButtonProps & React.RefAttributes<DropdownButtonRef>
> & {
  Group: typeof DropdownBaseGroup;
  Item: typeof DropdownBaseItem;
};

DropdownButtonExtraStaticProp.Group = DropdownBaseGroup;
DropdownButtonExtraStaticProp.Item = DropdownBaseItem;

export * from './types';
export {
  DropdownBaseGroup as DropdownButtonGroup,
  DropdownBaseItem as DropdownButtonItem
};
export default DropdownButtonExtraStaticProp;
