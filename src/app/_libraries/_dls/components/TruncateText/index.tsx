import React, {
  useState,
  useImperativeHandle,
  useRef,
  forwardRef,
  useLayoutEffect,
  useCallback,
  useEffect,
  MutableRefObject,
  ForwardRefRenderFunction,
  HTMLAttributes,
  ReactNode
} from 'react';

// utils
import ResizeObserver from 'resize-observer-polyfill';
import classnames from 'classnames';
import {
  getPadding,
  getMargin,
  getAvailableWidth,
  genAmtId
} from '../../utils';
import { isBoolean, isEmpty, isString } from '../../lodash';

export interface TruncateTextProps
  extends Omit<HTMLAttributes<HTMLSpanElement>, 'prefix' | 'suffix'>,
    DLSId {
  lines?: number;
  onTruncate?: (isTruncated: boolean) => void;
  ellipsisMoreText?: string;
  ellipsisLessText?: string;

  autoShowTooltip?: boolean;
  resizable?: boolean;
  followRef?: MutableRefObject<HTMLElement | null>;
  classes?: {
    text?: string;
    ellipsis?: string;
  };
  prefix?: {
    /**
     * Only display prefix when truncated, default: true.
     * If you want it always visible, set it to false
     */
    onlyDisplayOnTruncate?: boolean;
    element?: ReactNode;
  };
  suffix?: {
    /**
     * Only display suffix when truncated, default: true.
     * If you want it always visible, set it to false
     */
    onlyDisplayOnTruncate?: boolean;
    element?: ReactNode;
  };
  suffixElement?: ReactNode;
  shown?: boolean;
  onShown?: (nextShown: boolean) => void;
}

const TruncateText: ForwardRefRenderFunction<
  HTMLSpanElement,
  TruncateTextProps
> = (
  {
    classes,
    className,
    autoShowTooltip = true,
    resizable,
    title: titleProp,
    lines = 1,
    children,
    ellipsisMoreText,
    ellipsisLessText,
    followRef: followRefProp,
    onTruncate,
    prefix,
    suffix,
    shown: shownProp,
    onShown,

    id,
    dataTestId,
    ...props
  },
  ref
) => {
  const { text: textClassName, ellipsis: ellipsisClassName } = classes || {};

  const {
    onlyDisplayOnTruncate: prefixOnlyDisplayOnTruncate = true,
    element: prefixElement
  } = prefix || {};
  const {
    onlyDisplayOnTruncate: suffixOnlyDisplayOnTruncate = true,
    element: suffixElement
  } = suffix || {};
  const hasPrefix = Boolean(prefixElement);
  const hasSuffix = Boolean(suffixElement);

  const containerRef = useRef<HTMLSpanElement | null>(null);
  const textRef = useRef<HTMLSpanElement | null>(null);
  const prefixRef = useRef<HTMLSpanElement | null>(null);
  const ellipsisRef = useRef<HTMLSpanElement | null>(null);
  const suffixRef = useRef<HTMLSpanElement | null>(null);

  const [parentElement, setParentElement] = useState<HTMLElement | null>(
    followRefProp?.current || null
  );
  const [isTruncated, setIsTruncated] = useState(false);
  const [isExcludeWrapperSpacing, setIsExcludeWrapperSpacing] = useState(false);
  const [rendered, setRendered] = useState(false);
  const [showAll, setShowAll] = useState(shownProp);
  const [content, setContent] = useState(isString(children) ? children : '');
  const [width, setWidth] = useState(0);
  const [title, setTitle] = useState<string | undefined>('');

  const getTextWithStyle = (text: string) => {
    const element = textRef.current as HTMLSpanElement;
    const style = window.getComputedStyle(element);

    switch (style.textTransform) {
      case 'capitalize':
        return text
          .split(' ')
          .map((word) => word.charAt(0).toUpperCase() + word.slice(1))
          .join(' ');
      case 'uppercase':
        return text.toUpperCase();
      case 'lowercase':
        return text.toLowerCase();
      default:
        return text;
    }
  };

  const getTruncateText = (
    text: string,
    maxHeight: number,
    elementForRender: HTMLElement,
    ellipsisElement: HTMLSpanElement,
    prefixElement: HTMLSpanElement,
    suffixElement: HTMLSpanElement
  ) => {
    // get words list
    const wordList = text.split(' ');
    let currentText = '';

    // get text which is just over maxHeight by one word
    for (let i = 0; i < wordList.length; i++) {
      elementForRender.textContent = currentText;

      // add gap before checking
      elementForRender.prepend(prefixElement);
      // add ellipsis before checking
      elementForRender.append(ellipsisElement);
      // add gap before checking
      elementForRender.append(suffixElement);

      const { height } = elementForRender.getBoundingClientRect();

      // add more words when height isn't over maxHeight
      if (height <= maxHeight) {
        currentText = !currentText
          ? wordList[i]
          : `${currentText} ${wordList[i]}`;
      }
      // remove gap after checking
      elementForRender.removeChild(prefixElement);
      // remove ellipsis after checking
      elementForRender.removeChild(ellipsisElement);
      // remove gap after checking
      elementForRender.removeChild(suffixElement);
    }

    let result = '';
    // remove characters which over maxHeight
    for (let i = currentText.length - 1; i >= 0; i--) {
      // continue when result was updated
      if (!isEmpty(result)) continue;

      elementForRender.textContent = currentText.slice(0, i).trim();

      // add gap before checking
      elementForRender.prepend(prefixElement);
      // add ellipsis before checking
      elementForRender.append(ellipsisElement);
      // add gap before checking
      elementForRender.append(suffixElement);
      const { height } = elementForRender.getBoundingClientRect();

      if (height <= maxHeight) {
        result = currentText.slice(0, i).trim();
      }
      // remove gap after checking
      elementForRender.removeChild(prefixElement);
      // remove ellipsis after checking
      elementForRender.removeChild(ellipsisElement);
      // remove gap after checking
      elementForRender.removeChild(suffixElement);
    }

    return result;
  };

  // generate hidden Element with specific Width
  const generateCalculateElement = (
    elementType: string,
    elementWidth?: number
  ) => {
    const div = document.createElement(elementType);
    div.style.visibility = 'hidden';
    div.style.wordBreak = 'break-word';
    div.style.whiteSpace = 'normal';
    if (elementWidth) div.style.width = `${elementWidth}px`;
    return div;
  };

  const checkAndGetNodeTruncate = useCallback(
    (text: string, lines: number) => {
      const { lineHeight } = window.getComputedStyle(
        textRef.current as HTMLSpanElement
      );
      const maxHeight = parseInt(lineHeight) * lines;
      const nativeText = getTextWithStyle(text);

      // create gap element to prepend into calculate element
      const prefixElement = generateCalculateElement('span');
      prefixElement.style.marginLeft = `${prefixRef.current?.offsetWidth}px`;

      const div = generateCalculateElement('div', width);
      // make calculate element same font size & weight with truncate element
      // by adding className of truncate component
      className && div.classList.add(...className.split(' '));
      containerRef.current!.appendChild(div);
      div.innerHTML = nativeText;

      // create gap element to append into calculate element
      const suffixElement = generateCalculateElement('span');
      suffixRef.current?.classList.add('d-inline-block', 'invisible');
      suffixElement.style.display = 'inline-block';
      suffixElement.style.width = `${suffixRef.current?.offsetWidth}px`;
      suffixRef.current?.classList.remove('d-inline-block', 'invisible');

      // prepend gap element before calculate container height
      div.prepend(prefixElement);

      // append gap element before calculate container height
      div.append(suffixElement);

      const { height: divHeight } = div.getBoundingClientRect();

      // No truncate
      if (divHeight <= maxHeight) {
        // don't show ellipsis
        ellipsisRef.current!.classList.add('d-none');
        // check should show suffix or not
        if (hasPrefix && prefixOnlyDisplayOnTruncate) {
          prefixRef.current!.classList.add('d-none');
        }
        // check should show suffix or not
        if (hasSuffix && suffixOnlyDisplayOnTruncate) {
          suffixRef.current!.classList.add('d-none');
        }
        // clean up
        containerRef.current!.removeChild(div);
        return { truncated: false, text: nativeText };
      }
      // 01/17/2022: move code above to function >> WIP

      // If truncated, try removing prefix and suffix
      if (hasSuffix && suffixOnlyDisplayOnTruncate) {
        suffixElement.remove();
        const { height: nextDivHeight } = div.getBoundingClientRect();
        if (nextDivHeight <= maxHeight) {
          // don't show ellipsis
          ellipsisRef.current!.classList.add('d-none');
          // don't show suffix
          suffixRef.current!.classList.add('d-none');
          // clean up
          containerRef.current!.removeChild(div);
          return { truncated: false, text: nativeText };
        }
      }
      if (hasPrefix && prefixOnlyDisplayOnTruncate) {
        prefixElement.remove();
        const { height: nextDivHeight } = div.getBoundingClientRect();
        if (nextDivHeight <= maxHeight) {
          // don't show ellipsis
          ellipsisRef.current!.classList.add('d-none');
          // don't show prefix
          prefixRef.current!.classList.add('d-none');
          // clean up
          containerRef.current!.removeChild(div);
          return { truncated: false, text: nativeText };
        }
      }
      // 01/17/2022: move code above to function >> WIP

      const ellipsisElement = generateCalculateElement('span');
      ellipsisElement.textContent = `... ${ellipsisMoreText || ''}`;

      const result = getTruncateText(
        nativeText,
        maxHeight,
        div,
        ellipsisElement,
        prefixElement,
        suffixElement
      );

      containerRef.current!.removeChild(div);

      return {
        truncated: true,
        text: showAll ? nativeText.trim() : `${result}...`
      };
    },
    [
      width,
      showAll,
      className,
      ellipsisMoreText,
      hasPrefix,
      hasSuffix,
      prefixOnlyDisplayOnTruncate,
      suffixOnlyDisplayOnTruncate
    ]
  );

  const updateWidth = useCallback(() => {
    const maxWidth = getAvailableWidth(parentElement);
    const { parentElement: wrapperElement } =
      containerRef.current as HTMLSpanElement;

    // remove closet parent spacing
    let excludeSpacing = 0;
    if (isExcludeWrapperSpacing) {
      const padding = getPadding(wrapperElement);
      const margin = getMargin(wrapperElement);
      excludeSpacing =
        padding!.left + padding!.right + margin!.left + margin!.right;
    }

    setWidth(maxWidth - excludeSpacing);
  }, [parentElement, isExcludeWrapperSpacing]);

  const handleShownMode = (nextShown: boolean) => {
    onShown?.(nextShown);

    if (isBoolean(shownProp)) return;

    setShowAll(!nextShown);
  };

  useImperativeHandle(ref, () => containerRef.current!, []);

  // trigger truncate text
  useLayoutEffect(() => {
    if (!isString(children) || isEmpty(children) || !width) return;

    // reset display ellipsis, prefix, suffix
    ellipsisRef.current!.classList.remove('d-none');
    prefixRef.current!.classList.remove('d-none');
    suffixRef.current!.classList.remove('d-none');

    // generate the truncated text
    const { truncated, text } = checkAndGetNodeTruncate(
      children,
      lines > 0 ? lines : 1
    );

    onTruncate?.(truncated);

    setIsTruncated(truncated);
    setContent(text);
  }, [rendered, width, lines, children, checkAndGetNodeTruncate, onTruncate]);

  // Setup resizable watching
  useLayoutEffect(() => {
    // return in first mount or children isn't a text
    if (!parentElement || !isString(children) || !rendered) return;

    // calculate max width one time
    if (!resizable) return updateWidth();

    // calculate max width after parent's size was changed
    const resizeObserver = new ResizeObserver(() => updateWidth());
    resizeObserver.observe(parentElement!);

    // clear ResizeObserver
    return () => resizeObserver.disconnect();
  }, [resizable, children, rendered, parentElement, updateWidth]);

  // Update parentElement which TruncateText will follow
  useEffect(() => {
    const { parentElement } = containerRef.current as HTMLSpanElement;

    if (followRefProp?.current) setIsExcludeWrapperSpacing(true);

    setParentElement(followRefProp?.current || parentElement);
  }, [followRefProp]);

  // useDisplayTitle
  // set title for text when text is truncated or user click [show less]
  useEffect(() => {
    if (!autoShowTooltip) return;

    const newTitle = !isTruncated || showAll ? '' : titleProp;

    setTitle(newTitle);
  }, [autoShowTooltip, titleProp, isTruncated, showAll]);

  // set props
  useEffect(() => setShowAll(shownProp), [shownProp]);

  useEffect(() => setRendered(true), [ellipsisMoreText]);

  if (isEmpty(children) || !isString(children)) {
    return (
      <span
        ref={containerRef}
        className={classnames('dls-truncate-text', className)}
        {...props}
      />
    );
  }

  return (
    <span
      ref={containerRef}
      title={title}
      className={classnames('dls-truncate-text', className)}
      {...props}
    >
      <span ref={prefixRef}>{prefixElement}</span>
      <span
        ref={textRef}
        className={textClassName}
        id={id}
        data-testid={genAmtId(dataTestId, 'dls-truncate-text', 'TruncateText')}
      >
        {content}
      </span>
      <span
        ref={ellipsisRef}
        onClick={() => handleShownMode(!showAll)}
        className={classnames('link text-decoration-none', ellipsisClassName)}
      >
        {' '}
        {showAll ? ellipsisLessText : ellipsisMoreText}
      </span>
      <span ref={suffixRef}>{suffixElement}</span>
    </span>
  );
};

export default forwardRef<HTMLSpanElement, TruncateTextProps>(TruncateText);
