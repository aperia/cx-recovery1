import { AxiosRequestConfig, CancelTokenSource } from 'axios';
import { InputHTMLAttributes, ReactElement } from 'react';
import { StateFile } from './File';
import { Props as FilesProps } from './ListFile';

export interface IDX {
  idx: string;
}

export interface PromiseFile {
  status: string;
  value?: IDX;
  reason?: IDX;
}
interface RenderProps {
  files: StateFile[];
  onRemoveFile?: (index: number) => void;
  isEditName?: boolean;
  isEditDescription?: boolean;
  typeFiles?: string[];
  maxFile?: number;
  minFile?: number;
  isCancelUploadingFile?: boolean;
}
export interface UploadProps
  extends Omit<InputHTMLAttributes<HTMLInputElement>, 'ref'>,
    Omit<FilesProps, 'files' | 'onRemoveFile' | 'className'>,
    DLSId {
  files?: StateFile[];

  className?: string;
  classNameListFile?: string;

  onFinally?: (result: PromiseFile[]) => void;
  onRemove?: (indexInFiles: number) => void;

  multiple?: boolean;
  inputConfig?: InputHTMLAttributes<HTMLInputElement>;
  saveUrl: string | undefined | any;

  renderElm?: (props: RenderProps) => ReactElement | null;
  renderFiles?: (props: RenderProps) => ReactElement | null;
  /**
   * any change file with call it
   */
  onChangeFile?: (data: { files: StateFile[] }) => void;
  /**
   * Get Form data to post in mode uncontrolled
   */
  formatFormData?: (formData: FormData) => FormData;
  /**
   * Config for upload mode uncontrolled
   */
  axiosOptions?: AxiosRequestConfig;
}

export interface UploadRef {
  containerElement: HTMLDivElement;
  inputElement: HTMLInputElement;
  uploadFiles: () => void;
  cancelTokenFiles: Record<string, CancelTokenSource>;
  files: StateFile[];
}
