import React, { ReactElement, useRef } from 'react';
import classNames from 'classnames';
import { MAX_FILE, MIN_FILE } from './helper';
import File, { Props as FileProps, StateFile } from './File';
import { genAmtId } from '../../utils';

export interface Props
  extends Omit<FileProps, 'className' | 'file' | 'index'>,
    DLSId {
  className?: string;
  classNameFile?: string;
  files: StateFile[];
  renderFile?: (props: FileProps) => ReactElement | null;
  inlineFileItem?: boolean;
}

const ListFile: React.FC<Props> = ({
  renderFile,
  files,
  onDownloadFile,
  renderDownloadFile,
  onRemoveFile,
  classNameFile,
  className,
  isEditFileName,
  isEditFileDescription,
  typeFiles,
  maxFile = MAX_FILE,
  minFile = MIN_FILE,
  isCancelUploadingFile,

  dataTestId
}) => {
  const containerRef = useRef<HTMLDivElement | null>(null);
  const hasFile = files.length > 0;

  return (
    <div
      className={classNames({ 'list-file': hasFile }, className)}
      ref={containerRef}
      data-testid={genAmtId(
        dataTestId,
        'dls-upload-list-file',
        'Upload.ListFile'
      )}
    >
      {files.map((file, index) => {
        const props = {
          className: classNameFile,
          key: file.idx,
          file,
          index,
          onDownloadFile,
          renderDownloadFile,
          onRemoveFile,
          isEditFileName,
          isEditFileDescription,
          typeFiles,
          maxFile,
          minFile,
          isCancelUploadingFile
        };
        return renderFile ? (
          renderFile(props)
        ) : (
          <File dataTestId={dataTestId} {...props} />
        );
      })}
    </div>
  );
};
export default ListFile;
