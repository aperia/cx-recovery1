import React from 'react';
import '@testing-library/jest-dom';
import { act, cleanup, render, screen } from '@testing-library/react';
import ListFile from './ListFile';

afterEach(cleanup);
import { queryByClass } from '../../test-utils/queryHelpers';
import '@testing-library/jest-dom';

describe('ListFile', () => {
  it('Render list Files', () => {
    const handleRemoveFile = jest.fn();
    const { container } = render(
      <ListFile
        onRemoveFile={handleRemoveFile}
        files={[
          {
            idx: '1',
            name: 'name',
            length: 1024,
            item: () => {
              return null;
            }
          }
        ]}
        typeFiles={[]}
        isEditFileName
      />
    );

    let cpmItemFile: any;
    act(() => {
      cpmItemFile = queryByClass(container, 'list-file');
    });

    expect(cpmItemFile).toBeInTheDocument();
  });

  it('Render custom list Files', () => {
    const handleRemoveFile = jest.fn();
    render(
      <ListFile
        onRemoveFile={handleRemoveFile}
        files={[
          {
            idx: '1',
            name: 'name',
            length: 1024,
            item: (index: number) => {
              return null;
            }
          }
        ]}
        typeFiles={[]}
        renderFile={() => <div data-testid="render-files" />}
        isEditFileName
      />
    );

    expect(screen.getByTestId('render-files')).toBeInTheDocument();
  });
});
