import { useContext } from 'react';

import InfoBarContext from './InfoBarContext';

const useInfoBar = () => useContext(InfoBarContext);

export default useInfoBar;
