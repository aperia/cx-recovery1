import useInfoBar from './useInfoBar';
import { renderHook } from '@testing-library/react-hooks';

describe('useInfoBar hook', () => {
  it('should', () => {
    const hook = renderHook(() => useInfoBar());
    expect(hook.result.current.isInsideInfoBar).toEqual(false);
  });
});
