import React, {
  RefForwardingComponent,
  useRef,
  useImperativeHandle,
  forwardRef,
  useState,
  useLayoutEffect
} from 'react';
import ReactDOM from 'react-dom';

// components
import ToastBase, { ToastBaseProps } from './ToastBase';

// utils
import { genAmtId } from '../../utils';
import classnames from 'classnames';
import isEmpty from 'lodash.isempty';

export interface ToastDataType {
  id: string;
  type?: 'attention' | 'success' | 'warning' | 'error';
  message?: string;
}

export type DirectionType =
  | 'top'
  | 'top-end'
  | 'top-start'
  | 'bottom'
  | 'bottom-end'
  | 'bottom-start';

export interface ToastProps
  extends Omit<ToastBaseProps, 'dataId' | 'onClose'>,
    DLSId {
  id?: string;
  data: ToastDataType[];
  direction?: DirectionType;
  onClose: (id: string) => void;
}

const Toast: RefForwardingComponent<HTMLDivElement, ToastProps> = (
  {
    id,
    data,
    direction = 'top',
    from,
    to,
    delay = 3000,
    duration = 250,
    autohide,
    closeButton,
    resizable,
    onClose,

    dataTestId
  },
  ref
) => {
  const containerRef = useRef<HTMLDivElement | null>(null);

  const [portal, setPortal] = useState<React.ReactNode>(null);

  useImperativeHandle(ref, () => containerRef.current!);

  useLayoutEffect(() => {
    const isTopDirection =
      direction === 'top' ||
      direction === 'top-start' ||
      direction === 'top-end';
    setPortal(
      ReactDOM.createPortal(
        <div
          ref={containerRef}
          className={classnames('dls-toast-group', direction)}
          id={id}
          data-testid={genAmtId(dataTestId, 'dls-toast', 'Toast')}
        >
          {data.map(({ id: dataId, message, type }) => {
            return (
              <ToastBase
                key={dataId}
                dataId={dataId}
                from={from}
                to={to}
                message={message}
                delay={duration + delay}
                duration={duration}
                autohide={autohide}
                type={type}
                anchor={containerRef.current!}
                onClose={() => onClose(dataId)}
                closeButton={closeButton}
                resizable={resizable}
                isTopDirection={isTopDirection}
              />
            );
          })}
        </div>,
        document.body
      )
    );
  }, [
    id,
    data,
    direction,
    from,
    to,
    duration,
    delay,
    autohide,
    closeButton,
    resizable,
    onClose,
    dataTestId
  ]);

  if (isEmpty(data)) return null;

  return portal as React.ReactElement;
};

export default forwardRef<HTMLDivElement, ToastProps>(Toast);
