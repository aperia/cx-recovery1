import React from 'react';

// components
import {
  Toast,
  ToastHeaderProps as BootstrapToastHeaderProps
} from 'react-bootstrap';

export interface ToastHeaderProps extends BootstrapToastHeaderProps {
  ref?: React.MutableRefObject<HTMLDivElement | null>;
}

const ToastHeader: React.FC<ToastHeaderProps> = ({
  closeButton = true,
  closeLabel = 'Close',
  bsPrefix,
  children,
  ref,
  ...props
}) => {
  return (
    <Toast.Header
      closeButton={closeButton}
      closeLabel={closeLabel}
      bsPrefix={bsPrefix}
      ref={ref}
      {...props}
    >
      {children}
    </Toast.Header>
  );
};

export default ToastHeader;
