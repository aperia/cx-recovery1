import React from 'react';

// components/types
import { Toast } from 'react-bootstrap';
import Icon, { IconProps } from '../../components/Icon';
import TruncateText from '../../components/TruncateText';

// utils
import { canvasTextWidth } from '../../utils';

export interface ToastBodyProps {
  as?: React.ElementType;
  bsPrefix?: string;
  type?: 'attention' | 'success' | 'warning' | 'error';
  showToastIcon?: boolean;
  showCloseIcon?: boolean;
  onClose?: () => void;
}

const ToastBody: React.FC<ToastBodyProps> = ({
  as,
  bsPrefix,
  type = 'success',
  showToastIcon = true,
  showCloseIcon = true,
  onClose,
  children
}) => {
  const iconMapping = {
    attention: 'megaphone',
    success: 'success',
    warning: 'warning',
    error: 'error'
  };

  const textMetrics = canvasTextWidth(children as string);

  return (
    <Toast.Body as={as} bsPrefix={bsPrefix}>
      <div className="d-flex align-items-center">
        {showToastIcon && (
          <Icon
            name={iconMapping[type] as IconProps['name']}
            size="5x"
            className="specific-icon"
          />
        )}
        <div className="content">
          {textMetrics && textMetrics.width > 640 ? (
            <TruncateText title={children as string} resizable lines={2}>
              {children}
            </TruncateText>
          ) : (
            <p>{children}</p>
          )}
        </div>
        {showCloseIcon && <Icon name="close" size="3x" onClick={onClose} />}
      </div>
    </Toast.Body>
  );
};

export default ToastBody;
