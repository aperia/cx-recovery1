import React, { ReactElement } from 'react';
import '@testing-library/jest-dom';
import { render } from '@testing-library/react';

// components
import ToastBody from './ToastBody';

// utils
import { queryByClass } from '../../test-utils/queryHelpers';

// mocks
import '../../test-utils/mocks/mockCanvas';

const renderComponent = (element: ReactElement) => {
  const wrapper = render(element);

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

describe('Render', () => {
  describe('render with truncateText', () => {
    it('should not render truncateText with short text', () => {
      const { baseElement } = renderComponent(
        <ToastBody>Short text</ToastBody>
      );

      expect(queryByClass(baseElement, /dls-truncate-text/)).toBeNull();
      expect(queryByClass(baseElement, /toast-body/)).toBeInTheDocument();
    });

    it('should render with truncate text', () => {
      const { baseElement } = renderComponent(
        <ToastBody>This is a long text</ToastBody>
      );

      expect(
        queryByClass(baseElement, /dls-truncate-text/)
      ).toBeInTheDocument();
      expect(queryByClass(baseElement, /toast-body/)).toBeInTheDocument();
    });
  });

  describe('render close icon', () => {
    it('should not render close icon', () => {
      const { baseElement } = renderComponent(
        <ToastBody showToastIcon={false} showCloseIcon={false}>
          Short text
        </ToastBody>
      );

      expect(queryByClass(baseElement, /icon-close/)).toBeNull();
    });

    it('should render close icon', () => {
      const { baseElement } = renderComponent(
        <ToastBody showToastIcon={false} showCloseIcon>
          Short text
        </ToastBody>
      );

      expect(queryByClass(baseElement, /icon-close/)).toBeInTheDocument();
    });
  });

  describe('render toast icon', () => {
    it('should not render toast icon', () => {
      const { baseElement } = renderComponent(
        <ToastBody showToastIcon={false} showCloseIcon={false}>
          Short text
        </ToastBody>
      );

      expect(queryByClass(baseElement, /icon/)).toBeNull();
    });

    it('should render toast icon success', () => {
      const { baseElement } = renderComponent(
        <ToastBody showToastIcon type="success" showCloseIcon={false}>
          Short text
        </ToastBody>
      );

      expect(queryByClass(baseElement, /icon-success/)).toBeInTheDocument();
    });

    it('should render toast icon warning', () => {
      const { baseElement } = renderComponent(
        <ToastBody showToastIcon type="warning" showCloseIcon={false}>
          Short text
        </ToastBody>
      );

      expect(queryByClass(baseElement, /icon-warning/)).toBeInTheDocument();
    });

    it('should render toast icon error', () => {
      const { baseElement } = renderComponent(
        <ToastBody showToastIcon type="error" showCloseIcon={false}>
          Short text
        </ToastBody>
      );

      expect(queryByClass(baseElement, /icon-error/)).toBeInTheDocument();
    });

    it('should render toast icon attention', () => {
      const { baseElement } = renderComponent(
        <ToastBody showToastIcon type="attention" showCloseIcon={false}>
          Short text
        </ToastBody>
      );

      expect(queryByClass(baseElement, /icon-megaphone/)).toBeInTheDocument();
    });
  });
});

describe('Actions', () => {
  it('onClose', () => {
    const onClose = jest.fn();

    const { baseElement } = renderComponent(
      <ToastBody showToastIcon={false} showCloseIcon onClose={onClose}>
        Short text
      </ToastBody>
    );

    queryByClass(baseElement, /icon-close/)!.click();

    expect(onClose).toBeCalled();
  });
});
