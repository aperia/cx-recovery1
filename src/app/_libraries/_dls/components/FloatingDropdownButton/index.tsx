import React, { RefForwardingComponent, useState } from 'react';

// components/types
import {
  FloatingDropdownButtonRef,
  FloatingDropdownButtonProps
} from './types';

// components
import Icon from '../Icon';
import Button from '../Button';
import { DropdownBaseGroup, DropdownBaseItem } from '../DropdownBase';
import DropdownList from '../DropdownList';

// utils
import classnames from 'classnames';
import { classes, genAmtId } from '../../utils';

const FloatingDropdownButton: RefForwardingComponent<
  FloatingDropdownButtonRef,
  FloatingDropdownButtonProps
> = (
  { children, onClickNavigation, id, dataTestId, popupBaseProps, ...props },
  ref
) => {
  const [container, setContainer] = useState<HTMLDivElement | null>(null);
  const [opened, setOpened] = useState<boolean>();

  const containerId = genAmtId(
    dataTestId,
    'dls-floating-dropdown-button',
    'FloatingDropdownButton'
  );

  return (
    <div
      ref={setContainer}
      className={classnames(
        classes.floatingDropdownButton.container,
        opened && 'active'
      )}
      id={id}
      data-testid={containerId}
    >
      <DropdownList
        variant="no-border"
        popupBaseProps={
          {
            reference: container,
            ...popupBaseProps
          } as any
        }
        onVisibilityChange={nextOpened => setOpened(nextOpened)}
        dataTestId={genAmtId(containerId, 'options', 'FloatingDropdownButton')}
        {...props}
      >
        {children}
      </DropdownList>
      <Button
        variant="icon-primary"
        onClick={onClickNavigation}
        dataTestId={genAmtId(
          containerId,
          'navigation-btn',
          'FloatingDropdownButton'
        )}
      >
        <Icon name="arrow-right" />
      </Button>
    </div>
  );
};

const FloatingDropdownButtonExtraStaticProp = React.forwardRef<
  FloatingDropdownButtonRef,
  FloatingDropdownButtonProps
>(FloatingDropdownButton) as React.ForwardRefExoticComponent<
  FloatingDropdownButtonProps & React.RefAttributes<FloatingDropdownButtonRef>
> & {
  Group: typeof DropdownBaseGroup;
  Item: typeof DropdownBaseItem;
};

FloatingDropdownButtonExtraStaticProp.Item = DropdownBaseItem;

export * from './types';

export {
  DropdownBaseGroup as FloatingDropdownButtonGroup,
  DropdownBaseItem as FloatingDropdownButtonItem
};

export default FloatingDropdownButtonExtraStaticProp;
