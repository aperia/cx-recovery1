import { HTMLAttributes } from 'react';
import { IconProps } from '../Icon';

export interface LabelProps
  extends Omit<HTMLAttributes<HTMLSpanElement>, 'asterisk ' | 'ref' | 'error'>,
    DLSId {
  asterisk?: boolean;
  errorIconProps?: IconProps;
  error?: boolean;
}
