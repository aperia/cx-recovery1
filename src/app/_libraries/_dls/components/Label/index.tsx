import React from 'react';

// components/types
import Icon from '../Icon';
import { LabelProps } from './types';

// utils
import get from 'lodash.get';
import classnames from 'classnames';
import { className, genAmtId } from '../../utils';

const FloatingLabel: React.RefForwardingComponent<
  HTMLSpanElement,
  LabelProps
> = (
  {
    children,
    error,
    asterisk,
    className: classNameProp,
    errorIconProps,

    id,
    dataTestId,
    ...props
  },
  ref
) => {
  const errorClassName = get(errorIconProps, 'className') as string;

  return (
    <span
      ref={ref}
      className={classnames(className.floatingLabel.CONTAINER, classNameProp)}
      id={id}
      data-testid={genAmtId(dataTestId, 'dls-floating-label', 'FloatingLabel')}
      {...props}
    >
      {children}
      {asterisk && <span className="asterisk">*</span>}
      {error && (
        <Icon
          name="error"
          size="3x"
          className={classnames(errorClassName)}
          {...errorIconProps}
        />
      )}
    </span>
  );
};

export * from './types';
export default React.forwardRef<HTMLSpanElement, LabelProps>(FloatingLabel);
