import '@testing-library/jest-dom';
import { render, RenderResult } from '@testing-library/react';
import React from 'react';
import FloatingLabel, { LabelProps } from '.';

describe('Test Label Component', () => {
  const mockLabelProp = {
    id: 'label-id',
    className: 'mock-class'
  };

  const renderLabel = (
    props?: Omit<LabelProps, 'children'>,
    children?: React.ReactElement | string
  ): RenderResult => {
    return render(
      <div>
        <FloatingLabel ref={() => {}} {...mockLabelProp} {...props}>
          {children || 'Mock Label'}
        </FloatingLabel>
      </div>
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Check render label', () => {
    const wrapper = renderLabel();
    const { getByText } = wrapper;

    const label = getByText(/Mock Label/i);
    expect(label).toBeInTheDocument();
    expect(label.className).toContain('mock-class');
  });
  it('Check render label with asterisk & error', () => {
    const wrapper = renderLabel(
      {
        asterisk: true,
        error: true,
        errorIconProps: {
          name: 'eye-hide'
        }
      },
      <b>Bold label</b>
    );

    const { getByText } = wrapper;
    expect(getByText(/Bold label/i)).toBeInTheDocument();

    const asterisk = wrapper.baseElement.querySelector(
      'span.asterisk.color-danger'
    );
    expect(asterisk).not.toBeInTheDocument();

    const errorIcon = wrapper.baseElement.querySelector('i.icon');
    expect(errorIcon).toBeInTheDocument();
    expect(errorIcon?.className).toContain('icon-eye-hide');
  });
});
