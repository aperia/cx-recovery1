export const white = '#fff';
export const blue = '#1f84f4';
export const blue_l08 = '#4699f6';
export const blue_l16 = '#6daff8';
export const blue_l24 = '#94c4fa';
export const blue_l32 = '#bbd9fc';
export const blue_l40 = '#e1effe';
export const blue_d08 = '#0b70df';
export const blue_d12 = '#0a66cc';

export const red = '#f72d1d';
export const red_l08 = '#f85244';
export const red_l16 = '#fa766c';
export const red_l24 = '#fb9b93';
export const red_l32 = '#fdbfbb';
export const red_l40 = '#fee4e2';
export const red_d08 = '#e31808';
export const red_d12 = '#cf1607';

export const grey = '#666666';
export const grey_l16 = '#8f8f8f';
export const grey_l40 = '#cccccc';

export const light_l04 = '#ccccd5';
export const light_l12 = '#e3e3e8';
export const light_l16 = '#eeeef1';

export const btnCSS = {
  display: 'inline-block',
  position: 'relative',
  'font-size': '1.4rem',
  'text-align': 'center',
  'text-decoration': 'none',
  'white-space': 'nowrap',
  'border-width': 'thin',
  'border-style': 'solid',
  'border-radius': '0.4rem',
  cursor: 'pointer',
  transition: 'background 0.3s',
  height: '3.6rem',
  'line-height': '2rem',
  'min-width': '8rem',
  'user-select': 'none'
};

export const btnColor = {
  'font-weight': 500,
  padding: '0.7rem 1.5rem'
};

export const btnPrimary = {
  color: white,
  'background-color': blue,
  'border-color': 'transparent'
};
export const btnPrimary_Focus = {
  'border-color': '#0a66cc',
  'box-shadow': '0 0 0 0.1rem #0a66cc'
};

export const btnSecondary = {
  color: '#666666',
  'background-color': '#fff',
  'border-color': '#ccccd5'
};
export const btnSecondary_Focus = {
  'border-color': '#eeeef1',
  'box-shadow': '0 0 0 0.1rem #ccccd5'
};

export const btnDanger = {
  color: '#fff',
  'background-color': '#f72d1d',
  'border-color': 'transparent'
};
export const btnDanger_Focus = {
  'border-color': '#cf1607',
  'box-shadow': '0 0 0 0.1rem #cf1607'
};

it('no test with this file', () => {
  expect(true).toBeTruthy();
});
