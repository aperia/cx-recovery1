import { PaginationProps } from 'react-bootstrap';
import { CONTAINER_WITH_GTE_992 } from './constants';

export interface pageSizeInfo {
  page: number;
  size: number;
}

export interface PaginationWrapperProps extends DLSId {
  totalItem: number;
  pageNumber?: number;
  numberDisplay?: number;
  className?: string;
  compact?: boolean;

  pageAble?: boolean;
  pageSize?: number[];
  pageSizeValue?: number;
  pageSizeText?: string;
  textInfo?: string;

  onChangePage?: (page: number) => void;
  onChangePageSize?: (pageInfo: pageSizeInfo) => void;
}

export interface PaperProps extends PaginationProps, DLSId {
  totalPage: number;
  pageNumber: number;
  modeDisplay: typeof CONTAINER_WITH_GTE_992;
  onClick: (page: number) => void;
}

export interface PaginationItemProps extends DLSId {
  disabled?: boolean;
  onClick?: () => void;
  children?: React.ReactNode;
}

export interface InfoPaginationProps extends DLSId {
  textInfo: string;
}

export interface PageNumberProps extends DLSId {
  pageSizes?: number[];
  modeDisplay: typeof CONTAINER_WITH_GTE_992;
  pageSizeValue?: number;
  onChangePageSize: (pageNumber: number) => void;
}
