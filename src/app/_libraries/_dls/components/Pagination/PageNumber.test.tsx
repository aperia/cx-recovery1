import React from 'react';
import { act, fireEvent, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import PageNumber from '../Pagination/PageNumber';
import { queryAllByClass, queryByClass } from '../../test-utils/queryHelpers';
import { CONTAINER_WITH_GTE_992 } from './constants';

jest.mock('../../utils/canvasTextWidth.ts');

describe('Test PaginationNumber', () => {
  it('Should have pageSize and haven"t prop text ', () => {
    const handleOnChangePageSize = jest.fn();
    jest.useFakeTimers();
    const { container, baseElement } = render(
      <PageNumber
        compact={true}
        pageSizes={[5, 10, 15]}
        pageSizeValue={10}
        onChangePageSize={handleOnChangePageSize}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const dropdownPageSize = queryByClass(container, /text-field-container/);
    act(() => {
      dropdownPageSize?.focus();
      dropdownPageSize?.click();
      jest.runAllTimers();
    });
    const queryItemDropdown = queryAllByClass(
      baseElement as HTMLElement,
      /label/
    );
    fireEvent.click(queryItemDropdown[0]);
    expect(handleOnChangePageSize).not.toBeCalled();
    expect(queryItemDropdown.length).toEqual(4);
  });

  it('Should have pageSize undefined', () => {
    const handleOnChangePageSize = jest.fn();
    jest.useFakeTimers();
    const { container, baseElement } = render(
      <PageNumber
        compact={true}
        pageSizeValue={10}
        onChangePageSize={handleOnChangePageSize}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const dropdownPageSize = queryByClass(container, /text-field-container/);
    act(() => {
      dropdownPageSize?.focus();
      dropdownPageSize?.click();
      jest.runAllTimers();
    });
    const queryItemDropdown = queryAllByClass(
      baseElement as HTMLElement,
      /label/
    );
    expect(handleOnChangePageSize).not.toHaveBeenCalled();
    expect(queryItemDropdown.length).toEqual(1);
  });

  it('Should have prop text', () => {
    const { container } = render(
      <PageNumber
        text={'text'}
        compact={false}
        pageSizeValue={10}
        onChangePageSize={jest.fn()}
        modeDisplay={CONTAINER_WITH_GTE_992}
      />
    );
    const queryClassText = queryByClass(container, /pagination-text/);
    expect(queryClassText?.textContent).toEqual('Results per page');
    expect(queryClassText).toBeInTheDocument();
  });
});
