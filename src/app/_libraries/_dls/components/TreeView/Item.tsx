import React from 'react';

// components
import Button from '../Button';
import CheckBox from '../CheckBox';
import Icon from '../Icon';
import ReactHighLightWords from 'react-highlight-words';
import { TreeViewItemProps } from './types';

// utils
import { isEmpty } from 'lodash';
import classnames from 'classnames';
import isEqual from 'react-fast-compare';
import { genAmtId, replaceSpecialCharacter } from '../../utils';

const Item: React.FC<TreeViewItemProps> = (props) => {
  const renderText = (args: Partial<TreeViewItemProps>) => {
    const { variant, label, highlightText } = args;
    if (variant !== 'text') return null;

    return (
      <label className="line-label">
        {highlightText ? (
          <ReactHighLightWords
            searchWords={replaceSpecialCharacter(highlightText).split(' ')}
            textToHighlight={label || ''}
            highlightClassName="dls-mark"
          />
        ) : (
          label
        )}
      </label>
    );
  };

  const renderCheckBox = (args: Partial<TreeViewItemProps>) => {
    const {
      variant,
      indeterminate,
      checked,
      onToggleCheck,
      readOnly,
      disabled,
      label,
      highlightText
    } = args;
    if (variant !== 'checkbox') return null;

    const handleOnChange = () => {
      onToggleCheck?.(props);
    };

    return (
      <CheckBox>
        <CheckBox.Input
          indeterminate={indeterminate}
          checked={checked}
          onChange={handleOnChange}
          readOnly={readOnly}
          disabled={disabled}
        />
        <CheckBox.Label>
          {highlightText ? (
            <ReactHighLightWords
              searchWords={replaceSpecialCharacter(highlightText).split(' ')}
              textToHighlight={label || ''}
              highlightClassName="dls-mark"
            />
          ) : (
            label
          )}
        </CheckBox.Label>
      </CheckBox>
    );
  };

  return (
    <div
      className={classnames('dls-treeview-item', {
        'has-group': props.children,
        expanded: props.expanded
      })}
      id={genAmtId(
        `${props.idProp!}_${props.label}`,
        'dls-treeview-item',
        'TreeViewItem'
      )}
    >
      <div className="dls-treeview-line">
        {!isEmpty(props.items) && (
          <Button
            onClick={() => props.onToggleExpand(props)}
            variant="icon-secondary"
            size="sm"
          >
            <Icon name={props.expanded ? 'minus' : 'plus'} />
          </Button>
        )}
        {renderText({
          variant: props.variant,
          label: props.label,
          highlightText: props.highlightText
        })}
        {renderCheckBox({
          variant: props.variant,
          onToggleCheck: props.onToggleCheck,
          indeterminate: props.indeterminate,
          checked: props.checked,
          readOnly: props.readOnly,
          disabled: props.disabled,
          label: props.label,
          highlightText: props.highlightText
        })}
      </div>
      {props.children && (
        <div className="dls-treeview-group">{props.children}</div>
      )}
    </div>
  );
};

export default React.memo(Item, isEqual);
