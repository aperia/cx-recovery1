import { TooltipProps } from '../Tooltip';

export interface TreeViewOnChangeEvent {
  value?: TreeViewValue[];
  activeItemId?: string;
  activeItemPath?: string[];
}

export interface TreeViewOnChange {
  (event: TreeViewOnChangeEvent): void;
}

export interface TreeViewValue {
  id: string;
  label?: string;
  indeterminate?: boolean;
  expanded?: boolean;
  checked?: boolean;
  items?: TreeViewValue[];
  readOnly?: boolean;

  /** not supported yet */
  disabled?: boolean;
  tooltipProps?: TooltipProps;
}

export interface TreeViewRef {}

export interface TreeViewProps extends DLSId {
  defaultValue?: TreeViewValue[];
  value?: TreeViewValue[];
  onChange?: TreeViewOnChange;
  variant?: 'text' | 'checkbox';
  highlightText?: string;
}

export interface TreeViewItemRef {}

export interface TreeViewItemProps extends TreeViewValue {
  onToggleExpand: (item: TreeViewItemProps) => void;
  onToggleCheck: (item: TreeViewItemProps) => void;
  path: string[];
  variant: TreeViewProps['variant'];
  highlightText?: TreeViewProps['highlightText'];
  children?:
    | React.ReactElement<TreeViewItemProps>
    | React.ReactElement<TreeViewItemProps>[];
  idProp?: string;
  dataTestId?: string;
}
