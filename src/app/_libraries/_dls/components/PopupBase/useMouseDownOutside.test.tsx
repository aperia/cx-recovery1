import React from 'react';

// testing library
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

// mocks
import '../../test-utils/mocks/mockCanvas';
import useMouseDownOutside from './useMouseDownOutside';

const Wrapper: React.FC<{ reference?: any; popupElement?: any }> = (props) => {
  useMouseDownOutside(
    { current: {} } as any,
    { opened: true, reference: { contains: () => true }, ...props } as any
  );

  return <div>Wrapper</div>;
};

describe('test useMouseDownOutside', () => {
  it('handleMouseDownOutside return if reference or popupElement is undefined', () => {
    const { baseElement, rerender, queryByText } = render(
      <Wrapper opened={false} />
    );
    userEvent.click(baseElement);

    rerender(<Wrapper reference={{ contains: () => true }} />);
    userEvent.click(baseElement);

    rerender(
      <Wrapper reference={{ contains: () => true }} popupElement={{}} />
    );
    userEvent.click(baseElement);

    rerender(
      <Wrapper
        reference={{ contains: () => false }}
        popupElement={{ contains: () => false }}
        storage={{ values: undefined }}
      />
    );
    userEvent.click(baseElement);

    rerender(
      <Wrapper
        reference={{ contains: () => false }}
        popupElement={{ contains: () => false }}
        storage={{ values: [] }}
      />
    );
    userEvent.click(baseElement);

    expect(queryByText('Wrapper')).toBeInTheDocument();
  });
});
