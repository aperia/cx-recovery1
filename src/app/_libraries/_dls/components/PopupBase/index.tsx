import React, {
  useState,
  useRef,
  useEffect,
  CSSProperties,
  useImperativeHandle
} from 'react';

// components/types
import { Placement } from '@popperjs/core';
import { CSSTransition } from 'react-transition-group';

// utils
import { className, genAmtId } from '../../utils';
import { useDOMRect, usePopper } from '../../hooks';
import classnames from 'classnames';
import { useStorage } from '../../providers';
import { isBoolean } from '../../lodash';
import { usePopoverContext } from '../Popover';

export interface PopupBaseRef {
  element?: HTMLElement;
}

export interface PopupBaseProps extends DLSId {
  opened?: boolean;
  onVisibilityChange?: (nextOpened: boolean) => void;
  reference: HTMLElement;
  /**
   * - Not support controlled/uncontrolled
   */
  placement?: Placement;
  /**
   * - Not support controlled/uncontrolled
   */
  onPlacementChange?: (nextPlacement: Placement) => void;
  /**
   * anchor element
   */
  anchor?: HTMLElement;
  /**
   * - When opened is true, popup element is not exists, because we just begin mounting element
   * - if you need access DOM popup element, access PopupBase ref from this event
   */
  onPopupShown?: () => void;
  onPopupClosed?: () => void;
  fluid?: boolean;
  duration?: number;
  popupBaseClassName?: string;
  popupAnimationClassName?: string;
  style?: CSSProperties;
  children?: React.ReactNode;
}

const PopupBase: React.RefForwardingComponent<PopupBaseRef, PopupBaseProps> = (
  {
    opened: openedProp,
    onVisibilityChange,
    reference,
    placement: placementProp = 'bottom-start',
    onPlacementChange,
    anchor,
    onPopupShown,
    onPopupClosed,
    fluid,
    duration = 300,
    popupBaseClassName,
    popupAnimationClassName,
    style,
    children,

    dataTestId
  },
  ref
) => {
  // contexts
  const storage = useStorage();
  const { opened: popoverOpened, keepMount: popoverKeepMount } =
    usePopoverContext();

  // refs
  const parentElementRef = useRef<HTMLElement | null>(null);
  const keepRef = useRef<Record<string, any>>({});
  const isOpenedBefore = useRef(false);

  // states
  const [opened, setOpened] = useState(!!openedProp);
  const [popupElement, setPopupElement] = useState<HTMLElement | null>(null);

  // provide ref
  useImperativeHandle(
    ref,
    () => ({
      element: popupElement!
    }),
    [popupElement]
  );

  // keep reference
  keepRef.current.onPlacementChange = onPlacementChange;
  keepRef.current.onPopupShown = onPopupShown;
  keepRef.current.onPopupClosed = onPopupClosed;

  // handle CSSTransition onEnter stage
  const handleOnEnter = (element: HTMLElement) => {
    // set first time enter
    if (!parentElementRef.current) {
      parentElementRef.current = element.parentElement;
    }

    if (anchor) return anchor.append(element);
    document.body.append(element);
  };

  // handle CSSTransition onExited stage
  const handleOnExited = (element: HTMLElement) => {
    parentElementRef.current?.append?.(element);
  };

  // set props
  useEffect(() => setOpened(!!openedProp), [openedProp]);

  // handle initial/re-initial popper instance
  const [popperInstance, initialStyles] = usePopper({
    reference,
    popper: popupElement!,
    defaultOptions: {
      placement: placementProp
    }
  });

  popperInstance?.update();

  // handle fluid width popup element
  const { width } = useDOMRect({
    element: reference,
    watch: Boolean(fluid),
    picks: ['width']
  });

  // remove component contains PopupBase will trigger remove PopupBase DOM from document
  useEffect(() => () => popupElement?.remove(), [popupElement]);

  /* istanbul ignore next */
  useEffect(() => {
    if (
      !isBoolean(popoverKeepMount) ||
      !isBoolean(popoverOpened) ||
      !popoverKeepMount ||
      popoverOpened
    ) {
      return;
    }

    popupElement?.remove();
  }, [popoverKeepMount, popoverOpened, popupElement]);

  // if the popup element is opened
  // we need to save it in a context (if the context exists)
  // popup elements in the context will be used for different situations
  // ex: check click outside
  useEffect(() => {
    if (
      !opened ||
      !popupElement ||
      !Array.isArray(storage.values) ||
      storage.values.indexOf(popupElement) !== -1
    ) {
      return;
    }

    storage.add?.(popupElement);
  }, [opened, popupElement, storage]);

  // if the popup element is closed
  // we need to remove it from the context (if the context exists)
  // popup elements in the context will be used for different situations
  // ex: check click outside for complex case
  //     <PopupBase>
  //        <DropdownList />
  //     </PopupBase>
  useEffect(() => {
    if (opened || !popupElement) return;

    storage.remove?.(popupElement);
  }, [opened, popupElement, storage]);

  // handle call onPopupShown when popup element is mounted in DOM
  useEffect(() => {
    if (!opened) return;

    process.nextTick(() => {
      // just call onPopupClosed when isOpenedBefore.current is true
      isOpenedBefore.current = true;

      const { onPopupShown } = keepRef.current;
      onPopupShown?.();
    });
  }, [opened]);

  // handle call onPopupClosed when popup element is unmounted in DOM
  useEffect(() => {
    if (!isBoolean(opened) || opened || !isOpenedBefore.current) return;

    process.nextTick(() => {
      // reset isOpenedBefore.current to false to prepare for next cycle open/close
      isOpenedBefore.current = false;

      const { onPopupClosed } = keepRef.current;
      onPopupClosed?.();
    });
  }, [opened]);

  // handle scroll to close popup
  useEffect(() => {
    if (!opened) return;

    // MultiSelect will dynamic height input, so scroll event auto triggered
    // const handleOpenedModeAfter = after(10, keepRef.current.handleOpenedMode);

    const handleOnScroll = (event: Event) => {
      const target = event.target as HTMLElement;

      // normal popup case (DropdownList/MultiSelect/DatePicker/DateRangePicker/ComboBox/DropdownButton)
      if (target && popupElement?.contains(target)) {
        return;
      }
      if (target && target.scrollTop === 0 && target.scrollLeft > 0) {
        return;
      }

      // AttributeSearch case (ComboBox control)
      if (target?.closest?.('.dls-control-popup')) return;

      (document.activeElement as HTMLElement)?.blur?.();
    };

    window.addEventListener('scroll', handleOnScroll, true);
    return () => {
      window.removeEventListener('scroll', handleOnScroll, true);
    };
  }, [opened, popupElement]);

  // handle placement change
  useEffect(() => {
    if (!popperInstance?.state.placement) return;

    keepRef.current.onPlacementChange?.(popperInstance?.state.placement);
  }, [popperInstance?.state.placement]);

  return (
    <div className="d-none">
      <CSSTransition
        in={opened}
        appear={opened}
        timeout={duration}
        mountOnEnter
        unmountOnExit
        onEnter={handleOnEnter}
        onExited={handleOnExited}
      >
        {(state) => {
          return (
            <div
              ref={setPopupElement}
              className={classnames(
                className.popupBase.CONTAINER,
                state,
                popupBaseClassName
              )}
              style={{
                ...style,
                ...initialStyles,
                width: width || 'auto'
              }}
              data-testid={genAmtId(dataTestId, 'dls-popup-base', 'PopupBase')}
            >
              <div
                className={classnames(
                  className.popupBase.ANIMATION_CONTAINER,
                  state,
                  popupAnimationClassName
                )}
                data-popper-placement={popperInstance?.state.placement}
                style={{
                  animationDuration: `${duration}ms`,
                  animationFillMode: 'forwards'
                }}
              >
                {children}
              </div>
            </div>
          );
        }}
      </CSSTransition>
    </div>
  );
};

export default React.forwardRef<PopupBaseRef, PopupBaseProps>(PopupBase);
