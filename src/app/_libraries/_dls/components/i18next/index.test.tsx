import React, { useState } from 'react';
import '@testing-library/jest-dom';
import I18nextProvider from './index';
import { render, screen } from '@testing-library/react';
import { useTranslation } from '../../hooks';

const TestTranslation = () => {
  const { t } = useTranslation();
  return <div>{t('txt_acc_nbr')}</div>;
};

const TestChangeLanguage = () => {
  const [lang, setLang] = useState({
    lang: 'en',
    data: {
      txt_change_language: 'Change Language',
      txt_acc_nbr: 'Account number'
    }
  });
  const a = () => {
    setLang({
      lang: 'en',
      data: {
        txt_acc_nbr: 'Account number',
        txt_change_language: 'Change Language'
      }
    });
  };
  return (
    <I18nextProvider initReactI18nextProps={true} resource={lang}>
      <div data-testid="click-change-language" onClick={a}>
        Change Language
      </div>
    </I18nextProvider>
  );
};

describe('Test i18next', () => {
  it('Test i18next With initReactI18nextProps is true', () => {
    render(
      <I18nextProvider
        initReactI18nextProps={true}
        resource={{ lang: 'en', data: { txt_acc_nbr: 'Account number' } }}
      >
        <TestTranslation />
      </I18nextProvider>
    );
    const queryByText = screen.queryByText('Account number')!;
    expect(queryByText.textContent).toEqual('Account number');
  });

  it('Test i18next With initReactI18nextProps is false', () => {
    render(
      <>
        <TestTranslation />
        <I18nextProvider
          resource={{ lang: 'en', data: { txt_acc_nbr: 'Account number' } }}
        >
          Test
        </I18nextProvider>
      </>
    );
    const queryByText = screen.queryByText('Account number')!;
    expect(queryByText.textContent).toEqual('Account number');
  });

  it('Test i18next With resource Empty', () => {
    render(
      <>
        <I18nextProvider>
          <TestTranslation />
        </I18nextProvider>
      </>
    );

    const queryByText = screen.queryByText('Account number')!;
    expect(queryByText?.textContent).toEqual(undefined);
  });

  it('Test i18next With change language', () => {
    render(<TestChangeLanguage />);
    const getButton = screen.queryByTestId('click-change-language')!;
    getButton.focus();
    getButton.click();
    expect(getButton).toBeInTheDocument();
  });
});
