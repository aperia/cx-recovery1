import React from 'react';

// test
import { render } from '@testing-library/react';
import { queryByClass } from '../../test-utils';
import '@testing-library/jest-dom';

// components
import InlineMessage from '.';

describe('Inline Message', () => {
  it('should render with text only', () => {
    const { container, getByText } = render(
      <InlineMessage ref={{ current: null }} variant="info">
        Text content
      </InlineMessage>
    );

    expect(getByText('Text content')).toBeTruthy();
    expect(queryByClass(container, 'icon')).toBeFalsy();
  });

  it('should render with icon info', async () => {
    const { container, getByText } = render(
      <InlineMessage variant="info" withIcon>
        Text content
      </InlineMessage>
    );

    expect(getByText('Text content')).toBeTruthy();
    expect(queryByClass(container, 'icon icon-megaphone')).toBeTruthy();
  });

  it('should render with icon warning', async () => {
    const { container, getByText } = render(
      <InlineMessage variant="warning" withIcon>
        Text content
      </InlineMessage>
    );

    expect(getByText('Text content')).toBeTruthy();
    expect(queryByClass(container, 'icon icon-warning')).toBeTruthy();
  });

  it('should render with icon danger', async () => {
    const { container, getByText } = render(
      <InlineMessage variant="danger" withIcon>
        Text content
      </InlineMessage>
    );

    expect(getByText('Text content')).toBeTruthy();
    expect(queryByClass(container, 'icon icon-error')).toBeTruthy();
  });
});
