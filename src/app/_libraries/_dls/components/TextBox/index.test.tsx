import React from 'react';
import '@testing-library/jest-dom';
import { cleanup, fireEvent, render, screen } from '@testing-library/react';
import TextBox from './';

afterEach(cleanup);
import { queryByClass } from '../../test-utils/queryHelpers';
import '@testing-library/jest-dom';
import { TextBoxRef } from './types';

describe('Textbox', () => {
  describe('Uncontrolled', () => {
    it('Basic', () => {
      const ref = React.createRef<TextBoxRef>();

      const { container } = render(
        <TextBox ref={ref} label="label" id="id" name="city" />
      );

      const textBox = screen.getByRole('textbox');
      expect(textBox).toBeInTheDocument();
      const label = queryByClass(container, 'dls-floating-label');
      expect(label).toBeInTheDocument();
    });

    it('prefix', () => {
      const prefix = 'prefix';
      const { container, rerender } = render(
        <TextBox prefix={prefix} label="label" />
      );

      expect(screen.getByText(prefix)).toBeInTheDocument();

      rerender(
        <TextBox
          prefix={<div className="new-prefix">prefix</div>}
          label="label"
        />
      );
      expect(queryByClass(container, 'new-prefix')).toBeInTheDocument();
    });

    it('suffix', () => {
      const suffix = 'suffix';
      const { container, rerender } = render(
        <TextBox suffix={suffix} label="label" />
      );

      expect(screen.getByText(suffix)).toBeInTheDocument();

      rerender(
        <TextBox
          suffix={<div className="new-suffix">suffix</div>}
          label="label"
        />
      );
      expect(queryByClass(container, 'new-suffix')).toBeInTheDocument();
    });
  });

  describe('Controlled', () => {
    it('display error tooltip', () => {
      render(
        <TextBox
          error={{ message: 'Error abc', status: true }}
          required
          label="label long long"
          autoFocus
          id="id"
        />
      );

      expect(screen.getByText('Error abc')).toBeInTheDocument();
    });
  });

  describe('action', () => {
    it('onBlur - onFocus - uncontrolled onChange', () => {
      const handleFocus = jest.fn();
      const handleBlur = jest.fn();
      const handleChange = jest.fn();
      render(
        <TextBox
          onBlur={handleBlur}
          onFocus={handleFocus}
          onChange={handleChange}
          label="label long long"
          id="id"
          name="city"
        />
      );

      const textBox = screen.getByRole('textbox');

      const eventFocus = { target: {} };
      fireEvent.focus(textBox, eventFocus);
      expect(handleFocus).toHaveBeenCalled();

      const eventBlur = { target: {} };
      fireEvent.blur(textBox, eventBlur);
      expect(handleBlur).toHaveBeenCalled();

      fireEvent.change(textBox, { target: { value: '1' } });
      expect(handleChange).toHaveBeenCalled();
      expect(textBox.getAttribute('value')).toBe('1');
    });

    it('onChange - controlled', () => {
      const handleChange = jest.fn();
      const { rerender } = render(
        <TextBox
          onChange={handleChange}
          label="label long long"
          id="id"
          name="city"
          value="12"
        />
      );

      const textBox = screen.getByRole('textbox');
      fireEvent.change(textBox, { target: { value: '1' } });
      expect(handleChange).toHaveBeenCalled();
      rerender(
        <TextBox
          onChange={handleChange}
          label="label long long"
          id="id"
          name="city"
          value="1"
        />
      );
      expect(textBox.getAttribute('value')).toBe('1');
    });

    it('click label', () => {
      const handleChange = jest.fn();
      const { container } = render(
        <TextBox
          onChange={handleChange}
          label="label long long"
          id="id"
          name="city"
          value="12"
        />
      );

      const textBox = screen.getByRole('textbox');
      const labelElement = queryByClass(container, /dls-floating-label/);
      labelElement?.click();

      expect(textBox).toHaveFocus();
    });
  });
});
