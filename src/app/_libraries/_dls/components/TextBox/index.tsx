import React, {
  useState,
  useMemo,
  useImperativeHandle,
  useEffect,
  useRef,
  useLayoutEffect
} from 'react';

// utils
import { className, genAmtId, truncateTooltip } from '../../utils';
import { nanoid } from 'nanoid';

// component
import Label from '../Label';
import Tooltip from '../Tooltip';

// helper
import classNames from 'classnames';
import isFunction from 'lodash.isfunction';
import pick from 'lodash.pick';

// types
import { TextBoxRef, TextBoxProps } from './types';

// hooks
import { useBlurOnScroll } from '../../hooks';

const TextBox: React.RefForwardingComponent<TextBoxRef, TextBoxProps> = (
  {
    className: classNameProp,
    errorTooltipProps,
    size,
    label,
    error,
    prefix,
    suffix,
    defaultValue,
    readOnly,
    disabled,
    autoFocus,
    required,
    value: valueProp,
    onBlur,
    onFocus,
    onChange,
    id,
    name,

    dataTestId,
    ...rest
  },
  ref
) => {
  // refs
  const containerRef = useRef<HTMLDivElement | null>(null);
  const inputRef = useRef<HTMLInputElement | null>(null);
  const floatingLabelRef = useRef<HTMLSpanElement | null>(null);
  const selectionRef = useRef<[number | null, number | null] | null>(null);

  // states
  const [value, setValue] = useState<typeof valueProp>();
  const [isFocus, setIsFocus] = useState(autoFocus || false);
  const [selection, setSelection] = useState<
    [number | null, number | null] | null
  >(null);

  const { message, status } = pick(error, 'message', 'status');
  const isOpenedTooltip = Boolean(status && message && isFocus);
  const isSmallSize = size === 'sm';
  const hasLabel = !!label && !isSmallSize;
  const isGroup = prefix || suffix;
  const isFloating = (hasLabel && isFocus && !readOnly) || isGroup;
  const isRequired = required && !readOnly && !disabled;

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    isFunction(onChange) &&
      onChange({ ...event, target: { ...event.target, id: id!, name: name! } });

    // controlled
    if (typeof valueProp === 'string') {
      selectionRef.current = [
        event.target.selectionStart,
        event.target.selectionEnd
      ];
      return;
    }

    // uncontrolled
    setValue(event.target.value);
  };

  const handleFocus = (event: React.FocusEvent<HTMLInputElement>) => {
    isFunction(onFocus) &&
      onFocus({ ...event, target: { ...event.target, id: id!, name: name! } });

    setIsFocus(true);
  };

  const handleBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    isFunction(onBlur) &&
      onBlur({ ...event, target: { ...event.target, id: id!, name: name! } });

    setIsFocus(false);
  };

  const prefixView = useMemo(() => {
    if (!prefix) return null;
    if (React.isValidElement(prefix)) return prefix;

    return <span className={className.textBox.PREFIX}>{prefix}</span>;
  }, [prefix]);

  const suffixView = useMemo(() => {
    if (!suffix) return null;
    if (React.isValidElement(suffix)) return suffix;

    return <span className={className.textBox.SUFFIX}>{suffix}</span>;
  }, [suffix]);

  // custom ref public
  useImperativeHandle(
    ref,
    () => ({
      inputElement: inputRef.current!,
      containerElement: containerRef.current!
    }),
    []
  );

  // add blur on scroll if Tooltip is opened && error && scrolled
  useBlurOnScroll(inputRef.current!, Boolean(status && isFocus));

  useLayoutEffect(() => {
    if (!selection || !inputRef.current) return;
    // update cursor
    const ref = inputRef.current as any;
    [ref.selectionStart, ref.selectionEnd] = selection;
  }, [selection]);

  // set props
  useEffect(() => {
    setValue(valueProp === null ? undefined : valueProp);
    setSelection(selectionRef.current);
  }, [valueProp]);

  // handle truncate tooltip label
  useEffect(() => truncateTooltip(floatingLabelRef.current!), [label]);

  const containerClassName = classNames(
    className.input.CONTAINER,
    className.textBox.CONTAINER,
    size,
    {
      [className.state.FLOATING]: isFloating,
      [className.state.FOCUSED]: isFocus,
      [className.state.DISABLED]: disabled,
      [className.state.ERROR]: status,
      [className.state.READ_ONLY]: readOnly,
      [className.input.SUFFIX]: !!suffix,
      [className.input.PREFIX]: !!prefix,
      [className.input.LABEL]: hasLabel
    },
    classNameProp
  );

  const containerId = genAmtId(dataTestId, 'dls-text-box', 'TextBox');

  return (
    <Tooltip
      opened={isOpenedTooltip}
      element={message}
      variant="error"
      placement="top-start"
      triggerClassName="d-block"
      dataTestId={genAmtId(containerId, 'text-box-error', 'TextBox')}
      {...errorTooltipProps}
    >
      <div
        ref={containerRef}
        className={containerClassName}
        data-testid={containerId}
      >
        {prefixView}
        <input
          ref={inputRef}
          value={value}
          onChange={handleChange}
          onFocus={handleFocus}
          onBlur={handleBlur}
          type="text"
          defaultValue={defaultValue}
          readOnly={readOnly}
          disabled={disabled}
          autoFocus={autoFocus}
          required={isRequired}
          autoComplete={nanoid()}
          data-testid={genAmtId(containerId, 'input', 'TextBox')}
          {...rest}
        />
        {hasLabel && (
          <Label
            ref={floatingLabelRef}
            asterisk={isRequired}
            error={status}
            onClick={() => inputRef.current?.focus()}
            dataTestId={genAmtId(containerId, 'label', 'TextBox')}
          >
            <span>{label}</span>
          </Label>
        )}
        {suffixView}
      </div>
    </Tooltip>
  );
};

export * from './types';
export default React.forwardRef<TextBoxRef, TextBoxProps>(TextBox);
