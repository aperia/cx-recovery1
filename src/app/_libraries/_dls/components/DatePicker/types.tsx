// components/types
import DatePicker from '.';
import { TooltipProps } from '../Tooltip';
import { MaskedTextBoxRef } from '../MaskedTextBox';
import { PopupBaseRef, PopupBaseProps } from '../PopupBase';
import { CalendarProps, CalendarTileProperties, Detail } from 'react-calendar';

export interface DatePickerChangeEvent
  extends React.ChangeEvent<typeof DatePicker> {
  target: EventTarget &
    typeof DatePicker & { value?: Date; id?: string; name?: string };
  value?: Date;
}

export interface DatePickerViewConfig {
  placeholder: string;
  format: string;
  minDetail?: MinDetail;
  maxDetail?: MaxDetail;
  label?: string;
  mask: (string | RegExp)[];
  validLength: number;
}

export interface DatePickerProps extends DLSId {
  value?: Date;
  onChange?: (event: DatePickerChangeEvent) => void;

  label?: React.ReactNode;
  size?: 'small';

  onBlur?: (event: React.FocusEvent) => void;
  onFocus?: (event: React.FocusEvent) => void;

  opened?: boolean;
  onVisibilityChange?: PopupBaseProps['onVisibilityChange'];
  onPopupShown?: PopupBaseProps['onPopupShown'];
  onPopupClosed?: PopupBaseProps['onPopupClosed'];

  readOnly?: boolean;
  disabled?: boolean;
  placeholder?: string;

  required?: boolean;
  error?: {
    message?: string;
    status?: boolean;
  };
  errorTooltipProps?: TooltipProps;

  popupBaseProps?: Omit<
    PopupBaseProps,
    | 'opened'
    | 'onVisibilityChange'
    | 'reference'
    | 'onPopupShown'
    | 'onPopupClose'
  >;

  id?: string;
  name?: string;
  autoFocus?: boolean;
  className?: string;

  minDate?: Date;
  maxDate?: Date;
  dateTooltip?: (
    date: Date,
    view?: Detail
  ) => React.ReactElement | undefined | null;
  disabledRange?: Array<{
    fromDate?: Date;
    toDate?: Date;
  }>;

  titleDisabled?: (
    props: CalendarTileProperties & { activeStartDate: Date }
  ) => boolean;

  defaultView?: 'month' | 'year';
  hideIcon?: boolean;
  locale?: CalendarProps['locale'];
  todayLabel?: string;
  currentMonthLabel?: string;
}

export interface DatePickerRef {
  containerElement?: HTMLDivElement | null;
  inputContainerElement?: HTMLDivElement | null;
  maskedTextBox?: MaskedTextBoxRef | null;
  popupBaseRef?: React.MutableRefObject<PopupBaseRef | null>;
}

export type MinDetail = CalendarProps['minDetail'];

export type MaxDetail = CalendarProps['maxDetail'];
