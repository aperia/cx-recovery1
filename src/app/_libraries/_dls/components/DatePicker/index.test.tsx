import React from 'react';

// testing library
import {
  render,
  screen,
  fireEvent,
  queryHelpers,
  RenderResult,
  act
} from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

// mocks
import '../../test-utils/mocks/mockCanvas';
import '../../test-utils/mocks/mockInnerText';
import mockDate from '../../test-utils/mockDate';
import DatePickerWrapper from './mock-test/DatePickerMock';

// helpers
import datetime from 'date-and-time';
import { queryAllByClass } from '../../test-utils/queryHelpers';
import { DatePickerProps } from './types';

// components
import Tooltip from '../Tooltip';
import CalendarFooter from './CalendarFooter';
import Button from '../Button';

// author: Nguyen Nguyen

let wrapper: RenderResult;

interface RenderComponentProps extends Omit<DatePickerProps, 'children'> {}

const yesterdayString = '05/11/2021';
const todayString = '05/12/2021';
const tomorrowString = '05/13/2021';

const yesterday = new Date(yesterdayString);
const today = new Date(todayString);
const tomorrow = new Date(tomorrowString);

const name = 'datePicked';

const renderComponent = (props: RenderComponentProps) => {
  wrapper = render(
    <div>
      <DatePickerWrapper
        name={name}
        value={today}
        label="DatePicker"
        {...props}
      />
    </div>
  );

  act(() => {
    jest.runAllTimers();
  });

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement,
    container: wrapper.container,
    rerender: (nextProps: DatePickerProps) =>
      wrapper.rerender(
        <div>
          <DatePickerWrapper
            name={name}
            value={today}
            label="DatePicker"
            {...nextProps}
          />
        </div>
      )
  };
};

const getNode = {
  get input() {
    return wrapper.baseElement.querySelector('input')!;
  },
  get textField() {
    return wrapper.baseElement.querySelector('.text-field-container')!;
  },
  get iconCalendar() {
    return wrapper.baseElement.querySelector('.icon-calendar')!;
  },
  get container() {
    return wrapper.baseElement.querySelector('.dls-date-picker')!;
  },
  // NGUYEN TODO
  containerWith: (className: string) => {
    const classNames = className.split(' ').join('.');
    return wrapper.baseElement.querySelector(`.dls-date-picker.${classNames}`);
  }
};

beforeEach(() => {
  jest.useFakeTimers();
  mockDate.set(today);
});

afterEach(() => {
  wrapper?.unmount();
});

const queryByDate = (date: Date | string) => {
  return queryHelpers.queryByAttribute(
    'aria-label',
    wrapper.baseElement as HTMLElement,
    datetime.format(new Date(date), 'MMMM D, YYYY')
  );
};

const queryByMonth = (date: Date | string) => {
  return queryHelpers.queryByAttribute(
    'aria-label',
    wrapper.baseElement as HTMLElement,
    datetime.format(new Date(date), 'MMMM YYYY')
  );
};

const userEventType = (element: HTMLElement, text: string) => {
  userEvent.type(element, text);
  act(() => {
    jest.runAllTimers();
  });
};

const userEventClick = (element: HTMLElement) => {
  userEvent.click(element);
  act(() => {
    jest.runAllTimers();
  });
};

describe('Render', () => {
  it('should render element with class "dls-date-picker" with empty value', () => {
    renderComponent({ value: undefined });
    expect(getNode.container).toBeInTheDocument();
    expect(getNode.input.value).toEqual('');
  });

  it('render with year, decade, century defaultView', () => {
    renderComponent({ opened: true, defaultView: 'year' });

    // input value is 05/12/2012
    expect(getNode.input.value).toEqual('05/2021');
  });

  it('should render icon with class "icon-calendar"', () => {
    renderComponent({});
    expect(getNode.iconCalendar).toBeInTheDocument();
  });

  it('render no icon when hideIcon = true', () => {
    renderComponent({ hideIcon: true });
    expect(getNode.iconCalendar).not.toBeInTheDocument();
  });

  it('should have tooltip for cache date', () => {
    const { baseElement } = renderComponent({ opened: true });
    expect(
      queryAllByClass(baseElement as HTMLElement, /dls-tooltip-cache-date/)[0]
    ).toBeInTheDocument();
  });

  it('should render with readOnly', () => {
    renderComponent({ readOnly: true });

    expect(getNode.input.value).toEqual(todayString);
    expect(getNode.containerWith('dls-readonly')).toBeInTheDocument();
  });

  it('should render with disabled', () => {
    renderComponent({ disabled: true });

    expect(getNode.input.value).toEqual(todayString);
    expect(getNode.containerWith('dls-disabled')).toBeInTheDocument();
  });

  it('should render with required', () => {
    const { container } = renderComponent({ required: true });

    expect(container.querySelector('.asterisk')).toBeInTheDocument();
  });

  it('should render with small size', () => {
    const { container } = renderComponent({ size: 'small' });

    expect(container.querySelector('.dls-small')).toBeInTheDocument();
  });

  it('with disabledRange', () => {
    renderComponent({
      opened: true,
      disabledRange: [
        { fromDate: yesterday, toDate: yesterday },
        { fromDate: yesterday },
        { toDate: yesterday }
      ]
    });

    expect(queryByDate(yesterday)!.closest('button')).toBeDisabled();
  });

  it('without disabledRange', () => {
    renderComponent({
      opened: true,
      disabledRange: undefined
    });

    expect(true).toEqual(true);
  });

  it('with disabledRange in year defaultView', () => {
    renderComponent({
      opened: true,
      disabledRange: [
        { fromDate: new Date('05/01/2021'), toDate: new Date('05/30/2021') }
      ],
      defaultView: 'year'
    });

    expect(queryByMonth(today)!.closest('button')).toBeDisabled();
  });

  it('should swap when minDate > maxDate and render normally', () => {
    renderComponent({ opened: true, maxDate: yesterday, minDate: tomorrow });

    expect(true).toEqual(true);
  });

  it('should auto opened when autoFocus', () => {
    const onFocus = jest.fn();

    renderComponent({ autoFocus: true, onFocus });

    expect(onFocus).toBeCalled();
  });

  describe('Render when error', () => {
    const message = 'error message';

    it('when dropdown is unopened', () => {
      renderComponent({ error: { status: true, message } });

      expect(getNode.input.value).toEqual(todayString);
      expect(getNode.containerWith('dls-error')).toBeInTheDocument();
      expect(screen.queryByText(message)).toBeNull();
    });

    it('when dropdown is opened', () => {
      renderComponent({ error: { status: true, message }, opened: true });

      expect(getNode.input.value).toEqual(todayString);
      expect(
        getNode.containerWith('dls-focused dls-error')
      ).toBeInTheDocument();
      expect(screen.queryByText(message)).not.toBeNull();
    });
  });

  describe('Render CalendarFooter', () => {
    it('should render label with "Today" if label prop is undefined', () => {
      render(<CalendarFooter />);

      // temp
      expect(true).toBeTruthy();
    });
  });
});

describe('Actions', () => {
  describe('titleDisabled', () => {
    const titleDisabled = jest.fn();

    it('controlled', () => {
      renderComponent({ opened: true, value: undefined, titleDisabled });

      expect(titleDisabled).toBeCalled();
    });

    it('in year defaultView', () => {
      renderComponent({
        opened: true,
        value: undefined,
        defaultView: 'year',
        minDate: new Date('05/10/2021'),
        maxDate: new Date('09/10/2021')
      });

      expect(queryByMonth('04/10/2021')!.closest('button')).toBeDisabled();
      expect(queryByMonth('10/10/2021')!.closest('button')).toBeDisabled();
    });

    it('in month defaultView', () => {
      renderComponent({
        opened: true,
        value: undefined,
        defaultView: 'month',
        minDate: new Date('05/10/2021'),
        maxDate: new Date('05/20/2021')
      });

      expect(queryByDate('05/09/2021')!.closest('button')).toBeDisabled();
      expect(queryByDate('05/21/2021')!.closest('button')).toBeDisabled();
    });

    it('should call checkDisabled with month/year/decade/century view', () => {
      const { baseElement, rerender } = renderComponent({
        opened: true,
        value: undefined,
        defaultView: 'month',
        minDate: new Date('05/10/2021'),
        maxDate: new Date('05/20/2021')
      });

      // cover year view
      const calendarElement = baseElement.querySelector('.react-calendar');
      const navigationElement = calendarElement?.querySelector(
        '.react-calendar__navigation__label'
      );
      userEvent.click(navigationElement!);

      // cover century and decade
      rerender({
        opened: true,
        value: undefined,
        defaultView: 'month',
        minDate: new Date('05/10/2021'),
        maxDate: new Date('05/20/2050')
      });

      userEvent.click(navigationElement!);
      userEvent.click(navigationElement!);

      // temp
      expect(true).toBeTruthy();
    });
  });

  describe('handleOnChangeCalendar', () => {
    describe('without value', () => {
      it('input is focused', () => {
        const newDayString = '05/15/2021';

        renderComponent({
          opened: true,
          value: undefined
        });

        userEventClick(getNode.input);
        expect(document.activeElement === getNode.input).toBeTruthy();

        expect(queryByDate(newDayString)!).toBeInTheDocument();
        userEventClick(queryByDate(newDayString)!);

        expect(getNode.input.value).toEqual(newDayString);
      });

      it('has date input', () => {
        const newDayString = '05/15/2021';

        renderComponent({ opened: true, value: today });

        userEventClick(queryByDate(newDayString)!);

        expect(getNode.input.value).toEqual(newDayString);
      });
    });
  });

  describe('handleOnViewChange', () => {
    it('should call setView', () => {
      const { baseElement } = renderComponent({ opened: true });

      const navigationElement = baseElement.querySelector(
        '.react-calendar__navigation__label'
      );
      userEvent.click(navigationElement!);

      expect(true).toBe(true);
    });
  });

  describe('handleChange', () => {
    it('should return if readOnly is true', () => {
      const onChange = jest.fn();

      renderComponent({ readOnly: true, onChange });

      const inputElement = getNode.textField.querySelector('input');
      userEvent.type(inputElement!, '{0}');

      expect(onChange).not.toBeCalled();
    });

    it("dont't should return if readOnly is true", () => {
      const onChange = jest.fn();

      renderComponent({ readOnly: false, onChange });

      const inputElement = getNode.textField.querySelector('input');
      userEvent.type(inputElement!, '{0}');

      expect(onChange).not.toBeCalled();
    });

    it('should return if disabled is true', () => {
      const onChange = jest.fn();

      renderComponent({ disabled: true, onChange });

      const inputElement = getNode.textField.querySelector('input');
      userEvent.type(inputElement!, '{0}');

      expect(onChange).not.toBeCalled();
    });
  });

  describe('handleFocus', () => {
    const onFocus = jest.fn();

    it('should return', () => {
      renderComponent({ onFocus, value: today });

      const maskedTextBoxInput = getNode.textField.querySelector('input');
      fireEvent.focus(maskedTextBoxInput!);

      expect(onFocus).not.toBeCalled();
    });

    it('when disabled', () => {
      renderComponent({ onFocus, value: today, disabled: true });

      fireEvent.focus(getNode.textField);

      expect(onFocus).not.toBeCalled();
    });

    it('when readonly', () => {
      renderComponent({ onFocus, readOnly: true });

      fireEvent.focus(getNode.textField);

      expect(onFocus).not.toBeCalled();
    });

    it('when enabled', () => {
      renderComponent({ onFocus, value: today });

      fireEvent.focus(getNode.textField);

      expect(onFocus).not.toBeCalled();
    });

    it('when press TAB/SHIFT-TAB key', () => {
      renderComponent({ value: undefined });

      fireEvent.keyDown(document, {
        key: 'Tab',
        keyCode: 9,
        shiftKey: true
      });

      // just coverage
      fireEvent.keyDown(document, {
        key: 'Tab',
        keyCode: 10,
        shiftKey: true
      });

      expect(document.body).toEqual(document.activeElement);
    });

    describe('when enabled and autofocus', () => {
      it('with date input', () => {
        renderComponent({
          label: 'label',
          onFocus,
          value: today,
          autoFocus: true
        });

        userEvent.click(getNode.textField);

        expect(onFocus).toBeCalledWith(
          expect.objectContaining({ value: today })
        );
      });

      it('without date input', () => {
        renderComponent({
          label: 'label',
          onFocus,
          value: undefined,
          autoFocus: true
        });

        userEvent.click(getNode.textField);

        expect(onFocus).toBeCalled();
      });

      it('opened and without date input', () => {
        renderComponent({
          opened: true,
          label: 'label',
          onFocus,
          value: undefined,
          autoFocus: true
        });

        userEvent.click(getNode.textField);

        expect(onFocus).toBeCalled();
      });
    });
  });

  describe('handleBlur', () => {
    const onBlur = jest.fn();

    it('when clicked input date before blur', () => {
      renderComponent({ onBlur, value: today });

      userEvent.click(getNode.input);

      expect(getNode.input === document.activeElement).toBeTruthy();

      // trigger on blur
      fireEvent.blur(getNode.input);

      expect(getNode.input === document.activeElement).toBeFalsy();

      expect(onBlur).toBeCalledWith(expect.objectContaining({ value: today }));
    });

    it('when click input date and a different date button', () => {
      renderComponent({ onBlur, value: today, opened: true });

      userEvent.click(getNode.input);

      expect(getNode.input === document.activeElement).toBeTruthy();

      userEvent.click(queryByDate(yesterdayString)!);

      fireEvent.blur(getNode.input);
      expect(getNode.input === document.activeElement).toBeFalsy();

      expect(onBlur).toBeCalledWith(
        expect.objectContaining({ value: yesterday })
      );
    });
  });

  describe('handleOnChangeMaskedTextBox', () => {
    describe('when disabled', () => {
      it('without date value', () => {
        renderComponent({ value: undefined, disabled: true });

        userEventType(getNode.input, '05132021');

        expect(getNode.input.value).toEqual('');
      });

      it('already has date value', () => {
        renderComponent({ value: today, disabled: true });

        userEventType(getNode.input, tomorrowString);

        expect(getNode.input.value).toEqual(todayString);
      });
    });

    describe('when enabled', () => {
      it('typing correct format', () => {
        renderComponent({ value: undefined });

        userEventType(getNode.input, '05112021');

        expect(getNode.input.value).toEqual('05/11/2021');

        // typing backspace 1 time
        userEventType(getNode.input, '{backspace}');
        // unfocus input bar
        userEvent.click(document.body);
        expect(getNode.input.value).toEqual('05/11/2021');

        userEventType(
          getNode.input,
          new Array(10).fill('{backspace}').join('')
        );

        expect(getNode.input.value).toEqual('');
      });

      it('typing wrong format', () => {
        renderComponent({ value: undefined });

        userEventType(getNode.input, '99999999');
        expect(getNode.input.value).toEqual('');
      });

      it('when date input < minDate', () => {
        renderComponent({ value: undefined, minDate: tomorrow });

        userEvent.type(getNode.input, todayString);
        expect(getNode.input.value).toEqual(tomorrowString);
      });

      it('when date input > maxDate', () => {
        renderComponent({ value: undefined, maxDate: yesterday });

        userEvent.type(getNode.input, todayString);
        expect(getNode.input.value).toEqual(yesterdayString);
      });
    });
  });

  describe('handleOnMouseDownIcon', () => {
    const onVisibilityChange = jest.fn();
    describe('when disabled', () => {
      it('with date input', () => {
        renderComponent({ value: today, onVisibilityChange, disabled: true });
        userEvent.click(getNode.textField);
        expect(onVisibilityChange).not.toBeCalled();
      });

      it('without date input', () => {
        renderComponent({
          value: undefined,
          onVisibilityChange,
          disabled: true
        });
        userEvent.click(getNode.textField);
        expect(onVisibilityChange).not.toBeCalled();
      });
    });
    describe('when enabled', () => {
      describe('body was focused', () => {
        it('with date input', () => {
          renderComponent({ value: today, onVisibilityChange });
          userEvent.click(getNode.iconCalendar);
          expect(getNode.input === document.activeElement).toBeTruthy();
          expect(onVisibilityChange).toBeCalledWith(true);
        });
        it('without date input', () => {
          renderComponent({ value: undefined, onVisibilityChange });
          userEvent.click(getNode.iconCalendar);
          expect(getNode.input === document.activeElement).toBeTruthy();
          expect(onVisibilityChange).toBeCalledWith(true);
        });
        it('opened and without date input', () => {
          const onPopupShown = jest.fn();
          renderComponent({
            opened: true,
            value: undefined,
            onVisibilityChange,
            onPopupShown
          });
          userEvent.click(getNode.iconCalendar);
          expect(getNode.input === document.activeElement).toBeTruthy();
          expect(onVisibilityChange).toBeCalledWith(true);
          expect(onPopupShown).toBeCalled();
        });
      });
      it('input date was focus', () => {
        renderComponent({ opened: true, value: undefined, onVisibilityChange });
        userEvent.click(getNode.input);
        expect(getNode.input === document.activeElement).toBeTruthy();
        userEvent.click(getNode.iconCalendar);
        expect(getNode.input === document.activeElement).toBeFalsy();
        expect(onVisibilityChange).toBeCalledWith(true);
      });
    });
  });

  describe('handleClickButtonFooter', () => {
    describe('when there is no date input', () => {
      it('when today is greater than maxDate', () => {
        renderComponent({
          opened: true,
          value: undefined,
          maxDate: yesterday
        });

        fireEvent.click(screen.getByText('Today'));

        expect(getNode.input.value).toEqual('');
      });

      it('when today is less than minDate', () => {
        renderComponent({
          opened: true,
          value: undefined,
          minDate: tomorrow
        });

        fireEvent.click(screen.getByText('Today'));

        expect(getNode.input.value).toEqual('');
      });

      it('when today is within minDate and maxDate', () => {
        renderComponent({
          opened: true,
          value: undefined,
          minDate: yesterday,
          maxDate: tomorrow
        });

        fireEvent.click(screen.getByText('Today'));

        expect(getNode.input.value).toEqual(todayString);
      });
    });
    describe('when already has date input', () => {
      it('should change value of date input to today', () => {
        renderComponent({ opened: true, value: yesterday });

        fireEvent.click(screen.getByText('Today'));

        expect(getNode.input.value).toEqual(todayString);
      });
    });
  });
});

describe('Hooks', () => {
  describe('useDateTooltipPortals', () => {
    it('should run dateTooltip feature', () => {
      renderComponent({
        opened: true,
        dateTooltip: (date) => <Tooltip element="Date tooltip" />
      });

      expect(true).toBeTruthy();
    });

    it('should return if dateTooltip return undefined', () => {
      renderComponent({
        opened: true,
        dateTooltip: (date) => undefined
      });

      expect(true).toBeTruthy();
    });

    it('should call onClickTrigger if date is clicked', () => {
      const { baseElement } = renderComponent({
        opened: true,
        dateTooltip: (date) => <Tooltip element="Date tooltip" />
      });

      const calendarElement = baseElement.querySelector('.react-calendar');

      const tooltipTrigger = calendarElement?.querySelector(
        '.dls-tooltip-date-picker'
      );
      userEvent.click(tooltipTrigger!);

      expect(true).toBeTruthy();
    });

    it('should cover century/decade switch case', () => {
      const { baseElement } = renderComponent({
        opened: true,
        dateTooltip: (date) => <Tooltip element="Date tooltip" />
      });

      const calendarElement = baseElement.querySelector('.react-calendar');
      const navigationElement = calendarElement?.querySelector(
        '.react-calendar__navigation__label'
      );

      userEvent.click(navigationElement!);
      userEvent.click(navigationElement!);

      expect(true).toBeTruthy();
    });

    it('should cover handlePopupClosed and apply next tabIndex is fakeFocusRef.current', () => {
      jest.useFakeTimers();
      const { container, queryByText } = render(
        <div>
          <DatePickerWrapper
            label="DatePicker"
            onPopupClosed={() => undefined}
          />
          <Button autoBlur={false}>Test handleOnPopupClosed</Button>
        </div>
      );

      const inputElement = container.querySelector(
        '.text-field-container input'
      )!;

      const buttonElement = queryByText('Test handleOnPopupClosed')!;
      userEvent.click(inputElement);
      jest.runAllTimers();

      userEvent.click(document.body);
      jest.runAllTimers();

      userEvent.click(inputElement);
      jest.runAllTimers();

      userEvent.click(buttonElement);
      jest.runAllTimers();

      expect(true).toBeTruthy();
    });
  });
});
