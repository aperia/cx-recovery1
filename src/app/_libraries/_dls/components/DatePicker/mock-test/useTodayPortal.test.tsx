import React from 'react';

// testing library
import { act, render } from '@testing-library/react';
import '@testing-library/jest-dom';

// mocks
import '../../../test-utils/mocks/mockCanvas';

// hooks
import { useTodayPortal } from '../hooks';

const WrapperTodayPortalHook: React.FC<any> = ({
  dependencyShouldUndefined = true
}) => {
  useTodayPortal(
    { current: { element: document.createElement('div') } },
    () => null,
    dependencyShouldUndefined
      ? (undefined as any)
      : { opened: true, viewConfig: {}, isDisabled: false, view: 'month' }
  );

  return null;
};

describe('useTodayPortal', () => {
  it('should cover 100%', () => {
    jest.useFakeTimers();
    const { rerender } = render(<WrapperTodayPortalHook />);

    rerender(<WrapperTodayPortalHook dependencyShouldUndefined={false} />);

    act(() => {
      jest.runAllTimers();
    });

    expect(true).toBeTruthy();
  });
});
