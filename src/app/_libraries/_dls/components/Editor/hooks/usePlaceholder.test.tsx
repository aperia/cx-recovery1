// mock libs
import '@testing-library/jest-dom';
import usePlaceholder from './usePlaceholder';

describe('usePlaceholder', () => {
  it('contentBlockData is dls-indent', () => {
    expect(
      usePlaceholder({
        getCurrentContent: () => ({
          hasText: () => false,
          getBlockMap: () => ({
            first: () => ({
              getType: () => 'someType',
              getData: () => ({
                get: () => 'dls-indent'
              })
            })
          })
        })
      } as any)
    ).toBeTruthy();
  });

  it("contentBlockData is ['dls-indent']", () => {
    expect(
      usePlaceholder({
        getCurrentContent: () => ({
          hasText: () => false,
          getBlockMap: () => ({
            first: () => ({
              getType: () => 'unstyled',
              getData: () => ({
                get: () => ['dls-indent']
              })
            })
          })
        })
      } as any)
    ).toBeTruthy();
  });

  it('should cover all 100%', () => {
    expect(
      usePlaceholder({
        getCurrentContent: () => ({
          hasText: () => true,
          getBlockMap: () => ({
            first: () => ({
              getType: () => 'someType'
            })
          })
        })
      } as any)
    ).toBeTruthy();
  });
});
