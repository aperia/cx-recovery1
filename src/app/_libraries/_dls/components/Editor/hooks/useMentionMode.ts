import { useMemo } from 'react';

import { ToolKeys } from '../types';

export interface MentionModeHook {
  (tools?: ToolKeys): boolean;
}

const useMentionMode: MentionModeHook = (tools) => {
  const isMentionMode = useMemo(() => {
    if (!Array.isArray(tools)) return false;

    return tools.length === 1 && tools.indexOf('Mention') !== -1;
  }, [tools]);

  return isMentionMode;
};

export default useMentionMode;
