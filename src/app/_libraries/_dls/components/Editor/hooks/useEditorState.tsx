import React, { useLayoutEffect, useRef, useState } from 'react';

import { CompositeDecorator, DraftDecorator, EditorState } from 'draft-js';
import { convertFromHTML } from 'draft-convert';
import { convertFromHTMLConfig } from '../stuff';

export interface RenewEditorStateFn {
  (html?: string): void;
}

export interface EditorStateHook {
  (decorators: DraftDecorator[], maxPlainText?: number): {
    tupleEditorState: [
      EditorState,
      React.Dispatch<React.SetStateAction<EditorState>>,
      typeof maxPlainText
    ];
    renewEditorState: RenewEditorStateFn;
  };
}

const useEditorState: EditorStateHook = (decorators, maxPlainText) => {
  // refs
  const skipCreateEditorFirstTimeRef = useRef(false);

  // states
  const [editor, setEditorState] = useState<EditorState>(() => {
    const compositeDecorator = new CompositeDecorator(decorators);
    return EditorState.createEmpty(compositeDecorator);
  });

  const renewEditorState: RenewEditorStateFn = (html = '') => {
    const compositeDecorator = new CompositeDecorator(decorators);

    const nextEditorState = EditorState.createWithContent(
      convertFromHTML(convertFromHTMLConfig)(html),
      compositeDecorator
    );

    setEditorState(nextEditorState);
  };

  useLayoutEffect(() => {
    if (!skipCreateEditorFirstTimeRef.current) {
      skipCreateEditorFirstTimeRef.current = true;
      return;
    }

    const compositeDecorator = new CompositeDecorator(decorators);
    setEditorState(EditorState.createEmpty(compositeDecorator));
  }, [decorators]);

  return {
    tupleEditorState: [editor, setEditorState, maxPlainText],
    renewEditorState
  };
};

export default useEditorState;
