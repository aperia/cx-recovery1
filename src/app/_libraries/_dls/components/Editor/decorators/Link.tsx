import React from 'react';

import { ContentState } from 'draft-js';

export interface LinkProps {
  contentState: ContentState;
  entityKey: string;
  decoratedText: string;
}

export interface LinkData {
  webAddress?: string;
  title?: string;
  isOpenNewWindow?: boolean;
  target?: boolean;
}

const Link: React.FC<LinkProps> = ({
  contentState,
  entityKey,
  decoratedText,
  ...props
}) => {
  const entity = contentState.getEntity(entityKey);
  const entityData = entity.getData() as LinkData;

  return (
    <a
      href={entityData?.webAddress}
      title={entityData?.title}
      target={
        entityData?.target
          ? entityData?.isOpenNewWindow
            ? '_blank'
            : ''
          : undefined
      }
      rel={entityData?.target ? 'noreferrer' : undefined}
      className="link"
    >
      {props.children}
    </a>
  );
};

export default Link;
