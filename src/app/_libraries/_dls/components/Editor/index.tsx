import React, {
  SyntheticEvent,
  useImperativeHandle,
  useMemo,
  useRef,
  useState
} from 'react';

import {
  Editor as DraftEditor,
  EditorProps as DraftEditorProps,
  EditorState,
  RichUtils,
  ContentState,
  Modifier
} from 'draft-js';
import 'draft-js/dist/Draft.css';

// hooks
import {
  useCompositeDecorator,
  useEditorState,
  useMentionMode,
  usePlaceholder,
  usePlainTextChange,
  useTools
} from './hooks';
import { useDeepValue } from '../../hooks';

// contexts
import { EditorContext } from './contexts';

// components/types/fns
import {
  convertToHTML,
  convertFromHTML as convertFromHTMLDraftConvert
} from 'draft-convert';
import { blockStyleFn } from './fns';
import { convertFromHTMLConfig, convertToHTMLConfig } from './stuff';
import { ToolKeys } from './types';

// types
import Tooltip, { TooltipProps } from '../Tooltip';

// utils
import { flattenDeep, isFunction, pick } from '../../lodash';
import { isArray } from 'app/_libraries/_utils';

import classnames from 'classnames';
import { classes, genAmtId } from '../../utils';

// tools
import { Mention } from './tools';
import { MentionRef } from './tools/Mention';

export interface EditorContenteditableElement extends HTMLInputElement {}

export interface EditorRef {
  getHTML: () => string;
  setHTML: (html: string) => void;
}
export interface EditorProps extends DLSId {
  tools?: ToolKeys;
  placeholder?: string;
  onFocus?: React.FocusEventHandler<EditorContenteditableElement>;
  onBlur?: React.FocusEventHandler<EditorContenteditableElement>;

  mentionData?: any[];
  mentionTextField?: string;
  error?: {
    message?: string;
    status?: boolean;
  };
  errorTooltipProps?: TooltipProps;

  /**
   * - Just working on mention tool (only mention tool)
   */
  maxPlainText?: number;
  /**
   * - Just working on mention tool (only mention tool)
   */
  onPlainTextChange?: (nextPlainText: string) => void;

  name?: string;
  className?: string;
  readOnly?: boolean;
}

const Editor: React.RefForwardingComponent<EditorRef, EditorProps> = (
  {
    tools,
    placeholder = 'Type and format your text.',
    onFocus,
    onBlur,
    mentionData,
    mentionTextField,
    error,
    errorTooltipProps,

    readOnly = false,

    maxPlainText = -1,
    onPlainTextChange,

    id,
    dataTestId
  },
  ref
) => {
  const originTools = useDeepValue(tools);
  const flattenDeepTools = useMemo(
    () => flattenDeep(originTools),
    [originTools]
  ) as ToolKeys;
  const decorators = useCompositeDecorator(flattenDeepTools);
  const { tupleEditorState, renewEditorState } = useEditorState(
    decorators,
    maxPlainText
  );
  const editorStatePersisted = useDeepValue(tupleEditorState);
  const [editorState, setEditorState] = editorStatePersisted;
  const isMentionMode = useMentionMode(flattenDeepTools);
  const [focused, setFocused] = useState(false);

  // render tools
  const [toolElements, invisibleTools] = useTools(tools);

  // refs
  const [editorContainerElement, setEditorContainerElement] =
    useState<HTMLDivElement | null>(null);
  const mentionRef = useRef<MentionRef | null>(null);
  const coreRef = useRef<Record<string, any>>({});

  const contentStateToHTML = (contentState: ContentState) => {
    const html = convertToHTML(convertToHTMLConfig)(contentState);
    return html;
  };

  const handleKeyCommand = (command: string, fromEditorState: EditorState) => {
    if (isMentionMode) {
      return 'not-handled';
    }

    const nextEditorState = RichUtils.handleKeyCommand(
      fromEditorState,
      command
    );
    if (nextEditorState) {
      setEditorState(nextEditorState);
      return 'handled';
    }

    return 'not-handled';
  };

  const handleOnChange = (nextEditorState: EditorState) => {
    setEditorState(nextEditorState);
  };

  const handleFocus = (event: SyntheticEvent) => {
    isFunction(onFocus) && onFocus(event as any);

    setFocused(true);
  };

  const handleBlur = (event: SyntheticEvent) => {
    isFunction(onBlur) && onBlur(event as any);

    setFocused(false);
  };

  const handlePastedText: DraftEditorProps['handlePastedText'] = (
    text,
    html,
    editorState
  ) => {
    if (isMentionMode) {
      const selectedTextLength = getLengthOfSelectedText(editorState);
      const plainTextCurrent = editorState.getCurrentContent().getPlainText();
      let nextText = text;
      if (
        maxPlainText > 0 &&
        plainTextCurrent.length + selectedTextLength + text.length >
          maxPlainText
      ) {
        const remainLength =
          maxPlainText - plainTextCurrent?.length + selectedTextLength;
        nextText = text.slice(
          0,
          plainTextCurrent ? remainLength : maxPlainText
        );
      }

      const nextContentState = Modifier.replaceWithFragment(
        editorState.getCurrentContent(),
        editorState.getSelection(),
        convertFromHTMLDraftConvert(convertFromHTMLConfig)(
          nextText
        ).getBlockMap()
      );

      let nextEditorState = EditorState.push(
        editorState,
        nextContentState,
        'insert-fragment'
      );

      nextEditorState = EditorState.moveFocusToEnd(nextEditorState);
      setEditorState(nextEditorState);

      return 'handled';
    }

    const nextContentState = Modifier.replaceWithFragment(
      editorState.getCurrentContent(),
      editorState.getSelection(),
      convertFromHTMLDraftConvert(convertFromHTMLConfig)(
        html || text
      ).getBlockMap()
    );

    let nextEditorState = EditorState.push(
      editorState,
      nextContentState,
      'insert-fragment'
    );

    nextEditorState = EditorState.moveFocusToEnd(nextEditorState);
    setEditorState(nextEditorState);

    return 'handled';
  };

  const handleReturn: DraftEditorProps['handleReturn'] = () => {
    if (mentionRef.current?.isMentionShowing) {
      return 'handled';
    }

    return 'not-handled';
  };

  const handleBeforeInput: DraftEditorProps['handleBeforeInput'] = (
    characters,
    editorState
  ) => {
    if (!isMentionMode) return 'not-handled';

    const currentContent = editorState.getCurrentContent();
    const currentContentLength = currentContent.getPlainText('').length;
    const selectedTextLength = getLengthOfSelectedText(editorState);
    if (
      maxPlainText > 0 &&
      currentContentLength - selectedTextLength > maxPlainText - 1
    ) {
      return 'handled';
    }

    return 'not-handled';
  };

  const getLengthOfSelectedText = (nextEditorState: EditorState) => {
    const currentSelection = nextEditorState.getSelection();
    const isCollapsed = currentSelection.isCollapsed();
    let length = 0;

    if (!isCollapsed) {
      const currentContent = nextEditorState.getCurrentContent();
      const startKey = currentSelection.getStartKey();
      const endKey = currentSelection.getEndKey();
      const startBlock = currentContent.getBlockForKey(startKey);
      const isStartAndEndBlockAreTheSame = startKey === endKey;
      const startBlockTextLength = startBlock.getLength();
      const startSelectedTextLength =
        startBlockTextLength - currentSelection.getStartOffset();
      const endSelectedTextLength = currentSelection.getEndOffset();
      const keyAfterEnd = currentContent.getKeyAfter(endKey);
      if (isStartAndEndBlockAreTheSame) {
        length +=
          currentSelection.getEndOffset() - currentSelection.getStartOffset();
      } else {
        let currentKey = startKey;

        while (currentKey && currentKey !== keyAfterEnd) {
          if (currentKey === startKey) {
            length += startSelectedTextLength + 1;
          } else if (currentKey === endKey) {
            length += endSelectedTextLength;
          } else {
            length += currentContent.getBlockForKey(currentKey).getLength() + 1;
          }
          currentKey = currentContent.getKeyAfter(currentKey);
        }
      }
    }
    return length;
  };

  // core refs
  coreRef.current.onPlainTextChange = onPlainTextChange;

  useImperativeHandle(ref, () => {
    return {
      getHTML: () => {
        const contentState = editorState.getCurrentContent();
        let nextHTML = contentStateToHTML(contentState);

        if (isMentionMode) {
          nextHTML = nextHTML
            ?.split('<span class="dls-editor-mention-decorator">')
            .join('{');
          nextHTML = nextHTML?.split('</span>').join('}');
          const divElement = document.createElement('div');
          divElement.innerHTML = nextHTML!;

          return divElement.textContent!;
        }

        return nextHTML;
      },
      setHTML: renewEditorState
    };
  });

  usePlainTextChange(editorState, isMentionMode, onPlainTextChange);
  const [isShowPlaceholder] = usePlaceholder(editorState);

  const shouldRenderMention =
    (invisibleTools as any)?.some(
      (tool: typeof invisibleTools) => tool === 'Mention'
    ) && isArray(mentionData);

  // check should show error or not
  const { message, status } = pick(error, 'message', 'status');

  return (
    <EditorContext.Provider value={editorStatePersisted}>
      <div
        ref={setEditorContainerElement}
        className={classnames('dls-editor', {
          [classes.state.error]: Boolean(status),
          [classes.state.focused]: focused
        })}
        id={id}
        data-testid={genAmtId(dataTestId, 'dls-editor', 'Editor')}
      >
        <div
          aria-readonly={Boolean(readOnly)}
          className="dls-editor-tools-container"
        >
          {toolElements}
        </div>
        <Tooltip
          opened={Boolean(status && focused)}
          element={message}
          variant="error"
          placement="top-start"
          triggerClassName="d-block"
          dataTestId={`${dataTestId}-editor-error`}
          {...errorTooltipProps}
        >
          <DraftEditor
            readOnly={Boolean(readOnly)}
            editorState={editorState}
            onChange={handleOnChange}
            handleKeyCommand={handleKeyCommand}
            blockStyleFn={blockStyleFn}
            handlePastedText={handlePastedText}
            onFocus={handleFocus}
            onBlur={handleBlur}
            placeholder={isShowPlaceholder ? placeholder : ''}
            handleBeforeInput={handleBeforeInput}
            handleReturn={handleReturn}
          />
        </Tooltip>
        {shouldRenderMention && (
          <Mention
            ref={mentionRef}
            editorContainerElement={editorContainerElement}
            mentionData={mentionData}
            mentionTextField={mentionTextField}
          />
        )}
      </div>
    </EditorContext.Provider>
  );
};

const EditorForwardRef = React.forwardRef<EditorRef, EditorProps>(Editor);
export default EditorForwardRef;
