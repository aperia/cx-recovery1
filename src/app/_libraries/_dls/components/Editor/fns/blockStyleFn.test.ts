// mock libs
import '@testing-library/jest-dom';
import blockStyleFn from './blockStyleFn';

describe('fns', () => {
  it('blockStyleFn', () => {
    const contentBlock = {
      getData: () => ({
        get: (ky: string) => ky + ` className`
      })
    };

    expect(blockStyleFn(contentBlock as any)).toEqual(
      'BLOCK_TYPE_BLOCK_DATA_KY className TEXT_ALIGN_BLOCK_DATA_KY IN_OUT_DENT_BLOCK_DATA_KY'
    );
  });
});
