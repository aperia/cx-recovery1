// mock libs
import '@testing-library/jest-dom';
import getCurrentSelectedBlock from './getCurrentSelectedBlock';

describe('fns', () => {
  it('getCurrentSelectedBlock', () => {
    const { currentBlock } = getCurrentSelectedBlock({
      getSelection: () => ({
        getStartKey: () => 'startKy',
        getEndKey: () => 'endKy'
      }),
      getCurrentContent: () => ({
        getBlockMap: () => ({
          skipWhile: (args: (_: string, ky: string) => boolean) => {
            args('hi_', 'hiKy');

            return {
              takeWhile: (args2: (_2: string, ky2: string) => boolean) => {
                args2('hi_2', 'hiKy2');

                return {
                  some: (args3: (block: any) => boolean) => {
                    args3(null);

                    return false;
                  }
                };
              }
            };
          }
        }),
        getBlockForKey: () => 'getBlockForKey'
      })
    } as any);

    expect(currentBlock).toEqual('getBlockForKey');
  });
});
