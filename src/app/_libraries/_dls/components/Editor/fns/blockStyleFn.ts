import classnames from 'classnames/dedupe';

// block data keys
import {
  TEXT_ALIGN_BLOCK_DATA_KY,
  IN_OUT_DENT_BLOCK_DATA_KY,
  BLOCK_TYPE_BLOCK_DATA_KY
} from '../tools';

// handle style a block type when render
const blockStyleFn = (contentBlock: Draft.ContentBlock): string => {
  const currentBlockData = contentBlock.getData();

  // blockTypeClassName will has value when we run from BlockTypes tool itself
  const blockTypeClassName = currentBlockData.get(BLOCK_TYPE_BLOCK_DATA_KY);

  // textAlignClassName will has value when we run from TextAlign tool itself
  const textAlignClassName = currentBlockData.get(TEXT_ALIGN_BLOCK_DATA_KY);

  // indentClassName will has value when we run from Indent tool itself
  const inOutDentClassName = currentBlockData.get(IN_OUT_DENT_BLOCK_DATA_KY);

  // className will has value when we paste the content from outside
  // after, the content will be converted from convertFromHTMLConfig
  // at this time, contentBlock has:
  // data: {
  //   className: node.className
  // }
  const contentBlockClassName = currentBlockData.get('className');

  return classnames(
    blockTypeClassName,
    textAlignClassName,
    inOutDentClassName,
    contentBlockClassName
  );
};

export default blockStyleFn;
