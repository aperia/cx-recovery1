import { EditorState } from 'draft-js';

const getCurrentSelectedBlock = (editorState: EditorState) => {
  const selectionState = editorState.getSelection();
  const startKey = selectionState.getStartKey();
  const endKey = selectionState.getEndKey();

  const contentState = editorState.getCurrentContent();

  // atomic block is custom block
  const hasAtomicBlock = contentState
    .getBlockMap()
    .skipWhile((_, ky) => ky !== startKey)
    .takeWhile((_, ky) => ky !== endKey)
    .some((block) => block?.getType() === 'atomic');

  const currentBlock = contentState.getBlockForKey(startKey);

  return {
    contentState,
    currentBlock,
    hasAtomicBlock,
    selectionState
  };
};

export default getCurrentSelectedBlock;
