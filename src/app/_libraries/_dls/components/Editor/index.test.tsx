import React, { useEffect, useRef } from 'react';

// testing library
import { createEvent, fireEvent, render } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

// mocks
import '../../test-utils/mocks/mockCanvas';

// components
import Editor, { EditorRef } from '.';
import Button from '../Button';
import { keycode } from '../../utils';

import draft from 'draft-js';

const MockEditorWrapper: React.FC<any> = ({
  shouldSetHTML = false,
  ...props
}) => {
  const editorRef = useRef<EditorRef>();

  useEffect(() => {
    if (!shouldSetHTML) return;

    editorRef.current?.setHTML('<div class="h5">phat/</div');
  }, [shouldSetHTML]);

  return (
    <>
      <Editor
        tools={['BlockTypes', 'Bold', 'InOutdent']}
        ref={editorRef as any}
        error={{
          message: 'Editor error message',
          status: true
        }}
        {...props}
      />
      <Button onClick={() => editorRef.current?.getHTML()}>Get HTML</Button>
    </>
  );
};

const MockEditorMentionWrapper: React.FC<any> = ({
  getHTML,
  setHTML,
  ...props
}) => {
  const editorRef = useRef<EditorRef>();

  useEffect(() => {
    if (!setHTML) return;

    editorRef.current?.setHTML(setHTML);
  }, [setHTML]);

  return (
    <div>
      <Editor
        tools={['Mention']}
        ref={editorRef as any}
        error={{
          message: 'Editor error message',
          status: true
        }}
        mentionData={[
          { id: 1, description: 'Contact Name' },
          { id: 2, description: 'Contact Phone' },
          { id: 3, description: 'Operator ID' }
        ]}
        mentionTextField={'description'}
        {...props}
      />
      <Button
        onClick={() => {
          const html = editorRef.current?.getHTML();
          getHTML?.(html);
        }}
      >
        Get HTML
      </Button>
    </div>
  );
};

describe('Editor', () => {
  it('should cover contentStateToHTML', () => {
    const { queryByText } = render(<MockEditorWrapper />);

    const getHTMLButtonElement = queryByText('Get HTML')!;
    userEvent.click(getHTMLButtonElement);
  });

  it('should cover handleKeyCommand', () => {
    const { container } = render(<MockEditorWrapper />);
    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    ) as HTMLElement;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => 'abc'
      }
    });

    fireEvent(editorContainer, event);

    userEvent.type(editorContainer, '{ctrl}B');
    userEvent.type(editorContainer, '{backspace}');

    expect(true).toBeTruthy();
  });

  it('should cover handleOnFocus/handleOnBlur', () => {
    const handleFocus = jest.fn();
    const handleBlur = jest.fn();
    const { container, queryByText } = render(
      <MockEditorWrapper
        onFocus={handleFocus}
        onBlur={handleBlur}
        shouldSetHTML
      />
    );

    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    ) as HTMLElement;
    editorContainer.focus();

    const getHTMLButtonElement = queryByText('Get HTML')!;
    userEvent.click(getHTMLButtonElement);

    expect(handleFocus).toBeCalled();
    expect(handleBlur).toBeCalled();
  });

  it('should cover handleReturn', () => {
    Range.prototype.getBoundingClientRect = () => ({
      width: 100,
      height: 50,
      bottom: 0,
      left: 0,
      right: 0,
      top: 0,
      x: 0,
      y: 0,
      toJSON: () => undefined
    });
    const { container } = render(<MockEditorWrapper />);

    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    )!;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => 'abc/'
      }
    });

    fireEvent(editorContainer, event);

    fireEvent.keyDown(editorContainer, {
      key: 'ArrowDown',
      keyCode: keycode.DOWN
    });
    fireEvent.keyDown(editorContainer, {
      key: 'Enter',
      keyCode: keycode.ENTER
    });

    expect(true).toBeTruthy();
  });

  it('should cover 100%', () => {
    Range.prototype.getBoundingClientRect = () => ({
      width: 100,
      height: 50,
      bottom: 0,
      left: 0,
      right: 0,
      top: 0,
      x: 0,
      y: 0,
      toJSON: () => undefined
    });
    const { container } = render(<MockEditorWrapper />);

    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    )!;

    fireEvent.keyDown(editorContainer, {
      key: 'Enter',
      keyCode: keycode.ENTER
    });

    expect(true).toBeTruthy();
  });
});

describe('EditorMention', () => {
  it('should render Editor Mention', () => {
    const getHTML = jest.fn();
    const { queryByText } = render(
      <MockEditorMentionWrapper getHTML={getHTML} />
    );

    const getHTMLButtonElement = queryByText('Get HTML')!;
    userEvent.click(getHTMLButtonElement);

    expect(getHTML).toBeCalled();
  });

  it('should handleKeyCommand', () => {
    const { container } = render(<MockEditorMentionWrapper />);

    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    ) as HTMLElement;

    userEvent.type(editorContainer, '{ctrl}B');
    expect(true).toBeTruthy();
  });

  it('should handlePastedText with maxPlainText is 1', () => {
    const { container } = render(<MockEditorMentionWrapper maxPlainText={1} />);

    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    ) as HTMLElement;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => 'abc/'
      }
    });

    fireEvent(editorContainer, event);
    expect(true).toBeTruthy();
  });

  it('should handlePastedText with maxPlainText is 0', () => {
    jest.useFakeTimers();
    const { container } = render(<MockEditorMentionWrapper maxPlainText={0} />);

    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    ) as HTMLElement;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => 'abasdadasdas'
      }
    });

    fireEvent(editorContainer, event);
    jest.runAllTimers();
    expect(true).toBeTruthy();
  });

  it('should handlePastedText with maxPlainText is 10 and plainTextCurrent is empty string', () => {
    jest.useFakeTimers();
    const { container } = render(
      <MockEditorMentionWrapper maxPlainText={10} />
    );

    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    ) as HTMLElement;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => 'abasdadasdasdasdc'
      }
    });

    fireEvent(editorContainer, event);
    jest.runAllTimers();
    expect(true).toBeTruthy();
  });

  it('should handlePastedText with maxPlainText is 10 and plainTextCurrent is not empty string', () => {
    jest.useFakeTimers();
    const { container } = render(
      <MockEditorMentionWrapper maxPlainText={10} setHTML="Hello" />
    );

    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    ) as HTMLElement;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => 'abasdadasdasdasdc'
      }
    });

    fireEvent(editorContainer, event);
    jest.runAllTimers();
    expect(true).toBeTruthy();
  });

  it('should cover handleReturn', () => {
    jest.useFakeTimers();
    Range.prototype.getBoundingClientRect = () => ({
      width: 100,
      height: 50,
      bottom: 0,
      left: 0,
      right: 0,
      top: 0,
      x: 0,
      y: 0,
      toJSON: () => undefined
    });
    const { container } = render(<MockEditorMentionWrapper />);

    const editorContainer = container.querySelector(
      '.public-DraftEditor-content'
    )!;

    const event = createEvent.paste(editorContainer, {
      clipboardData: {
        types: ['text/plain'],
        getData: () => '/'
      }
    });

    fireEvent(editorContainer, event);

    fireEvent.keyDown(editorContainer, {
      key: 'ArrowDown',
      keyCode: keycode.DOWN
    });
    fireEvent.keyDown(editorContainer, {
      key: 'Enter',
      keyCode: keycode.ENTER
    });
    jest.runAllTimers();
    expect(true).toBeTruthy();
  });

  describe('getLengthOfSelectedText', () => {
    it('should call handleBeforeInput isMentionMode false', () => {
      const originEditor = draft.Editor;
      draft.Editor = ({ handleBeforeInput }) => {
        return (
          <div>
            <div className="DraftEditor-editorContainer"></div>
            <input />
            <Button
              onClick={() =>
                handleBeforeInput('a', {
                  getCurrentContent: () => ({
                    getPlainText: () => ({
                      length: 1
                    }),
                    getBlockForKey: () => ({
                      getLength: () => 1
                    }),
                    getKeyAfter: () => ''
                  }),
                  getSelection: () => ({
                    isCollapsed: () => false,
                    getStartKey: () => 0,
                    getEndKey: () => 0,
                    getEndOffset: () => 1,
                    getStartOffset: () => 1
                  })
                })
              }
            >
              handleBeforeInput
            </Button>
          </div>
        ) as any;
      };

      const wrapper = render(<MockEditorWrapper />);

      const handleBeforeInput = wrapper.queryByText('handleBeforeInput')!;
      userEvent.click(handleBeforeInput);

      draft.Editor = originEditor;
      expect(true).toBeTruthy();
    });

    it('should call getLengthOfSelectedText if', () => {
      const originEditor = draft.Editor;
      draft.Editor = ({ handleBeforeInput }) => {
        return (
          <div>
            <div className="DraftEditor-editorContainer"></div>
            <input />
            <Button
              onClick={() =>
                handleBeforeInput('a', {
                  getCurrentContent: () => ({
                    getPlainText: () => ({
                      length: 20
                    }),
                    getBlockForKey: () => ({
                      getLength: () => 1
                    }),
                    getKeyAfter: () => ''
                  }),
                  getSelection: () => ({
                    isCollapsed: () => false,
                    getStartKey: () => 0,
                    getEndKey: () => 0,
                    getEndOffset: () => 1,
                    getStartOffset: () => 1
                  })
                })
              }
            >
              handleBeforeInput
            </Button>
          </div>
        ) as any;
      };

      const wrapper = render(<MockEditorMentionWrapper maxPlainText={20} />);

      const handleBeforeInput = wrapper.queryByText('handleBeforeInput')!;
      userEvent.click(handleBeforeInput);

      draft.Editor = originEditor;
      expect(true).toBeTruthy();
    });

    it('should call getLengthOfSelectedText else', () => {
      const originEditor = draft.Editor;
      draft.Editor = ({ handleBeforeInput }) => {
        return (
          <div>
            <div className="DraftEditor-editorContainer"></div>
            <input />
            <Button
              onClick={() =>
                handleBeforeInput('a', {
                  getCurrentContent: () => ({
                    getPlainText: () => ({
                      length: 1
                    }),
                    getBlockForKey: () => ({
                      getLength: () => 1
                    }),
                    getKeyAfter: () => 4
                  }),
                  getSelection: () => ({
                    isCollapsed: () => false,
                    getStartKey: () => 1,
                    getEndKey: () => 2,
                    getEndOffset: () => 1,
                    getStartOffset: () => 1
                  })
                })
              }
            >
              handleBeforeInput
            </Button>
          </div>
        ) as any;
      };

      const wrapper = render(<MockEditorMentionWrapper />);

      const handleBeforeInput = wrapper.queryByText('handleBeforeInput')!;

      userEvent.click(handleBeforeInput);
      draft.Editor = originEditor;
      expect(true).toBeTruthy();
    });
  });

  it('should call getLengthOfSelectedText while else if', () => {
    const originEditor = draft.Editor;
    let keyAfter = 2;
    draft.Editor = ({ handleBeforeInput }) => {
      return (
        <div>
          <div className="DraftEditor-editorContainer"></div>
          <input />
          <Button
            onClick={() =>
              handleBeforeInput('a', {
                getCurrentContent: () => ({
                  getPlainText: () => ({
                    length: 1
                  }),
                  getBlockForKey: () => ({
                    getLength: () => 1
                  }),
                  getKeyAfter: () => {
                    const next = keyAfter++;
                    return next > 10 ? 2 : next;
                  }
                }),
                getSelection: () => ({
                  isCollapsed: () => false,
                  getStartKey: () => 1,
                  getEndKey: () => 10,
                  getEndOffset: () => 1,
                  getStartOffset: () => 1
                })
              })
            }
          >
            handleBeforeInput
          </Button>
        </div>
      ) as any;
    };
    const wrapper = render(<MockEditorMentionWrapper />);

    const handleBeforeInput = wrapper.queryByText('handleBeforeInput')!;

    userEvent.click(handleBeforeInput);
    draft.Editor = originEditor;
    expect(true).toBeTruthy();
  });
});
