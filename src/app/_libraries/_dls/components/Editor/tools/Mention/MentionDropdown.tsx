import React, { useEffect, useState } from 'react';

// utils
import { get, isObject } from 'lodash';

// components
import DropdownList, {
  DropdownListProps,
  DropdownListRef
} from '../../../DropdownList';
import { DropdownBaseChangeEvent } from '../../../DropdownBase';

export interface MentionDropdownRef extends DropdownListRef {}

export interface MentionDropdownProps
  extends Omit<DropdownListProps, 'children'> {
  mentionData?: any[];
  mentionTextField?: string;
}

const MentionDropdown: React.RefForwardingComponent<
  MentionDropdownRef,
  MentionDropdownProps
> = (
  {
    variant = 'no-border',
    opened,
    onChange,
    mentionData,
    mentionTextField,
    ...props
  },
  ref
) => {
  const [value, setValue] = useState(null);

  const getText = (value: any, textField: string) => {
    if (isObject(value)) {
      return get(value, textField);
    }

    return value;
  };

  const handleOnChange = (event: DropdownBaseChangeEvent) => {
    setValue(event.target.value);

    onChange?.(event);
  };

  // reset value
  useEffect(() => setValue(null), [opened]);

  return (
    <DropdownList
      ref={ref}
      value={value}
      onChange={handleOnChange}
      opened={opened}
      variant={variant}
      allowCallOnChangeByUpDown={false}
      textField={mentionTextField}
      tabIndex={-1}
      {...props}
    >
      {mentionData!.map((item, index) => {
        return (
          <DropdownList.Item
            key={index}
            value={item}
            label={getText(item, mentionTextField!)}
          />
        );
      })}
    </DropdownList>
  );
};

export default React.forwardRef<MentionDropdownRef, MentionDropdownProps>(
  MentionDropdown
);
