import ReactDOM from 'react-dom';
import React, { useEffect, useImperativeHandle, useRef, useState } from 'react';

import { EditorState, Entity, Modifier, SelectionState } from 'draft-js';

// hooks
import { useDeepValue } from '../../../../hooks';
import { useEditorContext } from '../../contexts';

// utils
import { get, isObject } from '../../../../lodash';

// components
import MentionDropdown, {
  MentionDropdownProps,
  MentionDropdownRef
} from './MentionDropdown';
export interface MentionRef {
  isMentionShowing?: boolean;
}
export interface MentionProps {
  editorContainerElement?: HTMLDivElement | null;
  mentionData?: any[];
  mentionTextField?: string;
}

const MENTION_ENTITY_KY = Entity.create('MENTION', 'SEGMENTED');

const Mention: React.RefForwardingComponent<MentionRef, MentionProps> = (
  {
    editorContainerElement,
    mentionData: mentionDataProp,
    mentionTextField: mentionTextFieldProp = ''
  },
  ref
) => {
  const [editorState, setEditorState, maxPlainText] = useEditorContext();
  const mentionData = useDeepValue(mentionDataProp);
  const mentionTextField = useDeepValue(mentionTextFieldProp);

  // refs
  const coreRef = useRef<Record<string, any>>({});

  // states
  const [mentionDropdown, setMentionDropdown] =
    useState<MentionDropdownRef | null>(null);
  const [opened, setOpened] = useState(false);
  const [portal, setPortal] = useState<React.ReactPortal | null>(null);

  // check at current selection has draft entity or not
  const hasDraftEntity = () => {
    const selectionState = editorState.getSelection();
    if (!selectionState.getHasFocus()) return false;

    const startKey = selectionState.getStartKey();
    const contentState = editorState.getCurrentContent();
    const block = contentState.getBlockForKey(startKey);
    const hasEntity = block.getEntityAt(selectionState.getStartOffset() - 1);

    return Boolean(hasEntity);
  };

  // get DOM position of the selection (to render MentionDropdown)
  const trigger = '/';
  const getPositionBySelection = () => {
    const selection = window.getSelection();

    if (!selection || selection.rangeCount === 0 || hasDraftEntity()) return;

    const range = selection.getRangeAt(0);

    // full text of the text selection
    // example: <div>nguyen pha/|n hoa phat</div>
    // => textContent = "nguyen phan hoa phat"
    // => range.startOffset = 11
    // => leftText = "nguyen pha/"
    const textContent = range.startContainer.textContent!;
    const leftText = textContent.substring(0, range.startOffset);

    // triggerIndex = 10
    const triggerIndex = range.startOffset - 1;
    if (triggerIndex === -1) return;

    // rightTextFromTrigger = "/"
    const rightTextFromTrigger = leftText.substring(triggerIndex);

    const parentElement = range.startContainer.parentElement;
    const draftEditorContainerElement = parentElement?.closest(
      '.DraftEditor-editorContainer'
    ) as HTMLElement;
    if (!draftEditorContainerElement) return;

    const draftEditorContainerPos =
      draftEditorContainerElement.getBoundingClientRect();
    const rangePosition = range.getBoundingClientRect();

    const relativeLeft = rangePosition.left - draftEditorContainerPos.left;
    const relativeTop =
      rangePosition.height + rangePosition.top - draftEditorContainerPos.top;

    return {
      text: rightTextFromTrigger,
      start: triggerIndex,
      end: range.startOffset,
      relativeLeft,
      relativeTop,
      isShow: rightTextFromTrigger === trigger
    };
  };

  const handleOnChangeMentionDropdown: MentionDropdownProps['onChange'] = (
    event
  ) => {
    const position = getPositionBySelection()!;

    const nextValue = event.target.value;
    const text = isObject(nextValue)
      ? get(nextValue, mentionTextFieldProp)
      : nextValue;

    let selection = editorState.getSelection();
    let nextEditorState = editorState;

    // can't simulate
    /* istanbul ignore next */
    if (!selection.getFocusOffset()) {
      const nativeSelection = window.getSelection()!;
      selection = selection.set(
        'focusOffset',
        nativeSelection.focusOffset
      ) as SelectionState;
      selection = selection.set(
        'anchorOffset',
        nativeSelection.anchorOffset
      ) as SelectionState;
      nextEditorState = EditorState.forceSelection(nextEditorState, selection);
    }

    const nextSelection = selection.set(
      'anchorOffset',
      selection.getFocusOffset() - position.text.length
    ) as SelectionState;

    // create next content state has mention entity
    const contentStateWithEntity = Modifier.replaceText(
      nextEditorState.getCurrentContent(),
      nextSelection,
      text,
      undefined,
      MENTION_ENTITY_KY
    );

    // apply mention entity to EditorState
    const editorStateWithEntity = EditorState.push(
      nextEditorState,
      contentStateWithEntity,
      'apply-entity'
    );

    const contentAfter = editorStateWithEntity.getCurrentContent();
    const plainTextLengthAfter = contentAfter.getPlainText().length;
    const space = ' ';

    if (plainTextLengthAfter + space.length > maxPlainText!) {
      setOpened(false);
      return;
    }

    const contentStateWithSpace = Modifier.insertText(
      contentStateWithEntity,
      editorStateWithEntity.getSelection(),
      space
    );

    const editorStateWithSpace = EditorState.push(
      editorStateWithEntity,
      contentStateWithSpace,
      'apply-entity'
    );

    setEditorState(editorStateWithSpace);
  };

  coreRef.current.getPositionBySelection = getPositionBySelection;
  coreRef.current.handleOnChangeMentionDropdown = handleOnChangeMentionDropdown;

  useImperativeHandle(ref, () => ({
    isMentionShowing: opened
  }));

  useEffect(() => {
    if (!editorContainerElement) return;

    const editableElement = editorContainerElement.querySelector(
      '.DraftEditor-editorContainer'
    ) as HTMLElement;

    const nextPortal = ReactDOM.createPortal(
      <MentionDropdown
        ref={setMentionDropdown}
        mentionData={mentionData}
        mentionTextField={mentionTextField}
        onChange={coreRef.current.handleOnChangeMentionDropdown}
        opened={opened}
      />,
      editableElement
    );

    setPortal(nextPortal);
  }, [editorContainerElement, opened, mentionData, mentionTextField]);

  useEffect(() => {
    if (!mentionDropdown) return;

    const position = coreRef.current.getPositionBySelection();
    if (!position) {
      return setOpened(false);
    }

    const containerElement = mentionDropdown.inputElement?.closest(
      '.dls-popper-trigger'
    ) as HTMLElement;
    containerElement.style.left = `${position.relativeLeft}px`;
    containerElement.style.top = `${position.relativeTop}px`;

    setOpened(position.isShow);
  }, [mentionDropdown, editorState]);

  return portal as any;
};

export default React.forwardRef<MentionRef, MentionProps>(Mention);
