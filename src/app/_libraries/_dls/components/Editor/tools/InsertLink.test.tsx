import React, { useEffect, useRef } from 'react';

// testing library
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

// mocks
import '../../../test-utils/mocks/mockCanvas';

// components
import Editor, { EditorRef } from '..';

const MockEditorWrapper: React.FC<any> = ({ shouldSetHTML = false }) => {
  const editorRef = useRef<EditorRef>();

  useEffect(() => {
    if (!shouldSetHTML) return;

    editorRef.current?.setHTML('<div class="h5">phat/</div');
  }, [shouldSetHTML]);

  return (
    <Editor
      tools={['BlockTypes', 'Bold', 'InsertLink']}
      ref={editorRef as any}
    />
  );
};

describe('InsertLink', () => {
  it('should cover 100%', () => {
    const { container, queryByText } = render(<MockEditorWrapper />);

    const iconLinkElement = container.querySelector('.icon-link')!;
    userEvent.click(iconLinkElement);

    const webAddressInputElement =
      queryByText('Web Address')!.parentElement!.querySelector('input')!;
    const titleInputElement =
      queryByText('Title')!.parentElement!.querySelector('input')!;
    const openLinkCheckboxElement = queryByText(
      'Open link in new window'
    )!.parentElement!.querySelector('input')!;

    userEvent.type(webAddressInputElement, 'Web Address text');
    userEvent.type(titleInputElement, 'Title text');
    userEvent.click(openLinkCheckboxElement);

    const insertButton = queryByText('Insert')!;
    userEvent.click(insertButton);

    userEvent.click(iconLinkElement);
    const closeButton = queryByText('Cancel')!;
    userEvent.click(closeButton);

    expect(webAddressInputElement).toHaveValue('Web Address text');
    expect(titleInputElement).toHaveValue('Title text');
  });
});
