import React from 'react';
// testing library
import {
  render,
  fireEvent,
  RenderResult,
  queryByTitle
} from '@testing-library/react';
import '@testing-library/jest-dom';
// mocks
import '../../../test-utils/mocks/mockCanvas';
// component
import Editor from '../index';
let wrapper: RenderResult;
const MockComponent: React.FC<any> = () => {
  return (
    <div>
      <Editor tools={['UnderLine']} />
    </div>
  );
};
const renderComponent = () => {
  jest.useFakeTimers();
  wrapper = render(<MockComponent />);
  jest.runAllTimers();
  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement,
    container: wrapper.container,
    rerender: () => wrapper.rerender(<MockComponent />)
  };
};
describe('UnderLine', () => {
  it('should render ', () => {
    renderComponent();
    expect(true).toBeTruthy();
  });
  it('when clicked, UnderLine button becomes active', () => {
    const { baseElement } = renderComponent();
    expect(
      queryByTitle(baseElement, 'UnderLine')?.classList.contains('active')
    ).toBeFalsy();
    fireEvent.mouseDown(queryByTitle(baseElement, 'UnderLine')!);
    jest.runAllTimers();
    expect(
      queryByTitle(baseElement, 'UnderLine')?.classList.contains('active')
    ).toBeTruthy();
    expect(true).toBeTruthy();
  });
});
