import React, { useEffect, useState } from 'react';

// components
import Button from '../../Button';

// hooks
import { useEditorContext } from '../contexts';

// utils
import Icon from '../../Icon';
import { getCurrentSelectedBlock } from '../fns';
import { EditorState, Modifier } from 'draft-js';

// others
import { Map } from 'immutable';

export interface InOutdentProps {}

// constants
const IN_OUT_DENT_BLOCK_DATA_KY = 'IN_OUT_DENT_BLOCK_DATA_KY';

const InOutdent: React.FC<InOutdentProps> = (props) => {
  const [editorState, setEditorState] = useEditorContext();

  const [isIndentActive, setIsIndentActive] = useState(false);

  const handleMouseDown = (
    event: React.MouseEvent<HTMLButtonElement, MouseEvent>,
    type: 'indent' | 'outdent'
  ) => {
    event.preventDefault();

    const selectedBlock = getCurrentSelectedBlock(editorState);
    const {
      contentState,
      currentBlock,
      // comment out, can't cover this case for unit test
      // hasAtomicBlock,
      selectionState
    } = selectedBlock;

    // comment out, can't cover this case for unit test
    // if (hasAtomicBlock || !currentBlock) return;

    const currentBlockData = currentBlock.getData();
    const currentBlockClassName = currentBlockData.get('className') as
      | string
      | undefined;

    const isIndent =
      Boolean(currentBlockData.get(IN_OUT_DENT_BLOCK_DATA_KY)) ||
      currentBlockClassName?.includes('dls-indent');
    const nextIndentClassName = isIndent ? '' : 'dls-indent';

    const nextMap: Record<string, string> = {
      [IN_OUT_DENT_BLOCK_DATA_KY]: nextIndentClassName
    };
    if (currentBlockClassName) {
      nextMap['className'] = currentBlockClassName.replace('dls-indent', '');
    }

    const nextEditorState = EditorState.push(
      editorState,
      Modifier.mergeBlockData(contentState, selectionState, Map(nextMap)),
      'change-block-data'
    );

    setEditorState(nextEditorState);
  };

  // if the caret changed, we need toggle indent state
  useEffect(() => {
    const selectionState = editorState.getSelection();
    const startKey = selectionState.getStartKey();
    const contentBlock = editorState
      .getCurrentContent()
      .getBlockForKey(startKey);
    const blockData = contentBlock.getData();

    // blockData.get(IN_OUT_DENT_BLOCK_DATA_KY) will work if we run Indent tool itself
    //
    // blockData.get(className) will work if convertFromHTMLConfig is run
    // convertFromHTMLConfig run: after pasted/setHTML
    const currentClassName = (blockData.get(IN_OUT_DENT_BLOCK_DATA_KY) ||
      blockData.get('className') ||
      '') as string;

    const nextIsActive = currentClassName.includes('dls-indent');

    setIsIndentActive(nextIsActive);
  }, [editorState]);

  return (
    <>
      <Button
        onMouseDown={(event) => handleMouseDown(event, 'indent')}
        className="dls-editor-tool"
        variant="icon-secondary"
        title="Indent"
        disabled={isIndentActive}
      >
        <Icon name="indent" />
      </Button>
      <Button
        onMouseDown={(event) => handleMouseDown(event, 'outdent')}
        className="dls-editor-tool"
        variant="icon-secondary"
        title="Outdent"
        disabled={!isIndentActive}
      >
        <Icon name="outdent" />
      </Button>
    </>
  );
};

export { IN_OUT_DENT_BLOCK_DATA_KY };
export default InOutdent;
