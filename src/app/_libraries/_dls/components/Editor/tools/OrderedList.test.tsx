import React, { useEffect, useRef } from 'react';

// testing library
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';
import userEvent from '@testing-library/user-event';

// mocks
import '../../../test-utils/mocks/mockCanvas';

// components
import Editor, { EditorRef } from '..';

const MockEditorWrapper: React.FC<any> = ({ shouldSetHTML = false }) => {
  const editorRef = useRef<EditorRef>();

  useEffect(() => {
    if (!shouldSetHTML) return;

    editorRef.current?.setHTML('<div class="h5">phat/</div');
  }, [shouldSetHTML]);

  return (
    <Editor
      tools={['BlockTypes', 'Bold', 'OrderedList']}
      ref={editorRef as any}
    />
  );
};

describe('OrderedList', () => {
  it('should cover 100%', () => {
    const { container } = render(<MockEditorWrapper />);

    const iconElement = container.querySelector('.icon-list-number')!;
    userEvent.click(iconElement);

    expect(true).toBeTruthy();
  });
});
