import React from 'react';

import { RichUtils } from 'draft-js';
import Button from '../../Button';

import { useEditorContext } from '../contexts';
import classnames from 'classnames';
import Icon from '../../Icon';

export interface UnderLineProps {}

const UnderLine: React.FC<UnderLineProps> = () => {
  const [editorState, setEditorState] = useEditorContext();

  const handleUnOrderedList: React.MouseEventHandler<HTMLButtonElement> = (
    event
  ) => {
    event.preventDefault();

    const nextEditorState = RichUtils.toggleInlineStyle(
      editorState,
      'UNDERLINE'
    );
    setEditorState(nextEditorState);
  };

  return (
    <Button
      onMouseDown={handleUnOrderedList}
      className={classnames('dls-editor-tool', {
        active: editorState.getCurrentInlineStyle().has('UNDERLINE')
      })}
      variant="icon-secondary"
      title="UnderLine"
    >
      <Icon name="underline" />
    </Button>
  );
};

export default UnderLine;
