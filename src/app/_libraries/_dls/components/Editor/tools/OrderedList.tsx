import React from 'react';

import { EditorState, RichUtils } from 'draft-js';
import Button from '../../Button';

import { useEditorContext } from '../contexts';
import Icon from '../../Icon';

export interface OrderedListProps {}

const OrderedList: React.FC<OrderedListProps> = () => {
  const [editorState, setEditorState] = useEditorContext();

  const handleUnOrderedList = () => {
    // maintain selection before invoke toggleBlockType
    const selectionState = editorState.getSelection();

    // toggleBlockType
    let nextEditorState = RichUtils.toggleBlockType(
      editorState,
      'ordered-list-item'
    );

    // apply selection maintained after invoke toggleBlockType
    nextEditorState = EditorState.forceSelection(
      nextEditorState,
      selectionState
    );

    // set new editorState
    setEditorState(nextEditorState);
  };

  return (
    <Button
      className="dls-editor-tool"
      onClick={handleUnOrderedList}
      variant="icon-secondary"
      title="OrderedList"
    >
      <Icon name="list-number" />
    </Button>
  );
};

export default OrderedList;
