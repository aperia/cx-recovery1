import { EditorState, RichUtils } from 'draft-js';
import React, { useEffect, useState } from 'react';
import Button from '../../Button';
import CheckBox from '../../CheckBox';
import Icon from '../../Icon';
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from '../../Modal';
import TextBox from '../../TextBox';
import { useEditorContext } from '../contexts';

export interface InsertLinkProps {
  target?: boolean;
}

const InsertLink: React.FC<InsertLinkProps> = ({ target = true }) => {
  // contexts
  const [editorState, setEditorState] = useEditorContext();

  // states
  const [webAddress, setWebAddress] = useState('');
  const [title, setTitle] = useState('');
  const [isOpenNewWindow, setIsOpenNewWindow] = useState(false);

  const [opened, setOpened] = useState(false);

  const handleOpenModal = () => setOpened(true);

  const handleCloseModal = () => setOpened(false);

  const handleCancel = handleCloseModal;

  // handle insert link
  const handleOk = () => {
    const contentState = editorState.getCurrentContent();

    const nextContentState = contentState.createEntity('LINK', 'MUTABLE', {
      webAddress,
      title,
      isOpenNewWindow
    });

    const lastEntityKy = nextContentState.getLastCreatedEntityKey();

    let nextEditorState = EditorState.set(editorState, {
      currentContent: nextContentState
    });

    nextEditorState = RichUtils.toggleLink(
      nextEditorState,
      nextEditorState.getSelection(),
      lastEntityKy
    );

    setEditorState(nextEditorState);

    setOpened(false);
  };

  // handle change webAddress
  const handleOnChangeWebAddress: React.ChangeEventHandler<HTMLInputElement> = (
    event
  ) => {
    setWebAddress(event.target.value);
  };

  // handle change title
  const handleOnChangeTitle: React.ChangeEventHandler<HTMLInputElement> = (
    event
  ) => {
    setTitle(event.target.value);
  };

  // handle change isOpenNewWindow
  const handleOnChangeIsOpenNewWindow = (
    event: React.ChangeEvent<HTMLInputElement>
  ) => {
    setIsOpenNewWindow(event.target.checked);
  };

  useEffect(() => {
    // const currentContent = editorState.getCurrentContent();
    // const currentSelection = editorState.getSelection();
    // const startKey = currentSelection.getStartKey();
    // console.log(
    //   '.....',
    //   currentContent.getBlockForKey(startKey).getEntityAt(0)
    // );
    // const blocksList = currentContent.getBlocksAsArray();
    // blocksList.forEach((block) => {
    //   block.findEntityRanges(
    //     (character) => {
    //       if (character.getEntity() === 'LINK') {
    //         return true;
    //       }
    //       return false;
    //     },
    //     (start, end) => {}
    //   );
    // });
  }, [editorState]);

  return (
    <>
      <Button
        className="dls-editor-tool"
        onClick={handleOpenModal}
        variant="icon-secondary"
        title="InsertLink"
      >
        <Icon name="link" />
      </Button>
      <Modal xs show={opened} animationDuration={500} placement="center">
        <ModalHeader border closeButton onHide={handleCloseModal}>
          <ModalTitle>Insert hyperlink</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <div className="dls-text-link-group">
            <span className="dls-text-link-group-label">Web Address</span>
            <TextBox
              value={webAddress}
              onChange={handleOnChangeWebAddress}
              size="sm"
            />
          </div>
          <div className="dls-text-link-group">
            <span className="dls-text-link-group-label">Title</span>
            <TextBox value={title} onChange={handleOnChangeTitle} size="sm" />
          </div>
          {target ? (
            <CheckBox>
              <CheckBox.Input
                checked={isOpenNewWindow}
                onChange={handleOnChangeIsOpenNewWindow}
              />
              <CheckBox.Label>Open link in new window</CheckBox.Label>
            </CheckBox>
          ) : null}
        </ModalBody>
        <ModalFooter
          cancelButtonText="Cancel"
          okButtonText="Insert"
          onCancel={handleCancel}
          onOk={handleOk}
        />
      </Modal>
    </>
  );
};

export default InsertLink;
