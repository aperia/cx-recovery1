declare module 'vanilla-text-mask' {
  export interface MaskFunction {
    (rawValue: string): string;
  }

  export interface PipeFunction {
    (conformedValue: string, config: any): string;
  }

  export interface ConformToMaskFunction {
    (text: string, mask: (string | RegExp)[] | MaskFunction, config?: any): {
      conformedValue: string;
    };
  }

  export interface MaskInputController {
    destroy: () => void;
    textMaskInputElement: {
      state: {
        previousConformedValue: string;
        previousPlaceholder: string;
      };
      update: () => void;
    };
  }

  export interface MaskInputFunction {
    (args: MaskInputArgs): MaskInputController;
  }

  export interface MaskInputArgs {
    inputElement: HTMLInputElement;
    mask: (string | RegExp)[] | MaskFunction;
    guide?: boolean;
    placeholderChar?: string;
    keepCharPositions?: boolean;
    pipe?: PipeFunction;
    showMask?: boolean;
  }

  export const maskInput: MaskInputFunction;

  export const conformToMask: ConformToMaskFunction;
}
