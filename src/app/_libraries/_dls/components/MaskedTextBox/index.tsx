import React, {
  useRef,
  useLayoutEffect,
  useState,
  useImperativeHandle,
  useEffect,
  useMemo,
  useCallback
} from 'react';

// components
import Label from '../Label';
import Tooltip from '../Tooltip';

// utils
import { conformToMask } from 'vanilla-text-mask';
import classnames from 'classnames';
import {
  keycode,
  getIndexDiff,
  className,
  extendsEvent,
  genAmtId,
  truncateTooltip
} from '../../utils';
import {
  get,
  isFunction,
  isNumber,
  isObject,
  isString,
  pick
} from '../../lodash';
import { nanoid } from 'nanoid';

// types
import {
  MaskedTextBoxRef,
  MaskedTextBoxProps,
  MaskedTextBoxReferenceRef
} from './types';

// hooks
import { useBlurOnScroll } from '../../hooks';

const MaskedTextBox: React.RefForwardingComponent<
  MaskedTextBoxRef,
  MaskedTextBoxProps
> = (
  {
    name,
    id,
    type = 'text',
    mask,
    guide = true,
    placeholderChar = '_',
    keepCharPositions = false,
    pipe,
    value: valueProp,
    onChange,
    onKeyDown,
    onFocus,
    onBlur,

    label,
    className: classNameProp,
    readOnly,
    disabled,
    required,
    error,
    small,

    errorTooltipProps,

    dataTestId,
    ...props
  },
  ref
) => {
  // refs
  const inputRef = useRef<HTMLInputElement | null>(null);
  const isPasteRef = useRef(false);
  const keepSelectionStartRef = useRef(-1);
  const keepReferenceRef = useRef<MaskedTextBoxReferenceRef>({});
  const floatingLabelRef = useRef<HTMLSpanElement | null>(null);

  // states
  const [value, setValue] = useState((valueProp as string) || '');
  const [focus, setFocus] = useState(false);

  // keep reference
  keepReferenceRef.current.onChange = onChange;
  keepReferenceRef.current.valueProp = valueProp as string;

  const floating = (label && focus && !readOnly) || value;
  const { message, status } = pick(error, 'message', 'status');
  const isOpenedTooltip = Boolean(status && message && focus);

  // return full mask
  const fullMask = useMemo(() => {
    const fullMaskList = mask.map((pattern) => {
      if (isObject(pattern) && isFunction(pattern.test)) {
        return placeholderChar;
      }
      return pattern;
    });

    return fullMaskList.join('');
  }, [mask, placeholderChar]);

  // handle remove by range (if selectionStart, selectionEnd is differnece)
  // return: new conformed value
  const handleRemoveByRange = (
    selectionStart: number,
    selectionEnd: number
  ) => {
    const patterns = mask.slice(selectionStart, selectionEnd);
    const newSubValue = patterns
      .map((pattern) => {
        if (isObject(pattern) && isFunction(pattern.test)) {
          return placeholderChar;
        }

        return pattern;
      })
      .join('');
    const nextConformedValue =
      value.slice(0, selectionStart) +
      newSubValue +
      value.slice(selectionEnd, value.length);

    return nextConformedValue;
  };

  // handle get raw value
  const getRawValue = (conformedValue: string): string | number => {
    const rawValue = mask.reduce((prev, curr, index) => {
      if (
        isObject(curr) &&
        isFunction(curr.test) &&
        conformedValue[index] !== placeholderChar
      ) {
        prev += conformedValue[index];
      }

      return prev;
    }, '');

    if (isNumber(+rawValue) && !isNaN(+rawValue)) {
      return +rawValue;
    }

    return rawValue as string;
  };

  // handle onChange controlled/uncontrolled
  const handleOnChangeMode = useCallback(
    (nextValue: string, nextRawValue: string | number) => {
      let nextEvent = extendsEvent({} as any, 'target', {
        name,
        id,
        value: nextValue,
        rawValue: nextRawValue
      });
      nextEvent = extendsEvent(nextEvent, null, {
        value: nextValue,
        rawValue: nextRawValue
      });

      const onChange = keepReferenceRef.current.onChange;
      const valueProp = keepReferenceRef.current.valueProp;

      // controlled mode
      if (isString(valueProp)) {
        return isFunction(onChange) && onChange(nextEvent);
      }

      // uncontrolled mode
      setValue(nextValue);
      isFunction(onChange) && onChange(nextEvent);
    },
    [id, name]
  );

  // example if input: (123) |456-___
  // when you press the delete button, output will be: (123) |_56-___
  // and if you keep pressing the delete button, output will be: (123) |__6-___
  const handleDeleteKeyboard = (
    event: React.KeyboardEvent<HTMLInputElement>
  ) => {
    // prevent default behavior of the delete keyboard
    event.preventDefault();

    const { conformedValue } = conformToMask(value, mask, {
      mask,
      guide,
      placeholderChar,
      keepCharPositions,
      pipe
    });

    const originSelectionStart = get(
      inputRef.current,
      'selectionStart',
      -1
    ) as number;
    let selectionStart = originSelectionStart;
    const selectionEnd = get(inputRef.current, 'selectionEnd', -1) as number;

    // not handle if the caret is in the last index
    if (
      selectionStart === selectionEnd &&
      selectionStart === conformedValue.length
    ) {
      return;
    }

    // has range
    if (
      selectionStart !== -1 &&
      selectionEnd !== -1 &&
      selectionStart !== selectionEnd
    ) {
      const nextConformedValue = handleRemoveByRange(
        selectionStart,
        selectionEnd
      );
      handleOnChangeMode(nextConformedValue, getRawValue(nextConformedValue));

      // keep selection start current and adjust it after that
      keepSelectionStartRef.current = selectionStart;

      return;
    }

    // has no range
    // if (selectionStart === -1) return; // never happen
    let pattern = mask[selectionStart];

    while (
      conformedValue[selectionStart] === placeholderChar ||
      !isObject(pattern) ||
      !isFunction(pattern.test)
    ) {
      selectionStart += 1;
      pattern = mask[selectionStart];

      if (selectionStart === conformedValue.length) {
        selectionStart = -1;
        break;
      }
    }

    // if (selectionStart === -1) return; // never happen

    const characters = conformedValue.split('');
    characters[selectionStart] = placeholderChar;
    const nextConformedValue = characters.join('');
    handleOnChangeMode(nextConformedValue, getRawValue(nextConformedValue));

    // keep selection start current and adjust it later
    keepSelectionStartRef.current = originSelectionStart;
  };

  // example if input is: (123) |456-___
  // when you press the backspace button, output will be: (123)| 456-___
  // and if you keep pressing the backspace button, output will be: (123|) 456-___
  // and so on..., output will be: (12|_) 456-___
  const handleBackspaceKeyboard = (
    event: React.KeyboardEvent<HTMLInputElement>
  ) => {
    // prevent default behavior of the backspace keyboard
    event.preventDefault();

    const { conformedValue } = conformToMask(value, mask, {
      mask,
      guide,
      placeholderChar,
      keepCharPositions,
      pipe
    });

    let selectionStart = get(inputRef.current, 'selectionStart') as number;
    const selectionEnd = get(inputRef.current, 'selectionEnd') as number;

    // has range
    if (
      selectionStart !== -1 &&
      selectionEnd !== -1 &&
      selectionStart !== selectionEnd
    ) {
      const nextConformedValue = handleRemoveByRange(
        selectionStart,
        selectionEnd
      );
      handleOnChangeMode(nextConformedValue, getRawValue(nextConformedValue));

      // keep selection start current and adjust it after that
      keepSelectionStartRef.current = selectionStart;

      return;
    }

    // has no range
    // if (selectionStart === -1) return; // never happen
    selectionStart -= 1;
    const pattern = mask[selectionStart];
    const character = conformedValue[selectionStart];

    if (
      isObject(pattern) &&
      isFunction(pattern.test) &&
      character !== placeholderChar
    ) {
      const characters = conformedValue.split('');
      characters[selectionStart] = placeholderChar;
      const nextConformedValue = characters.join('');
      handleOnChangeMode(nextConformedValue, '');

      // keep selection start current and adjust it after that
      keepSelectionStartRef.current = selectionStart;
      return;
    }

    // move caret position right now
    // if (selectionStart === -1) return; // never happen
    inputRef.current?.setSelectionRange(selectionStart, selectionStart);
  };

  // if the keyboard is not valid, keep the caret position
  // else move caret to valid position, and fill character
  // if has range, clear range and fill the first valid placeholder
  const handleOtherKeyboard = (
    event: React.KeyboardEvent<HTMLInputElement>
  ) => {
    const character = event.key;
    const selectionStart = get(inputRef.current, 'selectionStart') as number;
    const selectionEnd = get(inputRef.current, 'selectionEnd') as number;
    let nextConformedValue = '';

    // ignore if key code is arrow left/right tab/shift tab
    if (
      event.keyCode === keycode.LEFT ||
      event.keyCode === keycode.RIGHT ||
      event.keyCode === keycode.TAB ||
      keycode.isShiftTab(event.shiftKey, event.keyCode) ||
      keycode.isCopy(event.ctrlKey, event.keyCode) ||
      keycode.isPaste(event.ctrlKey, event.keyCode) ||
      !value
    ) {
      return;
    }

    let moveSelectionStart = selectionStart;
    let pattern = mask[moveSelectionStart];
    while (!isObject(pattern) || !isFunction(pattern.test)) {
      if (moveSelectionStart >= mask.length) break;
      pattern = mask[++moveSelectionStart];
    }

    if (isObject(pattern) && !pattern.test(character)) {
      return event.preventDefault();
    }

    // if has range, clear range and input first character
    if (
      selectionStart !== -1 &&
      selectionEnd !== -1 &&
      selectionStart !== selectionEnd
    ) {
      nextConformedValue = handleRemoveByRange(selectionStart, selectionEnd);

      const nextCharacters = nextConformedValue.split('');
      nextCharacters[moveSelectionStart] = character;
      nextConformedValue = nextCharacters.join('');

      handleOnChangeMode(nextConformedValue, getRawValue(nextConformedValue));

      // keep selection start current and adjust it after that
      keepSelectionStartRef.current = moveSelectionStart + 1;

      // prevent onChange trigger
      event.preventDefault();
    }
  };

  // handle delete keyboard, backspace, other keyboard
  const handleOnKeyDown = (event: React.KeyboardEvent<HTMLInputElement>) => {
    if (readOnly) return;

    // handle onKeyDown prop
    isFunction(onKeyDown) && onKeyDown(event);

    switch (event.keyCode) {
      case keycode.DELETE:
        return handleDeleteKeyboard(event);
      case keycode.BACKSPACE:
        return handleBackspaceKeyboard(event);
      default:
        handleOtherKeyboard(event);
    }
  };

  const handleOnPaste = () => {
    isPasteRef.current = true;
  };

  const handleOnChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const nextValue = get(event, ['target', 'value']) as string;
    let { conformedValue } = conformToMask(nextValue, mask, {
      mask,
      guide,
      placeholderChar,
      keepCharPositions,
      pipe
    });

    const previousValue = value || fullMask;
    const indexDiff = getIndexDiff(previousValue, conformedValue);

    if (
      previousValue[indexDiff] !== conformedValue[indexDiff] &&
      !isPasteRef.current
    ) {
      const previousCharacters = previousValue.split('');
      previousCharacters[indexDiff] = conformedValue[indexDiff];
      conformedValue = previousCharacters.join('');
    }

    handleOnChangeMode(conformedValue, getRawValue(conformedValue));
    indexDiff !== -1 &&
      !isPasteRef.current &&
      (keepSelectionStartRef.current = indexDiff + 1);
    isPasteRef.current = false;
  };

  const handleFocus = () => {
    let nextEvent = extendsEvent({} as any, 'target', { name, id, value });
    nextEvent = extendsEvent(nextEvent, null, { value });

    isFunction(onFocus) && onFocus(nextEvent);
    setFocus(true);
  };

  const handleBlur = () => {
    let nextEvent = extendsEvent({} as any, 'target', { name, id, value });
    nextEvent = extendsEvent(nextEvent, null, { value });

    isFunction(onBlur) && onBlur(nextEvent);
    setFocus(false);
  };

  // custom ref public
  useImperativeHandle(
    ref,
    () => ({
      inputElement: inputRef.current!
    }),
    []
  );

  // add blur on scroll if Tooltip is opened && error && scrolled
  useBlurOnScroll(inputRef.current!, Boolean(status && focus));

  // set props
  useEffect(() => {
    setValue((valueProp as string) || '');
  }, [valueProp]);

  // handle adjust caret position base on keepSelectionStartRef
  useLayoutEffect(() => {
    if (keepSelectionStartRef.current === -1 || !value) return;

    inputRef.current?.setSelectionRange(
      keepSelectionStartRef.current,
      keepSelectionStartRef.current
    );
    keepSelectionStartRef.current = -1;
  }, [value]);

  // handle reset value to empty string if value === mask
  useEffect(() => {
    if (value !== fullMask) return;

    handleOnChangeMode('', '');
  }, [value, fullMask, handleOnChangeMode]);

  // handle truncate tooltip label
  useEffect(() => truncateTooltip(floatingLabelRef.current!), [label]);

  const containerId = genAmtId(
    dataTestId,
    'dls-masked-text-box',
    'MaskedTextBox'
  );

  return (
    <Tooltip
      opened={isOpenedTooltip}
      element={message}
      variant="error"
      placement="top-start"
      triggerClassName="d-block"
      dataTestId={genAmtId(containerId, 'error', 'MaskedTextBox')}
      {...errorTooltipProps}
    >
      <div
        className={classnames(
          'dls-masked-textbox',
          focus && className.state.FOCUSED,
          readOnly && className.state.READ_ONLY,
          disabled && className.state.DISABLED,
          status && className.state.ERROR,
          !small && floating && className.state.FLOATING,
          classNameProp
        )}
        data-testid={containerId}
      >
        <div className="text-field-container">
          <input
            ref={inputRef}
            value={value}
            onChange={handleOnChange}
            onKeyDown={handleOnKeyDown}
            onPaste={handleOnPaste}
            onFocus={handleFocus}
            onBlur={handleBlur}
            readOnly={readOnly}
            disabled={disabled}
            autoComplete={nanoid()}
            type={type}
            data-testid={genAmtId(containerId, 'input', 'MaskedTextBox')}
            {...props}
          />
          {label && (
            <Label
              ref={floatingLabelRef}
              asterisk={required}
              error={status}
              onClick={() => inputRef.current?.focus()}
              dataTestId={genAmtId(containerId, 'label', 'MaskedTextBox')}
            >
              <span className="text-truncate">{label}</span>
            </Label>
          )}
        </div>
      </div>
    </Tooltip>
  );
};

export * from './types';
export { conformToMask };
export default React.forwardRef<MaskedTextBoxRef, MaskedTextBoxProps>(
  MaskedTextBox
);
