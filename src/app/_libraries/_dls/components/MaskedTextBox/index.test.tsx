import React from 'react';
import '@testing-library/jest-dom';
import { render, RenderResult, screen } from '@testing-library/react';

// components
import { queryByClass } from '../../test-utils/queryHelpers';
import userEvent from '@testing-library/user-event';

import MockMaskedTextBoxWrapper, {
  MaskedTextBoxWrapperProps
} from './MockMaskedTextBoxWrapper';

interface RenderComponentProps extends Omit<MaskedTextBoxWrapperProps, 'mask'> {
  mask?: (string | RegExp)[];
}

let wrapper: RenderResult;

const maskDefault = [
  /\d/,
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  '-',
  /\d/,
  /\d/,
  /\d/,
  /\d/
];

const renderComponent = ({
  mask = maskDefault,
  ...props
}: RenderComponentProps) => {
  wrapper = render(
    <MockMaskedTextBoxWrapper mask={mask} label="label" {...props} />
  );

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

const getNode = {
  get container() {
    return queryByClass(
      wrapper.baseElement as HTMLElement,
      /dls-masked-textbox/
    );
  },
  get input() {
    return screen.getByRole('textbox') as HTMLInputElement;
  },
  containerWith: (className: string) => {
    return queryByClass(
      wrapper.baseElement as HTMLElement,
      new RegExp(`dls-masked-textbox ${className}`)
    );
  }
};

describe('Render', () => {
  it('should render', () => {
    renderComponent({ mask: maskDefault });

    expect(getNode.container).toBeInTheDocument();
  });

  it('should render when readOnly', () => {
    renderComponent({ readOnly: true });

    expect(getNode.containerWith('dls-readonly')).toBeInTheDocument();
  });

  it('should render when readOnly', () => {
    renderComponent({ disabled: true });

    expect(getNode.containerWith('dls-disabled')).toBeInTheDocument();
  });

  it('should render when readOnly', () => {
    renderComponent({ disabled: true });

    expect(getNode.containerWith('dls-disabled')).toBeInTheDocument();
  });

  it('should render when error', () => {
    renderComponent({ error: { status: true, message: 'Error' } });

    expect(getNode.containerWith('dls-error')).toBeInTheDocument();
  });
});

const userPress = (text: string) => {
  userEvent.click(getNode.input);
  userEvent.type(getNode.input, text);
  userEvent.click(document.body);
};

const userPaste = (text: string) => {
  userEvent.click(getNode.input);
  userEvent.paste(getNode.input, text);
  userEvent.click(document.body);
};

describe('Actions', () => {
  describe('handleOnChange', () => {
    it('uncontrolled', () => {
      const onChange = jest.fn();

      renderComponent({ onChange });

      userPress('123456789');

      expect(getNode.input.value).toEqual('123-45-6789');
      expect(onChange).toBeCalledWith('123-45-6789');
    });

    it('controlled', () => {
      const onChange = jest.fn();

      renderComponent({ value: '123-45-678', onChange });

      userPress('9');

      expect(onChange).toBeCalled();
      expect(getNode.input.value).toEqual('123-45-6789');
      expect(onChange).toBeCalledWith('123-45-6789');
    });

    it('when mask not only allow number', () => {
      const onChange = jest.fn();

      const mask = [/\w/, /\w/, /\w/, '-', /\w/, /\w/, /\w/];

      renderComponent({ mask, value: 'abc-de', onChange });

      userPress('f');

      expect(onChange).toBeCalled();
      expect(getNode.input.value).toEqual('abc-def');
      expect(onChange).toBeCalledWith('abc-def');
    });

    it('when user paste', () => {
      const onChange = jest.fn();

      renderComponent({ onChange });

      userPaste('123456789');

      expect(onChange).toBeCalled();
      expect(getNode.input.value).toEqual('123-45-6789');
      expect(onChange).toBeCalledWith('123-45-6789');
    });
  });

  describe('handleDeleteKeyboard', () => {
    it('has no selection range', () => {
      const onKeyDown = jest.fn();
      const onChange = jest.fn();

      renderComponent({ value: '123-45-6789', onKeyDown, onChange });

      userPress('{delete}');

      expect(onKeyDown).toBeCalled();
      expect(getNode.input.value).toEqual('123-45-6789');
      expect(onChange).not.toBeCalled();
    });

    describe('has selection range', () => {
      describe('selectionStart === selectionEnd', () => {
        it('when corsor is before a charactor', () => {
          const onKeyDown = jest.fn();
          const onChange = jest.fn();

          renderComponent({
            value: '123-45-____',
            onKeyDown,
            onChange
          });

          getNode.input.setSelectionRange(6, 6);
          userPress('{delete}');

          expect(onKeyDown).toBeCalled();
          expect(getNode.input.value).toEqual('123-45-____');
          expect(onChange).toBeCalledWith('123-45-____');
        });

        it('when corsor is before hyphen', () => {
          const onKeyDown = jest.fn();
          const onChange = jest.fn();

          renderComponent({
            value: '123-45-6789',
            onKeyDown,
            onChange
          });

          getNode.input.setSelectionRange(6, 6);
          userPress('{delete}');

          expect(onKeyDown).toBeCalled();
          expect(getNode.input.value).toEqual('123-45-_789');
          expect(onChange).toBeCalledWith('123-45-_789');
        });
      });

      it('selectionStart !== selectionEnd', () => {
        const onKeyDown = jest.fn();
        const onChange = jest.fn();

        renderComponent({ value: '123-45-6789', onKeyDown, onChange });

        getNode.input.setSelectionRange(6, 11);
        userPress('{delete}');

        expect(onKeyDown).toBeCalled();
        expect(getNode.input.value).toEqual('123-45-____');
        expect(onChange).toBeCalledWith('123-45-____');
      });
    });
  });

  describe('handleBackspaceKeyboard', () => {
    it('has no selection range', () => {
      const onKeyDown = jest.fn();
      const onChange = jest.fn();

      renderComponent({ value: '1__-__-____', onKeyDown, onChange });

      getNode.input.setSelectionRange(1, 1);
      userPress('{backspace}');

      expect(onKeyDown).toBeCalled();
      expect(getNode.input.value).toEqual('');
      expect(onChange).toBeCalledWith('');
    });

    describe('has selection range', () => {
      it('selectionStart === selectionEnd', () => {
        const onKeyDown = jest.fn();
        const onChange = jest.fn();

        renderComponent({ mask: [], value: '123-45-67', onKeyDown, onChange });

        getNode.input.setSelectionRange(10, 10);
        userPress('{backspace}');

        expect(onKeyDown).toBeCalled();
        expect(getNode.input.value).toEqual('123-45-67');
        expect(onChange).not.toBeCalled();
      });

      it('selectionStart !== selectionEnd', () => {
        const onKeyDown = jest.fn();
        const onChange = jest.fn();

        renderComponent({ value: '123-45-6789', onKeyDown, onChange });

        getNode.input.setSelectionRange(6, 11);
        userPress('{backspace}');

        expect(onKeyDown).toBeCalled();
        expect(getNode.input.value).toEqual('123-45-____');
        expect(onChange).toBeCalledWith('123-45-____');
      });
    });
  });

  describe('handleOtherKeyboard', () => {
    it('press key with readonly', () => {
      const onKeyDown = jest.fn();
      const onChange = jest.fn();

      renderComponent({
        value: '123-45-678',
        onKeyDown,
        onChange,
        readOnly: true
      });

      userPress('a');

      expect(onKeyDown).not.toBeCalled();
      expect(onChange).not.toBeCalled();
    });

    it('press key wrong mask', () => {
      const onKeyDown = jest.fn();
      const onChange = jest.fn();

      renderComponent({ value: '123-45-678', onKeyDown, onChange });

      userPress('a');

      expect(onKeyDown).toBeCalled();
      expect(getNode.input.value).toEqual('123-45-678');
      expect(onChange).not.toBeCalled();
    });

    it('number of pressing greater than mask length', () => {
      const onKeyDown = jest.fn();
      const onChange = jest.fn();

      renderComponent({ onChange, onKeyDown });

      userPress('1234567890');

      expect(onKeyDown).toBeCalled();
      expect(getNode.input.value).toEqual('123-45-6789');
      expect(onChange).toBeCalledWith('123-45-6789');
    });

    it('has no selection range', () => {
      const onKeyDown = jest.fn();
      const onChange = jest.fn();

      renderComponent({ value: '123-45-678', onKeyDown, onChange });

      userPress('9');

      expect(onKeyDown).toBeCalled();
      expect(getNode.input.value).toEqual('123-45-6789');
      expect(onChange).toBeCalledWith('123-45-6789');
    });

    describe('has selection range', () => {
      it('selectionStart === selectionEnd', () => {
        const onKeyDown = jest.fn();
        const onChange = jest.fn();

        renderComponent({
          mask: maskDefault,
          value: '123-45-678',
          onKeyDown,
          onChange
        });

        getNode.input.setSelectionRange(10, 10);
        userPress('9');

        expect(onKeyDown).toBeCalled();
        expect(onChange).toBeCalledWith('123-45-6789');
        expect(getNode.input.value).toEqual('123-45-6789');
      });

      it('selectionStart !== selectionEnd', () => {
        const onKeyDown = jest.fn();
        const onChange = jest.fn();

        renderComponent({ value: '123-45-6788', onKeyDown, onChange });

        getNode.input.setSelectionRange(10, 11);
        userPress('9');

        expect(onKeyDown).toBeCalled();
        expect(getNode.input.value).toEqual('123-45-6789');
        expect(onChange).toBeCalledWith('123-45-6789');
      });
    });
  });

  it('handleFocus and handleBlur', () => {
    const onFocus = jest.fn();
    const onBlur = jest.fn();

    renderComponent({ onFocus, onBlur });

    userEvent.click(getNode.input);
    expect(onFocus).toBeCalled();

    userEvent.click(document.body);
    expect(onBlur).toBeCalled();
  });

  it('should focus input when click label', () => {
    const { baseElement } = renderComponent({ mask: maskDefault });
    const labelElement = baseElement.querySelector('.dls-floating-label')!;
    const inputElement = baseElement.querySelector('input')!;

    userEvent.click(labelElement);
    expect(inputElement).toHaveFocus();
  });
});
