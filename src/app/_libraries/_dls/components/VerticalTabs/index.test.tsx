import React from 'react';

// testing libs
import userEvent from '@testing-library/user-event';
import '@testing-library/user-event';
import '@testing-library/jest-dom';
import { render, RenderResult, getByText, act } from '@testing-library/react';

// mocks
import '../../test-utils/mocks/mockCanvas';

// component
import Tabs, { TabsProps } from '.';
import SimpleBar from '../../components/SimpleBar';

const mockComponent = (content: string) => <h5>{content + ' Mock Content'}</h5>;

const mockData = [
  {
    title: 'Account Information',
    component: mockComponent('Account Information'),
    id: 'accountInforId'
  },
  {
    title: 'Collection Information',
    component: mockComponent('Collection Information'),
    id: 'collectionInforId'
  }
];

const renderVerticalTabs = (
  props: TabsProps,
  renderContent?: boolean
): RenderResult => {
  return render(
    <Tabs ref={{ current: null }} {...props}>
      <Tabs.Nav>
        {mockData.map(({ id, title }) => (
          <Tabs.Nav.Item key={id}>
            <Tabs.Nav.Link eventKey={id}>{title}</Tabs.Nav.Link>
          </Tabs.Nav.Item>
        ))}
      </Tabs.Nav>
      {renderContent && (
        <Tabs.Content>
          {mockData.map(({ id, component: Component }) => (
            <Tabs.Pane eventKey={id} key={id}>
              {Component}
            </Tabs.Pane>
          ))}
        </Tabs.Content>
      )}
    </Tabs>
  );
};

const renderVerticalTabsWithSimpleBar = (props: TabsProps): RenderResult => {
  return render(
    <Tabs ref={{ current: null }} {...props}>
      <div style={{ width: 200, height: 100 }}>
        <SimpleBar>
          <Tabs.Nav ref={() => {}} className="test">
            {mockData.map(({ id, title }) => (
              <Tabs.Nav.Item key={id}>
                <Tabs.Nav.Link eventKey={id}>{title}</Tabs.Nav.Link>
              </Tabs.Nav.Item>
            ))}
          </Tabs.Nav>
        </SimpleBar>
      </div>
    </Tabs>
  );
};

describe('Test VerticalTabs Component', () => {
  it('Render component with navigation', () => {
    const wrapper = renderVerticalTabs({}, false);

    const tabList = wrapper.getByRole('tablist');
    expect(tabList).toBeInTheDocument();

    const navItems = tabList.querySelectorAll('a[role="tab"]');

    // render enough navigation item
    expect(navItems.length).toBe(mockData.length);
    // render correct title
    navItems.forEach((element, i) => {
      expect(element.textContent).toBe(mockData[i].title);
    });
  });
  it('Render component with content panel', () => {
    const wrapper = renderVerticalTabs(
      { defaultActiveKey: mockData[0].id, hideCollapsedButton: true },
      true
    );

    const tabContent = wrapper.baseElement.querySelector('.tab-content');
    expect(tabContent).toBeInTheDocument();

    const tabPanel = tabContent!.querySelectorAll('div[role="tabpanel"]');
    // check panel content
    expect(tabPanel.length).toBe(mockData.length);
    // render correct content
    tabPanel.forEach((element, i) => {
      expect(
        getByText(element as HTMLElement, mockData[i].title + ' Mock Content')
      ).toBeInTheDocument();
    });

    // default active tab
    const activeNav = wrapper.baseElement.querySelector(
      `a[data-rb-event-key="${mockData[0].id}"]`
    )!;

    expect(activeNav.className).toContain('active');
    expect(activeNav.getAttribute('aria-selected')).toBeTruthy();

    // default panel content
    const activeContentPanel = wrapper
      .getByText(mockData[0].title + ' Mock Content')
      .closest("div[role='tabpanel']");

    expect(activeContentPanel?.getAttribute('aria-hidden')).toBe('false');
    expect(activeContentPanel?.className).toContain('active show');

    // hide collapse button
    const collButton = wrapper.baseElement.querySelector(
      '.expand-collapse-navbar'
    );
    expect(collButton).not.toBeInTheDocument();
  });
  it('Expand/Collapse behavior', () => {
    jest.useFakeTimers();
    const collapsed = true;
    const onCollapse = jest.fn((value: boolean) => value);
    const wrapper = renderVerticalTabs(
      {
        defaultActiveKey: mockData[0].id,
        collapsed,
        onToggleCollapsed: onCollapse,
        collapseMessage: 'Collapse Menu',
        expandMessage: 'Expand Menu'
      },
      true
    );

    // collapsed
    const tabList = wrapper.baseElement.querySelector('div[role="tablist"]')!;
    expect(tabList.className).toContain('is-collapsed');

    // collapse button
    const collButton = wrapper.baseElement.querySelector(
      '.expand-collapse-navbar'
    )!;
    act(() => {
      userEvent.click(collButton);
      jest.runAllTimers();
    });

    expect(onCollapse).toHaveBeenCalledWith(false);
  });
  it('Render with childrent component', () => {
    const wrapper = renderVerticalTabsWithSimpleBar({});

    // render simplebar
    const simpleBar = wrapper.baseElement.querySelector('.simplebar-content');
    expect(simpleBar).toBeInTheDocument();

    // render Tabs Nav inside simpleBar
    const nav = simpleBar!.querySelector('div[role="tablist"]');
    expect(nav).toBeInTheDocument();
  });
  it('Switching Tabs', () => {
    const wrapper = renderVerticalTabs(
      { defaultActiveKey: mockData[0].id, hideCollapsedButton: true },
      true
    );

    const secondTab = wrapper.baseElement.querySelector(
      `a[data-rb-event-key="${mockData[1].id}"]`
    )! as HTMLElement;
    const secondPanel = wrapper
      .getByText(/Collection Information Mock Content/i)
      .closest('[role="tabpanel"]')!;
    expect(secondTab.getAttribute('aria-selected')).toBe('false');
    expect(secondPanel.getAttribute('aria-hidden')).toBe('true');

    // clicking navigation
    secondTab.click();
    expect(secondTab.getAttribute('aria-selected')).toBe('true');
    expect(secondPanel.getAttribute('aria-hidden')).toBe('false');
  });
});
describe('Test Vertical Tabs Component Styles', () => {
  it('Collapse/Expand button', () => {
    const wrapper = renderVerticalTabs(
      { defaultActiveKey: mockData[0].id },
      true
    );

    // collapse button
    const collButton = wrapper.baseElement.querySelector(
      '.expand-collapse-navbar'
    )!;

    // Icon
    const icon = collButton.querySelector('.icon')!;
    expect(icon).toBeInTheDocument();
    expect(icon.className).toContain('icon-push-left');
    act(() => {
      userEvent.click(collButton);
      jest.runAllTimers();
    });
    expect(icon.className).toContain('icon-push-right');
  });
});
