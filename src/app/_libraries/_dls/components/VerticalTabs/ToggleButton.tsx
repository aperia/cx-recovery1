import React from 'react';

// types
import isFunction from 'lodash.isfunction';

// components
import Icon from '../Icon';
import Tooltip from '../Tooltip';

// utils
import { genAmtId } from '../../utils';

export interface ToggleButtonProps extends DLSId {
  expandMessage?: string;
  collapseMessage?: string;
  collapsed?: boolean;
  onToggleCollapsed?: (collapsed: boolean) => void;
}

const ToggleButton: React.FC<ToggleButtonProps> = ({
  expandMessage,
  collapseMessage,
  collapsed,
  onToggleCollapsed,

  dataTestId
}) => {
  const handleClick = () => {
    isFunction(onToggleCollapsed) && onToggleCollapsed(!collapsed);
  };

  return (
    <span
      className="expand-collapse-navbar"
      onClick={handleClick}
      data-testid={genAmtId(dataTestId, 'toggle-button', 'VerticalTabs.toggle')}
    >
      <Tooltip
        variant="primary"
        placement="right"
        element={collapsed ? expandMessage : collapseMessage}
        dataTestId={`${dataTestId}-tooltip-toggle-button`}
      >
        <Icon name={collapsed ? 'push-right' : 'push-left'} />
      </Tooltip>
    </span>
  );
};
export default ToggleButton;
