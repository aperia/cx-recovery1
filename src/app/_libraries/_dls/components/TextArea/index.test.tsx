import React from 'react';

// test
import '@testing-library/jest-dom';
import { fireEvent, render, RenderResult } from '@testing-library/react';
import '../../test-utils/mocks/mockCanvas';

// components
import TextArea, { TextAreaProps } from '.';
import userEvent from '@testing-library/user-event';

describe('Test TextArea Component', () => {
  const mockTextAreaProp = {
    id: 'text-area-id',
    name: 'text-area-name',
    label: 'Mock label',
    placeholder: 'Mock placeholder'
  };

  const renderTextArea = (props?: TextAreaProps): RenderResult => {
    return render(
      <div>
        <TextArea ref={() => {}} {...mockTextAreaProp} {...props} />
      </div>
    );
  };

  afterEach(() => {
    jest.clearAllMocks();
  });

  it('Check render text area with label, check style', () => {
    const wrapper = renderTextArea();
    const { getByText } = wrapper;

    const label = getByText(/Mock label/i);
    expect(label).toBeInTheDocument();
  });

  it('Check render text area with error, check style', () => {
    const wrapper = renderTextArea({
      error: {
        message: 'Mock error',
        status: true
      }
    });
    const { getByText } = wrapper;

    const errorIcon = wrapper.baseElement.querySelector('i.icon');
    expect(errorIcon).toBeInTheDocument();

    const textArea = wrapper.baseElement.querySelector(
      '.text-field-container > textarea'
    ) as HTMLTextAreaElement;

    fireEvent.focus(textArea);

    const triggerTooltip = wrapper.baseElement.querySelector(
      '.dls-popper-trigger'
    ) as HTMLSpanElement;

    // Trigger error tooltip on mouse enter
    fireEvent.mouseEnter(triggerTooltip);
    expect(getByText('Mock error')).toBeInTheDocument();
  });

  it('Check text area changed', () => {
    const onChangeFn = jest.fn();
    const mockValue = 'mock value';

    const wrapper = renderTextArea({
      onChange: onChangeFn
    });
    const textArea = wrapper.baseElement.querySelector(
      '.text-field-container > textarea'
    ) as HTMLTextAreaElement;
    fireEvent.change(textArea, { target: { value: mockValue } });

    expect(onChangeFn).toHaveBeenCalled();
    expect(textArea.value).toEqual(mockValue);
  });

  it('Check text area changed with default value', () => {
    const onChangeFn = jest.fn();
    const mockValue = 'mock value';

    const wrapper = renderTextArea({
      value: 'default value',
      onChange: onChangeFn
    });
    const textArea = wrapper.baseElement.querySelector(
      '.text-field-container > textarea'
    ) as HTMLTextAreaElement;
    fireEvent.change(textArea, { target: { value: mockValue } });

    expect(onChangeFn).toHaveBeenCalled();
    expect(textArea.value).toEqual('default value');
  });

  it('Check text area focused & blured, check focused style', () => {
    const onFocusFn = jest.fn(),
      onBlurFn = jest.fn();

    const wrapper = renderTextArea({
      onFocus: onFocusFn,
      onBlur: onBlurFn
    });
    const textArea = wrapper.baseElement.querySelector(
      '.text-field-container > textarea'
    ) as HTMLTextAreaElement;

    fireEvent.focus(textArea);
    expect(onFocusFn).toHaveBeenCalled();

    fireEvent.blur(textArea);
    expect(onBlurFn).toHaveBeenCalled();
  });

  it('Check text area render with required prop, check style', () => {
    const wrapper = renderTextArea({
      required: true
    });

    const asterisk = wrapper.baseElement.querySelector('.asterisk');
    expect(asterisk).toBeInTheDocument();
  });

  it('Should run mousedown with label', () => {
    const onFocus = jest.fn();
    const wrapper = renderTextArea({
      required: true,
      onFocus
    });

    userEvent.click(wrapper.container.querySelector('.dls-floating-label')!);
    expect(onFocus).toBeCalled();
  });
});
