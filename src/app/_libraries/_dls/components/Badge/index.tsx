import React, {
  useRef,
  forwardRef,
  useImperativeHandle,
  RefForwardingComponent,
  ReactNode
} from 'react';

// utils
import classnames from 'classnames';
import { genAmtId } from '../../utils';

export interface BadgeProps extends DLSId {
  color?: 'green' | 'red' | 'cyan' | 'orange' | 'purple' | 'grey';
  disabled?: boolean;
  children?: ReactNode;
  textTransformNone?: boolean;
}

const Badge: RefForwardingComponent<HTMLSpanElement, BadgeProps> = (
  {
    color,
    disabled = false,
    children,
    textTransformNone = false,
    
    id,
    dataTestId
  },
  ref
) => {
  const containerRef = useRef<HTMLSpanElement | null>(null);

  useImperativeHandle(ref, () => containerRef.current!);

  return (
    <span
      ref={containerRef}
      className={classnames(
        `badge badge-${color}`,
        disabled && 'disabled',
        textTransformNone && 'text-transform-none'
      )}
      id={id}
      data-testid={genAmtId(dataTestId, 'dls-badge', 'Badge')}
    >
      {children}
    </span>
  );
};

export default forwardRef<HTMLSpanElement, BadgeProps>(Badge);
