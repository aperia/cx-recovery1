import React, {
  DetailedHTMLProps,
  ForwardRefExoticComponent,
  ForwardRefRenderFunction,
  HTMLAttributes,
  ReactElement,
  ReactNodeArray,
  RefAttributes,
  useImperativeHandle,
  useRef
} from 'react';
import classNames from 'classnames';
import { className, genAmtId } from '../../utils';
import Input from './Input';
import Label from './Label';
import { useInputWithLabel } from '../../hooks';

export interface RadioRef {}

export interface RadioProps
  extends DetailedHTMLProps<HTMLAttributes<HTMLDivElement>, HTMLDivElement>,
    DLSId {
  children: ReactElement | ReactNodeArray;
}
export interface ExtraStaticProp
  extends ForwardRefExoticComponent<RadioProps & RefAttributes<RadioRef>> {
  Input: typeof Input;
  Label: typeof Label;
}

const Radio: ForwardRefRenderFunction<RadioRef, RadioProps> = (
  { className: classNameProp, children, dataTestId, ...props },
  ref
) => {
  const containerRef = useRef<HTMLDivElement | null>(null);
  useImperativeHandle(ref, () => containerRef.current!);

  const testId = genAmtId(dataTestId, 'dls-radio', 'Radio');

  // Mapped children, add label if doesn't exist
  const { element, inputProps } = useInputWithLabel({
    children,
    Input,
    Label,
    dataTestId: testId
  });

  return (
    <div
      className={classNames(
        className.radio.CONTAINER,
        classNameProp,
        inputProps?.className
      )}
      ref={containerRef}
      data-testid={testId}
      {...props}
    >
      {element}
    </div>
  );
};

const ExtraStaticRadio = React.forwardRef<RadioRef, RadioProps>(
  Radio
) as ExtraStaticProp;
ExtraStaticRadio.Input = Input;
ExtraStaticRadio.Label = Label;

// Form.Check = ExoticRadio as any;

export default ExtraStaticRadio;
