import React, { MutableRefObject, ReactElement } from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

// utils
import { queryByClass } from '../../test-utils';

// components
import Fade from './Fade';

const mockRef: MutableRefObject<HTMLDivElement | null> = { current: null };

const renderComponent = (element: ReactElement) => {
  const wrapper = render(element);

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

describe('Render', () => {
  it('should render', () => {
    const text = 'Fade animation';
    renderComponent(
      <Fade ref={mockRef} in={true}>
        <p>{text}</p>
      </Fade>
    );

    expect(screen.getByText(text)).toBeInTheDocument();
  });

  it('should render with custom className', () => {
    const text = 'Fade animation';
    const { baseElement } = renderComponent(
      <Fade in={true} animationCustomClassName="class-name" duration={200}>
        <p>{text}</p>
      </Fade>
    );

    expect(queryByClass(baseElement, /class-name/)).toBeInTheDocument();
  });

  it('should update animation', () => {
    const duration = 2000;
    const { baseElement } = renderComponent(
      <Fade in={true} duration={duration}>
        <p>Fade animation</p>
      </Fade>
    );

    expect(queryByClass(baseElement, /dls-animation/)).toBeInTheDocument();
    expect(queryByClass(baseElement, /dls-animation/)).toHaveStyle({
      'animation-duration': `${duration}ms`
    });
  });
});
