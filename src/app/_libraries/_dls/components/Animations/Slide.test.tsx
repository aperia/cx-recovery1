import React, { MutableRefObject, ReactElement } from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

// utils
import { queryByClass } from '../../test-utils';

// components
import Slide from './Slide';

const mockRef: MutableRefObject<HTMLDivElement | null> = { current: null };

const renderComponent = (element: ReactElement) => {
  const wrapper = render(element);

  return {
    wrapper,
    baseElement: wrapper.baseElement as HTMLElement
  };
};

describe('Render', () => {
  it('should render', () => {
    const text = 'Slide animation';
    renderComponent(
      <Slide ref={mockRef} in={true}>
        <p>{text}</p>
      </Slide>
    );

    expect(screen.getByText(text)).toBeInTheDocument();
  });

  it('should render with custom className', () => {
    const text = 'Slide animation';
    const { baseElement } = renderComponent(
      <Slide in={true} animationCustomClassName="class-name" duration={200}>
        <p>{text}</p>
      </Slide>
    );

    expect(queryByClass(baseElement, /class-name/)).toBeInTheDocument();
  });

  it('should update animation', () => {
    const duration = 2000;
    const { baseElement } = renderComponent(
      <Slide in={true} duration={duration}>
        <p>Slide animation</p>
      </Slide>
    );

    expect(queryByClass(baseElement, /dls-animation/)).toBeInTheDocument();
    expect(queryByClass(baseElement, /dls-animation/)).toHaveStyle({
      'animation-duration': `${duration}ms`
    });
  });
});
