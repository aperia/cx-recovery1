import React, { useEffect, useRef } from 'react';
import classnames from 'classnames';
import { Chart, ChartData, ChartConfiguration } from 'chart.js';

import { useDeepValue } from '../../hooks';
import { genAmtId } from '../../utils';
import { useRegister } from './hooks';
import { cloneDeep } from 'app/helpers';

export interface BarhartData extends ChartData<'bar', any> {}

export interface BarChartProps extends DLSId {
  config: ChartConfiguration;
  className?: string;
  height?: number;
  width?: number;
}

const BarChart: React.RefForwardingComponent<HTMLDivElement, BarChartProps> = (
  {
    config: configProp,
    className,
    height = 190,
    width = 200,
    id,
    dataTestId
  },
  ref
) => {
  // refs
  const canvasRef = useRef<HTMLCanvasElement | null>(null);

  // vars
  const config = useDeepValue(configProp);

  // chartJs register
  useRegister();

  useEffect(() => {
    const canvas = canvasRef.current!;
    canvas.height = height;
    canvas.width = width;
    const ctx = canvas.getContext('2d')!;
    const barChart = new Chart(ctx, cloneDeep(config));

    return () => barChart.destroy();
  }, [config, height, width]);

  return (
    <div
      ref={ref}
      className={classnames('dls-chart dls-bar-chart', className)}
      id={id}
      data-testid={genAmtId(dataTestId, 'dls-bar-chart', 'BarChart')}
    >
      <div className="dls-bar-chart-legend d-flex" />
      <canvas ref={canvasRef} />
    </div>
  );
};

export default React.forwardRef<HTMLDivElement, BarChartProps>(BarChart);
