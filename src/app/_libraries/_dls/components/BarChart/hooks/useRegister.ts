import { useEffect, useRef } from 'react';

import { Chart, registerables } from 'chart.js';

export interface RegisterHook {
  (): void;
}

const useRegister: RegisterHook = () => {
  const isRegisteredRef = useRef(false);

  if (!isRegisteredRef.current) {
    // we can optimize package size by adjust registerables below
    Chart.register(...registerables);
    isRegisteredRef.current = true;
  }

  useEffect(() => () => Chart.unregister(...registerables), []);
};

export default useRegister;
