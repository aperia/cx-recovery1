import React from 'react';
import { render, screen } from '@testing-library/react';
import '@testing-library/jest-dom';

import CalendarFooter from './CalendarFooter';

describe('render', () => {
  it('should render without label', () => {
    render(<CalendarFooter />);

    expect(screen.getByText('Today')).toBeInTheDocument();
  });

  it('should render with label', () => {
    const label = 'label';

    render(<CalendarFooter label={label} />);

    expect(screen.getByText(label)).toBeInTheDocument();
  });
});
