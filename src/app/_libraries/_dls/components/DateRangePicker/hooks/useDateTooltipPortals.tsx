import React, { useEffect, useRef, useState } from 'react';
import ReactDOM from 'react-dom';

// components/types
import { Detail } from 'react-calendar';
import { PopupBaseRef } from '../../PopupBase';
import { TooltipProps } from '../../Tooltip';
import { DateRangePickerProps } from '../types';

// utils
import classnames from 'classnames';
import { get, isFunction } from '../../../lodash';

export interface DateTooltipPortalsHookDependencies {
  opened?: boolean;
  dateTooltip?: DateRangePickerProps['dateTooltip'];
  view?: Detail;
}

export interface DateTooltipPortalsHook {
  (
    popupBaseRef: React.MutableRefObject<PopupBaseRef | null>,
    dependencies: DateTooltipPortalsHookDependencies
  ): void;
}

const useDateTooltipPortals: DateTooltipPortalsHook = (
  popupBaseRef,
  { opened, dateTooltip, view }
) => {
  // states
  const [dateTooltipPortals, setDateTooltipPortals] = useState<
    React.ReactPortal[]
  >([]);

  // refs
  const keepRef = useRef<Record<string, any>>({});

  // keep references
  keepRef.current.popupBaseRef = popupBaseRef;
  keepRef.current.dateTooltip = dateTooltip;

  useEffect(() => {
    const { dateTooltip } = keepRef.current;
    if (!opened || !isFunction(dateTooltip)) return;

    let mutationObserver: MutationObserver;

    process.nextTick(() => {
      const { popupBaseRef } = keepRef.current;
      const popupElement = get(popupBaseRef.current, 'element') as HTMLElement;

      // handle add date tooltip
      // filter element title, read cached date, require outside generate Tooltip react element
      const handleAddDateTooltip = () => {
        const dateTooltipPortals: React.ReactPortal[] = [];

        const titleElements = popupElement?.querySelectorAll(
          '.react-calendar .react-calendar__tile'
        );

        Array.prototype.forEach.call(titleElements, (element: HTMLElement) => {
          const tooltipCachedDateElement = element?.querySelector(
            '.dls-tooltip-cache-date'
          ) as HTMLElement;
          const dateCached = new Date(tooltipCachedDateElement.innerText);

          const tooltipReactElement = dateTooltip(
            dateCached,
            view
          ) as React.ReactElement<TooltipProps>;
          if (!React.isValidElement(tooltipReactElement)) return;

          const { triggerClassName: tooltipTriggerClassName } =
            tooltipReactElement.props;

          const abbrElement = element?.querySelector('abbr') || element;

          const tooltipReactElementEdited = React.cloneElement(
            tooltipReactElement,
            {
              ...tooltipReactElement.props,
              triggerClassName: classnames(
                tooltipTriggerClassName,
                'dls-tooltip-date-picker'
              ),
              onClickTrigger: () => abbrElement.click(),
              children: 'PP'
            }
          );

          const portal = ReactDOM.createPortal(
            tooltipReactElementEdited,
            abbrElement
          );
          dateTooltipPortals.push(portal);
        });

        setDateTooltipPortals(dateTooltipPortals);
      };

      // create new mutation observer
      mutationObserver = new MutationObserver(handleAddDateTooltip);

      // run handleAddDateTooltip first time
      handleAddDateTooltip();

      // start observe
      mutationObserver.observe(popupElement, {
        childList: true,
        subtree: true
      });
    });

    return () => mutationObserver?.disconnect();
  }, [opened, view]);

  return dateTooltipPortals;
};

export default useDateTooltipPortals;
