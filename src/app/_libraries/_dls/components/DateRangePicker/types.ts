// components/types
import DateRangePicker from '.';
import { TooltipProps } from '../Tooltip';
import { MaskedTextBoxRef } from '../MaskedTextBox';
import { PopupBaseRef, PopupBaseProps } from '../PopupBase';
import { CalendarProps, CalendarTileProperties, Detail } from 'react-calendar';

export interface DateRangePickerKeepReference<T, T2, T3, T4>
  extends Record<string, any> {
  handleValueMode: T;
  value: T2;
  isValidDate: T3;
  correctDateRange: T4;
  inputStartElement?: HTMLElement;
  inputEndElement?: HTMLElement;
}

export interface IsValidDate {
  (value?: any): value is Date | boolean;
}

export interface CorrectDateRange {
  (
    dateConfig: DateRangeConfig,
    value?: DateRangePickerValue
  ): DateRangePickerValue;
}

export type DateRangeConfig = {
  min: {
    year: number;
    month: number;
    beginDate: Date;
  };
  max: {
    year: number;
    month: number;
    beginDate: Date;
  };
};

export type DateRangePickerValue = {
  start?: Date;
  end?: Date;
};

export interface DateRangePickerChangeEvent
  extends React.ChangeEvent<typeof DateRangePicker> {
  target: EventTarget &
    typeof DateRangePicker & {
      value?: DateRangePickerValue;
      id?: string;
      name?: string;
    };
  value?: DateRangePickerValue;
}

export interface DateRangePickerProps extends DLSId {
  value?: DateRangePickerValue;
  onChange?: (event: DateRangePickerChangeEvent) => void;

  label?: React.ReactNode;
  size?: 'small';

  onBlur?: (event: React.FocusEvent) => void;
  onFocus?: (event: React.FocusEvent) => void;

  opened?: boolean;
  onVisibilityChange?: (nextOpened?: boolean) => void;

  readOnly?: boolean;
  disabled?: boolean;
  placeholder?: string;

  required?: boolean;
  error?: {
    message?: string;
    status?: boolean;
  };
  errorTooltipProps?: TooltipProps;

  popupBaseProps?: Omit<
    PopupBaseProps,
    'opened' | 'onVisibilityChange' | 'reference'
  >;

  id?: string;
  name?: string;
  autoFocus?: boolean;
  className?: string;

  minDate?: Date;
  maxDate?: Date;
  dateTooltip?: (
    date: Date,
    view?: Detail
  ) => React.ReactElement | undefined | null;
  disabledRange?: Array<{
    fromDate?: Date;
    toDate?: Date;
  }>;

  titleDisabled?: (
    props: CalendarTileProperties & { activeStartDate: Date }
  ) => boolean;

  view?: 'month' | 'year';
}

export interface DateRangePickerRef {
  containerElement?: HTMLDivElement | null;
  inputContainerElement?: HTMLDivElement | null;
  maskedTextBox?: MaskedTextBoxRef | null;
  popupBase?: PopupBaseRef | null;
}

export type DateRangePickerMinDetail = CalendarProps['minDetail'];

export type DateRangePickerMaxDetail = CalendarProps['maxDetail'];
