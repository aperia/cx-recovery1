import React, { useCallback, useState } from 'react';
import DateRangePicker from '.';

import { DateRangePickerChangeEvent, DateRangePickerProps } from './types';

const DateRangePickerWrapper = ({
  value: valueProp,
  onFocus,
  ...props
}: DateRangePickerProps) => {
  const [value, setValue] = useState(valueProp);

  const handleChange = ({ value }: DateRangePickerChangeEvent) => {
    setValue(value);
  };

  const handleFocus = useCallback(
    (e: React.FocusEvent<Element>) => {
      onFocus && onFocus(e);
    },
    [onFocus]
  );

  return (
    <div>
      <DateRangePicker
        onFocus={handleFocus}
        onChange={handleChange}
        value={value}
        {...props}
      />
    </div>
  );
};

export default DateRangePickerWrapper;
