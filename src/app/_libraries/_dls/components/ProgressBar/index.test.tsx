import React, { MutableRefObject } from 'react';
import { render } from '@testing-library/react';
import { queryAllByClass, queryByClass } from '../../test-utils/queryHelpers';
import ProgressBar from './';

const mockRef: MutableRefObject<HTMLDivElement | null> = { current: null };

describe('Progress Bar component', () => {
  describe('Throw Error', () => {
    it('length color !== length value', () => {
      const data = {
        colors: [],
        values: [1]
      };
      try {
        render(<ProgressBar data={data} />);
      } catch (error) {
        expect(error.message).toEqual(
          'Length Color will equal with length value!'
        );
      }
    });

    it('label color !== length value', () => {
      const data = {
        labels: [],
        colors: ['#000'],
        values: [1]
      };
      try {
        render(<ProgressBar data={data} />);
      } catch (error) {
        expect(error.message).toEqual(
          'Length labels will equal with length value!'
        );
      }
    });
  });

  it('Render UI', () => {
    const data = {
      labels: [undefined, undefined] as any,
      colors: ['rgb(92, 186, 230)', 'rgb(246, 99, 100)'],
      values: [180, 680],
      average: 200
    };

    const wrapper = render(<ProgressBar ref={mockRef} data={data} />);

    // top element
    const elmTop = queryByClass(wrapper.container, /dls-progress-bar-top/);
    expect(elmTop?.textContent).toEqual(
      `${data.values[0]} / ${data.values[1]}`
    );

    // progress
    const [elm0, elm1] = queryAllByClass(
      wrapper.container,
      /dls-item-progress/
    );

    expect(window.getComputedStyle(elm0).width).toEqual('100%');
    expect(window.getComputedStyle(elm0).backgroundColor).toEqual(
      data.colors[1]
    );

    expect(window.getComputedStyle(elm1).width).toEqual('26%');
    expect(window.getComputedStyle(elm1).backgroundColor).toEqual(
      data.colors[0]
    );

    // average
    const elmAverage = queryByClass(wrapper.container, /dls-progress-line/);
    expect(window.getComputedStyle(elmAverage!).marginLeft).toEqual('29%');

    // percent / right element
    const elmRight = queryByClass(
      wrapper.container,
      /dls-progress-bar-percent/
    );
    expect(elmRight?.textContent).toEqual(
      `${Math.round((data.values[0] / data.values[1]) * 100)}%`
    );
  });

  it('Render UI with custom', () => {
    const data = {
      colors: ['rgb(92, 186, 230)', 'rgb(246, 99, 100)'],
      values: [180, 680],
      average: 200
    };
    const wrapper = render(
      <ProgressBar
        data={data}
        renderTopView={() => <div className="test-top">custom top</div>}
        renderPercent={() => <div className="test-right">custom percent</div>}
        renderAverage={() => <div className="test-bottom">custom average</div>}
      />
    );

    // top element
    const elmTop = queryByClass(wrapper.container, /test-top/);
    expect(elmTop?.textContent).toEqual('custom top');

    // percent / right element
    const elmRight = queryByClass(wrapper.container, /test-right/);
    expect(elmRight?.textContent).toEqual('custom percent');

    // bottom
    const elmBottom = queryByClass(wrapper.container, /test-bottom/);
    expect(elmBottom?.textContent).toEqual('custom average');
  });

  it('Render UI with empty value', () => {
    const data = {
      colors: [],
      values: []
    };
    const wrapper = render(<ProgressBar data={data} />);
    const elmTop = queryByClass(wrapper.container, /dls-progress-bar-top/);
    const items = queryAllByClass(wrapper.container, /dls-item-progress/);
    const elmAverage = queryByClass(wrapper.container, /dls-progress-line/);
    const elmRight = queryByClass(
      wrapper.container,
      /dls-progress-bar-percent/
    );

    expect(elmTop).toBeNull();
    expect(items.length).toEqual(0);
    expect(elmAverage).toBeNull();
    expect(elmRight).toBeNull();
  });
});
