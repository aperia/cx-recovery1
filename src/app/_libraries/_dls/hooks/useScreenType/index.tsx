import { useLayoutEffect, useState } from 'react';
import { ScreenSize, ScreenType } from './types';

export interface UseScreenTypeHook {
  (): ScreenType | undefined;
}

const useScreenType: UseScreenTypeHook = () => {
  const [screenType, setScreenType] = useState<ScreenType>();

  const handleCheckScreenType = () => {
    // if (!window) return ScreenType.UNAVAILABLE;

    const isDesktop =
      window.innerWidth >= ScreenSize.DESKTOP.width &&
      window.innerHeight >= ScreenSize.DESKTOP.height;

    const isTabletLandscape =
      window.innerWidth >= ScreenSize.TABLET_LANDSCAPE.width &&
      window.innerHeight >= ScreenSize.TABLET_LANDSCAPE.height;

    const isTabletPortrait =
      window.innerWidth >= ScreenSize.TABLET_PORTRAIT.width &&
      window.innerHeight >= ScreenSize.TABLET_PORTRAIT.height;

    if (isDesktop) return ScreenType.DESKTOP;
    if (isTabletLandscape) return ScreenType.TABLET_LANDSCAPE;
    if (isTabletPortrait) return ScreenType.TABLET_PORTRAIT;
    return ScreenType.CUSTOM;
  };

  useLayoutEffect(() => {
    const handleUpdateScreenType = () => {
      process.nextTick(() => {
        const screenType = handleCheckScreenType();
        setScreenType(screenType);
      });
    };

    handleUpdateScreenType();

    window.addEventListener('orientationchange', handleUpdateScreenType);
    return () => {
      window.removeEventListener('orientationchange', handleUpdateScreenType);
    };
  }, []);

  return screenType;
};

export default useScreenType;
