import React, {
  Children,
  cloneElement,
  isValidElement,
  ReactElement,
  ReactNode,
  useEffect,
  useMemo,
  useRef,
  useState
} from 'react';
import { InputProps as CheckBoxInputProps } from '../../components/CheckBox/Input';
import { InputProps as RadioInputProps } from '../../components/Radio/Input';

// components
import Tooltip, { TooltipProps } from '../../components/Tooltip';

// utils
import { className, genAmtId, genId } from '../../utils';

export interface UseInputWithLabelArgs {
  autoGenId?: boolean;
  children: any;
  Input: any;
  Label: any;
  dataTestId?: string;
}

export interface UseInputWithLabelHook {
  (args: UseInputWithLabelArgs): {
    element: ReactNode;
    inputProps?: CheckBoxInputProps | RadioInputProps;
  };
}

const useInputWithLabel: UseInputWithLabelHook = ({
  autoGenId = true,
  children,
  Input,
  Label,
  dataTestId
}) => {
  // refs
  const idRef = useRef(autoGenId ? genId() : '');
  const fakeInputRef = useRef<HTMLDivElement | null>(null);
  const labelRef = useRef<HTMLDivElement | null>(null);
  const coreRef = useRef<Record<string, any>>({});

  // states
  const [inputElement, setInputElement] = useState<HTMLInputElement | null>();

  /* istanbul ignore next */
  const handleClickOnFakeInput = () => {
    const input = coreRef.current.inputElement as HTMLInputElement;
    input?.click();
    input?.focus();
  };

  coreRef.current.inputElement = inputElement!;
  coreRef.current.handleClickOnFakeInput = handleClickOnFakeInput;

  // Mapped children, add label if doesn't exist
  const result = useMemo(() => {
    const { handleClickOnFakeInput } = coreRef.current;

    const tooltip = Children.toArray(children).find(
      (child) => isValidElement(child) && child.type === Tooltip
    ) as React.ReactElement<TooltipProps>;

    const inputChildWrappedByTooltip =
      tooltip &&
      isValidElement(tooltip.props.children) &&
      tooltip.props.children.type === Input
        ? tooltip.props.children
        : null;

    const inputChild = Children.toArray(children).find(
      (child) => isValidElement(child) && child.type === Input
    ) as ReactElement;

    const nextTooltip =
      tooltip &&
      cloneElement(tooltip, {
        ...tooltip.props,
        children: (
          <div
            ref={fakeInputRef}
            className={className.checkbox.FAKE_INPUT}
            onClick={handleClickOnFakeInput}
          />
        )
      });
    const nextInputChild = inputChildWrappedByTooltip || inputChild;
    const nextId = nextInputChild?.props?.id || idRef.current;

    const inputChildWithId =
      nextInputChild &&
      cloneElement(nextInputChild, {
        ...nextInputChild?.props,
        ref: setInputElement,
        id: nextId,
        'data-testid': genAmtId(dataTestId!, 'input', 'UseInputWithLabelHook')
      });

    const labelChild = Children.toArray(children).find(
      (child) => isValidElement(child) && child.type === Label
    ) as ReactElement;
    const labelChildWithHtmlFor =
      labelChild &&
      cloneElement(labelChild, {
        ...labelChild.props,
        ref: labelRef,
        htmlFor: nextId,
        'data-testid': genAmtId(dataTestId!, 'label', 'UseInputWithLabelHook')
      });

    return {
      element: (
        <>
          {nextTooltip}
          {inputChildWithId || <Input id={idRef.current} />}
          {labelChildWithHtmlFor || <Label ref={labelRef} htmlFor={nextId} />}
        </>
      ),
      inputProps: nextInputChild.props
    };
  }, [children, Input, dataTestId, Label]);

  useEffect(() => {
    if (!fakeInputRef.current || !labelRef.current) return;

    const nextCursor = window.getComputedStyle(labelRef.current)['cursor'];
    fakeInputRef.current.style.cursor = nextCursor;
  }, [result.element]);

  return result;
};

export default useInputWithLabel;
