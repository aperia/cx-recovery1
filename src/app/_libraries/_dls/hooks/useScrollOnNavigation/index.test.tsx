import React, { useRef } from 'react';
import { render } from '@testing-library/react';

import throwError from '../../utils/throwError';
import useScrollOnNavigation from '.';
import {
  createMockMutationObserver,
  clearMockMutationObserver
} from '../../test-utils/mocks/mockMutationObserver';
import { className } from '../../utils';

const Comp = ({
  marginArg,
  isContainsArg,
  isContainerExist = true,
  isSelectedElementExist = true,
  direction = 'top',
  marginByItems
}: {
  isContainerExist?: boolean;
  isSelectedElementExist?: boolean;
  direction?: string;
  marginArg?: number;
  isContainsArg?: (target: HTMLElement) => boolean;
  marginByItems?: number;
}) => {
  const scrollContainerRef = useRef(null);

  useScrollOnNavigation(
    scrollContainerRef,
    { current: direction },
    isContainsArg,
    marginArg,
    marginByItems
  );

  return (
    <div>
      {isContainerExist && (
        <div ref={scrollContainerRef}>
          {isSelectedElementExist && (
            <div className={className.state.SELECTED} />
          )}
        </div>
      )}
    </div>
  );
};

const isContainsArgMock = (element: HTMLElement) => !!element;
const target = document.createElement('div');

createMockMutationObserver([{ target }]);
afterAll(clearMockMutationObserver);

describe('useScrollOnNavigation', () => {
  it('when container is not existed', () => {
    render(<Comp isContainerExist={false} />);

    expect(true).toEqual(true);
  });

  it('when container is existed', () => {
    render(<Comp isContainerExist />);

    expect(true).toEqual(true);
  });

  it('when marginArg less than 0', () => {
    const cantLessThan = jest.fn();
    jest.spyOn(throwError, 'cantLessThan').mockImplementation(cantLessThan);
    render(<Comp marginArg={-1} />);

    expect(cantLessThan).toBeCalledWith('margin', 0);
  });

  it('do not trigger scroll', () => {
    render(<Comp isContainsArg={isContainsArgMock} />);

    expect(true).toEqual(true);
  });

  it('do not trigger scroll', () => {
    render(<Comp isContainsArg={isContainsArgMock} />);

    expect(true).toEqual(true);
  });

  it('trigger scroll top', () => {
    Element.prototype.scrollTo = jest.fn();
    Object.defineProperties(HTMLElement.prototype, {
      offsetHeight: { get: () => 1000 },
      scrollTop: { get: () => 1000 }
    });

    render(
      <Comp
        isContainsArg={isContainsArgMock}
        marginArg={1000}
        direction="top"
      />
    );

    expect(true).toEqual(true);
  });

  it('trigger scroll bottom', () => {
    Element.prototype.scrollTo = jest.fn();
    Object.defineProperties(HTMLElement.prototype, {
      offsetHeight: { get: () => 1000 },
      offsetTop: { get: () => 1000 }
    });

    render(
      <Comp
        isContainsArg={isContainsArgMock}
        marginArg={1000}
        direction="bottom"
        isSelectedElementExist={false}
      />
    );

    expect(true).toEqual(true);
  });

  it('trigger scroll bottom', () => {
    Element.prototype.scrollTo = jest.fn();
    Object.defineProperties(HTMLElement.prototype, {
      offsetHeight: { get: () => 1000 },
      offsetTop: { get: () => 1000 }
    });

    jest.useFakeTimers();

    render(
      <Comp
        isContainsArg={isContainsArgMock}
        marginArg={1000}
        direction="bottom"
        marginByItems={2}
      />
    );
    jest.runAllTimers();

    expect(true).toEqual(true);
  });
});
