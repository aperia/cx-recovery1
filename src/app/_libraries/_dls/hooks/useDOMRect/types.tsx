export interface DOMRectConfig {
  element?: HTMLElement;
  watch?: boolean;
  picks?: [keyof DOMRect];
}

export interface UseDOMRectHook {
  (config: DOMRectConfig): DOMRect;
}
