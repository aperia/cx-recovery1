import React from 'react';

// testing library
import { render } from '@testing-library/react';
import '@testing-library/jest-dom';

// mocks
import '../../test-utils/mocks/mockCanvas';

// hooks
import useDOMRect from '.';

jest.mock('resize-observer-polyfill', () =>
  jest.requireActual('../../test-utils/mocks/MockResizeObserver')
);

const Wrapper: React.FC<any> = (props) => {
  useDOMRect(props.isUndefined ? undefined : props);

  return <div>Wrapper</div>;
};

describe('useDOMRect', () => {
  it('should cover 100%', () => {
    const { queryByText } = render(<Wrapper isUndefined />);

    expect(queryByText('Wrapper')).toBeInTheDocument();
  });

  it('should cover 100%', () => {
    const { queryByText } = render(<Wrapper />);

    expect(queryByText('Wrapper')).toBeInTheDocument();
  });

  it('should cover 100%', () => {
    const { rerender, queryByText } = render(
      <Wrapper
        watch
        element={{
          getBoundingClientRect: () => ({
            c: 'c'
          })
        }}
      />
    );

    rerender(
      <Wrapper
        watch
        element={{
          getBoundingClientRect: () => ({
            c: 'c'
          })
        }}
        picks={['b']}
      />
    );

    rerender(
      <Wrapper
        watch
        element={{
          getBoundingClientRect: () => ({
            c: 'c'
          })
        }}
        picks={['c']}
      />
    );

    expect(queryByText('Wrapper')).toBeInTheDocument();
  });
});
