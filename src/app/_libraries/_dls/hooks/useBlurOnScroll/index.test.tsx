import React from 'react';

// testing library
import { fireEvent, render } from '@testing-library/react';
import '@testing-library/jest-dom';

// mocks
import '../../test-utils/mocks/mockCanvas';

// hooks
import useBlurOnScroll from '.';

const Wrapper: React.FC<any> = ({ shouldRun }) => {
  useBlurOnScroll(
    {
      blur: () => undefined
    } as any,
    shouldRun
  );

  return <div>Wrapper</div>;
};

describe('useTodayPortal', () => {
  it('should cover 100%', () => {
    jest.useFakeTimers();
    const { rerender, queryByText } = render(<Wrapper shouldRun />);

    fireEvent.scroll(document.body, { target: { scrollY: 100 } });

    rerender(<Wrapper />);

    expect(queryByText('Wrapper')).toBeInTheDocument();
  });
});
