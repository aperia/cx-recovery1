import { Type } from './';
import { getType } from './Type';

describe('Test metadata > Type.ts', () => {
  it('should provide a metadata value which indicates the type of the property.', () => {
    class Person {
      @Type('string')
      firstName!: string;

      @Type('string')
      lastName!: string;

      @Type(Date)
      dateOfBirth!: Date;

      @Type('boolean')
      isMale!: boolean;

      @Type('number')
      salary!: number;
    }

    const person = new Person();
    expect(getType(person, 'firstName')).toBe('string');
    expect(getType(person, 'lastName')).toBe('string');
    expect(getType(person, 'dateOfBirth')).toBe(Date);
    expect(getType(person, 'isMale')).toBe('boolean');
    expect(getType(person, 'salary')).toBe('number');
  });
});
