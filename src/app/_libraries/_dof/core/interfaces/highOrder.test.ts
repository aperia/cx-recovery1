import { composeComponent, HighOrderFunction } from "./";

describe("Test interfaces > highOrder.ts", () => {
  it("test composeComponent function", () => {
    const initComponent = jest.fn().mockName("Init mock"),
      hoc1: HighOrderFunction<any> = () => jest.fn().mockName("Mock fn 1"),
      hoc2: HighOrderFunction<any> = () => jest.fn().mockName("Mock fn 2"),
      hoc3: HighOrderFunction<any> = () => jest.fn().mockName("Mock fn 3");
    const hocGroups: HighOrderFunction<any>[] = [hoc1, hoc2, hoc3];

    const result = composeComponent<any>(initComponent, ...hocGroups);

    expect(result.getMockName()).toEqual("Mock fn 3");
  });
});
