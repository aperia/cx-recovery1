/**
 * Represents a high-order function of a component.
 * @template TComponent Type of the components of the high-order pipeline, usually a function.
 */
export interface HighOrderFunction<TComponent = any> {
  /**
   * @param next The next component in the high-order pipeline.
   * @returns {TComponent} The resulted component.
   */
  (next: TComponent): TComponent;
}

/**
 * Composes a component from the initial one and an array of its high-order functions.
 * @template TComponent Type of the component. Default to `any`.
 * @param initialComponent The initial component.
 * @param highOrderFunctions The array of high-order functions of the component.
 * @returns {TComponent} The component as the result of execute high-order functions on the initial component.
 */
export function composeComponent<TComponent = any>(
  initialComponent: TComponent,
  ...highOrderFunctions: HighOrderFunction<TComponent>[]
): TComponent {
  const component = highOrderFunctions.reduce<TComponent>(
    (next, highOrderFunction) => highOrderFunction(next),
    initialComponent
  );
  return component;
}
