import React from "react";
import { WrappedFieldProps, BaseFieldProps, Field } from "redux-form";
import { Promising } from "./types";
import {
  ValidatorConstructor,
  ValidationOptions,
} from "../validation/interfaces";

export interface FieldValidationRuleDescriptor<
  TOptions extends ValidationOptions<TError> = ValidationOptions<any>,
  TError = string
> {
  ruleName: string;
  errorMsg: string;
  ruleOptions?: TOptions;
}

export type FieldValidationRule<
  TOptions extends ValidationOptions<TError> = ValidationOptions<any>,
  TError = string
> = FieldValidationRuleDescriptor<TOptions, TError>;

export interface FieldDescriptor<ControlProps = unknown> {
  name: string;
  type: string | React.ComponentType<ControlProps>;
  props?: ControlProps;
  dataField: string;
  label?: string;
  validationRules?: FieldValidationRule[];
  dataSourceApi?: DataApiConfiguration;
  dataSourceName?: string;
  onValueChangedFunc?: string;
  onBlurFunc?: string;
  /**
   * initial data list for dropdown, combobox
   */
  data?: any;
}

export interface ViewLayoutElement<LayoutProps = unknown> {
  name: string;
  renderedNode: React.ReactNode;
  layoutProps?: LayoutProps;
}

export interface ViewLayoutProps<LayoutProps = unknown> {
  viewElements: ViewLayoutElement<LayoutProps>[];
  viewName: string;
}

export interface ViewLayoutDescriptor<LayoutOptions = never> {
  name: string | React.ComponentType<LayoutOptions>;
  layoutOptions?: LayoutOptions;
}

/**
 * Provides information of how to render a field on a view.
 *
 * It can be a simple `string` that identifies which field to render without further customizations,
 * or a partial information of `FieldDescriptor` to override some or all properties of the field.
 *
 * When the field has a special need for its position, use `layoutProps` to specify the necessary layout properties
 * to describe the position of the field on the view.
 *
 * @template LayoutProps Type of the layout properties associated with a field to identify its position on the view.
 */
export type FieldDescriptorOverrides = Pick<FieldDescriptor, "name"> &
  Partial<FieldDescriptor>;
export type ViewElementDescriptor<LayoutProps = unknown> =
  | string
  | (FieldDescriptorOverrides & { layoutProps?: LayoutProps });

export interface DataApiConfiguration {
  name: string;
  url: string;
  customRequest?: string;
}

/**
 * Represents a descriptor which describes how a view looks like.
 * @template LayoutOptions Type of the options used to customize the view layout.
 * @template LayoutProps Type of the props used by the view layout to position a view element in a view.
 */
export interface ViewDescriptor<
  LayoutOptions extends ViewLayoutProps<LayoutProps> = ViewLayoutProps<any>,
  LayoutProps = unknown
> {
  /**
   * The ID of the view.
   */
  name: string;

  /**
   * The implementation class or the name/key of the implementation class in the layout registry
   * that handles the layout logic.
   */
  layout?: string | ViewLayoutDescriptor<LayoutOptions>;

  /**
   * The fields of the view.
   */
  fields?: ViewElementDescriptor<LayoutProps>[];

  /**
   * The data API for the view.
   */
  dataApi?: DataApiConfiguration;

  /**
   * The data API name for the view.
   */
  dataName?: string;

  /**
   * Is view cache in redux or not.
   */
  isCached?: boolean;
}

export interface CommonControlProps extends WrappedFieldProps {
  id: string;
  label?: string;
  tooltip?: string;
  data?: [];
  enable?: boolean;
  visible?: boolean;
  readOnly?: boolean;
  required?: boolean;
}

export interface ControlsMap {
  [controlKey: string]:
    | React.ComponentType<CommonControlProps>
    | Promising<React.ComponentType<CommonControlProps>>;
}

export interface ControlRegistry {
  readonly controlsMap: ControlsMap;
  registerControl<
    TControlProps extends CommonControlProps = CommonControlProps
  >(
    controlKey: string,
    controlType:
      | React.ComponentType<TControlProps>
      | Promising<React.ComponentType<TControlProps>>
  ): void;
  unregisterControl(controlKey: string): void;
}

export interface FieldsMap {
  [fieldId: string]: FieldDescriptor<any> | Promise<FieldDescriptor<any>>;
}

export interface FieldRegistry {
  readonly fieldsMap: FieldsMap;
  registerField(
    fieldId: string,
    field: FieldDescriptor<any> | Promise<FieldDescriptor<any>>
  ): void;
  registerFields(fields: Array<FieldDescriptor<any>>): void;
  unregisterField(fieldId: string): void;
}

export interface LayoutsMap {
  [layoutKey: string]:
    | React.ComponentType<ViewLayoutProps<any>>
    | Promise<React.ComponentType<ViewLayoutProps<any>>>;
}

export interface LayoutRegistry {
  readonly layoutsMap: LayoutsMap;
  registerLayout(
    layoutKey: string,
    layoutComponent:
      | React.ComponentType<any>
      | Promise<React.ComponentType<any>>
  ): void;
  unregisterLayout(layoutKey: string): void;
}

export interface ValidatorsMap<TError = string> {
  [validatorKey: string]:
    | ValidatorConstructor<any, any, TError>
    | Promise<ValidatorConstructor<any, any, TError>>;
}

export interface ValidatorRegistry {
  readonly validatorsMap: ValidatorsMap;
  registerValidator<
    TOptions extends ValidationOptions<TError> = ValidationOptions<any>,
    TValue = any,
    TError = string
  >(
    validatorKey: string,
    validatorClass:
      | ValidatorConstructor<TOptions, TValue, TError>
      | Promise<ValidatorConstructor<TOptions, TValue, TError>>
  ): void;
  unregisterValidator(validatorKey: string): void;
}

export interface ViewsMap {
  [viewId: string]:
    | Omit<ViewDescriptor<any, any>, "name">
    | Promise<Omit<ViewDescriptor<any, any>, "name">>;
}

export interface ViewRegistry {
  readonly viewsMap: ViewsMap;
  readonly allViews: Array<string> | undefined;

  registerView<
    LayoutOptions extends ViewLayoutProps<LayoutProps> = ViewLayoutProps<any>,
    LayoutProps = unknown
  >(
    viewId: string,
    viewDescriptor:
      | Omit<ViewDescriptor<LayoutOptions, LayoutProps>, "name">
      | Promise<Omit<ViewDescriptor<LayoutOptions, LayoutProps>, "name">>
  ): void;
  registerViews(
    viewDescriptors:
      | Array<ViewDescriptor<any, any>>
      | Promise<Array<ViewDescriptor<any, any>>>
  ): void;
  registerAvailableViews(allViews: Array<string>): void;
  unregisterView(viewId: string): void;
}

export interface DataSourceMap {
  [name: string]: Omit<DataApiConfiguration, "name">;
}

// Value Change handling
export interface ExtraFieldProps extends BaseFieldProps {
  props: {
    // Field level
    setValue: (value: any) => void;
    setVisible: (value: boolean) => void;
    setEnable: (value: boolean) => void;
    setReadOnly: (value: boolean) => void;
    setRequired: (value: boolean) => void;
    setData: (values: Array<any>) => void;
    setOthers: (others: any) => void;
  };
}
export interface ValueChangeMap {
  [valueChangeFuncName: string]: IValueChangeFunc;
}
export type IFindField = (name: string) => Field<ExtraFieldProps>;
export type ISetFormValue = (formKey: string, values: any) => void;
export type IValueChangeFunc = (
  name: string,
  newValue: any,
  previousValue: any,
  onFind: IFindField,
  formKey?: string,
  setFormValue?: ISetFormValue,
  event?: any,
  formValues?: any
) => void;
export interface IChangeFuncRegistry {
  valueChangeFuncMap: ValueChangeMap;
  register(
    valueChangeFuncName: string,
    handleValueChange: IValueChangeFunc
  ): void;
  resolve(valueChangeFuncName: string): IValueChangeFunc;
}
