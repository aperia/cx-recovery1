/**
 * Represents an abstract class.
 */
export interface AbstractClass<T> extends Function {
    prototype: T;
}

/**
 * Represents a constructor function.
 */
export type Constructor<T> = new (...args: any[]) => T;

/**
 * Represents a class of an object.
 */
export interface Class<T> extends AbstractClass<T> {
    new (...args: any[]): T;
}

/**
 * Checks whether a value is a class or not.
 * @param v The value to check
 */
export function isClass<T = any>(v: any): v is Class<T> {
    return typeof v === 'function';
}

/**
 * Represents a *promising* component.
 */
export interface Promising<TComponent> {
    /**
     * Asynchronously gets the given component.
     * @returns {Promise<TComponent>} The `Promise` which resolves to the component.
     */
    promise(): Promise<TComponent>;
}

export function isPromisingOf<TComponent>(value: any): value is Promising<TComponent> {
    return value !== undefined && typeof (value as Promising<TComponent>).promise === 'function';
}