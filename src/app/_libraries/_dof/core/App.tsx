/* eslint-disable react-hooks/exhaustive-deps */
import React, { useEffect, useMemo, useState } from 'react';
import { ReducersMapObject } from 'redux';
import { Provider } from 'react-redux';

import RegisteredComponentsContext, {
  RegisteredControlsMap,
  RegisteredFieldsMap,
  RegisteredHighOrderValidationFactoriesMap,
  RegisteredLayoutsMap,
  RegisteredComponentsContextData,
  RegisteredViewsMap,
  RegisteredDataSourceMap
} from './contexts/RegisteredComponentsContext';

import {
  ValidatorConstructor,
  HighOrderValidation
} from './validation/interfaces';
import * as builtinHighOrderValidationFactories from './validation/factories';

import {
  ControlsMap,
  FieldsMap,
  LayoutsMap,
  ValidatorsMap,
  ViewsMap,
  DataSourceMap
} from './interfaces/dynamic';
import {
  controlRegistry,
  fieldRegistry,
  layoutRegistry,
  validatorRegistry,
  viewRegistry
} from './registries';

import store, { addReducersToStore } from './redux/createAppStore';
import dynamicMiddleware from './redux/dynamicMiddleware';

const { addMiddleware } = dynamicMiddleware;

export const ALL_VIEWS = 'allViews';

export interface AppProps {
  customControls?: ControlsMap;
  customLayouts?: LayoutsMap;
  customReducers?: ReducersMapObject<any, any>;
  customValidators?: ValidatorsMap;
  customDataSource?: DataSourceMap;
  urlConfigs: string[];
  urlDataSource?: string[];
  middleware?: any[];
}

interface Config {
  registeredFields: RegisteredFieldsMap;
  registeredViews: RegisteredViewsMap;
  allViews?: Array<string>;
}

const App: React.FunctionComponent<AppProps> = ({
  children,
  customControls,
  customLayouts,
  customReducers,
  customValidators,
  customDataSource,
  urlDataSource,
  urlConfigs,
  middleware
}) => {
  // Add custom reducer
  useMemo(() => addReducersToStore(store, customReducers), [customReducers]);

  // Add custom middleware
  useMemo(() => addMiddleware(middleware), [middleware]);

  const [registeredComponents, setRegisteredComponents] =
    useState<RegisteredComponentsContextData>({
      registeredControls: {},
      registeredFields: {},
      registeredHighOrderValidationFactories: {},
      registeredLayouts: {},
      registeredViews: {},
      registeredDataSource: {},
      allViews: undefined
    });

  useEffect(() => {
    async function resolveRegisteredComponents(
      customControlsMap: ControlsMap = {},
      customLayoutsMap: LayoutsMap = {},
      customValidatorsMap: ValidatorsMap = {},
      customDataSourceMap: DataSourceMap = {},
      urlDataSourceMap: string[] = []
    ): Promise<void> {
      const [
        fetchConfigData,
        registeredControls,
        registeredHighOrderValidationFactories,
        registeredLayouts,
        registeredDataSource
      ] = await Promise.all([
        composeConfigData(urlConfigs),
        composeRegisteredControls(customControlsMap),
        composeRegisteredHighOrderValidationFactories(customValidatorsMap),
        composeRegisteredLayouts(customLayoutsMap),
        composeRegisteredDataSource(urlDataSourceMap, customDataSourceMap)
      ]);

      const { registeredFields, registeredViews, allViews } = fetchConfigData;
      setRegisteredComponents({
        registeredControls,
        registeredFields,
        registeredHighOrderValidationFactories,
        registeredLayouts,
        registeredViews,
        registeredDataSource,
        allViews
      });
    }

    resolveRegisteredComponents(
      customControls,
      customLayouts,
      customValidators,
      customDataSource,
      urlDataSource
    );
  }, [customControls, customLayouts, customValidators]);

  return (
    <Provider store={store}>
      <RegisteredComponentsContext.Provider value={registeredComponents}>
        {children}
      </RegisteredComponentsContext.Provider>
    </Provider>
  );
};

export default App;

async function composeRegisteredControls(
  customControls: ControlsMap
): Promise<RegisteredControlsMap> {
  const registeredControls: RegisteredControlsMap = {};
  await copyValuesAsync(controlRegistry.controlsMap, registeredControls);
  await copyValuesAsync(customControls, registeredControls);
  return registeredControls;
}

async function composeConfigData(urlConfigs: string[]): Promise<Config> {
  let configFieldView: Config = {
    registeredFields: {},
    registeredViews: {},
    allViews: undefined
  };

  let fieldMaps: FieldsMap = {};
  let viewMaps: ViewsMap = {};
  let allViews: Array<string> | undefined = undefined;

  await Promise.all(
    urlConfigs.map(url =>
      fetch(url, {
        headers: { Accept: 'application/json' }
      }).then(res => res.json())
    )
  ).then(async data => {
    const configData = mergeDeep(data);
    fieldMaps = configData['fields'].reduce(
      (map: any, field: { name: string }) => ({ ...map, [field.name]: field }),
      {}
    );

    viewMaps = configData['views'].reduce(
      (map: any, view: { name: string }) => ({ ...map, [view.name]: view }),
      {}
    );

    // ADD 20201014: allViews
    allViews = process.env.REACT_APP_ALL_VIEWS
      ? configData[process.env.REACT_APP_ALL_VIEWS]
      : configData[ALL_VIEWS];
  });

  await Promise.all([
    composeRegisteredFields(fieldMaps),
    composeRegisteredViews(viewMaps)
  ]).then(([registeredFields, registeredViews]) => {
    configFieldView = {
      registeredFields,
      registeredViews
    };
  });

  // ADD 20201014: allViews
  allViews &&
    (await composeRegisteredAvailableViews(allViews).then(views => {
      configFieldView = {
        ...configFieldView,
        allViews: views
      };
    }));

  return configFieldView;
}
async function composeRegisteredFields(
  customFields: FieldsMap
): Promise<RegisteredFieldsMap> {
  const registeredFields: RegisteredFieldsMap = {};
  await copyValuesAsync(fieldRegistry.fieldsMap, registeredFields);
  await copyValuesAsync(customFields, registeredFields);
  return registeredFields;
}

async function composeRegisteredLayouts(
  customLayouts: LayoutsMap
): Promise<RegisteredLayoutsMap> {
  const registeredLayouts: RegisteredLayoutsMap = {};
  await copyValuesAsync(layoutRegistry.layoutsMap, registeredLayouts);
  await copyValuesAsync(customLayouts, registeredLayouts);
  return registeredLayouts;
}

async function composeRegisteredHighOrderValidationFactories(
  customValidators: ValidatorsMap
): Promise<RegisteredHighOrderValidationFactoriesMap> {
  const customHighOrderValidationFactories: RegisteredHighOrderValidationFactoriesMap =
    {};
  const registeredValidators = {
    ...validatorRegistry.validatorsMap,
    ...customValidators
  };
  await Promise.all(
    Object.keys(registeredValidators).map(async key => {
      const validatorConstructor = await Promise.resolve(
        registeredValidators[key]
      );
      customHighOrderValidationFactories[key] =
        createHighOrderValidationFactoryFromValidatorDescriptor(
          validatorConstructor
        );
    })
  );
  return {
    ...builtinHighOrderValidationFactories,
    ...customHighOrderValidationFactories
  };
}

function composeRegisteredViews(
  customViews: ViewsMap
): Promise<RegisteredViewsMap> {
  return Promise.resolve({
    ...viewRegistry.viewsMap,
    ...customViews
  });
}

function composeRegisteredAvailableViews(
  allViews: Array<string>
): Promise<Array<string>> {
  return Promise.resolve(
    viewRegistry.allViews
      ? [...viewRegistry.allViews, ...allViews]
      : [...allViews]
  );
}

async function composeRegisteredDataSource(
  urlDataSource: string[],
  customDataSource: DataSourceMap
): Promise<RegisteredDataSourceMap> {
  let dataSourceMap: DataSourceMap = {};
  await Promise.all(
    urlDataSource.map(url => fetch(url).then(res => res.json()))
  ).then(async data => {
    const dataSource = mergeDeepDataSource(data);
    dataSourceMap = dataSource.reduce<DataSourceMap>(
      (map: any, dataSrc: { name: any }) => ({
        ...map,
        [dataSrc.name]: dataSrc
      }),
      {}
    );
  });

  await copyValuesAsync(customDataSource, dataSourceMap);

  return dataSourceMap;
}

export function createHighOrderValidationFactoryFromValidatorDescriptor<
  TOptions = any,
  TValue = any,
  TError = string
>(
  constructor: ValidatorConstructor<TOptions, TValue, TError>
): (options: TOptions) => HighOrderValidation<TValue, TError> {
  return (options?: TOptions) => next => (value, context) => {
    const validator = new constructor(options);
    return validator.validate(value, context) || next(value, context);
  };
}

async function copyValuesAsync<TValue>(
  source: { [key: string]: TValue | Promise<TValue> },
  dest: { [key: string]: TValue }
): Promise<void> {
  await Promise.all(
    Object.keys(source).map(async key => {
      const value = await Promise.resolve(source[key]);
      dest[key] = value;
    })
  );
}

// Update 20200324
function mergeDeep(source: any[]): any {
  let fields: any[] = [];
  let views: any[] = [];
  let allViews = undefined;

  source.forEach(item => {
    fields = item['fields'] ? [...fields, ...item['fields']] : fields;
  });
  source.forEach(item => {
    views = item['views'] ? [...views, ...item['views']] : views;
  });

  source.forEach(item => {
    allViews = item[ALL_VIEWS];
  });

  fields = [...fields]
    .reverse()
    .filter(
      (value, index, arr) =>
        arr.findIndex(t => t['name'] === value['name']) === index
    );
  views = [...views]
    .reverse()
    .filter(
      (value, index, arr) =>
        arr.findIndex(t => t['name'] === value['name']) === index
    );
  return {
    fields: fields,
    views: views,
    allViews
  };
}

function mergeDeepDataSource(source: any[]): any[] {
  let dataSourceNames: any[] = [];

  source.forEach(item => {
    dataSourceNames = [...dataSourceNames, ...item];
  });

  dataSourceNames = [...dataSourceNames].filter(
    (value, index, arr) =>
      arr.findIndex(t => t['name'] === value['name']) === index
  );
  return dataSourceNames;
}
