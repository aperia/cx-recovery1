import { HighOrderValidationFactory, ValidationOptions } from "../interfaces";

const Required: HighOrderValidationFactory<ValidationOptions, string> = ({
  errorMsg = "Value cannot be blank or empty.",
} = {}) => (next) => (value, context) => {
  const requiredOf = context.requiredOf ? context.requiredOf() : true;

  return (value === undefined ||
    value === null ||
    (typeof value === "string" && value.trim() === "") ||
    (Array.isArray(value) && value.length === 0)) &&
    requiredOf
    ? errorMsg
    : next(value, context);
};

export default Required;
