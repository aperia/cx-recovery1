import Required from "./Required";
import NoValidation from "../NoValidation";
import { ValidationContext } from "../interfaces";

describe("MinLength(number)", function () {
  const context: ValidationContext = {
    labelOf(): string | undefined {
      return undefined;
    },
    valueOf() {
      return undefined;
    },
  };

  it("should return a validation message when the value is null or undefined.", function () {
    const validationFn = Required({})(NoValidation);
    const message1 = validationFn(null, context);
    expect(message1).not.toBeUndefined();

    const message2 = validationFn(undefined, context);
    expect(message2).not.toBeUndefined();
  });

  it("should NOT return a validation message when a value is neither null nor undefined.", function () {
    const validationFn = Required({})(NoValidation);
    const message = validationFn("12345", context);
    expect(message).toBeUndefined();
  });

  it("should NOT return a validation message when options is undefined.", function () {
    const contextRequired = {
      ...context,
      requiredOf() {
        return true;
      },
    };

    const validationFn = Required(undefined as any)(NoValidation);
    const message = validationFn([] as any, contextRequired);
    expect(message).not.toBeUndefined();
  });
});
