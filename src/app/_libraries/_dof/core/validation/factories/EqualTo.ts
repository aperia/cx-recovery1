import { CrossFieldsValidationOptions, HighOrderValidationFactory } from '../interfaces';

const EqualTo: HighOrderValidationFactory<CrossFieldsValidationOptions, any> = ({ 
    fieldToCompare, 
    errorMsg
} = {}) => (next) => (value, context) => 
    !!value && !!fieldToCompare && value !== context.valueOf(fieldToCompare) 
        ? (errorMsg || `Value must be equal to ${context.labelOf(fieldToCompare)}.`) 
        : next(value, context);

export default EqualTo;
