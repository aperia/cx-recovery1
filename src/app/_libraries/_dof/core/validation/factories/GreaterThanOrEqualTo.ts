import { CrossFieldsValidationOptions, HighOrderValidationFactory } from '../interfaces';

const GreaterThanOrEqualTo: HighOrderValidationFactory<CrossFieldsValidationOptions, any> = ({ 
    fieldToCompare, 
    errorMsg
} = {}) => (next) => (value, context) => 
    !!value && !!fieldToCompare && value < context.valueOf(fieldToCompare) 
        ? (errorMsg || `Value must be greater than or equal to ${context.labelOf(fieldToCompare)}.`) 
        : next(value, context);

export default GreaterThanOrEqualTo;
