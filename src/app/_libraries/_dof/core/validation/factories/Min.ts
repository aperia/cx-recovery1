import { HighOrderValidationFactory, ValidationOptions } from "../interfaces";
import OfType from "./OfType";
import { compareTo } from "../../utils";

export interface MinValidationOptions<TError = string>
  extends ValidationOptions<TError> {
  minValue?: number;
}

const Min: HighOrderValidationFactory<
  MinValidationOptions,
  number | string
> = ({ minValue, errorMsg = "" } = {}) => (next) =>
  OfType<number | string>(
    errorMsg,
    "number",
    "string",
  )((value, context) => {
    if (value !== undefined && value !== null && minValue !== undefined) {
      // For typeof value == "number"
      if (typeof value == "number") {
        return value < minValue ? errorMsg : next(value, context);
        // For typeof value == "string"
      } else {
        return compareTo(value, minValue.toString()) == -1
          ? errorMsg
          : next(value, context);
      }
    } else {
      return next(value, context);
    }
  });

export default Min;
