import { HighOrderValidationFactory, ValidationOptions } from "../interfaces";
import OfType from "./OfType";
import { compareTo } from "../../utils";

export interface MaxValidationOptions<TError = string>
  extends ValidationOptions<TError> {
  maxValue?: number;
}

const Max: HighOrderValidationFactory<
  MaxValidationOptions,
  number | string
> = ({ maxValue, errorMsg = "" } = {}) => (next) =>
  OfType<number | string>(
    errorMsg,
    "number",
    "string",
  )((value, context) => {
    if (value !== undefined && value !== null && maxValue !== undefined) {
      // For typeof value == "number"
      if (typeof value == "number") {
        return value > maxValue ? errorMsg : next(value, context);
        // For typeof value == "string"
      } else {
        return compareTo(value, maxValue.toString()) == 1
          ? errorMsg
          : next(value, context);
      }
    } else {
      return next(value, context);
    }
  });

export default Max;
