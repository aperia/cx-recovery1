import { valueChangeRegistry } from "./";
import { IValueChangeFunc } from "..";

describe("Text Registries > valueChangeRegistry", () => {
  const mockValueChangeFunc: IValueChangeFunc = () => {};

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("test register function", () => {
    valueChangeRegistry.register("mock-value-change-fn-1", mockValueChangeFunc);

    expect(valueChangeRegistry.valueChangeFuncMap["mock-value-change-fn-1"]).toEqual(
      mockValueChangeFunc
    );
  });

  it("test resolve function", () => {
    valueChangeRegistry.register("mock-value-change-fn-1", mockValueChangeFunc);

    const result = valueChangeRegistry.resolve("mock-value-change-fn-1");
    expect(result).toEqual(mockValueChangeFunc);
  });
});
