import React from 'react';
import { ControlsMap, ControlRegistry } from '../interfaces/dynamic';
import { Promising } from '../interfaces/types';

class DefaultControlRegistry implements ControlRegistry {
    readonly controlsMap: ControlsMap = {};

    registerControl(controlKey: string, controlType: React.ComponentType<any> | Promising<React.ComponentType<any>>): void {
        this.controlsMap[controlKey] = controlType;
    }

    unregisterControl(controlKey: string): void {
        delete this.controlsMap[controlKey];
    }
}

const controlRegistry: ControlRegistry = new DefaultControlRegistry();

export default controlRegistry;