import { controlRegistry } from "./";

describe("Text Registries > controlRegistry", () => {
  const mockControlComponent = {
    displayName: "Control component",
  } as React.FunctionComponent;

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("test registerControl function", () => {
    controlRegistry.registerControl("mock-control-1", mockControlComponent);

    expect(controlRegistry.controlsMap["mock-control-1"]).toEqual(
      mockControlComponent
    );
  });

  it("test unregisterControl function", () => {
    controlRegistry.registerControl("mock-control-1", mockControlComponent);
    expect(controlRegistry.controlsMap["mock-control-1"]).toEqual(
      mockControlComponent
    );

    controlRegistry.unregisterControl("mock-control-1");
    expect(controlRegistry.controlsMap["mock-control-1"]).toBeUndefined();
  });
});
