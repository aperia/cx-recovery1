import { resolveControl, resolveView } from "./actions";
import { Promising, ViewDescriptor } from "..";
import {
  RESOLVE_CONTROL_SUCCESS,
  RESOLVE_CONTROL_FAILURE,
  RESOLVE_VIEW_SUCCESS,
  RESOLVE_VIEW_FAILURE,
} from "./actionTypes";
import { FunctionComponent } from "react";

describe("Text Redux > actions", () => {
  const mockControlComponent = {
    displayName: "Control component",
  } as FunctionComponent<any>;

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("test resolveControl in case registeredControl is promising > success", () => {
    const dispatchFn = jest.fn();
    const registeredControlPr = {
      promise: () =>
        Promise.resolve<React.ComponentType<any>>(mockControlComponent),
    } as Promising<React.ComponentType<any>>;

    const resolveControlCb = resolveControl(
      "mock-control-promise-resolve",
      registeredControlPr
    );
    expect(typeof resolveControlCb).toBe("function");

    resolveControlCb(dispatchFn, jest.fn(), {} as never).then(() => {
      expect(dispatchFn).toHaveBeenCalledWith({
        controlKey: "mock-control-promise-resolve",
        controlType: mockControlComponent,
        type: RESOLVE_CONTROL_SUCCESS,
      });
    });
  });

  it("test resolveControl in case registeredControl is promising > failure", () => {
    const dispatchFn = jest.fn();
    const registeredControlPr = {
      promise: () => Promise.reject<any>("mock error"),
    } as Promising<React.ComponentType<any>>;

    const resolveControlCb = resolveControl(
      "mock-control-promise-reject",
      registeredControlPr
    );
    expect(typeof resolveControlCb).toBe("function");

    resolveControlCb(dispatchFn, jest.fn(), {} as never).then(() => {
      expect(dispatchFn).toHaveBeenCalledWith({
        controlKey: "mock-control-promise-reject",
        error: "mock error",
        type: RESOLVE_CONTROL_FAILURE,
      });
    });
  });

  it("test resolveControl in case registeredControl is component", () => {
    const dispatchFn = jest.fn();

    const resolveControlCb = resolveControl(
      "mock-control-component",
      mockControlComponent
    );
    expect(typeof resolveControlCb).toBe("function");

    resolveControlCb(dispatchFn, jest.fn(), {} as never).then(() => {
      expect(dispatchFn).toHaveBeenCalledWith({
        controlKey: "mock-control-component",
        controlType: mockControlComponent,
        type: RESOLVE_CONTROL_SUCCESS,
      });
    });
  });

  it("test resolveView > success", () => {
    const dispatchFn = jest.fn();
    const viewPromise = Promise.resolve<Omit<ViewDescriptor<any, any>, "name">>(
      {
        layout: "mock-layout",
      }
    );

    const resolveViewCb = resolveView("mock-view-promise-resolve", viewPromise);
    expect(typeof resolveViewCb).toBe("function");

    resolveViewCb(dispatchFn, jest.fn(), {} as never).then(() => {
      expect(dispatchFn).toHaveBeenCalledWith({
        viewId: "mock-view-promise-resolve",
        viewDescriptor: {
          layout: "mock-layout",
        },
        type: RESOLVE_VIEW_SUCCESS,
      });
    });
  });

  it("test resolveView > failure", () => {
    const dispatchFn = jest.fn();
    const viewPromise = Promise.reject<any>("mock error");

    const resolveViewCb = resolveView("mock-view-promise-reject", viewPromise);
    expect(typeof resolveViewCb).toBe("function");

    resolveViewCb(dispatchFn, jest.fn(), {} as never).then(() => {
      expect(dispatchFn).toHaveBeenCalledWith({
        viewId: "mock-view-promise-reject",
        error: "mock error",
        type: RESOLVE_VIEW_FAILURE,
      });
    });
  });
});
