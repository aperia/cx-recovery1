import React from 'react';
import { FormStateMap } from 'redux-form';
import { ViewDescriptor } from '../interfaces';

export interface RegisteredControlsCache {
    [controlKey: string]: React.ComponentType<any>;
}

export interface RegisteredViewsCache {
    [viewName: string]: Omit<ViewDescriptor<any, any>, 'name'>;
}

export interface AppCaches {
    controls: RegisteredControlsCache;
    views: RegisteredViewsCache;
}

export interface AppState {
    form: FormStateMap;
    caches: AppCaches;
}