import { caches } from "./reducers";
import {
  RESOLVE_CONTROL_SUCCESS,
  RESOLVE_CONTROL_FAILURE,
  RESOLVE_VIEW_SUCCESS,
  RESOLVE_VIEW_FAILURE,
} from "./actionTypes";

describe("Text Redux > reducers", () => {
  const mockControlComponent = {
    displayName: "Control component",
  } as React.FunctionComponent<any>;

  afterEach(() => {
    jest.clearAllMocks();
  });

  it("test controls reducer action > RESOLVE_CONTROL_SUCCESS", () => {
    const appState = caches(
      {
        controls: {},
        views: {},
      },
      {
        controlKey: "mock-control-key",
        controlType: mockControlComponent,
        type: RESOLVE_CONTROL_SUCCESS,
      }
    );

    expect(appState.controls).toEqual({
      "mock-control-key": mockControlComponent,
    });
  });
  it("test controls reducer action > RESOLVE_CONTROL_FAILURE", () => {
    const appState = caches(
      {
        controls: {
          "mock-control-key": mockControlComponent,
        },
        views: {},
      },
      {
        controlKey: "mock-control-key-error",
        error: "Mock error",
        type: RESOLVE_CONTROL_FAILURE,
      }
    );

    expect(appState.controls).toEqual({
      "mock-control-key": mockControlComponent,
    });
  });

  it("test views reducer action > RESOLVE_VIEW_SUCCESS", () => {
    const appState = caches(
      {
        controls: {},
        views: {},
      },
      {
        viewId: "mock-view-id",
        viewDescriptor: {
          layout: "mock-layout",
        },
        type: RESOLVE_VIEW_SUCCESS,
      }
    );

    expect(appState.views["mock-view-id"].layout).toEqual("mock-layout");
  });

  it("test views reducer action > RESOLVE_VIEW_FAILURE", () => {
    const appState = caches(
      {
        controls: {},
        views: {
          "mock-view-id": {
            layout: "mock-layout",
          },
        },
      },
      {
        viewId: "mock-view-id-error",
        error: "mock error",
        type: RESOLVE_VIEW_FAILURE,
      }
    );

    expect(appState.views["mock-view-id"].layout).toEqual("mock-layout");
    expect(appState.views["mock-view-id-error"]).toBeUndefined();
  });
});
