import { App, DataApiContext, View } from './index';

describe('Test CORE > index', () => {
  it('Should import as default successfully', () => {
    expect(App).not.toBeNull();
    expect(View).not.toBeNull();
    expect(DataApiContext).not.toBeNull();
  });
});
