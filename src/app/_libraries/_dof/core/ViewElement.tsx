/* eslint-disable react-hooks/exhaustive-deps */
import React, {
  useContext,
  useMemo,
  useEffect,
  useState,
  useCallback
} from 'react';
import { Field as FormField, change } from 'redux-form';

import {
  FieldDescriptor,
  FieldValidationRule,
  FieldValidationRuleDescriptor
} from './interfaces/dynamic';

import { useControl } from './hooks';
import RegisteredComponentsContext from './contexts/RegisteredComponentsContext';
import DataApiContext, { DataApiRequest } from './contexts/DataApiContext';
import valueChangeRegistry from './registries/valueChangeRegistry';
import { useDispatch } from 'react-redux';
import { ValidationContext } from './validation/interfaces';
import { composeComponent } from './interfaces/highOrder';
import NoValidation from './validation/NoValidation';

export interface ViewElementProps {
  id: string;
  dataTestId?: string;
  name: string;
  formKey: string;
  overrides?: Partial<Omit<FieldDescriptor, 'name'>>;
  childToParentCallback?: any;
}
// FIELD
const field = (fieldProps: any) => {
  const {
    setEnable,
    setVisible,
    setData,
    setValue,
    setReadOnly,
    setRequired,
    setOthers,

    id,
    dataTestId,
    data,
    label,
    enable,
    readOnly,
    required,
    visible,
    Control,
    meta,
    input,

    ...props
  } = fieldProps;

  if (!visible) return null;
  if (!Control) return null;

  return (
    <Control
      setEnable={setEnable}
      setVisible={setVisible}
      setData={setData}
      setValue={setValue}
      setReadOnly={setReadOnly}
      setRequired={setRequired}
      setOthers={setOthers}
      id={id}
      dataTestId={dataTestId}
      data={data}
      label={label}
      enable={enable}
      readOnly={readOnly}
      required={required}
      Control={Control}
      meta={meta}
      input={input}
      {...props}
    />
  );
};

const ViewElement: React.RefForwardingComponent<any, ViewElementProps> = (
  { id, dataTestId, name, formKey, overrides, childToParentCallback },
  ref
) => {
  const registeredComponentsContext = useContext(RegisteredComponentsContext);

  const [onChangeFuncNotFound, setOnChangeFuncNotFound] = useState('');

  const fieldDescriptor = useMemo<FieldDescriptor<any>>(() => {
    const { registeredFields: { [name]: registeredField } = {} } =
      registeredComponentsContext;
    return {
      ...registeredField,
      ...overrides,
      name
    };
  }, [name, registeredComponentsContext, overrides]);

  const {
    dataField: configDataField,
    type,
    props = {},
    label,
    dataSourceApi,
    dataSourceName,
    onValueChangedFunc,
    onBlurFunc,
    data: listItem,
    validationRules,
    ...rest
  } = fieldDescriptor;

  // initial data list for dropdown, combobox
  const [data, setData] = useState(listItem);

  const enableProp = props.enable;
  const [enable, setEnable] = useState(
    enableProp === undefined ? true : enableProp
  );

  const visibleProp = props.visible;
  const [visible, setVisible] = useState(
    visibleProp === undefined ? true : visibleProp
  );

  const readOnlyProp = props.readOnly;
  const [readOnly, setReadOnly] = useState(
    readOnlyProp === undefined ? false : readOnlyProp
  );

  const requiredProp = props.required;
  const [required, setRequired] = useState<boolean>(
    requiredProp === undefined ? false : requiredProp
  );

  const [others, setOthers] = useState<any>({ ...rest, validationRules });

  // dataSourceApi
  const dataSourceApiContext = useContext(DataApiContext);
  useEffect(() => {
    if (dataSourceApi) {
      const { url, customRequest } = dataSourceApi;
      let request: DataApiRequest;
      if (customRequest && dataSourceApiContext) {
        const customRequestMethod = dataSourceApiContext[customRequest];
        if (customRequestMethod === undefined) {
          return;
        }
        //console.log("customRequestMethod: ", customRequestMethod);
        request = customRequestMethod(url);
      } else {
        request = {
          method: 'GET',
          headers: [],
          requestData: {}
        };
      }
      (async function () {
        const { method, headers, requestData } = request;
        //console.log("field " + label + " request data: ", request);

        let response: Response;
        if (method === 'GET' || method === 'HEAD') {
          response = await fetch(url, {
            method,
            headers: headers.reduce<Record<string, string>>(
              (curr, next) => ({ ...curr, ...next }),
              {}
            )
          });
        } else {
          response = await fetch(url, {
            method,
            body: JSON.stringify(requestData),
            headers: headers.reduce<Record<string, string>>(
              (curr, next) => ({ ...curr, ...next }),
              {}
            )
          });
        }
        const values = await response.json();
        setData(values);
      })();
    }
  }, [dataSourceApiContext, dataSourceApi]);

  // dataSourceName
  const { registeredDataSource } = registeredComponentsContext;
  useEffect(() => {
    if (
      dataSourceName &&
      !dataSourceApi &&
      Object.entries(registeredDataSource).length > 0
    ) {
      const dataSource = registeredDataSource[dataSourceName];
      if (dataSource === undefined) {
        return;
      }

      const { url, customRequest } = dataSource;
      let request: DataApiRequest;
      if (customRequest && dataSourceApiContext) {
        const customRequestMethod = dataSourceApiContext[customRequest];

        //console.log("customRequestMethod 2: ", customRequestMethod);

        request = customRequestMethod(url);
      } else {
        request = {
          method: 'GET',
          headers: [],
          requestData: {}
        };
      }
      (async function () {
        const { method, headers, requestData } = request;
        //console.log("field " + label + " request data: ", request);

        let response: Response;
        if (method === 'GET' || method === 'HEAD') {
          response = await fetch(url, {
            method,
            headers: headers.reduce<Record<string, string>>(
              (curr, next) => ({ ...curr, ...next }),
              {}
            )
          });
        } else {
          response = await fetch(url, {
            method,
            body: JSON.stringify(requestData),
            headers: headers.reduce<Record<string, string>>(
              (curr, next) => ({ ...curr, ...next }),
              {}
            )
          });
        }
        const values = await response.json();
        setData(values);
      })();
    }
  }, [dataSourceApiContext, dataSourceName]);

  // onValueChangedFunc
  const onChangeHandler = (event: any, newValue: any, previousValue: any) => {
    // AP CR: trigger onChange in onBlur even previousValue === newValue
    if (onValueChangedFunc) {
      if (valueChangeRegistry.resolve(onValueChangedFunc)) {
        childToParentCallback(
          onValueChangedFunc,
          newValue,
          previousValue,
          name,
          event
        );
      } else {
        setOnChangeFuncNotFound(`${onValueChangedFunc} not found!`);
      }
    }
  };

  // onBlurHandler
  const onBlurHandler = (event: any, newValue: any, previousValue: any) => {
    if (onBlurFunc) {
      if (valueChangeRegistry.resolve(onBlurFunc)) {
        childToParentCallback(onBlurFunc, newValue, previousValue, name, event);
      } else {
        setOnChangeFuncNotFound(`${onBlurFunc} not found!`);
      }
    }
  };

  const dispatch = useDispatch();
  const setValue = (value: any) => {
    if (configDataField) {
      dispatch(change(formKey, configDataField, value, false));
    }
  };

  // Field-level validate
  const validate = useCallback(
    (value, allValues) => {
      const {
        registeredFields = {},
        registeredHighOrderValidationFactories = {}
      } = registeredComponentsContext;

      const validationContext: ValidationContext = {
        labelOf: fieldIdOrDataField => {
          if (label) {
            return label;
          } else {
            return fieldIdOrDataField;
          }
        },
        valueOf: fieldIdOrDataField => {
          const registeredField = registeredFields[fieldIdOrDataField];
          if (registeredField) {
            const { dataField } = registeredField;
            return allValues[dataField];
          } else {
            return value;
          }
        },
        requiredOf: () => required
      };

      if (
        configDataField === undefined ||
        others.validationRules === undefined ||
        others.validationRules.length === 0
      ) {
        return;
      }

      const validateFn = composeComponent(
        NoValidation,
        ...normalizeValidationRules(others.validationRules)
          .reverse()
          .map(({ ruleName, ruleOptions }) => {
            const highOrderValidationFactory =
              registeredHighOrderValidationFactories[ruleName];
            return highOrderValidationFactory(ruleOptions);
          })
      );

      // // If value is a object (for dropdownlist)
      // if(typeof value === "object")
      //   return validateFn(value["key"], validationContext);
      // else
      //   return validateFn(value, validationContext);
      return validateFn(value, validationContext);
    },
    [registeredComponentsContext, required, others]
  );

  const Control = useControl(type);
  return type === undefined ? (
    <h5>Field {name} is not found!</h5>
  ) : dataSourceApi &&
    dataSourceApi.customRequest &&
    dataSourceApiContext &&
    dataSourceApiContext[dataSourceApi.customRequest] === undefined ? (
    <h5>
      {name}: customRequest {dataSourceApi.customRequest} is not found!
    </h5>
  ) : dataSourceName && registeredDataSource[dataSourceName] === undefined ? (
    <h5>
      {name}: DataSourceName {dataSourceName} is not found!
    </h5>
  ) : onChangeFuncNotFound ? (
    <h5>
      {name}: ValueChangeFunc {onValueChangedFunc} is not found!
    </h5>
  ) : (
    <FormField
      name={configDataField || name}
      onChange={onChangeHandler}
      onBlur={onBlurHandler}
      ref={ref}
      props={{
        setEnable,
        setVisible,
        setData,
        setValue,
        setReadOnly,
        setRequired,
        setOthers,
        ...props,
        ...others,

        id,
        dataTestId,
        data,
        label,
        enable,
        readOnly,
        required,
        visible,
        Control
      }}
      validate={enable && !readOnly ? validate : undefined}
      component={field}
    />
  );
};

function normalizeValidationRules(
  validationRules: FieldValidationRule[]
): FieldValidationRuleDescriptor[] {
  return validationRules.map<FieldValidationRuleDescriptor>(rule => ({
    ...rule,
    ruleOptions: {
      ...rule.ruleOptions,
      errorMsg: rule.errorMsg
    }
  }));
}

export default React.forwardRef(ViewElement);
