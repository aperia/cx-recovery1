import '@testing-library/jest-dom';
import { render, waitFor } from '@testing-library/react';
import React from 'react';
import { Reducer } from 'redux';
import { ValidationContext, Validator } from '.';
import App, {
  createHighOrderValidationFactoryFromValidatorDescriptor
} from './App';
import { ValidatorsMap } from './interfaces';
import store from './redux/createAppStore';
import { validatorRegistry, viewRegistry } from './registries';
import NoValidation from './validation/NoValidation';

describe('Text CORE > App', () => {
  let originFetch: any;

  const reducerMock1: Reducer<{ data1: string }> = (
    state = { data1: 'mock-data-1' }
  ) => {
    return state;
  };
  const reducerMock2: Reducer<{ data2: string }> = (
    state = { data2: 'mock-data-2' }
  ) => {
    return state;
  };

  beforeEach(() => {
    jest.resetModules();

    originFetch = window.fetch;
    window.fetch = jest.fn().mockResolvedValue({
      json: jest.fn().mockResolvedValue({
        fields: [
          {
            name: 'field-1'
          }
        ],
        views: [
          {
            name: 'views-1'
          }
        ],
        allViews: ['all-view-1', 'all-view-2'],
        name: {
          url: 'mock url'
        }
      })
    } as any);
  });

  afterEach(() => {
    window.fetch = originFetch;
  });

  it('Render DOF App with url config & custom reducers', async () => {
    const wrapper = render(
      <App
        urlConfigs={['mockFieldUrl', 'mockViewUrl']}
        customReducers={{ reducerMock1, reducerMock2 }}
      >
        <div>Mock children</div>
      </App>
    );

    await waitFor(() => {
      const { getByText } = wrapper;

      expect(store.getState().reducerMock1).toEqual({ data1: 'mock-data-1' });
      expect(store.getState().reducerMock2).toEqual({ data2: 'mock-data-2' });
      expect(getByText('Mock children')).toBeInTheDocument();
    });
  });

  it('Render DOF App with process.env.REACT_APP_ALL_VIEWS = allViews', async () => {
    process.env.REACT_APP_ALL_VIEWS = 'allViews';

    const wrapper = render(
      <App
        urlConfigs={['mockFieldUrl', 'mockViewUrl']}
        customReducers={{ reducerMock1, reducerMock2 }}
      >
        <div>Mock children 01</div>
      </App>
    );

    await waitFor(() => {
      const { getByText } = wrapper;

      expect(getByText('Mock children 01')).toBeInTheDocument();
    });
  });

  it('Render DOF App with customValidators', async () => {
    class MockValidator implements Validator<string> {
      validate(value: string, context: ValidationContext): string | undefined {
        if (value === 'mock-error-value') {
          return 'Mock error';
        }

        return undefined;
      }
    }

    const customValidators: ValidatorsMap = {
      mockValidator: Promise.resolve(MockValidator)
    };

    validatorRegistry.registerValidator('mockValidator', MockValidator);

    const wrapper = render(
      <App
        urlConfigs={['mockFieldUrl', 'mockViewUrl']}
        customReducers={{ reducerMock1, reducerMock2 }}
        customValidators={customValidators}
      >
        <div>Mock children 02</div>
      </App>
    );

    await waitFor(() => {
      const { getByText } = wrapper;

      expect(getByText('Mock children 02')).toBeInTheDocument();
    });
  });

  it('Render DOF App without register viewAlls', async () => {
    viewRegistry.registerAvailableViews(undefined as any);

    const wrapper = render(
      <App
        urlConfigs={['mockFieldUrl', 'mockViewUrl']}
        customReducers={{ reducerMock1, reducerMock2 }}
      >
        <div>Mock children 03</div>
      </App>
    );

    await waitFor(() => {
      const { getByText } = wrapper;

      expect(viewRegistry.allViews).toBeUndefined();
      expect(getByText('Mock children 03')).toBeInTheDocument();
    });
  });

  it('Render DOF App with urlDataSource', async () => {
    window.fetch = jest.fn().mockResolvedValue({
      json: jest.fn().mockResolvedValue([
        {
          name: {
            url: 'mock url'
          }
        }
      ])
    } as any);

    const wrapper = render(
      <App
        urlDataSource={['mockDatasourceUrl']}
        urlConfigs={['mockFieldUrl', 'mockViewUrl']}
        customReducers={{ reducerMock1, reducerMock2 }}
      >
        <div>Mock children 04</div>
      </App>
    );

    await waitFor(() => {
      const { getByText } = wrapper;
      expect(getByText('Mock children 04')).toBeInTheDocument();
    });
  });

  it('Test function createHighOrderValidationFactoryFromValidatorDescriptor', async () => {
    class MockValidator implements Validator<string> {
      validate(value: string, context: ValidationContext): string | undefined {
        if (value === 'mock-error-value') {
          return 'Mock error';
        }

        return undefined;
      }
    }

    validatorRegistry.registerValidator('mockValidator', MockValidator);

    const validatorConstructor = await Promise.resolve(
      validatorRegistry.validatorsMap['mockValidator']
    );

    const context: ValidationContext = {
      labelOf(): string | undefined {
        return undefined;
      },
      valueOf(_anyField: string): any {
        return 100;
      }
    };

    const highOrderValidation =
      createHighOrderValidationFactoryFromValidatorDescriptor(
        validatorConstructor
      );

    let validationMsg = highOrderValidation({})(NoValidation)(
      'mock-error-value',
      context
    );
    expect(validationMsg).toEqual('Mock error');

    validationMsg = highOrderValidation({})(NoValidation)('', context);
    expect(validationMsg).toBeUndefined();
  });
});
