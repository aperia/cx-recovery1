import React, { useEffect, useMemo, useRef, useState } from 'react';
import ReactDOM from 'react-dom';
import { ViewLayoutProps } from '../../core';

export interface HtmlTemplateLayoutProps {
  elementRef: string;
}

interface HtmlTemplateLayoutOptions
  extends ViewLayoutProps<HtmlTemplateLayoutProps> {
  as?: React.ElementType;
  templateUrl: string;
}

const HtmlTemplateLayout: React.FunctionComponent<HtmlTemplateLayoutOptions> =
  ({ as: Wrapper = 'div', templateUrl, viewElements, viewName }) => {
    const isCancelledRef = useRef<boolean>(false);
    const wrapperElementRef = useRef<HTMLElement>();
    const [templateHtml, setTemplateHtml] =
      useState<string | undefined>(undefined);
    const [portals, setPortals] =
      useState<React.ReactPortal[] | undefined>(undefined);

    useEffect(() => {
      isCancelledRef.current = false;
      return () => {
        isCancelledRef.current = true;
      };
    });

    useEffect(() => {
      async function loadTemplate(url: string): Promise<void> {
        const response = await fetch(url);
        if (response.ok) {
          const html = await response.text();
          if (!isCancelledRef.current) {
            setTemplateHtml(html);
          }
        }
      }

      loadTemplate(templateUrl);
    }, [templateUrl]);

    //console.log("viewElements: ", viewElements);

    const renderResults = useMemo(
      () =>
        (viewElements || [])
          .map(viewElement => ({
            elementRef:
              viewElement.layoutProps && viewElement.layoutProps.elementRef
                ? viewElement.layoutProps.elementRef
                : viewElement.name,
            reactNode: viewElement.renderedNode
          }))
          .filter(renderResult => renderResult !== undefined),
      [viewElements]
    );

    //console.log("renderResults: ", renderResults);

    useEffect(() => {
      if (templateHtml !== undefined) {
        const portals = renderResults
          .map<React.ReactPortal[]>(renderResult => {
            const portals: React.ReactPortal[] = [];
            const matchedNodes = wrapperElementRef.current!.querySelectorAll(
              `[data-element-ref=${renderResult!.elementRef}]`
            );

            matchedNodes.forEach(node => {
              node.setAttribute('role', 'dof-field');
              return portals.push(
                ReactDOM.createPortal(renderResult!.reactNode, node)
              );
            });
            return portals;
          })
          .reduce((current, next) => [...current, ...next]);
        setPortals(portals);
      }
    }, [templateHtml, renderResults]);

    //Update 20200324 - {templateHtml} not found!
    return (
      <>
        {templateHtml ? (
          <>
            <Wrapper
              role="dof-view"
              role-name={viewName}
              dangerouslySetInnerHTML={{ __html: templateHtml }}
              ref={wrapperElementRef}
            />
            {portals}
          </>
        ) : (
          <h5>{templateHtml} not found!</h5>
        )}
      </>
    );
  };

export default HtmlTemplateLayout;
