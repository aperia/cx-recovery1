import { FormatterType, TemplateProps } from './type';

const storeGenerator = () => {
  const views: Record<string, React.FC<TemplateProps>> = {};

  const formatters: Record<string, FormatterType> = {};
  const registerTemplate = (
    name: string,
    component: React.FC<{ label: string; value?: string }>
  ) => {
    views[name] = component;
  };

  const registerFormatter = (name: string, formatter: (val: any) => string) => {
    formatters[name] = formatter;
  };
  return { views, registerTemplate, registerFormatter, formatters };
};

const store = storeGenerator();

export { store };
