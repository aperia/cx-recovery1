export function getValueFromObject<T>(
  obj: Record<string, unknown>,
  path?: string
): T | undefined {
  try {
    if (!path) return undefined;
    const pathArr = path.split('.');
    return pathArr.reduce(
      (currentObj, currentPath) => (currentObj as never)[currentPath],
      obj
    ) as T;
  } catch {
    return undefined;
  }
}
