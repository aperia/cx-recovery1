export * from './type';
import { store } from './store';
import ViewTemplate from './View';

const { registerTemplate, registerFormatter } = store;

export { registerTemplate, registerFormatter, ViewTemplate };
