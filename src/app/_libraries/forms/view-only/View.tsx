import React from 'react';
import { getValueFromObject } from './helper';
import { store } from './store';
import { FormatterType, FormViewProps } from './type';

const ViewTemplate: React.FC<FormViewProps> = props => {
  const { config, data } = props;

  return (
    <div className={config.className}>
      {config.fields.map(field => {
        const FieldComponent = store.views[field.component];
        if (!FieldComponent) {
          console.error(
            `It seem you missing register ${field.component} component`
          );
        }
        const format = getValueFromObject<FormatterType<string | undefined>>(
          store.formatters,
          field.format
        );
        const value = format
          ? format(getValueFromObject(data, field.dataField))
          : getValueFromObject<string>(data, field.dataField);
        return (
          FieldComponent &&
          !field.hidden && (
            <div className={field.className} key={field.name}>
              <FieldComponent
                label={field.label}
                value={value}
                {...field.props}
                data-testid={`${config.name}-${field.name}`}
              />
            </div>
          )
        );
      })}
    </div>
  );
};

export default ViewTemplate;
