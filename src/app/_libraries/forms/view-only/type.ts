export type TemplateProps = {
  label: string;
  value?: string;
}

export type FormatterType<T = string> = (val: T) => string;

export interface FieldConfig {
  name: string;
  className?: string;
  dataField: string;
  format?: string;
  label: string;
  component: string;
  hidden?: boolean;
  props?: Record<string, string | boolean | undefined>;
}

export interface Config {
  name: string;
  className?: string;
  fields: FieldConfig[];
}

export interface FormViewProps<T = {}> {
  config: Config;
  data: T;
}
