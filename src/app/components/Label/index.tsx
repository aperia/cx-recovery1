import { genAmtId } from 'app/_libraries/_dls/utils';
import React, { ReactNode } from 'react';

export interface LabelProps {
  label: string;
  value: string | ReactNode;
  id?: string;
  dataTestId?: string;
}

const Label: React.FC<LabelProps> = ({ label, value, id, dataTestId }) => {
  return (
    <div className="d-flex">
      <span
        className="color-grey"
        data-testid={genAmtId(dataTestId!, 'label', 'Label')}
      >
        {label}:
      </span>
      <span
        id={id}
        className="ml-4"
        data-testid={genAmtId(dataTestId!, 'value', 'Label')}
      >
        {value}
      </span>
    </div>
  );
};
export default Label;
