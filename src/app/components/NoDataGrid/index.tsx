import { I18N_TEXT } from 'app/constants';
import { Icon, IconProps } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import classNames from 'classnames';
import { isString } from 'lodash';
import React from 'react';

export interface NoDataGridProps {
  iconName?: IconProps['name'];
  text?: React.ReactNode;
  children?: React.ReactNode;
  dataTestId?: string;
  className?: string;
}

export const NoDataGrid: React.FC<NoDataGridProps> = ({
  iconName = 'file',
  text = I18N_TEXT.NO_DATA_TO_DISPLAY,
  children,
  dataTestId,
  className = ''
}) => {
  const { t } = useTranslation();
  return (
    <div
      className={classNames('block-no-data', className)}
      data-testid={genAmtId(dataTestId, 'no-data-grid', 'NoDataGrid')}
    >
      <Icon
        name={iconName}
        className="fs-80 color-light-l12"
        dataTestId={dataTestId}
      />
      {isString(text) ? (
        <p
          className="mt-16 color-grey"
          data-testid={genAmtId(dataTestId, 'no-data-grid_text', 'NoDataGrid')}
        >
          {t(text)}
        </p>
      ) : (
        text
      )}

      {children}
    </div>
  );
};
