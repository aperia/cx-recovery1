import React, { useEffect, useState } from 'react';

// components
import { Icon } from 'app/_libraries/_dls/components';
import { MaskAccNbrControl } from '../_dof_controls/controls/MaskAccNbrControl';
import If from 'pages/__commons/If';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { isEmpty } from 'lodash';
import classNames from 'classnames';

export interface MaskAccNbrProps {
  label: string;
  value: MagicKeyValue;
}

const MaskAccNbr: React.FC<MaskAccNbrProps> = ({ label, value }) => {
  const { t } = useTranslation();
  const [show, setShow] = useState(false);
  const [data, setData] = useState('');

  useEffect(() => {
    setData(value?.accountNumber);
  }, [value]);

  const handleSetData = (event: React.MouseEvent<HTMLElement>) => {
    const newData: string = data.includes('•')
      ? value?.accountNumberUnMask
      : value?.accountNumber;
    setData(newData);
    setShow(prevShow => !prevShow);
    event.stopPropagation();
  };

  return (
    <div className="d-flex">
      {label && <span className="color-grey">{t(label)}:</span>}
      <span
        className={classNames('form-group-static__sensitive', {
          'ml-4': label
        })}
      >
        <MaskAccNbrControl accNbr={data} />

        <If condition={!isEmpty(value)}>
          <Icon onClick={handleSetData} name={show ? 'eye-hide' : 'eye'} />
        </If>
      </span>
    </div>
  );
};

export default MaskAccNbr;
