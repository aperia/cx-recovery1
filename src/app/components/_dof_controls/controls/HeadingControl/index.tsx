import React from 'react';

import { CommonControlProps } from 'app/_libraries/_dof/core';

// components
import { Icon, Tooltip } from 'app/_libraries/_dls/components';

// utils
import cn from 'classnames';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
export interface HeadingControlProps extends CommonControlProps {
  as?: React.ElementType;
  name?: string;
  headingClassName?: string;
  tooltip?: string;
  sizeTooltip?: string;
  className?: string;
  dataTestId?: string;
  hasLabelStar?: boolean;
}

const HeadingControl: React.FC<HeadingControlProps> = ({
  input: { value },
  as: Component = 'h5',
  label,
  children,
  tooltip,
  sizeTooltip = '5x',
  id,
  dataTestId,
  headingClassName = '',
  hasLabelStar = false
}) => {
  const { t } = useTranslation();
  return (
    <div className="d-flex align-items-center">
      <Component
        id={id}
        className={cn(headingClassName, {
          'mr-8': tooltip,
          'color-grey': Component === 'h6'
        })}
        data-testid={genAmtId(dataTestId!, 'heading-control', 'HeadingControl')}
      >
        {value || t(label) || children}
        {hasLabelStar && <span className="color-red"> *</span>}
      </Component>
      {tooltip && (
        <Tooltip
          placement="top"
          element={t(tooltip)}
          triggerClassName="d-flex"
          dataTestId={dataTestId}
        >
          <Icon
            name="information"
            size={sizeTooltip as any}
            className="color-grey-l32"
            dataTestId={dataTestId}
          />
        </Tooltip>
      )}
    </div>
  );
};

export default HeadingControl;
