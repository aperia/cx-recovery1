import React from 'react';
import { Tooltip } from 'app/_libraries/_dls/components';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import { isEmpty } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface TagLineControlProps extends CommonControlProps {}

const TagLineControl: React.FC<TagLineControlProps> = ({
  input: { value: valueProp },
  label
}) => {
  const { t } = useTranslation();

  if (isEmpty(valueProp)) return null;

  return (
    <p className="color-grey-l16">
      <span className="text-capitalize">{t(label)}:</span>
      {valueProp?.map((item: RefDataValue) => (
        <Tooltip key={item?.value} element={item?.description}>
          <span className="py-2 px-8 bg-blue-l40 ml-4 color-grey-d20 fs-12 rounded cursor-pointer">
            {item?.value}
          </span>
        </Tooltip>
      ))}
    </p>
  );
};

export default TagLineControl;
