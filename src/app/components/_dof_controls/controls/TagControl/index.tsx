import React, { useEffect, useState } from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import { isEmpty } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import classnames from 'classnames';
import { checkCurrentAgencyCode } from 'pages/__commons/helpers';

export interface TagControlProps extends CommonControlProps {}

const TagControl: React.FC<TagControlProps> = ({
  input: { value: valueProp }
}) => {
  const { t } = useTranslation();
  const ASSIGN_CODE = '999';

  const [value, setValue] = useState<string>(valueProp);

  const color = value === ASSIGN_CODE ? 'purple' : 'orange';

  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  if (isEmpty(valueProp)) return null;

  return (
    <span className={classnames(`tag text-capitalize tag-${color}`)}>
      {t(checkCurrentAgencyCode(value))}
    </span>
  );
};

export default TagControl;
