import React, { Fragment, useEffect, useMemo, useState } from 'react';
import {
  DropdownBaseChangeEvent,
  MultiSelect,
  MultiSelectProps
} from 'app/_libraries/_dls/components';
import { DropdownBaseItemVariant } from 'app/_libraries/_dls/components/DropdownBase';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import isUndefined from 'lodash.isundefined';
import pickBy from 'lodash.pickby';
import isArray from 'lodash.isarray';

export interface MultiSelectControlProps
  extends CommonControlProps,
    Omit<MultiSelectProps, 'id' | 'label' | 'children'> {
  options?: { combineValues?: boolean; isError: boolean; max?: number };
  labelField?: string;
  dataTestId: string;
  uniqueField?: string;
  itemVariant?: DropdownBaseItemVariant;
}

const MultiSelectControl: React.FC<MultiSelectControlProps> = ({
  id,
  dataTestId,
  label,
  enable,
  data = [],
  input: { value: valueProp, onChange, onBlur },
  meta: { touched, error, invalid },
  textField = 'value',
  uniqueField,
  itemVariant,
  options,
  readOnly,
  required
}) => {
  const { t } = useTranslation();
  const [value, setValue] = useState<MagicKeyValue[]>(valueProp || []);
  const maxSize: number | undefined = options?.max;

  const errorTooltipProps = {
    opened: !options?.isError || invalid ? undefined : false
  };

  const handleOnChange = (event: DropdownBaseChangeEvent) => {
    setValue(event.target.value);
  };

  const handleOnBlur = (event: React.FocusEvent<Element>) => {
    if (!enable || readOnly) return;
    const truthyValue = isUndefined(value) ? null : value;
    onChange(truthyValue);
    onBlur(truthyValue);
  };

  const newData = useMemo(() => {
    if (!options?.combineValues || !uniqueField) {
      return data;
    }
    return data.map(item => ({
      ...(item as object),
      [textField]: `${item[uniqueField]} - ${item[textField]}`
    }));
  }, [data, options?.combineValues, textField, uniqueField]);

  useEffect(() => {
    if (isArray(valueProp)) {
      setValue(valueProp);
    }
  }, [setValue, valueProp]);

  return (
    <Fragment>
      <MultiSelect
        id={id}
        dataTestId={`${dataTestId}-multi-select-control`}
        label={t(label)}
        value={value}
        onChange={handleOnChange}
        onBlur={handleOnBlur}
        required={required}
        textField={uniqueField}
        popupBaseProps={{
          popupBaseClassName: 'inside-infobar'
        }}
        error={
          (enable && touched && invalid && required) || options?.isError
            ? {
                message: t(error),
                status: invalid || Boolean(options?.isError)
              }
            : undefined
        }
        errorTooltipProps={pickBy(
          errorTooltipProps,
          item => !isUndefined(item)
        )}
      >
        {newData.map((item, index) => {
          const isChecked =
            itemVariant === 'checkbox' &&
            !isUndefined(value.find(e => e.value === item.value));
          const isDisabled =
            !isChecked && !isUndefined(maxSize) && value.length >= maxSize;
          return (
            <MultiSelect.Item
              value={item}
              label={item[textField]}
              key={index}
              variant={itemVariant}
              checked={isChecked}
              disabled={isDisabled}
            />
          );
        })}
      </MultiSelect>
    </Fragment>
  );
};

export default MultiSelectControl;
