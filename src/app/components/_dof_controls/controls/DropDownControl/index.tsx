import React, { useEffect, useState } from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';

// components
import {
  DropdownBaseChangeEvent,
  DropdownListProps,
  DropdownList
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import {
  isEmpty,
  isEqual,
  isNull,
  isString,
  isUndefined,
  pickBy
} from 'app/helpers';
export interface DropDownControlProps
  extends CommonControlProps,
    Omit<DropdownListProps, 'id' | 'label' | 'children'> {
  options?: {
    combineValues?: boolean;
    isHaveNoneSelection?: boolean;
    isError?: boolean;
  };
  uniqueField?: string;
  className?: string;
}

export const DropDownControl: React.FC<DropDownControlProps> = props => {
  const {
    label,
    enable,
    data = [],
    input: { name, value: valueProp, onChange, onBlur },
    meta: { error, invalid, touched },
    readOnly,
    required,
    textField = 'fieldValue',
    options,
    uniqueField = '',
    dataTestId,
    className
  } = props;

  const [value, setValue] = useState<any>(valueProp);

  const { t } = useTranslation();

  const handleOnChange = (event: DropdownBaseChangeEvent) => {
    const { value: dropdownValue } = event.target;
    if (isNull(dropdownValue)) return setValue(null);
    setValue(dropdownValue);
  };

  const handleOnBlur = (event: React.FocusEvent<Element>) => {
    if (readOnly) return;
    onChange(value);
    onBlur(value);
  };

  useEffect(() => {
    if (isString(valueProp)) {
      const item =
        uniqueField && data?.find(val => val[uniqueField] === valueProp);
      setValue(item);
    } else {
      uniqueField
        ? setValue(
            data?.find(val => val[uniqueField] === valueProp[uniqueField])
          )
        : setValue(data?.find(val => isEqual(val, valueProp)));
    }
  }, [valueProp, data, uniqueField]);

  const isShowAsterisk = !readOnly;

  const { combineValues, isHaveNoneSelection } = options || {};
  const mappingValue = data?.find(val => isEqual(val, value));

  const errorTooltipProps = {
    opened: !options?.isError || invalid ? undefined : false
  };

  return (
    <DropdownList
      dataTestId={dataTestId}
      className={className}
      optional={isHaveNoneSelection}
      name={name}
      label={t(label)}
      readOnly={readOnly}
      textField={textField}
      textFieldRender={itemValue => {
        if (isEmpty(itemValue)) return '';
        if (!(combineValues && uniqueField)) {
          return itemValue[textField];
        }
        return `${itemValue[uniqueField]} - ${itemValue[textField]}`;
      }}
      onChange={handleOnChange}
      onBlur={handleOnBlur}
      value={uniqueField && data?.length ? mappingValue : value}
      required={isShowAsterisk && enable && required}
      popupBaseProps={{
        popupBaseClassName: 'inside-infobar'
      }}
      error={
        (isShowAsterisk && enable && required && touched && invalid) ||
        options?.isError
          ? {
              message: t(error),
              status: invalid || Boolean(options?.isError)
            }
          : undefined
      }
      errorTooltipProps={pickBy(errorTooltipProps, item => !isUndefined(item))}
    >
      {data?.map((item, index) => {
        const dropDownLbl =
          combineValues && uniqueField
            ? `${item[uniqueField]} - ${item[textField]}`
            : item[textField];
        return (
          <DropdownList.Item value={item} label={dropDownLbl} key={index} />
        );
      })}
    </DropdownList>
  );
};

export default DropDownControl;
