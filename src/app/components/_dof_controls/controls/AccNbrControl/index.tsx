import React, { Fragment, useEffect, useState } from 'react';

// components
import { Col, Row } from 'react-bootstrap';
import { Icon, TruncateText } from 'app/_libraries/_dls/components';
import { MaskAccNbrControl } from '../MaskAccNbrControl';
import If from 'pages/__commons/If';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// types
import { CommonControlProps } from 'app/_libraries/_dof/core';

// helpers
import { isEmpty } from 'lodash';
import classNames from 'classnames';

export interface AccNbrControlProps extends CommonControlProps {
  options: {
    direction: 'vertical' | 'horizontal';
    autoWidth?: boolean;
    labelGrey?: boolean;
  };
}

const AccNbrControl: React.FC<AccNbrControlProps> = ({
  label,
  input: { value },
  options
}) => {
  const { t } = useTranslation();
  const [show, setShow] = useState(false);
  const [data, setData] = useState('');

  useEffect(() => {
    setData(value?.accountNumber);
  }, [value]);

  const handleSetData = (event: React.MouseEvent<HTMLElement>) => {
    const newData: string = data.includes('•')
      ? value?.accountNumberUnMask
      : value?.accountNumber;
    setData(newData);
    setShow(prevShow => !prevShow);
    event.stopPropagation();
  };

  const renderLayoutLabel = () => {
    if (options?.autoWidth && label) {
      return <TruncateText>{t(label)}</TruncateText>;
    }

    return label ? (
      <span className="form-group-static__label">
        <TruncateText>{t(label)}</TruncateText>
      </span>
    ) : (
      ''
    );
  };

  const renderLayoutContent = () => {
    return (
      <span className="form-group-static__sensitive">
        <MaskAccNbrControl accNbr={data} />

        <If condition={!isEmpty(value)}>
          <Icon onClick={handleSetData} name={show ? 'eye-hide' : 'eye'} />
        </If>
      </span>
    );
  };

  const renderLayoutHorizontal = () => {
    if (options?.autoWidth) {
      return (
        <div className="d-flex">
          <span
            className={classNames('mr-4', options?.labelGrey && 'color-grey')}
          >
            {renderLayoutLabel()}:
          </span>
          <span className="form-group-static__text">
            {renderLayoutContent()}
          </span>
        </div>
      );
    }

    return (
      <Row className="mb-8 no-gutters">
        <Col xs={5}>{renderLayoutLabel()}</Col>
        <Col xs={7}>{renderLayoutContent()}</Col>
      </Row>
    );
  };

  const renderLayoutVertical = () => {
    return (
      <>
        {renderLayoutLabel()}
        {renderLayoutContent()}
      </>
    );
  };

  return (
    <Fragment>
      {options?.direction === 'horizontal'
        ? renderLayoutHorizontal()
        : renderLayoutVertical()}
    </Fragment>
  );
};

export default AccNbrControl;
