import React from 'react';
import { TruncateText } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_TEXT } from 'app/constants';

export interface LessMoreAction {
  id: string;
  value: string;
  truncateLines?: number;
}

const LessMoreAction: React.FC<LessMoreAction> = ({
  id,
  value,
  truncateLines = 2
}) => {
  const { t } = useTranslation();
  const [shown, setShown] = React.useState(false);

  return (
    <TruncateText
      id={`${id}__truncate-less-more`}
      resizable
      ellipsisLessText={t(I18N_TEXT.LESS)}
      ellipsisMoreText={t(I18N_TEXT.MORE)}
      className="text-break"
      title={value}
      lines={truncateLines}
      shown={shown}
      onShown={() => setShown(!shown)}
    >
      {value}
    </TruncateText>
  );
};

export default LessMoreAction;
