import React, { useEffect, useState } from 'react';
import { TruncateText, PopoverProps } from 'app/_libraries/_dls/components';

import { CommonControlProps } from 'app/_libraries/_dof/core';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { Col, Row } from 'react-bootstrap';

// helpers
import { TextViewControlFormat } from 'app/constants/enums';
import { formatContactPhone } from 'app/helpers/formatContactPhone';
import { formatCurrency, get, isEmpty } from 'app/helpers';
import LessMoreAction from './LessMoreActions';
import TextShowInformation from './TextShowInformation';
export interface TextViewControlProps extends CommonControlProps {
  options: {
    direction: 'vertical' | 'horizontal';
    autoWidth?: boolean;
    valueBold?: boolean;
    format: TextViewControlFormat;
    popover?: {
      valueKey?: string;
      descriptionKey?: string;
      options?: PopoverProps;
    };
    labelInfo?: string;
  };
}

const TextViewControl: React.FC<TextViewControlProps> = ({
  id,
  input: { value: valueProp },
  label,
  options
}) => {
  const { t } = useTranslation();
  const { format = TextViewControlFormat.Text } = options || {};
  const [value, setValue] = useState<string>(valueProp);

  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  const formatGeneration = (val: any) => {
    switch (format) {
      case TextViewControlFormat.Text:
        return ((val ? val : '') + '')?.trim();
      case TextViewControlFormat.PhoneNumber:
        return formatContactPhone(val);
      case TextViewControlFormat.Amount:
        return formatCurrency(val);
      case TextViewControlFormat.LessMoreActions:
        return <LessMoreAction id={id} value={val} />;
      case TextViewControlFormat.ShowInformation: {
        const _value = get(val, options?.popover?.valueKey || '', '');
        const description = get(val, options?.popover?.descriptionKey || '');

        if (_value.length === 0) return '';

        return (
          <TextShowInformation
            id={id}
            value={_value}
            description={description}
            options={options?.popover?.options}
          />
        );
      }
      default:
        return val;
    }
  };

  const renderLayoutLabel = () => {
    if (options?.labelInfo) {
      const description = options?.labelInfo || '';

      if (options?.labelInfo && options?.labelInfo?.length === 0)
        return <TruncateText>{t(label)}</TruncateText>;

      return (
        <TextShowInformation
          id={id}
          value={t(label) || ''}
          description={t(description)}
          options={options?.popover?.options}
        />
      );
    }

    return <TruncateText>{t(label)}</TruncateText>;
  };

  const textValue = !isEmpty(value) ? formatGeneration(value) : '';

  const renderLayoutHorizontal = () => {
    if (options?.autoWidth) {
      return (
        <div className="d-flex align-items-center">
          <span className="color-grey mr-6">{renderLayoutLabel()}:</span>
          {/* Fix bug truncate text - TextShowInformation. Do not wrap tag "div, span.." for {textValue} */}
          {textValue}
        </div>
      );
    }

    if (options?.valueBold) {
      return (
        <div className="d-flex align-items-center">
          <span className="mr-6">{renderLayoutLabel()}:</span>
          <span className="fw-500">{textValue}</span>
        </div>
      );
    }

    return (
      <Row className="mb-8 no-gutters">
        <Col xs={5}>{renderLayoutLabel()}</Col>
        <Col xs={7}>
          <b>{textValue}</b>
        </Col>
      </Row>
    );
  };

  const renderLayoutVertical = () => {
    return (
      <>
        <span className="form-group-static__label d-flex align-items-center">
          {renderLayoutLabel()}
        </span>

        <div className={'content-empty d-flex align-items-center pre-line'}>
          {textValue}
        </div>
      </>
    );
  };

  return options?.direction === 'horizontal'
    ? renderLayoutHorizontal()
    : renderLayoutVertical();
};

export default TextViewControl;
