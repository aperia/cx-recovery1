import React from 'react';
import {
  Button,
  Icon,
  Popover,
  PopoverProps,
  Tooltip
} from 'app/_libraries/_dls/components';
export interface TextShowInformationProps extends PopoverProps {
  id: string;
  value: string;
  description: string | React.ReactNode;
  options?: PopoverProps;
  onlyTooltip?: boolean;
}

const TextShowInformation: React.FC<TextShowInformationProps> = ({
  id,
  value,
  description,
  options,
  onlyTooltip = false
}) => {
  const renderElementDescription = (val: string | React.ReactNode) => {
    if (typeof val === 'string') {
      const numSplit = val.split('\n');

      if (numSplit.length >= 2) {
        return (
          <>
            {numSplit.map((item: string, index: number) => (
              <p key={index.toString()}>{item}</p>
            ))}
          </>
        );
      }
    }

    return val;
  };
  if (onlyTooltip) {
    return (
      <div className="d-flex align-item-center">
        <span title={value} className="text-truncate mr-8">
          {value}
        </span>

        <Tooltip
          element={renderElementDescription(description)}
          placement="top"
          triggerClassName="d-flex align-items-center"
        >
          <Icon name="information" size="4x" className="color-grey-l16" />
        </Tooltip>
      </div>
    );
  }

  return (
    <>
      <span title={value} className="text-truncate mr-6">
        {value}
      </span>
      {value.length > 0 && (
        <Popover
          key={id.toString()}
          element={renderElementDescription(description)}
          size="auto"
          {...options}
        >
          <Button
            size="sm"
            variant="icon-secondary"
            className="d-flex justify-content-center align-items-center"
            onClick={event => event.stopPropagation()}
          >
            <Icon name="information" size="4x" className="color-grey-l16" />
          </Button>
        </Popover>
      )}
    </>
  );
};

export default TextShowInformation;
