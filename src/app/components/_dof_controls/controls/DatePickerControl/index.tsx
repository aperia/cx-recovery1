import React from 'react';

// components
import {
  DatePicker,
  DatePickerChangeEvent,
  DatePickerProps
} from 'app/_libraries/_dls/components';

import { CommonControlProps } from 'app/_libraries/_dof/core';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Helper
import { isUndefined } from 'app/helpers';

export interface DatePickerControlProps
  extends CommonControlProps,
    Omit<DatePickerProps, 'id' | 'label'> {
  options?: {
    isError?: boolean;
    min?: Date;
  };
}

const DatePickerControl: React.FC<DatePickerControlProps> = ({
  input: { name, value: valueProp, onChange, onBlur },
  meta: { error, valid, invalid, touched, form },
  id,
  dataTestId,
  label,
  enable,
  readOnly,
  options,
  required,
  ...props
}) => {
  const { t } = useTranslation();

  const handleOnChange = (event: DatePickerChangeEvent) => {
    const currentValue = event.target.value;
    onChange(isUndefined(currentValue) ? null : currentValue);
  };

  const handleOnBlur = (event: React.FocusEvent<Element>) => {
    if (readOnly) return;
    const currentValue = (event.target as MagicKeyValue).value;
    onBlur(isUndefined(currentValue) ? null : currentValue);
  };

  return (
    <DatePicker
      id={id}
      dataTestId={dataTestId}
      name={name}
      value={valueProp}
      onChange={handleOnChange}
      onBlur={handleOnBlur}
      label={t(label)}
      disabled={!enable}
      readOnly={readOnly}
      required={enable && !readOnly && required}
      error={
        (required && touched && invalid) || options?.isError
          ? {
              message: t(error),
              status: invalid || options?.isError
            }
          : undefined
      }
      popupBaseProps={{
        popupBaseClassName: 'inside-infobar'
      }}
      {...props}
    />
  );
};

export default DatePickerControl;
