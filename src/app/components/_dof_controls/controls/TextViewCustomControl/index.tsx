import React, { useEffect, useState } from 'react';
import { TruncateText } from 'app/_libraries/_dls/components';

import { CommonControlProps } from 'app/_libraries/_dof/core';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { Col, Row } from 'react-bootstrap';

// helpers
import { TextViewControlFormat } from 'app/constants/enums';
import { isEmpty } from 'lodash';
import { formatContactPhone } from 'app/helpers/formatContactPhone';
import { formatCurrency } from 'app/helpers';
import LessMoreAction from './LessMoreAction';

export interface TextViewCustomControlProps extends CommonControlProps {
  options: {
    direction: 'vertical' | 'horizontal';
    format: TextViewControlFormat;
  };
}

const TextViewCustomControl: React.FC<TextViewCustomControlProps> = ({
  id,
  input: { value: valueProp },
  options
}) => {
  const { t } = useTranslation();
  const { format = TextViewControlFormat.Text } = options || {};
  const [value, setValue] = useState<any>(valueProp);

  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  const formatGeneration = (val: any) => {
    switch (format) {
      case TextViewControlFormat.Text:
        return ((val ? val : '') + '')?.trim();
      case TextViewControlFormat.PhoneNumber:
        return formatContactPhone(val);
      case TextViewControlFormat.Amount:
        return formatCurrency(val);
      case TextViewControlFormat.LessMoreActions:
        return <LessMoreAction id={id} value={val} />;
      default:
        return val;
    }
  };
  const textValue = !isEmpty(value?.value)
    ? formatGeneration(value?.value)
    : '';

  return options?.direction === 'horizontal' ? (
    <Row className="mb-8 no-gutters">
      <Col xs={5}>
        <TruncateText>{t(value?.label)}</TruncateText>
      </Col>
      <Col xs={7}>
        <b className={'form-group-static__text'}>{textValue}</b>
      </Col>
    </Row>
  ) : (
    <>
      <span className="form-group-static__label">
        <TruncateText>{t(value?.label)}</TruncateText>
      </span>

      <div className={'form-group-static__text pre-line'}>{textValue}</div>
    </>
  );
};

export default TextViewCustomControl;
