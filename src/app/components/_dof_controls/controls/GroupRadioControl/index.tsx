import React, { useMemo, useCallback, useState, useEffect } from 'react';

// types
import { CommonControlProps } from 'app/_libraries/_dof/core';
import { useTranslation } from 'app/_libraries/_dls/hooks';
// components
import { Radio } from 'app/_libraries/_dls/components';

// helpers
import classnames from 'classnames';
import { isArrayHasValue } from 'app/helpers';
import { genAmtId } from 'app/_libraries/_dls/utils';

export interface IRadio {
  description: string;
  className: string;
  value: string;
  readOnly: boolean;
  disabled: boolean;
}

export interface GroupRadioControlProps extends CommonControlProps {
  radios: IRadio[];
  options?: {
    className?: string;
  };
  dataTestId?: string;
}

const GroupRadioControl: React.FC<GroupRadioControlProps> = ({
  input: { name, value: valueProp, onChange },
  meta: { error, valid, invalid, touched },
  options,
  radios,
  id,
  label,
  tooltip,
  readOnly,
  required,
  enable,
  dataTestId,
  ...props
}) => {
  const { t } = useTranslation();
  const [value, setValue] = useState(valueProp);

  const handleChange = useCallback(
    data => {
      setValue(data);
    },
    [setValue]
  );

  const handleBlur = useCallback(
    data => {
      onChange(data);
    },
    [onChange]
  );

  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  const renderRadioInput = useMemo(() => {
    if (!isArrayHasValue(radios)) return null;
    return radios.map((item, index) => {
      const checked = value === item.value ? true : false;
      return (
        <Radio
          key={`${name}-${item.value}`}
          className={classnames(item.className)}
          dataTestId={genAmtId(
            `${item.value.toString()}_${dataTestId || ''}`,
            'group-radio-control_item',
            'GroupRadioControl'
          )}
        >
          <Radio.Input
            id={`${name}-${item.value}`}
            name={name}
            onChange={() => handleChange(item.value)}
            onBlur={() => handleBlur(item.value)}
            checked={checked}
            readOnly={item.readOnly}
            disabled={item.disabled}
          />
          <Radio.Label>{t(item.description)}</Radio.Label>
        </Radio>
      );
    });
  }, [radios, name, dataTestId, t, handleChange, handleBlur, value]);

  return (
    <div id={id} className={classnames(options?.className)}>
      {label ? (
        <div className="d-flex">
          <h6
            className="color-grey"
            data-testid={genAmtId(
              dataTestId!,
              'group-radio-control_label',
              'GroupRadioControl'
            )}
          >
            {t(label)}
          </h6>
          {required && (
            <span className="asterisk color-danger ml-4 mt-n4">*</span>
          )}
        </div>
      ) : null}
      <div
        className="custom-group-radio"
        data-testid={genAmtId(
          dataTestId!,
          'group-radio-control_group',
          'GroupRadioControl'
        )}
      >
        {renderRadioInput}
      </div>
    </div>
  );
};

export default GroupRadioControl;
