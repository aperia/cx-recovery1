import React, { useEffect, useState } from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import classNames from 'classnames';

export interface TextValueControlProps extends CommonControlProps {
  options?: { isBold?: boolean; isView?: boolean };
}

const TextValueControl: React.FC<TextValueControlProps> = ({
  input: { value: valueProp },
  label,
  options
}) => {
  const { t } = useTranslation();

  const [value, setValue] = useState<string>(valueProp);

  const checkNoneValue = () => (value.includes('None') ? '' : value);

  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  return (
    <>
      {options?.isView ? (
        <p className="row align-items-center">
          {label && (
            <span className="text-capitalize color-grey-d20 fw-500 w-30">
              {t(label)}
            </span>
          )}
          <span className="color-grey-d20 w-70 form-group-static__text">
            {checkNoneValue()}
          </span>
        </p>
      ) : (
        <p className={classNames('color-grey-l16')}>
          {label && <span className="text-capitalize">{t(label)}:</span>}
          <span
            className={classNames(
              'color-grey-d20',
              label && 'ml-4',
              options?.isBold && 'fw-500'
            )}
          >
            {checkNoneValue()}
          </span>
        </p>
      )}
    </>
  );
};

export default TextValueControl;
