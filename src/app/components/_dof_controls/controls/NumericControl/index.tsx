import React, { useEffect, useMemo, useState } from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';

// components
import {
  NumericProps,
  NumericTextBox,
  OnChangeNumericEvent
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

import classnames from 'classnames';

import { isNumber, isString } from 'app/helpers';

export interface NumericControlProps
  extends CommonControlProps,
    Omit<NumericProps, 'id' | 'label'> {
  dataTestId?: string;

  options?: {
    disabled?: boolean;
    min?: number;
    max?: number;
  };
}

const NumericControl: React.FC<NumericControlProps> = ({
  input: { value: valueProp, onChange, onBlur, name },
  meta: { error, valid, invalid, touched },
  id,
  dataTestId,
  label,
  tooltip,
  enable,
  required,
  format,
  maxLength,
  readOnly,
  autoFill,
  options,
  className,
  ...props
}) => {
  const { t } = useTranslation();

  const [value, setValue] = useState<React.ReactText>(valueProp);

  const handleOnChange = (event: OnChangeNumericEvent) => {
    const { value: valueChange } = event.target;
    setValue(valueChange);
  };

  const handleOnBlur = (event: React.FocusEvent<HTMLInputElement>) => {
    if (!enable || readOnly) return;
    const { value: valueBlur } = event.target;
    onChange(valueBlur);
    onBlur(valueBlur);
  };

  useEffect(() => {
    setValue(valueProp);
  }, [valueProp]);

  const renderMessageError = useMemo(() => {
    if (isString(error) || isNumber(error)) {
      return t(error);
    }
    return t(error?.messageCustom, { count: error?.count });
  }, [error, t]);

  return (
    <div className={classnames(className)}>
      <NumericTextBox
        id={id}
        dataTestId={dataTestId}
        label={t(label)}
        value={value}
        format={format}
        maxLength={maxLength}
        required={required}
        readOnly={readOnly}
        autoFill={autoFill}
        error={
          enable && required && touched && invalid
            ? {
                message: t(renderMessageError),
                status: invalid
              }
            : undefined
        }
        onChange={handleOnChange}
        onBlur={handleOnBlur}
        {...options}
      />
    </div>
  );
};

export default NumericControl;
