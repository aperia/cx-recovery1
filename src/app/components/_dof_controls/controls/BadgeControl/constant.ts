import { BadgeProps } from 'app/_libraries/_dls/components';

const COMMON_STATUS = {
  Blank: 'green',
  active: 'green',
  purged: 'orange',
  recall: 'green',
  assign: 'purple'
} as Record<string, BadgeProps['color']>;

export { COMMON_STATUS };
