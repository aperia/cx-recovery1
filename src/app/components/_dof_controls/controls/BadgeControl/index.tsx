import { Badge, TruncateText } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import React from 'react';
import { COMMON_STATUS } from './constant';

export interface BadgeControlProps extends CommonControlProps {
  options: {
    isTransFormText?: boolean;
  };
}

const BadgeControl: React.FC<BadgeControlProps> = ({
  label,
  options,
  input: { value }
}) => {
  const { t } = useTranslation();
  const renderValue = () => {
    const color =
      value?.trim().length === 0
        ? COMMON_STATUS['Blank']
        : COMMON_STATUS[value];

    return (
      value && (
        <Badge color={color} textTransformNone={options?.isTransFormText}>
          {value}
        </Badge>
      )
    );
  };

  return (
    <>
      {label ? (
        <span className="form-group-static__label">
          <TruncateText>{t(label)}</TruncateText>
        </span>
      ) : (
        ''
      )}
      {renderValue()}
    </>
  );
};

export default BadgeControl;
