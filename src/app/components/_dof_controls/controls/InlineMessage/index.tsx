import React, { Fragment } from 'react';
import { CommonControlProps } from 'app/_libraries/_dof/core';
import { InlineMessage as InlineMessageDLS } from 'app/_libraries/_dls/components';
import { Variant } from 'react-bootstrap/esm/types';
import { isArrayHasValue } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface InlineMessageProps extends CommonControlProps {
  props: { className: string };
  variant?: Variant;
  dataTestId?: string;
}

const BREAK_SYMBOL = '\n';

const InlineMessage: React.FunctionComponent<InlineMessageProps> = ({
  input: { value },
  props,
  variant = 'danger',
  dataTestId
}) => {
  const { t } = useTranslation();

  if (!isArrayHasValue(value)) return null;

  const messages =
    typeof value === 'string' ? value?.split(BREAK_SYMBOL) : value;

  return (
    <Fragment>
      {messages.map((message: string, index: number) => {
        return (
          <InlineMessageDLS
            id={`inline-message-${index}`}
            dataTestId={dataTestId}
            key={index}
            variant={variant}
            className={props?.className}
          >
            {t(message)}
          </InlineMessageDLS>
        );
      })}
    </Fragment>
  );
};

export default InlineMessage;
