import { DataApiRequest } from 'app/_libraries/_dof/core';

const requestBuilder = (request: Partial<DataApiRequest>): DataApiRequest => {
  const DEFAULT_HEADERS = [
    {
      Accept: 'application/json',
      'Content-Type': 'application/json'
    }
  ];
  const { method, headers, requestData } = request;
  return {
    method: method || 'POST',
    headers: headers || DEFAULT_HEADERS,
    requestData: requestData
  };
};

function getMonitorCriteriaCalendarCode(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'monitorCriteriaCalendarCode'
    },
    method: 'GET'
  });
}

function getMonitorCriteriaNoteCodes(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'monitorCriteriaNoteCodes'
    },
    method: 'GET'
  });
}

function getMonitorCriteriaIntervalUnit(): DataApiRequest {
  return requestBuilder({
    requestData: {
      componentID: 'monitorCriteriaIntervalUnit'
    },
    method: 'GET'
  });
}

export const refData = {
  getMonitorCriteriaCalendarCode,
  getMonitorCriteriaNoteCodes,
  getMonitorCriteriaIntervalUnit
};
