import React, {
  useState,
  useRef,
  ReactNode,
  useCallback,
  useEffect
} from 'react';
import isFunction from 'lodash.isfunction';
import HeaderControl, { HeaderControlProps } from './HeaderControl';
import cn from 'classnames';

export interface SectionControlProps {
  title: string | ReactNode;
  subTitle?: string | ReactNode;
  classNames?: string;
  classNamesView?: string;
  onToggle?: (expand: boolean, sectionKey: string) => void;
  isExpand?: boolean;
  sectionKey: string;
  isEdit?: boolean;
  isAdd?: HeaderControlProps['isAdd'];
  typeEdit?: string;
  textTooltipEdit?: string;
  dataTestId?: string;
  isTitleNormal?: boolean;

  // refactoring
  dropdownButton?: HeaderControlProps['dropdownButton'];
}

const SectionControl: React.FC<SectionControlProps> = ({
  title: titleProp,
  subTitle,
  classNames,
  classNamesView,
  isExpand: isExpandProps = false,
  onToggle,
  sectionKey,
  isEdit,
  isAdd,
  typeEdit,
  textTooltipEdit,
  dataTestId,
  isTitleNormal,

  // refactoring
  dropdownButton,
  ...props
}) => {
  const sectionRef = useRef<HTMLDivElement | null>(null);
  const [isExpand, setExpand] = useState<boolean>(false);

  useEffect(() => {
    setExpand(isExpandProps);
  }, [isExpandProps]);

  const handleExpandCollapse = useCallback(() => {
    if (isFunction(onToggle)) {
      onToggle(isExpand, sectionKey);
    } else {
      setExpand(!isExpand);
    }
  }, [isExpand, onToggle, sectionKey]);

  return (
    <section className={classNames}>
      <div className="d-flex justify-content-between align-items-center">
        <HeaderControl
          title={titleProp}
          onExpandCollapse={handleExpandCollapse}
          isExpand={isExpand}
          isEdit={isEdit}
          isAdd={isAdd}
          typeEdit={typeEdit}
          textTooltipEdit={textTooltipEdit}
          dataTestId={dataTestId}
          isTitleNormal={isTitleNormal}
          // refactoring
          dropdownButton={dropdownButton}
        />
      </div>
      <div
        className={cn({
          'd-none': !isExpand
        })}
        data-toggle={isExpand}
        ref={sectionRef}
      >
        <div className={classNamesView}>{props.children}</div>
      </div>
    </section>
  );
};

export default SectionControl;
