import React, { ReactNode, useMemo } from 'react';

// components
import {
  Icon,
  Button,
  Tooltip,
  DropdownButton,
  DropdownButtonProps,
  TooltipProps,
  IconProps,
  ButtonProps
} from 'app/_libraries/_dls/components';

// helpers
import { I18N_TEXT } from 'app/constants/i18nText';
import classnames from 'classnames';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { genAmtId } from 'app/_libraries/_dls/utils';
import classNames from 'classnames';

export interface HeaderControlProps {
  onExpandCollapse?: () => void;
  title: string | ReactNode;
  isExpand?: boolean;
  isEdit?: boolean;
  isAdd?: {
    handleAdd?: () => void;
    buttonLabel?: string;
    enableToolTip?: boolean;
    disableButton?: boolean;
    enableSectionButton?: boolean;
    className?: string;
  };
  isTitleNormal?: boolean;
  typeEdit?: string;
  textTooltipEdit?: string;
  textTooltipAdd?: string;
  dataTestId?: string;

  // this prop below to refactoring this component
  // we need more flexible for right component
  // right hand side can become any component
  dropdownButton?: {
    placementDropdown?: TooltipProps['placement'];
    tooltipText?: string;
    tooltipVariant?: TooltipProps['variant'];
    tooltipTriggerClassName?: string;
    items?: Array<{ text?: string; value?: string }>;
    onSelect: DropdownButtonProps['onSelect'];
    buttonIcon?: React.ReactElement<IconProps>;
    buttonIconVariant?: ButtonProps['variant'];
  };
}

const HeaderControl: React.FC<HeaderControlProps> = ({
  onExpandCollapse,
  title,
  isExpand,
  isEdit,
  isAdd,
  isTitleNormal,
  typeEdit,
  textTooltipEdit,
  textTooltipAdd,
  dataTestId,

  // refactoring
  dropdownButton
}) => {
  const { t } = useTranslation();

  const handleEdit = () => {};

  const dropdownButtonChildren = useMemo(() => {
    if (!Array.isArray(dropdownButton?.items)) return [];

    const result = dropdownButton?.items.map(item => {
      return (
        <DropdownButton.Item
          dataTestId={dataTestId}
          key={item.value}
          label={t(item.text)}
          value={item.value}
        />
      );
    });

    return result!;
  }, [dropdownButton?.items, t, dataTestId]);

  const headerTitle = useMemo(() => {
    if (!isAdd) {
      if (isTitleNormal)
        return (
          <p
            className="fw-500 ml-8"
            data-testid={genAmtId(
              dataTestId!,
              'header-control_text',
              'HeaderControl'
            )}
          >
            {title}
          </p>
        );

      return (
        <h5
          className="ml-4"
          data-testid={genAmtId(
            dataTestId!,
            'header-control_text',
            'HeaderControl'
          )}
        >
          {title}
        </h5>
      );
    }

    return (
      <p
        className={classNames(
          'fw-500',
          !isAdd.enableSectionButton && 'ml-8',
          isAdd.className
        )}
        data-testid={genAmtId(
          dataTestId!,
          'header-control_text',
          'HeaderControl'
        )}
      >
        {title}
      </p>
    );
  }, [dataTestId, isAdd, isTitleNormal, title]);

  return (
    <React.Fragment>
      <div className="d-flex align-items-center w-100">
        {!isAdd?.enableSectionButton && (
          <Tooltip
            triggerClassName="ml-n4"
            placement="top"
            variant={'primary'}
            element={isExpand ? t(I18N_TEXT.COLLAPSE) : t(I18N_TEXT.EXPAND)}
            dataTestId={dataTestId}
          >
            <Button
              size="sm"
              variant="icon-secondary"
              onClick={onExpandCollapse}
              dataTestId={genAmtId(
                dataTestId!,
                'header-control_expand-collapse-btn',
                'HeaderControl'
              )}
            >
              <Icon name={isExpand ? 'minus' : 'plus'} />
            </Button>
          </Tooltip>
        )}
        {headerTitle}

        {isEdit && (
          <Tooltip
            triggerClassName="ml-auto mr-n4"
            placement="top"
            variant={'primary'}
            element={t(textTooltipEdit)}
            dataTestId={dataTestId}
          >
            <Button
              variant="icon-secondary"
              onClick={handleEdit}
              dataTestId={genAmtId(
                dataTestId!,
                'header-control_edit-btn',
                'HeaderControl'
              )}
            >
              <Icon name="edit" />
            </Button>
          </Tooltip>
        )}

        {isAdd &&
          (isAdd.enableToolTip ? (
            <Tooltip
              triggerClassName="ml-auto mr-n4"
              placement="top"
              variant={'primary'}
              element={t(textTooltipAdd)}
              dataTestId={dataTestId}
            >
              <Button
                className="mr-n8"
                disabled={isAdd.disableButton}
                size="sm"
                variant="outline-primary"
                onClick={isAdd.handleAdd}
                dataTestId={genAmtId(
                  dataTestId!,
                  'header-control_add-btn',
                  'HeaderControl'
                )}
              >
                {isAdd.buttonLabel}
              </Button>
            </Tooltip>
          ) : (
            <div className="ml-auto">
              <Button
                disabled={isAdd.disableButton}
                className="mr-n8"
                size="sm"
                variant="outline-primary"
                onClick={isAdd.handleAdd}
                dataTestId={genAmtId(
                  dataTestId!,
                  'header-control_add-btn',
                  'HeaderControl'
                )}
              >
                {isAdd.buttonLabel}
              </Button>
            </div>
          ))}

        {dropdownButton && (
          <Tooltip
            element={t(dropdownButton.tooltipText)}
            variant={dropdownButton.tooltipVariant || 'primary'}
            triggerClassName={classnames(
              dropdownButton.tooltipTriggerClassName,
              'ml-auto mr-n4'
            )}
            dataTestId={dataTestId}
          >
            <DropdownButton
              popupBaseProps={{
                popupBaseClassName: 'inside-infobar',
                placement: dropdownButton?.placementDropdown
              }}
              buttonProps={{
                children: dropdownButton.buttonIcon || (
                  <Icon name="more-vertical" />
                ),
                variant: dropdownButton.buttonIconVariant || 'icon-secondary'
              }}
              onSelect={dropdownButton.onSelect}
              dataTestId={genAmtId(
                dataTestId!,
                'header-control_more-actions',
                'HeaderControl'
              )}
            >
              {dropdownButtonChildren}
            </DropdownButton>
          </Tooltip>
        )}
      </div>
    </React.Fragment>
  );
};

export default HeaderControl;
