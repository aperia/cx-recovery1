import React, { FC, useEffect, useMemo, useState } from 'react';
import { useField } from 'formik';
import dateTime from 'date-and-time';
import classNames from 'classnames';

import {
  DatePicker,
  DatePickerChangeEvent,
  DatePickerProps,
  Tooltip
} from 'app/_libraries/_dls/components';
import { isFunction, isUndefined, pickBy } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { getNextDateWith } from 'pages/AccountDetail/FinacialInfomation/helpers';

export interface DatePickerControlProps extends DatePickerProps {
  name: string;
  forceError?: boolean;
  forceUpdateValue?: boolean;
  minDaysFromToday?: number;
  maxDaysFromToday?: number;
  pastDateDisabledText?: string;
  futureDateDisabledText?: string;
  minDateDisabledText?: string;
  maxDateDisabledText?: string;
  sameOrBeforeDisabledText?: string;
  forceMessageError?: string;
  onValueChange?: (event: DatePickerChangeEvent) => void;
}

export const DatePickerControl: FC<DatePickerControlProps> = ({
  className,
  name,
  forceError,
  forceUpdateValue,
  minDaysFromToday,
  maxDaysFromToday,
  pastDateDisabledText,
  futureDateDisabledText,
  minDateDisabledText,
  maxDateDisabledText,
  sameOrBeforeDisabledText,
  forceMessageError,
  onValueChange,

  ...props
}) => {
  const { t } = useTranslation();
  const [field, { error, touched }] = useField(name);
  const [value, setValue] = useState(field.value);

  const openTooltip = useMemo(() => {
    if (!forceError) {
      return undefined;
    }
    setTimeout(() => {
      return forceError && forceMessageError;
    }, 0);
  }, [forceError, forceMessageError]);

  const errorTooltipProps = {
    opened: openTooltip
  };
  const today = (function () {
    const toDay = new Date();
    toDay.setHours(0, 0, 0, 0);
    return toDay;
  })();

  const minDate = (function () {
    if (minDaysFromToday === undefined) return undefined;
    const returnDate = dateTime.addDays(new Date(), minDaysFromToday);
    returnDate.setHours(0, 0, 0, 0);
    return returnDate;
  })();

  const maxDate = (function () {
    if (maxDaysFromToday === undefined) return undefined;
    const returnDate = dateTime.addDays(new Date(), maxDaysFromToday - 1);
    returnDate.setHours(0, 0, 0, 0);
    return returnDate;
  })();

  const handleDateTooltip = (date: Date) => {
    if (
      sameOrBeforeDisabledText &&
      minDaysFromToday &&
      minDaysFromToday < 0 &&
      date < getNextDateWith(new Date(), minDaysFromToday - 1)
    ) {
      return <Tooltip element={sameOrBeforeDisabledText} />;
    }
    if (pastDateDisabledText && date < today) {
      return <Tooltip element={pastDateDisabledText} />;
    }

    if (futureDateDisabledText && date > today) {
      return <Tooltip element={futureDateDisabledText} />;
    }

    if (minDaysFromToday && minDate && date < minDate && minDateDisabledText) {
      return <Tooltip element={minDateDisabledText} />;
    }
    if (maxDate && date > maxDate && maxDateDisabledText) {
      return <Tooltip element={maxDateDisabledText} />;
    }
    return undefined;
  };

  const handleChange: (e: DatePickerChangeEvent) => void = e => {
    const { value: newValue } = e.target;
    setValue(newValue);
    if (isFunction(onValueChange)) {
      const event = {
        ...e,
        target: {
          ...e.target,
          name
        }
      } as DatePickerChangeEvent;
      onValueChange(event);
    }
  };

  const handleOnBlur: (event: React.FocusEvent<Element>) => void = e => {
    // because event dont have target.name
    // so, manually add name to formik could be control
    const handler: ProxyHandler<React.FocusEvent<Element>> = {
      get: function (target, prop) {
        if (prop === 'target') {
          return {
            ...e.target,
            name
          };
        }
        return Reflect.get(target, prop);
      }
    };
    const newEvent = new Proxy(e, handler);
    field.onBlur(newEvent);
    field.onChange(newEvent);
  };

  useEffect(() => {
    if (!touched) {
      setValue(field.value ? new Date(field.value) : undefined);
    }
  }, [touched, field.value]);

  // for form Item Payment in Batch modal
  useEffect(() => {
    if (forceUpdateValue) {
      setValue(field.value ? new Date(field.value) : undefined);
    }
  }, [field.value, forceUpdateValue]);

  return (
    <div className={classNames(className)}>
      <DatePicker
        {...props}
        label={t(props.label)}
        error={
          (!props.disabled &&
            !props.readOnly &&
            props.required &&
            touched &&
            error) ||
          forceError
            ? {
                message: t(
                  forceError && forceMessageError ? forceMessageError : error
                ),
                status: !!error || forceError
              }
            : undefined
        }
        required={!props.readOnly && !props.disabled && props.required}
        popupBaseProps={{
          popupBaseClassName: 'inside-infobar'
        }}
        errorTooltipProps={pickBy(
          errorTooltipProps,
          item => !isUndefined(item)
        )}
        minDate={minDate}
        maxDate={maxDate}
        dateTooltip={handleDateTooltip}
        value={value}
        onChange={handleChange}
        onBlur={handleOnBlur}
      />
    </div>
  );
};
