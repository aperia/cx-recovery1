import React, { FC, useEffect, useState } from 'react';
import { useField } from 'formik';
import pickBy from 'lodash.pickby';
import { isFunction, isUndefined } from 'app/_libraries/_dls/lodash';
import classNames from 'classnames';
import {
  NumericProps,
  NumericTextBox,
  OnChangeNumericEvent
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface FormikNumericControlProps extends NumericProps {
  name: string;
  format?: string;
  forceError?: boolean;
  forceShowBorderError?: boolean;
  onValueChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  onValueBlur?: (event: React.ChangeEvent<HTMLInputElement>) => void;
}

export const FormikNumericControl: FC<FormikNumericControlProps> = ({
  className,
  format = 'c2',
  name,
  forceError,
  forceShowBorderError, // show border error when outfocus and focus again item payment in batch modal
  onValueChange,
  onValueBlur,
  ...props
}) => {
  const { t } = useTranslation();
  const [field, { error, touched }] = useField(name);
  const [value, setValue] = useState(field.value);

  useEffect(() => {
    setValue(field?.value || '');
  }, [touched, field.value]);

  const errorTooltipProps = {
    opened: !forceError ? undefined : !!error && forceError
  };

  const handleChange = (e: OnChangeNumericEvent) => {
    setValue(e.value);

    if (isFunction(onValueChange)) {
      const event = {
        ...e,
        target: {
          ...e.target,
          value: e.value
        }
      } as unknown as React.ChangeEvent<HTMLInputElement>;
      onValueChange(event);
    }
  };

  const handleOnBlur: React.FocusEventHandler<HTMLInputElement> = e => {
    field.onBlur({ target: { name, value } });
    field.onChange({ target: { name, value } });

    // Show FTC Details when blur TransactionAmount in Batch Modal
    if (isFunction(onValueBlur)) {
      const event = {
        ...e,
        target: { name, value }
      } as unknown as React.ChangeEvent<HTMLInputElement>;
      onValueBlur(event);
    }
  };

  return (
    <div className={classNames(className)}>
      <NumericTextBox
        {...props}
        name={name}
        format={format}
        value={value}
        onChange={handleChange}
        onBlurCapture={handleOnBlur}
        error={
          (!props.disabled && props.required && touched && error) || forceError || forceShowBorderError
            ? {
                message: t(error),
                status: !!error || forceError
              }
            : undefined
        }
        errorTooltipProps={pickBy(
          errorTooltipProps,
          item => !isUndefined(item)
        )}
      />
    </div>
  );
};
