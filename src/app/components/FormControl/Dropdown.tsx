import React, {
  FC,
  JSXElementConstructor,
  useEffect,
  useMemo,
  useState
} from 'react';
import { useField } from 'formik';

// components
import {
  DropdownBaseChangeEvent,
  DropdownList,
  DropdownListProps
} from 'app/_libraries/_dls/components';

// helpers
import { isEmpty, isUndefined, pickBy, isArray } from 'app/helpers';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface DropdownControlProps
  extends Omit<DropdownListProps, 'children'> {
  name: string;
  forceError?: boolean;
  isCombine?: boolean;
  hasNone?: boolean;
  options: RefDataValue[];
  onlyShowValue?: boolean;
  onValueChange?: Function;
}

function generateDescription(
  itemValue: RefDataValue,
  isCombine?: boolean,
  onlyShowValue?: boolean
) {
  if (isEmpty(itemValue)) return '';

  if (onlyShowValue && itemValue.value) {
    return itemValue.value;
  }

  if (!isCombine || !itemValue.value) {
    return itemValue.description;
  }

  if (itemValue.value === 'None') return itemValue.value;

  return `${itemValue.value} - ${itemValue.description}`;
}

const DropdownControl: FC<DropdownControlProps> = ({
  className,
  name,
  forceError,
  options: optionsProp,
  isCombine,
  onlyShowValue,
  onValueChange,
  ...props
}) => {
  const { t } = useTranslation();
  const [{ value: valueProps, onChange, onBlur }, { error, touched }] =
    useField(name);

  const [value, setValue] = useState(valueProps);

  const handleChange = (e: DropdownBaseChangeEvent) => {
    const currentValue = e.target.value;

    setValue(currentValue?.value);
    onValueChange && onValueChange(e);
  };

  const handleOnBlur = (e: React.FocusEvent<Element>) => {
    if (props.disabled || props.readOnly) return;
    // convert value type RefDataValue to string
    const handler: ProxyHandler<React.FocusEvent<Element>> = {
      get: function (target, prop) {
        if (prop === 'target') {
          return {
            value: (e.target as unknown as { value: RefDataValue })?.value
              ?.value,
            name: (e.target as unknown as { name: string }).name
          } as DropdownProxyEvent;
        }
        return Reflect.get(target, prop);
      }
    };

    const newEvent = new Proxy(e, handler);
    onBlur(newEvent);
    onChange(newEvent);
  };

  const options = useMemo(() => {
    const optionsFiltered = optionsProp.filter(item => item.show);
    return !props.hasNone
      ? optionsFiltered
      : (
          [
            { value: undefined, description: 'None', show: true }
          ] as unknown as RefDataValue[]
        ).concat(optionsFiltered);
  }, [optionsProp, props.hasNone]);

  const genDropdownValue = useMemo(() => {
    const optionsGroups = options.filter(option =>
      Object.keys(option).includes('group')
    );

    if (isArray(optionsGroups)) {
      for (const optionGroup of optionsGroups) {
        const dataRef = optionGroup.group?.data;
        if (isArray(dataRef)) {
          const result = dataRef.find(
            (val: RefDataValue) => val?.value === value
          );
          if (!result) {
            continue;
          }
          return result;
        }
      }
    }

    return isArray(options) ? options.find(val => val?.value === value) : value;
  }, [options, value]);
  const errorTooltipProps = {
    opened: !forceError ? undefined : false
  };

  useEffect(() => {
    setValue(valueProps);
  }, [options, touched, valueProps]);

  return (
    <div className={className}>
      <DropdownList
        {...props}
        name={name}
        label={t(props.label)}
        value={genDropdownValue}
        onChange={handleChange}
        onBlur={handleOnBlur}
        textField={'description'}
        textFieldRender={textValue =>
          generateDescription(
            textValue,
            isCombine,
            onlyShowValue
          ) as unknown as React.ReactElement<
            any,
            string | JSXElementConstructor<any>
          >
        }
        popupBaseProps={{
          popupBaseClassName: 'inside-infobar'
        }}
        error={
          (props.required && touched && error && !props.disabled) || forceError
            ? {
                message: t(error),
                status: !!error || forceError
              }
            : undefined
        }
        errorTooltipProps={pickBy(
          errorTooltipProps,
          item => !isUndefined(item)
        )}
        required={props.required && !props.disabled}
      >
        {options.map((item: any) => {
          if (item['group']) {
            const { label = '', data = [] } = item['group'];
            if (!Array.isArray(data)) return <></>;
            return (
              <DropdownList.Group key={label} label={label}>
                {data?.map((option: any) => (
                  <DropdownList.Item
                    className="mt-6"
                    value={option}
                    label={generateDescription(
                      option,
                      isCombine,
                      onlyShowValue
                    )}
                    key={option.value}
                  />
                ))}
              </DropdownList.Group>
            );
          }
          const dropDownLbl = generateDescription(
            item,
            isCombine,
            onlyShowValue
          );

          return (
            <DropdownList.Item
              value={item}
              label={dropDownLbl}
              key={item.value}
            />
          );
        })}
      </DropdownList>
    </div>
  );
};

export { DropdownControl, generateDescription };
