import React, { useEffect, useMemo, useState } from 'react';
import { useField } from 'formik';

// components
import { TextBox, TextBoxProps } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { isUndefined, pickBy, isFunction } from 'app/helpers';

type TextBoxControlProps = TextBoxProps & {
  name: string;
  regex?: RegExp;
  forceError?: boolean;
  forceUpdateValue?: boolean;
  onValueChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
  suffixOnFocus?: string;
  formatNumber?: (value: number) => string;
};

const TextBoxControl: React.FC<TextBoxControlProps> = ({
  onValueChange,
  formatNumber,
  className,
  name,
  regex,
  forceError,
  forceUpdateValue,
  suffixOnFocus,
  ...props
}) => {
  const { t } = useTranslation();
  const [field, { error, touched }] = useField(name);

  const [value, setValue] = useState(field.value);
  const [isFocus, setIsFocus] = useState(false);

  const handleChange: React.ChangeEventHandler<HTMLInputElement> = e => {
    const { value: newValue } = e.target;
    const changeValue = regex ? newValue.replace(regex, '') : newValue;

    setValue(changeValue);

    if (isFunction(onValueChange)) {
      const event = {
        ...e,
        target: {
          ...e.target,
          value: changeValue
        }
      } as React.ChangeEvent<HTMLInputElement>;
      onValueChange(event);
    }
  };

  const handleOnBlur: React.FocusEventHandler<HTMLInputElement> = e => {
    field.onBlur(e);
    field.onChange(e);
    setIsFocus(false);

    if (isFunction(formatNumber) && value !== '') {
      const parseValue = parseFloat(value);

      const changeValue = formatNumber(parseValue);

      setValue(changeValue);
    }
  };

  const handleFocus: React.FocusEventHandler<HTMLInputElement> = e => {
    setIsFocus(true);
  };

  useEffect(() => {
    if (!touched) {
      setValue(field?.value || '');
    }
  }, [touched, field.value]);

  // for form Item Payment in Batch modal
  useEffect(() => {
    if (forceUpdateValue) {
      setValue(field?.value || '');
    }
  }, [field.value, forceUpdateValue]);

  const errorTooltipProps = {
    opened: !forceError || error ? undefined : false
  };

  const suffixValue = useMemo(() => {
    if (!isUndefined(suffixOnFocus)) {
      if (value || (!value && isFocus)) return suffixOnFocus;
    }

    if (props?.suffix) {
      return props.suffix;
    }
  }, [isFocus, props.suffix, suffixOnFocus, value]);

  return (
    <div className={className}>
      <TextBox
        {...props}
        suffix={suffixValue}
        value={value}
        onFocus={handleFocus}
        onChange={handleChange}
        onBlur={handleOnBlur}
        label={t(props.label)}
        name={name}
        error={
          (!props.disabled && touched && error) || forceError
            ? {
                message: t(error) || '',
                status: !!error || !!forceError
              }
            : undefined
        }
        errorTooltipProps={pickBy(
          errorTooltipProps,
          item => !isUndefined(item)
        )}
      />
    </div>
  );
};

export { TextBoxControl };
