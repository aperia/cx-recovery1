import React, { useEffect, useState } from 'react';

// utils
import { isUndefined, pickBy } from 'app/helpers';

// components
import {
  conformToMask,
  MaskedTextBox,
  MaskedTextBoxProps
} from 'app/_libraries/_dls/components';

// hooks
import { useField } from 'formik';
import { useTranslation } from 'app/_libraries/_dls/hooks';

type MaskedTextBoxControlProps = MaskedTextBoxProps & {
  name: string;
  forceError?: boolean;
  className?: string;
  parseReg?: RegExp;
};

const MaskedTextBoxControl: React.FC<MaskedTextBoxControlProps> = ({
  name,
  forceError,
  ref,
  className,
  parseReg = '',
  ...props
}) => {
  const { t } = useTranslation();
  const [field, { error, touched }] = useField(name);

  const [value, setValue] = useState(field.value);

  const handleChange: React.ChangeEventHandler<HTMLInputElement> = e => {
    setValue(e.target.value);
  };

  const handleBlur: React.FocusEventHandler<HTMLInputElement> = e => {
    field.onBlur(e);
    field.onChange(e);
  };

  useEffect(() => {
    if (!touched) {
      setValue(conformToMask(field?.value || '', props.mask).conformedValue);
    }
  }, [touched, field.value, props.mask]);

  const errorTooltipProps = {
    opened: !forceError || error ? undefined : false
  };

  return (
    <div className={className}>
      <MaskedTextBox
        dataTestId={name}
        name={name}
        {...props}
        label={t(props.label)}
        required={!props.disabled && props.required}
        value={value}
        onChange={handleChange}
        onBlur={handleBlur}
        error={
          (!props.disabled && touched && error) || forceError
            ? {
                message: t(error),
                status: !!error || forceError
              }
            : undefined
        }
        errorTooltipProps={pickBy(
          errorTooltipProps,
          item => !isUndefined(item)
        )}
      />
    </div>
  );
};

export { MaskedTextBoxControl };
