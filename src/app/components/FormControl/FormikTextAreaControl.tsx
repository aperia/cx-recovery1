import React, { FC } from 'react';
import { useField } from 'formik';
import { TextArea, TextAreaProps } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface FormikTextAreaControlProps extends TextAreaProps {
  name: string;
  maxLength: number;
  disabledEnter?: boolean;
}

const FormikTextAreaControl: FC<FormikTextAreaControlProps> = ({
  className,
  name,
  label,
  maxLength,
  disabledEnter,
  ...props
}) => {
  const { t } = useTranslation();
  const [field, { error, touched }] = useField(name);

  const handleOnKeyPress: React.KeyboardEventHandler<HTMLTextAreaElement> =
    e => {
      if (e.key === 'Enter' && disabledEnter) e.preventDefault();
    };

  return (
    <>
      <TextArea
        label={t(label)}
        maxLength={maxLength}
        className={`resize-none h-120px ${className}`}
        onKeyPress={handleOnKeyPress}
        {...field}
        {...props}
        error={
          !props.disabled && props.required && touched && error
            ? {
                message: t(error),
                status: !!error
              }
            : undefined
        }
      />
      <p className="color-grey-l24 fs-12 mt-8">
        {t('txt_character_left', {
          count: maxLength - (field.value?.length || 0)
        })}
      </p>
    </>
  );
};

export default FormikTextAreaControl;
