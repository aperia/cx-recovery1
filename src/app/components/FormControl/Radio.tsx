import React, { useEffect, useState } from 'react';
import { useField } from 'formik';

// components
import { Radio } from 'app/_libraries/_dls/components';

// hooks

type RadioControlProps = {
  name: string;
  className?: string;
  label?: string;
  required?: boolean;
  description?: string;
  firstValue: string;
  secondValue: string;
};

const RadioControl: React.FC<RadioControlProps> = ({
  className,
  name,
  label,
  firstValue,
  secondValue,
  description,
  required
}) => {
  const [field, { touched }] = useField(name);

  const [value, setValue] = useState(field.value);

  const handleChange = (val: string) => {
    setValue(val);
  };

  const handleOnBlur = () => {
    field.onBlur({ target: { name, value } });
    field.onChange({ target: { name, value } });
  };

  useEffect(() => {
    if (!touched) {
      setValue(field.value);
    }
  }, [touched, field.value]);

  return (
    <div className={className}>
      <div className="row">
        {!!label && (
          <p className={'fw-500 fs-14 col-5 color-grey'}>
            {`${label}:`}
            {required && <span className="asterisk color-danger ml-4">*</span>}
          </p>
        )}
        <div className="col-12">
          <Radio className="d-inline-block mr-24">
            <Radio.Input
              name={name}
              checked={value === 'Y'}
              onChange={() => handleChange('Y')}
              onBlur={handleOnBlur}
            />
            <Radio.Label>{firstValue}</Radio.Label>
          </Radio>

          <Radio className="d-inline-block">
            <Radio.Input
              name={name}
              checked={value === 'N'}
              onChange={() => handleChange('N')}
              onBlur={handleOnBlur}
            />
            <Radio.Label>{secondValue}</Radio.Label>
          </Radio>
        </div>
        {description && (
          <p className="col-12 mt-4 fs-12 color-grey-l24">{description}</p>
        )}
      </div>
    </div>
  );
};

export { RadioControl };
