import React, { useEffect, useMemo, useState } from 'react';

// components
import {
  ComboBox,
  ComboBoxProps,
  DropdownBaseChangeEvent
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { generateDescription } from './Dropdown';
import { useField } from 'formik';

import { isUndefined, pickBy } from 'app/helpers';
import { FORM_NAMES } from 'pages/AgencyManagement/components/SaveAgencyRequestModal/constants';
import { I18N_TEXT } from 'app/constants';
import { InforDebtor } from 'pages/AgencyManagement/types';
import { Debtors } from 'pages/AccountManagement/types';

export interface ComboBoxControlProps extends Omit<ComboBoxProps, 'children'> {
  className?: string;
  name: string;
  forceError?: boolean;
  isCombine?: boolean;
  options: RefDataValue[];
  isAutoSelect?: boolean;
  setFieldValueFormik?: (field: string, values: string) => void;
  inforDebtor?: InforDebtor;
  debtorList?: Debtors[];
  onValueChange?: Function;
}

const ComboBoxControl: React.FC<ComboBoxControlProps> = ({
  name,
  className,
  label,
  options: optionsProps,
  isCombine,
  forceError,
  isAutoSelect,
  setFieldValueFormik,
  inforDebtor,
  debtorList,
  onValueChange,

  ...props
}) => {
  const { t } = useTranslation();
  const [{ value: valueProp, onChange, onBlur }, { error, touched }] =
    useField(name);

  const [value, setValue] = useState<string>(valueProp);

  const handleOnChange = (event: DropdownBaseChangeEvent) => {
    setValue(event.target.value?.value);
    onValueChange && onValueChange(event);
  };

  const handleOnBlur = (event: React.FocusEvent<Element>) => {
    // convert value type RefDataValue to string
    const handler: ProxyHandler<React.FocusEvent<Element>> = {
      get: function (target, prop) {
        if (prop === 'target') {
          return {
            value: (event.target as unknown as { value: RefDataValue })?.value
              ?.value,
            name: (event.target as unknown as { name: string }).name
          };
        }
        return Reflect.get(target, prop);
      }
    };
    const newEvent = new Proxy(event, handler);
    onBlur(newEvent);
    onChange(newEvent);
  };

  const options = useMemo(() => {
    const optionsFiltered = optionsProps.filter(opt => opt.show);
    return optionsFiltered.map(opt => ({
      ...opt,
      description: generateDescription(opt, isCombine)
    }));
  }, [isCombine, optionsProps]);

  const comboBoxValue = useMemo(() => {
    if (isAutoSelect && options.length === 1) {
      setFieldValueFormik &&
        setFieldValueFormik(FORM_NAMES.saveNumber.name, options[0].value);

      return options[0];
    }
    return options.length ? options.find(val => val?.value === value) : value;
  }, [isAutoSelect, options, setFieldValueFormik, value]);

  const genDivHTML = useMemo(() => {
    return (
      <div
        dangerouslySetInnerHTML={{
          __html: `${t(
            I18N_TEXT.CANNOT_SELECT_DEBTOR_NAME
          )} <span class="fw-500">${inforDebtor?.accNumber}</span>.`
        }}
      ></div>
    );
  }, [inforDebtor?.accNumber, t]);

  // find the reference of binding props
  useEffect(() => {
    if (!touched) {
      setValue(valueProp);
    }
  }, [valueProp, options, touched]);

  const errorTooltipProps = {
    opened: !forceError || error ? undefined : false
  };

  return (
    <div className={className}>
      <ComboBox
        {...props}
        name={name}
        label={t(label)}
        value={comboBoxValue}
        onChange={handleOnChange}
        onBlur={handleOnBlur}
        textField={'description'}
        required={props.required && !props.disabled}
        noResult={t('txt_no_results_found')}
        popupBaseProps={{
          popupBaseClassName: 'inside-infobar'
        }}
        error={
          (!props.disabled && touched && error && props.required) || forceError
            ? {
                message: t(error),
                status: !!error || forceError
              }
            : undefined
        }
        errorTooltipProps={pickBy(
          errorTooltipProps,
          item => !isUndefined(item)
        )}
      >
        {inforDebtor && debtorList
          ? options.map(item => {
              return (
                <ComboBox.Item
                  variant="custom"
                  value={item}
                  label={item.description}
                  key={item.value}
                  disabled={item.isDisable}
                  tooltipProps={
                    item.isDisable
                      ? {
                          element: genDivHTML,
                          placement: 'top-start'
                        }
                      : undefined
                  }
                >
                  {inforDebtor && (
                    <div className="d-flex justify-content-between">
                      <span>{item.value}</span>
                      <span>
                        SSN:{' '}
                        {
                          debtorList?.find(
                            (debtor: Debtors) =>
                              debtor.debtorName === item.value
                          )?.ssn
                        }
                      </span>
                    </div>
                  )}
                </ComboBox.Item>
              );
            })
          : options.map(item => {
              return (
                <ComboBox.Item
                  value={item}
                  label={item.description}
                  key={item.value}
                />
              );
            })}
      </ComboBox>
    </div>
  );
};

export { ComboBoxControl };
