import React, { useEffect, useState } from 'react';
import { useField } from 'formik';

// components
import { Radio } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { isFunction } from 'app/helpers';
import classNames from 'classnames';

type GroupRadioControlProps = {
  name: string;
  className?: string;
  direction?: 'vertical' | 'horizontal';
  options: RefDataValue[];
  forceUpdateValue?: boolean;
  onValueChange?: (event: React.ChangeEvent<HTMLInputElement>) => void;
};

const GroupRadioControl: React.FC<GroupRadioControlProps> = ({
  onValueChange,
  className,
  name,
  direction = 'vertical',
  options,
  forceUpdateValue
}) => {
  const { t } = useTranslation();
  const [field, { touched }] = useField(name);

  const [value, setValue] = useState(field.value);

  const handleChange = (val: string) => {
    setValue(val);

    if (isFunction(onValueChange)) {
      const event = {
        target: { name, value: val }
      } as React.ChangeEvent<HTMLInputElement>;

      onValueChange(event);
    }
  };

  const handleOnBlur = () => {
    field.onBlur({ target: { name, value } });
    field.onChange({ target: { name, value } });
  };

  useEffect(() => {
    if (!touched) {
      setValue(field.value);
      field.onBlur({ target: { name } });
    }
  }, [touched, field.value, field, name]);

  // for form Item Payment in Batch modal
  useEffect(() => {
    if (forceUpdateValue) {
      setValue(field.value);
    }
  }, [field.value, forceUpdateValue]);

  return (
    <div
      className={classNames(
        className,
        direction === 'vertical' ? 'radio-vertical' : 'radio-horizontal'
      )}
    >
      {options.map((radioItem, index) => (
        <Radio key={`${name}-${index}`}>
          <Radio.Input
            name={name}
            checked={value === radioItem.value}
            onChange={() => handleChange(radioItem.value)}
            onBlur={handleOnBlur}
          />
          <Radio.Label>{t(radioItem.description)}</Radio.Label>
        </Radio>
      ))}
    </div>
  );
};

export { GroupRadioControl };
