import React, { useEffect, useState } from 'react';
import { useField } from 'formik';

// components
import { Radio } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { isFunction } from 'lodash';
import classNames from 'classnames';

type YesNoQuestionControlProps = {
  name: string;
  className?: string;
  direction?: 'vertical' | 'horizontal';
  required?: boolean;
  onValueChange?: Function;
};

const YesNoQuestionControl: React.FC<YesNoQuestionControlProps> = ({
  className,
  direction = 'horizontal',
  name,
  onValueChange
}) => {
  const { t } = useTranslation();
  const [field, { touched }] = useField(name);

  const [value, setValue] = useState(field.value);

  const handleChange = (val: string) => {
    setValue(val);
    isFunction(onValueChange) && onValueChange(val);
  };

  const handleOnBlur = () => {
    field.onBlur({ target: { name, value } });
    field.onChange({ target: { name, value } });
  };

  useEffect(() => {
    if (!touched) {
      setValue(field.value);
    }
  }, [touched, field.value]);

  return (
    <div
      className={classNames(
        className,
        direction === 'vertical' ? 'radio-vertical' : 'radio-horizontal'
      )}
    >
      <Radio>
        <Radio.Input
          name={name}
          checked={value === 'Y'}
          onChange={() => handleChange('Y')}
          onBlur={handleOnBlur}
        />
        <Radio.Label>{t('txt_yes')}</Radio.Label>
      </Radio>

      <Radio>
        <Radio.Input
          name={name}
          checked={value === 'N'}
          onChange={() => handleChange('N')}
          onBlur={handleOnBlur}
        />
        <Radio.Label>{t('txt_no')}</Radio.Label>
      </Radio>
    </div>
  );
};

export { YesNoQuestionControl };
