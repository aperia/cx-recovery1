// CheckBox
import React, { FC } from 'react';
import { useField } from 'formik';
import classNames from 'classnames';
import { CheckBox, CheckBoxProps } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface CheckboxControlProps
  extends Omit<CheckBoxProps, 'children' | 'ref'> {
  name: string;
  label: string;
  status?: boolean;
}

export const CheckboxControl: FC<CheckboxControlProps> = ({
  className,
  name,
  label,
  status = false,
  dataTestId,
  ...props
}) => {
  const { t } = useTranslation();
  const [field] = useField(name);

  return (
    <div className={classNames(className)}>
      <CheckBox dataTestId={dataTestId}>
        <CheckBox.Input
          readOnly={status}
          checked={field.value}
          {...props}
          {...field}
        />
        <CheckBox.Label>{t(label)}</CheckBox.Label>
      </CheckBox>
    </div>
  );
};
