import React, { useCallback, useContext, useLayoutEffect } from 'react';

// hooks

import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { isFunction, isUndefined } from 'lodash';
import classNames from 'classnames';
import { JumpLinksContext } from '.';

export interface JumpLinksItemProps {
  scrollDistance?: number;
  itemIndex?: string;
  title?: string;
  children?: React.ReactNode;
  to?: string;
}

const JumpLinksItem: React.FC<JumpLinksItemProps> = ({
  scrollDistance = 0,
  itemIndex,
  title,
  children,
  to
}) => {
  const { t } = useTranslation();
  const { activeIndex, setActiveIndex } = useContext(JumpLinksContext);

  useLayoutEffect(() => {
    const element = document.querySelector(`#${to}`);
    if (!element) return;

    if (element instanceof HTMLElement) {
      const marginTop = window
        .getComputedStyle(element)
        .marginTop.replace('px', '');

      const marginTopVal = !isNaN(+marginTop) ? +marginTop : 0;

      const sectionOffsetTop = element.offsetTop - marginTopVal;

      // assign active class to jump link item while scrolling
      if (scrollDistance === 0) {
        setActiveIndex('0');
        return;
      }

      if (scrollDistance >= sectionOffsetTop) {
        setActiveIndex(itemIndex);
      }
    }
  }, [itemIndex, scrollDistance, to, setActiveIndex]);

  const handleClick = useCallback(() => {
    if (isUndefined(to)) return;

    const element = document.querySelector(`#${to}`);
    if (!element) return;

    (element as unknown as HTMLDivElement).scrollIntoView({
      behavior: 'smooth',
      block: 'start'
    });

    if (isFunction(setActiveIndex)) {
      setActiveIndex(itemIndex);
    }
  }, [itemIndex, to, setActiveIndex]);

  return (
    <a
      className={classNames('jump-link', {
        active: itemIndex === activeIndex
      })}
      onClick={handleClick}
    >
      {title ? t(title || '') : children}
    </a>
  );
};

export default React.memo(JumpLinksItem);
