import React, { createContext, ReactNode, useCallback, useState } from 'react';

// components
import If from 'pages/__commons/If';
import { Icon } from 'app/_libraries/_dls/components';
import JumpLinksItem, { JumpLinksItemProps } from './Item';

// hooks
import useMediaQuery from 'app/hooks/useMediaQuery';

// helpers
import classNames from 'classnames';

export const JumpLinksContext = createContext<any>({});

interface StaticComponents {
  Item: React.FC<JumpLinksItemProps>;
}

export interface JumpLinksProps {
  children:
    | React.ReactElement<typeof JumpLinksItem>[]
    | React.ReactElement<typeof JumpLinksItem>;
  initActiveIndex?: string;
  className?: string;
  scrollDistance?: number;
}

export type IJumpLinks<P> = React.FC<P> & StaticComponents;

const JumpLinks: React.FC<JumpLinksProps> = React.memo(
  ({ scrollDistance, children, initActiveIndex = '-1', className }: any) => {
    const [activeIndex, setActiveIndex] = useState(initActiveIndex);
    const [isShow, setIsShow] = useState(false);

    const handleMediaQueryChange = useCallback((matches: boolean) => {
      // isDesktop
      if (matches) return;

      // Non-desktop
      setIsShow(false); // auto toggle off panel at break-point 1024
    }, []);

    const isDesktop = useMediaQuery({
      query: '(min-width: 1025px)',
      onMediaQueryChange: handleMediaQueryChange
    });

    const handleToggleJumpLinks = useCallback(() => {
      setIsShow(!isShow);
    }, [isShow]);

    const childrenWithExtraProps = React.Children.map<ReactNode, ReactNode>(
      children,
      (child, index) => {
        if (React.isValidElement(child)) {
          return React.cloneElement(child, {
            ...child.props,
            itemIndex: index.toString(),
            scrollDistance
          } as JumpLinksItemProps);
        }
      }
    );

    return (
      <JumpLinksContext.Provider value={{ activeIndex, setActiveIndex }}>
        <div className="jump-links-panel">
          <If condition={!isDesktop}>
            <span
              className="jump-links-toggle-btn"
              onClick={handleToggleJumpLinks}
            >
              <Icon
                className="color-grey-l16"
                size="5x"
                name={'arrow-navigator'}
              />
            </span>
          </If>

          <div
            className={classNames(className, 'd-none', {
              'd-block': isDesktop || (!isDesktop && isShow)
            })}
          >
            <div className="jump-links">{childrenWithExtraProps}</div>
          </div>
        </div>
      </JumpLinksContext.Provider>
    );
  }
);

const JumpLinkExtraStaticProp = JumpLinks as IJumpLinks<JumpLinksProps>;
JumpLinkExtraStaticProp.Item = JumpLinksItem;

export { JumpLinksItem, JumpLinkExtraStaticProp as default };
