import React from 'react';

import {
  Button,
  Icon,
  Tooltip,
  TooltipProps
} from 'app/_libraries/_dls/components';
import { useScreenType, useTranslation } from 'app/_libraries/_dls/hooks';
import { ScreenType } from 'app/_libraries/_dls/hooks/useScreenType/types';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { I18N_TEXT } from 'app/constants';

export interface TogglePinProps extends TooltipProps {
  dataTestId?: string;
  isPinned: boolean;
  onToggle: () => void;
}

const TogglePin: React.FC<TogglePinProps> = ({
  dataTestId,
  isPinned,
  onToggle,
  ...props
}) => {
  const { t } = useTranslation();
  const screenType = useScreenType();
  const shouldRender =
    screenType === ScreenType.DESKTOP ||
    screenType === ScreenType.TABLET_LANDSCAPE ||
    true;

  const testId = genAmtId(dataTestId!, 'toggle-pin-unpin', 'TogglePin');

  return shouldRender ? (
    <Tooltip
      element={t(isPinned ? I18N_TEXT.UNPIN : I18N_TEXT.PIN_TO_RIGHT)}
      placement="top"
      variant="primary"
      dataTestId={testId}
      {...props}
    >
      <Button variant="icon-secondary" onClick={onToggle} dataTestId={testId}>
        <Icon name={isPinned ? 'unpin' : 'pin'} dataTestId={testId} />
      </Button>
    </Tooltip>
  ) : null;
};

export default TogglePin;
