import React, {
  Fragment,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef
} from 'react';
import { batch, useDispatch } from 'react-redux';

// components
import {
  InfoBar as InfoBarBS,
  InfoBarDropDownItem,
  InfoBarItem,
  InfoBarProps
} from 'app/_libraries/_dls/components';
import EmptySection from './EmptySection';
import NoteCode from 'pages/__commons/NoteCode';
import Payout from 'pages/Payout';
import WorkDateSchedule from 'pages/WorkDateSchedule';

// Hooks
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { useScreenType, useTranslation } from 'app/_libraries/_dls/hooks';

// Constants
import { I18N_TEXT } from 'app/constants';

// Redux
import {
  selectIsExpand,
  selectPinned,
  selectRecover
} from './_redux/selectors';
import { actionsInfoBar, InfoBarSection } from './_redux';
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { getAccountDetail } from 'pages/AccountDetail/_redux/selectors';

// Types
import { ScreenType } from 'app/_libraries/_dls/hooks/useScreenType/types';

// Helpers
import { isUndefined } from 'app/helpers/lodash';
import { ledgerActions } from 'pages/AccountDetail/Ledger/_redux';

export interface InfoBarDropDownItemProps extends InfoBarDropDownItem {
  dataTestId?: string;
}

interface InfoBarComponentProps {}

const InfoBar: React.FC<InfoBarComponentProps> = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();

  const pinned = useSelectorId<InfoBarSection | undefined>(selectPinned);
  const isExpand = useSelectorId<boolean>(selectIsExpand);
  const recover = useSelectorId<InfoBarSection | undefined>(selectRecover);
  const pinnedRecover = isExpand && recover ? recover : pinned;
  const testId = 'account-detail-infobar';
  const isMounted = useRef(true);

  // InfoBar tablet portrait logic
  const screenType = useScreenType();
  const { groupId } = useSelectorId(getAccountDetail) || {};

  const items: InfoBarItem[] = useMemo(() => {
    return [
      {
        id: InfoBarSection.Notes,
        name: t(I18N_TEXT.NOTE_CODE),
        icon: { name: 'comment' },
        component: () => {
          return <NoteCode shouldRenderPinnedIcon />;
        }
      },
      {
        id: InfoBarSection.WorkDateSchedule,
        name: t(I18N_TEXT.WORK_DATE_SCHEDULE),
        icon: { name: 'calendar' },
        component: () => {
          return <WorkDateSchedule storeId={storeId} />;
        }
      },
      {
        id: InfoBarSection.Correspondence,
        name: t(I18N_TEXT.CORRESPONDENCE),
        icon: { name: 'email' },
        mode: 'static',
        onClickIcon: () => {
          dispatch(accountManagementActions.onSetModalCorrespondence(true));
        }
      },
      {
        id: InfoBarSection.Ledger,
        name: t(I18N_TEXT.LEDGER),
        icon: { name: 'fee-penalty' },
        mode: 'static',
        onClickIcon: () => {
          dispatch(ledgerActions.toggleShow({ storeId }));
        }
      },
      {
        id: InfoBarSection.Payout,
        name: t(I18N_TEXT.PAYOUT),
        icon: { name: 'flow-loan' },
        component: () => {
          return <Payout storeId={storeId} groupId={groupId} />;
        }
      }
    ] as InfoBarItem[];
  }, [dispatch, groupId, storeId, t]);

  const handleExpand = () =>
    dispatch(actionsInfoBar.updateIsExpand({ storeId, isExpand: true }));
  const handleCollapse = () =>
    dispatch(actionsInfoBar.updateIsExpand({ storeId, isExpand: false }));

  useEffect(() => {
    setImmediate(() => {
      document
        .querySelector('body')
        ?.classList.toggle('infobar-pinned', !!pinned);
    });
  }, [pinned]);

  // InfoBar pinned default is Notes
  useLayoutEffect(() => {
    if (!isUndefined(pinned)) return;

    if (isMounted.current) {
      dispatch(
        actionsInfoBar.updatePinned({
          storeId,
          pinned: InfoBarSection.Notes
        })
      );

      isMounted.current = false;
    }
  }, [dispatch, pinned, storeId]);

  // InfoBar recover logic
  useEffect(() => {
    let timeoutId: ReturnType<typeof setTimeout>;

    if (!isExpand && pinned) {
      dispatch(actionsInfoBar.updateRecover({ storeId, recover: pinned }));
      timeoutId = setTimeout(() => {
        dispatch(actionsInfoBar.updatePinned({ storeId, pinned: undefined }));
      }, 300);
    }

    if (isExpand && recover) {
      batch(() => {
        dispatch(actionsInfoBar.updateRecover({ storeId, recover: undefined }));
        dispatch(actionsInfoBar.updatePinned({ storeId, pinned: recover }));
      });
    }

    return () => clearTimeout(timeoutId);
  }, [dispatch, isExpand, pinned, recover, storeId]);

  // InfoBar tablet portrait logic
  useLayoutEffect(() => {
    dispatch(
      actionsInfoBar.updateIsExpand({
        storeId,
        isExpand: screenType !== ScreenType.TABLET_PORTRAIT
      })
    );
  }, [dispatch, screenType, storeId]);

  return (
    <Fragment>
      <InfoBarBS
        items={items}
        pinned={pinnedRecover as InfoBarProps['pinned']}
        expand={isExpand}
        onExpand={handleExpand}
        onCollapse={handleCollapse}
        emptyElement={<EmptySection />}
        dataTestId={testId}
        collapseText={t('txt_collapse_sidebar')}
        expandText={t('txt_expand_sidebar')}
      />
    </Fragment>
  );
};

export default React.memo(InfoBar);
