import { createSelector } from '@reduxjs/toolkit';
import { InfoBarSection, InfoBarState } from '.';

const getInfoBar = (states: RootState, storeId: string): InfoBarState =>
  states.infoBar[storeId] || {};

export const selectPinned = createSelector(
  getInfoBar,
  (data: InfoBarState): InfoBarSection | undefined => data?.pinned
);

export const selectIsExpand = createSelector(
  getInfoBar,
  (data: InfoBarState): boolean => !!data?.isExpand
);

export const selectRecover = createSelector(
  getInfoBar,
  (data: InfoBarState): InfoBarSection | undefined => data?.recover
);
