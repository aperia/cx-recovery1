import { createSlice, PayloadAction } from '@reduxjs/toolkit';

export interface InfoBarPayload {
  storeId: string;
  pinned?: InfoBarSection;
  isExpand?: boolean;
  recover?: InfoBarSection;
}

export enum InfoBarSection {
  Notes = 'Notes',
  WorkDateSchedule = 'Work Date Schedule',
  Correspondence = 'Correspondence',
  Payout = 'Payout',
  Ledger = 'Ledger'
}

export interface InfoBarState {
  pinned?: InfoBarSection;
  isExpand?: boolean;
  recover?: InfoBarSection;
}

const { actions, reducer } = createSlice({
  name: 'infoBar',
  initialState: {} as Record<string, InfoBarState>,
  reducers: {
    removeInfoBar: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;
      delete draftState[storeId];
    },
    updatePinned: (draftState, action: PayloadAction<InfoBarPayload>) => {
      const { storeId, pinned } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        pinned: pinned!
      };
    },
    updateIsExpand: (draftState, action: PayloadAction<InfoBarPayload>) => {
      const { storeId, isExpand } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isExpand: isExpand!
      };
    },
    updateRecover: (draftState, action: PayloadAction<InfoBarPayload>) => {
      const { storeId, recover } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        recover: recover!
      };
    }
  }
});

const allActions = {
  ...actions
};

export { allActions as actionsInfoBar, reducer };
