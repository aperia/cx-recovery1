import React from 'react';

// components
import { Icon } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_TEXT } from 'app/constants';

export interface EmptySectionProps {
  dataTestId?: string;
}

const EmptySection: React.FC<EmptySectionProps> = ({ dataTestId }) => {
  const { t } = useTranslation();

  return (
    <div className="d-flex flex-column align-items-center">
      <Icon
        className="fs-80 mt-80 color-light-l12"
        name="pin"
        dataTestId={dataTestId}
      />
      <p className="color-grey mt-16">{t(I18N_TEXT.NO_SECTIONS_PINNER_HERE)}</p>
    </div>
  );
};

export default EmptySection;
