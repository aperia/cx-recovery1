import React, { FC } from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';

// helper
import classNames from 'classnames';
import dateTime from 'date-and-time';
import { I18N_TEXT } from 'app/constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';

interface FailedApiReloadProps {
  id: string;
  dataTestId?: string;
  className?: string;
  dataLoadText?: string;
  onReload: () => void;
}
const FailedApiReload: FC<FailedApiReloadProps> = ({
  id,
  dataTestId,
  className = 'mt-24',
  onReload,
  dataLoadText = I18N_TEXT.TRY_AGAIN_CLICK_RELOAD
}) => {
  const { t } = useTranslation();
  const date = dateTime.format(new Date(), 'MM/DD/YYYY hh:mm:ss A');

  return (
    <div className={classNames('text-center', className)}>
      <p
        className="color-grey-d20"
        data-testid={genAmtId(dataTestId!, 'failure-text', 'FailedApiReload')}
      >
        {t(dataLoadText)}
      </p>
      <p
        className="fs-12 color-grey mt-4"
        data-testid={genAmtId(dataTestId!, 'failure-date', 'FailedApiReload')}
      >
        {t(I18N_TEXT.FAILURE_OCCUR_ON, { date })}
      </p>
      <Button
        id={`${id}_Button`}
        dataTestId={genAmtId(dataTestId!, 'reload-btn', 'FailedApiReload')}
        onClick={onReload}
        className="mt-24"
        size="sm"
      >
        {t(I18N_TEXT.RELOAD)}
      </Button>
    </div>
  );
};

export default FailedApiReload;
