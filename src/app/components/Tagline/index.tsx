import React from 'react';
import { Tooltip } from 'app/_libraries/_dls/components';
import { convertValueDescription } from 'pages/__commons/helpers';

const TagLine = (itemValue: RefDataValue) => {
  const convertToolTip = convertValueDescription(itemValue);
  return (
    <Tooltip triggerClassName="mt-4" element={convertToolTip}>
      <span className="tag-line">{itemValue?.value}</span>
    </Tooltip>
  );
};

export default TagLine;
