import React, { FC, useEffect, useState } from 'react';
import { useField } from 'formik';
import pickBy from 'lodash.pickby';
import { isUndefined } from 'app/_libraries/_dls/lodash';
import classNames from 'classnames';
import {
  NumericProps,
  NumericTextBox,
  OnChangeNumericEvent
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface FormikNumericControlProps extends NumericProps {
  name: string;
  format?: string;
  forceError?: boolean;
}

export const FormikNumericControl: FC<FormikNumericControlProps> = ({
  className,
  format = 'c2',
  name,
  forceError,
  ...props
}) => {
  const { t } = useTranslation();
  const [field, { error, touched }] = useField(name);
  const [value, setValue] = useState(field.value);

  useEffect(() => {
    setValue(field?.value || '');
  }, [touched, field.value]);

  const errorTooltipProps = {
    opened: !forceError ? undefined : !!error && forceError
  };

  const handleChange = (e: OnChangeNumericEvent) => {
    setValue(e.value);
  };

  const handleOnBlur: React.FocusEventHandler<HTMLInputElement> = e => {
    field.onBlur({ target: { name, value } });
    field.onChange({ target: { name, value } });
  };

  return (
    <div className={classNames(className)}>
      <NumericTextBox
        {...props}
        name={name}
        format={format}
        value={value}
        onChange={handleChange}
        onBlurCapture={handleOnBlur}
        error={
          (!props.disabled && props.required && touched && error) || forceError
            ? {
                message: t(error),
                status: !!error || forceError
              }
            : undefined
        }
        errorTooltipProps={pickBy(
          errorTooltipProps,
          item => !isUndefined(item)
        )}
      />
    </div>
  );
};
