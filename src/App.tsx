import React, { useEffect, useState } from 'react';

// DOF
import { App as DOFApp, DataApiContext } from 'app/_libraries/_dof/core';
import 'app/_libraries/_dof/layouts';
import 'registerControls';

// Components
import AppContainer from 'AppContainer';
import ErrorBoundary from 'pages/__commons/ErrorBoundary';
import FallbackErrorComponent from 'pages/__commons/ErrorBoundary/FallbackErrorComponent';

// Reducers
import { reducerMappingList } from './storeConfig';
import I18next from 'pages/__commons/I18nextProvider';
import MappingProvider from 'pages/__commons/MappingProvider';
import axios from 'axios';
import classNames from 'classnames';

import { refData } from 'app/components/_dof_controls/ref-data';
import { DATA_SOURCE } from 'app/helpers';

// redux middleware
import thunk from 'redux-thunk';
import { catchErrorMiddleWare } from './customMiddleWare';

export interface AppProps {}

export const urlConfigs = [
  'dof/views/accountList.json',
  'dof/fields/accountList.json',
  'dof/fields/accountInfo.json',

  'dof/views/monitorCriteria.json',
  'dof/fields/monitorCriteria.json',

  'dof/views/returnCriteria.json',
  'dof/fields/returnCriteria.json',

  'dof/views/agencyUserDetail.json',
  'dof/fields/agencyUserDetail.json',

  'dof/views/monitorDeleteBody.json',
  'dof/fields/monitorDeleteBody.json',

  'dof/views/returnDeleteBody.json',
  'dof/fields/returnDeleteBody.json',

  'dof/views/feeInformation.json',
  'dof/fields/feeInformation.json',

  'dof/views/monitorCriteriaList.json',
  'dof/fields/monitorCriteriaList.json',

  'dof/views/returnCriteriaList.json',
  'dof/fields/returnCriteriaList.json',

  'dof/fields/manualAssignRecallReq.json',
  'dof/views/manualAssignRecallReq.json',

  'dof/fields/manualRecallList.json',
  'dof/views/manualRecallList.json',

  'dof/fields/viewManualAssignRecall.json',
  'dof/views/viewManualAssignRecall.json',

  'dof/fields/debtorDetailAccountInformation.json',
  'dof/views/debtorDetailAccountInformation.json',

  'dof/fields/debtorDetailEmployerInformation.json',
  'dof/views/debtorDetailEmployerInformation.json',

  'dof/fields/associatedAccountInfo.json',
  'dof/views/associatedAccountInfo.json',

  'dof/fields/snapshotAccountDetail.json',
  'dof/views/snapshotAccountDetail.json',

  'dof/fields/generalInfo.json',
  'dof/views/generalInfo.json',

  'dof/fields/agencyInfo.json',
  'dof/views/agencyInfo.json',

  'dof/fields/paymentInfo.json',
  'dof/views/paymentInfo.json',

  'dof/fields/miscellaneousInfo.json',
  'dof/views/miscellaneousInfo.json',

  'dof/fields/financialInfo.json',
  'dof/views/financialInfo.json',

  'dof/fields/bankruptcyInfoAccount.json',
  'dof/views/bankruptcyInfoAccount.json',

  'dof/fields/bankruptcyInfoDebtorAddress.json',
  'dof/views/bankruptcyInfoDebtorAddress.json',

  'dof/fields/bankruptcyInfoDebtorCase.json',
  'dof/views/bankruptcyInfoDebtorCase.json',

  'dof/fields/deceasedInfoAccount.json',
  'dof/views/deceasedInfoAccount.json',

  'dof/fields/deceasedInfoDebtorAddress.json',
  'dof/views/deceasedInfoDebtorAddress.json',

  'dof/fields/FinancialInfoCreditor.json',
  'dof/views/FinancialInfoCreditor.json',

  'dof/fields/demographicsDebtorInfo.json',
  'dof/views/demographicsDebtorInfo.json',

  'dof/fields/demographicsSpouseInfo.json',
  'dof/views/demographicsSpouseInfo.json',

  'dof/fields/demographicsChildrenInfo.json',
  'dof/views/demographicsChildrenInfo.json',

  'dof/fields/fsHomeInfo.json',
  'dof/views/fsHomeInfo.json',

  'dof/fields/fsAutomobileInfo.json',
  'dof/views/fsAutomobileInfo.json',

  'dof/fields/fsMonthlyAveragePayments.json',
  'dof/views/fsMonthlyAveragePayments.json',

  'dof/fields/fsChildcareInfo.json',
  'dof/views/fsChildcareInfo.json',

  'dof/fields/fsOtherAssets.json',
  'dof/views/fsOtherAssets.json',

  'dof/fields/fsComment.json',
  'dof/views/fsComment.json',

  'dof/fields/fsBankInfo.json',
  'dof/views/fsBankInfo.json',

  'dof/fields/fsPreviousAddresses.json',
  'dof/views/fsPreviousAddresses.json',

  'dof/fields/fsRelativesInfo.json',
  'dof/views/fsRelativesInfo.json',

  'dof/fields/fsReferencesInfo.json',
  'dof/views/fsReferencesInfo.json',

  'dof/fields/fsLienInfo.json',
  'dof/views/fsLienInfo.json',

  'dof/fields/suitInformation.json',
  'dof/views/suitInformation.json',

  'dof/fields/accountGroup.json',
  'dof/views/accountGroup.json',

  'dof/fields/queueList.json',
  'dof/views/queueList.json',

  'dof/views/queueDetail.json',

  'dof/fields/pendingCorrespondence.json',
  'dof/views/pendingCorrespondence.json',

  'dof/views/accountItemDetailsPayment.json',
  'dof/views/accountItemDetailsPaymentMoreInfo.json',

  'dof/fields/payoutInfo.json',
  'dof/views/payoutInfo.json',

  'dof/views/ledgerInfo.json',
  'dof/fields/ledgerInfo.json',

  'dof/views/ledgerTransactionStatistic.json',
  'dof/fields/ledgerTransactionStatistic.json',

  'dof/views/queueDetail.json'
];

const App: React.FC<AppProps> = () => {
  const [isLoadingConfig, setLoadingConfig] = useState<boolean>(true);

  const handleGetConfig = async () => {
    try {
      const mainUrls = ['config/main.json'];
      const listPromises = mainUrls.map(item => {
        return axios.get(item);
      });

      const response = await Promise.all(listPromises);

      const [mainConfigResponse] = response;
      const mainConfig = mainConfigResponse.data as AppConfiguration;

      window.appConfig = {
        ...mainConfig
      };
    } catch (e) {
      console.log(e);
    } finally {
      setLoadingConfig(false);
    }
  };

  useEffect(() => {
    handleGetConfig();
  }, []);

  if (isLoadingConfig)
    return (
      <div
        className={classNames({
          loading: isLoadingConfig,
          'vh-100': isLoadingConfig
        })}
      />
    );

  return (
    <DataApiContext.Provider value={refData}>
      <DOFApp
        customReducers={reducerMappingList}
        urlConfigs={urlConfigs}
        customDataSource={DATA_SOURCE}
        middleware={[thunk, catchErrorMiddleWare]}
      >
        <MappingProvider>
          <I18next>
            <ErrorBoundary fallbackComponent={FallbackErrorComponent}>
              <AppContainer />
            </ErrorBoundary>
          </I18next>
        </MappingProvider>
      </DOFApp>
    </DataApiContext.Provider>
  );
};

export default App;
