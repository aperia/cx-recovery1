import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelectorId } from 'app/hooks';

// constants
import { I18N_TEXT } from 'app/constants';

// components
import PinnedHeader from 'pages/__commons/PinnedHeader';
import { View } from 'app/_libraries/_dof/core';
import {
  DatePicker,
  DatePickerChangeEvent,
  Tooltip
} from 'app/_libraries/_dls/components';

// types
import { IPayout } from './types';

// selector
import { selectPayoutData } from './_redux/selectors';

// helpers
import usePrepareSnapshot from './hooks/usePrepareSnapshot';
import { payoutActions } from './_redux';
import { formatTimeDefault, isToday } from 'app/helpers';
import { FormatTime } from 'app/constants/enums';
import { InfoBarSection } from 'app/components/InfoBar/_redux';

export interface TopInfoProps {
  storeId: string;
  groupId: string;
}
const TopInfo: React.FC<TopInfoProps> = ({ storeId, groupId }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const data = useSelectorId<IPayout | undefined>(selectPayoutData);
  const dataSnapshot = usePrepareSnapshot(data);

  const [throughDate, setThroughDate] = useState<Date | undefined>(new Date());

  const handleChange = useCallback(
    ({ value }: DatePickerChangeEvent) => {
      setThroughDate(value);

      const selectedDateStr =
        formatTimeDefault(value || '', FormatTime.Date) || '';

      const dateDesc = isToday(selectedDateStr)
        ? `${t(I18N_TEXT.TODAY)}`.toLowerCase()
        : selectedDateStr;

      dispatch(
        payoutActions.setSelectedThroughDate({
          storeId,
          selectedThroughDate: {
            value: selectedDateStr,
            description: dateDesc
          }
        })
      );

      if (selectedDateStr) {
        dispatch(
          payoutActions.getPayout({
            storeId,
            groupId,
            throughDate: selectedDateStr
          })
        );
      }
    },
    [dispatch, groupId, storeId, t]
  );

  useEffect(() => {
    const selectedDateStr =
      formatTimeDefault(throughDate || '', FormatTime.Date) || '';

    const dateDesc = isToday(selectedDateStr)
      ? `${t(I18N_TEXT.TODAY)}`.toLowerCase()
      : selectedDateStr;

    dispatch(
      payoutActions.setSelectedThroughDate({
        storeId,
        selectedThroughDate: {
          value: selectedDateStr,
          description: dateDesc
        }
      })
    );
  }, [dispatch, groupId, storeId, t, throughDate]);

  const handleDateTooltip = useCallback(
    (date: Date) => {
      if (new Date(date) < new Date()) {
        return (
          <Tooltip element={t(I18N_TEXT.THROUGH_DATE_MUST_NOT_BE_PAST_DATE)} />
        );
      }
    },
    [t]
  );

  return (
    <div className="bg-light-l20 border-bottom px-24 pt-24 pb-16">
      <PinnedHeader
        storeId={storeId}
        title={I18N_TEXT.PAYOUT}
        itemIdPinned={InfoBarSection.Payout}
      />
      <View
        id={`payoutInfo-${storeId}`}
        formKey={`payoutInfo-${storeId}`}
        descriptor={`payoutInfo`}
        value={dataSnapshot}
      />

      <p className="mt-16">{t(I18N_TEXT.THROUGH_DATE_DESCRIPTION)}</p>

      <div className="mt-16">
        <DatePicker
          popupBaseProps={{
            popupBaseClassName: 'inside-infobar'
          }}
          name="throughDate"
          label={t(I18N_TEXT.THROUGH_DATE)}
          value={throughDate}
          onChange={handleChange}
          minDate={new Date()}
          dateTooltip={handleDateTooltip}
        />
      </div>
    </div>
  );
};

export default TopInfo;
