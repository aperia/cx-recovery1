import { ISnapshotPayout } from './../types';
import { useMemo } from 'react';

// hooks
import useRefData from 'app/hooks/useRefData';

// types
import { FormatTime } from 'app/constants/enums';

// helpers
import { formatCurrency, formatTimeDefault } from 'app/helpers';
import { getDescriptionFromCode } from 'pages/__commons/helpers';
import { convertValueToDataView } from 'app/_libraries/_dls/components/NumericTextBox/service';
import { DEFAULT_PERCENT } from 'app/_libraries/_dls/components/NumericTextBox/constant';

const usePrepareSnapshot = (data?: ISnapshotPayout) => {
  const paymentScheduleOptions = useRefData('paymentSchedule');

  return useMemo(() => {
    if (!data) return {} as ISnapshotPayout;

    const {
      paymentSchedule,
      paymentScheduleAmount,
      paymentScheduleStartDate,
      interestRate,
      perDiem,
      principal,
      otherIncome,
      interest,
      costs,
      total_account_balance,
      total_group_balance
    } = data;

    return {
      ...data,
      paymentSchedule: getDescriptionFromCode(
        paymentScheduleOptions,
        paymentSchedule,
        true
      ),
      interestRate: `${convertValueToDataView(
        interestRate,
        'p1'
      )}${DEFAULT_PERCENT}`,
      perDiem: `${convertValueToDataView(perDiem, 'p1')}${DEFAULT_PERCENT}`,
      principal: principal ? formatCurrency(principal) : '',
      otherIncome: otherIncome ? formatCurrency(otherIncome) : '',
      interest: interest ? formatCurrency(interest) : '',
      costs: costs ? formatCurrency(costs) : '',
      total_account_balance: total_account_balance
        ? formatCurrency(total_account_balance)
        : '',
      total_group_balance: total_group_balance
        ? formatCurrency(total_group_balance)
        : '',

      paymentScheduleAmountWithStartDate: `${formatCurrency(
        paymentScheduleAmount
      )} • ${formatTimeDefault(paymentScheduleStartDate, FormatTime.Date)}`
    };
  }, [data, paymentScheduleOptions]);
};

export default usePrepareSnapshot;
