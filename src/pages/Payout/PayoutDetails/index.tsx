import React from 'react';

// components
import List from './List';
import { ItemProps } from './Item';

// constants
import { I18N_TEXT } from 'app/constants';

export interface PayoutDetailsProps {}
const PayoutDetails: React.FC<PayoutDetailsProps> = () => {
  const items: ItemProps[] = [
    {
      label: I18N_TEXT.INTEREST_RATE,
      value: I18N_TEXT.INTEREST_RATE_DESC
    },
    {
      label: I18N_TEXT.PER_DIEM,
      value: I18N_TEXT.PER_DIEM_DESC
    },
    {
      label: I18N_TEXT.PRINCIPAL,
      value: I18N_TEXT.PRINCIPAL_DESC
    },
    {
      label: I18N_TEXT.OTHER_INCOME,
      value: I18N_TEXT.OTHER_INCOME_DESC
    },
    {
      label: I18N_TEXT.INTEREST,
      value: I18N_TEXT.INTEREST_DESC
    },
    {
      label: I18N_TEXT.COSTS,
      value: I18N_TEXT.COSTS_DESC
    }
  ];

  return (
    <>
      <List data={items} />
    </>
  );
};

export default PayoutDetails;
