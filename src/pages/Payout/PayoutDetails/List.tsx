import React from 'react';

// components
import Item, { ItemProps } from './Item';

export interface ListProps {
  data: ItemProps[];
}
const List: React.FC<ListProps> = ({ data }) => {
  return (
    <>
      {data.map((item: ItemProps, index: number) => {
        const { label, value } = item;
        return (
          <Item
            key={`itemDetails-${index}-${new Date().getTime()}`}
            label={label}
            value={value}
          />
        );
      })}
    </>
  );
};

export default List;
