import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface ItemProps {
  label: string;
  value: string;
}

const Item: React.FC<ItemProps> = ({ label, value }) => {
  const { t } = useTranslation();
  const symbol = '•';

  return (
    <div className="d-flex mt-16">
      <span className="mr-12">{symbol}</span>
      <div>
        <p>{t(label)}</p>
        <p className="color-grey mt-4">{t(value)}</p>
      </div>
    </div>
  );
};

export default Item;
