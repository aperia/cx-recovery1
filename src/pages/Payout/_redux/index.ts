import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { getPayout, getPayoutBuilder } from './getPayout';

// types
import { IInitialStatePayout } from './../types';

const initialState: IInitialStatePayout = {};

export const { actions, reducer } = createSlice({
  name: 'payout',
  initialState: initialState,
  reducers: {
    setSelectedThroughDate: (
      draftState: IInitialStatePayout,
      action: PayloadAction<{ storeId: string; selectedThroughDate: RefDataValue }>
    ) => {
      const { storeId, selectedThroughDate } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        selectedThroughDate
      };
    }
  },
  extraReducers: builder => {
    getPayoutBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getPayout
};

export { combineActions as payoutActions };
