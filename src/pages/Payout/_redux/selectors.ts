import { createSelector } from '@reduxjs/toolkit';

// types
import { IPayout, IPayoutState, ISnapshotPayout } from './../types';

const getPayoutState = (state: RootState, storeId: string): IPayoutState => {
  return state.payout[storeId];
};

export const selectIsLoadingPayout = createSelector(
  getPayoutState,
  (data: IPayoutState): boolean => !!data?.isLoading
);

export const selectPayoutData = createSelector(
  getPayoutState,
  (data: IPayoutState): IPayout | undefined => data?.selected
);

export const selectThroughDate = createSelector(
  getPayoutState,
  (data: IPayoutState): RefDataValue | undefined => data?.selectedThroughDate
);

export const selectSnapshotPayout = createSelector(
  getPayoutState,
  (data: IPayoutState): ISnapshotPayout | undefined => data?.snapshot
);
