import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// constants
import { API_ERROR } from 'app/constants';
import { FormatTime } from 'app/constants/enums';

// service
import payoutServices from '../payoutServices';

// types
import { IInitialStatePayout, IPayout } from '../types';

// helpers
import { formatTimeDefault } from 'app/helpers';

export interface IPayloadGetPayout {
  data?: IPayout;
}
export interface IGetPayoutArgs {
  storeId: string;
  groupId: string;
  throughDate?: string;
}

export const getPayout = createAsyncThunk<
  IPayloadGetPayout,
  IGetPayoutArgs,
  ThunkAPIConfig
>('payout/getPayout', async (args, thunkAPI) => {
  const {
    storeId,
    groupId,
    throughDate = formatTimeDefault(new Date(), FormatTime.DateWithHyphen) || ''
  } = args;

  try {
    const response = await payoutServices.getPayout({
      storeId,
      groupId,
      throughDate
    });

    return response.data;
  } catch (error) {
    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  }
});

export const getPayoutBuilder = (
  builder: ActionReducerMapBuilder<IInitialStatePayout>
) => {
  const { pending, fulfilled, rejected } = getPayout;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true,
        isError: false
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: false,
        selected: data,
        snapshot: data
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: true,
        snapshot: undefined
      };
    });
};
