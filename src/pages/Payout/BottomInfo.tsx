import React, { useCallback, useMemo } from 'react';
import { useDispatch } from 'react-redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useSelector, useSelectorId } from 'app/hooks';
import usePrepareSnapshot from './hooks/usePrepareSnapshot';

// selectors
import { selectPayoutData, selectThroughDate } from './_redux/selectors';
import { selectorAccountList } from 'pages/AccountManagement/_redux/accountList/selector';

// actions
import { tabActions } from 'pages/__commons/TabBar/_redux';

// constants
import { I18N_TEXT } from 'app/constants';
import { STORE_ACCOUNT_GROUP_KEY } from 'pages/AccountGroup/constants';

// types
import { IPayout } from './types';
import { IAccounts } from 'pages/AccountManagement/types';

// components
import { Button, Icon, Popover } from 'app/_libraries/_dls/components';
import Table from './Table';
import PayoutDetails from './PayoutDetails';

// helpers
import { numToWords } from 'app/helpers/numberToWords';

export interface BottomInfoProps {
  groupId: string;
}
const BottomInfo: React.FC<BottomInfoProps> = ({ groupId }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { accNumber } = useAccountDetail();

  const accounts: IAccounts[] = useSelector(selectorAccountList);
  const account = accounts.find(
    (item: IAccounts) => item.accNumber.accountNumberUnMask === accNumber
  );

  const { accountNumber } = account || {};

  const data = useSelectorId<IPayout | undefined>(selectPayoutData);
  const { description: throughDateDesc } =
    useSelectorId<RefDataValue | undefined>(selectThroughDate) || {};
  const dataSnapshot = usePrepareSnapshot(data);

  const {
    total_account_balance,
    total_group_balance,
    numberOfAccountInGroup = ''
  } = dataSnapshot;

  const rows = [
    {
      name: 'interestRate',
      label: I18N_TEXT.INTEREST_RATE
    },
    {
      name: 'perDiem',
      label: I18N_TEXT.PER_DIEM
    },
    {
      name: 'principal',
      label: I18N_TEXT.PRINCIPAL
    },
    {
      name: 'otherIncome',
      label: I18N_TEXT.OTHER_INCOME
    },
    {
      name: 'interest',
      label: I18N_TEXT.INTEREST
    },
    {
      name: 'costs',
      label: I18N_TEXT.COSTS
    }
  ];

  const numberOfAccountInGroupInWords = useMemo(() => {
    return `a ${numToWords(numberOfAccountInGroup)}-account group`;
  }, [numberOfAccountInGroup]);

  const handleOpenAccountGroup = useCallback(() => {
    const storeId = `${STORE_ACCOUNT_GROUP_KEY}_${groupId}`;

    dispatch(
      tabActions.addTab({
        title: `${t(I18N_TEXT.ACCOUNT_GROUP_TITLE)} - ${accountNumber}`,
        storeId,
        tabType: 'accountGroup',
        iconName: 'file',
        props: {
          storeId,
          groupId
        }
      })
    );
  }, [accountNumber, dispatch, groupId, t]);

  return (
    <div className="p-24 bg-white overlap-next-action">
      <div className="border br-radius-8">
        <div className="bg-light-l20 br-radius-8">
          <div className="d-flex flex-column align-items-center justify-content-center px-16 pt-22 pb-16 border-bottom">
            <p>
              {t(I18N_TEXT.TOTAL_OUTSTANDING_BALANCE_TODAY, {
                throughDate: throughDateDesc
              })}
            </p>
            <p className="fw-500 fs-16 mt-4">{total_account_balance}</p>
          </div>
        </div>

        <div className="p-16">
          <div className="d-flex align-items-center">
            <h6 className="color-grey mr-8">{t(I18N_TEXT.DETAILS)}</h6>
            <Popover
              placement="right"
              size="lg"
              containerClassName="inside-infobar"
              element={<PayoutDetails />}
              popoverInnerClassName="pt-0 px-16 pb-16"
            >
              <Button size="sm" variant="icon-secondary">
                <Icon name="information" className="color-grey-l08" size="4x" />
              </Button>
            </Popover>
          </div>

          <div className="mt-16">
            <Table>
              {rows.map((row, index) => (
                <Table.Row
                  key={`tableRow-${index}-${new Date().getTime()}`}
                  name={row.name}
                  label={row.label}
                  value={dataSnapshot[row.name as keyof typeof dataSnapshot]}
                />
              ))}
            </Table>
          </div>
        </div>
      </div>

      {!groupId ? null : (
        <div className="mt-16">
          <div>
            <span>
              {t(I18N_TEXT.TOTAL_OUTSTANDING_BALANCE_TODAY_DESCRIPTION_1)}
            </span>

            <a
              className="link text-decoration-none"
              onClick={handleOpenAccountGroup}
            >
              {numberOfAccountInGroupInWords}
            </a>
            <span>
              {t(I18N_TEXT.TOTAL_OUTSTANDING_BALANCE_TODAY_DESCRIPTION_2, {
                totalOutstandingBalanceAmount: total_group_balance
              })}
            </span>
            <span className="fw-500">{total_group_balance}</span>
            <span>
              {t(I18N_TEXT.TOTAL_OUTSTANDING_BALANCE_TODAY_DESCRIPTION_3, {
                throughDate: throughDateDesc
              })}
            </span>
          </div>
        </div>
      )}
    </div>
  );
};

export default BottomInfo;
