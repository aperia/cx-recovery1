export interface IPayout {
  id?: string;
  paymentSchedule: string;
  paymentScheduleAmount: string;
  paymentScheduleStartDate: string;
  numberOfAccountInGroup: string;
  interestRate?: string;
  perDiem?: string;
  principal?: string;
  otherIncome?: string;
  interest?: string;
  costs?: string;
  total_account_balance?: string;
  total_group_balance?: string;
}

export interface ISnapshotPayout extends IPayout {}

export interface IPayoutState {
  isOpen: boolean;
  isLoading: boolean;
  isError: boolean;
  data?: IPayout;
  selected?: IPayout;
  snapshot?: ISnapshotPayout;
  selectedThroughDate: RefDataValue;
}

export interface IInitialStatePayout {
  [storeId: string]: IPayoutState;
}
