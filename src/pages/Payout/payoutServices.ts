import { payoutController } from './__mock__/payoutController';

// types
import { IGetPayoutArgs } from './_redux/getPayout';

const payoutServices = {
  getPayout(args: IGetPayoutArgs) {
    return payoutController.getPayout(args);
  }
};

export default payoutServices;
