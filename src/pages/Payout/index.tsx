import React, { useEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';

// components
import TopInfo from './TopInfo';
import BottomInfo from './BottomInfo';
import SimpleBar from 'simplebar-react';

// actions
import { payoutActions } from './_redux';

export interface PayoutProps {
  storeId: string;
  groupId: string;
}
const Payout: React.FC<PayoutProps> = ({ storeId, groupId }) => {
  const dispatch = useDispatch();
  const isFirstMount = useRef<boolean>(true);

  useEffect(() => {
    if (isFirstMount.current) {
      isFirstMount.current = false;

      dispatch(
        payoutActions.getPayout({
          storeId,
          groupId
        })
      );
    }
  }, [dispatch, storeId, groupId]);

  return (
    <>
      <SimpleBar>
        <TopInfo storeId={storeId} groupId={groupId} />
        <BottomInfo groupId={groupId} />
      </SimpleBar>
    </>
  );
};

export default Payout;
