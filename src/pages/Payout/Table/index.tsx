import If from 'pages/__commons/If';
import React, { ReactNode } from 'react';

// components
import TableRow, { TableRowProps } from './TableRow';

interface StaticComponents {
  Row: React.FC<TableRowProps>;
}

export type ITable<P> = React.FC<P> & StaticComponents;

export interface TableProps {
  children:
    | React.ReactElement<typeof TableRow>[]
    | React.ReactElement<typeof TableRow>;
}

const Table: React.FC<TableProps> = React.memo(({ children }) => {
  const childrenWithExtraProps = React.Children.map<ReactNode, ReactNode>(
    children,
    (child, index) => {
      if (React.isValidElement(child)) {
        return (
          <>
            <If condition={index > 0 && index < React.Children.count(children)}>
              <div className="col-12 py-8">
                <div className="divider-dashed" />
              </div>
            </If>
            {React.cloneElement(child, {
              ...child.props
            } as TableRowProps)}
          </>
        );
      }
    }
  );
  return <div className="row">{childrenWithExtraProps}</div>;
});

const TableExtraStaticProp = Table as ITable<TableProps>;
TableExtraStaticProp.Row = TableRow;

export { TableRow, TableExtraStaticProp as default };
