import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface TableRowProps {
  name: string;
  label: string;
  value?: string;
}
const TableRow: React.FC<TableRowProps> = ({ name, label, value }) => {
  const { t } = useTranslation();

  return (
    <div className="col-12 d-flex justify-content-between">
      <span className="color-secondary fw-500 mr-8">{t(label)}:</span>
      <span className="form-group-static__text">{value}</span>
    </div>
  );
};

export default TableRow;
