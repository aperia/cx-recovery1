// mocks
import { Repository } from 'app/utils/repositories/concretes/Repository';
import payout from './payout.json';

// types
import { IPayout } from '../types';
import { IGetPayoutArgs } from '../_redux/getPayout';

const isHaveEmpty = window.location.href.includes('empty');

const data = [JSON.parse(JSON.stringify(payout))] as IPayout[];
const payoutRepository = new Repository<IPayout>(data);

const getResult = (data: IPayout[]) => {
  let result;

  if (isHaveEmpty) return result;

  if (Array.isArray(data) && data.length) {
    result = data[0];
  }

  return result;
};

export const payoutController = {
  getPayout(args: IGetPayoutArgs) {
    const { data } = payoutRepository.getAll();
    return Promise.resolve({
      data: { success: true, data: getResult(data) }
    });
  }
};
