import React from 'react';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { PSChart } from './PSChart';
import { formatCurrency } from 'app/helpers';
import { useSelector } from 'app/hooks';
import { selectedPaymentScheduled } from '../_redux/selectors';

interface DashboardPaymentScheduledProps {}
export const DashboardPaymentScheduled: React.FC<DashboardPaymentScheduledProps> =
  () => {
    const { t } = useTranslation();
    const paymentScheduled = useSelector(selectedPaymentScheduled);

    const items = {
      paymentRate: Math.round(Math.random() * 100),
      ...paymentScheduled
    };
    return (
      <>
        <h5>{t('txt_payment_scheduled')}</h5>
        <div className="chart-bar">
          <PSChart items={items} accountTitle="Account" amountTitle="Amount" />

          <div className="content-chart-bar">
            <h6 className="d-flex align-items-center">
              {t('txt_payment_rate')}
              <span className="fs-20 fw-500 ml-8 color-black">
                {items.paymentRate}%
              </span>
            </h6>
            <div className="chart-bar-value">
              <div className="d-flex align-items-center">
                <div className="w-25 d-flex">
                  <span className="circle-legend--color1 mr-4"></span>
                  <span className="color-grey fs-12">{t('txt_kept')}</span>
                </div>

                <span className="w-25 text-right">
                  {items?.accountsPaymentScheduledKept}
                </span>
                <span className="w-50 text-right">
                  {formatCurrency(items?.amountPaymentScheduledKept)}
                </span>
              </div>
              <div className="d-flex align-items-center mt-4">
                <div className="w-25 d-flex">
                  <span className="circle-legend--color2 mr-4"></span>
                  <span className="color-grey fs-12">{t('txt_total')}</span>
                </div>

                <span className="w-25 text-right">
                  {items?.accountsPaymentScheduled}
                </span>
                <span className="w-50 text-right">
                  {formatCurrency(items?.amountPaymentScheduled)}
                </span>
              </div>
            </div>
          </div>
        </div>
      </>
    );
  };
