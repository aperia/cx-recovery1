import React, { useCallback } from 'react';
import BarChart from 'app/_libraries/_dls/components/BarChart';
import { formatCurrencyVariant } from '../helpers';

interface PSChartProps {
  items: any;
  accountTitle: string;
  amountTitle: string;
}
interface IDataSetBarChart {
  labels: string;
  dataSet: MagicKeyValue;
  step: number;
  isFormat?: boolean;
}
const callbackFormat = (value: any) => formatCurrencyVariant(value).replace('.0', '');
const callbackValue = (value: any) => '    ' + value;
const formatLabel = (isFormat: boolean) => isFormat ? callbackFormat : callbackValue;

export const PSChart: React.FC<PSChartProps> = ({
  items,
  accountTitle,
  amountTitle
}) => {
  const config = useCallback(
    ({ labels, dataSet, step, isFormat = false }: IDataSetBarChart) => {
      return {
        data: {
          datasets: [
            {
              backgroundColor: '#FCB665',
              borderColor: '#5CBAE6',
              categoryPercentage: 0.35,
              data: [dataSet?.[0]],
              hoverBackgroundColor: '#FCB665',
              label: 'Item 1',
              colorLabel: '#666'
            },
            {
              backgroundColor: '#77D97C',
              borderColor: '#BCE05A',
              categoryPercentage: 0.35,
              data: [dataSet?.[1]],
              hoverBackgroundColor: '#77D97C',
              label: 'Item 2'
            }
          ],
          labels: [labels]
        },
        options: {
          interaction: {
            intersect: false,
            mode: 'point'
          },
          layout: {
            padding: {
              top: 12,
              left: 12,
              right: 12
            }
          },
          plugins: {
            legend: {
              display: false
            },
            title: {
              display: false,
              text: ''
            },
            tooltip: {
              bodyFont: {
                family: '"Open Sans", sans-serif',
                lineHeight: 1.6,
                size: 12,
                weight: '400'
              },
              caretPadding: 0,
              cornerRadius: 8,
              padding: {
                bottom: 4,
                left: 8,
                right: 8,
                top: 4
              }
            }
          },
          responsive: true,
          scales: {
            x: {
              beginAtZero: false,
              grid: {
                display: false,
                borderDash: [1, 10],
                borderColor: '#CCCCD5',
                tickColor: '#CCCCD5',
                tickLength: 8,
                offset: true
              },
              ticks: {
                padding: 6,
                color: '#666',
                font: {
                  family: '"Open Sans", sans-serif',
                  size: 11
                }
              },
              offset: true
            },
            y: {
              beginAtZero: true,
              grid: {
                display: true,
                borderDash: [4],
                borderColor: '#CCCCD5',
                tickColor: '#fff',
                drawBorder: false
              },
              ticks: {
                color: '#A3A3A3',
                font: {
                  family: '"Open Sans", sans-serif',
                  size: 11
                },
                callback: formatLabel(isFormat),
                stepSize: step
              }
            }
          }
        },
        type: 'bar'
      };
    },
    []
  );
  return (
    <>
      <div className="d-flex">
        <BarChart
          className="pr-lg-24 pr-xl-0"
          config={
            config({
              labels: accountTitle,
              dataSet: items?.accountDataSet,
              step: 300
            }) as any
          }
          height={160}
        />
        <BarChart
          className="pl-lg-24 pr-xl-0"
          config={
            config({
              labels: amountTitle,
              dataSet: items?.amountDataSet,
              step: 100000,
              isFormat: true
            }) as any
          }
          height={160}
        />
      </div>
    </>
  );
};
