export const TITLE = {
  TOTAL_ACCOUNTS: 'Total Accounts',
  TOTAL_AMOUNT: 'Total Amount',
  ACCOUNT: 'Account',
  ACCOUNT_WORKED: 'Account Worked',
  AMOUNT: 'Amount',
  PENDING_ACCOUNTS: 'Pending Accounts',
  PENDING_AMOUNT: 'Pending Amount'
};
