import { isString } from 'app/helpers';
export const formatCurrencyVariant = (value: number | string) => {
  const numbValue = isString(value) ? Number(value) : value;
  // 1.000.000 -> $1M
  // 100.000 -> $1K
  if (numbValue < 9999) return `${numbValue}`;
  if (numbValue >= 1000000) {
    return `$${(numbValue / 1000000).toFixed(1)}M`;
  }
  if (numbValue < 1000000 && numbValue >= 99999) {
    return `$${(numbValue / 1000).toFixed(1)}K`;
  }

  return `$${numbValue}`;
};

export const mappingToRefData = (data: any) => {
  return data?.map((item: any, index: number) => ({
    value: item.queueId,
    description: item.queueName
      ? `${item.queueId} - ${item.queueName}`
      : item.queueId,
    show: true
  }));
};

export const prepareRefDataList = (listData: RefDataValue[]) => {
  return [
    {
      group: {
        label: '',
        data: [{ value: 'All', description: 'All', show: true }]
      }
    },
    {
      group: {
        label: '',
        data: listData
      }
    }
  ];
};
