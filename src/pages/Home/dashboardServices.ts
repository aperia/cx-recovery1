import { dashboardController } from "./__mock__/dashboardController";

export const dashboardServices = {
    get: () => {
        return dashboardController.getDashboard();
    }
}