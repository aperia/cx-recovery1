import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { dashboardServices } from '../dashboardServices';
import { InitialStateDashboard } from '../types';

const GET_DASHBOAD = 'getDashboard';

export const getDashboard = createAsyncThunk<any, undefined, ThunkAPIConfig>(
  GET_DASHBOAD,
  async (_, thunkAPI) => {
    try {
      const res = await dashboardServices.get();
     return res.data
    } catch (error) {
      return thunkAPI.rejectWithValue(error);
    }
  }
);

export const getDashboardBuilder = (
  builder: ActionReducerMapBuilder<InitialStateDashboard>
) => {
  const { pending, fulfilled, rejected } = getDashboard;
  builder
    .addCase(pending, draftState => {
      draftState.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      const {dashboard } = action.payload
      draftState.isLoading = false;
      draftState.data = dashboard
    })
    .addCase(rejected, draftState => {
      draftState.isLoading = false;
      draftState.isError = true;
    });
};
