import { createSlice } from '@reduxjs/toolkit';
import {
  DashboardInitialState,
  InitialStateDashboard
} from '../types';
import { getDashboard, getDashboardBuilder } from './getDashboard';

const initialStateDashboard: InitialStateDashboard =
  DashboardInitialState as InitialStateDashboard;

const { actions, reducer } = createSlice({
  name: 'dashboard',
  initialState: initialStateDashboard,
  reducers: {},
  extraReducers: builder => {
    getDashboardBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getDashboard
};

export { combineActions as dashboardActions, reducer };
