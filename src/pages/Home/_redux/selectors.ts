import { createSelector } from '@reduxjs/toolkit';

export const getDataDashboard = (states: RootState) => states.dashboard.data;

export const selectedQueueList = createSelector(
  getDataDashboard,
  (data: MagicKeyValue) => data?.queueList
);

export const selectedWorkStatistics = createSelector(
  getDataDashboard,
  (data: MagicKeyValue) => data?.workStatistics
);

export const selectedPaymentScheduled = createSelector(
  getDataDashboard,
  (data: MagicKeyValue) => data?.paymentScheduled
);
