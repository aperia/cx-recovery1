import dashboard from './dashboard.json';

const dashboardData = JSON.parse(JSON.stringify(dashboard));

export const dashboardController = {
  getDashboard() {
    return Promise.resolve({
      data: {
        dashboard: dashboardData,
        success: true
      }
    });
  }
};
