import React, { useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';

// component
import {
  Button,
  DropdownBase,
  DropdownList,
  SimpleBar
} from 'app/_libraries/_dls/components';
import SearchAccountList from 'pages/AccountManagement/AccountList/SearchAccountList';
import StartQueueModal from 'pages/AccountActivity/StartQueueModal';

// hook
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { tabActions } from 'pages/__commons/TabBar/_redux';

// constant
import { I18N_TEXT } from 'app/constants';
import { STORE_ID_QUEUE_LIST } from 'pages/AccountActivity/constants';
import { dashboardActions } from './_redux';
import { formatTime, isEmpty } from 'app/helpers';
import { TODAY } from 'pages/AgencyManagement/components/SaveAgencyRequestModal/constants';
import { DashboardWorkStatistics } from './DashboardWorkStatistics';
import { DashboardPaymentScheduled } from './DashboardPaymentScheduled';
import useRefData from 'app/hooks/useRefData';
import { useSelector } from 'app/hooks';
import { selectedQueueList } from './_redux/selectors';
import { generateDescription } from 'app/components';
import QueueList from 'pages/AccountActivity/QueueList';
import { mappingToRefData, prepareRefDataList } from './helpers';

export interface HomeProps {}

const Home: React.FC<HomeProps> = () => {
  const [queueValue, setQueueValue] = useState<RefDataValue>(
    {} as RefDataValue
  );

  const [periodValue, setPeriodValue] = useState<RefDataValue>(
    {} as RefDataValue
  );
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const queueList = useSelector(selectedQueueList);

  const genQueueListOption = useMemo(() => {
    const data = mappingToRefData(queueList);
    return prepareRefDataList(data);
  }, [queueList]);

  const periodOptions: RefDataValue[] = useRefData('period');

  const handleOpenQueueList = () => {
    dispatch(
      tabActions.addTab({
        title: t(I18N_TEXT.QUEUE_LIST),
        storeId: STORE_ID_QUEUE_LIST,
        tabType: 'queueList',
        iconName: 'file'
      })
    );
  };

  const renderItemDropdownList = (data: RefDataValue[], isCombine = false) => {
    if (isEmpty(data)) return <></>;

    return data.map((item: RefDataValue) => {
      if (item['group']) {
        const { data = [] } = item['group'];
        if (!Array.isArray(data)) return <></>;
        return (
          <DropdownBase.Group label="">
            {data.map(option => (
              <DropdownBase.Item
                className="mt-6"
                value={option}
                label={option.description}
                key={option.value}
              />
            ))}
          </DropdownBase.Group>
        );
      }

      const dropDownLbl = generateDescription(item, isCombine);
      return (
        <DropdownList.Item label={dropDownLbl} key={item.value} value={item} />
      );
    });
  };

  const handleChangePeriod = (event: DropdownBaseChangeEvent) => {
    setPeriodValue(event.target.value);
  };

  const handleChangeQueueValue = (event: DropdownBaseChangeEvent) => {
    setQueueValue(event.target.value);
  };

  useEffect(() => {
    dispatch(dashboardActions.getDashboard());
  }, [dispatch]);

  useEffect(() => {
    if (!isEmpty(periodOptions)) {
      setPeriodValue(periodOptions?.[0]);
    }
  }, [periodOptions]);

  useEffect(() => {
    if (!isEmpty(genQueueListOption)) {
      setQueueValue(genQueueListOption?.[0]?.group?.data?.[0]);
    }
  }, [genQueueListOption]);

  return (
    <SimpleBar className="bg-light-l20">
      <div className="bg-white border-bottom p-24">
        <div className="d-flex justify-content-center">
          <SearchAccountList small={false} screen="Home" />
        </div>
      </div>
      <div className="bg-white p-24">
        <div className="max-width-lg mx-auto">
          <h3 className="text-center">{t(I18N_TEXT.DASHBOARD)}</h3>
          <p className="text-center mt-8 color-grey">
            {t(I18N_TEXT.LAST_UPDATED, { dateValue: formatTime(TODAY)?.date })}
          </p>
          <div className="d-flex justify-content-between my-24">
            <h4>{t('txt_collector_account_summary')}</h4>
            <div className="d-flex align-items-center mr-n8">
              <p className="color-grey fw-500 mr-4">{t('txt_queue')}:</p>
              <DropdownList
                value={queueValue}
                textField="description"
                onChange={e => handleChangeQueueValue(e)}
                variant="no-border"
                searchBar
              >
                {renderItemDropdownList(genQueueListOption as RefDataValue[])}
              </DropdownList>
            </div>
          </div>
          <div className="card p-24 bg-light-l20 br-radius-8">
            <div className="d-flex justify-content-between">
              <h5>
                {queueValue?.value === 'All'
                  ? t('txt_all_queues')
                  : queueValue.description}
              </h5>
              <div className="d-flex align-items-center mr-n8">
                <p className="color-grey fw-500 mr-4">{t('txt_period')}:</p>
                <DropdownList
                  value={periodValue}
                  textField="description"
                  variant={'no-border'}
                  onChange={e => handleChangePeriod(e)}
                >
                  {renderItemDropdownList(periodOptions)}
                </DropdownList>
              </div>
            </div>
            <div className="d-flex flex-wrap mt-16">
              <div className="w-xl-60 w-md-100">
                <div className="card p-24 br-radius-8">
                  <DashboardWorkStatistics
                    queueList={queueList}
                    queueValue={queueValue?.value}
                  />
                </div>
              </div>
              <div className="w-xl-40  w-md-100 pl-xl-24 pl-md-0 mt-md-24 mt-xl-0">
                <div className="card p-24 br-radius-8">
                  <DashboardPaymentScheduled />
                </div>
              </div>
            </div>
          </div>

          <div className="d-flex justify-content-between align-items-center mt-24">
            <h4>{t(I18N_TEXT.WORK_QUEUE)}</h4>
            <div>
              <Button
                size="sm"
                className="mr-n8"
                variant="outline-primary"
                onClick={() => handleOpenQueueList()}
              >
                {t(I18N_TEXT.VIEW_ALL_QUEUES)}
              </Button>
            </div>
          </div>
        </div>
        <QueueList screen="Home" />
      </div>
      <StartQueueModal />
    </SimpleBar>
  );
};

export default Home;
