export interface IDashboard {
  data: MagicKeyValue;
  isLoading: boolean;
  isError: boolean;
}

export interface InitialStateDashboard extends IDashboard {}

export const DashboardInitialState = {
  isLoading: false,
  isError: false,
  data: {}
};
