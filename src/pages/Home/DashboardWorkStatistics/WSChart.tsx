import { formatCurrency, formatQuantity } from 'app/helpers';
import {
  DoughnutChart,
  DoughnutChartData
} from 'app/_libraries/_dls/components';
import React, { useMemo } from 'react';
import { TITLE } from '../constants';
import { formatCurrencyVariant } from '../helpers';

interface WSChartProps {
  accountTitle: string;
  amountTitle: string;
  items: MagicKeyValue;
  type: string;
}

export const WSChart: React.FC<WSChartProps> = ({
  accountTitle,
  amountTitle,
  items,
  type
}) => {
  const dynamicProps: Record<string, any> = useMemo(() => {
    return {
      ['account']: {
        accountPending: items?.accountPending,
        accountWorked: items?.accountWorked,
        totalAccounts: items?.totalAccounts,
        amountPending: items?.amountPending,
        amountWorked: items?.amountWorked,
        totalAmount: items?.totalAmount
      }
    };
  }, [
    items?.accountPending,
    items?.accountWorked,
    items?.amountPending,
    items?.amountWorked,
    items?.totalAccounts,
    items?.totalAmount
  ]);

  const accountData = useMemo(() => {
    return {
      labels: [accountTitle, TITLE.PENDING_ACCOUNTS],
      datasets: [
        {
          label: TITLE.ACCOUNT,
          data: [
            Number(items?.accountWorked),
            Number(items?.accountPending) +
              Number(dynamicProps[type]?.accountWorked)
          ],
          backgroundColor: ['#AAEAF2', '#5CBAE6'],
          hoverBackgroundColor: ['#AAEAF2', '#5CBAE6'],
          hoverBorderColor: ['#fff', '#fff'],
          borderColor: ['#fff', '#fff']
        }
      ],
      center: {
        label: TITLE.TOTAL_ACCOUNTS.toUpperCase(),
        value: dynamicProps[type]?.totalAccounts
      }
    } as DoughnutChartData;
  }, [
    accountTitle,
    dynamicProps,
    items?.accountPending,
    items?.accountWorked,
    type
  ]);

  const amountData = useMemo(() => {
    return {
      labels: [amountTitle, TITLE.PENDING_AMOUNT],
      datasets: [
        {
          label: TITLE.AMOUNT,
          data: [
            Number(dynamicProps[type]?.amountWorked),
            Number(dynamicProps[type]?.amountPending) +
              Number(dynamicProps[type]?.amountWorked)
          ],
          backgroundColor: ['#D977C4', '#FFB3B3'],
          hoverBackgroundColor: ['#D977C4', '#FFB3B3'],
          hoverBorderColor: ['#fff', '#fff'],
          borderColor: ['#fff', '#fff']
        }
      ],
      center: {
        label: TITLE.TOTAL_AMOUNT.toUpperCase(),
        value: formatCurrencyVariant(dynamicProps[type]?.totalAmount)
      }
    } as DoughnutChartData;
  }, [amountTitle, dynamicProps, type]);

  return (
    <>
      <div className="d-flex justify-content-between">
        <DoughnutChart
          chartData={accountData}
          legendValueRender={value => {
            return isNaN(value) ? '0' : formatQuantity(value.toString());
          }}
          legendLabelRender={label => {
            return `${label}`;
          }}
        />
        <div className="border-right"></div>

        <DoughnutChart
          chartData={amountData}
          legendValueRender={value => {
            return isNaN(value) ? '0' : formatCurrency(value.toString());
          }}
          legendLabelRender={label => {
            return `${label}`;
          }}
        />
      </div>
    </>
  );
};
