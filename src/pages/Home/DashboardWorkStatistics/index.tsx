import React, { useMemo } from 'react';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { WSChart } from './WSChart';
import { useSelector } from 'app/hooks';
import { selectedWorkStatistics } from '../_redux/selectors';

interface DashboardWorkStatisticsProps {
  queueList: MagicKeyValue;
  queueValue: string;
}
export const DashboardWorkStatistics: React.FC<DashboardWorkStatisticsProps> =
  ({ queueList = [], queueValue = '' }) => {
    const { t } = useTranslation();
    const workStatistics = useSelector(selectedWorkStatistics);

    const infoQueue = useMemo(() => {
      if (queueValue === 'All') return {};
      return queueList?.find(
        (queue: MagicKeyValue) => queue.queueId === queueValue
      );
    }, [queueList, queueValue]);
    const items =
      queueValue === 'All'
        ? {
            accountPending: workStatistics?.pendingAccounts,
            accountWorked: workStatistics?.accountsWorked,
            amountPending: workStatistics?.pendingAmount,
            amountWorked: workStatistics?.amountWorked,
            workRate: workStatistics?.workRate,
            totalQueue: queueList?.length,
            totalAccounts: workStatistics?.totalAccounts,
            totalAmount: workStatistics?.totalAmount
          }
        : {
            accountPending: infoQueue?.pendingAccounts,
            accountWorked: infoQueue?.accountsWorked,
            amountPending: infoQueue?.pendingAmount,
            amountWorked: infoQueue?.amountWorked,
            workRate: infoQueue?.workRate,
            totalQueue: infoQueue?.totalQueue,
            totalAccounts: infoQueue?.totalAccounts,
            totalAmount: infoQueue?.totalAmount
          };
    return (
      <>
        <h5 className="mb-24">{t('txt_work_statistics')}</h5>
        <div className="w-md-100 w-lg-80 w-xl-100 mx-md-auto">
          <WSChart
            items={items}
            accountTitle={t('txt_account_worked')}
            amountTitle={t('txt_amount_worked')}
            type="account"
          />
        </div>

        <div className="d-flex justify-content-center align-items-center mt-32">
          <h6 className="d-flex align-items-center color-grey mr-24">
            {t('txt_work_rate')}
            <span className="fs-20 fw-500 color-black ml-8">
              {items.workRate}%
            </span>
          </h6>
          <h6 className="d-flex  align-items-center color-grey">
            {t('txt_total_queues')}
            <span className="fs-20 fw-500 color-black ml-8">
              {items.totalQueue}
            </span>
          </h6>
        </div>
      </>
    );
  };
