export const GET_SECTIONS = 'agencyManagement/getSections';

export interface IAgencyManagementState {
  sections: RefDataValue[];
  loading: boolean;
}

export interface IPayloadGetSections {
  sections: RefDataValue[];
}

export interface ISectionItem {
  id: string;
  title: string;
  Component: React.FC<unknown>;
}

export interface InforDebtor {
  accNumber: string;
  ssn: string;
}

export enum FORM_TYPE {
  AssignForm = 0,
  RecallForm
}