import { I18N_TEXT } from 'app/constants';
import { stringValidate } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { ChangeEvent } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { agencyAssignRequestActions } from '../AgencyAssignRequest/_redux';
import { selectedSearchValue as selectedAssignSearchValue } from '../AgencyAssignRequest/_redux/selectors';
import { agencyRecallRequestActions } from '../AgencyRecallRequest/_redux';
import { selectedSearchValue as selectedRecallSearchValue } from '../AgencyRecallRequest/_redux/selectors';
import { FORM_TYPE } from '../types';

const SEARCH_DATA = {
  [FORM_TYPE[0]]: {
    actions: agencyAssignRequestActions,
    selector: selectedAssignSearchValue,
    placeholder: I18N_TEXT.TYPE_TO_SEARCH_BY_SAVE_NUMBER
  },
  [FORM_TYPE[1]]: {
    actions: agencyRecallRequestActions,
    selector: selectedRecallSearchValue,
    placeholder: I18N_TEXT.TYPE_TO_SEARCH_BY_SAVE_NUMBER
  }
};

export const useSearchTextEvent = (formType = FORM_TYPE[0]) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const actions = SEARCH_DATA[formType].actions;
  const searchValue = useSelector(SEARCH_DATA[formType].selector);
  const searchPlaceholder = t(SEARCH_DATA[formType].placeholder);

  const handleSearch = () => {
    dispatch(actions.setFilterValue());
  };

  const handleClear = () => {
    dispatch(actions.clearSearchInput());
  };

  const handleChange = (e: ChangeEvent<HTMLInputElement>) => {
    const value = e.target.value;
    if (stringValidate(value).isNumber()) {
      dispatch(actions.changeSearchValue(value));
    }
  };

  return {
    searchValue,
    searchPlaceholder,
    handleSearch,
    handleChange,
    handleClear
  };
};
