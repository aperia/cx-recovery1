import * as Yup from 'yup';
import { batch, useDispatch, useSelector } from 'react-redux';
import { agencyAssignRequestActions } from '../AgencyAssignRequest/_redux';
import { agencyRecallRequestActions } from '../AgencyRecallRequest/_redux';

import {
  selectedFormOpen as formOpenAssignRequest,
  selectedFormValues as formValuesAssignRequest,
  selectInlineErrMsg as inlineErrMsgAR,
  selectInlineErrMsgDelete as inlineErrMsgDeleteAR
} from '../AgencyAssignRequest/_redux/selectors';

import {
  selectedFormAction,
  selectedFormOpen as formOpenRecallRequest,
  selectedFormValues as formValuesRecallRequest,
  selectInlineErrMsg as inlineErrMsgRR,
  selectInlineErrMsgDelete as inlineErrMsgDeleteRR
} from '../AgencyRecallRequest/_redux/selectors';

import { FORM_NAMES } from 'pages/AgencyManagement/components/SaveAgencyRequestModal/constants';

import { IAssignRequest } from '../AgencyAssignRequest/types';
import { IRecallRequest } from '../AgencyRecallRequest/types';
import { FORM_TYPE } from '../types';

export const useSaveAssignRequestForm = (formType = FORM_TYPE[0]) => {
  const dispatch = useDispatch();
  const openAssignRequest: boolean = useSelector(formOpenAssignRequest);
  const openRecallRequest: boolean = useSelector(formOpenRecallRequest);

  const valuesAssignRequest: IAssignRequest = useSelector(
    formValuesAssignRequest
  );
  const valuesRecallRequest: IRecallRequest = useSelector(
    formValuesRecallRequest
  );

  const inlineErrMsgAssignRequest = useSelector(inlineErrMsgAR);
  const inlineErrMsgRecallRequest = useSelector(inlineErrMsgRR);

  const inlineErrMsgDeleteAssignRequest = useSelector(inlineErrMsgDeleteAR);
  const inlineErrMsgDeleteRecallRequest = useSelector(inlineErrMsgDeleteRR);

  const formActionRecall: string = useSelector(selectedFormAction);

  const validationSchema = Yup.object().shape({
    [FORM_NAMES.saveNumber.name]: Yup.string().required(
      'txt_save_number_is_required'
    ),
    [FORM_NAMES.nextRunDate.name]: Yup.string().required(
      'txt_next_run_date_is_required'
    ),
    [FORM_NAMES.frequency.name]: Yup.string().required(
      'txt_frequency_is_required'
    ),
    [FORM_NAMES.count.name]: Yup.string().when(FORM_NAMES.frequency.name, {
      is: 'Custom',
      then: Yup.string().required('txt_count_is_required')
    }),
    [FORM_NAMES.reportOnly.name]: Yup.string().required(
      'txt_report_only_is_required'
    )
  });

  const onSubmit = (values: IAssignRequest | IRecallRequest) => {
    if (formType === FORM_TYPE[0]) {
      dispatch(
        agencyAssignRequestActions.saveAssignRequest({
          assignRequest: values
        })
      );
    }
    if (formType === FORM_TYPE[1]) {
      if (formActionRecall === 'ADD') {
        dispatch(
          agencyRecallRequestActions.addRecallRequest({
            newRecall: values
          })
        );
      }
      if (formActionRecall === 'EDIT') {
        dispatch(
          agencyRecallRequestActions.editRecallRequest({
            id: values.id || '',
            dataChange: values
          })
        );
      }
    }
  };

  const onCancel = () => {
    if (formType === FORM_TYPE[0]) {
      batch(() => {
        dispatch(agencyAssignRequestActions.resetFormValues());
        onToggleForm();
      });
    }
    if (formType === FORM_TYPE[1]) {
      batch(() => {
        dispatch(agencyRecallRequestActions.resetFormValues());
        onToggleForm();
      });
    }
  };

  const onToggleForm = () => {
    if (formType === FORM_TYPE[0]) {
      dispatch(agencyAssignRequestActions.toggleForm());
    }
    if (formType === FORM_TYPE[1]) {
      dispatch(agencyRecallRequestActions.toggleForm());
    }
  };

  return {
    open: formType === FORM_TYPE[0] ? openAssignRequest : openRecallRequest,
    values:
      formType === FORM_TYPE[0] ? valuesAssignRequest : valuesRecallRequest,
    inlineErrMsg:
      formType === FORM_TYPE[0]
        ? inlineErrMsgAssignRequest
        : inlineErrMsgRecallRequest,
    inlineErrMsgDelete:
      formType === FORM_TYPE[0]
        ? inlineErrMsgDeleteAssignRequest
        : inlineErrMsgDeleteRecallRequest,
    validationSchema,
    onSubmit,
    onCancel,
    onToggleForm
  };
};
