import { apiService } from 'app/utils/api.service';

const agencyManagementServices = {
  getSections() {
    const url = window.appConfig.api.agencyManagement.getSections;
    return apiService.get(url);
  },
  getSaveNumbers() {
    const url = window.appConfig.api.assignRequest.getSaveNumbers;
    return apiService.get(url);
  }
};

export default agencyManagementServices;
