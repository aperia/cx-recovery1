import React, {
  Fragment,
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef
} from 'react';
import {
  Button,
  InlineMessage,
  SimpleBar,
  TextSearch,
  Tooltip
} from 'app/_libraries/_dls/components';
import AssignRequestGrid from './AssignRequestGrid';
import ApiErrorDetail from 'pages/__commons/ApiErrorDetail';
import SaveAgencyRequestModal, {
  FormAction
} from 'pages/AgencyManagement/components/SaveAgencyRequestModal';

// constants & types
import { I18N_TEXT } from 'app/constants';
import { EXAMPLE_INLINE_MESSAGE } from './constants';
import { IAssignRequest } from './types';
import { FORM_TYPE } from '../types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSaveAssignRequestForm } from '../hooks/useSaveAssignRequestForm';
import { useSearchTextEvent } from '../hooks/useSearchTextEvent';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';

// helpers
import { genAmtId } from 'app/_libraries/_dls/utils';
import classNames from 'classnames';
import { isEmpty, isUndefined } from 'app/helpers';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { agencyAssignRequestActions } from './_redux';
import {
  selectedAssignRequestData,
  selectedAssignRequestLoading,
  selectedFilterValue,
  selectedFormAction
} from './_redux/selectors';
import { deleteAssignRequest } from './_redux/deleteAssignRequest';

const AgencyAssignRequest: React.FC = () => {
  const dataTestId = 'agencyAssignRequest';
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const loading: boolean = useSelector(selectedAssignRequestLoading);
  const data: IAssignRequest[] = useSelector(selectedAssignRequestData);
  const filterValue: string = useSelector(selectedFilterValue);
  const formAction: FormAction = useSelector(selectedFormAction);
  const isEmptyData: boolean = isEmpty(data) && isEmpty(filterValue);
  const divRef = useRef<HTMLDivElement | null>(null);

  const { data: saveNumberData } = useGetRefDataQuery('saveNumber');
  const disableAddBtn = isEmpty(
    saveNumberData?.saveNumber.filter(num =>
      isUndefined(data.find(item => item.saveNumber === num.value))
    ) as RefDataValue[]
  );

  const handleClearAndReset = useCallback(() => {
    dispatch(agencyAssignRequestActions.resetSearch());
  }, [dispatch]);

  const { open, values, validationSchema, onToggleForm, onSubmit, onCancel } =
    useSaveAssignRequestForm(FORM_TYPE[0]);

  const GroupButton = useMemo(
    () => (
      <div className="d-flex mr-n8">
        {filterValue && !isEmpty(data) ? (
          <Button
            variant="outline-primary"
            size="sm"
            onClick={handleClearAndReset}
          >
            {t(I18N_TEXT.CLEAR_AND_RESET)}
          </Button>
        ) : null}
        {disableAddBtn ? (
          <Tooltip
            placement="top-end"
            variant="default"
            element={t(I18N_TEXT.ALL_SAVE_NUMBERS_ARE_ADDED)}
          >
            <Button variant="outline-primary" size="sm" disabled>
              {t(I18N_TEXT.ADD_ASSIGN_REQUEST)}
            </Button>
          </Tooltip>
        ) : (
          <Button variant="outline-primary" size="sm" onClick={onToggleForm}>
            {t(I18N_TEXT.ADD_ASSIGN_REQUEST)}
          </Button>
        )}
      </div>
    ),
    [filterValue, data, handleClearAndReset, t, onToggleForm, disableAddBtn]
  );

  const {
    searchValue,
    searchPlaceholder,
    handleChange,
    handleClear,
    handleSearch
  } = useSearchTextEvent();

  useEffect(() => {
    dispatch(agencyAssignRequestActions.getAssignRequests());
    return () => {
      dispatch(agencyAssignRequestActions.removeStore());
    };
  }, [dispatch]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100% - 81px)`);
  }, []);

  return (
    <div ref={divRef}>
      <SimpleBar>
        <div
          className={classNames('px-24 py-22 max-width-lg mx-auto', {
            loading
          })}
        >
          <ApiErrorDetail
            forAction={[deleteAssignRequest.rejected.type]}
            className="mb-24"
          />
          {!isEmptyData ? (
            <Fragment>
              <div className="header-agency-request">
                <h5
                  className="header-title"
                  data-testid={genAmtId(dataTestId, 'title', '')}
                >
                  {t(I18N_TEXT.ASSIGN_REQUEST_LIST)}
                </h5>
                <div className="header-message">
                  <InlineMessage className="mb-0" variant="warning">
                    <span>{EXAMPLE_INLINE_MESSAGE}</span>
                  </InlineMessage>
                </div>
                <div className="header-search">
                  <TextSearch
                    value={searchValue}
                    placeholder={searchPlaceholder}
                    onChange={handleChange}
                    onSearch={handleSearch}
                    onClear={handleClear}
                  />
                </div>
                <div className="header-group-button">{GroupButton}</div>
              </div>
              <AssignRequestGrid />
            </Fragment>
          ) : (
            <Fragment>
              <h5 data-testid={genAmtId(dataTestId, 'title', '')}>
                {t(I18N_TEXT.ASSIGN_REQUEST_LIST)}
              </h5>
              <AssignRequestGrid onAdd={onToggleForm} />
            </Fragment>
          )}
          <SaveAgencyRequestModal
            open={open}
            data={data}
            value={values}
            title={
              values.id
                ? t(I18N_TEXT.EDIT_AGENCY_ASSIGN_REQUEST)
                : t(I18N_TEXT.ADD_AGENCY_ASSIGN_REQUEST)
            }
            action={formAction}
            target="assignRequest"
            validationSchema={validationSchema}
            onCancel={onCancel}
            onSubmit={values => onSubmit(values as IAssignRequest)}
          />
        </div>
      </SimpleBar>
    </div>
  );
};

export default AgencyAssignRequest;
