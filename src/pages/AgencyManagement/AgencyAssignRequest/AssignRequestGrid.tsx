import React, { Fragment, useCallback, useMemo } from 'react';
import {
  Button,
  ColumnType,
  Grid,
  SimpleBar,
  SortType
} from 'app/_libraries/_dls/components';
import { NoDataGrid, SearchNodata } from 'app/components';
import AgencyAssignGridActions from './Actions';
import LoadMoreAssign from './LoadMoreAssign';

// constants
import { I18N_TEXT } from 'app/constants';
import { IAssignRequest } from './types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { isEmpty } from 'app/helpers';

// redux
import { useDispatch, useSelector } from 'react-redux';
import {
  selectedAssignRequestData,
  selectedAssignRequestLoading,
  selectedAssignRequestSortBy,
  selectedFilterValue
} from './_redux/selectors';
import { agencyAssignRequestActions } from './_redux';
import { formatFrequency } from '../helpers';

interface IAssignRequestGrid {
  onAdd?: () => void;
}

const AssignRequestGrid: React.FC<IAssignRequestGrid> = props => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const assignRequestData: IAssignRequest[] = useSelector(
    selectedAssignRequestData
  );
  const assignRequestLoading: boolean = useSelector(
    selectedAssignRequestLoading
  );
  const filterValue: string = useSelector(selectedFilterValue);
  const sortBy: SortType = useSelector(selectedAssignRequestSortBy);

  const handleClear = useCallback(() => {
    dispatch(agencyAssignRequestActions.resetSearch());
  }, [dispatch]);

  const ASSIGN_REQUEST_LIST_COLUMNS: ColumnType<IAssignRequest>[] = useMemo(
    () => [
      {
        id: 'saveNumber',
        Header: t(I18N_TEXT.SAVE_NUMBER),
        isSort: true,
        width: 124,
        accessor: 'saveNumber'
      },
      {
        id: 'description',
        Header: t(I18N_TEXT.DESCRIPTION),
        isSort: true,
        width: 230,
        accessor: 'description'
      },
      {
        id: 'lastRunDate',
        Header: t(I18N_TEXT.LAST_RUN_DATE),
        isSort: true,
        width: 130,
        accessor: 'lastRunDate'
      },
      {
        id: 'nextRunDate',
        Header: t(I18N_TEXT.NEXT_RUN_DATE),
        isSort: true,
        width: 130,
        accessor: 'nextRunDate'
      },
      {
        id: 'frequency',
        Header: t(I18N_TEXT.FREQUENCY),
        isSort: true,
        width: 110,
        accessor: (data: IAssignRequest) => t(formatFrequency(data))
      },
      {
        id: 'reportOnly',
        Header: t(I18N_TEXT.REPORT_ONLY),
        isSort: true,
        width: 315,
        accessor: 'reportOnly'
      },
      {
        id: 'type',
        Header: t(I18N_TEXT.TYPE),
        isSort: true,
        width: 150,
        accessor: 'type'
      },
      {
        id: 'actions',
        Header: t(I18N_TEXT.ACTIONS),
        isSort: false,
        width: 134,
        isFixedRight: true,
        accessor: (data: IAssignRequest) => (
          <AgencyAssignGridActions {...data} />
        ),
        className: 'text-center td-sm'
      }
    ],
    [t]
  );

  const handleSortChange = useCallback(
    (sortType: SortType) => {
      dispatch(agencyAssignRequestActions.setSortType(sortType));
    },
    [dispatch]
  );

  const DataGridView = useMemo(() => {
    if (assignRequestLoading) return null;

    if (isEmpty(assignRequestData)) {
      if (!isEmpty(filterValue))
        return <SearchNodata onClearSearch={handleClear} />;
      return (
        <NoDataGrid text={t(I18N_TEXT.NO_REQUESTS_TO_DISPLAY)}>
          <Button variant="outline-primary" onClick={props?.onAdd}>
            {t(I18N_TEXT.ADD_ASSIGN_REQUEST)}
          </Button>
        </NoDataGrid>
      );
    }

    return (
      <Fragment>
        <Grid
          sortBy={[sortBy]}
          onSortChange={handleSortChange}
          className="mt-16"
          columns={ASSIGN_REQUEST_LIST_COLUMNS}
          data={assignRequestData}
        />
        <LoadMoreAssign />
      </Fragment>
    );
  }, [
    ASSIGN_REQUEST_LIST_COLUMNS,
    assignRequestData,
    assignRequestLoading,
    filterValue,
    handleClear,
    handleSortChange,
    props?.onAdd,
    sortBy,
    t
  ]);

  return <SimpleBar>{DataGridView}</SimpleBar>;
};

export default AssignRequestGrid;
