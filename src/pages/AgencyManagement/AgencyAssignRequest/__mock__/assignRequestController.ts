import { IAssignRequest } from '../types';
import assignRequestsJson from './assignRequests.json';

let assignRequestsData: IAssignRequest[] = JSON.parse(
  JSON.stringify(assignRequestsJson)
);
const isHaveError = window.location.href.includes('error');

export const assignRequestController = {
  getAssignRequests({
    startSequence,
    endSequence
  }: {
    startSequence: number;
    endSequence: number;
  }) {
    const assignData = assignRequestsData.slice(startSequence - 1, endSequence);
    return Promise.resolve({
      data: { success: true, assignRequests: assignData }
    });
  },
  saveAssignRequest(saveItem: IAssignRequest) {
    try {
      if (isHaveError)
      return Promise.reject({
        success: false,
        message: saveItem.id ? 'EDIT' : 'ADD'
      });

      if (saveItem.id) {
        const editIndex = assignRequestsData.findIndex(
          item => item.id === saveItem.id
        );
        if (editIndex === -1) throw Error('Item is not exist');
        assignRequestsData = [
          ...assignRequestsData.slice(0, editIndex),
          saveItem,
          ...assignRequestsData.slice(editIndex + 1)
        ];
      } else {
        assignRequestsData = [
          ...assignRequestsData,
          { ...saveItem, id: new Date().getTime().toString() }
        ];
      }
      return Promise.resolve({
        data: {
          success: true
        }
      });
    } catch (error) {
      return Promise.reject({
        data: {
          success: false,
          message: error
        }
      });
    }
  },
  deleteAssignRequest(id: string) {
    try {
      if (isHaveError)
      return Promise.reject({
        success: false,
        message: 'DELETE FAILED'
      });

      assignRequestsData = assignRequestsData.filter(
        (item: any) => item.id !== id
      );
      return Promise.resolve({ success: true, data: assignRequestsData });
    } catch (error) {
      return Promise.reject({
        success: false,
        message: 'Delete assign failed !'
      });
    }
  }
};
