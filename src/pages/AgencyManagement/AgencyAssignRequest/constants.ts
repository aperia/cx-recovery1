import { IAssignRequest } from './types';

export const EXAMPLE_INLINE_MESSAGE =
  'Requests made or modified after 5:00 PM CT with Next Run Date on the same day will run the next day.';

export const SAVE_ASSIGN_REQUEST = 'agencyAssignRequest/saveAssignRequest';
export const DELETE_ASSIGN_REQUEST = 'agencyAssignRequest/deleteAssignRequest';

export const DEFAULT_ASSIGN_REQUEST: IAssignRequest = {
  saveNumber: '',
  lastRunDate: '',
  nextRunDate: new Date().toString(),
  frequency: '',
  reportOnly: ''
};
