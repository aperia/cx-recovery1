import React from 'react';

// components
import LoadMore from 'app/components/LoadMore';

// redux
import { useDispatch, useSelector } from 'react-redux';

// helper
import { agencyAssignRequestActions } from './_redux';
import { selectorIsEnd, selectorIsLoadingLoadMore } from './_redux/selectors';

export interface LoadMoreAssignProps {}

const LoadMoreAssign: React.FC<LoadMoreAssignProps> = () => {
  const dispatch = useDispatch();
  const isEnd = useSelector(selectorIsEnd);
  const isLoading = useSelector(selectorIsLoadingLoadMore);

  const handleOnLoadMore = () => {
    dispatch(agencyAssignRequestActions.getAssignRequests());
  };

  return (
    <LoadMore
      loading={isLoading}
      onLoadMore={handleOnLoadMore}
      isEnd={isEnd}
    />
  );
};

export default LoadMoreAssign;
