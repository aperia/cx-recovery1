import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { checkEmpty, isArrayHasValue } from 'app/helpers';
import {
  IPayloadGetAssignRequests,
  GET_ASSIGN_REQUESTS,
  IAgencyAssignRequestState
} from '../types';
import { assignRequestController } from '../__mock__/assignRequestController';

const STEP_SEQUENCE = 25;
export const getAssignRequests = createAsyncThunk<
  IPayloadGetAssignRequests,
  { isRefresh: boolean } | undefined,
  ThunkAPIConfig
>(GET_ASSIGN_REQUESTS, async (args, thunkAPI) => {
  const { agencyAssignRequest } = thunkAPI.getState();
  const { startSequence, endSequence } = agencyAssignRequest;
  const { data } = await assignRequestController.getAssignRequests({
    startSequence: checkEmpty([args?.isRefresh, startSequence, 1]),
    endSequence: checkEmpty([args?.isRefresh, endSequence, STEP_SEQUENCE])
  });
  return {
    assignRequests: data.assignRequests
  };
});

export const getAssignRequestsBuilder = (
  builder: ActionReducerMapBuilder<IAgencyAssignRequestState>
) => {
  const { pending, fulfilled, rejected } = getAssignRequests;
  builder
    .addCase(pending, (draftState, action) => {
      if (draftState.startSequence === 1 || action.meta.arg?.isRefresh) {
        draftState.loading = true;
      } else {
        draftState.isLoadingLoadMore = true;
      }
    })
    .addCase(fulfilled, (draftState, action) => {
      const isRefresh = action.meta.arg?.isRefresh;
      if (draftState.startSequence === 1 || isRefresh) {
        draftState.loading = false;
      } else {
        draftState.isLoadingLoadMore = false;
      }
      draftState.isEnd = !isArrayHasValue(action.payload.assignRequests);
      draftState.data = checkEmpty([
        isRefresh,
        [...draftState.data, ...action.payload.assignRequests],
        action.payload.assignRequests
      ]);
      draftState.endSequence = checkEmpty([
        isRefresh,
        draftState.endSequence + STEP_SEQUENCE - 1,
        2 * STEP_SEQUENCE - 1
      ]);
      draftState.startSequence = checkEmpty([
        isRefresh,
        draftState.startSequence + STEP_SEQUENCE,
        STEP_SEQUENCE + 1
      ]);
    })
    .addCase(rejected, draftState => {
      draftState.loading = false;
    });
};
