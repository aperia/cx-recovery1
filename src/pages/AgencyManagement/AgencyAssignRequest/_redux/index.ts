import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { DEFAULT_ASSIGN_REQUEST } from '../constants';
import { IAgencyAssignRequestState, IAssignRequest } from '../types';
import {
  getAssignRequests,
  getAssignRequestsBuilder
} from './getAssignRequests';
import {
  saveAssignRequest,
  saveAssignRequestBuilder
} from './saveAssignRequest';
import {
  deleteAssignRequest,
  deleteAssignRequestBuilder
} from './deleteAssignRequest';
import { SortType } from 'app/_libraries/_dls/components';

const defaultSortBy: SortType = {
  id: 'saveNumber',
  order: undefined
};

const initialState: IAgencyAssignRequestState = {
  data: [],
  loading: false,
  searchValue: '',
  filterValue: '',
  formOpen: false,
  formValues: DEFAULT_ASSIGN_REQUEST,
  formAction: 'ADD',
  inlineErrMsg: '',
  inlineErrMsgDelete: '',
  dataSortBy: defaultSortBy,
  isEnd: false,
  isLoadingLoadMore: false,
  startSequence: 1,
  endSequence: 15,
};

const { actions, reducer } = createSlice({
  name: 'agencyAssignRequest',
  initialState,
  reducers: {
    changeSearchValue: (draftState, action: PayloadAction<string>) => {
      draftState.searchValue = action.payload;
    },
    setFilterValue: draftState => {
      draftState.filterValue = draftState.searchValue;
    },
    clearSearchInput: draftState => {
      draftState.searchValue = '';
    },
    resetSearch: draftState => {
      draftState.searchValue = '';
      draftState.filterValue = '';
    },
    toggleForm: draftState => {
      draftState.formOpen = !draftState.formOpen;
      draftState.formAction = 'ADD';
    },
    resetFormValues: draftState => {
      draftState.formValues = DEFAULT_ASSIGN_REQUEST;
    },
    triggerEditAssignRequest: (
      draftState,
      action: PayloadAction<IAssignRequest>
    ) => {
      draftState.formOpen = true;
      draftState.formValues = action.payload;
      draftState.formAction = 'EDIT';
    },
    setSortType: (draftState, action: PayloadAction<SortType>) => {
      if (action.payload.order) {
        draftState.dataSortBy = action.payload;
      } else {
        draftState.dataSortBy = defaultSortBy;
      }
    },
    setInlineErrMsg: (draftState, action: PayloadAction<string>) => {
      draftState.inlineErrMsg = action.payload;
    },
    setInlineErrMsgDelete: (draftState, action: PayloadAction<string>) => {
      draftState.inlineErrMsgDelete = action.payload;
    },
    removeStore: _ => initialState
  },
  extraReducers: builder => {
    getAssignRequestsBuilder(builder);
    saveAssignRequestBuilder(builder);
    deleteAssignRequestBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getAssignRequests,
  saveAssignRequest,
  deleteAssignRequest
};

export { combineActions as agencyAssignRequestActions, reducer };
