import { orderBy } from 'app/helpers';
import { createSelector } from 'reselect';
import { IAgencyAssignRequestState, IAssignRequest } from '../types';

export const getAgencyAssignRequestState = (states: RootState) =>
  states.agencyAssignRequest;

export const selectedAssignRequestData = createSelector(
  getAgencyAssignRequestState,
  (data: IAgencyAssignRequestState) =>
    orderBy(
      data.data.filter(item => item.saveNumber.indexOf(data.filterValue) >= 0),
      [
        item => {
          if (data.dataSortBy.id === 'frequency')
            return item.frequency !== 'Custom' ? +item.frequency : +item.count!;
          return item[data.dataSortBy.id as keyof IAssignRequest];
        }
      ],
      data.dataSortBy.order || 'asc'
    )
);

export const selectedAssignRequestSortBy = createSelector(
  getAgencyAssignRequestState,
  (data: IAgencyAssignRequestState) => data.dataSortBy
);

export const selectedAssignRequestLoading = createSelector(
  getAgencyAssignRequestState,
  (data: IAgencyAssignRequestState) => data.loading
);

export const selectedSearchValue = createSelector(
  getAgencyAssignRequestState,
  (data: IAgencyAssignRequestState) => data.searchValue
);

export const selectedFilterValue = createSelector(
  getAgencyAssignRequestState,
  (data: IAgencyAssignRequestState) => data.filterValue
);

export const selectedFormOpen = createSelector(
  getAgencyAssignRequestState,
  (data: IAgencyAssignRequestState) => data.formOpen
);

export const selectedFormValues = createSelector(
  getAgencyAssignRequestState,
  (data: IAgencyAssignRequestState) => data.formValues
);

export const selectedFormAction = createSelector(
  getAgencyAssignRequestState,
  (data: IAgencyAssignRequestState) => data.formAction
);

export const selectInlineErrMsg = createSelector(
  getAgencyAssignRequestState,
  (data: IAgencyAssignRequestState) => data.inlineErrMsg
);

export const selectInlineErrMsgDelete = createSelector(
  getAgencyAssignRequestState,
  (data: IAgencyAssignRequestState) => data.inlineErrMsgDelete
);
export const selectorIsEnd = createSelector(
  getAgencyAssignRequestState,
  (data: IAgencyAssignRequestState) => data.isEnd
);

export const selectorIsLoadingLoadMore = createSelector(
  getAgencyAssignRequestState,
  (data: IAgencyAssignRequestState) => data.isLoadingLoadMore
);
