import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { agencyAssignRequestActions } from '.';
import { apiErrorDetailActions } from 'pages/__commons/ApiErrorDetail/_redux/reducer';

import { assignRequestController } from '../__mock__/assignRequestController';
import agencyManagementServices from '../../agencyManagementServices';

import { DEFAULT_ASSIGN_REQUEST, SAVE_ASSIGN_REQUEST } from '../constants';
import { IAgencyAssignRequestState, ISaveAssignRequestArgs } from '../types';
import { I18N_TEXT } from 'app/constants';
import { formatTime } from 'app/helpers';

export const saveAssignRequest = createAsyncThunk<
  unknown,
  ISaveAssignRequestArgs,
  ThunkAPIConfig
>(SAVE_ASSIGN_REQUEST, async (args, { dispatch, rejectWithValue }) => {
  try {
    const { assignRequest } = args;
    const { data: saveNumberData } =
      await agencyManagementServices.getSaveNumbers();

    const { data } = await assignRequestController.saveAssignRequest({
      ...assignRequest,
      description:
        (saveNumberData as MagicKeyValue)?.saveNumber.find(
          (item: RefDataValue) => item.value === assignRequest.saveNumber
        )?.description || '',
      nextRunDate: formatTime(assignRequest.nextRunDate).date!
    });

    if (!data.success) throw data;

    return batch(() => {
      dispatch(agencyAssignRequestActions.getAssignRequests({isRefresh: true}));
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: assignRequest.id
            ? I18N_TEXT.UPDATE_ASSIGN_SUCCESS
            : I18N_TEXT.ASSIGN_REQUEST_ADDED
        })
      );
    });
  } catch (error) {
    if (error?.message) {
      dispatch(
        agencyAssignRequestActions.setInlineErrMsg(
          saveAssignRequest.rejected.type
        )
      );
      dispatch(
        apiErrorDetailActions.setErrValue({
          id: saveAssignRequest.rejected.type,
          value:
            error?.message === 'EDIT'
              ? 'Something when wrong when editing!'
              : 'Something when wrong when adding!'
        })
      );
    }
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message:
          error.message === 'EDIT'
            ? I18N_TEXT.UPDATE_ASSIGN_FAILED
            : I18N_TEXT.ASSIGN_REQUEST_FAILED_TO_ADD
      })
    );
    return rejectWithValue(error);
  }
});

export const saveAssignRequestBuilder = (
  builder: ActionReducerMapBuilder<IAgencyAssignRequestState>
) => {
  const { fulfilled } = saveAssignRequest;
  builder.addCase(fulfilled, draftState => {
    draftState.formOpen = false;
    draftState.formValues = DEFAULT_ASSIGN_REQUEST;
    draftState.formAction = 'ADD';
  });
};
