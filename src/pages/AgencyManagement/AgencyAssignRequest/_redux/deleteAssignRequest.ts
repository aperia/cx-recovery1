import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { apiErrorDetailActions } from 'pages/__commons/ApiErrorDetail/_redux/reducer';
import { agencyAssignRequestActions } from '.';

import { assignRequestController } from '../__mock__/assignRequestController';
import { I18N_TEXT } from 'app/constants';
import { DELETE_ASSIGN_REQUEST } from '../constants';
import { IAgencyAssignRequestState } from '../types';

export const deleteAssignRequest = createAsyncThunk<
  unknown,
  { id: string },
  ThunkAPIConfig
>(DELETE_ASSIGN_REQUEST, async (args, { dispatch, rejectWithValue }) => {
  try {
    const { success } = await assignRequestController.deleteAssignRequest(
      args.id
    );

    if (!success) throw new Error('Delete assign request failed.');
    return batch(() => {
      dispatch(agencyAssignRequestActions.getAssignRequests({isRefresh: true}));
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.DELETE_ASSIGN_SUCCESS
        })
      );
      dispatch(rootModalActions.close());
    });
  } catch (error) {
    if (error?.message === 'DELETE FAILED') {
      dispatch(
        agencyAssignRequestActions.setInlineErrMsgDelete(
          deleteAssignRequest.rejected.type
        )
      );
      dispatch(
        apiErrorDetailActions.setErrValue({
          id: deleteAssignRequest.rejected.type,
          value: `Something wrong when deleting!`
        })
      );
    }
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_TEXT.DELETE_ASSIGN_FAILED
        })
      );
      dispatch(rootModalActions.close());
    });
    return rejectWithValue(error);
  }
});

export const deleteAssignRequestBuilder = (
  builder: ActionReducerMapBuilder<IAgencyAssignRequestState>
) => {
  const { pending, fulfilled, rejected } = deleteAssignRequest;
  builder
    .addCase(pending, draftState => {
      draftState.loading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      draftState.loading = false;
    })
    .addCase(rejected, draftState => {
      draftState.loading = false;
    });
};
