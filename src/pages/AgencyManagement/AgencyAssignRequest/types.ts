import { SortType } from 'app/_libraries/_dls/components';
import { FormAction } from 'pages/AgencyManagement/components/SaveAgencyRequestModal';

export const GET_ASSIGN_REQUESTS = 'getAssignRequests';

export interface IAgencyAssignRequestState {
  data: IAssignRequest[];
  loading: boolean;
  searchValue: string;
  filterValue: string;
  formOpen: boolean;
  formValues: IAssignRequest;
  formAction: FormAction;
  inlineErrMsg: string;
  inlineErrMsgDelete: string;
  dataSortBy: SortType;
  isEnd: boolean;
  isLoadingLoadMore: boolean;
  startSequence: number;
  endSequence: number;
}

export interface IAssignRequest {
  id?: string;
  saveNumber: string;
  description?: string;
  lastRunDate: string;
  nextRunDate: string;
  frequency: string;
  count?: string;
  reportOnly: string;
  type?: string;
}

export interface IPayloadGetAssignRequests {
  assignRequests: IAssignRequest[];
}

export interface ISaveAssignRequestArgs {
  assignRequest: IAssignRequest;
}
