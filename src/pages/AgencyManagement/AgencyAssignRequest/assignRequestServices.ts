import { apiService } from 'app/utils/api.service';

const assignRequestServices = {
  getAssignRequests() {
    const url = window.appConfig.api.assignRequest.getAssignRequests;
    return apiService.get(url);
  }
};

export default assignRequestServices;
