import React from 'react';
import { Button } from 'app/_libraries/_dls/components';

// constants
import { I18N_TEXT } from 'app/constants';
import { IAssignRequest } from './types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import DeleteModalBody from 'pages/AgencyManagement/components/DeleteModalBody';
import { RootModalOptions } from 'pages/__commons/RootModal/types';
import { useDispatch } from 'react-redux';
import { agencyAssignRequestActions } from './_redux';

const AgencyAssignGridActions: React.FC<IAssignRequest> = props => {
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const handleDelete = () => {
    //TODO: handle delete
    dispatch(agencyAssignRequestActions.deleteAssignRequest({ id: props.id! }));
  };

  const showModalDeleteAssignRequest = () => {
    const options: RootModalOptions = {
      autoClose: false,
      size: 'xs',
      title: t(I18N_TEXT.DELETE_AGENCY_ASSIGN_REQUEST),
      body: (
        <DeleteModalBody
          item={props}
          messageBody={t(I18N_TEXT.DELETE_CONFIRM_MESSAGE)}
        />
      ),
      onConfirm: handleDelete,
      btnConfirmText: t(I18N_TEXT.DELETE),
      onCancel: () => dispatch(rootModalActions.close())
    };
    dispatch(rootModalActions.open(options));
  };

  const onEdit = () => {
    dispatch(agencyAssignRequestActions.triggerEditAssignRequest(props));
  };

  return (
    <div className="d-flex">
      <Button
        size="sm"
        variant="outline-danger"
        onClick={showModalDeleteAssignRequest}
      >
        {t(I18N_TEXT.DELETE)}
      </Button>
      <Button size="sm" variant="outline-primary" onClick={onEdit}>
        {t(I18N_TEXT.EDIT)}
      </Button>
    </div>
  );
};

export default AgencyAssignGridActions;
