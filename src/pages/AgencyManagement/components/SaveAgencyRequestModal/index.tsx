import React, { useEffect, useMemo, useState } from 'react';
import { FormikProvider, useFormik } from 'formik';

// components
import {
  ComboBoxControl,
  DatePickerControl,
  DropdownControl,
  TextBoxControl
} from 'app/components';
import { GroupRadioControl } from 'app/components/FormControl/GroupRadio';
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import ApiErrorDetail from '../../../__commons/ApiErrorDetail';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useGetRefDataQuery } from '../../../__commons/RefData/refDataQuery';

import { checkEmpty, formatTime, isEmpty, isUndefined } from 'app/helpers';
import { isBeforeToday } from 'pages/AgencyManagement/helpers';

// redux
import { addRecallRequest } from 'pages/AgencyManagement/AgencyRecallRequest/_redux/addRecallRequest';
import { editRecallRequest } from 'pages/AgencyManagement/AgencyRecallRequest/_redux/editRecallRequest';
import { saveAssignRequest } from 'pages/AgencyManagement/AgencyAssignRequest/_redux/saveAssignRequest';

// constants & types
import { IAssignRequest } from 'pages/AgencyManagement/AgencyAssignRequest/types';
import { IRecallRequest } from 'pages/AgencyManagement/AgencyRecallRequest/types';
import { FORM_NAMES, TODAY } from './constants';
import { NUMERIC_REPLACE_REGEX, I18N_TEXT } from 'app/constants';

type AgencyRequestTarget = 'assignRequest' | 'recallRequest';
export type FormAction = 'ADD' | 'EDIT';
export type RequestValuesType = IAssignRequest | IRecallRequest | MagicKeyValue;

interface ISaveAgencyRequestModal {
  open: boolean;
  action: FormAction;
  title?: string;
  value?: RequestValuesType;
  target: AgencyRequestTarget;
  validationSchema?: MagicKeyValue;
  submitButtonText?: string;
  data: RequestValuesType[];
  onSubmit?: (value: RequestValuesType) => void;
  onCancel?: () => void;
}

const SaveAgencyRequestModal: React.FC<ISaveAgencyRequestModal> = ({
  open,
  title = '',
  action,
  value = {},
  target,
  validationSchema,
  data,
  onSubmit,
  onCancel
}) => {
  const { t } = useTranslation();

  const [checkValue, setCheckValue] = useState<boolean>(false);
  const [isFocusFieldCount, setIsFocusFieldCount] = useState<boolean>(false);
  const [errorDate, setErrorDate] = useState<string>('');

  const { data: saveNumberData } = useGetRefDataQuery('saveNumber');
  const { data: frequencyData } = useGetRefDataQuery('frequency');

  const handleSubmit = () => {
    onSubmit?.(values);
  };

  const formik = useFormik<RequestValuesType>({
    initialValues: value,
    validationSchema: validationSchema,
    validateOnBlur: true,
    validateOnMount: false,
    onSubmit: handleSubmit
  });

  const {
    values,
    isValid,
    dirty,
    setValues,
    setFieldTouched,
    submitForm,
    resetForm,
    setFieldValue,
    setFieldError
  } = formik;

  const handleCancel = () => {
    resetForm();
    onCancel?.();
  };

  useEffect(() => {
    let errorMessage = '';
    if (values.nextRunDate && action === 'EDIT') {
      if (isBeforeToday(new Date(values.nextRunDate))) {
        errorMessage = I18N_TEXT.NEXT_RUN_DATE_NOT_PAST_DAY;
      }
    }
    setErrorDate(errorMessage);
  }, [values.nextRunDate, setErrorDate, action]);

  useEffect(() => {
    setValues(value);
  }, [setValues, value]);

  useEffect(() => {
    if (open === false) {
      resetForm();
    }
  }, [open, resetForm]);

  const saveNumberOptions = useMemo(() => {
    if (action === 'ADD') {
      return checkEmpty([
        saveNumberData?.saveNumber.filter(num =>
          isUndefined(data.find(item => item.saveNumber === num.value))
        ) as RefDataValue[],
        []
      ]);
    }
    return checkEmpty([saveNumberData?.saveNumber as RefDataValue[], []]);
  }, [data, action, saveNumberData?.saveNumber]);

  const frequencyOptions = useMemo(
    () => checkEmpty([frequencyData?.frequency as RefDataValue[], []]),
    [frequencyData?.frequency]
  );

  useEffect(() => {
    let checkValue = false;
    const value = saveNumberOptions.find(
      (item: RefDataValue) => item.value === values?.saveNumber && item.isActive
    );
    if (value) {
      checkValue = true;
    }

    setCheckValue(checkValue);
  }, [values.saveNumber, saveNumberOptions]);

  useEffect(() => {
    if (values.frequency !== 'Custom') {
      setFieldValue(FORM_NAMES.count.name, '');
      setFieldTouched(FORM_NAMES.count.name, false);
      setFieldError(FORM_NAMES.count.name, undefined);
      setIsFocusFieldCount(false);
    }
  }, [
    setFieldError,
    setFieldTouched,
    setFieldValue,
    setValues,
    values.count,
    values.frequency
  ]);

  return (
    <Modal show={open}>
      <ModalHeader border closeButton onHide={handleCancel}>
        <ModalTitle>{title}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <ApiErrorDetail
          forAction={[
            addRecallRequest.rejected.type,
            editRecallRequest.rejected.type,
            saveAssignRequest.rejected.type
          ]}
          className="mb-24"
        />
        <FormikProvider value={formik}>
          <ComboBoxControl
            required
            name={FORM_NAMES.saveNumber.name}
            label={FORM_NAMES.saveNumber.label}
            options={saveNumberOptions}
            isCombine
            readOnly={action === 'EDIT'}
            isAutoSelect
            setFieldValueFormik={setFieldValue}
          />
          {checkValue ? (
            <p className="color-grey-l24 mt-8 fs-12">{`${t(
              I18N_TEXT.LAST_RUN_DATE
            )}: ${formatTime(TODAY).date}`}</p>
          ) : (
            <p className="color-grey-l24 mt-8 fs-12">
              {t(I18N_TEXT.NO_LAST_RUN_DATE)}
            </p>
          )}
          <DatePickerControl
            required
            name={FORM_NAMES.nextRunDate.name}
            label={FORM_NAMES.nextRunDate.label}
            className="mt-16"
            pastDateDisabledText={t(I18N_TEXT.NEXT_RUN_DATE_NOT_PAST_DAY)}
            minDaysFromToday={0}
            forceError={!isEmpty(errorDate)}
            forceMessageError={errorDate}
          />
          <DropdownControl
            required
            name={FORM_NAMES.frequency.name}
            label={FORM_NAMES.frequency.label}
            options={frequencyOptions}
            className="mt-16"
          />
          {values.frequency === 'Custom' ? (
            <TextBoxControl
              required
              name={FORM_NAMES.count.name}
              label={FORM_NAMES.count.label}
              className="mt-16"
              suffix={
                isFocusFieldCount ? String(t(I18N_TEXT.DAYS)).toLowerCase() : ''
              }
              maxLength={3}
              regex={NUMERIC_REPLACE_REGEX}
              onFocus={() => setIsFocusFieldCount(true)}
            />
          ) : null}

          <p className="color-grey fw-500 mt-16">
            {t(I18N_TEXT.REPORT_ONLY)}:<span className="ml-4 color-red">*</span>
          </p>
          <GroupRadioControl
            className="mt-8"
            name={FORM_NAMES.reportOnly.name}
            options={FORM_NAMES.reportOnly.options[target]}
          />
        </FormikProvider>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t(I18N_TEXT.CANCEL)}
        okButtonText={t(action === 'EDIT' ? I18N_TEXT.SAVE : I18N_TEXT.SUBMIT)}
        onOk={submitForm}
        onCancel={handleCancel}
        disabledOk={!isValid || !dirty || !isEmpty(errorDate)}
      />
    </Modal>
  );
};

export default SaveAgencyRequestModal;
