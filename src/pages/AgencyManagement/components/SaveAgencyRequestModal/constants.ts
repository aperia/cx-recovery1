export const FORM_NAMES = {
  saveNumber: {
    name: 'saveNumber',
    label: 'txt_save_number'
  },
  nextRunDate: {
    name: 'nextRunDate',
    label: 'txt_next_run_date'
  },
  frequency: {
    name: 'frequency',
    label: 'txt_frequency'
  },
  count: {
    name: 'count',
    label: 'txt_count'
  },
  reportOnly: {
    name: 'reportOnly',
    label: 'txt_report_only',
    options: {
      assignRequest: [
        {
          value: 'No - Assign the accounts',
          description: 'txt_no_assign_the_acc'
        },
        {
          value: 'Yes - Produce RD-613, New Assigns report',
          description: 'txt_yes_new_assign_report'
        }
      ],
      recallRequest: [
        {
          value: 'No - Recall the accounts',
          description: 'txt_no_recall_the_acc'
        },
        {
          value: 'Yes - Produce RD-614, Recalls report',
          description: 'txt_yes_produce_recall_report'
        }
      ]
    }
  }
};
export const TODAY = new Date().toString();
