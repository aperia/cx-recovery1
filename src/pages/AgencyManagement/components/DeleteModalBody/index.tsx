import React from 'react';

import { IAssignRequest } from 'pages/AgencyManagement/AgencyAssignRequest/types';
import { IRecallRequest } from 'pages/AgencyManagement/AgencyRecallRequest/types';

interface IDeleteModalBodyProps {
  item: IRecallRequest | IAssignRequest;
  messageBody?: string;
}

const DeleteModalBody: React.FC<IDeleteModalBodyProps> = ({
  item,
  messageBody
}) => {
  return (
    <>
      <p>
        {messageBody}{' '}
        <span className="dls-sub-heading">{`${item.saveNumber} - ${item.description}`}</span>
        ?
      </p>
    </>
  );
};

export default DeleteModalBody;
