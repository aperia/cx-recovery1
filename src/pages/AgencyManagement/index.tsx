import React, {
  Suspense,
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState
} from 'react';

// components
import ExtraStaticTabs from 'app/_libraries/_dls/components/VerticalTabs';
import LazyLoadingFallback from 'app/components/LazyLoadingFallback';
import { SimpleBar } from 'app/_libraries/_dls/components';

// redux
import { agencyManagementActions } from './_redux';
import { selectedSectionList } from './_redux/selectors';
import { useDispatch, useSelector } from 'react-redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { mapSectionList } from './helpers';
import { ISectionItem } from './types';
import { I18N_TEXT } from 'app/constants';
import { isNull } from 'app/helpers';
import classNames from 'classnames';

const AgencyManagement: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [collapsed, setCollapsed] = useState(false);
  const [collapsing, setCollapsing] = useState(false);
  const [activeTab, setActiveTab] = useState('AMN');
  const divRef = useRef<HTMLDivElement | null>(null);

  const sections: RefDataValue[] = useSelector(selectedSectionList);
  const data: ISectionItem[] = mapSectionList(sections);

  const handleToggleCollapsed = useCallback(isCollapsed => {
    setCollapsed(isCollapsed);
    setCollapsing(true);
    const collapsingTimeout = setTimeout(() => {
      setCollapsing(false);
    }, 500);
    return () => clearTimeout(collapsingTimeout);
  }, []);

  const handleOnSelect = (eventKey: string | null) => {
    if (activeTab === eventKey) return;
    if (isNull(eventKey)) return;
    setActiveTab(eventKey);
  };

  useEffect(() => {
    dispatch(agencyManagementActions.getSections());
  }, [dispatch]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 193px)`);
  }, []);

  return (
    <div className="h-100">
      <div className="p-24 bg-light-l20 border-bottom">
        <h3>{t('txt_agency_management')}</h3>
      </div>
      <ExtraStaticTabs
        activeKey={activeTab}
        onToggleCollapsed={handleToggleCollapsed}
        onSelect={handleOnSelect}
        unmountOnExit
        mountOnEnter
        collapsed={collapsed}
        className="scrolled-nav"
        expandMessage={t(I18N_TEXT.EXPAND)}
        collapseMessage={t(I18N_TEXT.COLLAPSE)}
      >
        <div
          ref={divRef}
          className={classNames('tab-left', {
            'is-collapsed': collapsed,
            'is-collapsing': collapsing
          })}
        >
          <SimpleBar>
            <ExtraStaticTabs.Nav collapsed={collapsed}>
              {data.map(({ id, title }) => (
                <ExtraStaticTabs.Nav.Item key={id}>
                  <ExtraStaticTabs.Nav.Link eventKey={id}>
                    {title}
                  </ExtraStaticTabs.Nav.Link>
                </ExtraStaticTabs.Nav.Item>
              ))}
            </ExtraStaticTabs.Nav>
          </SimpleBar>
        </div>
        <ExtraStaticTabs.Content>
          {data.map(({ id, Component }) => {
            return (
              <ExtraStaticTabs.Pane eventKey={id} key={id}>
                <Suspense fallback={<LazyLoadingFallback />}>
                  <Component />
                </Suspense>
              </ExtraStaticTabs.Pane>
            );
          })}
        </ExtraStaticTabs.Content>
      </ExtraStaticTabs>
    </div>
  );
};

export default AgencyManagement;
