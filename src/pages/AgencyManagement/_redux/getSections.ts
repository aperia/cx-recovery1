import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import agencyManagementServices from '../agencyManagementServices';
import {
  GET_SECTIONS,
  IAgencyManagementState,
  IPayloadGetSections
} from '../types';

export const getSections = createAsyncThunk<
  IPayloadGetSections,
  undefined,
  ThunkAPIConfig
>(GET_SECTIONS, async (_, { rejectWithValue }) => {
  try {
    const { data } = await agencyManagementServices.getSections();
    return {
      sections: data as RefDataValue[]
    };
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getSectionsBuilder = (
  builder: ActionReducerMapBuilder<IAgencyManagementState>
) => {
  const { pending, fulfilled, rejected } = getSections;
  builder
    .addCase(pending, draftState => {
      draftState.loading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      draftState.loading = false;
      draftState.sections = action.payload.sections;
    })
    .addCase(rejected, draftState => {
      draftState.loading = false;
    });
};
