import { createSelector } from 'reselect';
import { IAgencyManagementState } from '../types';

export const getAgencyManagementStates = (states: RootState) =>
  states.agencyManagement;

export const selectedSectionList = createSelector(
  getAgencyManagementStates,
  (data: IAgencyManagementState) => data.sections
);
