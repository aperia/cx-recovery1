import { createSlice } from '@reduxjs/toolkit';
import { IAgencyManagementState } from '../types';
import { getSections, getSectionsBuilder } from './getSections';

const initialState: IAgencyManagementState = {
  sections: [],
  loading: false
};

const { actions, reducer } = createSlice({
  name: 'agencyManagement',
  initialState,
  reducers: {},
  extraReducers: builder => {
    getSectionsBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getSections
};

export { combineActions as agencyManagementActions, reducer };
