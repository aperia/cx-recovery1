import React, {
  Fragment,
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef
} from 'react';
import {
  Button,
  InlineMessage,
  SimpleBar,
  TextSearch,
  Tooltip
} from 'app/_libraries/_dls/components';
import RecallRequestGrid from './RecallRequestGrid';
import SaveAgencyRequestModal, {
  FormAction
} from 'pages/AgencyManagement/components/SaveAgencyRequestModal';
import ApiErrorDetail from 'pages/__commons/ApiErrorDetail';

// constants & types
import { I18N_TEXT } from 'app/constants';
import { EXAMPLE_INLINE_MESSAGE } from './constants';
import { IRecallRequest } from './types';
import { FORM_TYPE } from '../types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSaveAssignRequestForm } from '../hooks/useSaveAssignRequestForm';
import { useSearchTextEvent } from '../hooks/useSearchTextEvent';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';

// helpers
import { genAmtId } from 'app/_libraries/_dls/utils';
import classNames from 'classnames';
import { isEmpty, isUndefined } from 'app/helpers';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { agencyRecallRequestActions } from './_redux';
import {
  selectedRecallRequestData,
  selectedRecallRequestLoading,
  selectedFilterValue,
  selectedFormAction
} from './_redux/selectors';
import { deleteRecallRequest } from './_redux/deleteRecallRquest';

const AgencyRecallRequest: React.FC = () => {
  const dataTestId = 'agencyRecallRequest';
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const loading: boolean = useSelector(selectedRecallRequestLoading);
  const data: IRecallRequest[] = useSelector(selectedRecallRequestData);
  const filterValue: string = useSelector(selectedFilterValue);
  const formAction: FormAction = useSelector(selectedFormAction);
  const isEmptyData: boolean = isEmpty(data) && isEmpty(filterValue);
  const divRef = useRef<HTMLDivElement | null>(null);

  const { data: saveNumberData } = useGetRefDataQuery('saveNumber');
  const disableAddBtn = isEmpty(
    saveNumberData?.saveNumber.filter(num =>
      isUndefined(data.find(item => item.saveNumber === num.value))
    ) as RefDataValue[]
  );

  const handleClearAndReset = useCallback(() => {
    dispatch(agencyRecallRequestActions.resetSearch());
  }, [dispatch]);

  const { open, values, validationSchema, onToggleForm, onSubmit, onCancel } =
    useSaveAssignRequestForm(FORM_TYPE[1]);

  const GroupButton = useMemo(
    () => (
      <div className="d-flex mr-n8">
        {filterValue && !isEmpty(data) ? (
          <Button
            variant="outline-primary"
            size="sm"
            onClick={handleClearAndReset}
          >
            {t(I18N_TEXT.CLEAR_AND_RESET)}
          </Button>
        ) : null}
        {disableAddBtn ? (
          <Tooltip
            placement="top-end"
            variant="default"
            element={t(I18N_TEXT.ALL_SAVE_NUMBERS_ARE_ADDED)}
          >
            <Button variant="outline-primary" size="sm" disabled>
              {t(I18N_TEXT.ADD_RECALL_REQUEST)}
            </Button>
          </Tooltip>
        ) : (
          <Button variant="outline-primary" size="sm" onClick={onToggleForm}>
            {t(I18N_TEXT.ADD_RECALL_REQUEST)}
          </Button>
        )}
      </div>
    ),
    [filterValue, data, handleClearAndReset, t, disableAddBtn, onToggleForm]
  );

  const {
    searchValue,
    searchPlaceholder,
    handleChange,
    handleClear,
    handleSearch
  } = useSearchTextEvent(FORM_TYPE[1]);

  useEffect(() => {
    dispatch(agencyRecallRequestActions.getRecallRequests());
    return () => {
      dispatch(agencyRecallRequestActions.removeStore());
    };
  }, [dispatch]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100% - 81px)`);
  }, []);

  return (
    <div ref={divRef}>
      <SimpleBar>
        <div
          className={classNames('px-24 py-22 max-width-lg mx-auto', {
            loading
          })}
        >
          <ApiErrorDetail
            forAction={[deleteRecallRequest.rejected.type]}
            className="mb-24"
          />
          {!isEmptyData ? (
            <Fragment>
              <div className="header-agency-request">
                <h5
                  className="header-title"
                  data-testid={genAmtId(dataTestId, 'title', '')}
                >
                  {t(I18N_TEXT.RECALL_REQUEST_LIST)}
                </h5>

                <div className="header-message">
                  <InlineMessage className="mb-0" variant="warning">
                    <span>{EXAMPLE_INLINE_MESSAGE}</span>
                  </InlineMessage>
                </div>
                <div className="header-search">
                  <TextSearch
                    value={searchValue}
                    placeholder={searchPlaceholder}
                    onChange={handleChange}
                    onSearch={handleSearch}
                    onClear={handleClear}
                  />
                </div>
                <div className="header-group-button">{GroupButton}</div>
              </div>

              <RecallRequestGrid />
            </Fragment>
          ) : (
            <Fragment>
              <h5 data-testid={genAmtId(dataTestId, 'title', '')}>
                {t(I18N_TEXT.RECALL_REQUEST_LIST)}
              </h5>
              <RecallRequestGrid />
            </Fragment>
          )}
        </div>
        <SaveAgencyRequestModal
          open={open}
          data={data}
          value={values}
          action={formAction}
          title={
            values.id
              ? t('txt_edit_agency_recall_request')
              : t('txt_add_agency_recall_request')
          }
          target="recallRequest"
          validationSchema={validationSchema}
          onCancel={onCancel}
          onSubmit={values => onSubmit(values as IRecallRequest)}
        />
      </SimpleBar>
    </div>
  );
};

export default AgencyRecallRequest;
