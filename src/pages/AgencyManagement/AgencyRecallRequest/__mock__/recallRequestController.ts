import recallRequestsJson from './recallRequests.json';

let recallRequestsData = JSON.parse(JSON.stringify(recallRequestsJson));

const isHaveError = window.location.href.includes('error');

export const recallRequestController = {
  getRecallRequests({
    startSequence,
    endSequence
  }: {
    startSequence: number;
    endSequence: number;
  }){
    const recallData = recallRequestsData.slice(startSequence - 1, endSequence);
    return Promise.resolve({
      data: { success: true, recallRequests: recallData }
    });
  },
  getRecallRequestById(id: string) {
    return Promise.resolve({
      data: {
        success: true,
        recallRequest: recallRequestsData.find((item: any) => item.id === id)
      }
    });
  },
  addRecallRequest(newRecall: any) {
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: 'ADD FAILED'
      });
    recallRequestsData = [
      ...recallRequestsData,
      {
        ...newRecall,
        id: Date.now(),
        type: 'R - Recalls'
      }
    ];
    return Promise.resolve({
      success: true
    });
  },
  editRecallRequest(id: string, dataChange: any) {
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: 'EDIT FAILED'
      });
    const indexOfDataChange = recallRequestsData.findIndex(
      (item: any) => item.id === id
    );
    recallRequestsData = [
      ...recallRequestsData.slice(0, indexOfDataChange),
      { id, ...dataChange },
      ...recallRequestsData.slice(indexOfDataChange + 1)
    ];
    return Promise.resolve({
      success: true
    });
  },
  deleteRecallRequest(id: string) {
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: 'DELETE FAILED'
      });
    recallRequestsData = recallRequestsData.filter(
      (item: any) => item.id !== id
    );
    return Promise.resolve({ success: true, data: recallRequestsData });
  }
};
