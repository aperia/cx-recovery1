import React from 'react';

// components
import DeleteModalBody from 'pages/AgencyManagement/components/DeleteModalBody';
import { Button } from 'app/_libraries/_dls/components';

// redux
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { agencyRecallRequestActions } from './_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useDispatch } from 'react-redux';

// constants & types
import { I18N_TEXT } from 'app/constants';
import { IRecallRequest } from './types';
import { RootModalOptions } from 'pages/__commons/RootModal/types';

const AgencyRecallGridActions: React.FC<IRecallRequest> = props => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const handleDelete = () => {
    dispatch(
      agencyRecallRequestActions.deleteRecallRequest({ id: props.id || '' })
    );
  };

  const handleEdit = () => {
    dispatch(
      agencyRecallRequestActions.triggerEditRecallRequest({
        ...props
      })
    );
  };

  const showModalDeleteRecallRequest = () => {
    const options: RootModalOptions = {
      autoClose: false,
      size: 'xs',
      title: t(I18N_TEXT.DELETE_AGENCY_RECALL_REQUEST),
      body: (
        <DeleteModalBody
          item={props}
          messageBody={t(I18N_TEXT.DELETE_CONFIRM_MESSAGE)}
        />
      ),
      onConfirm: handleDelete,
      btnConfirmText: t(I18N_TEXT.DELETE),
      onCancel: () => dispatch(rootModalActions.close())
    };
    dispatch(rootModalActions.open(options));
  };

  return (
    <div className="d-flex">
      <Button
        size="sm"
        variant="outline-danger"
        onClick={showModalDeleteRecallRequest}
      >
        {t(I18N_TEXT.DELETE)}
      </Button>
      <Button size="sm" variant="outline-primary" onClick={handleEdit}>
        {t(I18N_TEXT.EDIT)}
      </Button>
    </div>
  );
};

export default AgencyRecallGridActions;
