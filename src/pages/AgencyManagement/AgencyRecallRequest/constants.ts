import { IRecallRequest } from './types';

export const EXAMPLE_INLINE_MESSAGE =
  'Requests made or modified after 5:00 PM CT with Next Run Date on the same day will run the next day.';

export const RECALL_REQUEST_FIELD = {
  SAVE_NUMBER: {
    name: 'saveNumber',
    label: 'txt_save_number'
  },
  NEXT_RUN_DATE: {
    name: 'nextRunDate',
    label: 'txt_next_run_date'
  },
  FREQUENCY: {
    name: 'frequency',
    label: 'txt_frequency'
  },
  REPORT_ONLY: {
    name: 'reportOnly'
  }
};

export const DEFAULT_RECALL_REQUEST: IRecallRequest = {
  saveNumber: '',
  lastRunDate: '',
  nextRunDate: new Date().toString(),
  frequency: '',
  reportOnly: ''
};

export const GET_RECALL_REQUESTS = 'getRecallRequests';
export const ADD_RECALL_REQUEST = 'addRecallRequest';
export const EDIT_RECALL_REQUEST = 'editRecallRequest';
export const DELETE_RECALL_REQUEST = 'deleteRecallRequest';
