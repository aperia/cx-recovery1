import React from 'react';

// components
import LoadMore from 'app/components/LoadMore';

// redux
import { useDispatch, useSelector } from 'react-redux';

// helper
import { agencyRecallRequestActions } from './_redux';
import { selectorIsEnd, selectorIsLoadingLoadMore } from './_redux/selectors';

export interface LoadMoreRecallProps {}

const LoadMoreRecall: React.FC<LoadMoreRecallProps> = () => {
  const dispatch = useDispatch();
  const isEnd = useSelector(selectorIsEnd);
  const isLoading = useSelector(selectorIsLoadingLoadMore);

  const handleOnLoadMore = () => {
    dispatch(agencyRecallRequestActions.getRecallRequests());
  };

  return (
    <LoadMore
      loading={isLoading}
      onLoadMore={handleOnLoadMore}
      isEnd={isEnd}
    />
  );
};

export default LoadMoreRecall;
