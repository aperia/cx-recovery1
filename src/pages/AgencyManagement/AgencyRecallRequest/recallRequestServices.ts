import { apiService } from 'app/utils/api.service';

const recallRequestServices = {
  getRecallRequests() {
    const url = window.appConfig.api.recallRequest.getRecallRequests;
    return apiService.get(url);
  }
};

export default recallRequestServices;
