import { SortType } from 'app/_libraries/_dls/components';
import { FormAction } from 'pages/AgencyManagement/components/SaveAgencyRequestModal';

export interface IAgencyRecallRequestState {
  data: IRecallRequest[];
  loading: boolean;
  searchValue: string;
  filterValue: string;

  formOpen: boolean;
  formValues: IRecallRequest;
  formAction: FormAction;
  inlineErrMsg: string;
  inlineErrMsgDelete: string;
  dataSortBy: SortType;
  isEnd: boolean;
  isLoadingLoadMore: boolean;
  startSequence: number;
  endSequence: number;
}

export interface IRecallRequest {
  id?: string;
  saveNumber: string;
  description?: string;
  lastRunDate: string;
  nextRunDate: string;
  frequency: string;
  count?: string;
  reportOnly: string;
  type?: string;
}

export interface IPayloadGetRecallRequests {
  recallRequests: IRecallRequest[];
}

export interface ISaveRecallRequestArgs {
  recallRequest: IRecallRequest;
  formAction: FormAction;
}
