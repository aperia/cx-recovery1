import React, { Fragment, useCallback, useMemo } from 'react';
import {
  Button,
  ColumnType,
  Grid,
  SimpleBar,
  SortType
} from 'app/_libraries/_dls/components';
import { NoDataGrid, SearchNodata } from 'app/components';
import AgencyRecallGridActions from './Actions';
import LoadMoreRecall from './LoadMoreRecall';

// constants & types
import { I18N_TEXT } from 'app/constants';
import { IRecallRequest } from './types';
import { FORM_TYPE } from '../types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSaveAssignRequestForm } from '../hooks/useSaveAssignRequestForm';

// helpers
import { isEmpty } from 'app/helpers';

// redux
import { useDispatch, useSelector } from 'react-redux';
import {
  selectedRecallRequestData,
  selectedRecallRequestLoading,
  selectedFilterValue,
  selectedRecallRequestSortBy
} from './_redux/selectors';
import { agencyRecallRequestActions } from './_redux';
import { formatFrequency } from '../helpers';

const RecallRequestGrid: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const recallRequestData: IRecallRequest[] = useSelector(
    selectedRecallRequestData
  );
  const recallRequestLoading: boolean = useSelector(
    selectedRecallRequestLoading
  );
  const filterValue: string = useSelector(selectedFilterValue);
  const sortBy: SortType = useSelector(selectedRecallRequestSortBy);

  const { onToggleForm } = useSaveAssignRequestForm(FORM_TYPE[1]);

  const handleClear = useCallback(() => {
    dispatch(agencyRecallRequestActions.resetSearch());
  }, [dispatch]);

  const RECALL_REQUEST_LIST_COLUMNS: ColumnType<IRecallRequest>[] = useMemo(
    () => [
      {
        id: 'saveNumber',
        Header: t(I18N_TEXT.SAVE_NUMBER),
        isSort: true,
        width: 124,
        accessor: 'saveNumber'
      },
      {
        id: 'description',
        Header: t(I18N_TEXT.DESCRIPTION),
        isSort: true,
        width: 230,
        accessor: 'description'
      },
      {
        id: 'lastRunDate',
        Header: t(I18N_TEXT.LAST_RUN_DATE),
        isSort: true,
        width: 130,
        accessor: 'lastRunDate'
      },
      {
        id: 'nextRunDate',
        Header: t(I18N_TEXT.NEXT_RUN_DATE),
        isSort: true,
        width: 130,
        accessor: 'nextRunDate'
      },
      {
        id: 'frequency',
        Header: t(I18N_TEXT.FREQUENCY),
        isSort: true,
        width: 110,
        accessor: (data: IRecallRequest) => t(formatFrequency(data))
      },
      {
        id: 'reportOnly',
        Header: t(I18N_TEXT.REPORT_ONLY),
        isSort: true,
        width: 315,
        accessor: 'reportOnly'
      },
      {
        id: 'type',
        Header: t(I18N_TEXT.TYPE),
        isSort: true,
        width: 150,
        accessor: 'type'
      },
      {
        id: 'actions',
        Header: t(I18N_TEXT.ACTIONS),
        isSort: true,
        width: 134,
        isFixedRight: true,
        accessor: (data: IRecallRequest) => (
          <AgencyRecallGridActions {...data} />
        ),
        className: 'text-center td-sm'
      }
    ],
    [t]
  );

  const handleSortChange = useCallback(
    (sortType: SortType) => {
      dispatch(agencyRecallRequestActions.setSortType(sortType));
    },
    [dispatch]
  );

  const DataGridView = useMemo(() => {
    if (recallRequestLoading) return null;

    if (isEmpty(recallRequestData)) {
      if (!isEmpty(filterValue))
        return <SearchNodata onClearSearch={handleClear} />;
      return (
        <NoDataGrid text={t(I18N_TEXT.NO_REQUESTS_TO_DISPLAY)}>
          <Button
            className="mt-24"
            variant="outline-primary"
            size="sm"
            onClick={onToggleForm}
          >
            {t(I18N_TEXT.ADD_RECALL_REQUEST)}
          </Button>
        </NoDataGrid>
      );
    }

    return (
      <Fragment>
        <Grid
          sortBy={[sortBy]}
          onSortChange={handleSortChange}
          className="mt-16"
          columns={RECALL_REQUEST_LIST_COLUMNS}
          data={recallRequestData}
        />
        <LoadMoreRecall />
      </Fragment>
    );
  }, [
    recallRequestLoading,
    recallRequestData,
    sortBy,
    handleSortChange,
    RECALL_REQUEST_LIST_COLUMNS,
    filterValue,
    handleClear,
    t,
    onToggleForm
  ]);

  return <SimpleBar>{DataGridView}</SimpleBar>;
};

export default RecallRequestGrid;
