import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

import { recallRequestController } from '../__mock__/recallRequestController';

import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { agencyRecallRequestActions } from '.';
import { apiErrorDetailActions } from 'pages/__commons/ApiErrorDetail/_redux/reducer';

import { DELETE_RECALL_REQUEST } from '../constants';
import { I18N_TEXT } from 'app/constants';
import { IPayloadGetRecallRequests, IAgencyRecallRequestState } from '../types';

export const deleteRecallRequest = createAsyncThunk<
  IPayloadGetRecallRequests,
  { id: string },
  ThunkAPIConfig
>(DELETE_RECALL_REQUEST, async (args, { dispatch, rejectWithValue }) => {
  try {
    const { data, success } = await recallRequestController.deleteRecallRequest(
      args.id
    );

    if (!success) throw new Error('Delete recall request failed.');
    batch(() => {
      dispatch(agencyRecallRequestActions.getRecallRequests({isRefresh: true}));
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.DELETE_RECALL_SUCCESS
        })
      );
      dispatch(rootModalActions.close());
    });
    return {
      recallRequests: data
    };
  } catch (error) {
    if (error?.message === 'DELETE FAILED') {
      dispatch(
        agencyRecallRequestActions.setInlineErrMsgDelete(
          deleteRecallRequest.rejected.type
        )
      );
      dispatch(
        apiErrorDetailActions.setErrValue({
          id: deleteRecallRequest.rejected.type,
          value: `Something wrong when deleting!`
        })
      );
    }
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_TEXT.DELETE_RECALL_FAILED
        })
      );
      dispatch(rootModalActions.close());
    });
    return rejectWithValue(error);
  }
});

export const deleteRecallRequestBuilder = (
  builder: ActionReducerMapBuilder<IAgencyRecallRequestState>
) => {
  const { pending, fulfilled, rejected } = deleteRecallRequest;
  builder
    .addCase(pending, draftState => {
      draftState.loading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      draftState.data = action.payload.recallRequests;
      draftState.loading = false;
    })
    .addCase(rejected, draftState => {
      draftState.loading = false;
    });
};
