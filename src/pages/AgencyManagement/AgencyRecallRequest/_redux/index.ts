import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import {
  getRecallRequests,
  getRecallRequestsBuilder
} from './getRecallRequests';

import { addRecallRequest, addRecallRequestBuilder } from './addRecallRequest';

import {
  editRecallRequest,
  editRecallRequestBuilder
} from './editRecallRequest';

import {
  deleteRecallRequest,
  deleteRecallRequestBuilder
} from './deleteRecallRquest';

import { DEFAULT_RECALL_REQUEST } from '../constants';
import { IAgencyRecallRequestState, IRecallRequest } from '../types';
import { SortType } from 'app/_libraries/_dls/components';

const defaultSortBy: SortType = {
  id: 'saveNumber',
  order: undefined
};

const initialState: IAgencyRecallRequestState = {
  data: [],
  loading: false,
  searchValue: '',
  filterValue: '',

  formOpen: false,
  formValues: DEFAULT_RECALL_REQUEST,
  formAction: 'ADD',
  inlineErrMsg: '',
  inlineErrMsgDelete: '',
  dataSortBy: defaultSortBy,
  isEnd: false,
  isLoadingLoadMore: false,
  startSequence: 1,
  endSequence: 15,
};

const { actions, reducer } = createSlice({
  name: 'agencyRecallRequest',
  initialState,
  reducers: {
    changeSearchValue: (draftState, action: PayloadAction<string>) => {
      draftState.searchValue = action.payload;
    },
    setFilterValue: draftState => {
      draftState.filterValue = draftState.searchValue;
    },
    clearSearchInput: draftState => {
      draftState.searchValue = '';
    },
    resetSearch: draftState => {
      draftState.searchValue = '';
      draftState.filterValue = '';
    },
    toggleForm: draftState => {
      draftState.formOpen = !draftState.formOpen;
      draftState.formAction = 'ADD';
    },
    resetFormValues: draftState => {
      draftState.formValues = DEFAULT_RECALL_REQUEST;
    },
    triggerEditRecallRequest: (
      draftState,
      action: PayloadAction<IRecallRequest>
    ) => {
      draftState.formOpen = true;
      draftState.formValues = action.payload;
      draftState.formAction = 'EDIT';
    },
    setInlineErrMsg: (draftState, action: PayloadAction<string>) => {
      draftState.inlineErrMsg = action.payload;
    },
    setInlineErrMsgDelete: (draftState, action: PayloadAction<string>) => {
      draftState.inlineErrMsgDelete = action.payload;
    },
    setSortType: (draftState, action: PayloadAction<SortType>) => {
      if (action.payload.order) {
        draftState.dataSortBy = action.payload;
      } else {
        draftState.dataSortBy = defaultSortBy;
      }
    },
    removeStore: _ => initialState
  },
  extraReducers: builder => {
    getRecallRequestsBuilder(builder);
    addRecallRequestBuilder(builder);
    editRecallRequestBuilder(builder);
    deleteRecallRequestBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getRecallRequests,
  addRecallRequest,
  editRecallRequest,
  deleteRecallRequest
};

export { combineActions as agencyRecallRequestActions, reducer };
