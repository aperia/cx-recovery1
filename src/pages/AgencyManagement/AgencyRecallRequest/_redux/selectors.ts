import { createSelector } from '@reduxjs/toolkit';
import { orderBy } from 'app/helpers';

import { FormAction } from 'pages/AgencyManagement/components/SaveAgencyRequestModal';

import { IAgencyRecallRequestState, IRecallRequest } from '../types';

export const getAgencyRecallRequestState = (
  states: RootState
): IAgencyRecallRequestState => states.agencyRecallRequest;

export const selectedRecallRequestData = createSelector(
  getAgencyRecallRequestState,
  (data: IAgencyRecallRequestState) =>
    orderBy(
      data.data.filter(item => item.saveNumber.indexOf(data.filterValue) >= 0),
      [
        item => {
          if (data.dataSortBy.id === 'frequency')
            return item.frequency !== 'Custom' ? +item.frequency : +item.count!;
          return item[data.dataSortBy.id as keyof IRecallRequest];
        }
      ],
      data.dataSortBy.order || 'asc'
    )
);

export const selectedRecallRequestSortBy = createSelector(
  getAgencyRecallRequestState,
  (data: IAgencyRecallRequestState) => data.dataSortBy
);

export const selectedRecallRequestLoading = createSelector(
  getAgencyRecallRequestState,
  (data: IAgencyRecallRequestState) => data.loading
);

export const selectedSearchValue = createSelector(
  getAgencyRecallRequestState,
  (data: IAgencyRecallRequestState) => data.searchValue
);

export const selectedFilterValue = createSelector(
  getAgencyRecallRequestState,
  (data: IAgencyRecallRequestState) => data.filterValue
);

const getInlineErrMsg = (state: RootState): string =>
  state.agencyRecallRequest.inlineErrMsg;
const getLoading = (state: RootState): boolean =>
  state.agencyRecallRequest.loading;
const getFormOpen = (state: RootState): boolean =>
  state.agencyRecallRequest.formOpen;
const getFormValues = (state: RootState): IRecallRequest =>
  state.agencyRecallRequest.formValues;
const getFormActionRecallRequest = (state: RootState): FormAction =>
  state.agencyRecallRequest.formAction;

export const selectLoading = createSelector(
  getLoading,
  (data: boolean) => data
);
export const selectInlineErrMsg = createSelector(
  getInlineErrMsg,
  (data: string) => data
);

export const selectInlineErrMsgDelete = createSelector(
  getAgencyRecallRequestState,
  (data: IAgencyRecallRequestState) => data.inlineErrMsgDelete
);

export const selectedFormOpen = createSelector(
  getFormOpen,
  (data: boolean) => data
);

export const selectedFormValues = createSelector(
  getFormValues,
  (data: IRecallRequest) => data
);

export const selectedFormAction = createSelector(
  getFormActionRecallRequest,
  (data: FormAction) => data
);

export const selectorIsEnd = createSelector(
  getAgencyRecallRequestState,
  (data: IAgencyRecallRequestState) => data.isEnd
);

export const selectorIsLoadingLoadMore = createSelector(
  getAgencyRecallRequestState,
  (data: IAgencyRecallRequestState) => data.isLoadingLoadMore
);
