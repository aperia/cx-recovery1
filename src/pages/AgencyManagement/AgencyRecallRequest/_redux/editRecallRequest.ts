import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

import { formatTime } from 'app/helpers';
import agencyManagementServices from 'pages/AgencyManagement/agencyManagementServices';
import { recallRequestController } from '../__mock__/recallRequestController';

import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { agencyRecallRequestActions } from '.';
import { apiErrorDetailActions } from 'pages/__commons/ApiErrorDetail/_redux/reducer';

import { EDIT_RECALL_REQUEST } from '../constants';
import { I18N_TEXT } from 'app/constants';
import { IAgencyRecallRequestState, IRecallRequest } from '../types';

export const editRecallRequest = createAsyncThunk<
  void,
  { id: string; dataChange: IRecallRequest },
  ThunkAPIConfig
>(EDIT_RECALL_REQUEST, async (args, { dispatch, rejectWithValue }) => {
  try {
    const { id, dataChange } = args;
    const { data: saveNumberData } =
      await agencyManagementServices.getSaveNumbers();
    const { success } = await recallRequestController.editRecallRequest(id, {
      ...dataChange,
      description:
        (saveNumberData as MagicKeyValue)?.saveNumber.find(
          (item: RefDataValue) => item.value === dataChange.saveNumber
        )?.description || '',
      nextRunDate: formatTime(dataChange.nextRunDate)?.date || ''
    });
    if (!success) throw new Error('Update recall request failed.');

    batch(() => {
      dispatch(agencyRecallRequestActions.toggleForm());
      dispatch(agencyRecallRequestActions.getRecallRequests({isRefresh: true}));
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.UPDATE_RECALL_SUCCESS
        })
      );
    });
  } catch (error) {
    if (error?.message === 'EDIT FAILED') {
      dispatch(
        agencyRecallRequestActions.setInlineErrMsg(
          editRecallRequest.rejected.type
        )
      );
      dispatch(
        apiErrorDetailActions.setErrValue({
          id: editRecallRequest.rejected.type,
          value: 'Something when wrong when editing!'
        })
      );
    }
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.UPDATE_RECALL_FAILED
      })
    );
    return rejectWithValue(error);
  }
});

export const editRecallRequestBuilder = (
  builder: ActionReducerMapBuilder<IAgencyRecallRequestState>
) => {
  const { pending, fulfilled, rejected } = editRecallRequest;
  builder
    .addCase(pending, draftState => {
      draftState.loading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      draftState.loading = false;
    })
    .addCase(rejected, draftState => {
      draftState.loading = false;
    });
};
