import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { agencyRecallRequestActions } from '.';
import { apiErrorDetailActions } from 'pages/__commons/ApiErrorDetail/_redux/reducer';

import agencyManagementServices from 'pages/AgencyManagement/agencyManagementServices';
import { recallRequestController } from '../__mock__/recallRequestController';

import { formatTime } from 'app/helpers';

// constants & types
import { I18N_TEXT } from 'app/constants';
import { ADD_RECALL_REQUEST } from '../constants';
import { IAgencyRecallRequestState, IRecallRequest } from '../types';
import { TODAY } from 'pages/AgencyManagement/components/SaveAgencyRequestModal/constants';

export const addRecallRequest = createAsyncThunk<
  void,
  { newRecall: IRecallRequest },
  ThunkAPIConfig
>(ADD_RECALL_REQUEST, async (args, { dispatch, rejectWithValue }) => {
  try {
    const { newRecall } = args;
    const { data: saveNumberData } =
      await agencyManagementServices.getSaveNumbers();
    const description =
      (saveNumberData as MagicKeyValue)?.saveNumber.find(
        (item: RefDataValue) => item.value === newRecall.saveNumber
      )?.description || '';
    const saveNumberActived = (
      saveNumberData as MagicKeyValue
    )?.saveNumber.find(
      (item: RefDataValue) =>
        item.value === newRecall.saveNumber && item.isActive
    );
    const { success } = await recallRequestController.addRecallRequest({
      ...newRecall,
      description,
      nextRunDate: formatTime(newRecall.nextRunDate)?.date || '',
      lastRunDate: saveNumberActived ? formatTime(TODAY)?.date : ''
    });

    if (!success) throw new Error('Add recall request failed.');

    batch(() => {
      dispatch(agencyRecallRequestActions.toggleForm());
      dispatch(
        agencyRecallRequestActions.getRecallRequests({ isRefresh: true })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.ADD_RECALL_SUCCESS
        })
      );
    });
  } catch (error) {
    if (error?.message === 'ADD FAILED') {
      dispatch(
        agencyRecallRequestActions.setInlineErrMsg(
          addRecallRequest.rejected.type
        )
      );
      dispatch(
        apiErrorDetailActions.setErrValue({
          id: addRecallRequest.rejected.type,
          value: 'Something when wrong when adding!'
        })
      );
    }

    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.ADD_RECALL_FAILED
      })
    );
    return rejectWithValue(error);
  }
});

export const addRecallRequestBuilder = (
  builder: ActionReducerMapBuilder<IAgencyRecallRequestState>
) => {
  const { pending, fulfilled, rejected } = addRecallRequest;
  builder
    .addCase(pending, draftState => {
      draftState.loading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      draftState.loading = false;
    })
    .addCase(rejected, draftState => {
      draftState.loading = false;
    });
};
