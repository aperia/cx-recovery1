import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

import { checkEmpty, isArrayHasValue } from 'app/helpers';
import { GET_RECALL_REQUESTS } from '../constants';
import { IPayloadGetRecallRequests, IAgencyRecallRequestState } from '../types';
import { recallRequestController } from '../__mock__/recallRequestController';

const STEP_SEQUENCE = 25;
export const getRecallRequests = createAsyncThunk<
  IPayloadGetRecallRequests,
  { isRefresh: boolean } | undefined,
  ThunkAPIConfig
>(GET_RECALL_REQUESTS, async (args, thunkAPI) => {
  const { agencyRecallRequest } = thunkAPI.getState();
  const { startSequence, endSequence } = agencyRecallRequest;
  const { data } = await recallRequestController.getRecallRequests({
    startSequence: checkEmpty([args?.isRefresh, startSequence, 1]),
    endSequence: checkEmpty([args?.isRefresh, endSequence, STEP_SEQUENCE])
  });

  return {
    recallRequests: data.recallRequests
  };
});

export const getRecallRequestsBuilder = (
  builder: ActionReducerMapBuilder<IAgencyRecallRequestState>
) => {
  const { pending, fulfilled, rejected } = getRecallRequests;
  builder
    .addCase(pending, (draftState, action) => {
      if (draftState.startSequence === 1 || action.meta.arg?.isRefresh) {
        draftState.loading = true;
      } else {
        draftState.isLoadingLoadMore = true;
      }
    })
    .addCase(fulfilled, (draftState, action) => {
      const isRefresh = action.meta.arg?.isRefresh;
      if (draftState.startSequence === 1 || isRefresh) {
        draftState.loading = false;
      } else {
        draftState.isLoadingLoadMore = false;
      }
      draftState.isEnd = !isArrayHasValue(action.payload.recallRequests);
      draftState.data = checkEmpty([
        isRefresh,
        [...draftState.data, ...action.payload.recallRequests],
        action.payload.recallRequests
      ]);
      draftState.endSequence = checkEmpty([
        isRefresh,
        draftState.endSequence + STEP_SEQUENCE - 1,
        2 * STEP_SEQUENCE - 1
      ]);
      draftState.startSequence = checkEmpty([
        isRefresh,
        draftState.startSequence + STEP_SEQUENCE,
        STEP_SEQUENCE + 1
      ]);
    })
    .addCase(rejected, draftState => {
      draftState.loading = false;
    });
};
