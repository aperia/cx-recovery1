import { I18N_TEXT } from 'app/constants';
import React from 'react';
import { IAssignRequest } from './AgencyAssignRequest/types';
import { IRecallRequest } from './AgencyRecallRequest/types';
import { ISectionItem } from './types';

export const AGENCY_MAINTENANCE = 'AMN';
export const AGENCY_ASSIGN_REQUEST = 'AAR';
export const AGENCY_RECALL_REQUEST = 'ARR';

export const SectionsMap = new Map<string, { component: React.FC<unknown> }>([
  [
    AGENCY_MAINTENANCE,
    {
      component: React.lazy(() => import('./AgencyMaintenance'))
    }
  ],
  [
    AGENCY_ASSIGN_REQUEST,
    {
      component: React.lazy(() => import('./AgencyAssignRequest'))
    }
  ],
  [
    AGENCY_RECALL_REQUEST,
    {
      component: React.lazy(() => import('./AgencyRecallRequest'))
    }
  ]
]);

const EmptyComponent: React.FC = () => {
  return <div></div>;
};

export const mapSectionList = (sections: RefDataValue[]): ISectionItem[] => {
  return sections.map(item => {
    return {
      id: item.value,
      title: item.description,
      Component: SectionsMap.get(item.value)?.component || EmptyComponent
    };
  });
};

export const isBeforeToday = (date: Date) => {
  return new Date(date.toDateString()) < new Date(new Date().toDateString());
};

export const formatFrequency = (item: IAssignRequest | IRecallRequest) => {
  if (item.frequency !== 'Custom') {
    if (item.frequency === '0') return I18N_TEXT.ONCE;
    return `${item.frequency} day${+item.frequency > 1 ? 's' : ''}`;
  } else {
    if (item.count === '0') return I18N_TEXT.ONCE;
    return `${item.count} day${+item.count! > 1 ? 's' : ''}`;
  }
};
