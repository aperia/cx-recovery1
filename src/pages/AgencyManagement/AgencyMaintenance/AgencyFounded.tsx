import React from 'react';

// components

import { Button } from 'app/_libraries/_dls/components';

// redux
import { useDispatch, useSelector } from 'react-redux';
import {
  selectorSearchAgencies,
  selectorIsSearching
} from './_redux/selectors';
import { agencyMaintenanceActions } from './_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers

import { I18N_TEXT } from 'app/constants';

export interface AgencyFoundedProps {}

const AgencyFounded: React.FC<AgencyFoundedProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const agencies = useSelector(selectorSearchAgencies);
  const isSearching = useSelector(selectorIsSearching);

  const handleClearAndReset = () => {
    dispatch(agencyMaintenanceActions.onClearSearch());
  };

  if (!isSearching) return null;

  return (
    <div className="d-flex justify-content-between align-items-center mb-16">
      <p>{t(I18N_TEXT.RESULT_FOUNDED, { count: agencies.length })}</p>
      <div className="text-right">
        <Button
          onClick={handleClearAndReset}
          variant="outline-primary"
          size="sm"
          className="mr-n8"
        >
          {t(I18N_TEXT.CLEAR_AND_RESET)}
        </Button>
      </div>
    </div>
  );
};

export default AgencyFounded;
