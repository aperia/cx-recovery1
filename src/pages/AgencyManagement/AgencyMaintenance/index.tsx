import React, {
  Fragment,
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef
} from 'react';

// components
import Agencies from './Agencies';
import SearchAgency from './Search';
import AddAgency from './AddAgency';
import { SimpleBar } from 'app/_libraries/_dls/components';
import SetUpAgencyManagement from './SetUpAgencyMaintenance';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { selectorIsError, selectorIsLoading } from './_redux/selectors';
import { agencyMaintenanceActions } from './_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { I18N_TEXT } from 'app/constants';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { DATA_TEST_ID_AGENCIES } from './helpers';
import FailedApiReload from 'app/components/FailedApiReload';

export interface AgencyMaintenanceProps {}

const AgencyManagement: React.FC<AgencyMaintenanceProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const isLoading = useSelector(selectorIsLoading);
  const isError = useSelector(selectorIsError);
  const divRef = useRef<HTMLDivElement | null>(null);

  const getAgency = useCallback(() => {
    dispatch(agencyMaintenanceActions.getAgency());
  }, [dispatch]);

  useEffect(() => {
    getAgency();
    return () => {
      dispatch(agencyMaintenanceActions.onUnmount());
    };
  }, [getAgency, dispatch]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100% - 81px)`);
  }, []);

  const renderUI = () => {
    if (isLoading) return <div className="loading" />;

    if (isError)
      return (
        <FailedApiReload
          id="agencies-failed-reload"
          dataTestId={`agencies_failed-api`}
          onReload={getAgency}
        />
      );

    return (
      <Fragment>
        <div className="d-flex justify-content-between align-items-center mb-16">
          <h5 data-testid={genAmtId(DATA_TEST_ID_AGENCIES, 'title', '')}>
            {t(I18N_TEXT.AGENCY_LIST)}
          </h5>
          <div className="d-flex align-items-center">
            <SearchAgency />
            <AddAgency />
            <SetUpAgencyManagement />
          </div>
        </div>
        <Agencies />
      </Fragment>
    );
  };

  return (
    <div ref={divRef}>
      <SimpleBar>
        <div className="px-24 py-22 max-width-lg mx-auto">{renderUI()}</div>
      </SimpleBar>
    </div>
  );
};

export default AgencyManagement;
