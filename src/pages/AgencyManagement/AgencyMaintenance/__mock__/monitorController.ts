import { I18N_TEXT } from 'app/constants';
import { IMonitorCriteria } from 'pages/__commons/MonitorMaintenance/types';
import monitors from './monitors.json';

let monitorData: IMonitorCriteria[] = JSON.parse(JSON.stringify(monitors));

const isHaveError = window.location.href.includes('error');

export const monitorController = {
  getMonitors: ({ page }: { page: number }) => {
    try {
      const returnDataByPage = () => {
        const ITEMS_PER_PAGE = 10;
        const start = ITEMS_PER_PAGE * (page - 1);
        const end = ITEMS_PER_PAGE * page;
        return [...monitorData.slice(start, end)] as IMonitorCriteria[]
      }
      return Promise.resolve({
        success: true,
        data: returnDataByPage(),
        totalItem: monitorData.length,
        currentPage: page
      });
    } catch (error) {
      return Promise.reject({
        response: {
          data: {
            success: false,
            message: 'Failed to get monitor data'
          }
        }
      });
    }
  },
  saveMonitor: (monitor: any) => {
    try {
      if (monitor.id) {
        const editIndex = monitorData.findIndex(item => item.id === monitor.id);
        if (isHaveError)
          return Promise.reject({
            data: {
              success: false,
              message: 'Monitor criteria failed to update.'
            }
          });
        monitorData = [
          ...monitorData.slice(0, editIndex),
          monitor,
          ...monitorData.slice(editIndex + 1)
        ];
        return Promise.resolve({
          success: true
        });
      } else {
        if (isHaveError)
          return Promise.reject({
            data: { success: false, message: 'Monitor criteria failed to add.' }
          });
        monitorData = [
          ...monitorData,
          { ...monitor, id: new Date().getTime() }
        ];
        return Promise.resolve({
          success: true
        });
      }
    } catch (error) {
      return Promise.reject({
        response: {
          data: {
            success: false,
            message: error
          }
        }
      });
    }
  },
  deleteMonitor: ({ id }: { id: string }) => {
    monitorData = monitorData.filter(
      (item: any) => item.id !== id
    ) as IMonitorCriteria[];
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: I18N_TEXT.DELETE_MONITOR_FAIL
      });
    return Promise.resolve({ success: true, data: monitorData });
  }
};
