import { I18N_TEXT } from 'app/constants';
import { isEqual } from 'app/helpers';
import { IReturnCriteria } from 'pages/__commons/ReturnMaintenance/types';
import returns from './returns.json';

let returnData = JSON.parse(JSON.stringify(returns));

const isHaveError = window.location.href.includes('error');

export const returnController = {
  getReturns: ({ page }: { page: number }) => {
    try {
      const returnDataByPage = () => {
        const ITEMS_PER_PAGE = 10;
        const start = ITEMS_PER_PAGE * (page - 1);
        const end = ITEMS_PER_PAGE * page;
        return [...returnData.slice(start, end)];
      };
      return Promise.resolve({
        success: true,
        data: returnDataByPage() as IReturnCriteria[],
        totalItem: returnData.length,
        currentPage: page
      });
    } catch (error) {
      return Promise.reject({
        response: {
          data: {
            success: false,
            message: 'Failed to get return data'
          }
        }
      });
    }
  },
  addReturn({
    returnCriteria,
    agencyCode
  }: {
    returnCriteria: any;
    agencyCode: string;
  }) {
    // Exist calendaCode && noteCodes
    const isExistsData = returnData.find(
      (item: any) =>
        returnCriteria.calendarCode === item.calendarCode &&
        returnCriteria.noteCode === item.noteCode
    );
    if (isExistsData) {
      return Promise.reject({
        response: {
          data: {
            success: false,
            message: '1'
          }
        }
      });
    }
    // TODO: handle mutation...
    returnData = [
      ...returnData,
      { ...returnCriteria, id: Date.now(), agencyCode }
    ];
    if (isHaveError)
      return Promise.reject({
        data: { success: false, message: 'Return criteria failed to add.' }
      });
    return Promise.resolve({ data: { success: true } });
  },
  editReturn: (newReturn: any, id: string, agencyCode: string) => {
    if (isHaveError)
      return Promise.reject({
        data: { success: false, message: 'Return criteria failed to update.' }
      });
    const editIndex = returnData.findIndex((item: any) => item.id === id);
    if (isEqual(returnData[editIndex], { ...newReturn, id })) {
      return Promise.resolve({
        success: true
      });
    }
    const splitArr = [
      ...returnData.slice(0, editIndex),
      ...returnData.slice(editIndex + 1)
    ];
    const isExistsData = splitArr.find(
      (item: any) =>
        returnData.length > 1 &&
        newReturn.calendarCode === item.calendarCode &&
        newReturn.noteCode === item.noteCode
    );
    if (isExistsData) {
      return Promise.reject({
        response: {
          data: {
            success: false,
            message: '1'
          }
        }
      });
    } else {
      returnData = [
        ...returnData.slice(0, editIndex),
        { id, ...newReturn },
        ...returnData.slice(editIndex + 1)
      ];
    }

    return Promise.resolve({
      success: true
    });
  },
  deleteReturn: ({ id }: { id: string }) => {
    returnData = returnData.filter(
      (item: any) => item.id !== id
    ) as IReturnCriteria[];
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: I18N_TEXT.DELETE_RETURN_FAIL
      });
    return Promise.resolve({ success: true, data: returnData });
  }
};
