import agenciesJson from './agencies.json';
import feeInformationJson from './feeInfomation.json';
import { AgencyFormValues } from 'pages/__commons/Agency/AgencyForm/types';
let agenciesData = JSON.parse(JSON.stringify(agenciesJson));
const feeInformationData = JSON.parse(JSON.stringify(feeInformationJson));

let agencyDetail: any;

export const agencyMaintenanceController = {
  getAgency({
    startSequence,
    endSequence
  }: {
    startSequence: number;
    endSequence: number;
  }) {
    agenciesData = agenciesData.sort((a: any, b: any) =>
      a.agencyCode > b.agencyCode ? 1 : -1
    );
    const agencies = agenciesData.slice(startSequence - 1, endSequence);
    return Promise.resolve({ data: { success: true, agencies } });
    // const url: string = window.appConfig?.api?.agencyManagement?.getAgencies;
    // return apiService.get<IAgency[]>(checkEmpty([url, '']));
  },

  addAgency({ agency }: { agency: AgencyFormValues }) {
    const isExists =
      agenciesData.findIndex(
        (item: AgencyFormValues) => item.agencyCode === agency.agencyCode
      ) > -1;
    if (isExists) {
      return Promise.reject({
        response: {
          data: {
            success: false,
            message: 'Agency already exists'
          }
        }
      });
    }
    agenciesData.push(agency);
    return Promise.resolve({ data: { success: true } });

    // const url: string = window.appConfig?.api?.agencyManagement?.addAgency;

    // return apiService.post<HttpResponse>(checkEmpty([url, '']), data);
  },

  deleteAgency: ({ id }: { id: string }) => {
    agenciesData = agenciesData.filter(
      (agency: any) => agency.agencyCode !== id
    );

    return Promise.resolve({ data: { success: true, data: agenciesData } });
  },

  editAgency: ({ prevAgency, agency }: { prevAgency: string; agency: any }) => {
    const agencyIndex = agenciesData.findIndex(
      (a: any) => a.agencyCode === prevAgency
    );
    const isExists =
      agenciesData.findIndex(
        (item: any) =>
          item.agencyCode === agency.agencyCode &&
          agency.agencyCode !== prevAgency
      ) > -1;
    if (isExists) {
      return Promise.reject({
        response: {
          data: {
            success: false,
            message: 'Agency already exists'
          }
        }
      });
    }
    agenciesData[agencyIndex] = agency;

    return Promise.resolve({
      data: { success: true }
    });
  },

  getAgencyDetail: ({ agencyCode }: { agencyCode: string }) => {
    const agency = agenciesData.find(
      (item: any) => item.agencyCode === agencyCode
    );
    return Promise.resolve({ data: { success: true, agency } });
  },
  // Return Criteria
  addReturn({
    returnCriteria,
    agencyCode
  }: {
    returnCriteria: any;
    agencyCode: string;
  }) {
    const isExists =
      agenciesData.findIndex((item: any) => item.agencyCode === agencyCode) >
      -1;
    if (!isExists) {
      return Promise.reject({
        response: {
          data: {
            success: false,
            message: 'Agency not exists!'
          }
        }
      });
    }
    const agencyData = agenciesData.find(
      (item: any) => item.agencyCode === agencyCode
    );
    // Exist calendaCode && noteCodes
    const isExistsData = agencyData.returnCriterialList.find(
      (item: any) =>
        returnCriteria.calendarCode === item.calendarCode ||
        returnCriteria.noteCode === item.noteCode
    );
    if (isExistsData) {
      return Promise.reject({
        response: {
          data: {
            success: false,
            message: 'This return criteria already exists.'
          }
        }
      });
    }
    // TODO: handle mutaion...

    return Promise.resolve({ data: { success: true } });
  },
  getReturnDetail: ({ agencyCode }: { agencyCode: string }) => {
    agencyDetail = agenciesData.find(
      (item: any) => item.agencyCode === agencyCode
    ) as any;

    return Promise.resolve({
      data: { success: true, returnCriteria: agencyDetail.returnCriterialList }
    });
  },
  deleteMonitor({ id }: { id: string }) {
    const monitorCriterialList = agencyDetail?.monitorCriterialList?.filter(
      (it: any) => it.calendarCode !== id
    );
    return Promise.resolve({ data: { ...agencyDetail, monitorCriterialList } });
  },
  deleteReturn({ id }: { id: string }) {
    const returnCriterialList = agencyDetail?.returnCriterialList?.filter(
      (it: any) => it.calendarCode !== id
    );
    return Promise.resolve({ data: { ...agencyDetail, returnCriterialList } });
  },
  getFeeInformation({ agencyCode }: { agencyCode: string }) {
    return Promise.resolve({ data: feeInformationData, success: true });
  }
};
