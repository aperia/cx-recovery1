import React from 'react';

// components
import LoadMore from 'app/components/LoadMore';

// redux
import { useDispatch, useSelector } from 'react-redux';

// helper
import { agencyMaintenanceActions } from './_redux';
import { selectorIsEnd, selectorIsLoadingLoadMore } from './_redux/selectors';

export interface LoadMoreAgencyProps {}

const LoadMoreAgency: React.FC<LoadMoreAgencyProps> = () => {
  const dispatch = useDispatch();
  const isEnd = useSelector(selectorIsEnd);
  const isLoading = useSelector(selectorIsLoadingLoadMore);

  const handleOnLoadMore = () => {
    dispatch(agencyMaintenanceActions.getAgency());
  };

  return (
    <LoadMore
      dataTestId="agencies-load-more"
      loading={isLoading}
      onLoadMore={handleOnLoadMore}
      isEnd={isEnd}
    />
  );
};

export default LoadMoreAgency;
