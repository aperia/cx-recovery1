import { orderBy } from 'app/helpers';
import { IAgency } from './types';

export const DATA_TEST_ID_AGENCIES = 'agencies';

export const handleSort = (sort: ISortType[], data: IAgency[]) => {
  if (!data || !data.length) return [];
  return orderBy(
    data,
    [
      item => {
        if (sort[0].id === 'agencyType') return item.agencyType;
        return item[sort[0].id];
      }
    ],
    [sort[0].order || 'asc']
  );
};
