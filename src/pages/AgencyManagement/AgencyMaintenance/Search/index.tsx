import React, { useCallback, useState } from 'react';

// component
import { Icon, Popover } from 'app/_libraries/_dls/components';
import Button from 'app/_libraries/_dls/components/Button';
import ElementPopover from './Element';

// redux
import { useSelector } from 'react-redux';
import { selectorIsSearching } from '../_redux/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { I18N_TEXT } from 'app/constants';
import classNames from 'classnames';

export interface SearchAgencyProps {}

const SearchAgency: React.FC<SearchAgencyProps> = () => {
  const { t } = useTranslation();
  const [isActive, setActive] = useState(false);
  const isDisplayIndicator = useSelector(selectorIsSearching);

  const handleSetActive = useCallback((opened: boolean) => {
    setActive(opened);
  }, []);

  return (
    <Popover
      size="md"
      opened={isActive}
      onVisibilityChange={opened => handleSetActive(opened)}
      placement="bottom-end"
      keepMount
      element={<ElementPopover onHandleSetActive={handleSetActive} />}
      triggerClassName="mr-8"
    >
      <Button
        className={classNames({
          asterisk: isDisplayIndicator,
          active: isActive
        })}
        onClick={() => handleSetActive(!isActive)}
        variant="outline-primary"
        size="sm"
      >
        <Icon name="search" />
        {t(I18N_TEXT.SEARCH)}
      </Button>
    </Popover>
  );
};

export default SearchAgency;
