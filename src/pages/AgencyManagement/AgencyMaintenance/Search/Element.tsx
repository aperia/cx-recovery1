import React, { Fragment, useEffect, useState } from 'react';

// components
import {
  Button,
  DropdownBaseChangeEvent,
  TextBox
} from 'app/_libraries/_dls/components';
import MultiSelect from 'app/_libraries/_dls/components/MultiSelect';

// redux
import { selectorAgencySearch } from '../_redux/selectors';
import { useDispatch, useSelector } from 'react-redux';
import { agencyMaintenanceActions, defaultSearch } from '../_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helper
import { REGEX_NON_SPECIAL_CHARACTERS, I18N_TEXT } from 'app/constants';
import { ISearchAgency } from '../types';
import { equal } from 'app/helpers';

export interface ElementPopoverProps {
  onHandleSetActive: (opened: boolean) => void;
}

const NAME_ELEMENT = {
  AGENCY_CODE: 'agencyCode',
  AGENCY_TYPE: 'agencyType',
  COMPANY_NAME: 'companyName'
};

const agencyTies: RefDataValue[] = [...defaultSearch.agencyType];

let searching: ISearchAgency = defaultSearch;

const ElementPopover: React.FC<ElementPopoverProps> = ({
  onHandleSetActive
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [search, setSearch] = useState(defaultSearch);

  const originalSearch = useSelector(selectorAgencySearch);

  const handleChange = (
    e: React.ChangeEvent<HTMLInputElement> | DropdownBaseChangeEvent
  ) => {
    const { value, name } = e.target;
    if (
      name === NAME_ELEMENT.AGENCY_CODE &&
      REGEX_NON_SPECIAL_CHARACTERS.test(value)
    ) {
      return;
    }
    setSearch(pre => ({ ...pre, [name]: value }));
  };

  useEffect(() => {
    if (!equal(originalSearch, searching)) {
      setSearch(defaultSearch);
      searching = defaultSearch;
    }
  }, [originalSearch]);

  const handleApply = () => {
    dispatch(agencyMaintenanceActions.onChangeSearch(search));
    onHandleSetActive(false);
    searching = search;
  };

  const handleReset = () => {
    setSearch(defaultSearch);
  };

  return (
    <Fragment>
      <h4 className="mb-16">{t(I18N_TEXT.SEARCH)}</h4>
      <TextBox
        className="mb-16"
        name={NAME_ELEMENT.AGENCY_CODE}
        label={t(I18N_TEXT.AGENCY_CODE)}
        maxLength={6}
        value={search.agencyCode}
        onChange={handleChange}
      />
      <MultiSelect
        name={NAME_ELEMENT.AGENCY_TYPE}
        value={search.agencyType}
        textField="description"
        onChange={handleChange}
        label={t(I18N_TEXT.AGENCY_TYPE)}
        variant="group"
        checkAll
      >
        {agencyTies?.map(item => (
          <MultiSelect.Item
            key={item.value}
            label={item.description}
            value={item}
            variant="checkbox"
          />
        ))}
      </MultiSelect>
      <TextBox
        className="mt-16"
        name={NAME_ELEMENT.COMPANY_NAME}
        value={search.companyName}
        label={t(I18N_TEXT.COMPANY_NAME)}
        onChange={handleChange}
        maxLength={40}
      />
      <div className="mt-24 d-flex justify-content-end">
        <Button onClick={handleReset} variant="secondary" size="sm">
          {t(I18N_TEXT.RESET_TO_DEFAULT)}
        </Button>
        <Button onClick={handleApply} variant="primary" size="sm">
          {t(I18N_TEXT.APPLY)}
        </Button>
      </div>
    </Fragment>
  );
};

export default ElementPopover;
