import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { AgencyFormValues } from 'pages/__commons/Agency/AgencyForm/types';
import { ISearchAgency, IStateReducer } from '../types';
import { deleteAgency, deleteAgencyBuilder } from './deleteAgency';
import { deleteMonitor, deleteMonitorBuilder } from './deleteMonitor';
import { deleteReturn, deleteReturnBuilder } from './deleteReturn';
import { getAgency, getAgencyBuilder } from './getAgency';
import { getMonitors, getMonitorsBuilder } from './getMonitors';
import { getReturns, getReturnsBuilder } from './getReturns';
import { saveMonitor } from './saveMonitor';
import { saveReturn, saveReturnBuilder } from './saveReturn';

export const defaultSearch: ISearchAgency = {
  agencyCode: '',
  companyName: '',
  agencyType: [
    { value: '1', description: '1 - Collection Agency' },
    { value: '2', description: '2 - Lawyer' },
    { value: '3', description: '3 - Inhouse' },
    { value: '4', description: '4 - User-defined codes' },
    { value: '5', description: '5 - User-defined codes' },
    { value: '6', description: '6 - User-defined codes' },
    { value: '7', description: '7 - User-defined codes' },
    { value: '8', description: '8 - User-defined codes' },
    { value: '9', description: '9 - Pending' },
    { value: 'S', description: 'S - Service' }
  ]
};

export const initialState: IStateReducer = {
  agencies: [],
  startSequence: 1,
  endSequence: 25,
  isLoading: false,
  isEnd: false,
  isLoadingLoadMore: false,
  isError: false,
  sort: [{ id: 'agencyCode', order: undefined }],
  search: {
    ...defaultSearch
  },
  monitorMaintenance: {
    data: [],
    totalItem: 0,
    loading: false,
    currentPage: 1
  },
  returnMaintenance: {
    data: [],
    totalItem: 0,
    loading: false,
    currentPage: 1
  }
};

export const { actions, reducer } = createSlice({
  name: 'agencyMaintenance',
  initialState: initialState,
  reducers: {
    onChangeSearch: (draftState, action: PayloadAction<ISearchAgency>) => {
      draftState.search = action.payload;
    },
    onChangeSort: (draftState, action: PayloadAction<ISortType[]>) => {
      draftState.sort = action.payload;
    },
    onClearSearch: draftState => {
      draftState.search = defaultSearch;
    },
    onToggleSetUpAgency: (
      draftState,
      action: PayloadAction<AgencyFormValues | undefined>
    ) => {
      draftState.setUpModal = {
        ...draftState.setUpModal,
        open: !draftState.setUpModal?.open,
        agency: action.payload
      };
    },
    onUnmount: () => initialState
  },
  extraReducers: builder => {
    getAgencyBuilder(builder);
    deleteAgencyBuilder(builder);
    getMonitorsBuilder(builder);
    getReturnsBuilder(builder);
    saveReturnBuilder(builder);
    deleteReturnBuilder(builder);
    deleteMonitorBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getAgency,
  deleteAgency,
  getMonitors,
  saveMonitor,
  getReturns,
  saveReturn,
  deleteReturn,
  deleteMonitor
};

export { combineActions as agencyMaintenanceActions };
