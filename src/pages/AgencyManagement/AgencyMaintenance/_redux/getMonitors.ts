import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import agencyMaintenanceServices from '../agencyMaintenanceServices';
import {
  GET_MONITORS,
  IArgGetMonitors,
  IPayloadGetMonitors,
  IStateReducer
} from '../types';

export const getMonitors = createAsyncThunk<
  IPayloadGetMonitors,
  IArgGetMonitors,
  ThunkAPIConfig
>(GET_MONITORS, async (args, { rejectWithValue }) => {
  const { page = 1 } = args;

  try {
    return await agencyMaintenanceServices.getMonitors({ page });
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getMonitorsBuilder = (
  builder: ActionReducerMapBuilder<IStateReducer>
) => {
  const { pending, fulfilled, rejected } = getMonitors;
  builder
    .addCase(pending, draftState => {
      draftState.monitorMaintenance.loading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      const { data, totalItem, currentPage } = action.payload;

      draftState.monitorMaintenance = {
        data,
        totalItem,
        loading: false,
        currentPage
      };
    })
    .addCase(rejected, draftState => {
      draftState.monitorMaintenance.loading = false;
    });
};
