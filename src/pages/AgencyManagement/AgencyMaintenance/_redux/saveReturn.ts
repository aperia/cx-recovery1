import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { checkNextPage } from 'pages/__commons/helpers';
import { IReturnCriteria } from 'pages/__commons/ReturnMaintenance/types';
import { returnMaintenanceActions } from 'pages/__commons/ReturnMaintenance/_redux/reducers';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { agencyMaintenanceActions } from '.';
import agencyMaintenanceServices from '../agencyMaintenanceServices';
import { ISaveReturnArgs, IStateReducer, SAVE_RETURN } from '../types';

export const saveReturn = createAsyncThunk<
  void,
  ISaveReturnArgs,
  ThunkAPIConfig
>(SAVE_RETURN, async (args, { dispatch, getState, rejectWithValue }) => {
  const { currentPage = 1, totalItem = 0 } =
    getState().agencyMaintenance.returnMaintenance;

  try {
    const { id, agencyCode, formType } = args;
    const reqBody: IReturnCriteria = {
      calendarCode: args.calendarCode,
      noteCode: args.noteCode,
      calendarInterval: args.calendarInterval,
      calendarIntervalType: args.calendarIntervalType,
      nsfDeleteCalendarCode: args.nsfDeleteCalendarCode,
      paymentDeleteCalendarCode: args.paymentDeleteCalendarCode
    };
    if (id && formType === 'EDIT') {
      await agencyMaintenanceServices.editReturn(reqBody, id, agencyCode);
    }
    if (formType === 'ADD') {
      await agencyMaintenanceServices.addReturn({
        returnCriteria: reqBody,
        agencyCode
      });
    }

    batch(() => {
      dispatch(returnMaintenanceActions.closeForm());

      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: args.id
            ? 'txt_return_criteria_updated'
            : 'txt_return_criteria_added'
        })
      );

      dispatch(
        agencyMaintenanceActions.getReturns({
          page: checkNextPage({ currentPage, totalItem })
        })
      );
    });
  } catch (error) {
    if (error?.response?.data?.message === '0') {
      dispatch(
        returnMaintenanceActions.setInlineErrMsg('txt_agency_code_not_exists')
      );
    }
    if (error?.response?.data?.message === '1') {
      dispatch(
        returnMaintenanceActions.setInlineErrMsg('txt_return_criteria_exists')
      );
    } else {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: args.id
            ? 'txt_return_criteria_update_fail'
            : 'txt_return_criteria_add_fail'
        })
      );
      return rejectWithValue({ response: error });
    }
  }
});

export const saveReturnBuilder = (
  builder: ActionReducerMapBuilder<IStateReducer>
) => {
  const { pending, fulfilled, rejected } = saveReturn;
  builder
    .addCase(pending, draftState => {
      draftState.returnMaintenance.loading = true;
    })
    .addCase(fulfilled, draftState => {
      draftState.returnMaintenance.loading = false;
    })
    .addCase(rejected, draftState => {
      draftState.returnMaintenance.loading = false;
    });
};
