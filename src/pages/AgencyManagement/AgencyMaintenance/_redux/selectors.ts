import { createSelector } from '@reduxjs/toolkit';
import { DEFAULT_PAGE_NUMBER, DEFAULT_PAGE_SIZE } from 'app/_libraries/_dls/components/Pagination/constants';
import { defaultSearch } from '.';
import { handleSort } from '../helpers';
import {
  IAgency,
  IMonitorMaintenance,
  IReturnMaintenance,
  ISearchAgency,
  IStateReducer
} from '../types';

const pageNumber = DEFAULT_PAGE_NUMBER
const pageSize = DEFAULT_PAGE_SIZE

export const selectorSearchAgencies = createSelector(
  [
    (state: RootState) => state.agencyMaintenance.agencies,
    (state: RootState) => state.agencyMaintenance.search,
    (state: RootState) => state.agencyMaintenance.sort
  ],
  (agencies: IAgency[], search: ISearchAgency, sort: ISortType[]) => {
    const { agencyCode, companyName, agencyType } = search;
    let data: IAgency[] = [];
    if (
      !agencyCode &&
      !companyName &&
      agencyType.length === defaultSearch.agencyType.length
    ) {
      data = agencies;
    } else {
      const filterType = agencyType.map(r => r.value);
      data = agencies.filter(r => {
        const isAgencyCode = r.agencyCode
          .toLowerCase()
          .includes(agencyCode?.toLowerCase());

        const isCompanyName = r.companyName
          .toLowerCase()
          .includes(companyName?.toLowerCase());

        const isAgencyType = filterType.includes(r.agencyType);
        return isCompanyName && isAgencyCode && isAgencyType;
      });
    }

    return handleSort(sort, data);
  }
);

export const selectorAgencySearch = createSelector(
  [(state: RootState) => state.agencyMaintenance.search],
  (search: ISearchAgency) => search
);

export const selectorSortAgencies = createSelector(
  [(state: RootState) => state.agencyMaintenance.sort],
  (sort: ISortType[]) => sort
);

export const selectorIsSearching = createSelector(
  [(state: RootState) => state.agencyMaintenance.search],
  (search: ISearchAgency) => {
    const { agencyCode, companyName, agencyType } = search;
    if (
      !agencyCode &&
      !companyName &&
      agencyType.length === defaultSearch.agencyType.length
    )
      return false;
    return true;
  }
);

export const selectorIsLoading = createSelector(
  [(state: RootState) => state.agencyMaintenance.isLoading],
  (isLoading: boolean) => isLoading
);

export const selectorIsError = createSelector(
  [(state: RootState) => state.agencyMaintenance.isError],
  (isError: boolean) => isError
);

export const selectorIsEnd = createSelector(
  [(state: RootState) => state.agencyMaintenance.isEnd],
  (isEnd: boolean) => isEnd
);

export const selectorIsLoadingLoadMore = createSelector(
  [(state: RootState) => state.agencyMaintenance.isLoadingLoadMore],
  (isLoadingLoadMore: boolean) => isLoadingLoadMore
);

export const getAgencyMaintenanceStates = (state: RootState) =>
  state.agencyMaintenance;

// temp
export const selectedSetUpAgencyModal = createSelector(
  getAgencyMaintenanceStates,
  (data: IStateReducer) => data.setUpModal
);

export const getMonitorMaintenance = (state: RootState) =>
  state.agencyMaintenance.monitorMaintenance;

export const getReturnMaintenance = (state: RootState) =>
  state.agencyMaintenance.returnMaintenance;

export const selectedMonitorData = createSelector(
  getMonitorMaintenance,
  (data: IMonitorMaintenance) => {
    const start = pageSize * (pageNumber - 1);
    const end = pageSize * pageNumber;
    return [...data.data.slice(start, end)];
  }
);

export const selectedMonitorTotalData = createSelector(
  getMonitorMaintenance,
  (data: IMonitorMaintenance) => data.totalItem || 0
);

export const selectedMonitorCurrentPage = createSelector(
  getMonitorMaintenance,
  (data: IMonitorMaintenance) => data.currentPage || 0
);

export const selectedReturnData = createSelector(
  getReturnMaintenance,
  (data: IReturnMaintenance) => {
    const start = pageSize * (pageNumber - 1);
    const end = pageSize * pageNumber;
    return [...data.data.slice(start, end)];
  }
);

export const selectedReturnTotalData = createSelector(
  getReturnMaintenance,
  (data: IReturnMaintenance) => data.totalItem || 0
);

export const selectedReturnCurrentPage = createSelector(
  getReturnMaintenance,
  (data: IReturnMaintenance) => data.currentPage || 0
);

export const selectedMonitorLoading = createSelector(
  getMonitorMaintenance,
  (data: IMonitorMaintenance) => data.loading
);

export const selectedReturnLoading = createSelector(
  getReturnMaintenance,
  (data: IReturnMaintenance) => data.loading
);

export const selectedSetUpAgency = createSelector(
  getAgencyMaintenanceStates,
  (data: IStateReducer) => data.setUpModal?.agency
);


