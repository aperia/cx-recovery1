import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import agencyMaintenanceServices from '../agencyMaintenanceServices';
import {
  GET_RETURNS,
  IArgGetReturns,
  IPayloadGetReturns,
  IStateReducer
} from '../types';

export const getReturns = createAsyncThunk<
  IPayloadGetReturns,
  IArgGetReturns,
  ThunkAPIConfig
>(GET_RETURNS, async (args, thunkAPI) => {
  const { page = 1 } = args;

  try {
    return await agencyMaintenanceServices.getReturns({ page });
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const getReturnsBuilder = (
  builder: ActionReducerMapBuilder<IStateReducer>
) => {
  const { pending, fulfilled, rejected } = getReturns;
  builder
    .addCase(pending, draftState => {
      draftState.returnMaintenance.loading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      const { data, totalItem, currentPage } = action.payload;

      draftState.returnMaintenance = {
        data,
        totalItem,
        loading: false,
        currentPage
      };
    })
    .addCase(rejected, draftState => {
      draftState.returnMaintenance.loading = false;
    });
};
