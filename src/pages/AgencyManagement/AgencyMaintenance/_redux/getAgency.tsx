import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { GET_AGENCY, IPayloadGetAgency, IStateReducer } from '../types';
import { checkEmpty, isArrayHasValue } from 'app/helpers/commons';
import { STEP_SEQUENCE } from 'app/constants';
import agencyMaintenanceServices from '../agencyMaintenanceServices';

export const getAgency = createAsyncThunk<
  IPayloadGetAgency,
  { isRefresh: boolean } | undefined,
  ThunkAPIConfig
>(GET_AGENCY, async (args, thunkAPI) => {
  try {
    const { agencyMaintenance } = thunkAPI.getState();
    const { startSequence, endSequence } = agencyMaintenance;
    const { data } = await agencyMaintenanceServices.getAgency({
      startSequence: checkEmpty([args?.isRefresh, startSequence, 1]),
      endSequence: checkEmpty([args?.isRefresh, endSequence, STEP_SEQUENCE])
    });
    return { data: data.agencies };
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const getAgencyBuilder = (
  builder: ActionReducerMapBuilder<IStateReducer>
) => {
  const { pending, fulfilled, rejected } = getAgency;
  builder
    .addCase(pending, (draftState, action) => {
      if (draftState.startSequence === 1 || action.meta.arg?.isRefresh) {
        draftState.isLoading = true;
      } else {
        draftState.isLoadingLoadMore = true;
      }
    })
    .addCase(fulfilled, (draftState, action) => {
      const isRefresh = action.meta.arg?.isRefresh;
      const { data } = action.payload;
      if (draftState.startSequence === 1 || isRefresh) {
        draftState.isLoading = false;
      } else {
        draftState.isLoadingLoadMore = false;
      }

      draftState.isError = checkEmpty([draftState.isError, false, true]);
      draftState.isEnd = !isArrayHasValue(data);
      draftState.agencies = checkEmpty([
        isRefresh,
        [...draftState.agencies, ...data],
        data
      ]);
      draftState.endSequence = checkEmpty([
        isRefresh,
        draftState.endSequence + STEP_SEQUENCE - 1,
        2 * STEP_SEQUENCE - 1
      ]);
      draftState.startSequence = checkEmpty([
        isRefresh,
        draftState.startSequence + STEP_SEQUENCE,
        STEP_SEQUENCE + 1
      ]);
    })
    .addCase(rejected, draftState => {
      draftState.isLoading = false;
      draftState.isError = true;
    });
};
