import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// services
import { agencyMaintenanceActions } from '.';
import agencyMaintenanceServices from '../agencyMaintenanceServices';

// types
import { API_ERROR, I18N_TEXT } from 'app/constants';
import { IPayloadDeleteMonitor } from '../../../AgencyDetails/types';
import { IStateReducer } from '../types';
import { batch } from 'react-redux';
import { checkPreviousPage } from 'pages/__commons/helpers';

export interface DeleteMonitorArgs {
  id: string;
}

export const deleteMonitor = createAsyncThunk<
  IPayloadDeleteMonitor,
  DeleteMonitorArgs,
  ThunkAPIConfig
>('agency/deleteMonitor', async (args, thunkAPI) => {
  const { id } = args;
  const { dispatch, getState } = thunkAPI;
  const { currentPage = 1, totalItem = 0 } =
    getState().agencyMaintenance.monitorMaintenance;

  try {
    const response = await agencyMaintenanceServices.deleteMonitor({
      id
    });
    batch(() => {
      dispatch(
        agencyMaintenanceActions.getMonitors({
          page: checkPreviousPage({ currentPage, totalItem })
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.DELETE_MONITOR_SUCCESS
        })
      );
      dispatch(rootModalActions.close());
    });

    return response;
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.DELETE_MONITOR_FAIL
      })
    );

    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  }
});

export const deleteMonitorBuilder = (
  builder: ActionReducerMapBuilder<IStateReducer>
) => {
  const { pending, fulfilled, rejected } = deleteMonitor;
  builder
    .addCase(pending, () => { })
    .addCase(fulfilled, (draftState, action) => {
      return {
        ...draftState,
        monitorMaintenance: {
          ...draftState.monitorMaintenance,
          data: action.payload.data
        }
      };
    })
    .addCase(rejected, () => { });
};
