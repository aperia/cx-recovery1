import { getRandomIntInclusive } from '../../../../app/helpers/commons';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// services
import agencyMaintenanceServices from '../agencyMaintenanceServices';

// types
import { IStateReducer } from '../types';
import { API_ERROR, I18N_TEXT } from 'app/constants';
import { agencyMaintenanceActions } from '.';
import { tabActions } from 'pages/__commons/TabBar/_redux';

export interface DeleteAgencyArgs {
  id: string;
}

export const deleteAgency = createAsyncThunk<
  unknown,
  DeleteAgencyArgs,
  ThunkAPIConfig
>('agency/deleteAgency', async ({ id }, thunkAPI) => {
  const { dispatch, getState } = thunkAPI;

  const { tab } = getState();

  try {
    const response = await agencyMaintenanceServices.deleteAgency({ id });
    const success = getRandomIntInclusive(0, 1);

    if (success) {
      dispatch(agencyMaintenanceActions.getAgency({ isRefresh: true }));

      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.DELETE_AGENCY_SUCCESS
        })
      );

      const agencyManagementTabStoreId = tab.tabs.find(
        tab => tab.tabType === 'agencyManagement'
      )?.storeId;

      if (tab.tabStoreIdSelected !== agencyManagementTabStoreId) {
        dispatch(tabActions.removeTab({ storeId: tab.tabStoreIdSelected }));

        if (agencyManagementTabStoreId) {
          dispatch(
            tabActions.selectTab({ storeId: agencyManagementTabStoreId })
          );
        }
      }
    } else {
      throw new Error(I18N_TEXT.DELETE_AGENCY_FAIL);
    }

    return response;
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.DELETE_AGENCY_FAIL
      })
    );

    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  } finally {
    dispatch(rootModalActions.close());
  }
});

export const deleteAgencyBuilder = (
  builder: ActionReducerMapBuilder<IStateReducer>
) => {
  const { pending, fulfilled, rejected } = deleteAgency;
  builder
    .addCase(pending, () => {})
    .addCase(fulfilled, (draftState, action) => {
      const { id } = action.meta.arg;
      const filterAgencies = draftState.agencies.filter(item => item.id !== id);
      draftState.agencies = filterAgencies;
    })
    .addCase(rejected, () => {});
};
