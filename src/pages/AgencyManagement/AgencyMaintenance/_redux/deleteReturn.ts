import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// services

// types
import { API_ERROR, I18N_TEXT } from 'app/constants';
import agencyMaintenanceServices from 'pages/AgencyManagement/AgencyMaintenance/agencyMaintenanceServices';
import { IPayloadDeleteReturn } from 'pages/AgencyDetails/types';
import { IStateReducer } from '../types';
import { batch } from 'react-redux';
import { checkPreviousPage } from 'pages/__commons/helpers';
import { agencyMaintenanceActions } from '.';

export interface DeleteReturnArgs {
  id: string;
}

export const deleteReturn = createAsyncThunk<
  IPayloadDeleteReturn,
  DeleteReturnArgs,
  ThunkAPIConfig
>('agency/deleteReturn', async (args, thunkAPI) => {
  const { id } = args;
  const { dispatch, getState } = thunkAPI;
  const { currentPage = 1, totalItem = 0 } =
    getState().agencyMaintenance.returnMaintenance;

  try {
    const response = await agencyMaintenanceServices.deleteReturn({
      id
    });

    batch(() => {
      dispatch(
        agencyMaintenanceActions.getReturns({
          page: checkPreviousPage({ currentPage, totalItem })
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.DELETE_RETURN_SUCCESS
        })
      );
      dispatch(rootModalActions.close());
    });

    return response;
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.DELETE_RETURN_FAIL
      })
    );

    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  }
});

export const deleteReturnBuilder = (
  builder: ActionReducerMapBuilder<IStateReducer>
) => {
  const { pending, fulfilled, rejected } = deleteReturn;
  builder
    .addCase(pending, () => {})
    .addCase(fulfilled, (draftState, action) => {
      return {
        ...draftState,
        returnMaintenance: {
          ...draftState.returnMaintenance,
          data: action.payload.data
        }
      };
    })
    .addCase(rejected, () => {});
};
