import { createAsyncThunk } from '@reduxjs/toolkit';
import { checkNextPage } from 'pages/__commons/helpers';
import { IMonitorCriteria } from 'pages/__commons/MonitorMaintenance/types';
import { monitorMaintenanceActions } from 'pages/__commons/MonitorMaintenance/_redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { agencyMaintenanceActions } from '.';
import agencyMaintenanceServices from '../agencyMaintenanceServices';
import { ISaveMonitorArgs, SAVE_MONITOR } from '../types';

export const saveMonitor = createAsyncThunk<
  undefined,
  ISaveMonitorArgs,
  ThunkAPIConfig
>(SAVE_MONITOR, async (args, { dispatch, getState, rejectWithValue }) => {
  const { currentPage = 1, totalItem = 0 } =
    getState().agencyMaintenance.monitorMaintenance;

  try {
    const formatNoteCodes: string[] = args.noteCodes.map(item => item.value);

    const formatMonitor: IMonitorCriteria = {
      ...args,
      calendarCode: args.calendarCode.value,
      noteCodes: formatNoteCodes,
      intervalStartUnit: args.intervalStartUnit.value,
      intervalActionUnit: args.intervalActionUnit.value
    };

    await agencyMaintenanceServices.saveMonitor(formatMonitor);

    batch(() => {
      dispatch(
        agencyMaintenanceActions.getMonitors({
          page: checkNextPage({ currentPage, totalItem })
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: args.id
            ? 'txt_monitor_criteria_updated'
            : 'txt_monitor_criteria_added'
        })
      );
      dispatch(monitorMaintenanceActions.onToggleFormModal());
    });
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: args.id
          ? 'txt_monitor_criteria_failed_to_update'
          : 'txt_monitor_criteria_failed_to_add'
      })
    );
    return rejectWithValue(error);
  }
});
