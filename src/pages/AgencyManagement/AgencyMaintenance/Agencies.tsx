import React, { Fragment, useCallback, useMemo } from 'react';

// components
import {
  Button,
  ColumnType,
  Grid,
  Tooltip
} from 'app/_libraries/_dls/components';
import AgencyFounded from './AgencyFounded';
import { SearchNodata } from 'app/components';
import LoadMoreAgency from './LoadMoreAgency';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import {
  selectorSearchAgencies,
  selectorSortAgencies
} from './_redux/selectors';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { tabActions } from 'pages/__commons/TabBar/_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import AgencyConfirmBody from './AgencyConfirmBody';
import ConditionalWrapper from 'pages/__commons/ConditionalWrapper';

// helper
import { I18N_TEXT } from 'app/constants';
import { DATA_TEST_ID_AGENCIES } from './helpers';
import { IAgency } from './types';
import { agencyMaintenanceActions } from './_redux';
import { checkEmpty } from 'app/helpers/commons';
import { agencyDetailsActions } from 'pages/AgencyDetails/_redux';
import { convertValueDescription } from 'pages/__commons/helpers';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import { useAgencyForm } from 'pages/__commons/Agency/AgencyForm/useAgencyForm';
export interface AgenciesProps {}

const Agencies: React.FC<AgenciesProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { openAgencyForm } = useAgencyForm();
  const { data: agencyTypeData } = useGetRefDataQuery('agencyType');
  const agencies = useSelector(selectorSearchAgencies);

  const sort = useSelector(selectorSortAgencies);

  const handleSort = (e: ISortType) => {
    dispatch(agencyMaintenanceActions.onChangeSort([e]));
  };

  const hasAccountAssigned = useCallback((data: IAgency) => {
    return data.companyName.includes('Perfect');
  }, []);

  const handleShowConfirmDelete = useCallback(
    (data: IAgency) => {
      dispatch(
        rootModalActions.open({
          autoClose: false,
          title: t(I18N_TEXT.DELETE_AGENCY_CONFIRM_TITLE),
          body: <AgencyConfirmBody data={data} />,
          btnConfirmText: t(I18N_TEXT.DELETE),
          btnCancelText: t(I18N_TEXT.CANCEL),
          onConfirm: () => {
            dispatch(rootModalActions.loading(true));

            dispatch(
              agencyMaintenanceActions.deleteAgency({
                id: data.agencyCode
              })
            );
          },
          onCancel: () => {
            dispatch(rootModalActions.close());
          }
        })
      );
    },
    [dispatch, t]
  );

  const handleOpenDetail = useCallback(
    (agencyItem: IAgency) => {
      const { id, agencyCode, companyName } = agencyItem;

      batch(() => {
        dispatch(
          tabActions.addTab({
            title: `${agencyCode} - ${companyName}`,
            storeId: id,
            tabType: 'agencyDetail',
            iconName: 'rating-line',
            props: agencyItem
          })
        );

        //Get AgencyDetail
        dispatch(agencyDetailsActions.getAgencyDetails({ id, agencyCode }));

        //Get FeeInformation
        dispatch(agencyDetailsActions.getFeeInformation({ id, agencyCode }));
      });
    },
    [dispatch]
  );

  const REPORT_LIST_COLUMNS: ColumnType<IAgency>[] = useMemo(
    () => [
      {
        id: 'agencyCode',
        Header: t(I18N_TEXT.AGENCY_CODE),
        isSort: true,
        width: 140,
        cellBodyProps: { className: 'td-sm' },
        accessor: (data: IAgency) => (
          <Button
            size="sm"
            variant="outline-primary"
            onClick={() => handleOpenDetail(data)}
          >
            {data.agencyCode}
          </Button>
        )
      },
      {
        id: 'agencyType',
        Header: t(I18N_TEXT.AGENCY_TYPE),
        isSort: true,
        width: 250,
        accessor: (data: IAgency) => {
          const agencyType =
            agencyTypeData !== undefined
              ? agencyTypeData.agencyType.find(
                  opt => opt.value === data.agencyType
                )
              : ({} as RefDataValue);
          return <Fragment>{convertValueDescription(agencyType)}</Fragment>;
        }
      },
      {
        id: 'companyName',
        Header: t(I18N_TEXT.COMPANY_NAME),
        isSort: true,
        width: 400,
        autoWidth: true,
        accessor: 'companyName'
      },
      {
        id: 'actions',
        Header: t(I18N_TEXT.ACTIONS),
        cellBodyProps: { className: 'td-sm' },
        accessor: (data: IAgency) => (
          <Fragment>
            <ConditionalWrapper
              condition={hasAccountAssigned(data)}
              wrapper={children => (
                <Tooltip
                  triggerClassName="mr-8"
                  element={
                    <span>
                      {t(I18N_TEXT.DISABLED_DELETE_AGENCY_FAIL)}
                    </span>
                  }
                >
                  {children}
                </Tooltip>
              )}
            >
              <Button
                disabled={hasAccountAssigned(data)}
                onClick={() => {
                  handleShowConfirmDelete(data);
                }}
                variant="outline-danger"
                size="sm"
              >
                {t(I18N_TEXT.DELETE)}
              </Button>
            </ConditionalWrapper>

            <Button
              variant="outline-primary"
              size="sm"
              onClick={() =>
                openAgencyForm({
                  formType: 'EDIT',
                  agency: data.agencyCode,
                  from: 'management'
                })
              }
            >
              {t(I18N_TEXT.EDIT)}
            </Button>
          </Fragment>
        ),
        width: 135,
        isFixedRight: true,
        className: 'td-sm text-center'
      }
    ],
    [
      t,
      handleOpenDetail,
      agencyTypeData,
      hasAccountAssigned,
      handleShowConfirmDelete,
      openAgencyForm
    ]
  );

  const handleClearAndReset = () => {
    dispatch(agencyMaintenanceActions.onClearSearch());
  };

  if (!checkEmpty([agencies, false]))
    return <SearchNodata onClearSearch={handleClearAndReset} />;

  return (
    <Fragment>
      <AgencyFounded />
      <Grid
        columns={REPORT_LIST_COLUMNS}
        onSortChange={handleSort}
        sortBy={sort}
        data={agencies}
        dataTestId={DATA_TEST_ID_AGENCIES}
      />
      <LoadMoreAgency />
    </Fragment>
  );
};

export default Agencies;
