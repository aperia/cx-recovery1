import { AgencyFormValues } from 'pages/__commons/Agency/AgencyForm/types';
import { IReturnCriteria } from 'pages/__commons/ReturnMaintenance/types';
import { IMonitorCriteria } from 'pages/__commons/MonitorMaintenance/types';
import { agencyMaintenanceController } from './__mock__/agencyController';
import { monitorController } from './__mock__/monitorController';
import { returnController } from './__mock__/returnController';

const agencyMaintenanceServices = {
  getAgency({
    startSequence,
    endSequence
  }: {
    startSequence: number;
    endSequence: number;
  }) {
    return agencyMaintenanceController.getAgency({
      startSequence,
      endSequence
    });
    // const agencies = agenciesData.slice(startSequence, endSequence);
    // return Promise.resolve({ data: { success: true, agencies } });
    // const url: string = window.appConfig?.api?.agencyManagement?.getAgencies;
    // return apiService.get<IAgency[]>(checkEmpty([url, '']));
  },

  deleteAgency({ id }: { id: string }) {
    return agencyMaintenanceController.deleteAgency({ id });
    // const url: string = window.appConfig?.api?.agencyManagement?.deleteAgency;
    // return apiService.get(checkEmpty([url, '']));
  },

  getAgencyDetails(data: { agencyCode: string }) {
    // const url: string = window.appConfig?.api?.agencyManagement?.agencyDetails;

    // return apiService.post(checkEmpty([url, '']), data);
    return agencyMaintenanceController.getAgencyDetail(data);
  },

  addAgency({ agency }: { agency: AgencyFormValues }) {
    return agencyMaintenanceController.addAgency({ agency });

    // const url: string = window.appConfig?.api?.agencyManagement?.addAgency;

    // return apiService.post<HttpResponse>(checkEmpty([url, '']), data);
  },
  editAgency(data: { prevAgency: string; agency: AgencyFormValues }) {
    return agencyMaintenanceController.editAgency(data);
  },

  addReturn({
    returnCriteria,
    agencyCode
  }: {
    returnCriteria: IReturnCriteria;
    agencyCode: string;
  }) {
    return returnController.addReturn({ returnCriteria, agencyCode });

    // const url: string = window.appConfig?.api?.agencyManagement?.addReturn;

    // return apiService.post<HttpResponse>(checkEmpty([url, '']), data);
  },
  getReturns({ page = 1 }: { page?: number }) {
    // const url: string = window.appConfig?.api?.agencyManagement?.returnDetails;

    // return apiService.post(checkEmpty([url, '']), data);
    return returnController.getReturns({ page });
  },
  editReturn(newReturn: IReturnCriteria, id: string, agencyCode: string) {
    return returnController.editReturn(newReturn, id, agencyCode);
  },
  getMonitors({ page = 1 }: { page?: number }) {
    return monitorController.getMonitors({ page });
  },
  saveMonitor(monitor: IMonitorCriteria) {
    return monitorController.saveMonitor(monitor);
  },
  deleteReturn(data: { id: string }) {
    // const url: string = window.appConfig?.api?.agencyManagement?.deleteReturn;
    // return apiService.post<AgencyDetails>(checkEmpty([url, '']), data);
    return returnController.deleteReturn({ id: data.id });
  },
  deleteMonitor(data: { id: string }) {
    // const url: string = window.appConfig?.api?.agencyManagement?.deleteMonitor;
    // return apiService.post<AgencyDetails>(checkEmpty([url, '']), data);
    return monitorController.deleteMonitor({ id: data.id });
  },
  getFeeInformation({ agencyCode }: { agencyCode: string }) {
    return agencyMaintenanceController.getFeeInformation({ agencyCode });
  }
};

export default agencyMaintenanceServices;
