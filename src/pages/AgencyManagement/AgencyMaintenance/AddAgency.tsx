import React from 'react';

// components
import Button from 'app/_libraries/_dls/components/Button';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAgencyForm } from 'pages/__commons/Agency/AgencyForm/useAgencyForm';

// constants
import { I18N_TEXT } from 'app/constants';

export interface AddAgencyProps {}

const AddAgency: React.FC<AddAgencyProps> = () => {
  const { t } = useTranslation();
  const { openAgencyForm } = useAgencyForm();
  return (
    <Button
      className="mr-n8"
      variant="outline-primary"
      size="sm"
      onClick={() => openAgencyForm({ formType: 'ADD' })}
    >
      {t(I18N_TEXT.ADD_AGENCY)}
    </Button>
  );
};

export default AddAgency;
