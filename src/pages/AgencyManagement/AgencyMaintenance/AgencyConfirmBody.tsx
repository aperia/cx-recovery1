import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helper
import { I18N_TEXT } from 'app/constants';
import { IAgency } from './types';

export interface AgencyConfirmBodyProps {
  data: IAgency;
}

const AgencyConfirmBody: React.FC<AgencyConfirmBodyProps> = ({ data }) => {
  const { t } = useTranslation();

  return (
    <>
      <p>{t(I18N_TEXT.DELETE_AGENCY_CONFIRM_BODY)}</p>
      <p className="mt-16">
        Agency: <strong>{`${data.agencyCode} - ${data.companyName}`}</strong>
      </p>
    </>
  );
};

export default AgencyConfirmBody;
