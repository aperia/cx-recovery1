import React, { useCallback } from 'react';
import { HorizontalTabs } from 'app/_libraries/_dls/components';
import MonitorMaintenance from 'pages/__commons/MonitorMaintenance';
import ReturnMaintenance from 'pages/__commons/ReturnMaintenance';

// constants
import { I18N_TEXT } from 'app/constants';
import { ISaveMonitorArgs } from '../types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { agencyMaintenanceActions } from '../_redux';
import {
  selectedMonitorCurrentPage,
  selectedMonitorData,
  selectedMonitorLoading,
  selectedMonitorTotalData,
  selectedReturnCurrentPage,
  selectedReturnData,
  selectedReturnTotalData
} from '../_redux/selectors';
import { ElectronicType } from 'pages/AgencyDetails/types';
import { IMonitorCriteria } from 'pages/__commons/MonitorMaintenance/types';
import useAgencyPagination from './hooks/useAgencyPagination';

const AgencyMaintenanceTabs: React.FC<{
  electronicInterfaceType?: string;
  agencyCode?: string;
}> = ({ electronicInterfaceType, agencyCode }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const testId = 'AgencyMaintenanceTabs';

  const monitorData: IMonitorCriteria[] = useSelector(selectedMonitorData);
  const monitorTotalData: number = useSelector(selectedMonitorTotalData);
  const monitorLoading: boolean = useSelector(selectedMonitorLoading);
  const monitorCurrentPage: number = useSelector(selectedMonitorCurrentPage);

  const returnList = useSelector(selectedReturnData);
  const returnTotalData: number = useSelector(selectedReturnTotalData);
  const returnCurrentPage: number = useSelector(selectedReturnCurrentPage);

  const {
    currentPageMonitor,
    currentPageReturn,
    onPageMonitorChange,
    onPageReturnChange
  } = useAgencyPagination({
    monitorCurrentPage,
    returnCurrentPage,
    totalMonitorItems: monitorTotalData,
    totalReturnItems: returnTotalData
  });

  const handleSubmitMonitor = useCallback(
    (monitor: ISaveMonitorArgs) => {
      dispatch(agencyMaintenanceActions.saveMonitor(monitor));
    },
    [dispatch]
  );

  if (
    ElectronicType.ELECTRONIC_GENERATE_WORKSHEET === electronicInterfaceType ||
    ElectronicType.ELECTRONIC_NOT_GENERATE === electronicInterfaceType
  ) {
    return (
      <HorizontalTabs className="bg-light-l20 max-width-lg mx-auto mt-n45">
        <HorizontalTabs.Tab
          id={`${testId}_tab_monitor_maintenance`}
          key={`${testId}_tab_monitor_maintenance`}
          eventKey={`${testId}_tab_monitor_maintenance`}
          title={t(I18N_TEXT.MONITOR_MAINTENANCE)}
        >
          <MonitorMaintenance
            data={monitorData}
            loading={monitorLoading}
            totalItem={monitorTotalData}
            currentPage={currentPageMonitor}
            onPageChange={onPageMonitorChange}
            onSubmit={handleSubmitMonitor}
          />
        </HorizontalTabs.Tab>
        <HorizontalTabs.Tab
          id={`${testId}_tab_return_maintenance`}
          key={`${testId}_tab_return_maintenance`}
          eventKey={`${testId}_tab_return_maintenance`}
          title={t(I18N_TEXT.RETURN_MAINTENANCE)}
        >
          <ReturnMaintenance
            data={returnList}
            agencyCode={agencyCode || ''}
            totalItem={returnTotalData}
            currentPage={currentPageReturn}
            onPageChange={onPageReturnChange}
          />
        </HorizontalTabs.Tab>
      </HorizontalTabs>
    );
  }
  return (
    <div className="border-top">
      <ReturnMaintenance
        data={returnList}
        agencyCode={agencyCode || ''}
        totalItem={monitorTotalData}
        currentPage={currentPageReturn}
        onPageChange={onPageReturnChange}
      />
    </div>
  );
};

export default AgencyMaintenanceTabs;
