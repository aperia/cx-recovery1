import { useState, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { checkNextPage } from 'pages/__commons/helpers';
import { agencyMaintenanceActions } from '../../_redux';

interface AgencyPaginationProps {
  monitorCurrentPage: number;
  returnCurrentPage: number;
  totalMonitorItems: number;
  totalReturnItems: number;
}
const useMonitorPagination = ({
  totalMonitorItems,
  totalReturnItems,
  monitorCurrentPage,
  returnCurrentPage
}: AgencyPaginationProps) => {
  const dispatch = useDispatch();
  const [currentPageMonitor, setCurrentPageMonitor] = useState(1);
  const [currentPageReturn, setCurrentPageReturn] = useState(1);

  useEffect(() => {
    setCurrentPageMonitor(monitorCurrentPage);
    setCurrentPageReturn(returnCurrentPage);
  }, [monitorCurrentPage, returnCurrentPage]);

  const onPageMonitorChange = (pageSelected: number) => {
    setCurrentPageMonitor(pageSelected);
    dispatch(
      agencyMaintenanceActions.getMonitors({
        page: checkNextPage({
          currentPage: pageSelected,
          totalItem: totalMonitorItems
        })
      })
    );
  };
  const onPageReturnChange = (pageSelected: number) => {
    setCurrentPageReturn(pageSelected);
    dispatch(
      agencyMaintenanceActions.getReturns({
        page: checkNextPage({
          currentPage: pageSelected,
          totalItem: totalReturnItems
        })
      })
    );
  };
  return {
    currentPageMonitor,
    currentPageReturn,
    onPageMonitorChange,
    onPageReturnChange
  };
};
export default useMonitorPagination;
