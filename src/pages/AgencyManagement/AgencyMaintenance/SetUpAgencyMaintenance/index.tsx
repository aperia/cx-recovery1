import React, { useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// components
import {
  ModalBody,
  ModalHeader,
  ModalTitle,
  TruncateText
} from 'app/_libraries/_dls/components';
import Modal from 'app/_libraries/_dls/components/Modal/Modal';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { agencyMaintenanceActions } from '../_redux';
import {
  selectedSetUpAgency,
  selectedSetUpAgencyModal
} from '../_redux/selectors';

// constants
import { I18N_TEXT } from 'app/constants';
import { AgencyFormValues } from 'pages/__commons/Agency/AgencyForm/types';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import AgencyMaintenanceTabs from './AgencyMaintenanceTabs';

const SetUpAgencyManagement: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const setUpAgencyModal = useSelector(selectedSetUpAgencyModal);
  const { data: electronicInterfaceTypeRefData } = useGetRefDataQuery(
    'electronicInterfaceType'
  );

  const selectedAgency: AgencyFormValues | undefined =
    useSelector(selectedSetUpAgency);

  useEffect(() => {
    dispatch(agencyMaintenanceActions.getMonitors({}));
    dispatch(agencyMaintenanceActions.getReturns({}));
  }, [dispatch]);

  const toggleModal = () => {
    dispatch(agencyMaintenanceActions.onToggleSetUpAgency());
  };

  return (
    <>
      <Modal show={setUpAgencyModal?.open || false} full>
        <ModalHeader border closeButton onHide={toggleModal}>
          <ModalTitle>
            {t(I18N_TEXT.SET_UP_AGENCY_MAINTENANCE)}
          </ModalTitle>
        </ModalHeader>
        <ModalBody className="p-0">
          <div className="bg-light-l20">
            <div className="p-24">
              <h4>
                {selectedAgency?.agencyCode} - {selectedAgency?.companyName}
              </h4>
              <span className="form-group-static__label mt-24">
                <TruncateText>
                  {t('txt_electronic_interface_type')}
                </TruncateText>
              </span>
              <div className={'form-group-static__text'}>
                {selectedAgency?.electronicInterfaceType} -{' '}
                {
                  electronicInterfaceTypeRefData?.electronicInterfaceType?.find(
                    (item: RefDataValue) =>
                      item.value === selectedAgency?.electronicInterfaceType
                  )?.description
                }
              </div>
            </div>
          </div>
          <AgencyMaintenanceTabs
            electronicInterfaceType={selectedAgency?.electronicInterfaceType}
          />
        </ModalBody>
      </Modal>
    </>
  );
};

export default SetUpAgencyManagement;
