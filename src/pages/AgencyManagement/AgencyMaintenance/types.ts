import { SortType } from 'app/_libraries/_dls/components';
import { AgencyFormValues } from 'pages/__commons/Agency/AgencyForm/types';
import { IMonitorCriteria } from 'pages/__commons/MonitorMaintenance/types';
import { IReturnCriteria } from 'pages/__commons/ReturnMaintenance/types';

export const GET_AGENCY = 'agency/getAgency';
export const DELETE_AGENCY = 'agency/deleteAgency';
export const GET_MONITORS = 'agency/getMonitors';
export const SAVE_MONITOR = 'agency/saveMonitor';
export const GET_RETURNS = 'agency/getReturns';
export const SAVE_RETURN = 'agency/saveReturn'
export interface IPayloadGetAgency {
  data: IAgency[];
}

export interface IAgency extends MagicKeyValue {
  id: string;
  agencyCode: string;
  agencyType: string;
  companyName: string;
}

export interface IStateReducer {
  agencies: IAgency[];
  sort: SortType[];
  search: ISearchAgency;
  isLoading: boolean;
  isEnd: boolean;
  startSequence: number;
  endSequence: number;
  isError: boolean;
  isLoadingLoadMore: boolean;
  setUpModal?: ISetUpModal;
  monitorMaintenance: IMonitorMaintenance;
  returnMaintenance: IReturnMaintenance;
}

export interface IMonitorMaintenance {
  data: IMonitorCriteria[];
  totalItem?: number;
  loading: boolean;
  currentPage?: number;
}
export interface IReturnMaintenance {
  data: IReturnCriteria[];
  totalItem?: number;
  loading: boolean;
  currentPage?: number;
}

export interface IArgGetMonitors {
  page?: number;
}
export interface IArgGetReturns {
  page?: number;
}

export interface ISetUpModal {
  open: boolean;
  agency?: AgencyFormValues;
}

export interface ISearchAgency {
  agencyCode: string;
  companyName: string;
  agencyType: RefDataValue[];
}

export interface IMonitorMaintenance {
  data: IMonitorCriteria[];
  loading: boolean;
}
export interface IReturnMaintenance {
  data: IReturnCriteria[];
  loading: boolean;
}
export interface IPayloadGetReturns {
  data: IReturnCriteria[];
  totalItem?: number;
  success: boolean;
  currentPage?: number;
}
export interface IPayloadGetMonitors {
  data: IMonitorCriteria[];
  totalItem?: number;
  success: boolean;
  currentPage?: number;
}

export interface IPayloadSaveMonitor { }

export interface ISaveMonitorArgs {
  id?: string;
  calendarCode: RefDataValue;
  noteCodes: RefDataValue[];
  intervalStart: string;
  intervalStartUnit: RefDataValue;
  intervalAction: string;
  intervalActionUnit: RefDataValue;
  calendarCodePost: string;
  paymentDeleteCalendarCode: string;
}

export interface ISaveReturnArgs extends IReturnCriteria {
  id?: string;
  agencyCode: string;
  formType: FormType
}