import React from 'react';

// component
import { View } from 'app/_libraries/_dof/core';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelectorId } from 'app/hooks';
import { selectorFeeInfo } from './_redux/selectors';

// helper
import { FeeInformationData } from './types';
import {
  convertValueDescription,
  getRefDataFromCode
} from 'pages/__commons/helpers';
import { I18N_TEXT } from 'app/constants';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';

const FeeInformation: React.FC = () => {
  const { t } = useTranslation();
  const { data: feeBasicData } = useGetRefDataQuery('feeBasic');
  const { data: remitNetCostsData } = useGetRefDataQuery('remitNetCosts');
  const { data: remitNetFeesData } = useGetRefDataQuery('remitNetFees');
  const { data: updCollectorData } = useGetRefDataQuery('updCollector');
  const { data: updCltrOnAssignToData } =
    useGetRefDataQuery('updCltrOnAssignTo');

  const selectorFeeInfoData =
    useSelectorId<FeeInformationData>(selectorFeeInfo);

  const { assignTo, feeBasic, remitNetCosts, remitNetFees, updCollector } =
    selectorFeeInfoData;

  const convertFieldData = {
    feeBasic: convertValueDescription(
      getRefDataFromCode(feeBasicData?.feeBasic, feeBasic)
    ),
    assignTo: convertValueDescription(
      getRefDataFromCode(updCltrOnAssignToData?.updCltrOnAssignTo, assignTo)
    ),
    remitNetCosts: getRefDataFromCode(
      remitNetCostsData?.remitNetCosts,
      remitNetCosts
    )?.description,
    remitNetFees: getRefDataFromCode(
      remitNetFeesData?.remitNetFees,
      remitNetFees
    )?.description,
    updCollector: getRefDataFromCode(
      updCollectorData?.updCollector,
      updCollector
    )?.description
  };

  return (
    <div className="col-12 bg-white p-24">
      <div className="d-flex justify-content-between pb-8">
        <h4>{t(I18N_TEXT.FEE_INFORMATION)}</h4>
      </div>
      <View
        formKey="ky-feeInformation"
        descriptor="feeInformation"
        value={convertFieldData}
      />
    </div>
  );
};

export default FeeInformation;
