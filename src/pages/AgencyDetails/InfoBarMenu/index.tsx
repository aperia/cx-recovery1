import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// component
import FeeInformation from '../FeeInformation';
import { InfoBar } from 'app/_libraries/_dls/components';

// helper
import { I18N_TEXT } from 'app/constants';

const InfoBarMenu = () => {
  const { t } = useTranslation();

  return (
    <InfoBar
      expand={false}
      items={[
        {
          id: '1',
          name: t(I18N_TEXT.FEE_INFORMATION),
          icon: {
            name: 'billing'
          },
          component: () => <FeeInformation />
        }
      ]}
    />
  );
};

export default InfoBarMenu;
