import React, { ReactNode, useCallback, useMemo } from 'react';
import { useDispatch } from 'react-redux';

// component
import { View } from 'app/_libraries/_dof/core';
import {
  Button,
  DropdownButton,
  DropdownButtonSelectEvent,
  Icon,
  Tooltip
} from 'app/_libraries/_dls/components';
import AgencyConfirmBody from 'pages/AgencyManagement/AgencyMaintenance/AgencyConfirmBody';
import ConditionalWrapper from 'pages/__commons/ConditionalWrapper';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelectorId } from 'app/hooks';
import { selectorAgencyDetails } from './_redux/selectors';
import { useAgencyForm } from 'pages/__commons/Agency/AgencyForm/useAgencyForm';

// helper
import { I18N_TEXT } from 'app/constants';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { AgencyDetails, AgencyUserDetailProps } from './types';
import {
  convertAddress,
  convertValueDescription,
  getRefDataFromCode
} from 'pages/__commons/helpers';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { agencyMaintenanceActions } from 'pages/AgencyManagement/AgencyMaintenance/_redux';
import { IAgency } from 'pages/AgencyManagement/AgencyMaintenance/types';
import AgencyMaintenanceTabs from 'pages/AgencyManagement/AgencyMaintenance/SetUpAgencyMaintenance/AgencyMaintenanceTabs';

const AgencyUserDetail: React.FC<AgencyUserDetailProps> = ({ title }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { openAgencyForm } = useAgencyForm();
  const { data: agencyTypeData } = useGetRefDataQuery('agencyType');
  const { data: assignedLevelData } = useGetRefDataQuery('assignedLevel');
  const { data: electronicInterfaceTypeData } = useGetRefDataQuery(
    'electronicInterfaceType'
  );
  const { data: exceptionFlagData } = useGetRefDataQuery('exceptionFlag');

  const agencyDetails = useSelectorId<AgencyDetails>(selectorAgencyDetails);

  const {
    id,
    agencyCode,
    agencyType,
    masterAgency,
    phoneNumber,
    address1,
    address2,
    zipCode,
    state,
    city,
    assignedLevel,
    contactName,
    electronicInterfaceType,
    exceptionFlag,
    companyName = '',
    fedTaxId
  } = agencyDetails;

  const convertFieldData = useMemo(
    () => ({
      address: convertAddress(address1, address2, zipCode, state, city),
      agencyType: convertValueDescription(
        getRefDataFromCode(agencyTypeData?.agencyType, agencyType)
      ),
      assignLevel: convertValueDescription(
        getRefDataFromCode(assignedLevelData?.assignedLevel, assignedLevel)
      ),
      contactName,
      masterAgency,
      phoneNumber,
      electronicInterfaceType: convertValueDescription(
        getRefDataFromCode(
          electronicInterfaceTypeData?.electronicInterfaceType,
          electronicInterfaceType
        )
      ),
      exceptionFlag: getRefDataFromCode(
        exceptionFlagData?.exceptionFlag,
        exceptionFlag
      )?.description,
      fedTaxId
    }),
    [
      address1,
      address2,
      agencyType,
      agencyTypeData?.agencyType,
      assignedLevel,
      assignedLevelData?.assignedLevel,
      city,
      contactName,
      electronicInterfaceType,
      electronicInterfaceTypeData?.electronicInterfaceType,
      exceptionFlag,
      exceptionFlagData?.exceptionFlag,
      fedTaxId,
      masterAgency,
      phoneNumber,
      state,
      zipCode
    ]
  );

  const handleShowConfirmDelete = useCallback(
    (data: IAgency) => {
      dispatch(
        rootModalActions.open({
          autoClose: false,
          title: t(I18N_TEXT.DELETE_AGENCY_CONFIRM_TITLE),
          body: <AgencyConfirmBody data={data} />,
          btnConfirmText: t(I18N_TEXT.DELETE),
          btnCancelText: t(I18N_TEXT.CANCEL),
          onConfirm: () => {
            dispatch(rootModalActions.loading(true));

            dispatch(
              agencyMaintenanceActions.deleteAgency({
                id: data.agencyCode
              })
            );
          },
          onCancel: () => {
            dispatch(rootModalActions.close());
          }
        })
      );
    },
    [dispatch, t]
  );

  const handleSelect = (event: DropdownButtonSelectEvent) => {
    const { value } = event;

    switch (value) {
      case t(I18N_TEXT.DELETE_AGENCY): {
        handleShowConfirmDelete({
          agencyCode,
          agencyType,
          companyName
        } as IAgency);
        break;
      }

      case t(I18N_TEXT.EDIT_AGENCY): {
        openAgencyForm({
          formType: 'EDIT',
          agency: agencyCode,
          from: 'snapshot'
        });
        break;
      }
      default:
        break;
    }
  };

  return (
    <>
      <div className="bg-light-l20 border-bottom">
        <div className="px-24 pt-24 pb-69 max-width-lg mx-auto">
          <div className="d-flex justify-content-between align-items-center">
            <h3 data-testid={genAmtId(title, 'title', '')}>{title}</h3>
            <div className="d-lg-none d-md-block mr-n8">
              <DropdownButton
                onSelect={handleSelect}
                buttonProps={{
                  children: <Icon name="more-vertical" />,
                  variant: 'icon-secondary'
                }}
              >
                <DropdownButton.Item
                  disabled={companyName.includes('Perfect')}
                  label={t(I18N_TEXT.DELETE_AGENCY)}
                  value={t(I18N_TEXT.DELETE_AGENCY)}
                />
                <DropdownButton.Item
                  label={t(I18N_TEXT.EDIT_AGENCY)}
                  value={t(I18N_TEXT.EDIT_AGENCY)}
                />
              </DropdownButton>
            </div>
            <div className="d-lg-block d-none">
              <ConditionalWrapper
                condition={companyName.includes('Perfect')}
                wrapper={(children: ReactNode) => (
                  <Tooltip
                    triggerClassName="mr-8"
                    element={
                      <span>{t(I18N_TEXT.DISABLED_DELETE_AGENCY_FAIL)}</span>
                    }
                  >
                    {children}
                  </Tooltip>
                )}
              >
                <Button
                  className="text-capitalize"
                  size="sm"
                  variant="outline-danger"
                  disabled={companyName.includes('Perfect')}
                  onClick={() => {
                    handleShowConfirmDelete({
                      agencyCode,
                      agencyType,
                      companyName
                    } as IAgency);
                  }}
                >
                  {t(I18N_TEXT.DELETE_AGENCY)}
                </Button>
              </ConditionalWrapper>

              <Button
                className="text-capitalize mr-n8"
                size="sm"
                variant="outline-primary"
                onClick={() =>
                  openAgencyForm({
                    formType: 'EDIT',
                    agency: agencyCode,
                    from: 'snapshot'
                  })
                }
              >
                {t(I18N_TEXT.EDIT_AGENCY)}
              </Button>
            </div>
          </div>
          <View
            formKey={`ky-agencyUserDetail-${id}`}
            descriptor="agencyUserDetail"
            value={convertFieldData}
          />
        </div>
      </div>
      <AgencyMaintenanceTabs
        electronicInterfaceType={agencyDetails.electronicInterfaceType}
        agencyCode={agencyDetails.agencyCode}
      />
    </>
  );
};

export default AgencyUserDetail;
