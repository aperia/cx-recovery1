import { IMonitorCriteria } from 'pages/__commons/MonitorMaintenance/types';
import { IReturnCriteria } from 'pages/__commons/ReturnMaintenance/types';

export const GET_AGENCY_DETAILS = 'agency/getAgencyDetails';
export const GET_FEE_INFORMATION = 'agency/getFeeInformation';

export const DEFAULT_AGENCY_DETAIL = {
  id: '',
  address1: '',
  address2: '',
  agencyCode: '',
  agencyType: '',
  assignTo: '',
  assignedLevel: '',
  companyName: '',
  contactName: '',
  phoneNumber: '',
  remitNetCosts: '',
  remitNetFees: '',
  state: '',
  updCollector: '',
  zipCode: '',
  fedTaxId: '',
  city: '',
  masterAgency: '',
  electronicInterfaceType: '',
  feeBasic: '',
  exceptionFlag: '',
  monitorCriterialList: [],
  returnCriterialList: []
} as AgencyDetails;

export const DEFAULT_FEE_INFORMATION = {
  feeBasic: '',
  remitNetCosts: '',
  remitNetFees: '',
  updCollector: '',
  updCltrOnAssignTo: ''
};

export interface AgencyTabProps {
  id: string;
  agencyCode: string;
  companyName: string;
}

export interface AgencyUserDetailProps {
  title?: string;
  agencyDetails?: AgencyDetails;
}

export interface AgencyDetails {
  id?: string;
  address1?: string;
  address2?: string;
  agencyCode?: string;
  agencyType?: string;
  assignTo?: string;
  assignedLevel?: string;
  companyName?: string;
  contactName?: string;
  phoneNumber?: string;
  remitNetCosts?: string;
  remitNetFees?: string;
  state?: string;
  updCollector?: string;
  zipCode?: string;
  fedTaxId?: string;
  city?: string;
  masterAgency?: string;
  electronicInterfaceType?: string;
  feeBasic?: string;
  exceptionFlag?: string;
  monitorCriterialList?: MagicKeyValue[];
  returnCriterialList?: IReturnCriteria[];
  feeInfo?: FeeInformationData;
}

export interface FeeInformationData {
  feeBasic?: string;
  remitNetCosts?: string;
  remitNetFees?: string;
  updCollector?: string;
  assignTo?: string;
}

export interface AgencyDetailsArgs {
  id: string;
  agencyCode: string;
}
export interface FeeInformationArgs {
  id: string;
  agencyCode: string;
}

export interface AgencyDetailsPayloads {
  data: {
    agency: AgencyDetails;
  };
}

export interface FeeInformationPayloads {
  data: FeeInformationData;
}

export interface AgencyDetailsReducer {
  [key: string]: AgencyDetails;
}

export interface FeeInformationReducer {
  [key: string]: FeeInformationData;
}

export enum ElectronicType {
  IN_HOUSE = '0',
  NON_ELECTRONIC = '1',
  ELECTRONIC_GENERATE_WORKSHEET = '2',
  ELECTRONIC_NOT_GENERATE = '3'
}

export interface IPayloadDeleteReturn {
  data: IReturnCriteria[];
}

export interface IPayloadDeleteMonitor {
  data: IMonitorCriteria[];
}

export enum DateType {
  DAY = '0',
  WEEK = '1',
  MONTH = '2'
}

export enum SuffixDate {
  DAY = 'day',
  WEEK = 'week',
  MONTH = 'month'
}
