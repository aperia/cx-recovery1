import React, { useMemo } from 'react';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_TEXT } from 'app/constants';
import {
  convertIntervalType,
  convertValueDescription,
  convertYesNoText,
  getRefDataFromCode
} from 'pages/__commons/helpers';
import { Col, Row } from 'react-bootstrap';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import TagLine from 'app/components/Tagline';
import { IReturnCriteria } from 'pages/__commons/ReturnMaintenance/types';
import classnames from 'classnames';

interface IReturnDeleteBodyProps {
  item: IReturnCriteria;
}

interface IGenerateValue {
  columnName: string;
  value: string | RefDataValue;
}

const ReturnDeleteBody: React.FC<IReturnDeleteBodyProps> = ({ item }) => {
  const { t } = useTranslation();
  const { data: noteCodeData } = useGetRefDataQuery('noteCode');
  const { data: calendarCodeData } = useGetRefDataQuery('calendarCode');

  const generateValue = useMemo(() => {
    const {
      calendarCode,
      paymentDeleteCalendarCode,
      noteCode,
      calendarInterval,
      calendarIntervalType,
      nsfDeleteCalendarCode
    } = item;

    return {
      calendarCode: {
        columnName: t(I18N_TEXT.CALENDAR_CODE),
        value: convertValueDescription(
          getRefDataFromCode(calendarCodeData?.calendarCode, calendarCode)
        )
      },
      noteCodes: {
        columnName: t(I18N_TEXT.NOTE_CODE),
        value: noteCodeData?.noteCode.find(
          (item: { value: string; description: string }) =>
            item.value === noteCode
        ) as RefDataValue
      },
      calendarInterval: {
        columnName: t(I18N_TEXT.CALENDAR_INTERVAL),
        value: convertIntervalType(calendarInterval, calendarIntervalType)
      },
      nsfDeleteCalendarCode: {
        columnName: t(I18N_TEXT.NSF_DELETE_CALENDAR_CODE),
        value: convertYesNoText(nsfDeleteCalendarCode)
      },
      paymentDeleteCalendarCode: {
        columnName: t(I18N_TEXT.PAYMENT_DELETE_CODE),
        value: convertYesNoText(paymentDeleteCalendarCode)
      }
    };
  }, [item, noteCodeData, t, calendarCodeData]);

  return (
    <>
      <p className="mb-16">{t(I18N_TEXT.DELETE_RETURN_CONFIRM)}</p>
      <div className="bg-light-l20 border br-light-l04 br-radius-8 px-16 pt-8 pb-16">
        {Object.values(generateValue).map(
          ({ columnName, value }: IGenerateValue, index) => (
            <Row
              className={classnames(
                'gutters-8',
                columnName === t(I18N_TEXT.NOTE_CODE) ? 'mt-4' : 'mt-8'
              )}
              key={index}
            >
              <Col
                xs={5}
                className={classnames(
                  'text-capitalize',
                  columnName === t(I18N_TEXT.NOTE_CODE) && 'mt-4'
                )}
              >
                {columnName}:
              </Col>
              <Col xs={7}>
                {typeof value === 'string' ? (
                  <span className="content-empty fw-500">{value}</span>
                ) : (
                  <TagLine {...value} />
                )}
              </Col>
            </Row>
          )
        )}
      </div>
    </>
  );
};

export default ReturnDeleteBody;
