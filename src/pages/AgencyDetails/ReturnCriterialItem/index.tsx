import React from 'react';

// components
import { I18N_TEXT } from 'app/constants';
import { Button, Card } from 'app/_libraries/_dls/components';
import TagLine from 'app/components/Tagline';

// hooks
import { useSelectorId } from 'app/hooks';

// redux
import { selectorAgencyDetails } from 'pages/AgencyDetails/_redux/selectors';

// helpers
import { useTranslation } from 'app/_libraries/_dls/hooks';
import {
  convertIntervalType,
  convertIntervalTypeToValue,
  convertValueDescription,
  convertYesNoText,
  getRefDataFromCode
} from 'pages/__commons/helpers';
import { AgencyDetails } from 'pages/AgencyDetails/types';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import { IReturnCriteria } from 'pages/__commons/ReturnMaintenance/types';
import { useReturnForm } from 'pages/__commons/ReturnMaintenance/useReturnForm';
import { useDispatch } from 'react-redux';
import { RootModalOptions } from 'pages/__commons/RootModal/types';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import ReturnDeleteBody from './ReturnDeleteBody';
import { agencyMaintenanceActions } from 'pages/AgencyManagement/AgencyMaintenance/_redux';

interface ReturnCriterialItemProps extends IReturnCriteria {
  noteCodes?: RefDataValue[];
}

const ReturnCriterialItem: React.FC<ReturnCriterialItemProps> = props => {
  const {
    calendarCode,
    noteCode,
    noteCodes,
    calendarInterval,
    calendarIntervalType,
    nsfDeleteCalendarCode,
    paymentDeleteCalendarCode,
    id
  } = props;
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { agencyCode } = useSelectorId<AgencyDetails>(selectorAgencyDetails);
  const { data: calendarCodeData } = useGetRefDataQuery('calendarCode');
  const { data: noteCodeData } = useGetRefDataQuery('noteCode');

  const convertFieldData = {
    calendarCode: convertValueDescription(
      getRefDataFromCode(calendarCodeData?.calendarCode, calendarCode)
    ),
    noteCode: noteCodes?.length
      ? noteCodes[0]
      : (noteCodeData?.noteCode.find(
          (item: { value: string; description: string }) =>
            item.value === noteCode
        ) as RefDataValue),
    calendarInterval: convertIntervalType(
      calendarInterval,
      calendarIntervalType
    ),
    nsfDeleteCalendarCode: convertYesNoText(nsfDeleteCalendarCode),
    paymentDeleteCalendarCode: convertYesNoText(paymentDeleteCalendarCode)
  };

  const onDeleteReturn = () => {
    dispatch(agencyMaintenanceActions.deleteReturn({ id: id || '' }));
  };

  const showModalDeleteReturn = () => {
    const options: RootModalOptions = {
      autoClose: false,
      size: 'sm',
      title: t(I18N_TEXT.TITLE_DELETE_RETURN_MODAL),
      body: <ReturnDeleteBody item={props} />,
      onConfirm: onDeleteReturn,
      btnConfirmText: t(I18N_TEXT.DELETE),
      onCancel: () => dispatch(rootModalActions.close())
    };
    dispatch(rootModalActions.open(options));
  };

  const { openReturnForm } = useReturnForm();

  const handleOpenEditReturn = () => {
    openReturnForm({
      formType: 'EDIT',
      agencyCode,
      initData: {
        calendarCode,
        calendarIntervalType: convertIntervalTypeToValue(calendarIntervalType),
        noteCode,
        paymentDeleteCalendarCode,
        nsfDeleteCalendarCode,
        calendarInterval,
        id
      } as IReturnCriteria
    });
  };
  return (
    <Card className="card br-radius-8 mt-16 p-16">
      <div className="row">
        <div className="col">
          <div className="row align-items-center">
            <div className="w-xl-50 w-lg-100 w-md-60">
              <span className="color-grey text-capitalize mr-4">
                {t(I18N_TEXT.CALENDAR_CODE)}:
              </span>
              <span className="fw-500">{convertFieldData.calendarCode}</span>
            </div>
            <div className="w-xl-25 w-lg-60 w-md-60 order-xl-1 order-lg-3 order-md-4 mt-8">
              <span className="color-grey text-capitalize mr-4">
                {t(I18N_TEXT.CALENDAR_INTERVAL)}:
              </span>
              {convertFieldData.calendarInterval}
            </div>
            <div className="w-xl-25 w-lg-40 w-md-40 order-xl-2 order-lg-2 order-md-3 mt-8">
              <span className="color-grey text-capitalize mr-4">
                {t(I18N_TEXT.NSF_DELETE_CALENDAR_CODE)}:
              </span>
              {convertFieldData.nsfDeleteCalendarCode}
            </div>
            <div className="w-xl-75 w-lg-60 w-md-60 order-xl-3 order-lg-1 order-md-2 mt-4">
              <span className="color-grey text-capitalize mr-4">
                {t(I18N_TEXT.NOTE_CODES)}:
              </span>
              <TagLine {...convertFieldData?.noteCode} />
            </div>
            <div className="w-xl-25 w-lg-40 w-md-40 order-xl-5 order-lg-4 order-md-5 mt-8">
              <span className="color-grey text-capitalize mr-4">
                {t(I18N_TEXT.PAYMENT_DELETE_CODE)}:
              </span>
              {convertFieldData.paymentDeleteCalendarCode}
            </div>
            <div className="w-md-40 d-md-block d-lg-none order-md-1 text-right">
              <Button
                size="sm"
                variant="outline-danger"
                onClick={showModalDeleteReturn}
              >
                {t(I18N_TEXT.DELETE)}
              </Button>
              <Button
                size="sm"
                className="mr-n8"
                variant="outline-primary"
                onClick={handleOpenEditReturn}
              >
                {t(I18N_TEXT.EDIT)}
              </Button>
            </div>
          </div>
        </div>
        <div className="col-auto align-items-xl-center d-none d-lg-flex">
          <Button
            size="sm"
            variant="outline-danger"
            onClick={showModalDeleteReturn}
          >
            {t(I18N_TEXT.DELETE)}
          </Button>
          <Button
            className="mr-n8"
            size="sm"
            variant="outline-primary"
            onClick={handleOpenEditReturn}
          >
            {t(I18N_TEXT.EDIT)}
          </Button>
        </div>
      </div>
    </Card>
  );
};

export default ReturnCriterialItem;
