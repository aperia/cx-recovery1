import React from 'react';
import { I18N_TEXT } from 'app/constants';
import { Button, Card } from 'app/_libraries/_dls/components';
import MonitorDeleteBody from './MonitorDeleteBody';
import TagLine from 'app/components/Tagline';

// hooks
import { useSelectorId } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { selectorAgencyDetails } from 'pages/AgencyDetails/_redux/selectors';
import { useDispatch } from 'react-redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { monitorMaintenanceActions } from 'pages/__commons/MonitorMaintenance/_redux';

// helpers
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import {
  revertIntervalValueToType,
  convertRefDataFromArray,
  convertValueDescription,
  getRefDataFromCode
} from 'pages/__commons/helpers';
import { formatInterval } from 'pages/__commons/MonitorMaintenance/helpers';

// constants
import { AgencyDetails } from 'pages/AgencyDetails/types';
import { RootModalOptions } from 'pages/__commons/RootModal/types';
import { IMonitorCriteria } from 'pages/__commons/MonitorMaintenance/types';
import { agencyMaintenanceActions } from 'pages/AgencyManagement/AgencyMaintenance/_redux';
import { isEmpty } from 'app/helpers';

const MonitorCriteriaItem: React.FC<IMonitorCriteria> = props => {
  const {
    id,
    calendarCode,
    noteCodes,
    intervalStart,
    intervalStartUnit,
    intervalAction,
    intervalActionUnit,
    calendarCodePost,
    paymentDeleteCalendarCode
  } = props;
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { agencyCode } = useSelectorId<AgencyDetails>(selectorAgencyDetails);
  const { data: calendarCodeData } = useGetRefDataQuery('calendarCode');
  const { data: noteCodeData } = useGetRefDataQuery('noteCode');

  const convertFieldData = {
    calendarCode: convertValueDescription(
      getRefDataFromCode(calendarCodeData?.calendarCode, calendarCode)
    ),
    noteCodes: convertRefDataFromArray(
      noteCodes,
      noteCodeData?.noteCode
    ) as RefDataValue[],
    intervalStart: formatInterval(intervalStart, intervalStartUnit),
    intervalAction: formatInterval(intervalAction, intervalActionUnit),
    calendarCodePost,
    paymentDeleteCalendarCode
  };

  const handleEdit = () => {
    dispatch(
      monitorMaintenanceActions.onToggleFormModal({
        id,
        calendarCode: {
          value: calendarCode,
          description: convertFieldData.calendarCode
        },
        noteCodes: convertFieldData.noteCodes,
        intervalStart,
        intervalAction,
        intervalStartUnit: intervalStartUnit
          ? {
              value: intervalStartUnit,
              description: intervalStartUnit
            }
          : undefined,
        intervalActionUnit: intervalActionUnit
          ? {
              value: intervalActionUnit,
              description: intervalActionUnit
            }
          : undefined,
        calendarCodePost,
        paymentDeleteCalendarCode
      })
    );
  };

  const deleteMonitor = () => {
    dispatch(agencyMaintenanceActions.deleteMonitor({ id: id || '' }));
  };

  const showModalDeleteMonitor = () => {
    const options: RootModalOptions = {
      autoClose: false,
      size: 'sm',
      title: t(I18N_TEXT.TITLE_DELETE_MONITOR_MODAL),
      body: (
        <MonitorDeleteBody
          item={{
            ...props,
            intervalStartUnit: revertIntervalValueToType(intervalStartUnit),
            intervalActionUnit: revertIntervalValueToType(intervalActionUnit)
          }}
        />
      ),
      onConfirm: deleteMonitor,
      onCancel: () => dispatch(rootModalActions.close()),
      btnConfirmText: t(I18N_TEXT.DELETE)
    };
    dispatch(rootModalActions.open(options));
  };

  return (
    <Card className="card br-radius-8 mt-16 p-16">
      <div className="row">
        <div className="col">
          <div className="row align-items-center">
            <div className="w-xl-50 w-lg-100 w-md-70">
              <span className="color-grey text-capitalize mr-4">
                {t(I18N_TEXT.CALENDAR_CODE)}:
              </span>
              <span className="fw-500">{convertFieldData.calendarCode}</span>
            </div>
            <div className="w-xl-25 w-lg-60 w-md-70 order-xl-1 order-lg-3 order-md-4 mt-8">
              <span>
                <span className="color-grey text-capitalize mr-4">
                  {t(I18N_TEXT.INTERVAL_START)}:
                </span>
                {isEmpty(convertFieldData.intervalStart) ? (
                  <span className="content-empty fw-500"></span>
                ) : (
                  convertFieldData.intervalStart
                )}
              </span>
              <span className="d-xl-none d-lg-inline-block">
                <span className="text-capitalize color-grey mr-4">
                  <span className="px-8">•</span>
                  {t(I18N_TEXT.INTERVAL_ACTION)}:
                </span>
                {isEmpty(convertFieldData.intervalAction) ? (
                  <span className="content-empty fw-500"></span>
                ) : (
                  convertFieldData.intervalAction
                )}
              </span>
            </div>
            <div className="w-xl-25 w-lg-40 w-md-30 order-xl-2 order-lg-2 order-md-3 mt-8 ml-lg-0 ml-md-n14">
              <span className="color-grey text-capitalize mr-4">
                {t(I18N_TEXT.CALENDAR_CODE_POST)}:
              </span>
              {t(convertFieldData.calendarCodePost)}
            </div>
            <div className="w-xl-50 w-lg-60 w-md-70 order-xl-3 order-lg-1 order-md-2 mt-4">
              <span className="color-grey text-capitalize mr-4">
                {t(I18N_TEXT.NOTE_CODES)}:
              </span>
              {convertFieldData?.noteCodes.map((item: RefDataValue, i) => (
                <TagLine key={`${i}-${agencyCode}`} {...item} />
              ))}
            </div>
            <div className="w-xl-25 order-xl-4 d-none d-xl-block mt-8">
              <span className="text-capitalize color-grey mr-4">
                {t(I18N_TEXT.INTERVAL_ACTION)}:
              </span>
              {isEmpty(convertFieldData.intervalAction) ? (
                <span className="content-empty fw-500"></span>
              ) : (
                convertFieldData.intervalAction
              )}
            </div>
            <div className="w-xl-25 w-lg-40 w-md-30 order-xl-5 order-lg-4 order-md-5 mt-8 ml-lg-0 ml-md-n14 text-nowrap">
              <span className="color-grey text-capitalize mr-4 d-lg-inline-block d-md-none">
                {t(I18N_TEXT.PAYMENT_DELETE_CODE)}:
              </span>
              <span className="color-grey text-capitalize mr-4 d-lg-none d-md-inline-block">
                {t(I18N_TEXT.PYMT_DEL_CALENDAR_CODE)}:
              </span>
              {t(convertFieldData.paymentDeleteCalendarCode)}
            </div>
            <div className="w-md-30 d-md-block d-lg-none order-md-1 text-right">
              <Button
                size="sm"
                variant="outline-danger"
                onClick={showModalDeleteMonitor}
              >
                {t(I18N_TEXT.DELETE)}
              </Button>
              <Button
                size="sm"
                className="mr-n8"
                variant="outline-primary"
                onClick={handleEdit}
              >
                {t(I18N_TEXT.EDIT)}
              </Button>
            </div>
          </div>
        </div>
        <div className="col-auto align-items-xl-center d-none d-lg-flex">
          <Button
            size="sm"
            variant="outline-danger"
            onClick={showModalDeleteMonitor}
          >
            {t(I18N_TEXT.DELETE)}
          </Button>
          <Button
            className="mr-n8"
            size="sm"
            variant="outline-primary"
            onClick={handleEdit}
          >
            {t(I18N_TEXT.EDIT)}
          </Button>
        </div>
      </div>
    </Card>
  );
};

export default MonitorCriteriaItem;
