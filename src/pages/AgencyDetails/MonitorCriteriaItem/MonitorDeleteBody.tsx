import React, { useMemo } from 'react';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import {
  convertIntervalType,
  convertRefDataFromArray,
  convertValueDescription,
  convertYesNoText,
  getRefDataFromCode
} from 'pages/__commons/helpers';
import { I18N_TEXT } from 'app/constants';
import TagLine from 'app/components/Tagline';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import { Col, Row } from 'react-bootstrap';
import { IMonitorCriteria } from 'pages/__commons/MonitorMaintenance/types';

import classnames from 'classnames';

interface IMonitorDeleteBodyProps {
  item: IMonitorCriteria;
}

interface IGenerateValue {
  columnName: string;
  value: string | RefDataValue[];
}

const MonitorDeleteBody: React.FC<IMonitorDeleteBodyProps> = ({ item }) => {
  const { t } = useTranslation();
  const { data: noteCodeData } = useGetRefDataQuery('noteCode');
  const { data: calendarCodeData } = useGetRefDataQuery('calendarCode');

  const generateValue = useMemo(() => {
    const {
      calendarCode,
      calendarCodePost,
      intervalAction,
      noteCodes,
      intervalStart,
      paymentDeleteCalendarCode,
      intervalActionUnit,
      intervalStartUnit
    } = item;

    return {
      calendarCode: {
        columnName: t(I18N_TEXT.CALENDAR_CODE),
        value: convertValueDescription(
          getRefDataFromCode(calendarCodeData?.calendarCode, calendarCode)
        )
      },
      noteCodes: {
        columnName: t(I18N_TEXT.NOTE_CODES),
        value: convertRefDataFromArray(
          noteCodes,
          noteCodeData?.noteCode
        ) as RefDataValue[]
      },
      intervalStart: {
        columnName: t(I18N_TEXT.INTERVAL_START),
        value: convertIntervalType(intervalStart, intervalStartUnit)
      },
      intervalAction: {
        columnName: t(I18N_TEXT.INTERVAL_ACTION),
        value: convertIntervalType(intervalAction, intervalActionUnit)
      },
      calendarCodePost: {
        columnName: t(I18N_TEXT.CALENDAR_CODE_POST),
        value: convertYesNoText(calendarCodePost)
      },
      paymentDeleteCalendarCode: {
        columnName: t(I18N_TEXT.PAYMENT_DELETE_CODE),
        value: convertYesNoText(paymentDeleteCalendarCode)
      }
    };
  }, [item, calendarCodeData, noteCodeData, t]);

  return (
    <>
      <p className="mb-16">
        {t(I18N_TEXT.DELETE_MONITOR_CONFIRM)}
      </p>
      <div className="bg-light-l20 border br-light-l04 br-radius-8 px-16 pt-8 pb-16">
        {Object.values(generateValue).map(
          ({ columnName, value }: IGenerateValue, index) => (
            <Row
              className={classnames(
                'gutters-8',
                columnName === t(I18N_TEXT.NOTE_CODES)
                  ? 'mt-4'
                  : 'mt-8'
              )}
              key={index}
            >
              <Col
                xs={5}
                className={classnames(
                  'text-capitalize',
                  columnName === t(I18N_TEXT.NOTE_CODES) && 'mt-4'
                )}
              >
                {columnName}:
              </Col>
              <Col xs={7}>
                {typeof value === 'string' ? (
                  <span className="content-empty fw-500">{value}</span>
                ) : (
                  value?.map((k: RefDataValue) => (
                    <TagLine key={k.value} {...k} />
                  ))
                )}
              </Col>
            </Row>
          )
        )}
      </div>
    </>
  );
};
export default MonitorDeleteBody;
