import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import agencyMaintenanceServices from 'pages/AgencyManagement/AgencyMaintenance/agencyMaintenanceServices';
import {
  FeeInformationArgs,
  FeeInformationPayloads,
  GET_FEE_INFORMATION
} from '../types';

export const getFeeInformation = createAsyncThunk<
  FeeInformationPayloads,
  FeeInformationArgs,
  ThunkAPIConfig
>(GET_FEE_INFORMATION, async (args, thunkAPI) => {
  try {
    const { agencyCode } = args;
    const { data } = await agencyMaintenanceServices.getFeeInformation({
      agencyCode
    });
    return { data };
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const getFeeInformationBuilder = (
  builder: ActionReducerMapBuilder<any>
) => {
  const { pending, fulfilled, rejected } = getFeeInformation;
  builder
    .addCase(pending, (draftState, action) => {})
    .addCase(fulfilled, (draftState, action) => {
      const { id } = action.meta.arg;
      const { data } = action.payload;

      draftState[id] = {
        ...draftState[id],
        feeInfo: data
      };
    })
    .addCase(rejected, draftState => {});
};
