import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import agencyMaintenanceServices from 'pages/AgencyManagement/AgencyMaintenance/agencyMaintenanceServices';
import {
  AgencyDetailsArgs,
  AgencyDetailsPayloads,
  GET_AGENCY_DETAILS
} from '../types';

export const getAgencyDetails = createAsyncThunk<
  AgencyDetailsPayloads,
  AgencyDetailsArgs,
  ThunkAPIConfig
>(GET_AGENCY_DETAILS, async (args, thunkAPI) => {
  try {
    const { agencyCode } = args;
    const { data } = await agencyMaintenanceServices.getAgencyDetails({
      agencyCode
    });

    return { data };
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const getAgencyBuilder = (builder: ActionReducerMapBuilder<any>) => {
  const { pending, fulfilled, rejected } = getAgencyDetails;
  builder
    .addCase(pending, (draftState, action) => {})
    .addCase(fulfilled, (draftState, action) => {
      const { id } = action.meta.arg;
      const { data } = action.payload;

      return {
        ...draftState,
        [id]: data.agency
      };
    })
    .addCase(rejected, draftState => {});
};
