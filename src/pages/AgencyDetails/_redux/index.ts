import { createSlice } from '@reduxjs/toolkit';
import { AgencyDetailsReducer } from '../types';
import { getAgencyDetails, getAgencyBuilder } from './getAgencyDetails';
import { getFeeInformation, getFeeInformationBuilder } from './getFeeInformation';

const initialState: AgencyDetailsReducer = {};

export const { actions, reducer } = createSlice({
  name: 'agencyDetails',
  initialState: initialState,
  reducers: {},
  extraReducers: builder => {
    getAgencyBuilder(builder);
    getFeeInformationBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getAgencyDetails,
  getFeeInformation
};

export { combineActions as agencyDetailsActions };
