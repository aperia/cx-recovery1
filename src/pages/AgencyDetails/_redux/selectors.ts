import { createSelector } from '@reduxjs/toolkit';
import { IReturnCriteria } from 'pages/__commons/ReturnMaintenance/types';
import { AgencyDetails, DEFAULT_AGENCY_DETAIL, DEFAULT_FEE_INFORMATION } from '../types';

const selectorAgencyDetailsData = (
  states: RootState,
  storeId: string
): AgencyDetails => states?.agencyDetails[storeId] || DEFAULT_AGENCY_DETAIL;

const getReturnCriteriaList = (
  states: RootState,
  storeId: string
): IReturnCriteria[] =>
  states?.agencyDetails[storeId]?.returnCriterialList || [];

export const selectorAgencyDetails = createSelector(
  selectorAgencyDetailsData,
  (data: AgencyDetails) => data
);

export const selectorReturnCriteriaList = createSelector(
  getReturnCriteriaList,
  (data: IReturnCriteria[]) => data
);

export const selectorFeeInfo = createSelector(
  selectorAgencyDetailsData,
  (data: AgencyDetails) => {
    return data?.feeInfo || DEFAULT_FEE_INFORMATION
  }
);
