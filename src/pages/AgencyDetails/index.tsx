import React, { useRef } from 'react';

// component
import { SimpleBar } from 'app/_libraries/_dls/components';
import AgencyUserDetail from './AgencyUserDetail';
import InfoBarMenu from './InfoBarMenu';

// hooks
import { AccountDetailProvider } from 'app/hooks';

// helper
import { AgencyTabProps } from './types';

const AgencyDetailsPage: React.FC<AgencyTabProps> = ({
  id,
  agencyCode,
  companyName
}) => {
  const title = `${agencyCode} - ${companyName}`;

  const contextValueRef = useRef({
    storeId: id,
    accNumber: ''
  });

  return (
    <AccountDetailProvider value={contextValueRef.current}>
      <div className="d-flex">
        <div className="info-bar-main">
          <SimpleBar>
            <AgencyUserDetail title={title} />
          </SimpleBar>
        </div>
        <InfoBarMenu />
      </div>
    </AccountDetailProvider>
  );
};

export default AgencyDetailsPage;
