import { createApi, fetchBaseQuery } from '@reduxjs/toolkit/query/react';

export const refDataApi = createApi({
  reducerPath: 'refDataApi',
  baseQuery: fetchBaseQuery({}),
  endpoints: builder => ({
    getRefData: builder.query<Record<string, RefDataValue[]>, string>({
      query: componentId => `refData/${componentId}.json`
    })
  })
});

// Export hooks for usage in functional components
export const { useGetRefDataQuery } = refDataApi;
