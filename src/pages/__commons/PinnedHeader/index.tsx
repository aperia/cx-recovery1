import React from 'react';
import { batch, useDispatch } from 'react-redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelectorId } from 'app/hooks';

// components
import If from 'pages/__commons/If';
import TogglePin from 'app/components/InfoBar/TogglePin';

// constants
import { actionsInfoBar, InfoBarSection } from 'app/components/InfoBar/_redux';

// selectors
import { selectPinned } from 'app/components/InfoBar/_redux/selectors';
import classNames from 'classnames';

export interface PinnedHeaderProps {
  storeId: string;
  itemIdPinned: InfoBarSection;
  title?: string;
  className?: string;
  renderProps?: React.ReactNode;
}
const PinnedHeader: React.FC<PinnedHeaderProps> = ({
  storeId,
  itemIdPinned,
  title,
  className,
  renderProps = null
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const pinned = useSelectorId(selectPinned);
  const isPinned = pinned === itemIdPinned;

  const handleTogglePin = () => {
    batch(() => {
      const thisPinned = isPinned ? undefined : itemIdPinned;

      dispatch(
        actionsInfoBar.updatePinned({
          storeId,
          pinned: thisPinned
        })
      );

      if (thisPinned) {
        dispatch(actionsInfoBar.updateRecover({ storeId, recover: undefined }));
      }

      dispatch(actionsInfoBar.updateIsExpand({ storeId, isExpand: !isPinned }));
    });
  };

  return (
    <div
      className={classNames(
        'd-flex flex-row column align-items-center justify-content-between',
        className
      )}
    >
      <div className="d-inline-flex">
        <If condition={!!title}>
          <h5 className="mr-8">{t(title)}</h5>
        </If>
        <TogglePin isPinned={isPinned} onToggle={handleTogglePin} />
      </div>
      {renderProps}
    </div>
  );
};

export default PinnedHeader;
