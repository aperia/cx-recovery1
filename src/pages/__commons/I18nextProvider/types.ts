export const ACTION_TYPE_MULTIPLE_LANGUAGE = 'i18next/getMultipleLanguage';

type TResource = Record<string, string>;

export interface ILang {
  lang: string;
}

export interface Resource {
  resource: TResource;
}

export interface IResource extends ILang {
  data: TResource;
}

export interface II18nextResponse extends ILang, Resource {}

export interface II18nextReturn extends ILang, Resource {}

export interface II18nextArg extends Partial<ILang> {}
export interface II18nextReducer extends ILang {
  resources: Record<string, TResource>;
  isLoading: boolean;
}
