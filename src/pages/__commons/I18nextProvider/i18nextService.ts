import axios from 'axios';
const i18nextService = {
  async getMultipleLanguage(lang: string) {
    const baseURL = `i18n`;
    let returnData = { lang, resource: {} };
    const data = await axios.get<{ key: string }>(
      `${baseURL}/${lang}/main.json`
    );
    returnData = {
      ...returnData,
      resource: { ...returnData.resource, ...data.data }
    };
    return { data: returnData };
  }
};

export default i18nextService;
