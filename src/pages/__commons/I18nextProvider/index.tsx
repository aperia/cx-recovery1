import React, { ReactElement, useEffect } from 'react';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { i18nextActions } from 'pages/__commons/I18nextProvider/_redux/reducers';
import { getResource } from 'pages/__commons/I18nextProvider/_redux/selectors';

// components/types
import { I18nextProvider } from 'app/_libraries/_dls/components';
import { IResource } from 'pages/__commons/I18nextProvider/types';

const I18next: React.FC = ({ children }) => {
  const dispatch = useDispatch();

  const resource = useSelector<RootState, IResource>(getResource);

  useEffect(() => {
    dispatch(i18nextActions.getMultipleLanguage({}));
  }, [dispatch]);

  return (
    <I18nextProvider resource={resource}>
      {children as ReactElement}
    </I18nextProvider>
  );
};

export default I18next;
