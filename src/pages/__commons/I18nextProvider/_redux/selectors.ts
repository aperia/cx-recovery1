import { createSelector } from '@reduxjs/toolkit';

export const getResource = createSelector(
  [
    (states: RootState) => states.i18next.resources,
    (states: RootState) => states.i18next.lang
  ],
  (resources: Record<string, Record<string, string>>, lang: string) => {
    return {
      lang,
      data: resources[lang]
    };
  }
);

export const getLang = createSelector(
  [(states: RootState) => states.i18next.lang],
  (lang: string) => lang
);
