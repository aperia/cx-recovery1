import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import {
  II18nextReturn,
  II18nextArg,
  II18nextReducer,
  ACTION_TYPE_MULTIPLE_LANGUAGE
} from '../types';
import { mappingDataFromObj } from 'app/helpers';
import i18nextService from '../i18nextService';

export const getMultipleLanguage = createAsyncThunk<
  II18nextReturn,
  II18nextArg,
  ThunkAPIConfig
>(ACTION_TYPE_MULTIPLE_LANGUAGE, async ({ lang = 'en' }, thunkAPI) => {
  try {
    const { mapping } = thunkAPI.getState();

    const { data } = await i18nextService.getMultipleLanguage(lang);
    const i18nextMapping = mapping.data.i18next;
    return mappingDataFromObj<II18nextReturn>(data, i18nextMapping);
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getMultipleLanguageBuilder = (
  builder: ActionReducerMapBuilder<II18nextReducer>
) => {
  builder
    .addCase(getMultipleLanguage.pending, draftState => {
      draftState.isLoading = true;
    })
    .addCase(getMultipleLanguage.fulfilled, (draftState, action) => {
      const { lang, resource } = action.payload;

      draftState.resources[lang] = { ...resource };
      draftState.lang = lang;
      draftState.isLoading = false;
    })
    .addCase(getMultipleLanguage.rejected, draftState => {
      draftState.isLoading = false;
    });
};
