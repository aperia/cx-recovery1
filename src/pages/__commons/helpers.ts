import { generateDescription } from 'app/components';
import { isEmpty, isUndefined } from 'app/helpers';
import { DEFAULT_PAGE_SIZE } from 'app/_libraries/_dls/components/Pagination/constants';
import { DateType, SuffixDate } from 'pages/AgencyDetails/types';

const ASSIGN_CODE = '999';

// convertValueDescription
export const convertValueDescription = (
  itemValue: RefDataValue | undefined | string
) => {
  if (isEmpty(itemValue) || isUndefined(itemValue)) return '';

  if (typeof itemValue === 'string') return itemValue;

  return `${itemValue.value} - ${itemValue.description}`;
};

export const getDescriptionFromCode = (
  options?: RefDataValue[],
  code?: string,
  isCombine?: boolean
): string => {
  if (!Array.isArray(options) || !options.length || !code) return '';
  const itemValue =
    options.find(item => item.value === code) || ({} as RefDataValue);

  return generateDescription(
    itemValue,
    isUndefined(isCombine) ? true : isCombine
  );
};

// getRefDataFromCode
export const getRefDataFromCode = (
  refOptions: RefDataValue[] = [],
  value?: string
) => {
  return refOptions.find(option => option.value === value);
};

// convertRefDataFromArray
export const convertRefDataFromArray = (
  values: string[] = [],
  refOptions: RefDataValue[] = []
) => {
  return (
    values?.map((note: string) =>
      refOptions.find(
        (ref: { value: string; description: string }) => ref.value === note
      )
    ) || []
  );
};

// convertIntervalType
export const checkPluralValue = (value: string, suffix = SuffixDate.DAY) => {
  const valueData = parseInt(value);
  if (Number.isNaN(valueData)) return '';
  return valueData > 1 || valueData === 0
    ? `${value} ${suffix}s`
    : `${value} ${suffix}`;
};

export const convertIntervalType = (
  value: string | undefined,
  type: string | undefined
) => {
  if (isUndefined(value) || isUndefined(type)) return '';
  switch (type) {
    case DateType.DAY:
      return checkPluralValue(value, SuffixDate.DAY);
    case DateType.WEEK:
      return checkPluralValue(value, SuffixDate.WEEK);
    case DateType.MONTH:
      return checkPluralValue(value, SuffixDate.MONTH);
    default:
      return '';
  }
};

// convertYesNoText
export const convertYesNoText = (value?: string) => {
  if (isUndefined(value)) return '';

  if (['y', 'yes'].includes(value.toLowerCase())) return 'Yes';

  return 'No';
};

export const convertIntervalTypeToValue = (value: string | undefined) => {
  if (isUndefined(value)) return '';
  switch (value) {
    case '0':
      return 'days';
    case '1':
      return 'weeks';
    case '2':
      return 'months';
    default:
      return '';
  }
};

export const revertIntervalValueToType = (value: string | undefined) => {
  if (isUndefined(value)) return '';
  switch (value) {
    case 'days':
      return '0';
    case 'weeks':
      return '1';
    case 'months':
      return '2';
    default:
      return '';
  }
};
// convertAddress
export const convertAddress = (
  address1?: string,
  address2?: string,
  zipCode?: string,
  state?: string,
  city?: string
) => {
  const address1Data = address1 ? `${address1}, ` : '';
  const address2Data = address2 ? `${address2}, ` : '';
  const cityData = city ? `${city}, ` : '';
  const stateData = state ? `${state} ` : '';
  const zipCodeData = zipCode ? `${zipCode}` : '';

  return `${address1Data}${address2Data}${cityData}${stateData}${zipCodeData}`;
};

// checkNextPage
export const checkNextPage = ({
  currentPage,
  totalItem
}: {
  currentPage: number;
  totalItem: number;
}) => {
  if (totalItem !== 0 && totalItem % DEFAULT_PAGE_SIZE === 0) {
    return currentPage + 1;
  }
  return currentPage;
};

export const checkPreviousPage = ({
  currentPage,
  totalItem
}: {
  currentPage: number;
  totalItem: number;
}) => {
  if (
    totalItem !== 0 &&
    currentPage - 1 > 0 &&
    (totalItem - 1) % DEFAULT_PAGE_SIZE === 0
  )
    return currentPage - 1;
  return currentPage;
};

// checkCurrentAgencyCode
export const checkCurrentAgencyCode = (value?: string) =>
  value === ASSIGN_CODE ? 'assign' : 'recall';
