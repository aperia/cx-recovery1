import { IReturnCriteria, ReturnCriteriaFormState } from './../types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';


const initialState: ReturnCriteriaFormState = {
  formType: '',
  loading: false,
  inlineErrMsg: '',
  initialFormValues: undefined,
  returnCriteriaCodeEditing: undefined,
  error: null
};

export const { actions, reducer } = createSlice({
  initialState,
  name: 'returnCriteriaForm',
  reducers: {
    openForm: (
      draftState,
      action: PayloadAction<{
        formType: FormType;
        agencyCode?: string;
        initData?: IReturnCriteria;
      }>
    ) => {
      const { formType, agencyCode, initData } = action.payload;
      draftState.formType = formType;
      draftState.returnCriteriaCodeEditing = agencyCode;
      draftState.initialFormValues = initData;
    },
    closeForm: draftState => {
      draftState.formType = '';
      draftState.inlineErrMsg = '';
      draftState.initialFormValues = undefined;
    },
    setInlineErrMsg: (draftState, action: PayloadAction<string>) => {
      draftState.inlineErrMsg = action.payload;
    }
  },
  extraReducers: builder => {

  }
});

const combineActions = {
  ...actions,

};

export {
  combineActions as returnMaintenanceActions,
  reducer as returnMaintenance
};
