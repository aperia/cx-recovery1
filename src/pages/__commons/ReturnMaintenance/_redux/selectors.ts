import { createSelector } from '@reduxjs/toolkit';
import { IReturnCriteria } from '../types';

const getFormType = (state: RootState): FormType => state.returnCriteriaForm.formType;
const getInlineErrMsg = (state: RootState): string =>
  state.returnCriteriaForm.inlineErrMsg;
const getLoading = (state: RootState): boolean => state.returnCriteriaForm.loading;
const getInitialFormValues = (state: RootState): IReturnCriteria | undefined =>
  state.returnCriteriaForm.initialFormValues;
const getReturnCriteriaCodeEditing = (state: RootState): string | undefined =>
  state.returnCriteriaForm.returnCriteriaCodeEditing;

export const selectFormType = createSelector(getFormType, data => data);
export const selectFormOpened = createSelector(getFormType, data => !!data);
export const selectLoading = createSelector(getLoading, data => data);
export const selectInlineErrMsg = createSelector(getInlineErrMsg, data => data);
export const selectInitialFormValues = createSelector(
  getInitialFormValues,
  data => data
);
export const selectReturnCriteriaCodeEditing = createSelector(
  getReturnCriteriaCodeEditing,
  data => data
);
