export const RETURN_CRITERIA_FORM_VIEW = 'returnCriteria';

export const RETURN_CRITERIA_FIELD = {
  CALENDAR_CODE: {
    name: 'calendarCode',
    label: 'txt_calendar_code'
  },
  NOTE_CODE: {
    name: 'noteCode',
    label: 'txt_note_codes'
  },
  CALENDAR_INTERVAL: { name: 'calendarInterval', label: 'txt_enter_counts' },
  CALENDAR_INTERVAL_TYPE: {
    name: 'calendarIntervalType'
  },
  PAYMENT_DELETE_CALENDAR_CODE: {
    name: 'paymentDeleteCalendarCode'
  },
  NSF_DELETE_CALENDAR_CODE: { name: 'nsfDeleteCalendarCode' }
};
