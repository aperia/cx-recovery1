import { apiService } from 'app/utils/api.service';
import { IReturnCriteria } from './types';

const returnMaintenanceServices = {
  getReturns() {
    const url: string = window.appConfig.api.return.getReturns;

    return apiService.post<IReturnCriteria[]>(url);
  }
};

export default returnMaintenanceServices;
