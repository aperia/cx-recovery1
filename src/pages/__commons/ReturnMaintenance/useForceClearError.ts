import { FormikErrors } from 'formik';
import { useEffect } from 'react';
import { IReturnCriteria } from './types';

export const useForceClearError: (
  setErrors: (errors: FormikErrors<IReturnCriteria>) => void,
  isError: boolean,
) => void = (setErrors, isError) => {
  useEffect(() => {
    if (isError) {
        setErrors({})
    }
  }, [setErrors, isError]);
};
