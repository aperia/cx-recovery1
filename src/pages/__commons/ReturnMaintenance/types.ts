import { SerializedError } from '@reduxjs/toolkit';

export interface IReturnCriteria {
  calendarCode: string;
  noteCode: string;
  calendarInterval: string;
  calendarIntervalType: string;
  paymentDeleteCalendarCode: string;
  nsfDeleteCalendarCode: string;
  agencyCode?: string;
  id?: string;
}

export interface ReturnMaintenanceProps {
  data: IReturnCriteria;
  loading: boolean;
  isAdding?: boolean;
}
export interface ReturnCriteriaFormState {
  formType: FormType;
  loading: boolean;
  error: SerializedError | null;
  inlineErrMsg: string;
  returnCriteriaCodeEditing?: string;
  initialFormValues?: IReturnCriteria;
}