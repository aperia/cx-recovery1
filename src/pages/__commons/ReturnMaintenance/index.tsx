import React, { useMemo } from 'react';

// components
import { Button, Pagination } from 'app/_libraries/_dls/components';
import { NoDataGrid } from 'app/components/NoDataGrid';
import { ReturnFormModal } from './ReturnFormModal';
import ReturnCriterialItem from 'pages/AgencyDetails/ReturnCriterialItem';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useReturnForm } from './useReturnForm';

// helpers
import { I18N_TEXT } from 'app/constants';
import { IReturnCriteria } from './types';
import { isEmpty } from 'app/helpers';
import { DEFAULT_PAGE_SIZE } from 'app/_libraries/_dls/components/Pagination/constants';

interface ReturnMaintenanceProps {
  data?: IReturnCriteria[];
  agencyCode: string;
  totalItem: number;
  currentPage?: number;
  onPageChange?(page: number): void;
}
const ReturnMaintenance: React.FC<ReturnMaintenanceProps> = props => {
  const { t } = useTranslation();
  const { data, agencyCode, totalItem, currentPage, onPageChange } = props;
  const { openReturnForm } = useReturnForm();
  const genBtnAddReturn = useMemo(() => {
    const isDisplayPagination = totalItem > DEFAULT_PAGE_SIZE;
    const addButton = (
      <Button
        className="text-capitalize"
        variant="outline-primary"
        size="sm"
        onClick={() => openReturnForm({ formType: 'ADD', agencyCode })}
      >
        {t(I18N_TEXT.ADD_RETURN_CRITERIA)}
      </Button>
    );

    if (!isEmpty(data)) {
      return (
        <>
          <div className="d-flex justify-content-between align-items-center mr-n8">
            <h5 className="text-capitalize">
              {t(I18N_TEXT.RETURN_CRITERIAL_LIST)}
            </h5>
            {addButton}
          </div>
          {data?.map((item, index) => (
            <ReturnCriterialItem key={index} {...item} />
          ))}
          {isDisplayPagination && (
            <div className="pt-16">
              <Pagination
                totalItem={totalItem}
                pageSize={[10]}
                pageNumber={currentPage}
                pageSizeValue={DEFAULT_PAGE_SIZE}
                compact
                onChangePage={onPageChange}
              />
            </div>
          )}
        </>
      );
    }
    return (
      <div>
        <h5 className="text-capitalize">
          {t(I18N_TEXT.RETURN_CRITERIAL_LIST)}
        </h5>
        <NoDataGrid>
          <div className="mt-24">{addButton}</div>
        </NoDataGrid>
      </div>
    );
  }, [
    totalItem,
    t,
    data,
    openReturnForm,
    agencyCode,
    currentPage,
    onPageChange
  ]);

  return (
    <div className="p-24 bg-white max-width-lg mx-auto">
      {genBtnAddReturn}
      <ReturnFormModal />
    </div>
  );
};

export default ReturnMaintenance;
