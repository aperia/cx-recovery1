import React from 'react';
import { useFormik, FormikProvider } from 'formik';

// components
import {
  InlineMessage,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import {
  DropdownControl,
  YesNoQuestionControl,
  TextBoxControl
} from 'app/components/FormControl';
import ApiErrorDetail from 'pages/__commons/ApiErrorDetail';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useReturnForm } from './useReturnForm';
import { useResetForm } from './useResetForm';
import { validationSchema } from './useReturnForm';

// redux
import { saveReturn } from 'pages/AgencyManagement/AgencyMaintenance/_redux/saveReturn';

// constants
import { NUMERIC_REPLACE_REGEX, I18N_TEXT } from 'app/constants';
import { RETURN_CRITERIA_FIELD } from './constants';

export const ReturnFormModal: React.FC = () => {
  const { t } = useTranslation();

  const {
    formOpened,
    inlineErrMsg,
    calendarCodeOptions,
    noteCodesOptions,
    calendarIntervalUnitOptions,
    closeReturnForm,
    onSubmit,
    isLoading,
    initialValues,
    addOrEdit
  } = useReturnForm();

  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema,
    validateOnMount: false
  });
  useResetForm(formOpened, formik.resetForm, initialValues);

  return (
    <Modal show={formOpened} loading={isLoading}>
      <ModalHeader border closeButton onHide={closeReturnForm}>
        <ModalTitle className="text-capitalize">
          {addOrEdit === 'EDIT'
            ? t(I18N_TEXT.EDIT_RETURN_CRITERIA)
            : t(I18N_TEXT.ADD_RETURN_CRITERIA)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody>
        <ApiErrorDetail
          forAction={[saveReturn.rejected.type]}
          className="mb-24"
        />
        {inlineErrMsg && (
          <InlineMessage className="mb-24" variant="danger">
            {t(inlineErrMsg)}
          </InlineMessage>
        )}
        <FormikProvider value={formik}>
          <DropdownControl
            name={RETURN_CRITERIA_FIELD.CALENDAR_CODE.name}
            label={t(RETURN_CRITERIA_FIELD.CALENDAR_CODE.label)}
            required
            options={calendarCodeOptions}
            isCombine
            forceError={!!inlineErrMsg}
          />

          <DropdownControl
            name={RETURN_CRITERIA_FIELD.NOTE_CODE.name}
            label={t(RETURN_CRITERIA_FIELD.NOTE_CODE.label)}
            className="mt-16"
            required
            options={noteCodesOptions}
            forceError={!!inlineErrMsg}
            isCombine
          />
          <h6 className="color-grey mt-16 text-capitalize fs-14">
            {t('txt_calendar_interval')}:
            <span className="color-red ml-2"> *</span>
          </h6>
          <div className="d-flex mt-8">
            <TextBoxControl
              name={RETURN_CRITERIA_FIELD.CALENDAR_INTERVAL.name}
              label={RETURN_CRITERIA_FIELD.CALENDAR_INTERVAL.label}
              className="w-65 pr-8"
              maxLength={3}
              regex={NUMERIC_REPLACE_REGEX}
            />
            <DropdownControl
              name={RETURN_CRITERIA_FIELD.CALENDAR_INTERVAL_TYPE.name}
              className="w-35 no-label pl-8"
              required
              options={calendarIntervalUnitOptions}
            />
          </div>
          <h6 className="color-grey mt-16 mb-8 text-capitalize fs-14">
            {t('txt_payment_delete_code')}:
            <span className="color-red ml-2"> *</span>
          </h6>
          <p className="color-grey my-8">
            {t('txt_delete_calendar_code_payment_post')}
          </p>
          <YesNoQuestionControl
            name={RETURN_CRITERIA_FIELD.PAYMENT_DELETE_CALENDAR_CODE.name}
            required
          />

          <h6 className="color-grey mt-16 text-capitalize fs-14">
            {t('txt_nsf_delete_calendar_code')}:
            <span className="color-red"> *</span>
          </h6>
          <p className="color-grey my-8">
            {t('txt_delete_calendar_code_payment_check')}
          </p>
          <YesNoQuestionControl
            name={RETURN_CRITERIA_FIELD.NSF_DELETE_CALENDAR_CODE.name}
            required
          />
        </FormikProvider>
      </ModalBody>
      <ModalFooter
        cancelButtonText={t(I18N_TEXT.CANCEL)}
        okButtonText={
          addOrEdit === 'EDIT' ? t(I18N_TEXT.SAVE) : t(I18N_TEXT.SUBMIT)
        }
        onCancel={closeReturnForm}
        onOk={formik.submitForm}
        disabledOk={!formik.isValid || (addOrEdit === 'ADD' && !formik.dirty)}
      />
    </Modal>
  );
};
