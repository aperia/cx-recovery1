import { useCallback, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';

// constant

// redux
import { agencyMaintenanceActions } from 'pages/AgencyManagement/AgencyMaintenance/_redux';
import {
  selectReturnCriteriaCodeEditing,
  selectFormOpened,
  selectFormType,
  selectInlineErrMsg,
  selectLoading,
  selectInitialFormValues
} from './_redux/selectors';
import { returnMaintenanceActions } from './_redux/reducers';

// hooks
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';

// helpers, types, constants
import { checkEmpty } from 'app/helpers';
import { IReturnCriteria } from './types';
import { RETURN_CRITERIA_FIELD } from './constants';
import { revertIntervalValueToType } from '../helpers';

export const initialDefaultValues: IReturnCriteria = {
  calendarCode: '',
  noteCode: '',
  calendarInterval: '',
  calendarIntervalType: 'days',
  paymentDeleteCalendarCode: '',
  nsfDeleteCalendarCode: ''
};

export const validationSchema = Yup.object().shape({
  [RETURN_CRITERIA_FIELD.CALENDAR_CODE.name]: Yup.string().required(
    'txt_calendar_code_is_required'
  ),
  [RETURN_CRITERIA_FIELD.NOTE_CODE.name]: Yup.string().required(
    'txt_note_codes_is_required'
  ),
  [RETURN_CRITERIA_FIELD.CALENDAR_INTERVAL.name]: Yup.string().required(
    'txt_calendar_interval_is_required'
  ),

  [RETURN_CRITERIA_FIELD.PAYMENT_DELETE_CALENDAR_CODE.name]:
    Yup.string().required(),
  [RETURN_CRITERIA_FIELD.NSF_DELETE_CALENDAR_CODE.name]: Yup.string().required()
});

export const useReturnForm = () => {
  const dispatch = useDispatch();
  const formOpened = useSelector(selectFormOpened);
  const addOrEdit = useSelector(selectFormType);
  const isLoading = useSelector(selectLoading);
  const inlineErrMsg = useSelector(selectInlineErrMsg);
  const initialReturnCriteriaValues = useSelector(selectInitialFormValues);
  const returnCriteriaCodeEditing = useSelector(
    selectReturnCriteriaCodeEditing
  );

  const initialValues = checkEmpty([
    initialReturnCriteriaValues,
    initialDefaultValues
  ]) as IReturnCriteria;

  const { data: calendarCode } = useGetRefDataQuery('calendarCode');
  const { data: noteCodes } = useGetRefDataQuery('noteCodes');
  const { data: calendarIntervalUnit } = useGetRefDataQuery(
    'calendarIntervalUnit'
  );

  // generate ref data options
  const calendarCodeOptions: RefDataValue[] = useMemo(
    () => checkEmpty([calendarCode?.calendarCode as RefDataValue[], []]),
    [calendarCode?.calendarCode]
  );

  const noteCodesOptions: RefDataValue[] = useMemo(
    () => checkEmpty([noteCodes?.noteCodes as RefDataValue[], []]),
    [noteCodes?.noteCodes]
  );

  const calendarIntervalUnitOptions: RefDataValue[] = useMemo(
    () =>
      checkEmpty([
        calendarIntervalUnit?.calendarIntervalUnit as RefDataValue[],
        []
      ]),
    [calendarIntervalUnit?.calendarIntervalUnit]
  );

  // handle open modal
  const openReturnForm = useCallback(
    ({
      formType,
      agencyCode,
      initData
    }: {
      formType: FormType;
      agencyCode?: string;
      initData?: IReturnCriteria;
    }) => {
      dispatch(
        returnMaintenanceActions.openForm({
          formType,
          agencyCode,
          initData
        })
      );
    },
    [dispatch]
  );

  // handle close modal
  const closeReturnForm = useCallback(() => {
    dispatch(returnMaintenanceActions.closeForm());
  }, [dispatch]);

  const onSubmit = useCallback(
    (values: IReturnCriteria) => {
      if (addOrEdit === 'ADD') {
        dispatch(
          agencyMaintenanceActions.saveReturn({
            ...values,
            calendarIntervalType: revertIntervalValueToType(
              values.calendarIntervalType
            ),
            agencyCode: returnCriteriaCodeEditing || '',
            formType: 'ADD'
          })
        );
      } else {
        dispatch(
          agencyMaintenanceActions.saveReturn({
            ...values,
            calendarIntervalType: revertIntervalValueToType(
              values.calendarIntervalType
            ),

            id: values.id,
            agencyCode: returnCriteriaCodeEditing || '',
            formType: 'EDIT'
          })
        );
      }
    },
    [dispatch, addOrEdit, returnCriteriaCodeEditing]
  );

  return {
    formOpened,
    inlineErrMsg,
    openReturnForm,
    calendarCodeOptions,
    noteCodesOptions,
    calendarIntervalUnitOptions,
    closeReturnForm,
    onSubmit,
    isLoading,
    initialValues,
    addOrEdit
  };
};
