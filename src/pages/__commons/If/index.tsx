import React from 'react';

interface IfProps {
  condition: boolean;
}

const If: React.FC<IfProps> = ({ condition, children }) => {
  if (condition) {
    return <>{children}</>;
  }

  return null;
};

export default If;
