import React, { useMemo } from 'react';

// helpers
import { get } from 'app/helpers';

// redux-store
import { useDispatch, useSelector } from 'react-redux';

// components
import Toast, {
  ToastDataType,
  ToastProps
} from 'app/_libraries/_dls/components/Toast';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// types
import { ToastType } from 'pages/__commons/ToastNotifications/_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface ToastNotificationsProps
  extends Omit<ToastProps, 'data' | 'onClose'> {
  dataTestId?: string;
}

const ToastNotifications: React.FC<ToastNotificationsProps> = ({
  id,
  delay = 3000,
  animation = false,
  anchor,
  autohide = true,
  direction = 'top',
  duration = 600,
  from = 0,
  to = 136,
  dataTestId
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const toasts = useSelector<RootState, ToastType[]>(state =>
    get(state.toastNotifications, 'notifications', [])
  );
  const data = useMemo(() => {
    return toasts.map(({ id: _id, type, message, msgVariables }) => {
      return {
        id: _id,
        type,
        message: message?.startsWith('txt') ? t(message, msgVariables) : message
      };
    }) as ToastDataType[];
  }, [toasts, t]);

  const handleClose = (_id: string) => {
    dispatch(actionsToast.removeToast({ id: _id }));
  };

  return (
    <Toast
      id={id}
      dataTestId={`${dataTestId}-toastNotification`}
      from={from}
      to={to}
      data={data}
      delay={delay}
      animation={animation}
      onClose={handleClose}
      anchor={anchor}
      autohide={autohide}
      direction={direction}
      duration={duration}
    />
  );
};

export default ToastNotifications;
