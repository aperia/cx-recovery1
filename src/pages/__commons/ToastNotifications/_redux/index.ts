import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { get, set, pullAllBy } from 'app/helpers/lodash';
export interface ToastType {
  id?: string;
  show?: boolean;
  type?: 'attention' | 'success' | 'warning' | 'error';
  message?: string;
  msgVariables?: Record<string, string | number>;
}

export interface ToastState {
  notifications: ToastType[];
}

const { actions, reducer } = createSlice({
  name: 'toastNotifications',
  initialState: {} as ToastState,
  reducers: {
    addToast: (draftState, action: PayloadAction<ToastType>) => {
      const { payload } = action;
      const now = new Date();
      payload.id = now.getTime().toString();
      const notifications = get(draftState, 'notifications', []);
      const newNotifications = [payload, ...notifications];
      set(draftState, 'notifications', newNotifications);
    },
    removeToast: (draftState, action: PayloadAction<Pick<ToastType, 'id'>>) => {
      const { id } = action.payload;
      const toasts = [...draftState.notifications];
      pullAllBy(toasts, [{ id }], 'id');
      set(draftState, 'notifications', toasts);
    },
    clearAllToast: draftState => {
      const newToasts: ToastType[] = [];
      set(draftState, 'notifications', newToasts);
    }
  }
});

const allActions = {
  ...actions
};

export { allActions as actionsToast, reducer };
