import { FormikState } from 'formik';
import { useEffect } from 'react';

export const useResetForm: <T>(
  formOpened: boolean,
  reset: (nextState?: Partial<FormikState<T>> | undefined) => void,
  initialValues: T
) => void = (formOpened, reset, initialValues) => {
  useEffect(() => {
    if (formOpened) {
      reset({ values: initialValues });
    }
  }, [formOpened, initialValues, reset]);
};
