import { useMemo } from 'react';

export const useAssignedLevelField = (
  assignedLevelOptions: RefDataValue[],
  electronicInterfaceType?: string
) => {
  const { isAssignedLevelDisabled, assignedLevelOptionsFiltered } =
    useMemo(() => {
      if (!electronicInterfaceType) {
        return {
          isAssignedLevelDisabled: true,
          assignedLevelOptionsFiltered: assignedLevelOptions
        };
      }
      // 0 - In-house Agency
      // WHEN Electronic Interface Type is "0 - In-house Agency - No electronic interface"
      // THEN this option is available
      if (electronicInterfaceType !== '0') {
        return {
          isAssignedLevelDisabled: false,
          assignedLevelOptionsFiltered: assignedLevelOptions.filter(
            opts => opts.value !== '0'
          )
        };
      }
      return {
        isAssignedLevelDisabled: false,
        assignedLevelOptionsFiltered: assignedLevelOptions
      };
    }, [electronicInterfaceType, assignedLevelOptions]);
  return { isAssignedLevelDisabled, assignedLevelOptionsFiltered };
};
