import React from 'react';

import {
  InlineMessage,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// hooks
import { useAgencyForm, validationSchema } from './useAgencyForm';
import { FormikProvider, useFormik } from 'formik';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAssignedLevelField } from './useAssignedLevelField';
import { useResetForm } from './useResetForm';

// components
import {
  DropdownControl,
  MaskedTextBoxControl,
  TextBoxControl
} from 'app/components/FormControl';
import { YesNoQuestionControl } from 'app/components/FormControl/YesNoQuestion';
import ApiErrorDetail from 'pages/__commons/ApiErrorDetail';

// constants
import { AGENCY_FIELD } from './constants';
import {
  ALPHA_NUMERIC_REPLACE_REGEX,
  ALPHA_NUMERIC_SPACE_REPLACE_REGEX,
  ALPHA_SPACE_REPLACE_REGEX,
  NUMERIC_REPLACE_REGEX
} from 'app/constants';

// redux
import { addAgency } from './_redux/addAgency';
import { editAgency } from './_redux/editAgency';

const generateVerbiage = (type: FormType) => {
  switch (type) {
    case 'EDIT':
      return { title: 'txt_edit_agency', action: 'txt_save' };
    case 'ADD':
      return { title: 'txt_add_agency', action: 'txt_submit' };
    default:
      return { title: '', action: '' };
  }
};

const AgencyForm = () => {
  const { t } = useTranslation();

  const {
    formOpened,
    inlineErrMsg,
    closeAgencyForm,
    agencyTypeOptions,
    feeBasicOptions,
    updCltrOnAssignToOptions,
    stateOptions,
    electronicInterfaceTypeOptions,
    assignedLevelOptions,
    onSubmit,
    isLoading,
    addOrEdit,
    initialValues
  } = useAgencyForm();

  // generate formik context
  const formik = useFormik({
    initialValues,
    onSubmit,
    validationSchema
  });

  useResetForm(formOpened, formik.resetForm, initialValues);

  const { isAssignedLevelDisabled, assignedLevelOptionsFiltered } =
    useAssignedLevelField(
      assignedLevelOptions,
      formik.values?.electronicInterfaceType
    );

  const { title, action } = generateVerbiage(addOrEdit);

  return (
    <>
      <Modal md show={formOpened} loading={isLoading}>
        <ModalHeader border closeButton onHide={closeAgencyForm}>
          <ModalTitle>{t(title)}</ModalTitle>
        </ModalHeader>

        <ModalBody>
          <ApiErrorDetail
            forAction={[addAgency.rejected.type, editAgency.rejected.type]}
            className="mb-24"
          />
          {inlineErrMsg && (
            <InlineMessage className="mb-24" variant="danger">
              {t(inlineErrMsg)}
            </InlineMessage>
          )}
          <FormikProvider value={formik}>
            <h5 className="fw-500">{t('txt_general_information')}</h5>
            <div className="row">
              <TextBoxControl
                name={AGENCY_FIELD.AGENCY_CODE.name}
                label={t(AGENCY_FIELD.AGENCY_CODE.label)}
                className="col-6 mt-16"
                maxLength={6}
                forceError={!!inlineErrMsg}
                regex={ALPHA_NUMERIC_REPLACE_REGEX}
                required
              />
              <DropdownControl
                name={AGENCY_FIELD.AGENCY_TYPE.name}
                label={t(AGENCY_FIELD.AGENCY_TYPE.label)}
                className="col-6 mt-16"
                required
                options={agencyTypeOptions}
                isCombine
              />
              <TextBoxControl
                name={AGENCY_FIELD.COMPANY_NAME.name}
                label={t(AGENCY_FIELD.COMPANY_NAME.label)}
                className="col-6 mt-16"
                maxLength={40}
                required
              />
              <TextBoxControl
                name={AGENCY_FIELD.MASTER_AGENCY.name}
                label={t(AGENCY_FIELD.MASTER_AGENCY.label)}
                className="col-6 mt-16"
                regex={ALPHA_NUMERIC_REPLACE_REGEX}
                maxLength={6}
              />
              <TextBoxControl
                name={AGENCY_FIELD.ADDRESS_1.name}
                label={t(AGENCY_FIELD.ADDRESS_1.label)}
                className="col-6 mt-16"
                regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
                maxLength={26}
              />
              <TextBoxControl
                name={AGENCY_FIELD.ADDRESS_2.name}
                label={t(AGENCY_FIELD.ADDRESS_2.label)}
                className="col-6 mt-16"
                regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
                maxLength={26}
              />
              <TextBoxControl
                name={AGENCY_FIELD.CITY.name}
                label={t(AGENCY_FIELD.CITY.label)}
                className="col-4 mt-16"
                maxLength={25}
                regex={ALPHA_SPACE_REPLACE_REGEX}
              />
              <DropdownControl
                name={AGENCY_FIELD.STATE.name}
                label={t(AGENCY_FIELD.STATE.label)}
                className="col-4 mt-16"
                isCombine
                options={stateOptions}
              />
              <TextBoxControl
                name={AGENCY_FIELD.ZIP_CODE.name}
                label={t(AGENCY_FIELD.ZIP_CODE.label)}
                className="col-4 mt-16"
                maxLength={10}
                regex={NUMERIC_REPLACE_REGEX}
              />
              <MaskedTextBoxControl
                name={AGENCY_FIELD.PHONE_NUMBER.name}
                label={t(AGENCY_FIELD.PHONE_NUMBER.label)}
                className="col-4 mt-16"
                mask={[
                  '(',
                  /\d/,
                  /\d/,
                  /\d/,
                  ')',
                  ' ',
                  /\d/,
                  /\d/,
                  /\d/,
                  '-',
                  /\d/,
                  /\d/,
                  /\d/,
                  /\d/
                ]}
                placeholder="(___) ___-____"
              />
              <TextBoxControl
                name={AGENCY_FIELD.FED_TAX_ID.name}
                label={t(AGENCY_FIELD.FED_TAX_ID.label)}
                className="col-4 mt-16"
                maxLength={12}
              />
              <TextBoxControl
                name={AGENCY_FIELD.CONTACT_NAME.name}
                label={t(AGENCY_FIELD.CONTACT_NAME.label)}
                className="col-4 mt-16"
                maxLength={40}
              />
              <div className="col-6 mt-16">
                <div className="d-flex">
                  <div className="w-40">
                    <p className="text-capitalize color-grey-d08 fw-500">
                      {t(AGENCY_FIELD.EXCEPTION_FLAG.label)}:
                    </p>
                  </div>
                  <div className="w-60">
                    <YesNoQuestionControl
                      name={AGENCY_FIELD.EXCEPTION_FLAG.name}
                    />
                  </div>
                </div>
                <p className="color-grey-l24 fs-12 mt-4">
                  {t('txt_exception_flag_description')}
                </p>
              </div>
            </div>
            <h5 className="fw-500 mt-24">{t('txt_fee_information')}</h5>
            <div className="row">
              <DropdownControl
                name={AGENCY_FIELD.FEE_BASIC.name}
                label={t(AGENCY_FIELD.FEE_BASIC.label)}
                className="col-6 mt-16"
                required
                options={feeBasicOptions}
                isCombine
              />
              <DropdownControl
                name={AGENCY_FIELD.UPD_CLTR_ON_ASSIGN_TO.name}
                label={t(AGENCY_FIELD.UPD_CLTR_ON_ASSIGN_TO.label)}
                className="col-6 mt-16"
                required
                options={updCltrOnAssignToOptions}
                isCombine
              />
              <div className="col-6 mt-16">
                <div className="d-flex">
                  <div className="w-40">
                    <p className="color-grey-d08 fw-500">
                      {t(AGENCY_FIELD.REMIT_NET_FEES.label)}:
                      <span className="ml-4 color-red">*</span>
                    </p>
                  </div>
                  <div className="w-60">
                    <YesNoQuestionControl
                      name={AGENCY_FIELD.REMIT_NET_FEES.name}
                    />
                  </div>
                </div>
                <p className="color-grey-l24 fs-12 mt-4">
                  {t('txt_remit_net_fees_description')}
                </p>
              </div>
              <div className="col-6 mt-16">
                <div className="d-flex">
                  <div className="w-40">
                    <p className="color-grey-d08 fw-500">
                      {t(AGENCY_FIELD.REMIT_NET_COSTS.label)}:
                      <span className="ml-4 color-red">*</span>
                    </p>
                  </div>
                  <div className="w-60">
                    <YesNoQuestionControl
                      name={AGENCY_FIELD.REMIT_NET_COSTS.name}
                    />
                  </div>
                </div>
                <p className="color-grey-l24 fs-12 mt-4">
                  {t('txt_remit_net_costs_description')}
                </p>
              </div>
              <div className="col-6 mt-16">
                <div className="d-flex">
                  <div className="w-40">
                    <p className="color-grey-d08 fw-500">
                      {t(AGENCY_FIELD.UPD_COLLECTOR.label)}:
                      <span className="ml-4 color-red">*</span>
                    </p>
                  </div>
                  <div className="w-60">
                    <YesNoQuestionControl
                      name={AGENCY_FIELD.UPD_COLLECTOR.name}
                    />
                  </div>
                </div>
                <p className="color-grey-l24 fs-12 mt-4">
                  {t('txt_upd_collector_description')}
                </p>
              </div>
            </div>
            <h5 className="fw-500 mt-24">{t('txt_electronic_interface')}</h5>
            <div className="row">
              <DropdownControl
                name={AGENCY_FIELD.ELECTRONIC_INTERFACE_TYPE.name}
                label={t(AGENCY_FIELD.ELECTRONIC_INTERFACE_TYPE.label)}
                className="col-6 mt-16"
                required
                options={electronicInterfaceTypeOptions}
                isCombine
              />
              <DropdownControl
                name={AGENCY_FIELD.ASSIGNED_LEVEL.name}
                label={t(AGENCY_FIELD.ASSIGNED_LEVEL.label)}
                className="col-6 mt-16"
                disabled={isAssignedLevelDisabled}
                options={assignedLevelOptionsFiltered}
                isCombine
                hasNone
              />
            </div>
          </FormikProvider>
        </ModalBody>
        <ModalFooter
          cancelButtonText={t('txt_cancel')}
          okButtonText={t(action)}
          onCancel={closeAgencyForm}
          onOk={formik.submitForm}
          disabledOk={!formik.isValid || (addOrEdit === 'ADD' && !formik.dirty)}
        ></ModalFooter>
      </Modal>
    </>
  );
};

export default AgencyForm;
