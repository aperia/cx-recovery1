import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { agencyFormActions } from './reducer';

// services
import agencyMaintenanceServices from 'pages/AgencyManagement/AgencyMaintenance/agencyMaintenanceServices';

// types, constants
import { AgencyFormState, AgencyFormValues } from '../types';
import { API_ERROR } from 'app/constants';
import { agencyMaintenanceActions } from 'pages/AgencyManagement/AgencyMaintenance/_redux';
import { uniqueid } from 'app/helpers';

export const addAgency = createAsyncThunk<
  unknown,
  AgencyFormValues,
  ThunkAPIConfig
>('addAgency', async (args, thunkAPI) => {
  const dispatch = thunkAPI.dispatch;
  try {
    const values = args;
    const { data } = await agencyMaintenanceServices.addAgency({
      agency: { ...values, id: uniqueid('agency_add_') }
    });

    dispatch(agencyFormActions.closeForm());
    // refresh list agency after add successfully
    dispatch(agencyMaintenanceActions.getAgency({ isRefresh: true }));

    // open detail agency after add successfully to add monitors ....
    dispatch(agencyMaintenanceActions.onToggleSetUpAgency(values));
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'success',
        message: 'txt_agency_added'
      })
    );
    return data;
  } catch (error) {
    // agency already exists
    if (error?.response?.data?.message === 'Agency already exists') {
      dispatch(
        agencyFormActions.setInlineErrMsg('txt_this_agency_code_already_exists')
      );
    } else {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_agency_failed_to_add'
        })
      );
      return thunkAPI.rejectWithValue({
        errorMessage: API_ERROR,
        response: error
      });
    }
  }
});

export const addAgencyBuilder = (
  builder: ActionReducerMapBuilder<AgencyFormState>
) => {
  builder
    .addCase(addAgency.pending, draftState => {
      draftState.loading = true;
      draftState.inlineErrMsg = '';
      draftState.error = null;
    })
    .addCase(addAgency.fulfilled, draftState => {
      draftState.loading = false;
    })
    .addCase(addAgency.rejected, (draftState, action) => {
      draftState.loading = false;
      draftState.error = action.error;
    });
};
