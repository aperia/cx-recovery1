import { createSelector } from '@reduxjs/toolkit';
import { AgencyFormValues } from '../types';

const getFormType = (state: RootState): FormType => state.agencyForm.formType;
const getInlineErrMsg = (state: RootState): string =>
  state.agencyForm.inlineErrMsg;
const getLoading = (state: RootState): boolean => state.agencyForm.loading;
const getInitialFormValues = (state: RootState): AgencyFormValues | undefined =>
  state.agencyForm.initialFormValues;
const getAgencyCodeEditing = (state: RootState): string | undefined =>
  state.agencyForm.agencyCodeEditing;

export const selectFormType = createSelector(getFormType, data => data);
export const selectFormOpened = createSelector(getFormType, data => !!data);
export const selectLoading = createSelector(getLoading, data => data);
export const selectInlineErrMsg = createSelector(getInlineErrMsg, data => data);
export const selectInitialFormValues = createSelector(
  getInitialFormValues,
  data => data
);
export const selectAgencyCodeEditing = createSelector(
  getAgencyCodeEditing,
  data => data
);
