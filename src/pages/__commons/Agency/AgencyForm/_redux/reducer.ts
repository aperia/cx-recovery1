import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// redux
import { addAgencyBuilder, addAgency } from './addAgency';
import { editAgencyBuilder, editAgency } from './editAgency';
import { getAgencyDetail, getAgencyDetailBuilder } from './getAgencyDetail';

// types
import { AgencyFormState, EditAgencyFrom } from '../types';

const initialState: AgencyFormState = {
  formType: '',
  editFrom: undefined,
  loading: false,
  inlineErrMsg: '',
  initialFormValues: undefined,
  agencyCodeEditing: undefined,
  error: null
};

export const { actions, reducer } = createSlice({
  name: 'agencyForm',
  initialState,
  reducers: {
    openForm: (
      draftState,
      action: PayloadAction<{
        formType: FormType;
        agency?: string;
        from?: EditAgencyFrom;
      }>
    ) => {
      const { formType, agency, from } = action.payload;
      draftState.formType = formType;
      draftState.editFrom = from;
      draftState.agencyCodeEditing = agency;
    },
    closeForm: draftState => {
      draftState.formType = '';
      draftState.editFrom = undefined;
      draftState.inlineErrMsg = '';
      draftState.agencyCodeEditing = '';
      draftState.initialFormValues = undefined;
    },
    setInlineErrMsg: (draftState, action: PayloadAction<string>) => {
      draftState.inlineErrMsg = action.payload;
    }
  },
  extraReducers: builder => {
    addAgencyBuilder(builder);
    getAgencyDetailBuilder(builder);
    editAgencyBuilder(builder);
  }
});

const agencyFormActions = {
  ...actions,
  addAgency,
  getAgencyDetail,
  editAgency
};

export { agencyFormActions };
