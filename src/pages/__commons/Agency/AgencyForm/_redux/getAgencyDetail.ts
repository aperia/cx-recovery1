import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// services
import agencyMaintenanceServices from 'pages/AgencyManagement/AgencyMaintenance/agencyMaintenanceServices';

// types, constants
import { AgencyFormState, AgencyFormValues } from '../types';

export const getAgencyDetail = createAsyncThunk<
  AgencyFormValues,
  { agencyCode: string },
  ThunkAPIConfig
>('getAgencyDetail', async (args, thunkAPI) => {
  const { data } = await agencyMaintenanceServices.getAgencyDetails(args);
  return data.agency;
});

export const getAgencyDetailBuilder = (
  builder: ActionReducerMapBuilder<AgencyFormState>
) => {
  builder
    .addCase(getAgencyDetail.pending, draftState => {
      draftState.loading = true;
      draftState.initialFormValues = undefined;
      draftState.error = null;
    })
    .addCase(getAgencyDetail.fulfilled, (draftState, action) => {
      draftState.loading = false;
      draftState.initialFormValues = action.payload;
    })
    .addCase(getAgencyDetail.rejected, (draftState, action) => {
      draftState.loading = false;
      draftState.initialFormValues = undefined;
      draftState.error = action.error;
    });
};
