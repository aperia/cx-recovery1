import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// redux
import { agencyMaintenanceActions } from 'pages/AgencyManagement/AgencyMaintenance/_redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { agencyFormActions } from './reducer';

// services
import agencyMaintenanceServices from 'pages/AgencyManagement/AgencyMaintenance/agencyMaintenanceServices';

// types, constants
import { AgencyFormState, AgencyFormValues } from '../types';
import { API_ERROR } from 'app/constants';
import { checkEmpty } from 'app/helpers';
import { agencyDetailsActions } from 'pages/AgencyDetails/_redux';

export const editAgency = createAsyncThunk<
  unknown,
  AgencyFormValues,
  ThunkAPIConfig
>('editAgency', async (args, thunkAPI) => {
  const { id, agencyCode } = args;
  const dispatch = thunkAPI.dispatch;
  const { agencyCodeEditing, editFrom } = thunkAPI.getState().agencyForm;

  try {
    const prevAgency = checkEmpty([agencyCodeEditing, '']) as string;
    const { data } = await agencyMaintenanceServices.editAgency({
      prevAgency,
      agency: args
    });

    dispatch(agencyFormActions.closeForm());
    switch (editFrom) {
      case 'management':
        dispatch(agencyMaintenanceActions.getAgency({ isRefresh: true }));
        break;
      case 'snapshot':
        dispatch(agencyDetailsActions.getAgencyDetails({ id, agencyCode }));
        break;
      default:
        break;
    }
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'success',
        message: 'txt_agency_updated'
      })
    );

    return data;
  } catch (error) {
    // agency already exists
    if (error?.response?.data?.message === 'Agency already exists') {
      dispatch(
        agencyFormActions.setInlineErrMsg('txt_this_agency_code_already_exists')
      );
    } else {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: 'txt_agency_failed_to_update'
        })
      );
      return thunkAPI.rejectWithValue({
        errorMessage: API_ERROR,
        response: error
      });
    }
  }
});

export const editAgencyBuilder = (
  builder: ActionReducerMapBuilder<AgencyFormState>
) => {
  builder
    .addCase(editAgency.pending, draftState => {
      draftState.loading = true;
      draftState.inlineErrMsg = '';
      draftState.error = null;
    })
    .addCase(editAgency.fulfilled, draftState => {
      draftState.loading = false;
    })
    .addCase(editAgency.rejected, (draftState, action) => {
      draftState.loading = false;
      draftState.error = action.error;
    });
};
