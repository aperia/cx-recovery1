import { useCallback, useEffect, useMemo } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import * as Yup from 'yup';

// constants
import { AGENCY_FIELD } from './constants';

// redux
import { agencyFormActions } from './_redux/reducer';
import {
  selectAgencyCodeEditing,
  selectFormOpened,
  selectFormType,
  selectInitialFormValues,
  selectInlineErrMsg,
  selectLoading
} from './_redux/selector';

// hooks
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';

// helpers, types, constants
import { PHONE_NUMBER_REGEX_MATCH } from 'app/constants';
import { checkEmpty } from 'app/helpers';
import { AgencyFormValues, EditAgencyFrom } from './types';
import { tabActions } from 'pages/__commons/TabBar/_redux';

const initialDefaultValues: AgencyFormValues = {
  id: '',
  agencyCode: '',
  agencyType: undefined,
  companyName: '',
  masterAgency: '',
  address1: '',
  address2: '',
  city: '',
  state: undefined,
  zipCode: '',
  phoneNumber: '',
  fedTaxId: '',
  contactName: '',
  exceptionFlag: 'N',
  feeBasic: undefined,
  assignTo: undefined,
  remitNetFees: '',
  remitNetCosts: '',
  updCollector: '',
  electronicInterfaceType: undefined,
  assignedLevel: undefined
};

export const validationSchema = Yup.object().shape({
  [AGENCY_FIELD.AGENCY_CODE.name]: Yup.string().required(
    'txt_agency_code_is_required'
  ),
  [AGENCY_FIELD.AGENCY_TYPE.name]: Yup.string().required(
    'txt_agency_type_is_required'
  ),
  [AGENCY_FIELD.COMPANY_NAME.name]: Yup.string().required(
    'txt_company_name_is_required'
  ),
  [AGENCY_FIELD.PHONE_NUMBER.name]: Yup.string().matches(
    PHONE_NUMBER_REGEX_MATCH,
    'txt_invalid_format'
  ),
  [AGENCY_FIELD.FEE_BASIC.name]: Yup.string().required(
    'txt_fee_basic_is_required'
  ),
  [AGENCY_FIELD.UPD_CLTR_ON_ASSIGN_TO.name]: Yup.string().required(
    'txt_upd_cltr_assign_to_is_required'
  ),
  [AGENCY_FIELD.REMIT_NET_FEES.name]: Yup.string().required(),
  [AGENCY_FIELD.REMIT_NET_COSTS.name]: Yup.string().required(),
  [AGENCY_FIELD.UPD_COLLECTOR.name]: Yup.string().required(
    'txt_upd_cltr_assign_to_is_required'
  ),
  [AGENCY_FIELD.ELECTRONIC_INTERFACE_TYPE.name]: Yup.string().required(
    'txt_electronic_interface_is_required'
  )
});

export const useAgencyForm = () => {
  const dispatch = useDispatch();
  const formOpened = useSelector(selectFormOpened);
  const addOrEdit = useSelector(selectFormType);
  const isLoading = useSelector(selectLoading);
  const inlineErrMsg = useSelector(selectInlineErrMsg);
  const initialAgencyValues = useSelector(selectInitialFormValues);
  const agencyCodeEditing = useSelector(selectAgencyCodeEditing);

  const initialValues = checkEmpty([
    initialAgencyValues,
    initialDefaultValues
  ]) as AgencyFormValues;

  const { data: agencyTypeData } = useGetRefDataQuery('agencyType');
  const { data: feeBasicData } = useGetRefDataQuery('feeBasic');
  const { data: updCltrOnAssignToData } =
    useGetRefDataQuery('updCltrOnAssignTo');
  const { data: stateData } = useGetRefDataQuery('state');
  const { data: electronicInterfaceTypeData } = useGetRefDataQuery(
    'electronicInterfaceType'
  );
  const { data: assignedLevelData } = useGetRefDataQuery('assignedLevel');

  // generate ref data options
  const agencyTypeOptions: RefDataValue[] = useMemo(
    () => checkEmpty([agencyTypeData?.agencyType as RefDataValue[], []]),
    [agencyTypeData?.agencyType]
  );
  const feeBasicOptions: RefDataValue[] = useMemo(
    () => checkEmpty([feeBasicData?.feeBasic as RefDataValue[], []]),
    [feeBasicData?.feeBasic]
  );
  const updCltrOnAssignToOptions: RefDataValue[] = useMemo(
    () =>
      checkEmpty([
        updCltrOnAssignToData?.updCltrOnAssignTo as RefDataValue[],
        []
      ]),
    [updCltrOnAssignToData?.updCltrOnAssignTo]
  );
  const stateOptions: RefDataValue[] = useMemo(
    () => checkEmpty([stateData?.state as RefDataValue[], []]),
    [stateData?.state]
  );
  const electronicInterfaceTypeOptions: RefDataValue[] = useMemo(
    () =>
      checkEmpty([
        electronicInterfaceTypeData?.electronicInterfaceType as RefDataValue[],
        []
      ]),
    [electronicInterfaceTypeData?.electronicInterfaceType]
  );
  const assignedLevelOptions: RefDataValue[] = useMemo(
    () => checkEmpty([assignedLevelData?.assignedLevel as RefDataValue[], []]),
    [assignedLevelData?.assignedLevel]
  );

  // handle open modal
  const openAgencyForm = useCallback(
    ({
      formType,
      agency,
      from
    }: {
      formType: FormType;
      agency?: string;
      from?: EditAgencyFrom;
    }) => {
      dispatch(agencyFormActions.openForm({ formType, agency, from }));
    },
    [dispatch]
  );

  // handle close modal
  const closeAgencyForm = useCallback(() => {
    dispatch(agencyFormActions.closeForm());
  }, [dispatch]);

  const onSubmit = useCallback(
    (values: AgencyFormValues) => {
      const { id, agencyCode, companyName } = values;
      if (addOrEdit === 'ADD') {
        dispatch(agencyFormActions.addAgency(values));
      } else {
        dispatch(agencyFormActions.editAgency(values));
        dispatch(
          tabActions.updateTabPops({
            title: `${values.agencyCode} - ${values.companyName}`,
            storeId: values.id,
            tabType: 'agencyDetail',
            iconName: 'rating-line',
            props: {
              id,
              agencyCode,
              companyName
            }
          })
        );
      }
    },
    [addOrEdit, dispatch]
  );

  useEffect(() => {
    if (agencyCodeEditing) {
      dispatch(
        agencyFormActions.getAgencyDetail({ agencyCode: agencyCodeEditing })
      );
    }
  }, [agencyCodeEditing, dispatch]);

  return {
    formOpened,
    inlineErrMsg,
    openAgencyForm,
    agencyTypeOptions,
    feeBasicOptions,
    updCltrOnAssignToOptions,
    stateOptions,
    electronicInterfaceTypeOptions,
    assignedLevelOptions,
    closeAgencyForm,
    onSubmit,
    isLoading,
    initialValues,
    addOrEdit
  };
};
