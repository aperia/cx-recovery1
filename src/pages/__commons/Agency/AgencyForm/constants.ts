export const AGENCY_FIELD = {
  AGENCY_CODE: { name: 'agencyCode', label: 'txt_agency_code' },
  AGENCY_TYPE: { name: 'agencyType', label: 'txt_agency_type' },
  COMPANY_NAME: { name: 'companyName', label: 'txt_company_name' },
  MASTER_AGENCY: { name: 'masterAgency', label: 'txt_master_agency' },
  ADDRESS_1: { name: 'address1', label: 'txt_address_1' },
  ADDRESS_2: { name: 'address2', label: 'txt_address_2' },
  CITY: { name: 'city', label: 'txt_city' },
  STATE: { name: 'state', label: 'txt_state' },
  ZIP_CODE: { name: 'zipCode', label: 'txt_zip_code' },
  PHONE_NUMBER: { name: 'phoneNumber', label: 'txt_phone_number' },
  FED_TAX_ID: { name: 'fedTaxId', label: 'txt_fed_tax_id' },
  CONTACT_NAME: { name: 'contactName', label: 'txt_contact_name' },
  EXCEPTION_FLAG: { name: 'exceptionFlag', label: 'txt_exception_flag' },
  FEE_BASIC: { name: 'feeBasic', label: 'txt_fee_basic' },
  UPD_CLTR_ON_ASSIGN_TO: {
    name: 'assignTo',
    label: 'txt_upd_cltr_on_assign_to'
  },
  REMIT_NET_FEES: { name: 'remitNetFees', label: 'txt_remit_net_fees' },
  REMIT_NET_COSTS: { name: 'remitNetCosts', label: 'txt_remit_net_costs' },
  UPD_COLLECTOR: { name: 'updCollector', label: 'txt_upd_collector' },
  ELECTRONIC_INTERFACE_TYPE: {
    name: 'electronicInterfaceType',
    label: 'txt_electronic_interface_type'
  },
  ASSIGNED_LEVEL: { name: 'assignedLevel', label: 'txt_assigned_level' }
};
