import { SerializedError } from '@reduxjs/toolkit';

export type EditAgencyFrom = 'snapshot' | 'management' | undefined;

export type AgencyFormState = {
  formType: FormType;
  editFrom: EditAgencyFrom;
  loading: boolean;
  error: SerializedError | null;
  inlineErrMsg: string;
  agencyCodeEditing?: string;
  initialFormValues?: AgencyFormValues;
};

export type AgencyFormValues = {
  id: string;
  agencyCode: string;
  agencyType?: string;
  companyName: string;
  masterAgency: string;
  address1: string;
  address2: string;
  city: string;
  state?: string;
  zipCode: string;
  phoneNumber?: string;
  fedTaxId: string;
  contactName: string;
  exceptionFlag: string;
  feeBasic?: string;
  assignTo?: string;
  remitNetFees: string;
  remitNetCosts: string;
  updCollector: string;
  electronicInterfaceType?: string;
  assignedLevel?: string;
};
