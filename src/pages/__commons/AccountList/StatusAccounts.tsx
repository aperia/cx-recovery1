import React from 'react';

// components
import { Button, GroupButton } from 'app/_libraries/_dls/components';

// types
import { TypeStatusAccountList } from 'pages/AccountManagement/types';

const groupButton = [
  { description: 'Active', value: 'active', classnames: 'mr-8' },
  { description: 'Purged', value: 'purged', classnames: '' }
];

export interface StatusAccountsProps {
  value: TypeStatusAccountList;
  handleChange: (value: string) => void;
}
const StatusAccounts: React.FC<StatusAccountsProps> = ({
  value,
  handleChange
}) => {
  return (
    <div className="d-flex">
      <GroupButton>
        {groupButton.map(item => {
          return (
            <Button
              key={item.value}
              selected={value === item.value}
              value={item.value}
              size="sm"
              onClick={() => handleChange(item.value)}
              variant="secondary"
            >
              {item.description}
            </Button>
          );
        })}
      </GroupButton>
    </div>
  );
};

export default StatusAccounts;
