import React from 'react';

// components
import { DropdownList } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helper
import { ordersDropdown } from 'app/constants';

export interface OrderByAccountsProps {
  value: RefDataValue;
  handleChange: (e: DropdownBaseChangeEvent) => void;
}
const OrderByAccounts: React.FC<OrderByAccountsProps> = ({
  value,
  handleChange
}) => {
  const { t } = useTranslation();

  return (
    <div className="d-flex align-items-center">
      <p className="fw-500 color-grey mr-4">{t('txt_order_by')}:</p>
      <DropdownList
        id={'DropdownSortByAccountList'}
        textField="description"
        variant={'no-border'}
        onChange={handleChange}
        value={value}
      >
        {ordersDropdown.map(item => (
          <DropdownList.Item
            key={item.value}
            label={t(item.description)}
            value={item}
          />
        ))}
      </DropdownList>
    </div>
  );
};

export default OrderByAccounts;
