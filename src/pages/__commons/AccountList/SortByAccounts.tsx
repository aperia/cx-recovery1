import React from 'react';

// components
import { DropdownList } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface SortByAccountsProps {
  value: RefDataValue;
  dropdownSortAccounts: RefDataValue[];
  handleChange: (e: DropdownBaseChangeEvent) => void;
}
const SortByAccounts: React.FC<SortByAccountsProps> = ({
  value,
  dropdownSortAccounts,
  handleChange
}) => {
  const { t } = useTranslation();

  return (
    <div className="d-flex align-items-center mr-24">
      <p className="fw-500 color-grey mr-4">{t('txt_sort_by')}:</p>
      <DropdownList
        id={'DropdownSortByAccountList'}
        textField="description"
        variant={'no-border'}
        value={value}
        onChange={handleChange}
      >
        {dropdownSortAccounts.map(item => (
          <DropdownList.Item
            key={item.value}
            label={t(item.description)}
            value={item}
          />
        ))}
      </DropdownList>
    </div>
  );
};

export default SortByAccounts;
