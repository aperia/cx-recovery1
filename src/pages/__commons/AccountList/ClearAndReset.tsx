import React from 'react';
// components
import { Button } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { I18N_TEXT } from 'app/constants';

interface ClearAndResetProps {
  onClick: () => void;
}

const ClearAndReset: React.FC<ClearAndResetProps> = ({ onClick }) => {
  const { t } = useTranslation();

  return (
    <Button
      className="ml-16"
      variant={'outline-primary'}
      size={'sm'}
      onClick={onClick}
    >
      {t(I18N_TEXT.CLEAR_AND_RESET)}
    </Button>
  );
};
export default ClearAndReset;
