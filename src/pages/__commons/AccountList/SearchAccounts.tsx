import React from 'react';

// components
import {
  AttributeSearch,
  AttributeSearchEvent,
  AttributeSearchRef,
  AttributeSearchValue
} from 'app/_libraries/_dls/components';
import { getInitialAttrData } from 'pages/AccountManagement/AccountList/AttributesSearch';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import {
  ATTRIBUTE_SEARCH_FIELD,
  FIELD_NOT_CONSTRAINT,
  NO_COMBINE_FIELDS
} from 'pages/AccountManagement/constant';
import { EMPTY_STRING, I18N_TEXT } from 'app/constants';

// helpers
import { every } from 'app/helpers';
import { attrValidations } from 'pages/AccountManagement/AccountList/searchValidation';
import { AttributeSearchFieldModel } from 'pages/AccountManagement/types';

export interface AccountListSearchProps {
  searchValues: AttributeSearchValue[];
  handleClear: () => void;
  handleChange: (event: AttributeSearchEvent) => void;
  handleSearch: (
    event: AttributeSearchEvent,
    error?: Record<string, string>
  ) => void;
  small?: boolean;
}
const AccountListSearch: React.FC<AccountListSearchProps> = ({
  searchValues,
  handleClear,
  handleChange,
  handleSearch,
  small = true
}) => {
  const { t } = useTranslation();

  const { data } = React.useMemo(() => getInitialAttrData(t), [t]);

  const attributeSearchRef = React.useRef<AttributeSearchRef | null>(null);

  const noCombineFields = React.useMemo(() => {
    const isEnableNoCombineFeature = every(searchValues, ({ key }) => {
      return FIELD_NOT_CONSTRAINT.includes(key as ATTRIBUTE_SEARCH_FIELD);
    });

    const translationFields = NO_COMBINE_FIELDS.map(r => ({
      ...r,
      filterMessage: t(r.filterMessage)
    }));

    return isEnableNoCombineFeature ? translationFields : [];
  }, [searchValues, t]);

  const renderPlaceHolder = () => {
    if (!searchValues.length) return t(I18N_TEXT.TYPE_TO_ADD_PARAMETER);

    if (FIELD_NOT_CONSTRAINT.includes(searchValues[0]?.key))
      return EMPTY_STRING;

    return t(I18N_TEXT.TYPE_TO_ADD_PARAMETER);
  };

  return (
    <AttributeSearch
      small={small}
      orderBy="asc"
      ref={attributeSearchRef}
      data={data}
      value={searchValues}
      placeholder={renderPlaceHolder()}
      noCombineFields={noCombineFields}
      validator={(data: AttributeSearchFieldModel) => attrValidations(data, t)}
      onClear={handleClear}
      onChange={handleChange}
      onSearch={handleSearch}
      clearTooltipProps={{
        element: t(I18N_TEXT.CLEAR_ALL_CRITERIA)
      }}
    />
  );
};

export default AccountListSearch;
