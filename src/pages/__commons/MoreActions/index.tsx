import React, { useMemo } from 'react';

// components
import {
  BtnSizes,
  Button,
  DropdownButton,
  DropdownButtonSelectEvent,
  Icon,
  Tooltip,
  TooltipProps
} from 'app/_libraries/_dls/components';

// helpers
import { useTranslation } from 'react-i18next';
import { isEmpty, isFunction } from 'app/helpers';

export interface ActionValue {
  label: string;
  uniqueId: string;
  disabled?: boolean;
}

export interface MoreActionProps {
  actions: ActionValue[];
  onSelect?: (event: ActionValue) => void;
  id?: string;
  dataTestId?: string;
  className?: string;
  size?: BtnSizes;
  placementTooltip?: TooltipProps['placement'];
  placementDropdown?: TooltipProps['placement'];
}

const MoreAction: React.FC<MoreActionProps> = ({
  actions,
  onSelect,
  id,
  dataTestId,
  className,
  size = 'sm',
  placementTooltip = 'top',
  placementDropdown = 'bottom'
}) => {
  const { t } = useTranslation();
  const dropdownListItem = useMemo(() => {
    return actions.map(item => (
      <DropdownButton.Item
        dataTestId={dataTestId}
        key={item.uniqueId}
        label={t(item.label)}
        value={item}
        disabled={item.disabled}
      />
    ));
  }, [actions, t, dataTestId]);

  if (isEmpty(actions)) {
    return null;
  }

  if (actions.length === 1) {
    const handleClick = () => {
      isFunction(onSelect) && onSelect(actions[0]);
    };

    return (
      <Tooltip
        triggerClassName="ml-auto mr-n4"
        placement="top"
        variant={'primary'}
        element={t(actions[0].label)}
        dataTestId={dataTestId}
      >
        <Button
          variant="icon-secondary"
          onClick={handleClick}
          dataTestId={dataTestId}
          disabled={actions[0].disabled}
          size="sm"
        >
          <Icon name={t(actions[0].uniqueId)} dataTestId={dataTestId} />
        </Button>
      </Tooltip>
    );
  }

  return (
    <Tooltip element="Actions" placement={placementTooltip} variant={'primary'}>
      <DropdownButton
        id={id}
        dataTestId={dataTestId}
        className={className}
        popupBaseProps={{
          popupBaseClassName: 'inside-infobar',
          placement: placementDropdown
        }}
        buttonProps={{
          size,
          children: <Icon name="more-vertical" />,
          variant: 'icon-secondary'
        }}
        onSelect={(event: DropdownButtonSelectEvent) => {
          isFunction(onSelect) && onSelect(event.target.value);
        }}
      >
        {dropdownListItem}
      </DropdownButton>
    </Tooltip>
  );
};

export default MoreAction;
