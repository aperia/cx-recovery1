import React, { useMemo } from 'react';
import { NoDataGrid } from 'app/components/NoDataGrid';
import { Button, Pagination } from 'app/_libraries/_dls/components';
import { MonitorCriteriaForm } from './MonitorCriteriaForm';
import MonitorCriteriaItem from 'pages/AgencyDetails/MonitorCriteriaItem';

// constants
import { I18N_TEXT } from 'app/constants';
import { IMonitorMaintenanceProps } from './types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch } from 'react-redux';
import { monitorMaintenanceActions } from './_redux';

// helpers
import classNames from 'classnames';
import { DEFAULT_PAGE_SIZE } from 'app/_libraries/_dls/components/Pagination/constants';
import { isEmpty, isUndefined } from 'app/helpers';

const MonitorMaintenance: React.FC<IMonitorMaintenanceProps> = props => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const {
    data,
    loading,
    isAdd,
    totalItem,
    currentPage,
    onPageChange,
    onSubmit
  } = props;

  const genBtnAddMonitor = useMemo(() => {
    const isDisplayPagination = totalItem > DEFAULT_PAGE_SIZE;
    const onToggle = () => {
      dispatch(monitorMaintenanceActions.onToggleFormModal());
    };

    const addButton = (isUndefined(isAdd) || isAdd === true) && (
      <Button
        className="text-capitalize"
        variant="outline-primary"
        size="sm"
        onClick={onToggle}
      >
        {t(I18N_TEXT.ADD_MONITOR_CRITERIA)}
      </Button>
    );

    if (loading) return null;

    if (data && !isEmpty(data)) {
      return (
        <div>
          <div className="d-flex justify-content-between align-items-center mr-n8">
            <h5>{t(I18N_TEXT.MONITOR_CRITERIA_LIST)}</h5>
            {addButton}
          </div>
          {data.map((item, index) => (
            <MonitorCriteriaItem key={index} {...item} />
          ))}

          {isDisplayPagination && (
            <div className="pt-16">
              <Pagination
                totalItem={totalItem}
                pageSize={[10]}
                pageNumber={currentPage}
                pageSizeValue={DEFAULT_PAGE_SIZE}
                compact
                onChangePage={onPageChange}
              />
            </div>
          )}
        </div>
      );
    }
    return (
      <div>
        <h5>{t(I18N_TEXT.MONITOR_CRITERIA_LIST)}</h5>
        <NoDataGrid>
          <div className="mt-24">{addButton}</div>
        </NoDataGrid>
      </div>
    );
  }, [totalItem, isAdd, t, loading, data, dispatch, currentPage, onPageChange]);

  return (
    <div
      className={classNames(['p-24 bg-white max-width-lg mx-auto', loading])}
    >
      {genBtnAddMonitor}
      <MonitorCriteriaForm onSubmit={onSubmit} />
    </div>
  );
};

export default MonitorMaintenance;
