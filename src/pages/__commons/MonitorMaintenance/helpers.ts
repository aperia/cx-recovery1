export const formatInterval = (value?: string, unit?: string) => {
  if (!value) return '';
  else if (+value === 1) return `${value} ${unit?.replace('s', '')}`;
  else return `${value} ${unit}`;
};

export const formatYesNoValue = (value?: boolean) => {
  return value ? 'txt_yes' : 'txt_no';
};
