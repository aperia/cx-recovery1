import React from 'react';
import {
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import Modal from 'app/_libraries/_dls/components/Modal/Modal';
import { View } from 'app/_libraries/_dof/core';

// constants
import { I18N_TEXT } from 'app/constants';
import { MONITOR_CRITERIA_FORM_VIEW } from './constants';
import { IMonitorCriteriaFormProps, ISelectedMonitor } from './types';
import { ISaveMonitorArgs } from 'pages/AgencyManagement/AgencyMaintenance/types';

//hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

//redux
import { useDispatch, useSelector } from 'react-redux';
import { selectedModalOpen, selectedEditMonitor } from './_redux/selectors';
import { monitorMaintenanceActions } from './_redux';
import { getFormValues, getFormSyncErrors } from 'redux-form';

// helpers
import { useIsDirtyForm } from 'app/hooks/useDirtyForm';
import { isEmpty } from 'app/helpers';

export const MonitorCriteriaForm: React.FC<IMonitorCriteriaFormProps> =
  props => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const { onSubmit } = props;
    const open: boolean = useSelector(selectedModalOpen);
    const value: ISelectedMonitor = useSelector(selectedEditMonitor);
    const formValues = useSelector(
      getFormValues(MONITOR_CRITERIA_FORM_VIEW)
    ) as ISaveMonitorArgs;
    const formErrors = useSelector(
      getFormSyncErrors(MONITOR_CRITERIA_FORM_VIEW)
    );
    const isDirtyForm = useIsDirtyForm([MONITOR_CRITERIA_FORM_VIEW]);

    const onToggle = () => {
      dispatch(monitorMaintenanceActions.onToggleFormModal());
    };

    const handleSubmit = () => {
      onSubmit && onSubmit(formValues);
    };

    return (
      <Modal show={open}>
        <ModalHeader border closeButton onHide={onToggle}>
          <ModalTitle>
            {t(
              value.id
                ? I18N_TEXT.EDIT_MONITOR_CRITERIA
                : I18N_TEXT.ADD_MONITOR_CRITERIA
            )}
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <View
            id={MONITOR_CRITERIA_FORM_VIEW}
            formKey={MONITOR_CRITERIA_FORM_VIEW}
            descriptor={MONITOR_CRITERIA_FORM_VIEW}
            value={value}
          />
        </ModalBody>
        <ModalFooter
          cancelButtonText={t(I18N_TEXT.CANCEL)}
          okButtonText={value.id ? t(I18N_TEXT.SAVE) : t(I18N_TEXT.SUBMIT)}
          onCancel={onToggle}
          onOk={handleSubmit}
          disabledOk={!isEmpty(formErrors) || !isDirtyForm}
        />
      </Modal>
    );
  };
