import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IMonitorMaintenanceState, ISelectedMonitor } from '../types';

const initialState: IMonitorMaintenanceState = {
  modalOpen: false,
  selected: {
    noteCodes: [],
    intervalStartUnit: {
      value: 'days',
      description: 'days'
    },
    intervalActionUnit: {
      value: 'days',
      description: 'days'
    }
  }
};

export const { actions, reducer } = createSlice({
  initialState,
  name: 'monitorMaintenance',
  reducers: {
    onToggleFormModal: (
      draftState,
      action: PayloadAction<ISelectedMonitor | undefined>
    ) => {
      draftState.modalOpen = !draftState.modalOpen;
      draftState.selected = action.payload || initialState.selected;
    }
  }
});

const combineActions = {
  ...actions
};

export { combineActions as monitorMaintenanceActions };
