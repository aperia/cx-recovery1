import { createSelector } from '@reduxjs/toolkit';
import { IMonitorMaintenanceState } from '../types';

const getMonitorMaintenanceState = (state: RootState) =>
  state.monitorMaintenance;

export const selectedModalOpen = createSelector(
  getMonitorMaintenanceState,
  (data: IMonitorMaintenanceState) => data.modalOpen
);

export const selectedEditMonitor = createSelector(
  getMonitorMaintenanceState,
  (data: IMonitorMaintenanceState) => data.selected
);
