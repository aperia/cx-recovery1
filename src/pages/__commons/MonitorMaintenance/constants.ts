export enum INTERVAL_UNIT {
  DAYS = 'days',
  WEEKS = 'weeks',
  MONTHS = 'months'
}
export const MONITOR_CRITERIA_FORM_VIEW = 'monitorCriteria';
