import { ISaveMonitorArgs } from 'pages/AgencyManagement/AgencyMaintenance/types';

export interface IMonitorMaintenanceState {
  modalOpen: boolean;
  selected: ISelectedMonitor;
  value?: IMonitorCriteria;
}

export interface IMonitorCriteria {
  id?: string;
  calendarCode: string;
  noteCodes: string[];
  intervalStart?: string;
  intervalStartUnit: string;
  intervalAction?: string;
  intervalActionUnit: string;
  calendarCodePost: string;
  paymentDeleteCalendarCode: string;
}

export interface IMonitorMaintenanceProps {
  data: IMonitorCriteria[];
  totalItem: number;
  loading?: boolean;
  isAdd?: boolean;
  currentPage?: number;
  onPageChange?(page: number): void;
  onSubmit?(value: ISaveMonitorArgs): void;
}

export interface IMonitorCriteriaFormProps {
  onSubmit?(value: ISaveMonitorArgs): void;
}

export interface ISelectedMonitor {
  id?: string;
  calendarCode?: RefDataValue;
  noteCodes?: RefDataValue[];
  intervalStart?: string;
  intervalStartUnit?: RefDataValue;
  intervalAction?: string;
  intervalActionUnit?: RefDataValue;
  calendarCodePost?: string;
  paymentDeleteCalendarCode?: string;
}
