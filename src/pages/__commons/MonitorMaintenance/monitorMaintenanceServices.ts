import { apiService } from 'app/utils/api.service';
import { IMonitorCriteria } from './types';

const monitorMaintenanceServices = {
  getMonitors() {
    const url: string = window.appConfig.api.monitor.getMonitors;

    return apiService.post<IMonitorCriteria[]>(url);
  }
};

export default monitorMaintenanceServices;
