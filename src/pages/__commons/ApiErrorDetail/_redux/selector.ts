import { createSelector } from '@reduxjs/toolkit';
import { checkEmpty } from 'app/helpers';

const getShowModal = (state: RootState, ids: string[]) => {
  // check if any id need to show -> show
  return ids.map(id => state.apiErrorDetail[id]?.show).some(show => show);
};

const getErrValue = (state: RootState, ids: string[]) => {
  // find the first error
  const value = ids.map(id => state.apiErrorDetail[id]?.value).find(val => val);
  return checkEmpty([value, '']);
};

export const selectShowErrModal = createSelector(getShowModal, data => data);
export const selectErrorValue = createSelector(getErrValue, data => data);
