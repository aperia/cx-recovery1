import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { ApiErrorDetailState } from '../types';

const initialState: ApiErrorDetailState = {};

export const { actions, reducer } = createSlice({
  name: 'apiErrorDetail',
  initialState,
  reducers: {
    openErrorDetail: (draftState, action: PayloadAction<{ ids: string[] }>) => {
      const { ids } = action.payload;
      ids.forEach(id => {
        draftState[id].show = true;
      });
    },
    closeErrorDetail: (
      draftState,
      action: PayloadAction<{ ids: string[] }>
    ) => {
      const { ids } = action.payload;
      ids.forEach(id => {
        draftState[id].show = false;
      });
    },
    registerId: (draftState, action: PayloadAction<{ ids: string[] }>) => {
      const { ids } = action.payload;
      ids.forEach(id => {
        draftState[id] = { show: false, value: '' };
      });
    },
    removeId: (draftState, action: PayloadAction<{ ids: string[] }>) => {
      const { ids } = action.payload;
      ids.forEach(id => {
        delete draftState[id];
      });
    },
    setErrValue: (
      draftState,
      action: PayloadAction<{ id: string; value: string }>
    ) => {
      const { id, value } = action.payload;
      if (draftState[id]) {
        draftState[id].value = value;
      }
    }
  },
  extraReducers: builder => {}
});

const apiErrorDetailActions = { ...actions };

export { apiErrorDetailActions };
