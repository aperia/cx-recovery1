import React from 'react';
import classnames from 'classnames';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface ApiErrorProp {
  onClickViewErrorDetail: () => void;
  className?: string;
}

const ApiError = React.forwardRef<HTMLDivElement, ApiErrorProp>(
  ({ onClickViewErrorDetail, className }, ref) => {
    const { t } = useTranslation();

    const testId = 'api-errors';

    return (
      <div className={classnames('api-error-detail', className)} ref={ref}>
        <div className="d-inline-block alert alert-danger mb-0">
          <span>{t('txt_api_error')}</span>
          <a
            onClick={onClickViewErrorDetail}
            className="link text-decoration-none ml-4"
            data-testid={testId}
          >
            {t('txt_view_detail')}
          </a>
        </div>
      </div>
    );
  }
);

export default ApiError;
