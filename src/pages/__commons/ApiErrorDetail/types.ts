export type ErrorDetail = {
  show: boolean;
  value: string;
};

export type ApiErrorDetailState = Record<string, ErrorDetail>;
