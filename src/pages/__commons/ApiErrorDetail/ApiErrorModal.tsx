import {
  Button,
  Icon,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  TextArea,
  Tooltip
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import React, { useMemo, useRef, useState } from 'react';

type ApiErrorModalProps = {
  show: boolean;
  onClose: () => void;
  errorValue: string | undefined;
};

export const ApiErrorModal: React.FC<ApiErrorModalProps> = ({
  show,
  onClose,
  errorValue
}) => {
  const testId = 'modal-error-detail';
  const { t } = useTranslation();
  const [tooltipValue, setTooltipValue] = useState('txt_copy');
  const errorTextBoxRef = useRef<HTMLTextAreaElement | null>(null);
  const handleCopyAction = () => {
    setTooltipValue('txt_copied');
    navigator.clipboard.writeText(errorTextBoxRef.current?.value || '');
  };

  const handleBlur = () => {
    setTooltipValue('txt_copy');
  };

  const copyButton = useMemo(() => {
    return (
      <Tooltip
        placement="top"
        variant="primary"
        element={t(tooltipValue)}
        displayOnClick={true}
      >
        <Button
          variant="icon-secondary"
          onClick={handleCopyAction}
          onMouseOver={handleBlur}
          aria-pressed="true"
          className="mr-n4"
          dataTestId={`${testId}_copyButton`}
        >
          <Icon name="product-info" />
        </Button>
      </Tooltip>
    );
  }, [t, tooltipValue]);

  return (
    <Modal show={show} sm>
      <ModalHeader
        border
        closeButton
        onHide={onClose}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_title`}>
          {t('txt_api_error_detail')}
        </ModalTitle>
      </ModalHeader>
      <ModalBody dataTestId={`${testId}_body`}>
        <div className="d-flex justify-content-between mb-16">
          <h5 data-testid={`${testId}-payload_title`}>
            {t('txt_api_error_detail_response_request_payload')}
          </h5>
          {copyButton}
        </div>
        <TextArea
          id={`${testId}-api-error-modal`}
          value={errorValue}
          className="resize-none h-300px"
          ref={errorTextBoxRef}
          dataTestId={`${testId}_errorValue`}
        />
      </ModalBody>
      <ModalFooter
        cancelButtonText={t('txt_close')}
        onCancel={onClose}
        dataTestId={`${testId}_footer`}
      />
    </Modal>
  );
};
