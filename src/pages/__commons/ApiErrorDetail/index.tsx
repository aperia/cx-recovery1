import React, { useEffect, useMemo } from 'react';
import isEqual from 'react-fast-compare';
import { useDispatch, useSelector } from 'react-redux';
import ApiError from './ApiError';
import { ApiErrorModal } from './ApiErrorModal';
import { apiErrorDetailActions } from './_redux/reducer';
import { selectErrorValue, selectShowErrModal } from './_redux/selector';

type ApiErrorDetailProps = {
  forAction: string[];
  storeId?: string;
  className?: string;
};

const ApiErrorDetail: React.FC<ApiErrorDetailProps> = ({
  forAction,
  storeId,
  className
}) => {
  const dispatch = useDispatch();
  const ids = useMemo(
    () =>
      forAction.map(actionName =>
        storeId ? `${storeId}-${actionName}` : actionName
      ),
    [forAction, storeId]
  );
  const showModal = useSelector((state: RootState) =>
    selectShowErrModal(state, ids)
  );
  const errValue = useSelector((state: RootState) =>
    selectErrorValue(state, ids)
  );

  const handleOpenDetail = () => {
    dispatch(apiErrorDetailActions.openErrorDetail({ ids }));
  };

  const handleCloseDetail = () => {
    dispatch(apiErrorDetailActions.closeErrorDetail({ ids }));
  };

  useEffect(() => {
    dispatch(apiErrorDetailActions.registerId({ ids }));

    return () => {
      dispatch(apiErrorDetailActions.removeId({ ids }));
    };
  }, [ids, dispatch]);

  return (
    <>
      {errValue && (
        <ApiError
          className={className}
          onClickViewErrorDetail={handleOpenDetail}
        />
      )}
      <ApiErrorModal
        show={showModal}
        errorValue={errValue}
        onClose={handleCloseDetail}
      />
    </>
  );
};

export default React.memo(ApiErrorDetail, isEqual);
