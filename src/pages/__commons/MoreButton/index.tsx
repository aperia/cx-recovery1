import React, { useMemo } from 'react';

// components
import {
  BtnSizes,
  DropdownButton,
  DropdownButtonSelectEvent,
  Icon,
  TooltipProps
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { isFunction } from 'app/helpers';

interface ButtonAttrs {
  label: string;
  value: string;
  event: any;
}
interface MoreButtonProps {
  listBtn: ButtonAttrs[];
  placementDropdown?: TooltipProps['placement'];
  className?: string;
  IconName?: 'more' | 'more-vertical';
  size?: BtnSizes;
}

const MoreButton: React.FC<MoreButtonProps> = ({
  listBtn,
  className,
  placementDropdown = 'bottom-end',
  IconName = 'more-vertical',
  size
}) => {
  const { t } = useTranslation();

  const dropdownListItem = useMemo(() => {
    return listBtn?.map((item, index) => (
      <DropdownButton.Item
        key={index}
        label={t(item.label)}
        value={t(item.value)}
      />
    ));
  }, [listBtn, t]);

  const handleSelect = (event: DropdownButtonSelectEvent) => {
    const { value } = event;
    const elementEvent = listBtn?.find(item => t(item.value) === value);
    isFunction(elementEvent?.event) && elementEvent?.event();
  };

  return (
    <DropdownButton
      className={className}
      popupBaseProps={{
        popupBaseClassName: 'inside-infobar',
        placement: placementDropdown
      }}
      buttonProps={{
        size,
        children: <Icon name={IconName} />,
        variant: 'icon-secondary'
      }}
      onSelect={handleSelect}
    >
      {dropdownListItem}
    </DropdownButton>
  );
};

export default MoreButton;
