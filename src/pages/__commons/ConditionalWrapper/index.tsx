import React, { ReactNode } from 'react';

export interface ConditionalWrapperProps {
  condition: boolean;
  wrapper: (children: ReactNode) => ReactNode;
  children: ReactNode;
}
const ConditionalWrapper: React.FC<ConditionalWrapperProps> = ({
  condition,
  wrapper,
  children
}) => {
  if (condition) {
    return <>{wrapper(children)}</>;
  }

  return <>{children}</>;
};

export default ConditionalWrapper;
