import React from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface FallbackComponentProps {
  dataTestId?: string;
}

const FallbackErrorComponent: React.FC<FallbackComponentProps> = ({
  dataTestId
}) => {
  const { t } = useTranslation();
  return (
    <div className="d-flex justify-content-center align-items-center vw-100 vh-100">
      <div className="text-center">
        <h2 className="mt-16">{t('txt_some_thing_went_wrong')}</h2>

        <Button
          id="tryAgainBtn"
          className="text-grey fs-14 mt-16"
          variant="primary"
          onClick={() => window.location.reload()}
          dataTestId={dataTestId}
        >
          {t('txt_try_again')}
        </Button>
      </div>
    </div>
  );
};

export default FallbackErrorComponent;
