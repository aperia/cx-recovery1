export interface MappingState {
  loading: boolean;
  data: MappingStateData;
}

export interface MappingStateData {
  i18next: Record<string, string>;
}
