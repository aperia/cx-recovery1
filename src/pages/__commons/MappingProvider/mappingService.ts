import axios from 'axios';

const mappingService = {
  async getMapping() {
    const url = window.appConfig.mappingUrl;
    return await axios.get(url);
  }
};

export default mappingService;
