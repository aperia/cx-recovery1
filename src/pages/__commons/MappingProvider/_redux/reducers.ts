import { createSlice } from '@reduxjs/toolkit';
import { MappingState } from '../types';
import { getMapping, getMappingBuilder } from './getMapping';

const { actions, reducer } = createSlice({
  name: 'mapping',
  initialState: {} as MappingState,
  reducers: {},
  extraReducers: builder => {
    getMappingBuilder(builder);
  }
});

const extraActions = {
  ...actions,
  getMapping
};

export * from '../types';
export { extraActions as actionsMapping, reducer };
