import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { MappingState } from '../types';
import mappingService from '../mappingService';
/**
 * - getMapping thunk
 */
export const getMapping = createAsyncThunk<any, undefined, ThunkAPIConfig>(
  'mapping/getMapping',
  async () => {
    try {
      const { data: mapping } = await mappingService.getMapping();
      return mapping;
    } catch (error) {}
  }
);

/**
 * - getMapping extra reducers builder
 */
export const getMappingBuilder = (
  builder: ActionReducerMapBuilder<MappingState>
) => {
  builder.addCase(getMapping.pending, draftState => {
    draftState.loading = true;
  });
  builder.addCase(getMapping.fulfilled, (draftState, action) => {
    draftState.data = action.payload;
    draftState.loading = false;
  });
  builder.addCase(getMapping.rejected, draftState => {
    draftState.loading = true;
  });
};
