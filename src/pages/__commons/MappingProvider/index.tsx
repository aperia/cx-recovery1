import React, { ReactElement, useEffect } from 'react';

import {
  actionsMapping,
  MappingStateData
} from 'pages/__commons/MappingProvider/_redux/reducers';
import { useDispatch, useSelector } from 'react-redux';
import { isEmpty } from 'app/helpers';

export interface MappingProviderProps {}

const MappingProvider: React.FC<MappingProviderProps> = ({ children }) => {
  const dispatch = useDispatch();
  const mapping = useSelector<RootState, MappingStateData>(
    state => state.mapping.data
  );

  useEffect(() => {
    dispatch(actionsMapping.getMapping());
  }, [dispatch]);

  if (isEmpty(mapping)) return null;
  return children as ReactElement;
};

export default MappingProvider;
