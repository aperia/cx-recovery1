import React from 'react';

// components
import { Icon } from 'app/_libraries/_dls/components';

// utils
import { genAmtId } from 'app/_libraries/_dls/utils';
import { isFunction } from 'app/helpers';

export interface TabTitleProps {
  storeId: string;
  title?: string;
  isHideCloseIcon?: boolean;
  onRemoveTab?: (storeId: string) => void;
  dataTestId?: string;
}

const TabTitle: React.FC<TabTitleProps> = ({
  storeId,
  title,
  isHideCloseIcon,
  onRemoveTab,
  dataTestId,
  ...props
}) => {
  const handleOnClick = (event: React.MouseEvent<HTMLDivElement>) => {
    event.stopPropagation();

    if (!storeId) return;
    isFunction(onRemoveTab) && onRemoveTab(storeId);
  };

  return (
    <>
      <span
        className="tab-title"
        id={storeId}
        data-testid={genAmtId(dataTestId!, 'tab-title', 'TabTitle')}
      >
        {title}
      </span>
      {!isHideCloseIcon && (
        <Icon name="close" onClick={handleOnClick} dataTestId={dataTestId} />
      )}
    </>
  );
};

export default TabTitle;
