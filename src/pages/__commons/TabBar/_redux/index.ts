import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// Types
import { SelectedTabPayload, TabState, ItemTab } from '../types';

const defaultTab = {
  tabs: [
    {
      storeId: 'home',
      title: 'txt_home',
      iconName: 'home',
      tabType: 'home',
      props: {},
      className: 'page-home'
    }
  ],
  tabStoreIdSelected: 'home'
};

const initialTabState = defaultTab;

export const { actions, reducer } = createSlice({
  name: 'tab',
  initialState: initialTabState as TabState,
  reducers: {
    // add new tab and active it, or active it if exists
    addTab: (draftState, action: PayloadAction<ItemTab>) => {
      const { storeId } = action.payload;

      // if tab is unique, check is it exists ?
      const tabActive = draftState.tabs.find(tab => tab.storeId === storeId);

      // always active tab
      draftState.tabStoreIdSelected = !tabActive ? storeId : tabActive?.storeId;

      // if not exists
      if (!tabActive) draftState.tabs.push(action.payload);
      // tab exists and unique => update info
      if (tabActive) {
        const indexOf = draftState.tabs.indexOf(tabActive);
        draftState.tabs[indexOf] = action.payload;
      }
    },
    // remove tab, if tab removed is tab selected => turn on logic select tab
    removeTab: (
      draftState,
      action: PayloadAction<Pick<ItemTab, 'storeId'>>
    ) => {
      const { storeId } = action.payload;
      const removeIndex = draftState.tabs.findIndex(
        tab => tab.storeId === storeId
      );
      const selectedIndex = draftState.tabs.findIndex(
        tab => tab.storeId === draftState.tabStoreIdSelected
      );

      // not found tab
      if (removeIndex === -1) return;

      draftState.tabs.splice(removeIndex, 1);

      const tabsLength = draftState.tabs.length;
      let newSelectedTab = selectedIndex;

      if (removeIndex < selectedIndex) newSelectedTab = selectedIndex - 1;

      if (newSelectedTab >= tabsLength - 1) newSelectedTab = tabsLength - 1;

      draftState.tabStoreIdSelected = draftState.tabs[newSelectedTab].storeId;
    },
    // select tab
    selectTab: (draftState, action: PayloadAction<SelectedTabPayload>) => {
      const { storeId } = action.payload;
      return {
        ...draftState,
        tabStoreIdSelected: storeId
      };
    },
    // update tab
    updateTabPops: (draftState, action: PayloadAction<Partial<ItemTab>>) => {
      const { storeId, ...newProps } = action.payload;
      const tabIndex = draftState.tabs.findIndex(
        item => item.storeId === storeId
      );
      const tab = draftState.tabs[tabIndex];
      if (!tab) return;
      if (action.payload?.newStoreId) {
        draftState.tabs[tabIndex] = Object.assign(tab, {
          ...newProps,
          storeId: action.payload.newStoreId
        });
        draftState.tabStoreIdSelected = action.payload.newStoreId;
      } else {
        draftState.tabs[tabIndex] = Object.assign(tab, newProps);
      }
    }
  }
});

export { actions as tabActions };
