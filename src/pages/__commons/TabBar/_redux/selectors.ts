import { createSelector } from '@reduxjs/toolkit';

export const takeTabSelected = createSelector(
  (state: RootState) => state.tab.tabStoreIdSelected,
  data => data
);

export const takeTabData = createSelector(
  (state: RootState) => state.tab,
  data => data
);
