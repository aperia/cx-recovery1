export interface ItemTab {
  tabType: TabTypeComponent;
  storeId: string;
  newStoreId?:string;
  title: string;
  iconName?:
    | 'account-details'
    | 'home'
    | 'file'
    | 'search-result'
    | 'action-list'
    | 'cross'
    | 'file'
    | 'rating-line';
  isNotUnique?: boolean;
  props?: MagicKeyValue;
  className?: string;
}

export interface TabState {
  tabs: ItemTab[];
  tabStoreIdSelected: string;
  accEValueSelected?: string;
}

export interface SelectedTabPayload {
  storeId: string;
}

export type TabTypeComponent =
  | 'home'
  | 'agencyManagement'
  | 'agencyDetail'
  | 'accountManagement'
  | 'accountDetail'
  | 'accountGroup'
  | 'queueList'
  | 'queueDetail'
  | 'batchManagement'
  | 'searchResult';
