// TabComponent
import AgencyManagement from 'pages/AgencyManagement';
import AgencyDetail from 'pages/AgencyDetails';
import Home from 'pages/Home';
import AccountManagement from 'pages/AccountManagement';
import AccountDetail from 'pages/AccountDetail';
import AccountGroup from 'pages/AccountGroup';
import QueueList from 'pages/AccountActivity/QueueList';
import QueueDetail from 'pages/QueueDetail';
import BatchManagement from 'pages/BatchManagement';
import SearchResult from 'pages/AccountManagement/SearchResult';

import { TabTypeComponent } from './types';

export const tabComponents: Record<TabTypeComponent, any> = {
  home: Home,
  agencyManagement: AgencyManagement,
  agencyDetail: AgencyDetail,
  accountManagement: AccountManagement,
  accountDetail: AccountDetail,
  accountGroup: AccountGroup,
  queueList: QueueList,
  queueDetail: QueueDetail,
  batchManagement: BatchManagement,
  searchResult: SearchResult
};
