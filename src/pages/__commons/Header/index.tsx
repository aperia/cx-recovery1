import React from 'react';

// component
import OpenTabAgency from 'pages/AgencyManagement/AgencyMaintenance/OpenTabAgency';
import OpenAccountManagement from 'pages/AccountManagement/OpenAccountManagement';
import OpenBatchManagement from 'pages/BatchManagement/OpenBatchManagement';

// helper
import { genAmtId } from 'app/_libraries/_dls/utils';


interface HeaderProps {}

const Header: React.FC<HeaderProps> = () => {
  return (
    <header className="header" id="header-component">
      <a
        className="cursor-pointer"
        data-testid={genAmtId(`$header`, 'logo', 'Header')}
      >
        <img src="images/remedy.svg" alt="Collections" />
      </a>
      <div>
        <OpenBatchManagement />
        <OpenAccountManagement />
        <OpenTabAgency />
      </div>
    </header>
  );
};

export default Header;
