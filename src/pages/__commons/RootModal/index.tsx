import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// components
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// hooks
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';

// types
import { BodyType, RootModalProps } from './types';
import { selectModalOptions } from 'pages/__commons/RootModal/_redux/selectors';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

const RootModal: React.FC<RootModalProps> = () => {
  const testId = 'rootModal';
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const options = useSelector(selectModalOptions);

  const {
    isOpen = false,
    isLoading = false,
    onConfirm,
    onCancel,
    autoClose = true,
    variant = 'danger',
    disableCancel = false,
    size = 'xs',
    className = ''
  } = options;

  const [show, setShow] = useState<boolean>(isOpen);

  useEffect(() => {
    setShow(isOpen);
  }, [isOpen]);

  const handleClose = useCallback(() => {
    dispatch(rootModalActions.close());
  }, [dispatch]);

  /**
   *  If you want to show another modal content once onConfirm/onCancel finished
   *  Please return your new ModalService.show(options) in that callback
   *
   *  This function will check if onConfirm/onCancel return a new ModalService.show(options) or not
   *  If yes, then autoClose flag for the current modal content will be ignored
   *  Otherwise, auto close modal will be triggered as default
   * @param {boolean} callbackResult
   * @returns {boolean}
   */
  const hasNewModalContent = (callbackResult: boolean): boolean => {
    return callbackResult;
  };

  const handleConfirm = useCallback(async () => {
    if (onConfirm) {
      if (hasNewModalContent(await onConfirm())) return;
    }

    autoClose && handleClose();
  }, [autoClose, handleClose, onConfirm]);

  const handleCancel = useCallback(async () => {
    if (onCancel) {
      if (hasNewModalContent(await onCancel())) return;
    }

    autoClose && handleClose();
  }, [autoClose, handleClose, onCancel]);

  const renders = (content?: BodyType) => {
    if (!content) return null;

    switch (typeof content) {
      case 'string':
        return t(content);
      case 'function':
        return content();
      default:
        return content;
    }
  };

  return (
    <Modal
      xs={size === 'xs'}
      md={size === 'md'}
      sm={size === 'sm'}
      lg={size === 'lg'}
      show={show}
      loading={isLoading}
      dataTestId={testId}
      className={className}
    >
      <ModalHeader
        border
        closeButton
        onHide={handleClose}
        dataTestId={`${testId}_header`}
      >
        <ModalTitle dataTestId={`${testId}_title`}>
          {renders(options?.title) || 'Modal title'}
        </ModalTitle>
      </ModalHeader>
      <ModalBody dataTestId={`${testId}_body`}>
        {renders(options?.body) || 'Modal body'}
      </ModalBody>
      <ModalFooter dataTestId={`${testId}_footer`}>
        {disableCancel ? null : (
          <Button
            variant="secondary"
            onClick={handleCancel}
            dataTestId={`${testId}_btnCancel`}
          >
            {renders(options?.btnCancelText)}
          </Button>
        )}

        <Button
          variant={variant}
          onClick={handleConfirm}
          dataTestId={`${testId}_btnConfirm`}
        >
          {renders(options?.btnConfirmText)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default RootModal;
