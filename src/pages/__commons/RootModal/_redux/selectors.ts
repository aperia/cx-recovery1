import { createSelector } from '@reduxjs/toolkit';
import { RootModalOptions } from 'pages/__commons/RootModal/types';

const getRootModalData = (state: RootState): Partial<RootModalOptions> => {
  return state.rootModal;
};

export const selectModalOptions = createSelector(
  getRootModalData,
  (rootModalOptions: RootModalOptions): RootModalOptions => rootModalOptions
);

export const selectIsLoading = createSelector(
  getRootModalData,
  (rootModalOptions: RootModalOptions): boolean | undefined => {
    return rootModalOptions?.isLoading;
  }
);
