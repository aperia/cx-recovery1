import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { RootModalOptions } from 'pages/__commons/RootModal/types';

export const initialState: Partial<RootModalOptions> = {
  title: '',
  body: '',
  btnCancelText: 'Cancel',
  btnConfirmText: 'Submit',
  
} as RootModalOptions;

export const { actions, reducer } = createSlice({
  name: 'rootModal',
  initialState,
  reducers: {
    open: (_, action: PayloadAction<RootModalOptions>) => {
      return { ...initialState, ...action.payload, isOpen: true };
    },
    loading: (draftState, action: PayloadAction<boolean>) => {
      return {
        ...draftState,
        isLoading: action.payload
      };
    },
    close: draftState => {
      return {
        ...draftState,
        isOpen: false
      };
    }
  }
});

export { actions as rootModalActions };
