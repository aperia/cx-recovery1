import { ReactElement } from 'react';

export type BodyType = string | ReactElement | (() => ReactElement);

export interface RootModalProps {}

export interface RootModalOptions {
  title?: string;
  body?: BodyType;
  btnCancelText?: string;
  btnConfirmText?: string;
  size?: 'xs' | 'sm' | 'md' | 'lg';
  isOpen?: boolean;
  isLoading?: boolean;
  onConfirm?: Function;
  onCancel?: Function;

  autoClose?: boolean;
  variant?: 'primary' | 'secondary' | 'danger';
  disableCancel?: boolean;

  className?: string;
}
