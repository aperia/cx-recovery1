import { I18N_TEXT } from 'app/constants';
import noteCodesJson from './noteCodes.json';

// types
import {
  IDeleteNoteCodeArgs,
  INoteCode,
  IPayloadGetNoteCode
} from 'pages/__commons/NoteCode/types';
import { IGetNoteCodeArgs } from './../types';
import { ISaveNoteCode } from '../_redux/saveNoteCode';

// helpers
import { orderBy } from 'app/helpers';

let noteCodeData = [] as INoteCode[];

try {
  noteCodeData = JSON.parse(JSON.stringify(noteCodesJson))
    .noteCodes as INoteCode[];
} catch (error) {}

const TIME_DELAY = 600;
const isHaveError = window.location.href.includes('error');
const isHaveEmpty = window.location.href.includes('empty');

export const noteCodeController = {
  getNoteCodes: ({ page, groupId }: IGetNoteCodeArgs) => {
    const ITEMS_PER_PAGE = 10;
    const totalItem = noteCodeData.length;

    const startIndex = (page - 1) * ITEMS_PER_PAGE;
    const endIndex = startIndex + (ITEMS_PER_PAGE - 1);

    const currentPage = Math.ceil((startIndex - 1) / ITEMS_PER_PAGE + 1);

    noteCodeData = orderBy(noteCodeData, 'createdDate', 'desc');
    noteCodeData = noteCodeData.filter(
      note => note?.groupId === undefined || note?.groupId === groupId
    );

    return new Promise<IPayloadGetNoteCode>(function (resolve, reject) {
      setTimeout(() => {
        resolve({
          success: true,
          data: isHaveEmpty ? [] : noteCodeData.slice(startIndex, endIndex + 1),
          totalItem,
          currentPage
        });
      }, TIME_DELAY);
    });
  },
  saveNoteCode: (saveNoteCode: ISaveNoteCode) => {
    try {
      const { id, value, description, groupId } = saveNoteCode;

      if (id) {
        if (isHaveError) {
          return Promise.reject({
            data: {
              success: false,
              message: I18N_TEXT.EDIT_NOTE_CODE_FAIL
            }
          });
        }

        // handle edit note
        const noteIndex = noteCodeData.findIndex(item => item.id === id);
        if (noteIndex === -1) {
          throw new Error(I18N_TEXT.EDIT_NOTE_CODE_FAIL);
        }

        const updateNote = {
          ...noteCodeData[noteIndex],
          value,
          description,
          groupId
        };

        noteCodeData = [
          ...noteCodeData.slice(0, noteIndex),
          updateNote,
          ...noteCodeData.slice(noteIndex + 1)
        ];
      } else {
        if (isHaveError) {
          return Promise.reject({
            data: { success: false, message: I18N_TEXT.ADD_NOTE_CODE_FAIL }
          });
        }
        // handle add note
        let lastId = '0';

        if (noteCodeData.length) {
          const sortedCopiedNoteData = [...noteCodeData].sort((a, b) => {
            if (!a?.id || !b?.id) return 0;

            return +b.id - +a.id;
          });

          lastId = sortedCopiedNoteData[0]?.id || '0';
        }

        const newNote = {
          id: `${+lastId + 1}`,
          value,
          description,
          groupId,
          show: true,
          createdDate: new Date().toUTCString()
        } as INoteCode;

        noteCodeData = noteCodeData.concat([newNote]);
      }

      return Promise.resolve({
        success: true,
        data: noteCodeData
      });
    } catch (error) {
      return Promise.reject({
        response: {
          data: {
            success: false,
            message: error
          }
        }
      });
    }
  },
  deleteNoteCode: ({ id }: IDeleteNoteCodeArgs) => {
    noteCodeData = noteCodeData.filter((item: INoteCode) => item.id !== id);
    return Promise.resolve({
      success: true,
      data: noteCodeData
    });
  }
};
