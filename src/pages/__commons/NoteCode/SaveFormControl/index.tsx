import React from 'react';

// components
import AddForm from 'pages/__commons/NoteCode/AddForm';
import EditForm from 'pages/__commons/NoteCode/EditForm';

// hooks
import useSaveNoteCodeForm from '../hooks/useSaveNoteCodeForm';

// helpers
import { FormikProvider } from 'formik';

export interface SaveFormControlProps {
  editMode: boolean;
  onSubmit: Function;
  onCancel: (editMode: boolean) => void;
}

const SaveFormControl: React.FC<SaveFormControlProps> = ({
  editMode,
  onSubmit,
  onCancel
}) => {
  const { formik, noteCodes } = useSaveNoteCodeForm({
    editMode,
    onSubmit
  });

  return (
    <FormikProvider value={formik}>
      {editMode ? (
        <EditForm
          editMode={editMode}
          noteCodes={noteCodes}
          onCancel={onCancel}
        />
      ) : (
        <AddForm
          editMode={editMode}
          noteCodes={noteCodes}
          onCancel={onCancel}
        />
      )}
    </FormikProvider>
  );
};

export default SaveFormControl;
