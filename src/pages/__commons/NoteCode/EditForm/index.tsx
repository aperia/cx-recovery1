import React, { useCallback } from 'react';

// components
import NoteCodeForm from 'pages/__commons/NoteCode/NoteCodeForm';
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// hooks
import { useSelectorId } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// selectors
import {
  selectEditFormIsLoading,
  selectEditFormIsOpen
} from '../_redux/selectors';

// types
import { CommonNoteCodeFormProps } from '../types';
import { useFormikContext } from 'formik';
import { I18N_TEXT } from 'app/constants';

export interface EditNoteCodeFormProps extends CommonNoteCodeFormProps {}

const EditForm: React.FC<EditNoteCodeFormProps> = ({
  editMode,
  noteCodes,
  onCancel
}) => {
  const { t } = useTranslation();
  const { submitForm, resetForm, isSubmitting, setSubmitting, isValid } =
    useFormikContext();

  const isOpen = useSelectorId<boolean>(selectEditFormIsOpen);
  const isLoading = useSelectorId<boolean>(selectEditFormIsLoading);

  const handleCancel = useCallback(() => {
    resetForm();
    setSubmitting(false);

    onCancel(true);
  }, [onCancel, resetForm, setSubmitting]);

  const handleSubmit = useCallback(() => {
    setSubmitting(true);
    submitForm();
  }, [setSubmitting, submitForm]);

  return (
    <>
      <Modal xs show={isOpen} loading={isLoading}>
        <ModalHeader border closeButton onHide={handleCancel}>
          <ModalTitle>{t(I18N_TEXT.EDIT_NOTE_CODE)}</ModalTitle>
        </ModalHeader>

        <ModalBody>
          <NoteCodeForm editMode={editMode} noteCodes={noteCodes} />
        </ModalBody>
        <ModalFooter
          cancelButtonText={t(I18N_TEXT.CANCEL)}
          okButtonText={t(I18N_TEXT.SAVE)}
          onCancel={handleCancel}
          onOk={handleSubmit}
          disabledOk={isSubmitting || !isValid}
        ></ModalFooter>
      </Modal>
    </>
  );
};

export default EditForm;
