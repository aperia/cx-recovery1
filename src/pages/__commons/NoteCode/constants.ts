import { I18N_TEXT } from 'app/constants';

export const NOTE_CODE_FIELD = {
  NOTE_CODE_CODE: { name: 'noteCode', label: 'txt_note_code_code' },
  NOTE_CODE_DESCRIPTION: {
    name: 'description',
    label: 'txt_note_code_description',
    limit: 700
  },
  NOTE_CODE_ALL_ACCOUNTS_CHECKBOX: {
    name: 'allAccountsCheckbox',
    label: I18N_TEXT.NOTE_CODE_ALL_ACCOUNTS_CHECKBOX
  }
};
