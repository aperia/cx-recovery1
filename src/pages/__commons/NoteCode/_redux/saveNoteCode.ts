import { batch } from 'react-redux';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// services
import noteCodeServices from '../noteCodeServices';

// constants
import { API_ERROR, I18N_TEXT } from 'app/constants';

// types
import { INoteCode, INoteCodeReducer, NoteCodeFormValues } from 'pages/__commons/NoteCode/types';

// helpers
import { noteCodeActions } from '.';
import { FormikHelpers } from 'formik';

export interface ISaveNoteCode {
  id?: string;
  value: string;
  description: string;
  show?: boolean;
  createdDate?: string;
  groupId?: string;
}
export interface SaveNoteCodeArgs {
  saveNoteCode: ISaveNoteCode;
  editMode: boolean;
  storeId: string;
  formikBag: FormikHelpers<NoteCodeFormValues>;
}

export interface IPayloadSaveNoteCode {
  data: INoteCode[];
}

export const saveNoteCode = createAsyncThunk<
  IPayloadSaveNoteCode,
  SaveNoteCodeArgs,
  ThunkAPIConfig
>('noteCode/saveNoteCode', async (args, thunkAPI) => {
  const { storeId, editMode, saveNoteCode, formikBag } = args;
  const { resetForm, setSubmitting } = formikBag;
  const { dispatch } = thunkAPI;

  try {
    const response = await noteCodeServices.saveNoteCode(saveNoteCode);

    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: editMode
            ? I18N_TEXT.EDIT_NOTE_CODE_SUCCESS
            : I18N_TEXT.ADD_NOTE_CODE_SUCCESS
        })
      );

      // close modal
      dispatch(
        noteCodeActions.onToggleFormModal({
          editMode,
          storeId,
          selected: undefined
        })
      );
    });

    resetForm();

    return response;
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: editMode
          ? I18N_TEXT.EDIT_NOTE_CODE_FAIL
          : I18N_TEXT.ADD_NOTE_CODE_FAIL
      })
    );

    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  } finally {
    setSubmitting(false);
  }
});

export const saveNoteCodeBuilder = (builder: ActionReducerMapBuilder<INoteCodeReducer>) => {
  const { pending, fulfilled, rejected } = saveNoteCode;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true,
        isError: false
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: false,
        data
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: true
      };
    });
};
