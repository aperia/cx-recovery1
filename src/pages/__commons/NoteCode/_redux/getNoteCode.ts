import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import noteCodeServices from '../noteCodeServices';

// types
import {
  IGetNoteCodeArgs,
  INoteCodeReducer,
  IPayloadGetNoteCode
} from 'pages/__commons/NoteCode/types';

export const getNoteCode = createAsyncThunk<
  IPayloadGetNoteCode,
  IGetNoteCodeArgs,
  ThunkAPIConfig
>('noteCode/getNoteCode', async (args, thunkAPI) => {
  const { storeId, page = 1, groupId } = args;

  try {
    return await noteCodeServices.getNoteCodes({ storeId, page, groupId });
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const getNoteCodeBuilder = (
  builder: ActionReducerMapBuilder<INoteCodeReducer>
) => {
  const { pending, fulfilled, rejected } = getNoteCode;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId, page = 1 } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true,
        isLoadMore: page !== 1,
        isError: false
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data: newData, totalItem, currentPage } = action.payload;

      const currentData = draftState[storeId]?.data || [];

      const isEnd = newData.length === 0;

      draftState[storeId] = {
        ...draftState[storeId],
        data: [...(currentPage === 1 ? [] : currentData), ...newData],
        totalItem,
        currentPage,
        isLoading: false,
        isLoadMore: false,
        isError: false,
        isEnd
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isLoadMore: false,
        isError: true
      };
    });
};
