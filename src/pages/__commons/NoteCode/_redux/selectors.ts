import { createSelector } from '@reduxjs/toolkit';

// types
import { IApplySortAndOrder, INoteCode, OrderByEnum, SortByEnum } from 'pages/__commons/NoteCode/types';
import { IChangeSort, IChangeOrder, INoteCodeState } from './../types';

// helpers
import { handleSort } from 'pages/__commons/NoteCode/helpers';
import { isEqual } from 'lodash';

const getNoteCodeState = (state: RootState, storeId: string): INoteCodeState =>
  state.noteCode[storeId];

const getApplySortAndOrder = (
  state: RootState,
  storeId: string
): IApplySortAndOrder =>
  state.noteCode[storeId]?.applySortAndOrder ||
  ({
    sortBy: 'date',
    orderBy: 'desc'
  } as IApplySortAndOrder);

export const selectNoteCodes = createSelector(
  getNoteCodeState,
  getApplySortAndOrder,
  (
    data: INoteCodeState,
    applySortAndOrder: IApplySortAndOrder
  ): INoteCode[] => {
    const noteCodes = data?.data || [];
    return handleSort(noteCodes, applySortAndOrder);
  }
);

export const selectIsSorting = createSelector(
  getApplySortAndOrder,
  (applySortAndOrder: IApplySortAndOrder): boolean => {
    return !isEqual(applySortAndOrder, {
      sortBy: 'date',
      orderBy: 'desc'
    } as IApplySortAndOrder);
  }
);

export const selectIsExpand = createSelector(
  getNoteCodeState,
  (data: INoteCodeState): boolean => data?.isExpand || false
);

export const selectAddFormIsOpen = createSelector(
  getNoteCodeState,
  (data: INoteCodeState): boolean => data?.addForm?.isOpen || false
);

export const selectEditFormIsOpen = createSelector(
  getNoteCodeState,
  (data: INoteCodeState): boolean => data?.editForm?.isOpen || false
);

export const selectAddFormIsLoading = createSelector(
  getNoteCodeState,
  (data: INoteCodeState): boolean => data?.addForm?.isLoading || false
);

export const selectEditFormIsLoading = createSelector(
  getNoteCodeState,
  (data: INoteCodeState): boolean => data?.editForm?.isLoading || false
);

export const selectIsLoading = createSelector(
  getNoteCodeState,
  (data: INoteCodeState): boolean => data?.isLoading || false
);

export const selectIsLoadMore = createSelector(
  getNoteCodeState,
  (data: INoteCodeState): boolean => data?.isLoadMore || false
);

export const selectIsCurrentPage = createSelector(
  getNoteCodeState,
  (data: INoteCodeState): number => data?.currentPage || 1
);

export const selectIsEnd = createSelector(
  getNoteCodeState,
  (data: INoteCodeState): boolean => data?.isEnd || false
);

export const selectIsError = createSelector(
  getNoteCodeState,
  (data: INoteCodeState): boolean => data?.isError || false
);

export const selectAddFormSelected = createSelector(
  getNoteCodeState,
  (data: INoteCodeState): INoteCode | undefined => data?.addForm?.selected
);

export const selectEditFormSelected = createSelector(
  getNoteCodeState,
  (data: INoteCodeState): INoteCode | undefined => data?.editForm?.selected
);

export const selectSortBy = createSelector(
  getNoteCodeState,
  (data: INoteCodeState): IChangeSort =>
    data?.changingSort || ({ sortBy: SortByEnum.DATE } as IChangeSort)
);

export const selectOrderBy = createSelector(
  getNoteCodeState,
  (data: INoteCodeState): IChangeOrder =>
    data?.changingOrder || ({ orderBy: OrderByEnum.DESC } as IChangeOrder)
);
