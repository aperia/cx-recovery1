import { batch } from 'react-redux';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// actions
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// services
import noteCodeServices from '../noteCodeServices';

// constants
import { API_ERROR, I18N_TEXT } from 'app/constants';

// types
import { INoteCode, INoteCodeReducer } from 'pages/__commons/NoteCode/types';

// helpers
import { getRandomIntInclusive } from 'app/helpers';

export interface DeleteNoteCodeArgs {
  storeId: string;
  id: string;
}

export interface IPayloadDeleteNoteCode {
  data: INoteCode[];
}

export const deleteNoteCode = createAsyncThunk<
  IPayloadDeleteNoteCode,
  DeleteNoteCodeArgs,
  ThunkAPIConfig
>('noteCode/deleteNoteCode', async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;

  try {
    const response = await noteCodeServices.deleteNoteCode(args);
    const success = getRandomIntInclusive(0, 1);

    if (success) {
      batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_TEXT.DELETE_NOTE_CODE_SUCCESS
          })
        );

        dispatch(rootModalActions.close());
      });
    } else {
      throw new Error(I18N_TEXT.DELETE_NOTE_CODE_FAIL);
    }

    return response;
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.DELETE_NOTE_CODE_FAIL
      })
    );

    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  }
});

export const deleteNoteCodeBuilder = (
  builder: ActionReducerMapBuilder<INoteCodeReducer>
) => {
  const { pending, fulfilled, rejected } = deleteNoteCode;
  builder
    .addCase(pending, () => {})
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        data
      };
    })
    .addCase(rejected, () => {});
};
