import { createSlice, PayloadAction } from '@reduxjs/toolkit';

// types
import {
  INoteCodeReducer,
  ExpandPayload,
  IChangeSortPayload,
  IChangeOrderPayload,
  IApplySortAndOrderPayload,
  INoteCodeFormState,
  IToggleFormModalPayload
} from 'pages/__commons/NoteCode/types';
import { deleteNoteCode, deleteNoteCodeBuilder } from './deleteNoteCode';
import { saveNoteCode, saveNoteCodeBuilder } from './saveNoteCode';

// builders
import { getNoteCode, getNoteCodeBuilder } from './getNoteCode';

export const initialState: INoteCodeReducer = {} as INoteCodeReducer;

export const { actions, reducer } = createSlice({
  name: 'noteCode',
  initialState,
  reducers: {
    reset: (draftState, action: PayloadAction<StoreIdPayload>) => {
      const { storeId } = action.payload;

      delete draftState[storeId];
    },
    setExpand: (draftState, action: PayloadAction<ExpandPayload>) => {
      const { storeId, isExpand } = action.payload;
      draftState[storeId].isExpand = isExpand;
    },
    changeSort: (draftState, action: PayloadAction<IChangeSortPayload>) => {
      const { storeId, sortBy } = action.payload;
      draftState[storeId].changingSort = {
        sortBy
      };
    },
    changeOrder: (draftState, action: PayloadAction<IChangeOrderPayload>) => {
      const { storeId, orderBy } = action.payload;
      draftState[storeId].changingOrder = {
        orderBy
      };
    },
    applySortAndOrder: (
      draftState,
      action: PayloadAction<IApplySortAndOrderPayload>
    ) => {
      const { storeId, sortBy, orderBy } = action.payload;
      draftState[storeId].applySortAndOrder = {
        sortBy,
        orderBy
      };
    },
    onToggleFormModal: (
      draftState,
      action: PayloadAction<IToggleFormModalPayload>
    ) => {
      const { storeId, editMode, selected } = action.payload;

      if (editMode) {
        const { editForm = {} as INoteCodeFormState } = draftState[storeId];
        const { isOpen = false } = editForm;

        draftState[storeId] = {
          ...draftState[storeId],
          editForm: {
            ...editForm,
            selected,
            isOpen: !isOpen
          }
        };
      } else {
        const { addForm = {} as INoteCodeFormState } = draftState[storeId];
        const { isOpen = false } = addForm;

        draftState[storeId] = {
          ...draftState[storeId],
          addForm: {
            ...addForm,
            selected,
            isOpen: !isOpen
          }
        };
      }
    }
  },
  extraReducers: builder => {
    getNoteCodeBuilder(builder);
    saveNoteCodeBuilder(builder);
    deleteNoteCodeBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getNoteCode,
  saveNoteCode,
  deleteNoteCode
};

export { combineActions as noteCodeActions };
