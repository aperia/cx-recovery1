import { FormikHelpers } from 'formik';

export interface CommonNoteCodeFormProps {
  editMode: boolean;
  noteCodes: RefDataValue[];
  onCancel: (editMode: boolean) => void;
}

export interface NoteCodeFormValues {
  noteCode: string;
  description: string;
  allAccountsCheckbox: boolean;
}

export interface InitialNoteCodeFormValues extends NoteCodeFormValues {
  noteCodes: INoteCode[];
}

export interface SubmitFormValues {
  editMode: boolean;
  values: NoteCodeFormValues;
  formikBag: FormikHelpers<NoteCodeFormValues>;
}

export interface INoteCode {
  id?: string;
  value: string;
  description: string;
  show: boolean;
  createdDate: string;
  groupId?: string;
  allAccountsCheckbox?: boolean;
}

export interface IPayloadGetNoteCode {
  success: boolean;
  data: INoteCode[];
  totalItem: number;
  currentPage: number;
}

export interface IGetNoteCodeArgs {
  storeId: string;
  page: number;
  groupId?: string;
}

export interface IDeleteNoteCodeArgs {
  id: string;
}

export enum SortByEnum {
  DATE = 'date',
  CODE = 'code'
}

export enum OrderByEnum {
  ASC = 'asc',
  DESC = 'desc'
}

export declare type valueSort = SortByEnum.DATE | SortByEnum.CODE;
export declare type valueOrder = OrderByEnum.DESC | OrderByEnum.ASC;

export const defaultSort = {
  sortBy: SortByEnum.DATE,
  orderBy: OrderByEnum.DESC
};

export interface IChangeSortPayload extends StoreIdPayload {
  sortBy: string;
}
export interface IChangeOrderPayload extends StoreIdPayload {
  orderBy: string;
}
export interface IApplySortAndOrderPayload extends StoreIdPayload {
  sortBy: string;
  orderBy: string;
}
export interface IToggleFormModalPayload extends StoreIdPayload {
  editMode: boolean;
  selected?: INoteCode;
}

export interface IChangeSort extends Omit<IChangeSortPayload, 'storeId'> {}

export interface IChangeOrder extends Omit<IChangeOrderPayload, 'storeId'> {}
export interface IApplySortAndOrder
  extends Omit<IApplySortAndOrderPayload, 'storeId'> {}

export interface INoteCodeFormState {
  isOpen: boolean;
  isLoading: boolean;
  isError: boolean;
  selected?: INoteCode;
  allAccountsCheckbox: boolean;
}

export interface INoteCodeState {
  addForm: INoteCodeFormState;
  editForm: INoteCodeFormState;
  changingSort: IChangeSort;
  changingOrder: IChangeOrder;
  applySortAndOrder: IApplySortAndOrder;
  totalItem: number;
  currentPage: number;
  data: INoteCode[];
  commonData: INoteCode[]; // shared notes among accounts
  isExpand: boolean; // expand or collapse all notes text more
  isLoading: boolean;
  isLoadMore: boolean;
  isEnd: boolean;
  isError: boolean;
  selected?: INoteCode;
}

export interface INoteCodeReducer {
  [storeId: string]: INoteCodeState;
}

export interface ExpandPayload extends StoreIdPayload {
  isExpand: boolean;
}
