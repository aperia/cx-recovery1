import React, { useCallback, useEffect, useRef, useState } from 'react';
import { batch, useDispatch } from 'react-redux';

// components
import { SimpleBar } from 'app/_libraries/_dls/components';
import AddSection from './AddSection';
import SaveFormControl from './SaveFormControl';
import Header from './Header';
import NoteCodeList from './NoteCodeList';
import LoadMore from 'app/components/LoadMore';
import If from 'pages/__commons/If';

// types
import { INoteCode, SubmitFormValues } from './types';

// actions
import { noteCodeActions } from './_redux';

// selectors
import {
  selectAddFormIsOpen,
  selectEditFormSelected,
  selectIsCurrentPage,
  selectIsEnd,
  selectIsLoading,
  selectIsLoadMore,
  selectNoteCodes
} from './_redux/selectors';

// hooks
import { useAccountDetail, useSelectorId } from 'app/hooks';

// constants
import { I18N_TEXT } from 'app/constants';

// helpers
import { debounce, getScrollParent } from 'app/helpers';
import { getAccountDetail } from 'pages/AccountDetail/_redux/selectors';

export interface NoteCodeProps {
  shouldRenderPinnedIcon?: boolean;
}
const NoteCode: React.FC<NoteCodeProps> = ({
  shouldRenderPinnedIcon = false
}) => {
  const dispatch = useDispatch();

  const { storeId } = useAccountDetail();

  const isAddFormOpen = useSelectorId<boolean>(selectAddFormIsOpen);
  const noteCodes = useSelectorId<INoteCode[]>(selectNoteCodes);

  const [scrollPosition, setScrollPosition] = useState(0);
  const ref = useRef<HTMLDivElement | null>(null);
  const scrollParentRef = useRef<HTMLElement | null>(null);

  const isLoading = useSelectorId<boolean>(selectIsLoading);
  const isLoadMore = useSelectorId<boolean>(selectIsLoadMore);
  const isEnd = useSelectorId<boolean>(selectIsEnd);
  const currentPage = useSelectorId<number>(selectIsCurrentPage);
  const isFirstTime = useRef<boolean>(true);

  const { groupId } = useSelectorId(getAccountDetail) || {};

  const selectedEdit =
    useSelectorId<INoteCode | undefined>(selectEditFormSelected) ||
    ({} as INoteCode);

  // handle init note list
  useEffect(() => {
    if (isFirstTime.current) {
      isFirstTime.current = false;

      batch(() => {
        dispatch(noteCodeActions.reset({ storeId }));
        dispatch(noteCodeActions.getNoteCode({ groupId, storeId, page: 1 }));
      });
    }
  }, [dispatch, groupId, storeId]);

  const handleShowAddForm = () => {
    dispatch(
      noteCodeActions.onToggleFormModal({
        editMode: false,
        storeId,
        selected: undefined
      })
    );
  };

  const handleCancel = (editMode: boolean) => {
    dispatch(
      noteCodeActions.onToggleFormModal({
        editMode,
        storeId,
        selected: undefined
      })
    );
  };

  const handleSubmit = ({ values, editMode, formikBag }: SubmitFormValues) => {
    const { noteCode: value, description, allAccountsCheckbox } = values;

    const saveNoteCode = {
      ...(editMode ? selectedEdit : {}),
      value,
      description,
      groupId: allAccountsCheckbox ? undefined : groupId
    };

    dispatch(
      noteCodeActions.saveNoteCode({
        saveNoteCode,
        editMode,
        storeId,
        formikBag
      })
    );
  };

  const handleUpdateScrollPosition = debounce(
    () => {
      scrollParentRef.current = getScrollParent(ref.current);
      if (scrollParentRef?.current) {
        setScrollPosition(scrollParentRef.current.scrollTop);
      }
    },
    300,
    { leading: false, trailing: true }
  );

  useEffect(() => {
    if (scrollPosition) {
      scrollParentRef.current = getScrollParent(ref.current);
      scrollParentRef.current?.scrollTo({
        top: scrollPosition,
        behavior: 'smooth'
      });
    }
  }, [scrollPosition]);

  const handleLoadMore = useCallback(() => {
    if (isEnd || isLoading || isLoadMore) return;

    dispatch(
      noteCodeActions.getNoteCode({ groupId, storeId, page: currentPage + 1 })
    );
  }, [currentPage, dispatch, groupId, isEnd, isLoadMore, isLoading, storeId]);

  const handleSetPositionAfterScroll = (event: React.UIEvent<HTMLElement>) => {
    handleUpdateScrollPosition();
  };

  return (
    <>
      <SimpleBar onScroll={handleSetPositionAfterScroll}>
        <div ref={ref}>
          <SaveFormControl
            editMode={false}
            onSubmit={handleSubmit}
            onCancel={handleCancel}
          />

          <SaveFormControl
            editMode={true}
            onSubmit={handleSubmit}
            onCancel={handleCancel}
          />

          <If condition={!isAddFormOpen}>
            <AddSection
              text={I18N_TEXT.ADD_NOTE_CODE}
              onClick={handleShowAddForm}
            />
          </If>

          <div className="p-24 bg-white overlap-next-action">
            <Header
              storeId={storeId}
              shouldRenderPinnedIcon={shouldRenderPinnedIcon}
              withSort={noteCodes.length > 0}
            />

            <div className="pt-8">
              <NoteCodeList data={noteCodes} />
            </div>

            <LoadMore
              onLoadMore={handleLoadMore}
              isEnd={isEnd}
              loading={isLoadMore}
              loadingText={I18N_TEXT.LOADING_MORE}
              endLoadMoreText={I18N_TEXT.END_OF_NOTES}
            />
          </div>
        </div>
      </SimpleBar>
    </>
  );
};

export default React.memo(NoteCode);
