import React from 'react';

// components
import { NoDataGrid } from 'app/components';
import NoteCodeItem from '../NoteCodeItem';

// types
import { INoteCode } from '../types';

// constants
import { I18N_TEXT } from 'app/constants';

// helpers
import { isEmpty } from 'app/helpers';

export interface NoteCodeListProps {
  data: INoteCode[];
}
const NoteCodeList: React.FC<NoteCodeListProps> = ({ data }) => {
  return (
    <>
      {isEmpty(data) ? (
        <div className="py-40">
          <NoDataGrid
            iconName="comment"
            text={I18N_TEXT.NO_NOTE_CODE_TO_DISPLAY}
          />
        </div>
      ) : (
        data.map((noteCode: INoteCode) => (
          <NoteCodeItem key={noteCode.id} data={noteCode} />
        ))
      )}
    </>
  );
};

export default NoteCodeList;
