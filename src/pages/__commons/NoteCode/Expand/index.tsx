import React from 'react';

// components
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';

// hooks
import { useSelectorId } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { useDispatch } from 'react-redux';
import { selectIsExpand } from '../_redux/selectors';
import { noteCodeActions } from '../_redux';

// i18n
import { I18N_TEXT } from 'app/constants';

export interface ExpandProps {
  storeId: string;
}

const Expand: React.FC<ExpandProps> = ({ storeId }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const isExpand = useSelectorId<boolean>(selectIsExpand);

  const handleExpand = () => {
    dispatch(noteCodeActions.setExpand({ storeId, isExpand: !isExpand }));
  };

  return (
    <Tooltip
      element={isExpand ? t(I18N_TEXT.COLLAPSE_ALL) : t(I18N_TEXT.EXPAND_ALL)}
      placement="top"
      variant="primary"
    >
      <Button
        id="note_code_expand-btn"
        onClick={handleExpand}
        variant="icon-secondary"
      >
        <Icon name={isExpand ? 'collapse-all' : 'expand-all'} />
      </Button>
    </Tooltip>
  );
};

export default Expand;
