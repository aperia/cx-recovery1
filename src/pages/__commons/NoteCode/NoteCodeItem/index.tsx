import React, { useCallback, useEffect, useMemo, useState } from 'react';
import { useDispatch } from 'react-redux';

// types
import { INoteCode } from '../types';

// hooks
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_TEXT } from 'app/constants';

// helpers
import classNames from 'classnames';

// components
import { TruncateText } from 'app/_libraries/_dls/components';
import MoreActions, { ActionValue } from 'pages/__commons/MoreActions';

// selectors
import { selectIsExpand } from '../_redux/selectors';

// actions
import { noteCodeActions } from '../_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import useRenderTime from '../hooks/useRenderTime';

export interface NoteCodeItemProps {
  data: INoteCode;
}
const NoteCodeItem: React.FC<NoteCodeItemProps> = ({ data }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { storeId } = useAccountDetail();
  const isExpand = useSelectorId<boolean>(selectIsExpand);
  const [shown, setShown] = useState(false);

  const handleOnShown = useCallback(() => {
    setShown(!shown);
  }, [shown]);

  useEffect(() => {
    setShown(isExpand);
  }, [isExpand]);

  const { createdDate } = data;
  const noteTime = useRenderTime(createdDate);

  const actions = useMemo(() => {
    const allActions = [
      {
        label: t(I18N_TEXT.EDIT),
        uniqueId: 'edit'
      },
      {
        label: t(I18N_TEXT.DELETE),
        uniqueId: 'delete'
      }
    ];
    return allActions.filter(item => {
      if (item.uniqueId === 'edit') {
        return true;
      }
      return true;
    });
  }, [t]);

  const onSelectActions = (event: ActionValue, data: INoteCode) => {
    switch (event.uniqueId) {
      case 'delete':
        dispatch(
          rootModalActions.open({
            autoClose: false,
            title: I18N_TEXT.DELETE_NOTE_CODE_TITLE,
            body: I18N_TEXT.DELETE_NOTE_CODE_CONFIRM,
            btnConfirmText: I18N_TEXT.DELETE,
            onConfirm: () => {
              dispatch(
                noteCodeActions.deleteNoteCode({ storeId, id: data.id || '' })
              );
            },
            onCancel: () => {
              dispatch(rootModalActions.close());
            }
          })
        );
        break;
      case 'edit':
        dispatch(
          noteCodeActions.onToggleFormModal({
            editMode: true,
            storeId,
            selected: data
          })
        );
        break;
      default:
        break;
    }
  };

  return (
    <div key={data.id} className="bg-light-l16 py-8 px-16 br-radius-8 mt-8">
      <div className="d-flex align-items-center justify-content-between mb-8">
        <p className="fs-14 color-grey-d20 fw-500">{data.value}</p>
        <div className="d-flex align-items-center">
          <p className="fs-12 color-grey">{noteTime}</p>
          <div className={classNames('ml-8')}>
            <MoreActions
              onSelect={event => onSelectActions(event, data)}
              actions={actions}
              placementDropdown="bottom-end"
            />
          </div>
        </div>
      </div>
      <TruncateText
        className="color-grey-d08"
        lines={2}
        resizable={true}
        onShown={handleOnShown}
        shown={shown}
        ellipsisLessText={t(I18N_TEXT.LESS)}
        ellipsisMoreText={t(I18N_TEXT.MORE)}
      >
        {data.description}
      </TruncateText>
    </div>
  );
};

export default NoteCodeItem;
