import React, { useCallback } from 'react';

// redux
import { noteCodeActions } from '../_redux';
import { useDispatch } from 'react-redux';

// components
import { Radio } from 'app/_libraries/_dls/components';

// hooks
import { useSelectorId } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// types
import { IChangeSort, SortByEnum } from '../types';

// constants
import { I18N_TEXT } from 'app/constants';

// helpers
import { selectSortBy } from '../_redux/selectors';

export interface SortByProps {
  storeId: string;
}
const SortBy: React.FC<SortByProps> = ({ storeId }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const valueRadio = useSelectorId<IChangeSort>(selectSortBy).sortBy;

  const handleChangeRadio = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const value = e.target.value;

      dispatch(
        noteCodeActions.changeSort({
          storeId,
          sortBy: value
        })
      );
    },
    [dispatch, storeId]
  );

  return (
    <div className="d-flex mt-16">
      <p className="mr-24 color-grey fs-14 fw-500">{t(I18N_TEXT.SORT_BY)}:</p>
      <Radio className="mr-24">
        <Radio.Input
          onChange={handleChangeRadio}
          checked={valueRadio === SortByEnum.DATE}
          value={SortByEnum.DATE}
          name="sortBy"
          id="DATE"
        />
        <Radio.Label>{t(I18N_TEXT.SORT_BY_DATE)}</Radio.Label>
      </Radio>
      <Radio>
        <Radio.Input
          onChange={handleChangeRadio}
          checked={valueRadio === SortByEnum.CODE}
          value={SortByEnum.CODE}
          name="sortBy"
          id="CODE"
        />
        <Radio.Label>{t(I18N_TEXT.SORT_BY_CODE)}</Radio.Label>
      </Radio>
    </div>
  );
};

export default SortBy;
