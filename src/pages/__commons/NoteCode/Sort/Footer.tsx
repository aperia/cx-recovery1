import React from 'react';

// redux
import { batch, useDispatch } from 'react-redux';

// components
import { Button } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// actions
import { noteCodeActions } from '../_redux';

// selectors
import { selectSortBy, selectOrderBy } from '../_redux/selectors';

// types
import { IChangeSort, IChangeOrder, SortByEnum, OrderByEnum } from '../types';

// constants
import { I18N_TEXT } from 'app/constants';
import { useSelectorId } from 'app/hooks';

interface IFooter {
  storeId: string;
  handleOpenPopper: () => void;
}

const Footer: React.FC<IFooter> = ({ storeId, handleOpenPopper }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const sortBy = useSelectorId<IChangeSort>(selectSortBy).sortBy;
  const orderBy = useSelectorId<IChangeOrder>(selectOrderBy).orderBy;

  const handleApply = () => {
    dispatch(
      noteCodeActions.applySortAndOrder({
        storeId,
        sortBy,
        orderBy
      })
    );

    handleOpenPopper();
  };

  const resetToDefault = () => {
    batch(() => {
      dispatch(
        noteCodeActions.changeSort({
          storeId,
          sortBy: SortByEnum.DATE
        })
      );

      dispatch(
        noteCodeActions.changeOrder({
          storeId,
          orderBy: OrderByEnum.DESC
        })
      );
    });
  };

  return (
    <div className="d-flex mt-24 justify-content-end">
      <Button onClick={resetToDefault} size="sm" variant="secondary">
        {t(I18N_TEXT.RESET_TO_DEFAULT)}
      </Button>
      <Button size="sm" onClick={handleApply} variant="primary">
        {t(I18N_TEXT.APPLY)}
      </Button>
    </div>
  );
};

export default React.memo(Footer);
