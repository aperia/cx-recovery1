import React, { useCallback } from 'react';

// redux
import { useDispatch } from 'react-redux';

// actions
import { noteCodeActions } from '../_redux';

// selectors
import { selectOrderBy } from '../_redux/selectors';

// components
import { Radio } from 'app/_libraries/_dls/components';

// hooks
import { useSelectorId } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// types
import { IChangeOrder, OrderByEnum } from '../types';

// constants
import { I18N_TEXT } from 'app/constants';

// helpers

export interface OrderByProps {
  storeId: string;
}

const OrderBy: React.FC<OrderByProps> = ({ storeId }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const valueRadio = useSelectorId<IChangeOrder>(selectOrderBy).orderBy;

  const handleChangeRadio = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      const value = e.target.value;
      dispatch(
        noteCodeActions.changeOrder({
          storeId,
          orderBy: value
        })
      );
    },
    [dispatch, storeId]
  );

  return (
    <div className="d-flex mt-16">
      <p className="mr-24 color-grey fs-14 fw-500">
        {t(I18N_TEXT.ORDER_BY)}:
      </p>
      <Radio className="mr-24">
        <Radio.Input
          onChange={handleChangeRadio}
          checked={valueRadio === OrderByEnum.ASC}
          value={OrderByEnum.ASC}
          name="orderBy"
          id="ASC"
        />
        <Radio.Label>{t(I18N_TEXT.ASCENDING)}</Radio.Label>
      </Radio>
      <Radio>
        <Radio.Input
          onChange={handleChangeRadio}
          checked={valueRadio === OrderByEnum.DESC}
          value={OrderByEnum.DESC}
          name="orderBy"
          id="DESC"
        />
        <Radio.Label>{t(I18N_TEXT.DESCENDING)}</Radio.Label>
      </Radio>
    </div>
  );
};

export default React.memo(OrderBy);
