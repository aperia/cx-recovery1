import React, { useCallback, useState } from 'react';

// components
import { Popover, Icon, Button, Tooltip } from 'app/_libraries/_dls/components';
import SortBy from './SortBy';
import Footer from './Footer';
import OrderBy from './OrderBy';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_TEXT } from 'app/constants';

// helpers
import classnames from 'classnames';
import { useSelectorId } from 'app/hooks';
import { selectIsSorting } from '../_redux/selectors';

export interface SortProps {
  storeId: string;
}

const Sort: React.FC<SortProps> = ({ storeId }) => {
  const { t } = useTranslation();
  const [isOpen, setOpen] = useState(false);

  const isDisplayIndicator = useSelectorId<boolean>(selectIsSorting);

  const handleTogglePopper = useCallback(() => {
    setOpen(!isOpen);
  }, [isOpen]);

  const handleVisibilityChange = (nextOpened: boolean) => {
    setOpen(nextOpened);
  };

  return (
    <Tooltip
      triggerClassName="mr-n4 ml-16"
      placement="top"
      element={t(I18N_TEXT.SORT_ORDER)}
      variant="primary"
    >
      <Popover
        containerClassName="inside-infobar"
        size="md"
        placement="bottom-end"
        opened={isOpen}
        onVisibilityChange={handleVisibilityChange}
        element={
          <>
            <h4 className="mb-16">{t(I18N_TEXT.SORT_ORDER)}</h4>
            <SortBy storeId={storeId} />
            <OrderBy storeId={storeId} />
            <Footer storeId={storeId} handleOpenPopper={handleTogglePopper} />
          </>
        }
      >
        <Button
          variant="icon-secondary"
          className={classnames({
            asterisk: isDisplayIndicator
          })}
          onClick={handleTogglePopper}
        >
          <Icon name="sort-by" />
        </Button>
      </Popover>
    </Tooltip>
  );
};

export default React.memo(Sort);
