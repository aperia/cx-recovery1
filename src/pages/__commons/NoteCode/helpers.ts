import { orderBy } from 'app/helpers';
import { IApplySortAndOrder, INoteCode } from 'pages/__commons/NoteCode/types';

export const handleSort = (
  noteCodes: INoteCode[],
  applySortAndOrder: IApplySortAndOrder
) => {
  // use slice() to copy the array and not just make a reference
  const data = noteCodes.slice(0);

  const sortKey = applySortAndOrder.sortBy === 'date' ? 'createdDate' : 'value';
  const sortOrder = applySortAndOrder.orderBy === 'asc' ? 'asc' : 'desc';

  const sortedData = orderBy(data, sortKey, sortOrder);
  return sortedData;
};
