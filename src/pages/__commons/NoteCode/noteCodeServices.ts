// import { apiService } from 'app/utils/api.service';
import { noteCodeController } from './__mocks__/noteCodeController';

// types
import { ISaveNoteCode } from './_redux/saveNoteCode';
import { DeleteNoteCodeArgs } from './_redux/deleteNoteCode';
import { IGetNoteCodeArgs } from './types';

const noteCodeServices = {
  getNoteCodes({ storeId, page, groupId }: IGetNoteCodeArgs) {
    return noteCodeController.getNoteCodes({ storeId, page, groupId });
  },
  saveNoteCode(noteCode: ISaveNoteCode) {
    return noteCodeController.saveNoteCode(noteCode);
  },
  deleteNoteCode(args: DeleteNoteCodeArgs) {
    return noteCodeController.deleteNoteCode(args);
  }
};

export default noteCodeServices;
