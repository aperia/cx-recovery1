import React from 'react';
import { batch, useDispatch } from 'react-redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelectorId } from 'app/hooks';

// components
import Sort from '../Sort';
import Expand from '../Expand';
import TogglePin from 'app/components/InfoBar/TogglePin';
import If from 'pages/__commons/If';

// constants
import { I18N_TEXT } from 'app/constants';

// selectors
import { selectPinned } from 'app/components/InfoBar/_redux/selectors';
import { actionsInfoBar, InfoBarSection } from 'app/components/InfoBar/_redux';

export interface HeaderProps {
  shouldRenderPinnedIcon?: boolean;
  storeId: string;
  withSort?: boolean;
}

const Header: React.FC<HeaderProps> = ({
  shouldRenderPinnedIcon = false,
  withSort = true,
  storeId
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const pinned = useSelectorId(selectPinned);
  const isPinned = pinned === InfoBarSection.Notes;

  const handleTogglePin = () => {
    batch(() => {
      const thisPinned = isPinned ? undefined : InfoBarSection.Notes;

      dispatch(
        actionsInfoBar.updatePinned({
          storeId,
          pinned: thisPinned
        })
      );

      if (thisPinned) {
        dispatch(actionsInfoBar.updateRecover({ storeId, recover: undefined }));
      }

      dispatch(actionsInfoBar.updateIsExpand({ storeId, isExpand: !isPinned }));
    });
  };

  return (
    <div className="d-flex flex-row column align-items-center justify-content-between">
      <div className="d-inline-flex">
        <h5 className="mr-8">{t(I18N_TEXT.NOTES)}</h5>
        {shouldRenderPinnedIcon && (
          <TogglePin isPinned={isPinned} onToggle={handleTogglePin} />
        )}
      </div>

      <If condition={withSort}>
        <div className="d-flex mr-n4">
          <div className="d-inline-flex mr-16">
            <Sort storeId={storeId} />
          </div>
          <div className="d-inline-flex">
            <Expand storeId={storeId} />
          </div>
        </div>
      </If>
    </div>
  );
};

export default Header;
