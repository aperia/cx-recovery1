import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface AddSectionProps {
  text: string;
  dataTestId?: string;
  onClick?: (event: React.MouseEvent<HTMLDivElement, MouseEvent>) => void;
}

const AddSection: React.FC<AddSectionProps> = ({ text, onClick }) => {
  const { t } = useTranslation();
  const handleOnClick = (
    event: React.MouseEvent<HTMLDivElement, MouseEvent>
  ) => {
    if (onClick) onClick(event);
  };

  if (!text) return null;

  return (
    <div
      className="py-18 px-24 bg-light-l20 border-bottom cursor-pointer"
      onClick={handleOnClick}
    >
      <span className="color-grey-l16">{t(text)}</span>
    </div>
  );
};

export default AddSection;
