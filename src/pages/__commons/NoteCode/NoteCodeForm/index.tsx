import React, { useCallback } from 'react';

// components
import { DropdownControl } from 'app/components/FormControl';
import FormikTextAreaControl from 'app/components/FormControl/FormikTextAreaControl';
import { CheckBox } from 'app/_libraries/_dls/components';

// types
import { INoteCode, NoteCodeFormValues } from '../types';

// constants
import { NOTE_CODE_FIELD } from '../constants';

// helpers
import { useFormikContext } from 'formik';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface NoteCodeFormProps {
  editMode: boolean;
  noteCodes: RefDataValue[];
}

const NoteCodeForm: React.FC<NoteCodeFormProps> = ({ editMode, noteCodes }) => {
  const { t } = useTranslation();
  const {
    setFieldValue,
    setFieldTouched,
    handleChange,
    values,
    resetForm,
    handleBlur
  } = useFormikContext<NoteCodeFormValues>();

  const handleValueChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      // reset form
      resetForm();

      // trigger change formik's value
      const item = e.value as INoteCode;
      const event = {
        ...e,
        target: {
          ...e.target,
          value: item.value
        }
      };

      handleChange(event);
      handleBlur(event);
      // set description correspond to selected noteCode
      setFieldValue(
        NOTE_CODE_FIELD.NOTE_CODE_DESCRIPTION.name,
        item?.description,
        false
      );

      setFieldTouched(NOTE_CODE_FIELD.NOTE_CODE_DESCRIPTION.name, true);
    },
    [handleBlur, handleChange, resetForm, setFieldTouched, setFieldValue]
  );

  const handleCheckboxChange = useCallback(
    (e: React.ChangeEvent<HTMLInputElement>) => {
      handleChange(e);
    },
    [handleChange]
  );

  return (
    <>
      <DropdownControl
        onValueChange={handleValueChange}
        name={NOTE_CODE_FIELD.NOTE_CODE_CODE.name}
        label={NOTE_CODE_FIELD.NOTE_CODE_CODE.label}
        required
        options={noteCodes as RefDataValue[]}
        onlyShowValue
      />
      <div className="mt-16">
        <FormikTextAreaControl
          name={NOTE_CODE_FIELD.NOTE_CODE_DESCRIPTION.name}
          label={NOTE_CODE_FIELD.NOTE_CODE_DESCRIPTION.label}
          maxLength={NOTE_CODE_FIELD.NOTE_CODE_DESCRIPTION.limit}
          disabled={!values?.noteCode}
          required={editMode ? true : !!values?.noteCode}
          disabledEnter
        />
      </div>
      <CheckBox className="mt-16">
        <CheckBox.Input
          name={NOTE_CODE_FIELD.NOTE_CODE_ALL_ACCOUNTS_CHECKBOX.name}
          checked={values.allAccountsCheckbox}
          onChange={handleCheckboxChange}
        />
        <CheckBox.Label>
          {t(NOTE_CODE_FIELD.NOTE_CODE_ALL_ACCOUNTS_CHECKBOX.label)}
        </CheckBox.Label>
      </CheckBox>
    </>
  );
};
export default NoteCodeForm;
