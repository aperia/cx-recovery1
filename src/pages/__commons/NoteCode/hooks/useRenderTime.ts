import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_TEXT } from 'app/constants';
import { FormatTime } from 'app/constants/enums';

// helpers
import { formatTimeDefault, isToday, isYesterday } from 'app/helpers';

const useRenderTime = (date: string): string => {
  const { t } = useTranslation();

  const noteTime = React.useMemo(() => {
    if (isToday(date)) {
      return `${t(I18N_TEXT.TODAY)} ${formatTimeDefault(
        date,
        FormatTime.FullTimeMeridiem
      )}`;
    }

    if (isYesterday(date)) {
      return `${t(I18N_TEXT.YESTERDAY)} ${formatTimeDefault(
        date,
        FormatTime.FullTimeMeridiem
      )}`;
    }

    return formatTimeDefault(date, FormatTime.AllDatetime);
  }, [date, t]);

  return noteTime || '';
};

export default useRenderTime;
