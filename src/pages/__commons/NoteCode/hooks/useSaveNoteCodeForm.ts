// types
import { INoteCode, NoteCodeFormValues } from 'pages/__commons/NoteCode/types';

// hooks
import useValidation from 'pages/__commons/NoteCode/hooks/useValidation';

// selectors
import {
  selectAddFormSelected,
  selectEditFormSelected
} from '../_redux/selectors';

// helpers
import { FormikContextType, FormikHelpers, useFormik } from 'formik';
import { useSelectorId } from 'app/hooks';
import useNoteCodePremiseList from './useNoteCodePremiseList';

export interface UseSaveNoteCodeFormProps {
  editMode: boolean;
  onSubmit: Function;
}

const useSaveNoteCodeForm = ({
  editMode,
  onSubmit
}: UseSaveNoteCodeFormProps) => {
  const { validationSchema } = useValidation();
  const { noteCodes } = useNoteCodePremiseList();

  const selectedAdd =
    useSelectorId<INoteCode | undefined>(selectAddFormSelected) ||
    ({} as INoteCode);

  const selectedEdit =
    useSelectorId<INoteCode | undefined>(selectEditFormSelected) ||
    ({} as INoteCode);

  const {
    value: noteCode,
    description,
    groupId
  } = editMode ? selectedEdit : selectedAdd;

  const isPublic = editMode && groupId === undefined;

  const formik: FormikContextType<NoteCodeFormValues> = useFormik({
    enableReinitialize: true,
    validateOnBlur: true,
    validateOnChange: false,
    initialValues: {
      noteCodeOptions: noteCodes,
      noteCode,
      description,
      allAccountsCheckbox: isPublic
    } as NoteCodeFormValues,
    onSubmit: (
      values: NoteCodeFormValues,
      formikBag: FormikHelpers<NoteCodeFormValues>
    ) => {
      onSubmit({ values, editMode, formikBag });
    },
    validationSchema
  });

  return { formik, noteCodes };
};

export default useSaveNoteCodeForm;
