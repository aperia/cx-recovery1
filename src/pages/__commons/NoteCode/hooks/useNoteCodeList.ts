import { useEffect } from 'react';
import { useDispatch } from 'react-redux';

// types
import { INoteCode } from '../types';

// hooks
import { useSelectorId } from 'app/hooks';

// helpers
import { noteCodeActions } from '../_redux';
import { selectIsLoading, selectNoteCodes } from '../_redux/selectors';

const useNoteCodeList = () => {
  const dispatch = useDispatch();

  const noteCodes = useSelectorId<INoteCode[]>(selectNoteCodes);
  const isLoading = useSelectorId<boolean>(selectIsLoading);

  useEffect(() => {
    dispatch(noteCodeActions.getNoteCode({ storeId: '888', page: 1 }));
  }, [dispatch]);

  return {
    isLoading,
    noteCodes
  };
};

export default useNoteCodeList;
