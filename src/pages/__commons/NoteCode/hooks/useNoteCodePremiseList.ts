import { useMemo } from 'react';

// hooks
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';

// helpers
import { checkEmpty } from 'app/helpers';

const useNoteCodePremiseList = () => {
  //   generate ref data options
  const { data: noteCodeData } = useGetRefDataQuery('noteCodePremises');
  const noteCodes: RefDataValue[] = useMemo(
    () => checkEmpty([noteCodeData?.noteCodes as RefDataValue[], []]),
    [noteCodeData?.noteCodes]
  );

  return {
    noteCodes
  };
};

export default useNoteCodePremiseList;
