// helpers
import * as Yup from 'yup';
import { I18N_TEXT } from 'app/constants';
import { NOTE_CODE_FIELD } from 'pages/__commons/NoteCode/constants';

const useValidation = () => {
  const validationSchema = Yup.object().shape({
    [NOTE_CODE_FIELD.NOTE_CODE_CODE.name]: Yup.string().required(
      I18N_TEXT.NOTE_CODE_CODE_REQUIRED
    ),
    [NOTE_CODE_FIELD.NOTE_CODE_DESCRIPTION.name]: Yup.string()
      .required(I18N_TEXT.NOTE_CODE_CODE_DESCRIPTION_REQUIRED)
      .max(NOTE_CODE_FIELD.NOTE_CODE_DESCRIPTION.limit)
  });

  return {
    validationSchema
  };
};

export default useValidation;
