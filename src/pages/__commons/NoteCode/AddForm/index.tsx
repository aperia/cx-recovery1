import React, { useCallback } from 'react';

// components
import NoteCodeForm from 'pages/__commons/NoteCode/NoteCodeForm';

// hooks
import { useSelectorId } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// selectors
import {
  selectAddFormIsLoading,
  selectAddFormIsOpen
} from '../_redux/selectors';

// types
import { CommonNoteCodeFormProps } from 'pages/__commons/NoteCode/types';
import { useFormikContext } from 'formik';

// helpers

import { Button } from 'app/_libraries/_dls/components';
import { I18N_TEXT } from 'app/constants';
import { NOTE_CODE_FIELD } from '../constants';

export interface AddNoteCodeFormProps extends CommonNoteCodeFormProps {}

const AddForm: React.FC<AddNoteCodeFormProps> = ({
  editMode,
  noteCodes,
  onCancel
}) => {
  const { t } = useTranslation();
  const {
    touched,
    isValid,
    submitForm,
    resetForm,
    isSubmitting,
    setSubmitting
  } = useFormikContext();

  const isOpen = useSelectorId<boolean>(selectAddFormIsOpen);
  const isLoading = useSelectorId<boolean>(selectAddFormIsLoading);

  const handleCancel = useCallback(() => {
    resetForm();
    setSubmitting(false);

    onCancel(false);
  }, [onCancel, resetForm, setSubmitting]);

  const handleSubmit = useCallback(() => {
    setSubmitting(true);
    submitForm();
  }, [setSubmitting, submitForm]);

  const isTouchedForm = useCallback(() => {
    return Object.keys(touched).includes(
      NOTE_CODE_FIELD.NOTE_CODE_DESCRIPTION.name
    );
  }, [touched]);

  if (!isOpen || isLoading) return null;

  return (
    <div className="bg-light-l20 px-24 py-16 border-bottom">
      <NoteCodeForm editMode={editMode} noteCodes={noteCodes} />

      <div className="d-flex justify-content-end mt-24">
        <Button size="sm" variant="secondary" onClick={handleCancel}>
          {t(I18N_TEXT.CANCEL)}
        </Button>
        <Button
          size="sm"
          disabled={isSubmitting || !isTouchedForm() || !isValid}
          variant="primary"
          onClick={handleSubmit}
        >
          {t(I18N_TEXT.SUBMIT)}
        </Button>
      </div>
    </div>
  );
};

export default AddForm;
