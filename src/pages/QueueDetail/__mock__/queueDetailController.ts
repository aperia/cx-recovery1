import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { IAccounts } from 'pages/AccountManagement/types';
import { IAccountsQueueDetail, ISearchWorkQueuePayload } from '../types';
import { accountGroupListData } from 'pages/AccountGroup/__mock__/accountGroupController';
import accountList from './accountList.json';
import { IDataGroupList } from 'pages/AccountGroup/types';
import { accountRepository } from 'pages/AccountActivity/__mock__/queueList/queueListController';
import { QueueAccount } from 'pages/AccountActivity/types';

export const accountListData: IAccounts[] = JSON.parse(
  JSON.stringify(accountList)
);

let _accountList = accountList;

const calendarCodeMock = [
  'BPP',
  'CAN',
  'CAS',
  'CBC',
  'DSP',
  'DSR',
  'JDF',
  'LFU',
  'LTR'
];

for (let i = 0; i < 20; i++) {
  const randomAccountId = `${Math.floor(Math.random() * 3) + 1}`;
  let accountData = accountList.find(obj => obj.id === randomAccountId);

  if (!accountData) {
    accountData = accountList[0];
  }

  _accountList = [
    ..._accountList,
    {
      id: `${5 + i}`,
      debtorName: accountData.debtorName,
      accountNumber: accountData.accountNumber,
      creditBureauAccNbr: `02200200091${Math.floor(Math.random() * 1000)}`,
      accNumber: accountData.accNumber,
      queueIDs: ['PNDDIS', 'ATY001', 'BBKWHS', 'FRDWHS'],
      status: ['active', 'purged'][Math.floor(Math.random() * 2)],
      agencyCode: 'PRI001 - Colddata Collection Agency',
      reasonCode: 'R - Charged Off',
      collectorCode: 'ATY001',
      statusCode: 'B13',
      calendarCode: calendarCodeMock[Math.floor(Math.random() * 8)],
      currentBalance: '$1,000.00',
      accountType: 'MC',
      accountClass: 'MG',
      specialCondition: ['B', 'D', 'X']
    }
  ];
}

export const queueDetailController = {
  getAccountList(queueID: string, searchValue: AttributeSearchValue[]) {
    const data = (_accountList as IAccountsQueueDetail[]).filter(item =>
      item.queueIDs.some((_queueID: string) => _queueID === queueID)
    );

    if (searchValue.length) {
      let filterData: IAccountsQueueDetail[] = [];

      filterData = data.filter((dataAccount: any) =>
        searchValue.every(searchItem => {
          if (searchItem.key === 'accountNumber') {
            return dataAccount?.accNumber?.accountNumberUnMask?.includes(
              searchItem.value
            );
          }
          return dataAccount[searchItem.key]
            .toLowerCase()
            .includes(searchItem.value.toLowerCase());
        })
      );

      return Promise.resolve({
        data: {
          success: true,
          data: filterData
        }
      });
    }
    return Promise.resolve({
      data: {
        success: true,
        data
      }
    });
  },
  getAccountToWork(params: ISearchWorkQueuePayload) {
    let data = (_accountList as IAccountsQueueDetail[]).filter(item =>
      item.queueIDs.some((_queueID: string) => _queueID === params.queueId)
    );

    if (params?.id) {
      data = data.filter(obj => obj.id === params.id);
    }

    let filterData: IAccountsQueueDetail[] = data;

    if (params?.calendarCode) {
      filterData = data.filter(
        (dataAccount: IAccountsQueueDetail) =>
          params?.calendarCode &&
          dataAccount.calendarCode
            .toLowerCase()
            .includes(params?.calendarCode.toLowerCase())
      );
    }

    if (filterData?.length === 0) {
      filterData = data;
    }

    let resultData: IAccountsQueueDetail = filterData[0];
    let queueDetail: QueueAccount | null = null;

    if (params?.workType) {
      const lastIndex = filterData.length - 1;

      switch (params.workType) {
        case 'First':
          resultData = filterData[0];
          break;
        case 'Last':
          resultData = filterData[lastIndex];
          break;
        case 'current_account':
          {
            if (params?.currentAccount) {
              const indexAccount = filterData.findIndex(
                obj =>
                  obj.accNumber.accountNumberUnMask === params?.currentAccount
              );

              if (indexAccount > -1) {
                resultData = filterData[indexAccount];
              }

              const queueDetailData = accountRepository
                .getAll()
                .data.find(obj => obj.queueID === params.queueId);

              if (queueDetailData) {
                queueDetail = queueDetailData;
              }
            }
          }
          break;
        case 'next_account':
          {
            if (params?.currentAccount) {
              const indexAccount = filterData.findIndex(
                obj =>
                  obj.accNumber.accountNumberUnMask === params?.currentAccount
              );

              const nextIndex = indexAccount + 1;

              if (nextIndex > 0 && nextIndex <= lastIndex) {
                resultData = filterData[nextIndex];
              }
            }
          }
          break;
        case 'previous_account':
          {
            if (params?.currentAccount) {
              const indexAccount = filterData.findIndex(
                obj =>
                  obj.accNumber.accountNumberUnMask === params?.currentAccount
              );

              const nextIndex = indexAccount - 1;

              if (nextIndex >= 0) {
                resultData = filterData[nextIndex];
              }
            }
          }
          break;
        default:
          break;
      }
    }

    const mappingDataGroup = accountGroupListData.find(
      item =>
        item.accNumber.accountNumber ===
        resultData.accNumber.accountNumberUnMask
    );

    const accountResult: IAccounts = accountListData.find(
      obj =>
        obj.accNumber.accountNumberUnMask ===
        resultData.accNumber.accountNumberUnMask
    ) as IAccounts;

    let accGroupList = [];

    if (mappingDataGroup) {
      accGroupList = accountGroupListData.filter(
        (obj: IDataGroupList) => obj.groupId === mappingDataGroup?.groupId
      );
    }

    return Promise.resolve({
      data: {
        success: true,
        data: {
          ...accountResult,
          groupId: mappingDataGroup?.groupId,
          numberOfGroup: accGroupList?.length
        },
        detail: queueDetail
      }
    });
  }
};
