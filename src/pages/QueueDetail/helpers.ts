import { orderBy } from 'app/helpers';

export const filterAndOrderAccounts = <T>(
  data: T[],
  sort: string,
  order: string
) => {
  if (!data.length) [];

  return orderBy(data, sort, order as 'asc' | 'desc');
};
