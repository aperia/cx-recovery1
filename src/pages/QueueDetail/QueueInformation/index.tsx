import React from 'react';

import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_TEXT } from 'app/constants';
import { QueueAccount } from 'pages/AccountActivity/types';
import { Button } from 'app/_libraries/_dls/components';

export interface QueueInformationProps {
  queueDetail: QueueAccount;
}

const QueueInformation: React.FC<QueueInformationProps> = ({ queueDetail }) => {
  const { t } = useTranslation();

  return (
    <div className="bg-light-l20 border-bottom">
      <div className="p-24 max-width-lg mx-auto">
        <div className="d-flex align-items-center justify-content-between">
          <h3>
            {queueDetail.queueID} - {queueDetail.queueName}
          </h3>
          <div className="mr-n8">
            <Button variant="outline-primary" size="sm">
              {t(I18N_TEXT.WORK_FROM_BEGINNING)}
            </Button>

            <Button variant="outline-primary" size="sm">
              {t(I18N_TEXT.WORK_FROM_LAST_WORKED)}
            </Button>
          </div>
        </div>
        <div className="mt-16 color-grey">
          <span>
            {t(I18N_TEXT.CREATE_DATE)}: {queueDetail.createdDate}
          </span>
          <span className="mx-8">•</span>
          <span>
            {t(I18N_TEXT.ASSIGN_TO)}: {queueDetail.assignedTo}
          </span>
        </div>
        <div className="row mt-24">
          <div className="col-6 col-lg-4 col-xl-3">
            <span className="fw-500">{t(I18N_TEXT.WORKED_ACCOUNTS)}:</span>
            <span className="pl-4 pr-8 border-right br-light">
              {queueDetail.workedAccounts.numberAccounts}
            </span>
            <span className="pl-8">${queueDetail.workedAccounts.value}</span>
          </div>
          <div className="col-6 col-lg-4 col-xl-3">
            <span className="fw-500">{t(I18N_TEXT.REMAINING_ACCOUNTS)}:</span>
            <span className="pl-4 pr-8 border-right br-light">
              {queueDetail.remainingAccounts.numberAccounts}
            </span>
            <span className="pl-8">${queueDetail.remainingAccounts.value}</span>
          </div>
          <div className="col-6 col-lg-4 col-xl-3 mt-16 mt-lg-0">
            <span className="fw-500">{t(I18N_TEXT.TOTAL_ACCOUNTS)}:</span>
            <span className="pl-4 pr-8 border-right br-light">
              {(
                parseInt(queueDetail.workedAccounts.numberAccounts) +
                parseInt(queueDetail.remainingAccounts.numberAccounts)
              ).toString()}
            </span>
            <span className="pl-8">
              $
              {(
                parseInt(queueDetail.workedAccounts.value) +
                parseInt(queueDetail.remainingAccounts.value)
              ).toString()}
              ,000.00
            </span>
          </div>
        </div>
      </div>
    </div>
  );
};

export default QueueInformation;
