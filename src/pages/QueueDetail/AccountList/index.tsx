import React, { useCallback, useEffect } from 'react';

// components
import If from 'pages/__commons/If';
import Accounts from './Accounts';
import { NoDataGrid, SearchNodata } from 'app/components';
import SearchAccountsQueueDetail from './SearchAccountsQueueDetail';
import OrderByAccountList from './OrderBy';
import SortByAccountList from './SortBy';
import { Pagination } from 'app/_libraries/_dls/components';
// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { queueDetailActions } from '../_redux';
import {
  selectorAccountsQueueDetail,
  selectorIsSearch
} from '../_redux/accountList/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { usePagination } from 'app/hooks/usePagination';

// constants
import { I18N_TEXT, PAGE_SIZE_COMMON } from 'app/constants';

export interface AccountListProps {
  queueID: string;
}

const AccountList: React.FC<AccountListProps> = ({ queueID }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const searchAccounts = useSelector(selectorIsSearch);
  const accounts = useSelector(selectorAccountsQueueDetail);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange,
    setCurrentPage,
    setCurrentPageSize
  } = usePagination(accounts);

  const getAccounts = useCallback(() => {
    dispatch(queueDetailActions.getAccountsQueueDetail({ queueID: queueID }));
    setCurrentPage(1);
    setCurrentPageSize(PAGE_SIZE_COMMON[0]);
  }, [dispatch, setCurrentPage, setCurrentPageSize, queueID]);

  const onClearSearch = useCallback(() => {
    batch(() => {
      dispatch(queueDetailActions.onClearAndReset());
      setCurrentPage(1);
      setCurrentPageSize(PAGE_SIZE_COMMON[0]);
    });
  }, [dispatch, setCurrentPage, setCurrentPageSize]);

  const renderAccounts = () => {
    if (accounts.length) {
      return <Accounts gridData={gridData} queueID={queueID}/>;
    } else {
      if (searchAccounts.length) {
        return (
          <SearchNodata
            text={t(I18N_TEXT.ADJUST_YOUR_SEARCH_FILTER)}
            onClearSearch={onClearSearch}
          />
        );
      } else {
        return <NoDataGrid text={I18N_TEXT.NO_RESULT_FOUND} />;
      }
    }
  };

  useEffect(() => {
    getAccounts();
  }, [searchAccounts, getAccounts]);

  return (
    <div className="p-24 max-width-lg mx-auto">
      <div className="d-flex justify-content-between align-items-center">
        <h4>{t(I18N_TEXT.ACCOUNT_LIST)}</h4>
        <SearchAccountsQueueDetail />
      </div>
      <div className="d-flex justify-content-end align-items-center mt-16 mr-n8">
        <SortByAccountList />
        <OrderByAccountList />
      </div>
      {renderAccounts()}
      <If condition={accounts.length > PAGE_SIZE_COMMON[0]}>
        <div className="pt-16">
          <Pagination
            totalItem={total}
            pageSize={pageSize}
            pageNumber={currentPage}
            pageSizeValue={currentPageSize}
            compact
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        </div>
      </If>
    </div>
  );
};

export default AccountList;
