import React, { useCallback } from 'react';

// component
import OrderByAccounts from 'pages/__commons/AccountList/OrderByAccounts';

// redux
import { useDispatch } from 'react-redux';
import { queueDetailActions } from 'pages/QueueDetail/_redux';
import { selectorOrderBy } from 'pages/QueueDetail/_redux/accountList/selectors';

// hooks
import { useSelector } from 'app/hooks';

interface OrderByAccountListProps {}

const OrderByAccountList: React.FC<OrderByAccountListProps> = () => {
  const dispatch = useDispatch();
  const value: RefDataValue = useSelector(selectorOrderBy);

  const handleChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      dispatch(queueDetailActions.onChangeOrderBy(e.value));
    },
    [dispatch]
  );

  return <OrderByAccounts value={value} handleChange={handleChange} />;
};
export default OrderByAccountList;
