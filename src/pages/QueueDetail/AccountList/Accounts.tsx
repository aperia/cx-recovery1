import React, { useCallback } from 'react';

// components
import { View } from 'app/_libraries/_dof/core';

// redux
import { useDispatch } from 'react-redux';
import { tabActions } from 'pages/__commons/TabBar/_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { formatDataAccounts } from 'pages/AccountGroup/helpers';
import { IAccountsQueueDetail } from 'pages/QueueDetail/types';
import { queueDetailActions } from '../_redux';
import { WorkQueueType } from 'pages/AccountActivity/types';
import { IAccounts } from 'pages/AccountManagement/types';

export interface AccountsProps {
  gridData: IAccountsQueueDetail[];
  queueID: string;
}

const Accounts: React.FC<AccountsProps> = ({ gridData, queueID }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const handleOpenAccountDetail = useCallback(
    (account: IAccounts) => {
      if (!account) return;

      const { id: storeId, accountNumber, accNumber, debtorName } = account;

      dispatch(
        tabActions.addTab({
          title: `${debtorName} - ${accountNumber}`,
          storeId,
          tabType: 'accountDetail',
          iconName: 'file',
          props: {
            storeId,
            debtorName,
            accountNumber
          }
        })
      );

      dispatch(
        queueDetailActions.onChangeBaseAccount({
          storeId: queueID,
          baseAccount: accNumber?.accountNumberUnMask
        })
      );
    },
    [dispatch, queueID]
  );

  const handleOpenDetail = useCallback(
    (account: IAccountsQueueDetail) => {
      const requestData = {
        params: {
          id: account.id,
          queueId: queueID,
          workType: 'First' as WorkQueueType
        },
        callback: handleOpenAccountDetail
      };
      dispatch(queueDetailActions.getAccountToWork(requestData));
    },
    [dispatch, queueID, handleOpenAccountDetail]
  );

  const renderLayout = useCallback(() => {
    return gridData.map((account, index) => {
      return (
        <div
          key={index}
          className="card card-radio cursor-pointer br-radius-8 p-16 mt-16"
          onClick={() => handleOpenDetail(account)}
        >
          <View
            id={`queueDetail-${index}`}
            formKey={`queueDetail-${index}`}
            descriptor="queueDetail"
            value={formatDataAccounts(t, account)}
          />
        </div>
      );
    });
  }, [gridData, t, handleOpenDetail]);

  return <>{renderLayout()} </>;
};
export default Accounts;
