import React, { useCallback, useEffect, useMemo } from 'react';

// components
import {
  AttributeSearch,
  AttributeSearchEvent,
  AttributeSearchRef,
  AttributeSearchValue,
  InputControl
} from 'app/_libraries/_dls/components';

// redux
import { useDispatch, useSelector } from 'react-redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { attrValidations } from 'pages/AccountManagement/AccountList/searchValidation';

// constants
import { ATTRIBUTE_SEARCH_FIELD } from 'pages/AccountManagement/constant';
import {
  I18N_TEXT,
  MAX_LENGTH_3,
  MAX_LENGTH_6,
  MAX_LENGTH_16,
  MAX_LENGTH_40
} from 'app/constants';
import { isEmpty } from 'app/helpers';
import { queueDetailActions } from '../_redux';
import { AttributeSearchFieldModel } from 'pages/AccountManagement/types';
import { keyText } from 'pages/QueueDetail/constants';
import { selectorIsSearch } from '../_redux/accountList/selectors';

export interface SearchAccountsQueueDetailProps {}

const SearchAccountsQueueDetail: React.FC<SearchAccountsQueueDetailProps> =
  () => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const [searchValues, setSearchValues] = React.useState<
      AttributeSearchValue[]
    >([]);

    const originalSearch = useSelector(selectorIsSearch);

    const attributeSearchRef = React.useRef<AttributeSearchRef | null>(null);

    const data = useMemo(() => {
      return [
        {
          component: (props: any) => (
            <InputControl
              placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
              maxLength={MAX_LENGTH_6}
              {...props}
            />
          ),
          description: t(I18N_TEXT.AGENCY_CODE_DESCRIPTION),
          groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
          key: ATTRIBUTE_SEARCH_FIELD.AGENCY_CODE,
          name: t(I18N_TEXT.AGENCY_CODE)
        },
        {
          component: (props: any) => (
            <InputControl
              mode="number"
              placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
              maxLength={MAX_LENGTH_16}
              {...props}
            />
          ),
          description: t(I18N_TEXT.ACCOUNT_NUMBER_DESCRIPTION),
          groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
          key: ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER,
          name: t(I18N_TEXT.ACCOUNT_NUMBER)
        },
        {
          component: (props: any) => (
            <InputControl
              placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
              maxLength={MAX_LENGTH_3}
              {...props}
            />
          ),
          description: t(I18N_TEXT.CALENDAR_CODE_DESCRIPTION),
          groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
          key: ATTRIBUTE_SEARCH_FIELD.CALENDAR_CODE,
          name: t(I18N_TEXT.CALENDAR_CODE)
        },
        {
          component: (props: any) => (
            <InputControl
              placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
              maxLength={MAX_LENGTH_40}
              {...props}
            />
          ),
          description: t(I18N_TEXT.DEBTOR_NAME_DESCRIPTION),
          groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
          key: ATTRIBUTE_SEARCH_FIELD.DEBTOR_NAME,
          name: t(I18N_TEXT.DEBTOR_NAME)
        },
        {
          component: (props: any) => (
            <InputControl
              placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
              maxLength={MAX_LENGTH_3}
              {...props}
            />
          ),
          description: t(I18N_TEXT.REASON_CODE_DESCRIPTION),
          groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
          key: ATTRIBUTE_SEARCH_FIELD.REASON_CODE,
          name: t(I18N_TEXT.REASON_CODE)
        }
      ];
    }, [t]);

    const handleClear = useCallback(() => setSearchValues([]), []);

    useEffect(() => {
      if (!originalSearch.length) {
        handleClear();
      }
    }, [originalSearch, handleClear]);

    const handleChange = useCallback((event: AttributeSearchEvent) => {
      const { value } = event;

      const validate =
        !value.length ||
        value.some(_value => {
          const { key, value: valueSearch } = _value;
          if (
            key === ATTRIBUTE_SEARCH_FIELD.AGENCY_CODE ||
            key === ATTRIBUTE_SEARCH_FIELD.CALENDAR_CODE ||
            key === ATTRIBUTE_SEARCH_FIELD.REASON_CODE
          ) {
            return (
              valueSearch !== keyText.SPACE && valueSearch !== keyText.HYPHEN
            );
          }
          return true;
        });
      validate && setSearchValues(value);
    }, []);

    const handleSearch = useCallback(
      (event: AttributeSearchEvent, error?: Record<string, string>) => {
        if (!isEmpty(error)) return;
        dispatch(queueDetailActions.onChangeSearchValue(event.value));
      },
      [dispatch]
    );

    return (
      <AttributeSearch
        small
        orderBy="asc"
        ref={attributeSearchRef}
        data={data}
        value={searchValues}
        // placeholder={renderPlaceHolder()}
        // noCombineFields={noCombineFields}
        validator={(data: AttributeSearchFieldModel) =>
          attrValidations(data, t)
        }
        onClear={handleClear}
        onChange={handleChange}
        onSearch={handleSearch}
        clearTooltipProps={{
          element: t(I18N_TEXT.CLEAR_ALL_CRITERIA)
        }}
      />
    );
  };

export default SearchAccountsQueueDetail;
