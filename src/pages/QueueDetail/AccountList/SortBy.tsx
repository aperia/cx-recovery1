import React, { useCallback } from 'react';

// component
import SortByAccounts from 'pages/__commons/AccountList/SortByAccounts';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { selectorSortBy } from 'pages/QueueDetail/_redux/accountList/selectors';
import { queueDetailActions } from 'pages/QueueDetail/_redux';

// constants
import { dropdownSortQueueDetailAccounts } from '../constants';

// helper

interface SortByAccountListProps {}

const SortByAccountList: React.FC<SortByAccountListProps> = () => {
  const dispatch = useDispatch();
  const value: RefDataValue = useSelector(selectorSortBy);

  const handleChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      dispatch(queueDetailActions.onChangeSortAccounts(e.value));
    },
    [dispatch]
  );

  return (
    <SortByAccounts
      value={value}
      dropdownSortAccounts={dropdownSortQueueDetailAccounts}
      handleChange={handleChange}
    />
  );
};
export default SortByAccountList;
