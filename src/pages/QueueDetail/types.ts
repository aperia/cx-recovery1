import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { WorkQueueType } from 'pages/AccountActivity/types';
import { IAccounts } from 'pages/AccountManagement/types';
import { orderByDefault, sortByDefault } from './constants';

export interface IAccountsQueueDetail {
  id: string;
  debtorName: string;
  queueIDs: string[];
  accountNumber: string;
  accNumber: {
    accountNumber: string;
    accountNumberUnMask: string;
  };
  status: string;
  agencyCode: string;
  reasonCode: string;
  calendarCode: string;
  collectorCode: string;
  statusCode: string;
  accountType: string;
  accountClass: string;
  specialCondition: Array<'B' | 'D' | 'F' | 'L' | 'X'>;
}

export interface AccountListQueueDetailReducer {
  sortBy: RefDataValue;
  orderBy: RefDataValue;
  searchValue: AttributeSearchValue[];
  data: IAccountsQueueDetail[];
  isLoading: boolean;
  isError: boolean;
}

export interface WorkQueueReducer {
  queueId?: string;
  calendarCode?: string;
  baseAccount?: string;
  currentAction?: string;
  data?: IAccounts;
  isLoading: boolean;
  isError: boolean;
}

export const AccountListQueueDetailInitialState: AccountListQueueDetailReducer =
  {
    sortBy: sortByDefault,
    orderBy: orderByDefault,
    searchValue: [],
    data: [],
    isLoading: false,
    isError: false
  };

export const WorkQueueInitialState: WorkQueueReducer = {
  isLoading: false,
  isError: false
};

export interface InitialStateQueueDetail {
  accountList: AccountListQueueDetailReducer;
  workQueue: {
    [storeId: string]: WorkQueueReducer;
  };
}

export interface ISearchWorkQueuePayload {
  id?:string;
  queueId: string;
  workType?: WorkQueueType;
  calendarCode?: string;
  currentAccount?: string;
}
