import { createSlice } from '@reduxjs/toolkit';

// accountList actions
import {
  onChangeSortAccounts,
  onChangeOrderBy,
  onChangeSearchValue,
  onClearAndReset,
  onChangeBaseAccount,
  onChangeCurrentAction
} from './accountList/actions';

import {
  InitialStateQueueDetail,
  AccountListQueueDetailInitialState,
} from '../types';
import {
  getAccountsQueueDetail,
  getAccountsQueueDetailBuilder
} from './accountList/getAccountsQueueDetail';
import {
  getAccountToWork,
  getAccountToWorkBuilder
} from './accountList/getAccountToWork';

const initialState: InitialStateQueueDetail = {
  accountList: AccountListQueueDetailInitialState,
  workQueue: {}
};

export const { actions, reducer } = createSlice({
  name: 'QueueDetail',
  initialState,
  reducers: {
    // accountList actions
    onChangeSortAccounts,
    onChangeOrderBy,
    onChangeSearchValue,
    onClearAndReset,
    onChangeBaseAccount,
    onChangeCurrentAction
  },
  extraReducers: builder => {
    getAccountsQueueDetailBuilder(builder);
    getAccountToWorkBuilder(builder);
  }
});
const combineActions = {
  ...actions,
  getAccountsQueueDetail,
  getAccountToWork
};

export { combineActions as queueDetailActions };
