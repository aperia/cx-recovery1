import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { set } from 'app/helpers';
import queueDetailServices from 'pages/QueueDetail/queueDetailServices';
import {
  IAccountsQueueDetail,
  InitialStateQueueDetail
} from 'pages/QueueDetail/types';

export const getAccountsQueueDetail = createAsyncThunk<
  { data: IAccountsQueueDetail[] },
  { queueID: string },
  ThunkAPIConfig
>('getAccountsQueueDetail', async (args, { getState, rejectWithValue }) => {
  try {
    const { queueID } = args;
    const { searchValue } = getState().queueDetail.accountList;
    const { data } = await queueDetailServices.getAccountList(
      queueID,
      searchValue
    );

    return { data: data.data };
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getAccountsQueueDetailBuilder = (
  builder: ActionReducerMapBuilder<InitialStateQueueDetail>
) => {
  const { pending, fulfilled, rejected } = getAccountsQueueDetail;
  builder
    .addCase(pending, (draftState, action) => {
      set(draftState, ['accountList', 'isLoading'], true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { data } = action.payload;

      set(draftState, ['accountList', 'isLoading'], false);
      set(draftState, ['accountList', 'data'], data);
    })
    .addCase(rejected, (draftState, action) => {
      set(draftState, ['accountList', 'isLoading'], false);
    });
};
