import { PayloadAction } from '@reduxjs/toolkit';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { InitialStateQueueDetail } from 'pages/QueueDetail/types';

import { orderByDefault, sortByDefault } from 'pages/QueueDetail/constants';
import { set } from 'app/helpers';

export const onChangeSortAccounts = (
  draftState: InitialStateQueueDetail,
  action: PayloadAction<RefDataValue>
) => {
  draftState.accountList.sortBy = action.payload;
};

export const onChangeOrderBy = (
  draftState: InitialStateQueueDetail,
  action: PayloadAction<RefDataValue>
) => {
  draftState.accountList.orderBy = action.payload;
};

export const onChangeSearchValue = (
  draftState: InitialStateQueueDetail,
  action: PayloadAction<AttributeSearchValue[]>
) => {
  draftState.accountList.searchValue = action.payload;
};

export const onChangeBaseAccount = (
  draftState: InitialStateQueueDetail,
  action: PayloadAction<{
    storeId: string;
    baseAccount?: string;
  }>
) => {
  set(
    draftState,
    ['workQueue', action.payload.storeId, 'baseAccount'],
    action.payload?.baseAccount
  );
};

export const onChangeCurrentAction = (
  draftState: InitialStateQueueDetail,
  action: PayloadAction<{
    storeId: string;
    action?: string;
  }>
) => {
  set(
    draftState,
    ['workQueue', action.payload.storeId, 'currentAction'],
    action.payload?.action
  );
};

export const onClearAndReset = (draftState: InitialStateQueueDetail) => {
  draftState.accountList.searchValue = [];
  draftState.accountList.orderBy = orderByDefault;
  draftState.accountList.sortBy = sortByDefault;
};
