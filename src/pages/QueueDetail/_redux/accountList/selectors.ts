import { createSelector } from '@reduxjs/toolkit';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { IAccounts } from 'pages/AccountManagement/types';
import { filterAndOrderAccounts } from 'pages/QueueDetail/helpers';
import { IAccountsQueueDetail } from 'pages/QueueDetail/types';

const getAccountsQueueDetail = (states: RootState): IAccountsQueueDetail[] =>
  states.queueDetail.accountList.data;
const getOrderBy = (states: RootState): RefDataValue =>
  states.queueDetail.accountList.orderBy;

const getSortBy = (states: RootState): RefDataValue =>
  states.queueDetail.accountList.sortBy;

export const selectorOrderBy = createSelector(
  getOrderBy,
  (data: RefDataValue) => data
);

export const selectorSortBy = createSelector(
  getSortBy,
  (data: RefDataValue) => data
);

export const selectorAccountsQueueDetail = createSelector(
  getAccountsQueueDetail,
  getSortBy,
  getOrderBy,
  (data: IAccountsQueueDetail[], sortBy: RefDataValue, orderBy: RefDataValue) =>
    filterAndOrderAccounts(data, sortBy.value, orderBy.value)
);

export const selectorIsSearch = createSelector(
  [(states: RootState) => states.queueDetail.accountList.searchValue],
  (data: AttributeSearchValue[]) => data
);

export const selectorCurrentAccountQueue = createSelector(
  [
    (states: RootState, storeId: string) =>
      states.queueDetail.workQueue[storeId]?.data
  ],
  (data?: IAccounts) => data
);

export const selectorCurrentQueueId = createSelector(
  [
    (states: RootState, storeId: string) =>
      states.queueDetail.workQueue[storeId]?.queueId
  ],
  (data?: string) => data
);

export const selectorCalendarCode = createSelector(
  [
    (states: RootState, storeId: string) =>
      states.queueDetail.workQueue[storeId]?.calendarCode
  ],
  (data?: string) => data
);

export const selectorBaseAccount = createSelector(
  [
    (states: RootState, storeId: string) =>
      states.queueDetail.workQueue[storeId]?.baseAccount
  ],
  (data?: string) => data
);

export const selectorCurrentAction = createSelector(
  [
    (states: RootState, storeId: string) =>
      states.queueDetail.workQueue[storeId]?.currentAction
  ],
  (data?: string) => data
);
