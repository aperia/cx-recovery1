import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { set, isFunction } from 'app/helpers';
import { QueueAccount } from 'pages/AccountActivity/types';
import { IAccounts } from 'pages/AccountManagement/types';

import queueDetailServices from 'pages/QueueDetail/queueDetailServices';
import {
  InitialStateQueueDetail,
  ISearchWorkQueuePayload
} from 'pages/QueueDetail/types';

interface IGetAccountToWork {
  params: ISearchWorkQueuePayload;
  storeId?: string;
  callback?: (data: IAccounts, detail?: QueueAccount | null) => void;
}

export const getAccountToWork = createAsyncThunk<
  { data: IAccounts; storeId: string },
  IGetAccountToWork,
  ThunkAPIConfig
>('getAccountToWork', async (args, { rejectWithValue }) => {
  try {
    const { data } = await queueDetailServices.getAccountToWork(args.params);

    if (isFunction(args?.callback)) {
      args.callback(data.data, data?.detail);
    }

    return { data: data.data, storeId: args?.storeId || data.data?.id };
  } catch (error: any) {
    return rejectWithValue(error);
  }
});

export const getAccountToWorkBuilder = (
  builder: ActionReducerMapBuilder<InitialStateQueueDetail>
) => {
  const { fulfilled } = getAccountToWork;
  builder.addCase(fulfilled, (draftState, action) => {
    const { data, storeId } = action.payload;
    const { params } = action.meta.arg;

    set(draftState, ['workQueue', storeId, 'data'], data);
    set(draftState, ['workQueue', storeId, 'queueId'], params.queueId);
    set(
      draftState,
      ['workQueue', storeId, 'calendarCode'],
      params?.calendarCode
    );
  });
};
