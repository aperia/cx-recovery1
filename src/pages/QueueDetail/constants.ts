import { ordersDropdown } from 'app/constants';

export const dropdownSortQueueDetailAccounts = [
  {
    description: 'Agency Code',
    value: 'agencyCode'
  },
  { description: 'Calendar Code', value: 'calendarCode' },
  { description: 'Current Balance', value: 'currentBalance' },
  { description: 'Debtor Name', value: 'debtorName' },
  { description: 'Last Work Date', value: 'lastWorkDate' },
  {
    description: 'Reason Code',
    value: 'reasonCode'
  }
];

export const sortByDefault = dropdownSortQueueDetailAccounts[4];

export const orderByDefault = ordersDropdown[0];

export const keyText = {
  SPACE: ' ',
  HYPHEN: '_'
};
