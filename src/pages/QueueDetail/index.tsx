import React from 'react';

// components
import AccountList from './AccountList';
import QueueInformation from './QueueInformation';

// types
import { QueueAccount } from 'pages/AccountActivity/types';
import { SimpleBar } from 'app/_libraries/_dls/components';

export interface QueueDetailProps {
  queueDetail: QueueAccount;
}

const QueueDetail: React.FC<QueueDetailProps> = ({ queueDetail }) => {
  return (
    <SimpleBar>
      <QueueInformation queueDetail={queueDetail} />
      <AccountList queueID={queueDetail.queueID} />
    </SimpleBar>
  );
};

export default QueueDetail;
