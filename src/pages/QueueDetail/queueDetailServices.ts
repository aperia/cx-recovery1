import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { ISearchWorkQueuePayload } from './types';
import { queueDetailController } from './__mock__/queueDetailController';

export const queueDetailServices = {
  getAccountList(queueID: string, searchValue: AttributeSearchValue[]) {
    return queueDetailController.getAccountList(queueID, searchValue);
  },
  getAccountToWork(params: ISearchWorkQueuePayload) {
    return queueDetailController.getAccountToWork(params);
  }
};
export default queueDetailServices;
