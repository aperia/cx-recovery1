import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState
} from 'react';

// components
import { SimpleBar } from 'app/_libraries/_dls/components';
import ExtraStaticTabs from 'app/_libraries/_dls/components/VerticalTabs';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { selectorActiveTab } from './_redux/manualAssignRecall/selectors';
import { accountManagementActions } from './_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { I18N_TEXT } from 'app/constants';
import { isNull } from 'app/helpers';
import classNames from 'classnames';
import { tabsAccountManagement, componentAccountManagement } from './constant';
import { AccountManagementTabID } from './types';
import { ManualAssignRecallReqForm } from './ManualAssignRecall/Form';

export interface AccountManagementProps {}

const AccountManagement: React.FC<AccountManagementProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [collapsed, setCollapsed] = useState(false);
  const [collapsing, setCollapsing] = useState(false);
  const divRef = useRef<HTMLDivElement | null>(null);
  const selectorTab = useSelector(selectorActiveTab);

  const handleToggleCollapsed = useCallback(isCollapsed => {
    setCollapsed(isCollapsed);
    setCollapsing(true);
    const collapsingTimeout = setTimeout(() => {
      setCollapsing(false);
    }, 500);
    return () => clearTimeout(collapsingTimeout);
  }, []);

  const handleOnSelect = (eventKey: string | null) => {
    if (selectorTab === eventKey) return;
    if (isNull(eventKey)) return;
    dispatch(
      accountManagementActions.onChangeTab(eventKey as AccountManagementTabID)
    );
  };

  useEffect(() => {
    return () => {
      dispatch(accountManagementActions.onChangeTab('accountMaintenance'));
    };
  }, [dispatch]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100% - 81px)`);
  }, []);

  return (
    <>
      <div className="p-24 bg-light-l20 border-bottom">
        <h3>{t(I18N_TEXT.ACCOUNT_MANAGEMENT)}</h3>
      </div>
      <ExtraStaticTabs
        activeKey={selectorTab}
        onToggleCollapsed={handleToggleCollapsed}
        onSelect={handleOnSelect}
        unmountOnExit
        mountOnEnter
        collapsed={collapsed}
        className="scrolled-nav"
        expandMessage={t(I18N_TEXT.EXPAND)}
        collapseMessage={t(I18N_TEXT.COLLAPSE)}
      >
        <div
          ref={divRef}
          className={classNames('tab-left', {
            'is-collapsed': collapsed,
            'is-collapsing': collapsing
          })}
        >
          <SimpleBar>
            <ExtraStaticTabs.Nav collapsed={collapsed}>
              {tabsAccountManagement.map(({ id, title }) => (
                <ExtraStaticTabs.Nav.Item key={id}>
                  <ExtraStaticTabs.Nav.Link eventKey={id}>
                    {t(title)}
                  </ExtraStaticTabs.Nav.Link>
                </ExtraStaticTabs.Nav.Item>
              ))}
            </ExtraStaticTabs.Nav>
          </SimpleBar>
        </div>
        <ExtraStaticTabs.Content>
          {componentAccountManagement.map(({ id, Component }) => (
            <ExtraStaticTabs.Pane eventKey={id} key={id}>
              <Component />
            </ExtraStaticTabs.Pane>
          ))}
        </ExtraStaticTabs.Content>
      </ExtraStaticTabs>
      <ManualAssignRecallReqForm
        isSearch={selectorTab === 'assignRecallList' ? true : false}
      />
    </>
  );
};

export default AccountManagement;
