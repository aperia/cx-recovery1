import React from 'react';

// redux
import { useDispatch } from 'react-redux';
import { tabActions } from 'pages/__commons/TabBar/_redux';

// helpers
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { I18N_TEXT } from 'app/constants';

export interface OpenAccountManagementProps {}

const OpenAccountManagement: React.FC<OpenAccountManagementProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const handleClick = () => {
    dispatch(
      tabActions.addTab({
        title: t(I18N_TEXT.ACCOUNT_MANAGEMENT),
        storeId: 'ACCOUNT_MANAGEMENT',
        tabType: 'accountManagement',
        iconName: 'file'
      })
    );
  };

  return (
    <span
      className="link-header"
      onClick={handleClick}
      data-testid={genAmtId('header_account_management', 'title', '')}
    >
      {t(I18N_TEXT.ACCOUNT_MANAGEMENT)}
    </span>
  );
};

export default OpenAccountManagement;
