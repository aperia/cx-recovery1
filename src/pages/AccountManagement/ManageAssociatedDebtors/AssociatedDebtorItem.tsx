import React, { useMemo } from 'react';
import { useDispatch } from 'react-redux';

// components
import { Button, Icon, Popover } from 'app/_libraries/_dls/components';
import UnlinkModalBody from './UnlinkModalBody';

// redux
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useRefData from 'app/hooks/useRefData';

// constants & types
import { I18N_TEXT } from 'app/constants';
import { RootModalOptions } from 'pages/__commons/RootModal/types';
import { ILinkDebtor } from 'pages/AccountManagement/types';
import {
  convertValueDescription,
  getRefDataFromCode
} from 'pages/__commons/helpers';
import { RelationType } from 'app/constants/enums';

interface AssociatedDebtorItemProps extends ILinkDebtor {
  currentPage: number;
}

export const AssociatedDebtorItem: React.FC<AssociatedDebtorItemProps> =
  props => {
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const relationRefData = useRefData('relation');
    const ecoaRefData = useRefData('ecoa');
    const sendCorrespondenceRefData = useRefData('sendCorrespondence');
    const ciicdRefData = useRefData('ciicd');

    const {
      debtorName,
      relation,
      ecoa,
      sendCorrespondence,
      piid,
      ciicd,
      last4SSN,
      id,
      currentPage,
      isBankruptcy,
      isDecease
    } = props;

    const convertData = useMemo(() => {
      return {
        ...props,
        relation: convertValueDescription(
          getRefDataFromCode(relationRefData, relation)
        ),
        ecoa: convertValueDescription(getRefDataFromCode(ecoaRefData, ecoa)),
        sendCorrespondence: convertValueDescription(
          getRefDataFromCode(sendCorrespondenceRefData, sendCorrespondence)
        ),
        ciicd: convertValueDescription(getRefDataFromCode(ciicdRefData, ciicd))
      };
    }, [
      props,
      relationRefData,
      relation,
      ecoaRefData,
      ecoa,
      sendCorrespondenceRefData,
      sendCorrespondence,
      ciicdRefData,
      ciicd
    ]);

    const handleUnlink = () => {
      dispatch(
        accountManagementActions.deleteAssociatedsDebtor({
          id: id || '',
          currentPage
        })
      );
    };

    const handleEdit = () => {
      dispatch(
        accountManagementActions.triggerEditLinkDebtors({
          debtorName,
          relation,
          ecoa,
          sendCorrespondence,
          last4SSN,
          id,
          piid,
          ciicd,
          isBankruptcy,
          isDecease
        })
      );
    };

    const showModalUnlink = () => {
      const options: RootModalOptions = {
        autoClose: false,
        size: 'xs',
        title: t(I18N_TEXT.CONFIRM_UNLINK_DEBTOR),
        body: (
          <UnlinkModalBody
            item={props}
            messageBody={t(I18N_TEXT.CONFIRM_UNLINK_MESSAGE)}
          />
        ),
        onConfirm: handleUnlink,
        btnConfirmText: t(I18N_TEXT.UNLINK),
        onCancel: () => dispatch(rootModalActions.close())
      };
      dispatch(rootModalActions.open(options));
    };

    return (
      <div className="card br-radius-8 mt-16 p-16 card-associated-debtors">
        <div className="row align-items-center">
          <div className="col-lg-3 col-md-3">
            <span className="fw-500">{debtorName}</span>
          </div>
          <div className="col-lg-5 col-md-6">
            <span>
              <span className="color-grey mr-4">{t(I18N_TEXT.RELATION)}:</span>
              {convertData.relation}
            </span>
          </div>
          <div className="col-lg-2 col-md-9 offset-lg-0 offset-md-3 order-lg-1 order-md-6 mt-lg-0 mt-md-4 text-nowrap">
            <span className="color-grey mr-4">{t(I18N_TEXT.PIID)}:</span>
            {piid}
          </div>
          <div className="col-lg-2 col-md-3 order-lg-1 order-md-1 text-right">
            {relation !== RelationType[3] ? (
              <Button
                size="sm"
                variant="outline-danger"
                onClick={showModalUnlink}
              >
                {t(I18N_TEXT.UNLINK)}
              </Button>
            ) : (
              <div></div>
            )}

            <Button
              size="sm"
              className="mr-n8"
              variant="outline-primary"
              onClick={handleEdit}
            >
              {t(I18N_TEXT.EDIT)}
            </Button>
          </div>
          <div className="col-lg-3 col-md-3 order-lg-1 order-md-2">
            <span className="color-grey mr-4">{t(I18N_TEXT.LAST_4_SSN)}:</span>
            {last4SSN}
          </div>
          <div className="col-lg-5 col-md-9 order-lg-1 order-md-3">
            <span className="color-grey mr-4">{t(I18N_TEXT.ECOA)}:</span>
            {convertData.ecoa}
          </div>
          <div className="col-lg-4 col-md-9 offset-lg-0 offset-md-3 order-lg-1 order-md-7 mt-lg-0 mt-md-4">
            <div className="d-flex">
              <span className="color-grey mr-4">{t(I18N_TEXT.CIICD)}:</span>
              <div className="d-flex align-items-center">
                <span className="mr-8">{ciicd}</span>
                {ciicd && (
                  <Popover
                    placement="top"
                    size="auto"
                    element={convertData.ciicd}
                  >
                    <Button
                      size="sm"
                      variant="icon-secondary"
                      className="d-flex justify-content-center align-items-center"
                    >
                      <Icon
                        name="information"
                        size="4x"
                        className="color-grey-l16"
                      />
                    </Button>
                  </Popover>
                )}
              </div>
            </div>
          </div>
          <div className="col-lg-3 col-md-3 order-lg-1 order-md-4 mt-4">
            {isBankruptcy && (
              <span className="tag tag-orange">{t(I18N_TEXT.BANKRUPTCY)}</span>
            )}
            {isDecease && (
              <span className="tag tag-red">{t(I18N_TEXT.DECEASE)}</span>
            )}
          </div>
          <div className="col-lg-9 col-md-9 order-lg-1 order-md-5 mt-4">
            <span className="color-grey mr-4">
              {t(I18N_TEXT.SEND_CORRESPONDENCE)}:
            </span>
            {convertData.sendCorrespondence}
          </div>
        </div>
      </div>
    );
  };
