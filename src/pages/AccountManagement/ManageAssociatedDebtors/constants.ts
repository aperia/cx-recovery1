import { genPIID } from "../helpers";

export const FORM_NAMES = {
  relation: {
    name: 'relation',
    label: 'txt_relation'
  },
  debtorName: {
    name: 'debtorName',
    label: 'txt_debtor'
  },
  ecoa: {
    name: 'ecoa',
    label: 'txt_ecoa'
  },
  sendCorrespondence: {
    name: 'sendCorrespondence',
    label: 'txt_send_correspondence'
  },
  piid: {
    name: 'piid',
    label: 'txt_piid'
  },
  ciicd: {
    name: 'ciicd',
    label: 'txt_ciicd'
  }
};
export const DEFAULT_PRIMARY_DEBTOR = {
  debtorName: '',
  relation: 'A',
  ecoa: '',
  last4SSN: '',
  sendCorrespondence: '',
  piid: genPIID(),
  ciicd: '',
  isBankruptcy: false,
  isDecease: false
};
