import React from 'react';

import { useTranslation } from 'app/_libraries/_dls/hooks';

import { I18N_TEXT } from 'app/constants';
import { mappingRelationValue } from './helpers';

interface IReplaceDebtorModalProps {
  accNumber: string;
  selectedRelation: string;
}

const ReplaceDebtorModal: React.FC<IReplaceDebtorModalProps> = ({
  accNumber,
  selectedRelation
}) => {
  const { t } = useTranslation();

  return (
    <p
      dangerouslySetInnerHTML={{
        __html: t(I18N_TEXT.REPLACE_EXIST_RELATION_MESSAGE, {
          selectedRelation: mappingRelationValue(selectedRelation),
          customHtml: `<span class="fw-500"> Account Number: ${accNumber} </span>`,
          interpolation: { escapeValue: false }
        })
      }}
    ></p>
  );
};

export default ReplaceDebtorModal;
