import React from 'react';

// components
import { View } from 'app/_libraries/_dof/core';
import ApiErrorDetail from 'pages/__commons/ApiErrorDetail';

// constants & types
import { IAccounts } from '../types';
import { getLast4SSN } from './helpers';

export interface AccDetailSnapshotProps {
  accData: IAccounts;
}

export const AccDetailSnapshotDebtor: React.FC<AccDetailSnapshotProps> = ({
  accData
}) => {
  return (
    <div className="bg-light-l20 border-bottom">
      <div className="p-24 max-width-lg mx-auto">
        <ApiErrorDetail forAction={['']} className="mb-24" />
        <h4 className="mb-16">{accData.debtorName}</h4>
        <View
          formKey={`accountAccountList-${accData?.id}`}
          descriptor="accountAccountList"
          value={{
            ...accData,
            last4SSN: getLast4SSN(accData?.ssn || ''),
          }}
        />
      </div>
    </div>
  );
};
