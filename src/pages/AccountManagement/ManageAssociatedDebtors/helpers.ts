import { RelationType } from 'app/constants/enums';
import { ILinkDebtor } from '../types';

export const isExistWithRelation = (
  listLinkDebtors: ILinkDebtor[],
  selectedRelation: string
) => {
  return listLinkDebtors.some(
    (item: ILinkDebtor) =>
      item.relation === selectedRelation &&
      (item.relation === RelationType[0] || item.relation === RelationType[1])
  );
};

export const mappingRelationValue = (relationValue?: string) => {
  switch (relationValue) {
    case RelationType[0]:
      return 'Legal Relationship';

    case RelationType[1]:
      return 'Co-Maker';

    case RelationType[2]:
      return 'Authorized User';

    case RelationType[3]:
      return 'Primary Debtor';

    default:
      return '';
  }
};

export const getLast4SSN = (ssn: string) => {
  if (!ssn) return '';
  return ssn.split('-')[2];
};

export const isExistPrimaryDebtor = (listLinkDebtors: ILinkDebtor[]) => {
  return listLinkDebtors?.some(
    (item: ILinkDebtor) => item.relation === RelationType[3]
  );
};
