import React from 'react';

import { useTranslation } from 'app/_libraries/_dls/hooks';

import { ILinkDebtor } from 'pages/AccountManagement/types';
import { I18N_TEXT } from 'app/constants';

interface IUnlinkModalBodyProps {
  item: ILinkDebtor;
  messageBody?: string;
}

const UnlinkModalBody: React.FC<IUnlinkModalBodyProps> = ({
  item,
  messageBody
}) => {
  const { t } = useTranslation();
  return (
    <>
      <p>{messageBody}</p>
      <p className="mt-16">
        {t(I18N_TEXT.DEBTOR)}:{' '}
        <span className="fw-500">{`${item.debtorName}`}</span>
      </p>
    </>
  );
};

export default UnlinkModalBody;
