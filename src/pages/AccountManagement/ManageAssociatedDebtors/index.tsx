import React, { useEffect } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';

// Components
import {
  Button, Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import AssociatedDebtors from '../components/AssociatedDebtors';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import {
  selectedLinkDebtorsData
} from 'pages/AccountManagement/_redux/associatedDebtor/selectors';

// constants & types
import { ILinkDebtor } from '../types';
import { I18N_TEXT } from 'app/constants';

// helpers
import { isEmpty } from 'app/helpers';

export interface ManageAssociatedDebtorsProps {
  open: boolean;
  isSearch?: boolean;
  onHide: () => void;
}

export const ManageAssociatedDebtors: React.FC<ManageAssociatedDebtorsProps> =
  ({ open, onHide }) => {
    const { t } = useTranslation();
    const dispatch = useDispatch();
    const listLinkDebtors: ILinkDebtor[] = useSelector(selectedLinkDebtorsData);

    useEffect(() => {
      batch(() => {
        dispatch(accountManagementActions.getDebtors({}));
        dispatch(
          accountManagementActions.getAssociatedDebtors({
            page: 1
          })
        );
      });
    }, [dispatch]);

    return (
      <Modal full show={open} loading={false} enforceFocus={false}>
        <ModalHeader
          border
          onHide={onHide}
          closeButton={!isEmpty(listLinkDebtors)}
          className="border-bottom"
        >
          <ModalTitle className="d-flex align-items-center">
            {t(I18N_TEXT.MANAGE_ASSOCIATED_DEBTORS)}
          </ModalTitle>
        </ModalHeader>
        <ModalBody noPadding><AssociatedDebtors fromAddAccount /></ModalBody>

        {!isEmpty(listLinkDebtors) && (
          <ModalFooter border>
            <Button variant="secondary" onClick={onHide}>
              {t(I18N_TEXT.CLOSE)}
            </Button>
          </ModalFooter>
        )}
      </Modal>
    );
  };
