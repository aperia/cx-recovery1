import React, { useCallback } from 'react';
import * as Yup from 'yup';
import { batch, useDispatch, useSelector } from 'react-redux';

// components
import ReplaceDebtorModal from '../ReplaceDebtorModal';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import {
  selectedFormActionLinkDebtors,
  selectedFormLinkDebtorsOpen,
  selectedFormValuesLinkDebtors,
  selectedLinkDebtorsCurrentPage,
  selectorAllLinkDebtor
} from 'pages/AccountManagement/_redux/associatedDebtor/selectors';
import { selectorDebtors } from 'pages/AccountManagement/_redux/debtorMaintenance/selectors';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

//constants & types
import { Debtors, ILinkDebtor } from 'pages/AccountManagement/types';
import { FORM_NAMES } from '../constants';
import { FormAction } from '../LinkDebtorModal';
import { FormType, RelationType } from 'app/constants/enums';
import { I18N_TEXT } from 'app/constants';
import { isExistWithRelation } from '../helpers';
import { RootModalOptions } from 'pages/__commons/RootModal/types';

import { isEqual } from 'app/helpers';


export const useLinkDebtorForm = ({
  accNumber,
  pageSize
}: {
  accNumber: string;
  pageSize: number;
}) => {
  const { t } = useTranslation();

  const dispatch = useDispatch();

  const formOpen: boolean = useSelector(selectedFormLinkDebtorsOpen);
  const formValues: ILinkDebtor = useSelector(selectedFormValuesLinkDebtors);
  const formAction: FormAction = useSelector(selectedFormActionLinkDebtors);
  const currentPage: number = useSelector(selectedLinkDebtorsCurrentPage);

  const allLinkDebtor: ILinkDebtor[] = useSelector(selectorAllLinkDebtor);

  const debtorList: Debtors[] = useSelector(selectorDebtors);

  const validationSchema = Yup.object().shape({
    [FORM_NAMES.relation.name]: Yup.string().required(
      I18N_TEXT.RELATION_IS_REQUIRED
    ),
    [FORM_NAMES.debtorName.name]: Yup.string().required(
      I18N_TEXT.DEBTOR_NAME_IS_REQUIRED
    ),
    [FORM_NAMES.ecoa.name]: Yup.string().required(I18N_TEXT.ECOA_IS_REQUIRED),
    [FORM_NAMES.sendCorrespondence.name]: Yup.string().required(
      I18N_TEXT.SEND_CORRESPONDENCE_IS_REQUIRED
    )
  });

  const onSubmit = (values: ILinkDebtor, linkAnotherDebtor: boolean) => {
    const isExistRelation =
      isExistWithRelation(allLinkDebtor, values.relation) &&
      (values.relation === RelationType[0] ||
        values.relation === RelationType[1]);

    if (formAction === FormType[0]) {
      if (isExistRelation) {
        dispatch(accountManagementActions.toggleFormLinkDebtors());
        showModalReplace(values, false, linkAnotherDebtor);
        return;
      }
      dispatch(
        accountManagementActions.addAssociatedDebtor({
          newAssociatedDebtor: values,
          debtorList,
          currentPage,
          pageSize,
          linkAnotherDebtor
        })
      );
    }
    if (formAction === FormType[1]) {
      if (isExistRelation) {
        dispatch(accountManagementActions.toggleFormLinkDebtors());
        if (!isEqual({ ...values }, { ...formValues })) {
          showModalReplace(values, true);
        } else {
          dispatch(
            accountManagementActions.editAssociatedDebtor({
              id: values.id || '',
              dataChange: values,
              debtorList,
              currentPage,
              pageSize,
              isExistRelation: true,
              isChange: false
            })
          );
        }
        return;
      }

      dispatch(
        accountManagementActions.editAssociatedDebtor({
          id: values.id || '',
          dataChange: values,
          debtorList,
          currentPage,
          pageSize
        })
      );
    }
  };

  const onCancel = () => {
    dispatch(accountManagementActions.removeStoreLinkDebtors());
  };

  const onToggleForm = () => {
    dispatch(accountManagementActions.toggleFormLinkDebtors());
  };

  const showModalReplace = useCallback(
    (values: ILinkDebtor, isEdit = false, linkAnotherDebtor = false) => {
      const options: RootModalOptions = {
        autoClose: false,
        size: 'sm',
        className: 'modal-debtor',
        title: t(I18N_TEXT.CONFIRM_REPLACE_EXIST_RELATION),
        body: (
          <ReplaceDebtorModal
            accNumber={accNumber}
            selectedRelation={values.relation}
          />
        ),
        onConfirm: () =>
          batch(() => {
            if (!isEdit) {
              dispatch(
                accountManagementActions.addAssociatedDebtor({
                  newAssociatedDebtor: values,
                  debtorList,
                  currentPage,
                  pageSize,
                  isExistRelation: true,
                  linkAnotherDebtor
                })
              );
            } else {
              dispatch(
                accountManagementActions.editAssociatedDebtor({
                  id: values.id || '',
                  dataChange: values,
                  debtorList,
                  currentPage,
                  pageSize,
                  isExistRelation: true
                })
              );
            }
            dispatch(rootModalActions.close());
          }),
        btnConfirmText: t(I18N_TEXT.REPLACE),
        onCancel: () => dispatch(rootModalActions.close())
      };
      dispatch(rootModalActions.open(options));
    },
    [dispatch, t, debtorList, currentPage, accNumber, pageSize]
  );

  return {
    open: formOpen,
    values: formValues,
    validationSchema,
    onSubmit,
    onCancel,
    onToggleForm
  };
};
