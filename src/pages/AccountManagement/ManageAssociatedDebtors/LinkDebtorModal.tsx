import React, { useEffect, useMemo } from 'react';
import { FormikProvider, useFormik } from 'formik';
import { useDispatch } from 'react-redux';

// components
import {
  ComboBoxControl,
  DropdownControl,
  TextBoxControl
} from 'app/components';
import {
  CheckBox,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import ApiErrorDetail from '../../__commons/ApiErrorDetail';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { Debtors, ILinkDebtor } from 'pages/AccountManagement/types';
import useRefData from 'app/hooks/useRefData';

// redux
import { addAssociatedDebtor } from '../_redux/associatedDebtor/addAssociatedDebtor';
import { editAssociatedDebtor } from '../_redux/associatedDebtor/editAssociatedDebtor';
import { accountManagementActions } from '../_redux';

// helpers
import { isFunction } from 'app/helpers';

// constants & types
import { FORM_NAMES } from './constants';
import { I18N_TEXT, NUMERIC_REPLACE_REGEX } from 'app/constants';
import { FormType, RelationType } from 'app/constants/enums';
import { InforDebtor } from 'pages/AgencyManagement/types';

export type FormAction = 'ADD' | 'EDIT';

interface ILinkDebtorModal {
  open: boolean;
  action: FormAction;
  title?: string;
  value?: ILinkDebtor;
  validationSchema?: MagicKeyValue;
  submitButtonText?: string;
  onSubmit?: (value: ILinkDebtor, linkAnotherDebtor: boolean) => void;
  onCancel?: () => void;
  inforDebtor: InforDebtor;
  debtorList: Debtors[];
  linkAnotherDebtorProp: boolean;
  existPrimaryDebtor: boolean;
  isAddPrmary: boolean;
}

const LinkDebtorModal: React.FC<ILinkDebtorModal> = ({
  open,
  title = '',
  action,
  value = {} as ILinkDebtor,
  inforDebtor,
  debtorList,
  validationSchema,
  linkAnotherDebtorProp = false,
  existPrimaryDebtor = false,
  isAddPrmary = false,
  onSubmit,
  onCancel
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const relationRefData = useRefData('relation');
  const ecoaOptions = useRefData('ecoa');
  const sendCorrespondenceOptions = useRefData('sendCorrespondence');
  const ciicdOptions = useRefData('ciicd');

  const debtorListOptions = useMemo(() => {
    return debtorList?.map((item: Debtors) => ({
      description: item.debtorName,
      value: item.debtorName,
      show: true,
      isDisable: Number(item.id) < 3
    }));
  }, [debtorList]);

  const handleSubmit = () => {
    isFunction(onSubmit) && onSubmit(values, linkAnotherDebtorProp);
    linkAnotherDebtorProp && resetForm();
  };

  const formik = useFormik<ILinkDebtor>({
    initialValues: value,
    validationSchema: validationSchema,
    validateOnBlur: true,
    validateOnMount: false,
    onSubmit: handleSubmit
  });

  const {
    values,
    isValid,
    dirty,
    setValues,
    setFieldValue,
    submitForm,
    resetForm
  } = formik;

  const relationOptions = useMemo(() => {
    if (existPrimaryDebtor && values?.relation !== RelationType[3]) {
      return relationRefData?.filter(
        (item: RefDataValue) => item.value !== RelationType[3]
      );
    }
    return relationRefData;
  }, [existPrimaryDebtor, relationRefData, values?.relation]);

  const piidValue = useMemo(() => {
    return debtorList?.find(
      (item: Debtors) => item.debtorName === values.debtorName
    )?.piid;
  }, [values?.debtorName, debtorList]);

  useEffect(() => {
    setFieldValue(FORM_NAMES.piid.name, piidValue);
  }, [setFieldValue, piidValue]);

  useEffect(() => {
    setValues(value);
  }, [setValues, value]);

  useEffect(() => {
    if (open === false) {
      resetForm();
      dispatch(accountManagementActions.changeLinkAnotherDebtors(false));
    }
  }, [open, resetForm, dispatch]);

  const handleCancel = () => {
    resetForm();
    isFunction(onCancel) && onCancel();
  };

  const handleCheckBoxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    dispatch(
      accountManagementActions.changeLinkAnotherDebtors(event.target.checked)
    );
  };

  return (
    <FormikProvider value={formik}>
      <Modal show={open} sm>
        <ModalHeader border closeButton onHide={handleCancel}>
          <ModalTitle>{title}</ModalTitle>
        </ModalHeader>
        <ModalBody>
          <ApiErrorDetail
            forAction={[
              addAssociatedDebtor.rejected.type,
              editAssociatedDebtor.rejected.type
            ]}
            className="mb-24"
          />
          <div className="row">
            <div className="col-5">
              <DropdownControl
                required={action === FormType[0]}
                name={FORM_NAMES.relation.name}
                label={FORM_NAMES.relation.label}
                options={relationOptions}
                isCombine
                readOnly={
                  (value?.relation === RelationType[3] &&
                    action === FormType[1]) ||
                  isAddPrmary
                }
              />
            </div>
            <div className="col-7">
              <ComboBoxControl
                required
                name={FORM_NAMES.debtorName.name}
                label={FORM_NAMES.debtorName.label}
                options={debtorListOptions}
                inforDebtor={inforDebtor}
                debtorList={debtorList}
                placeholder={t(I18N_TEXT.TYPE_AT_LEAST_3_CHARS)}
              />
            </div>
          </div>
          <div className="row">
            <div className="col-5">
              <ComboBoxControl
                required
                name={FORM_NAMES.ecoa.name}
                label={FORM_NAMES.ecoa.label}
                options={ecoaOptions}
                className="mt-16"
                isCombine
              />
            </div>
            <div className="col-7">
              <DropdownControl
                required
                name={FORM_NAMES.sendCorrespondence.name}
                label={FORM_NAMES.sendCorrespondence.label}
                options={sendCorrespondenceOptions}
                className="mt-16"
                isCombine
              />
            </div>
          </div>
          <div className="row">
            <div className="col-5">
              <TextBoxControl
                name={FORM_NAMES.piid.name}
                label={FORM_NAMES.piid.label}
                className="mt-16"
                maxLength={16}
                regex={NUMERIC_REPLACE_REGEX}
                readOnly
              />
            </div>
            <div className="col-7">
              <ComboBoxControl
                name={FORM_NAMES.ciicd.name}
                label={FORM_NAMES.ciicd.label}
                options={ciicdOptions}
                className="mt-16"
                isCombine
              />
            </div>
          </div>
        </ModalBody>
        <ModalFooter
          cancelButtonText={t(I18N_TEXT.CANCEL)}
          okButtonText={t(
            action === FormType[1] ? I18N_TEXT.SAVE : I18N_TEXT.SUBMIT
          )}
          onOk={submitForm}
          onCancel={handleCancel}
          disabledOk={!isValid || !dirty}
        >
          {action === FormType[0] && (
            <CheckBox className="d-flex flex-1">
              <CheckBox.Input
                checked={linkAnotherDebtorProp}
                onChange={handleCheckBoxChange}
              />
              <CheckBox.Label>
                {t(I18N_TEXT.LINK_ANOTHER_DEBTOR)}
              </CheckBox.Label>
            </CheckBox>
          )}
        </ModalFooter>
      </Modal>
    </FormikProvider>
  );
};

export default LinkDebtorModal;
