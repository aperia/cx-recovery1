import { PayloadAction } from '@reduxjs/toolkit';

// types
import {
  Debtors,
  InitialStateAccountManagement
} from 'pages/AccountManagement/types';

// helpers
import { isBoolean } from 'app/helpers';

export const onChangeSearchDebtors = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<string>
) => {
  draftState.debtorMaintenance.debtorList.valueSearch = action.payload;
};

export const onChangeSortDebtors = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<ISortType>
) => {
  draftState.debtorMaintenance.debtorList.sortBy = action.payload;
};

export const toggleDebtorDetailModal = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<{
    debtor?: Debtors;
    isOpen?: boolean;
  }>
) => {
  const { debtor, isOpen } = action.payload;
  draftState.debtorMaintenance = {
    ...draftState.debtorMaintenance,
    viewDetailOpen: isBoolean(isOpen)
      ? isOpen
      : !draftState.debtorMaintenance.viewDetailOpen,
    selectedDebtor: debtor
  };
};

export const onToggleFormModal = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<{
    selected: any;
  }>
) => {
  const { selected } = action.payload;

  const { debtorForm } = draftState.debtorMaintenance;

  draftState.debtorMaintenance.debtorForm = {
    ...debtorForm,
    selected,
    isOpen: !debtorForm.isOpen
  };
};
