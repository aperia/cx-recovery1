import { createSelector } from '@reduxjs/toolkit';

// types
import { Debtors } from 'pages/AccountManagement/types';
import { IDebtorFormReducer } from './../../types';
import { DebtorMaintenance } from 'pages/AccountManagement/types';

// helpers
import {
  searchDebtors,
  sortDebtors
} from 'pages/AccountManagement/DebtorMaintenance/helpers';

const getDebtorSort = (states: RootState): ISortType => {
  return states.accountManagement.debtorMaintenance.debtorList.sortBy;
};

const getDebtors = (states: RootState): Debtors[] => {
  return states.accountManagement.debtorMaintenance.debtorList.data;
};

const getValueSearchDebtors = (states: RootState): string => {
  return states.accountManagement.debtorMaintenance.debtorList.valueSearch;
};

const getDebtorMaintenanceState = (states: RootState) =>
  states.accountManagement.debtorMaintenance;
const getDebtorForm = (states: RootState): IDebtorFormReducer => {
  return states.accountManagement.debtorMaintenance.debtorForm;
};

export const selectDebtorFormIsOpen = createSelector(
  getDebtorForm,
  (data: IDebtorFormReducer): boolean => data.isOpen
);

export const selectSelectedDebtor = createSelector(
  getDebtorForm,
  (data: IDebtorFormReducer): Debtors | undefined => data.selected
);

export const selectorDebtors = createSelector(
  [getDebtors, getDebtorSort, getValueSearchDebtors],
  (data: Debtors[], sortBy: ISortType, valueSearch: string) =>
    sortDebtors(searchDebtors(data, valueSearch), sortBy)
);

export const selectorValueSearch = createSelector(
  [getValueSearchDebtors],
  (data: string) => data
);

export const selectorDebtorSort = createSelector(
  [getDebtorSort],
  (data: ISortType) => data
);

export const selectorIsError = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.debtorMaintenance.debtorList.isError
  ],
  (data: boolean) => data
);

export const selectedViewDetailOpen = createSelector(
  getDebtorMaintenanceState,
  (data: DebtorMaintenance) => data.viewDetailOpen
);

export const selectedDebtorDetail = createSelector(
  getDebtorMaintenanceState,
  (data: DebtorMaintenance) => data.selectedDebtor
);

export const selectedAddressHistory = createSelector(
  getDebtorMaintenanceState,
  (data: DebtorMaintenance) => data.addressHistory
);
