import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { GET_ADDRESS_HISTORY } from 'pages/AccountManagement/DebtorMaintenance/constants';
import {
  IGetAddressHistoryPayload,
  InitialStateAccountManagement
} from 'pages/AccountManagement/types';
import { debtorMaintenanceController } from 'pages/AccountManagement/__mock__/debtorMaintenanceController';

export const getAddressHistory = createAsyncThunk<
  IGetAddressHistoryPayload,
  undefined,
  ThunkAPIConfig
>(GET_ADDRESS_HISTORY, async _ => {
  const { data } = await debtorMaintenanceController.getAddressHistory();
  return {
    data: data.data
  };
});

export const getAddressHistoryBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { fulfilled } = getAddressHistory;
  builder.addCase(fulfilled, (draftState, action) => {
    draftState.debtorMaintenance.addressHistory = action.payload.data;
  });
};
