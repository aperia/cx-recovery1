import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { checkEmpty } from 'app/helpers';
import debtorMaintenanceServices from 'pages/AccountManagement/DebtorMaintenance/debtorMaintenanceServices';
import { InitialStateAccountManagement } from 'pages/AccountManagement/types';

export const getDebtors = createAsyncThunk<any, any, ThunkAPIConfig>(
  'getDebtorMaintenance',
  async (_, { rejectWithValue }) => {
    try {
      const { data } = await debtorMaintenanceServices.getDebtors();
      return { data: data.data };
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const getDebtorsBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { pending, fulfilled, rejected } = getDebtors;
  builder
    .addCase(pending, draftState => {
      draftState.debtorMaintenance.debtorList.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      const { data } = action.payload;
      const { isError } = draftState.debtorMaintenance.debtorList;
      draftState.debtorMaintenance.debtorList.isLoading = false;
      draftState.debtorMaintenance.debtorList.data = data;
      draftState.debtorMaintenance.debtorList.isError = checkEmpty([
        isError,
        false,
        true
      ]);
    })
    .addCase(rejected, draftState => {
      draftState.debtorMaintenance.debtorList.isLoading = false;
      draftState.debtorMaintenance.debtorList.isError = true;
    });
};
