import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// actions
import { accountManagementActions } from '..';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// constants
import { API_ERROR, I18N_TEXT } from 'app/constants';
import { FormikHelpers } from 'formik';

// service
import debtorMaintenanceServices from 'pages/AccountManagement/DebtorMaintenance/debtorMaintenanceServices';

export interface IPayloadSaveDebtor {}
export interface ISaveDebtorArgs {
  id?: string;
  values: any;
  formikBag: FormikHelpers<any>;
}

export const saveDebtor = createAsyncThunk<
  IPayloadSaveDebtor,
  ISaveDebtorArgs,
  ThunkAPIConfig
>('debtorMaintenance/saveDebtor', async (args, thunkAPI) => {
  const { id, values, formikBag } = args;

  const { resetForm, setSubmitting } = formikBag;
  const { dispatch } = thunkAPI;

  try {
    const response = await debtorMaintenanceServices.saveDebtor({
      id,
      values,
      formikBag
    });

    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: id
            ? I18N_TEXT.EDIT_DEBTOR_SUCCESS
            : I18N_TEXT.ADD_DEBTOR_SUCCESS
        })
      );

      // close modal
      dispatch(
        accountManagementActions.onToggleFormModal({
          selected: undefined
        })
      );
    });

    resetForm();

    return response;
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: id ? I18N_TEXT.EDIT_DEBTOR_FAIL : I18N_TEXT.ADD_DEBTOR_FAIL
      })
    );

    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  } finally {
    setSubmitting(false);
  }
});

export const saveDebtorBuilder = (
  builder: ActionReducerMapBuilder<IPayloadSaveDebtor>
) => {
  const { pending, fulfilled, rejected } = saveDebtor;
  builder
    .addCase(pending, () => {})
    .addCase(fulfilled, () => {})
    .addCase(rejected, () => {});
};
