import { batch } from 'react-redux';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// actions
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { accountManagementActions } from '..';

// services
import debtorMaintenanceServices from 'pages/AccountManagement/DebtorMaintenance/debtorMaintenanceServices';

// constants
import { API_ERROR, I18N_TEXT } from 'app/constants';

// types
import {
  IDeleteDebtorArgs,
  InitialStateAccountManagement
} from 'pages/AccountManagement/types';

export const deleteDebtor = createAsyncThunk<
  any,
  IDeleteDebtorArgs,
  ThunkAPIConfig
>('debtorMaintenance/deleteDebtor', async ({ id }, thunkAPI) => {
  const { dispatch } = thunkAPI;

  try {
    const { data } = await debtorMaintenanceServices.deleteDebtor({ id });
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.DELETE_DEBTOR_SUCCESS
        })
      );

      dispatch(rootModalActions.close());
      dispatch(
        accountManagementActions.toggleDebtorDetailModal({ isOpen: false })
      );
    });

    return { data: data.data };
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.DELETE_DEBTOR_FAIL
      })
    );

    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  } finally {
    dispatch(rootModalActions.close());
  }
});

export const deleteDebtorBuilder = (builder: ActionReducerMapBuilder<any>) => {
  const { pending, fulfilled, rejected } = deleteDebtor;
  builder
    .addCase(pending, () => {})
    .addCase(fulfilled, (draftState: InitialStateAccountManagement, action) => {
      const { id } = action.meta.arg;

      const { data } = draftState.debtorMaintenance.debtorList;
      const filteredData = data.filter((item: any) => item.id !== id);

      draftState.debtorMaintenance.debtorList.data = filteredData;
    })
    .addCase(rejected, () => {});
};
