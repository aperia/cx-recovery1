import { PayloadAction } from '@reduxjs/toolkit';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import {
  InitialStateAccountManagement,
  pendingCorrespondenceDefault
} from 'pages/AccountManagement/types';

export const onChangePendingCorrespondenceSearchValue = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<AttributeSearchValue[]>
) => {
  draftState.correspondence.pendingCorrespondence.searchValue = action.payload;
};

export const onChangePendingCorrespondenceSortBy = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<RefDataValue>
) => {
  draftState.correspondence.pendingCorrespondence.sortBy = action.payload;
};

export const onChangePendingCorrespondenceOrderBy = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<RefDataValue>
) => {
  draftState.correspondence.pendingCorrespondence.orderBy = action.payload;
};

export const onChangePendingCorrespondenceCurrentPage = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<number>
) => {
  draftState.correspondence.pendingCorrespondence.currentPage = action.payload;
};

export const onChangePendingCorrespondencePageSize = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<number>
) => {
  draftState.correspondence.pendingCorrespondence.pageSize = action.payload;
};

export const onPendingCorrespondenceClearAndReset = (draftState: InitialStateAccountManagement) => {
  draftState.correspondence.pendingCorrespondence.searchValue = pendingCorrespondenceDefault.searchValue;
  draftState.correspondence.pendingCorrespondence.data = pendingCorrespondenceDefault.data;
  draftState.correspondence.pendingCorrespondence.sortBy = pendingCorrespondenceDefault.sortBy;
  draftState.correspondence.pendingCorrespondence.totalItem = pendingCorrespondenceDefault.totalItem;
  draftState.correspondence.pendingCorrespondence.currentPage = pendingCorrespondenceDefault.currentPage;
  draftState.correspondence.pendingCorrespondence.pageSize = pendingCorrespondenceDefault.pageSize;
};