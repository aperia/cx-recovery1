import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { checkEmpty } from 'app/helpers';
import correspondenceServices from 'pages/AccountManagement/Correspondence/correspondenceServices';
import {
  InitialStateAccountManagement,
  IPayloadGetPendingCorrespondenceReturn
} from 'pages/AccountManagement/types';

interface IGetCorrespondenceArg {}

export const getPendingCorrespondence = createAsyncThunk<
  IPayloadGetPendingCorrespondenceReturn,
  IGetCorrespondenceArg,
  ThunkAPIConfig
>('getPendingCorrespondence', async (_, { getState, rejectWithValue }) => {
  try {
    const { sortBy, orderBy, searchValue, currentPage, pageSize } =
      getState().accountManagement.correspondence.pendingCorrespondence;
    const response = await correspondenceServices.getPendingCorrespondence({
      sortBy: sortBy.value,
      orderBy: orderBy.value,
      searchValue,
      page: currentPage,
      pageSize: pageSize
    });

    return { data: response.data };
  } catch (error: any) {
    return rejectWithValue(error);
  }
});

export const getPendingCorrespondenceBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { pending, fulfilled, rejected } = getPendingCorrespondence;
  builder
    .addCase(pending, (draftState, action) => {
      draftState.correspondence.pendingCorrespondence.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      const { data } = action.payload;
      const { isError } = draftState.correspondence.pendingCorrespondence;

      draftState.correspondence.pendingCorrespondence.isLoading = false;
      draftState.correspondence.pendingCorrespondence.data = data.data;
      draftState.correspondence.pendingCorrespondence.totalItem = data.totalItem;
      draftState.correspondence.pendingCorrespondence.currentPage = data.currentPage;
      draftState.correspondence.pendingCorrespondence.pageSize = data.pageSize;
      draftState.correspondence.pendingCorrespondence.isError = checkEmpty([
        isError,
        false,
        true
      ]);
    })
    .addCase(rejected, draftState => {
      draftState.correspondence.pendingCorrespondence.isLoading = false;
      draftState.correspondence.pendingCorrespondence.isError = true;
    });
};
