import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

import { I18N_TEXT } from 'app/constants';
import correspondenceServices from 'pages/AccountManagement/Correspondence/correspondenceServices';
import { InitialStateAccountManagement } from 'pages/AccountManagement/types';

import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

interface IGetCorrespondenceArg {
  id: string;
}

export const removePendingCorrespondence = createAsyncThunk<
  void,
  IGetCorrespondenceArg,
  ThunkAPIConfig
>(
  'removePendingCorrespondence',
  async (args, { dispatch, rejectWithValue }) => {
    try {
      const { success } =
        await correspondenceServices.removePendingCorrespondence(args.id);

      if (!success)
        throw new Error(I18N_TEXT.MSG_DELETE_PENDING_CORRESPONDENCE_ERROR);
      batch(() => {
        // get modal correspondence
        dispatch(accountManagementActions.getModalPendingCorrespondence({}));

        // get correspondence
        dispatch(accountManagementActions.getPendingCorrespondence({}));

        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_TEXT.MSG_DELETE_PENDING_CORRESPONDENCE_SUCCESS
          })
        );
        dispatch(rootModalActions.close());
      });
    } catch (error: any) {
      return rejectWithValue(error);
    }
  }
);

export const removePendingCorrespondenceBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { pending, fulfilled, rejected } = removePendingCorrespondence;
  builder
    .addCase(pending, (draftState, action) => {
      draftState.correspondence.pendingCorrespondence.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      draftState.correspondence.pendingCorrespondence.isLoading = false;
    })
    .addCase(rejected, draftState => {
      draftState.correspondence.pendingCorrespondence.isError = true;
    });
};
