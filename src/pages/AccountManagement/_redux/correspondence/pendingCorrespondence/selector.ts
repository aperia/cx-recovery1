import { createSelector } from '@reduxjs/toolkit';
import { formatListData } from 'app/helpers';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { IPendingCorrespondence } from 'pages/AccountManagement/types';

const getDataList = (states: RootState): IPendingCorrespondence[] => {
  return states.accountManagement.correspondence.pendingCorrespondence.data;
};

const getValueSearch = (states: RootState): AttributeSearchValue[] => {
  return states.accountManagement.correspondence.pendingCorrespondence
    .searchValue;
};

export const selectorModalList = createSelector(
  getDataList,
  (data: IPendingCorrespondence[]) => formatListData(data)
);

export const selectorValueSearch = createSelector(
  [getValueSearch],
  (data: AttributeSearchValue[]) => data
);

export const selectorIsError = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.correspondence.pendingCorrespondence.isError
  ],
  (data: boolean) => data
);

export const selectorIsLoading = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.correspondence.pendingCorrespondence.isLoading
  ],
  (data: boolean) => data
);

export const selectorTotalPage = createSelector(
  (states: RootState) =>
    states.accountManagement.correspondence.pendingCorrespondence.totalItem,
  (data: number) => data
);

export const selectorCurrentPage = createSelector(
  (states: RootState) =>
    states.accountManagement.correspondence.pendingCorrespondence.currentPage,
  (data: number) => data
);

export const selectorPageSize = createSelector(
  (states: RootState) =>
    states.accountManagement.correspondence.pendingCorrespondence.pageSize,
  (data: number) => data
);
