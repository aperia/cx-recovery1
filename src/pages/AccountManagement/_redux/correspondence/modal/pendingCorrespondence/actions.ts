import { PayloadAction } from '@reduxjs/toolkit';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import {
  InitialStateAccountManagement,
  pendingCorrespondenceDefault
} from 'pages/AccountManagement/types';

export const onChangeModalPendingCorrespondenceSearchValue = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<AttributeSearchValue[]>
) => {
  draftState.correspondence.modal.pendingCorrespondence.searchValue = action.payload;
};

export const onChangeModalPendingCorrespondenceCurrentPage = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<number>
) => {
  draftState.correspondence.modal.pendingCorrespondence.currentPage = action.payload;
};

export const onChangeModalPendingCorrespondencePageSize = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<number>
) => {
  draftState.correspondence.modal.pendingCorrespondence.pageSize = action.payload;
};

export const onModalPendingCorrespondenceClearAndReset = (draftState: InitialStateAccountManagement) => {
  draftState.correspondence.modal.pendingCorrespondence.searchValue = pendingCorrespondenceDefault.searchValue;
  draftState.correspondence.modal.pendingCorrespondence.data = pendingCorrespondenceDefault.data;
  draftState.correspondence.modal.pendingCorrespondence.sortBy = pendingCorrespondenceDefault.sortBy;
  draftState.correspondence.modal.pendingCorrespondence.totalItem = pendingCorrespondenceDefault.totalItem;
  draftState.correspondence.modal.pendingCorrespondence.currentPage = pendingCorrespondenceDefault.currentPage;
  draftState.correspondence.modal.pendingCorrespondence.pageSize = pendingCorrespondenceDefault.pageSize;
};