import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { checkEmpty } from 'app/helpers';
import correspondenceServices from 'pages/AccountManagement/Correspondence/correspondenceServices';
import {
  InitialStateAccountManagement,
  IPayloadGetPendingCorrespondenceReturn
} from 'pages/AccountManagement/types';

interface IGetCorrespondenceArg {}

export const getModalPendingCorrespondence = createAsyncThunk<
  IPayloadGetPendingCorrespondenceReturn,
  IGetCorrespondenceArg,
  ThunkAPIConfig
>('modal/getPendingCorrespondence', async (_, { getState, rejectWithValue }) => {
  try {
    const { sortBy, orderBy, searchValue, currentPage, pageSize } =
      getState().accountManagement.correspondence.modal.pendingCorrespondence;
    const response = await correspondenceServices.getPendingCorrespondence({
      sortBy: sortBy.value,
      orderBy: orderBy.value,
      searchValue,
      page: currentPage,
      pageSize: pageSize
    });

    return { data: response.data };
  } catch (error: any) {
    return rejectWithValue(error);
  }
});

export const getModalPendingCorrespondenceBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { pending, fulfilled, rejected } = getModalPendingCorrespondence;
  builder
    .addCase(pending, (draftState, action) => {
      draftState.correspondence.modal.pendingCorrespondence.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      const { data } = action.payload;
      const { isError } = draftState.correspondence.modal.pendingCorrespondence;

      draftState.correspondence.modal.pendingCorrespondence.isLoading = false;
      draftState.correspondence.modal.pendingCorrespondence.data = data.data;
      draftState.correspondence.modal.pendingCorrespondence.totalItem = data.totalItem;
      draftState.correspondence.modal.pendingCorrespondence.currentPage = data.currentPage;
      draftState.correspondence.modal.pendingCorrespondence.pageSize = data.pageSize;
      draftState.correspondence.modal.pendingCorrespondence.isError = checkEmpty([
        isError,
        false,
        true
      ]);
    })
    .addCase(rejected, draftState => {
      draftState.correspondence.modal.pendingCorrespondence.isLoading = false;
      draftState.correspondence.modal.pendingCorrespondence.isError = true;
    });
};
