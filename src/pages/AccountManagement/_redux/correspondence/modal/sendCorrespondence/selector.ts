import { createSelector } from '@reduxjs/toolkit';
import { ILetterCode, ILinkDebtor } from 'pages/AccountManagement/types';

export const selectorCollapseSendCorrespondence = createSelector(
  (states: RootState) =>
    states.accountManagement.correspondence.modal.sendCorrespondence.collapse,
  (data: boolean) => data
);

export const selectorDataSendCorrespondence = createSelector(
  (states: RootState) =>
    states.accountManagement.correspondence.modal.sendCorrespondence.itemCode,
  (data: ILetterCode | undefined) => data
);

export const selectorDebtorSelected = createSelector(
  (states: RootState) =>
    states.accountManagement.correspondence.modal.sendCorrespondence
      .debtorListSelected,
  (data: ILinkDebtor[]) => data
);
