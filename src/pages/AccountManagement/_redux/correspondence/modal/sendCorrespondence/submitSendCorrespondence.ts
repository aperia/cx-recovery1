import { batch } from 'react-redux';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import correspondenceServices from 'pages/AccountManagement/Correspondence/correspondenceServices';
import {
  CORRESPONDENCE_TAB,
  InitialStateAccountManagement,
  IPayloadSendCorrespondence
} from 'pages/AccountManagement/types';
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { I18N_TEXT } from 'app/constants';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

export const sendCorrespondence = createAsyncThunk<
  void,
  IPayloadSendCorrespondence,
  ThunkAPIConfig
>('modal/sendCorrespondence', async (arg, { dispatch, rejectWithValue }) => {
  try {
    const { success } = await correspondenceServices.sendCorrespondence(arg);
    if (!success) throw new Error(I18N_TEXT.MSG_SEND_CORRESPONDENCE_ERROR);
    batch(() => {
      // change tan pending correspondence
      dispatch(
        accountManagementActions.onChangeTabCorrespondence(
          CORRESPONDENCE_TAB.PENDING_CORRESPONDENCE
        )
      );

      // get list account group
      dispatch(
        accountManagementActions.onChangeModalPendingCorrespondenceSearchValue([
          {
            key: 'accountNumber',
            value: arg?.accNumber.accountNumberUnMask
          }
        ])
      );

      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.MSG_SEND_CORRESPONDENCE_SUCCESS
        })
      );
    });
  } catch (error: any) {
    return rejectWithValue(error);
  }
});

export const sendCorrespondenceBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { pending, fulfilled, rejected } = sendCorrespondence;
  builder
    .addCase(pending, (draftState, action) => {
      draftState.correspondence.modal.codeList.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      draftState.correspondence.modal.codeList.isLoading = true;
    })
    .addCase(rejected, draftState => {
      draftState.correspondence.modal.codeList.isLoading = false;
      draftState.correspondence.modal.codeList.isError = true;
    });
};
