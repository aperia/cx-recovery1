import { PayloadAction } from '@reduxjs/toolkit';
import {
  InitialStateAccountManagement,
  IChangeFlyoutSendCorrespondence,
  sendCorrespondenceDefault,
  ILinkDebtor
} from 'pages/AccountManagement/types';

export const onChangeCollapseSendCorrespondence = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<IChangeFlyoutSendCorrespondence>
) => {
  draftState.correspondence.modal.sendCorrespondence.collapse =
    action.payload.collapse;

  if (action.payload.collapse === true) {
    draftState.correspondence.modal.sendCorrespondence.itemCode = undefined;
  } else {
    draftState.correspondence.modal.sendCorrespondence.itemCode =
      action.payload.item;
    draftState.correspondence.modal.sendCorrespondence.debtorListSelected = [];
  }
};

export const onSelectReceiverSendCorrespondence = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<ILinkDebtor[]>
) => {
  draftState.correspondence.modal.sendCorrespondence.debtorListSelected =
    action.payload;
};

export const onSendCorrespondenceClearAndReset = (
  draftState: InitialStateAccountManagement
) => {
  draftState.correspondence.modal.sendCorrespondence.collapse =
    sendCorrespondenceDefault.collapse;
  draftState.correspondence.modal.sendCorrespondence.itemCode =
    sendCorrespondenceDefault.itemCode;
  draftState.correspondence.modal.sendCorrespondence.debtorListSelected =
    sendCorrespondenceDefault.debtorListSelected;
};
