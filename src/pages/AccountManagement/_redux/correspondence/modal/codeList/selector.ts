import { createSelector } from '@reduxjs/toolkit';
import { formatListData } from 'app/helpers';
import { ILetterCode } from 'pages/AccountManagement/types';

const getCodeListSortBy = (states: RootState): ISortType => {
  return states.accountManagement.correspondence.modal.codeList.sortBy;
};

const getCodeList = (states: RootState): ILetterCode[] => {
  return states.accountManagement.correspondence.modal.codeList.data;
};

const getValueSearchCodeList = (states: RootState): string => {
  return states.accountManagement.correspondence.modal.codeList.searchValue;
};

export const selectorCodeList = createSelector(
  getCodeList,
  (data: ILetterCode[]) => formatListData(data)
);

export const selectorValueSearch = createSelector(
  [getValueSearchCodeList],
  (data: string) => data
);

export const selectorSortBy = createSelector(
  [getCodeListSortBy],
  (data: ISortType) => data
);

export const selectorIsError = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.correspondence.modal.codeList.isError
  ],
  (data: boolean) => data
);

export const selectorIsLoading = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.correspondence.modal.codeList.isLoading
  ],
  (data: boolean) => data
);

export const selectorTypeCode = createSelector(
  (states: RootState) =>
    states.accountManagement.correspondence.modal.codeList.type,
  (data: RefDataValue) => data
);

export const selectorTotalPage = createSelector(
  (states: RootState) =>
    states.accountManagement.correspondence.modal.codeList.totalItem,
  (data: number) => data
);

export const selectorCurrentPage = createSelector(
  (states: RootState) =>
    states.accountManagement.correspondence.modal.codeList.currentPage,
  (data: number) => data
);

export const selectorPageSize = createSelector(
  (states: RootState) =>
    states.accountManagement.correspondence.modal.codeList.pageSize,
  (data: number) => data
);