import { PayloadAction } from '@reduxjs/toolkit';
import {
  InitialStateAccountManagement,
  codeListDefault
} from 'pages/AccountManagement/types';

export const onChangeLetterCodeType = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<RefDataValue>
) => {
  draftState.correspondence.modal.codeList.type = action.payload;
};

export const onChangeLetterCodeSearchValue = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<string>
) => {
  draftState.correspondence.modal.codeList.searchValue = action.payload;
};

export const onChangeLetterCodeSortBy = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<ISortType>
) => {
  draftState.correspondence.modal.codeList.sortBy = action.payload;
};

export const onChangeLetterCodeCurrentPage = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<number>
) => {
  draftState.correspondence.modal.codeList.currentPage = action.payload;
};

export const onChangeLetterCodePageSize = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<number>
) => {
  draftState.correspondence.modal.codeList.pageSize = action.payload;
};

export const onLetterCodeClearAndReset = (
  draftState: InitialStateAccountManagement
) => {
  draftState.correspondence.modal.codeList.searchValue =
    codeListDefault.searchValue;
  draftState.correspondence.modal.codeList.data = codeListDefault.data;
  draftState.correspondence.modal.codeList.sortBy = codeListDefault.sortBy;
  draftState.correspondence.modal.codeList.totalItem =
    codeListDefault.totalItem;
  draftState.correspondence.modal.codeList.currentPage =
    codeListDefault.currentPage;
  draftState.correspondence.modal.codeList.pageSize = codeListDefault.pageSize;
};
