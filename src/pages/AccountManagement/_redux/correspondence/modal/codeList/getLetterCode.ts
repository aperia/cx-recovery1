import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { checkEmpty } from 'app/helpers';
import correspondenceServices from 'pages/AccountManagement/Correspondence/correspondenceServices';
import {
  InitialStateAccountManagement,
  IPayloadGetCodeListReturn
} from 'pages/AccountManagement/types';

interface IGetCorrespondenceArg {}

export const getLetterCode = createAsyncThunk<
  IPayloadGetCodeListReturn,
  IGetCorrespondenceArg,
  ThunkAPIConfig
>('modal/getLetterCode', async (_, { getState, rejectWithValue }) => {
  try {
    const { sortBy, searchValue, type, currentPage, pageSize } =
      getState().accountManagement.correspondence.modal.codeList;
    const response = await correspondenceServices.getLetterCode({
      sortBy,
      type: type.value,
      searchValue: searchValue,
      page: currentPage,
      pageSize: pageSize
    });

    return { data: response.data };
  } catch (error: any) {
    return rejectWithValue(error);
  }
});

export const getCorrespondenceBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { pending, fulfilled, rejected } = getLetterCode;
  builder
    .addCase(pending, (draftState, action) => {
      draftState.correspondence.modal.codeList.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      const { data } = action.payload;
      const { isError } = draftState.correspondence.modal.codeList;

      draftState.correspondence.modal.codeList.isLoading = false;
      draftState.correspondence.modal.codeList.data = data.data;
      draftState.correspondence.modal.codeList.totalItem = data.totalItem;
      draftState.correspondence.modal.codeList.currentPage =
        data.currentPage;
      draftState.correspondence.modal.codeList.pageSize = data.pageSize;
      draftState.correspondence.modal.codeList.isError = checkEmpty([
        isError,
        false,
        true
      ]);
    })
    .addCase(rejected, draftState => {
      draftState.correspondence.modal.codeList.isLoading = false;
      draftState.correspondence.modal.codeList.isError = true;
    });
};
