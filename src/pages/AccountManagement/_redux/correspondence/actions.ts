import { PayloadAction } from '@reduxjs/toolkit';
import {
  CORRESPONDENCE_TAB,
  CorrespondenceInitialState,
  InitialStateAccountManagement,
} from 'pages/AccountManagement/types';

export const onSetModalCorrespondence = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<boolean>
) => {
  draftState.correspondence.modal.isOpenModal = action.payload;
  draftState.correspondence.modal.isDiscardForm = false;
};

export const onCheckFormDiscard = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<boolean>
) => {
  draftState.correspondence.modal.isDiscardForm = action.payload;
};

export const onChangeTabCorrespondence = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<CORRESPONDENCE_TAB>
) => {
  draftState.correspondence.modal.currentTab = action.payload;
};

export const onResetModalCorrespondence = (draftState: InitialStateAccountManagement) => {
  draftState.correspondence.modal.currentTab = CorrespondenceInitialState.modal.currentTab;
  draftState.correspondence.modal.isOpenModal = CorrespondenceInitialState.modal.isOpenModal;
  draftState.correspondence.modal.codeList = CorrespondenceInitialState.modal.codeList;
  draftState.correspondence.modal.pendingCorrespondence = CorrespondenceInitialState.modal.pendingCorrespondence;
};

export const onResetChangeTabCorrespondence = (draftState: InitialStateAccountManagement) => {
  draftState.correspondence.modal.codeList = CorrespondenceInitialState.modal.codeList;
  draftState.correspondence.modal.pendingCorrespondence = CorrespondenceInitialState.modal.pendingCorrespondence;
};
