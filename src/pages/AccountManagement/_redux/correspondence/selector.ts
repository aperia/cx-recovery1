import { createSelector } from '@reduxjs/toolkit';
import { CorrespondenceReducer, CORRESPONDENCE_TAB } from 'pages/AccountManagement/types';

const getCorrespondence = (states: RootState): CorrespondenceReducer =>
  states.accountManagement.correspondence;

export const selectModalCorrespondenceIsOpen = createSelector(
  getCorrespondence,
  (data: CorrespondenceReducer): boolean => data.modal.isOpenModal
);

export const selectIsDiscardFormModal = createSelector(
  getCorrespondence,
  (data: CorrespondenceReducer): boolean => data.modal.isDiscardForm
);

export const selectModalCurrentTab = createSelector(
  getCorrespondence,
  (data: CorrespondenceReducer): CORRESPONDENCE_TAB => data.modal.currentTab
);
