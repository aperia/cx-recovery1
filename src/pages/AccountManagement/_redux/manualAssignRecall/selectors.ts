import { createSelector } from '@reduxjs/toolkit';
import {
  IAccounts,
  IManuals,
  AccountManagementTabID
} from 'pages/AccountManagement/types';

const getManualData = (state: RootState) =>
  state.accountManagement.manualAssignRecall.data;

const getAccSearchData = (state: RootState) =>
  state.accountManagement.common.accSearchValue;

const getLoading = (state: RootState) =>
  state.accountManagement.manualAssignRecall.isLoading;

const getError = (state: RootState) =>
  state.accountManagement.manualAssignRecall.isError;

const getModal = (state: RootState) =>
  state.accountManagement.manualAssignRecall.isOpenModal;

const getActiveTab = (state: RootState) =>
  state.accountManagement.common.activeTab;

export const selectorAssignRecallList = createSelector(
  getManualData,
  (data: IManuals[]) => data
);

export const selectorSearchAcc = createSelector(
  getAccSearchData,
  (data: IAccounts) => data
);

export const selectorIsLoading = createSelector(
  getLoading,
  (data: boolean) => data
);

export const selectorIsError = createSelector(
  getError,
  (data: boolean) => data
);

export const selectorIsOpenModal = createSelector(
  getModal,
  (data: boolean) => data
);

export const selectorActiveTab = createSelector(
  getActiveTab,
  (data: AccountManagementTabID) => data
);
