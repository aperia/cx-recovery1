import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import accountServices from '../../accountServices';

// Utils
import {
  InitialStateAccountManagement,
  ManualAssignRecallArgs,
  AddManualAssignRecallPayload
} from 'pages/AccountManagement/types';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { accountManagementActions } from '..';
import { apiErrorDetailActions } from 'pages/__commons/ApiErrorDetail/_redux/reducer';

export const addManualAssign = createAsyncThunk<
  AddManualAssignRecallPayload,
  ManualAssignRecallArgs,
  ThunkAPIConfig
>('addManualRecall', async (args, thunkAPI) => {
  const { accNumber, accountData } = args;
  const { dispatch, getState } = thunkAPI;

  const { activeTab } = getState().accountManagement.common;

  try {
    const { data } = await accountServices.addManualAssignRecall({
      accNumber,
      accountData
    });

    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'success',
        message: 'txt_manual_assign_recall_added'
      })
    );

    dispatch(accountManagementActions.onSetModal(false));

    // Check if is different tab
    if (activeTab !== 'assignRecallList')
      dispatch(accountManagementActions.onChangeTab('assignRecallList'));

    return data;
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: 'txt_manual_assign_recall_added_fail'
      })
    );
    dispatch(accountManagementActions.onSetModal(true));
    dispatch(
      apiErrorDetailActions.setErrValue({
        id: addManualAssign.rejected.type,
        value: 'Something when wrong when adding!'
      })
    );
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const addManualAssignBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { pending, fulfilled, rejected } = addManualAssign;
  builder
    .addCase(pending, draftState => {
      draftState.manualAssignRecall.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      const { data } = action.payload;

      draftState.manualAssignRecall.data = data;
      draftState.manualAssignRecall.isLoading = false;
      draftState.manualAssignRecall.isError = false;
    })
    .addCase(rejected, draftState => {
      draftState.manualAssignRecall.isLoading = false;
      draftState.manualAssignRecall.isError = true;
    });
};
