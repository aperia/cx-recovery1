import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import accountServices from '../../accountServices';

// Utils
import {
  InitialStateAccountManagement,
  ManualAssignRecallPayload
} from 'pages/AccountManagement/types';

export const getManualAssign = createAsyncThunk<
  ManualAssignRecallPayload,
  undefined,
  ThunkAPIConfig
>('getManualRecall', async (_, thunkAPI) => {
  try {
    const { data } = await accountServices.getManualAssignRecall();

    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const getManualAssignBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { pending, fulfilled, rejected } = getManualAssign;
  builder
    .addCase(pending, draftState => {
      draftState.manualAssignRecall.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      const { data } = action.payload;

      draftState.manualAssignRecall.data = data;
      draftState.manualAssignRecall.isLoading = false;
    })
    .addCase(rejected, draftState => {
      draftState.manualAssignRecall.isLoading = false;
    });
};
