import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { apiErrorDetailActions } from 'pages/__commons/ApiErrorDetail/_redux/reducer';

import { associatedDebtorController } from 'pages/AccountManagement/__mock__/associatedsDebtorsController';
import { accountManagementActions } from '..';

import { I18N_TEXT } from 'app/constants';

import {
  ILinkDebtor,
  InitialStateAccountManagement
} from 'pages/AccountManagement/types';

interface IPayloadDeleteAssociatedsDebtor {
  linkDebtors: ILinkDebtor[];
}
const DELETE_ASSOCIATED_DEBTOR = 'deleteAssociatedsDebtor';

export const deleteAssociatedsDebtor = createAsyncThunk<
  IPayloadDeleteAssociatedsDebtor,
  { id: string; currentPage: number },
  ThunkAPIConfig
>(DELETE_ASSOCIATED_DEBTOR, async (args, { dispatch, rejectWithValue }) => {
  try {
    const { data, success } =
      await associatedDebtorController.deleteLinkDebtor(args.id);

    if (!success) throw new Error('Delete link debtor failed.');
    batch(() => {
      dispatch(
        accountManagementActions.getAssociatedDebtors({
          page: args.currentPage
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.DEBTOR_UNLINKED
        })
      );
      dispatch(rootModalActions.close());
    });
    return {
      linkDebtors: data
    };
  } catch (error) {
    if (error?.message === 'DELETE FAILED') {
      dispatch(
        apiErrorDetailActions.setErrValue({
          id: deleteAssociatedsDebtor.rejected.type,
          value: `Something wrong when unlink debtor!`
        })
      );
    }
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_TEXT.DEBTOR_UNLINK_FAILED
        })
      );
      dispatch(rootModalActions.close());
    });
    return rejectWithValue(error);
  }
});

export const deleteAssociatedsDebtorBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { pending, fulfilled, rejected } = deleteAssociatedsDebtor;
  builder
    .addCase(pending, draftState => {
      draftState.manageAssociatedDebtors.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      draftState.manageAssociatedDebtors.data = action.payload.linkDebtors;
      draftState.manageAssociatedDebtors.isLoading = false;
    })
    .addCase(rejected, draftState => {
      draftState.manageAssociatedDebtors.isLoading = false;
    });
};
