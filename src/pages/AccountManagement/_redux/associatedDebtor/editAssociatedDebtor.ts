import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

import { associatedDebtorController } from 'pages/AccountManagement/__mock__/associatedsDebtorsController';
// redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { accountManagementActions } from '..';
import { apiErrorDetailActions } from 'pages/__commons/ApiErrorDetail/_redux/reducer';

// constants & types
import { I18N_TEXT } from 'app/constants';
import {
  Debtors,
  ILinkDebtor,
  InitialStateAccountManagement
} from 'pages/AccountManagement/types';

import { getLast4SSN } from 'pages/AccountManagement/ManageAssociatedDebtors/helpers';

const EDIT_ASSOCIATED_DEBTOR = 'editAssociatedDebtor';

export const editAssociatedDebtor = createAsyncThunk<
  void,
  {
    id: string;
    dataChange: ILinkDebtor;
    debtorList: Debtors[];
    currentPage: number;
    pageSize: number;
    isExistRelation?: boolean;
    isChange?: boolean;
  },
  ThunkAPIConfig
>(EDIT_ASSOCIATED_DEBTOR, async (args, { dispatch, rejectWithValue }) => {
  try {
    const {
      id,
      dataChange,
      debtorList,
      currentPage,
      pageSize,
      isExistRelation = false,
      isChange = true
    } = args;

    if (isChange) {
      const ssn =
        debtorList?.find(
          (item: Debtors) => item.debtorName === dataChange.debtorName
        )?.ssn || '';

      const { success } = await associatedDebtorController.editLinkDebtor(id, {
        ...dataChange,
        last4SSN: getLast4SSN(ssn)
      });

      if (!success) throw new Error('Update Link Debtor failed.');
    }

    batch(() => {
      if (isChange) {
        !isExistRelation &&
          dispatch(accountManagementActions.toggleFormLinkDebtors());
        dispatch(
          accountManagementActions.getAssociatedDebtors({
            page: currentPage,
            pageSize
          })
        );
      }

      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.DEBTOR_RELATION_UPDATED
        })
      );
    });
  } catch (error) {
    if (error?.message === 'EDIT FAILED') {
      dispatch(
        apiErrorDetailActions.setErrValue({
          id: editAssociatedDebtor.rejected.type,
          value: 'Something when wrong when editing!'
        })
      );
    }

    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.DEBTOR_RELATION_UPDATE_FAIL
      })
    );
    return rejectWithValue(error);
  }
});

export const editAssociatedDebtorBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { pending, fulfilled, rejected } = editAssociatedDebtor;
  builder
    .addCase(pending, draftState => {
      draftState.manageAssociatedDebtors.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      draftState.manageAssociatedDebtors.isLoading = false;
    })
    .addCase(rejected, draftState => {
      draftState.manageAssociatedDebtors.isLoading = false;
    });
};
