import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { I18N_TEXT } from 'app/constants';
import {
  Debtors,
  ILinkDebtor,
  InitialStateAccountManagement
} from 'pages/AccountManagement/types';
import { associatedDebtorController } from 'pages/AccountManagement/__mock__/associatedsDebtorsController';
// redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { accountManagementActions } from '..';
import { apiErrorDetailActions } from 'pages/__commons/ApiErrorDetail/_redux/reducer';

import { getLast4SSN } from 'pages/AccountManagement/ManageAssociatedDebtors/helpers';

const ADD_ASSOCIATED_DEBTOR = 'addAssociatedDebtor';

export const addAssociatedDebtor = createAsyncThunk<
  void,
  {
    newAssociatedDebtor: ILinkDebtor;
    debtorList: Debtors[];
    currentPage: number;
    pageSize: number;
    isExistRelation?: boolean;
    linkAnotherDebtor: boolean;
  },
  ThunkAPIConfig
>(ADD_ASSOCIATED_DEBTOR, async (args, { dispatch, rejectWithValue }) => {
  try {
    const { newAssociatedDebtor, debtorList, currentPage, pageSize, isExistRelation = false, linkAnotherDebtor } = args;

    const ssn =
      debtorList?.find(
        (item: Debtors) => item.debtorName === newAssociatedDebtor.debtorName
      )?.ssn || '';

    const { success } = await associatedDebtorController.addLinkDebtor({
      ...newAssociatedDebtor,
      last4SSN: getLast4SSN(ssn)
    });

    if (!success) throw new Error('Add Link Debtor failed.');

    batch(() => {
      !isExistRelation &&  dispatch(accountManagementActions.toggleFormLinkDebtors());
       if (linkAnotherDebtor) {
        dispatch(accountManagementActions.toggleFormLinkDebtors());
       }
       if (linkAnotherDebtor && isExistRelation) {
        dispatch(accountManagementActions.resetAndAddAnotherLinkDebtors(linkAnotherDebtor));
       }

      dispatch(
        accountManagementActions.getAssociatedDebtors({
          page: currentPage,
          pageSize
        })
      );
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.DEBTOR_LINKED
        })
      );
    });
  } catch (error) {
    if (error?.message === 'ADD FAILED') {
      dispatch(
        apiErrorDetailActions.setErrValue({
          id: addAssociatedDebtor.rejected.type,
          value: 'Something when wrong when adding!'
        })
      );
    }

    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.DEBTOR_LINK_FAIL
      })
    );
    return rejectWithValue(error);
  }
});

export const addAssociatedDebtorBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { pending, fulfilled, rejected } = addAssociatedDebtor;
  builder
    .addCase(pending, draftState => {
      draftState.manageAssociatedDebtors.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      draftState.manageAssociatedDebtors.isLoading = false;
    })
    .addCase(rejected, draftState => {
      draftState.manageAssociatedDebtors.isLoading = false;
    });
};
