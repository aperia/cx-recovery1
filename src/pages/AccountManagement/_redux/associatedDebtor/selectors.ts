import { createSelector } from '@reduxjs/toolkit';
import { FormAction } from 'pages/AccountManagement/ManageAssociatedDebtors/LinkDebtorModal';
import { ILinkDebtor } from 'pages/AccountManagement/types';

// Manage Associated Debtor selectors
export const selectLoadingLinkDebtors = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.manageAssociatedDebtors.isLoading
  ],
  (data: boolean) => data
);

export const selectedFormLinkDebtorsOpen = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.manageAssociatedDebtors.formOpen
  ],
  (data: boolean) => data
);

export const selectedFormValuesLinkDebtors = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.manageAssociatedDebtors.formValues
  ],
  (data: ILinkDebtor) => data
);

export const selectedFormActionLinkDebtors = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.manageAssociatedDebtors.formAction
  ],
  (data: FormAction) => data
);

export const selectedLinkDebtorsTotalData = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.manageAssociatedDebtors.totalItem
  ],
  (data: number) => data
);

export const selectedLinkDebtorsCurrentPage = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.manageAssociatedDebtors.currentPage
  ],
  (data: number) => data
);

export const selectedLinkDebtorsData = createSelector(
  [
    (states: RootState) => states.accountManagement.manageAssociatedDebtors.data
  ],
  (data: ILinkDebtor[]) => data
);

export const selectorIsOpenModalLinkDebtors = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.manageAssociatedDebtors.isOpenModal
  ],
  (data: boolean) => data
);

export const selectorLinkAnotherDebtor = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.manageAssociatedDebtors.linkAnotherDebtor
  ],
  (data: boolean) => data
);

export const selectorAllLinkDebtor = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.manageAssociatedDebtors.allAssociatedsDebtors
  ],
  (data: ILinkDebtor[]) => data
);