import { PayloadAction } from '@reduxjs/toolkit';
import {
  DEFAULT_LINK_DEBTOR,
  ILinkDebtor,
  InitialStateAccountManagement
} from 'pages/AccountManagement/types';

// Manage Associated Debtor
export const onSetModalLinkDebtors = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<boolean>
) => {
  draftState.manageAssociatedDebtors.isOpenModal = action.payload;
};

export const removeStoreLinkDebtors = (
  draftState: InitialStateAccountManagement
) => {
  draftState.manageAssociatedDebtors.formOpen = false;
  draftState.manageAssociatedDebtors.formValues = DEFAULT_LINK_DEBTOR;
};

export const toggleFormLinkDebtors = (
  draftState: InitialStateAccountManagement
) => {
  draftState.manageAssociatedDebtors.formOpen =
    !draftState.manageAssociatedDebtors.formOpen;
  draftState.manageAssociatedDebtors.formAction = 'ADD';
};

export const resetFormValuesLinkDebtors = (
  draftState: InitialStateAccountManagement
) => {
  draftState.manageAssociatedDebtors.formValues = DEFAULT_LINK_DEBTOR;
};

export const triggerEditLinkDebtors = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<ILinkDebtor>
) => {
  draftState.manageAssociatedDebtors.formOpen = true;
  draftState.manageAssociatedDebtors.formValues = action.payload;
  draftState.manageAssociatedDebtors.formAction = 'EDIT';
};

export const resetAndAddAnotherLinkDebtors = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<boolean>
) => {
  draftState.manageAssociatedDebtors.formOpen = true;
  draftState.manageAssociatedDebtors.formAction = 'ADD';
  draftState.manageAssociatedDebtors.formValues = DEFAULT_LINK_DEBTOR;
  draftState.manageAssociatedDebtors.linkAnotherDebtor = action.payload;
};

export const changeLinkAnotherDebtors = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<boolean>
) => {
  draftState.manageAssociatedDebtors.linkAnotherDebtor = action.payload;
};

export const triggerAddPrimaryLinkDebtors = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<ILinkDebtor>
) => {
  draftState.manageAssociatedDebtors.formOpen = true;
  draftState.manageAssociatedDebtors.formValues = action.payload;
  draftState.manageAssociatedDebtors.formAction = 'ADD';
};

export const clearListAssociatedDebtors = (draftState: InitialStateAccountManagement) => {
  draftState.manageAssociatedDebtors.data = [];
}