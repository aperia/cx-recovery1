import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

import { associatedDebtorController } from 'pages/AccountManagement/__mock__/associatedsDebtorsController';

import {
  ILinkDebtor,
  InitialStateAccountManagement
} from 'pages/AccountManagement/types';

interface IPayloadGetAssociatedsDebtors {
  associatedsDebtors: ILinkDebtor[];
  allAssociatedsDebtors: ILinkDebtor[];
  totalItem: number;
  currentPage: number;
  success: boolean;
}
const GET_ASSOCIATED_DEBTORS = 'getAssociatedDebtors';

export const getAssociatedDebtors = createAsyncThunk<
  IPayloadGetAssociatedsDebtors,
  { page: number; pageSize?: number },
  ThunkAPIConfig
>(GET_ASSOCIATED_DEBTORS, async (args, thunkAPI) => {
  const { page = 1, pageSize = 5 } = args;

  try {
    const response = await associatedDebtorController.getAssociatedDebtors({
      page,
      pageSize
    });
    return response.data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const getAssociatedDebtorsBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { pending, fulfilled, rejected } = getAssociatedDebtors;
  builder
    .addCase(pending, draftState => {
      draftState.manageAssociatedDebtors.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      const {
        associatedsDebtors,
        allAssociatedsDebtors,
        totalItem,
        currentPage
      } = action.payload;

      draftState.manageAssociatedDebtors.data = associatedsDebtors;
      draftState.manageAssociatedDebtors.allAssociatedsDebtors =
        allAssociatedsDebtors;
      draftState.manageAssociatedDebtors.totalItem = totalItem;
      draftState.manageAssociatedDebtors.currentPage = currentPage;
      draftState.manageAssociatedDebtors.isLoading = false;
    })
    .addCase(rejected, draftState => {
      draftState.manageAssociatedDebtors.isLoading = false;
    });
};
