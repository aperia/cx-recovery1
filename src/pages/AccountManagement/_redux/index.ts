import {
  CorrespondenceInitialState,
  IAccounts
} from 'pages/AccountManagement/types';
import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import {
  AccountListInitialState,
  DebtorMaintenanceInitialState,
  AssociatedDebtorsInitialState,
  InitialStateAccountManagement,
  ManualListInitialState,
  AccountManagementTabID
} from '../types';

// accountList actions
import {
  onChangeOrderBy,
  onChangeSortAccounts,
  onChangeStatus,
  onChangeSearchValue,
  onClearAndReset,
  removeAccountList,
  onSearchByAccReset,
  onChangeAtScreen,
  onChangeSelectorAccount,
  onSetModal,
  onToggleAccountFormModal,
  setSnapAcc,
  triggerAutoSearchByAccNumber
} from './accountList/actions';

import { saveAccount, saveAccountBuilder } from './accountList/saveAccount';

// associatedDebtor actions
import {
  onSetModalLinkDebtors,
  removeStoreLinkDebtors,
  toggleFormLinkDebtors,
  resetFormValuesLinkDebtors,
  triggerEditLinkDebtors,
  resetAndAddAnotherLinkDebtors,
  changeLinkAnotherDebtors,
  triggerAddPrimaryLinkDebtors,
  clearListAssociatedDebtors
} from './associatedDebtor/actions';

import { getAccounts, getAccountsBuilder } from './accountList/getAccounts';

// debtor maintenance actions

import {
  onChangeSearchDebtors,
  onChangeSortDebtors,
  onToggleFormModal,
  toggleDebtorDetailModal
} from './debtorMaintenance/actions';

import { getDebtors, getDebtorsBuilder } from './debtorMaintenance/getDebtors';
import {
  deleteDebtor,
  deleteDebtorBuilder
} from './debtorMaintenance/deleteDebtor';

import { saveDebtor, saveDebtorBuilder } from './debtorMaintenance/saveDebtor';

// manual assign recall actions
import {
  searchAccountByAcc,
  searchAccountByAccBuilder
} from './accountList/searchAccountByAcc';

import {
  getManualAssign,
  getManualAssignBuilder
} from './manualAssignRecall/getManualAssign';

import {
  addManualAssign,
  addManualAssignBuilder
} from './manualAssignRecall/addManualAssign';

import {
  getAddressHistory,
  getAddressHistoryBuilder
} from './debtorMaintenance/getAddressHistory';
import {
  getAssociatedDebtorsBuilder,
  getAssociatedDebtors
} from './associatedDebtor/getAssociatedDebtors';
import {
  deleteAssociatedsDebtorBuilder,
  deleteAssociatedsDebtor
} from './associatedDebtor/deleteAssociatedDebtor';
import {
  addAssociatedDebtorBuilder,
  addAssociatedDebtor
} from './associatedDebtor/addAssociatedDebtor';
import {
  editAssociatedDebtorBuilder,
  editAssociatedDebtor
} from './associatedDebtor/editAssociatedDebtor';

// correspondence
import {
  onChangeTabCorrespondence,
  onSetModalCorrespondence,
  onResetModalCorrespondence,
  onResetChangeTabCorrespondence,
  onCheckFormDiscard
} from './correspondence/actions';
import {
  onChangeLetterCodeCurrentPage,
  onChangeLetterCodePageSize,
  onChangeLetterCodeSearchValue,
  onChangeLetterCodeSortBy,
  onChangeLetterCodeType,
  onLetterCodeClearAndReset
} from './correspondence/modal/codeList/actions';
import {
  getCorrespondenceBuilder,
  getLetterCode
} from './correspondence/modal/codeList/getLetterCode';

import {
  onChangeCollapseSendCorrespondence,
  onSelectReceiverSendCorrespondence,
  onSendCorrespondenceClearAndReset
} from './correspondence/modal/sendCorrespondence/actions';

import {
  sendCorrespondence,
  sendCorrespondenceBuilder
} from './correspondence/modal/sendCorrespondence/submitSendCorrespondence';

import {
  onChangeModalPendingCorrespondenceCurrentPage,
  onChangeModalPendingCorrespondencePageSize,
  onChangeModalPendingCorrespondenceSearchValue,
  onModalPendingCorrespondenceClearAndReset
} from './correspondence/modal/pendingCorrespondence/actions';

import {
  getModalPendingCorrespondence,
  getModalPendingCorrespondenceBuilder
} from './correspondence/modal/pendingCorrespondence/getPendingCorrespondence';

import {
  onChangePendingCorrespondenceCurrentPage,
  onChangePendingCorrespondenceOrderBy,
  onChangePendingCorrespondencePageSize,
  onChangePendingCorrespondenceSearchValue,
  onChangePendingCorrespondenceSortBy,
  onPendingCorrespondenceClearAndReset
} from './correspondence/pendingCorrespondence/actions';

import {
  getPendingCorrespondence,
  getPendingCorrespondenceBuilder
} from './correspondence/pendingCorrespondence/getPendingCorrespondence';

import {
  removePendingCorrespondence,
  removePendingCorrespondenceBuilder
} from './correspondence/pendingCorrespondence/removePendingCorrespondence';

const initialState: InitialStateAccountManagement = {
  common: {
    activeTab: 'accountMaintenance',
    accSearchValue: {} as IAccounts
  },
  debtorMaintenance: DebtorMaintenanceInitialState,
  accountList: AccountListInitialState,
  manualAssignRecall: ManualListInitialState,
  manageAssociatedDebtors: AssociatedDebtorsInitialState,
  correspondence: CorrespondenceInitialState
};

export const { actions, reducer } = createSlice({
  name: 'accountManagement',
  initialState: initialState,
  reducers: {
    // accountList actions
    removeAccountList,
    onChangeSelectorAccount,
    onChangeAtScreen,
    onClearAndReset,
    onChangeSortAccounts,
    onChangeOrderBy,
    onChangeStatus,
    onChangeSearchValue,
    onSearchByAccReset,
    onSetModal,
    onToggleAccountFormModal,
    setSnapAcc,
    triggerAutoSearchByAccNumber,

    // Manage Associated Debtor actions
    onSetModalLinkDebtors,
    removeStoreLinkDebtors,
    toggleFormLinkDebtors,
    resetFormValuesLinkDebtors,
    triggerEditLinkDebtors,
    resetAndAddAnotherLinkDebtors,
    changeLinkAnotherDebtors,
    triggerAddPrimaryLinkDebtors,
    clearListAssociatedDebtors,

    //debtor Maintenance actions
    onChangeSearchDebtors,
    onChangeSortDebtors,
    onToggleFormModal,

    // action common
    onChangeTab: (
      draftState: InitialStateAccountManagement,
      action: PayloadAction<AccountManagementTabID>
    ) => {
      draftState.common.activeTab = action.payload;
    },

    // debtor maintenance
    toggleDebtorDetailModal,

    // correspondence
    onCheckFormDiscard,
    onChangeTabCorrespondence,
    onSetModalCorrespondence,
    onResetModalCorrespondence,
    onResetChangeTabCorrespondence,

    onChangeLetterCodeCurrentPage,
    onChangeLetterCodePageSize,
    onChangeLetterCodeSearchValue,
    onChangeLetterCodeSortBy,
    onChangeLetterCodeType,
    onChangeCollapseSendCorrespondence,
    onLetterCodeClearAndReset,

    onChangePendingCorrespondenceCurrentPage,
    onChangePendingCorrespondenceOrderBy,
    onChangePendingCorrespondencePageSize,
    onChangePendingCorrespondenceSearchValue,
    onChangePendingCorrespondenceSortBy,
    onPendingCorrespondenceClearAndReset,

    onChangeModalPendingCorrespondenceCurrentPage,
    onChangeModalPendingCorrespondencePageSize,
    onChangeModalPendingCorrespondenceSearchValue,
    onModalPendingCorrespondenceClearAndReset,

    onSelectReceiverSendCorrespondence,
    onSendCorrespondenceClearAndReset
  },

  extraReducers: builder => {
    getAccountsBuilder(builder);
    getManualAssignBuilder(builder);
    searchAccountByAccBuilder(builder);
    getDebtorsBuilder(builder);
    getAddressHistoryBuilder(builder);
    saveAccountBuilder(builder);
    addManualAssignBuilder(builder);

    // Manage Associated Debtor builders
    getAssociatedDebtorsBuilder(builder);
    deleteAssociatedsDebtorBuilder(builder);
    deleteDebtorBuilder(builder);
    saveDebtorBuilder(builder);
    addAssociatedDebtorBuilder(builder);
    editAssociatedDebtorBuilder(builder);

    // correspondence
    getCorrespondenceBuilder(builder);
    getModalPendingCorrespondenceBuilder(builder);
    getPendingCorrespondenceBuilder(builder);
    removePendingCorrespondenceBuilder(builder);
    sendCorrespondenceBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getManualAssign,
  getAccounts,
  searchAccountByAcc,
  addManualAssign,
  getDebtors,
  getAddressHistory,
  saveAccount,

  // Manage Associated Debtor actions
  getAssociatedDebtors,
  deleteAssociatedsDebtor,
  deleteDebtor,
  saveDebtor,
  addAssociatedDebtor,
  editAssociatedDebtor,

  // correspondence
  getLetterCode,
  getModalPendingCorrespondence,
  getPendingCorrespondence,
  removePendingCorrespondence,
  sendCorrespondence
};

export { combineActions as accountManagementActions };
