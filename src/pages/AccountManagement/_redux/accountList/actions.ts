import { PayloadAction } from '@reduxjs/toolkit';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import {
  AccountListInitialState,
  AtScreenComponentAccounts,
  IAccounts,
  InitialStateAccountManagement,
  orderByDefault,
  sortByDefault,
  statusDefault
} from 'pages/AccountManagement/types';

export const onChangeSortAccounts = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<RefDataValue>
) => {
  draftState.accountList.sortBy = action.payload;
};

export const onChangeOrderBy = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<RefDataValue>
) => {
  draftState.accountList.orderBy = action.payload;
};

export const onChangeStatus = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<'active' | 'purged'>
) => {
  draftState.accountList.status = action.payload;
};

export const onChangeSearchValue = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<AttributeSearchValue[]>
) => {
  draftState.accountList.searchValue = action.payload;
};

export const onClearAndReset = (draftState: InitialStateAccountManagement) => {
  draftState.accountList.searchValue = [];
  draftState.accountList.data = [];
  draftState.accountList.orderBy = orderByDefault;
  draftState.accountList.sortBy = sortByDefault;
  draftState.accountList.status = statusDefault;

  if (draftState.accountList.atScreen === 'findAccount') {
    draftState.accountList.selectedAccount = {} as IAccounts;
  }
};

export const removeAccountList = (
  draftState: InitialStateAccountManagement
) => {
  return { ...draftState, accountList: AccountListInitialState };
};

export const onSearchByAccReset = (
  draftState: InitialStateAccountManagement
) => {
  draftState.common.accSearchValue = {} as IAccounts;
};

export const onChangeAtScreen = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<AtScreenComponentAccounts>
) => {
  draftState.accountList.atScreen = action.payload;
};

export const onChangeSelectorAccount = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<IAccounts>
) => {
  draftState.accountList.selectedAccount = action.payload;
};

export const onSetModal = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<boolean>
) => {
  draftState.manualAssignRecall.isOpenModal = action.payload;
};

export const onToggleAccountFormModal = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<{
    selected: any;
  }>
) => {
  const { selected } = action.payload;

  const { accountForm } = draftState.accountList;

  draftState.accountList.accountForm = {
    ...accountForm,
    selected,
    isOpen: !accountForm.isOpen
  };
};

export const setSnapAcc = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<IAccounts>
) => {
  const {
    accNumber,
    ssn,
    originalLoanNbr,
    status,
    debtorName,
    originalLoan
  } = action.payload;
  const { snapAcc } = draftState.accountList;

  draftState.accountList.snapAcc = {
    ...snapAcc,
    accNumber,
    ssn,
    originalLoanNbr,
    status,
    debtorName,
    originalLoan
  };
};

export const triggerAutoSearchByAccNumber = (
  draftState: InitialStateAccountManagement,
  action: PayloadAction<string>
) => {
  draftState.accountList.searchValue = [
    {
      key: 'accountNumber',
      value: action.payload
    }
  ] as AttributeSearchValue[];
};