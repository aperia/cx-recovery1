import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// actions
import { accountManagementActions } from '..';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// constants
import { API_ERROR, I18N_TEXT } from 'app/constants';
import { FormikHelpers } from 'formik';
import { tabActions } from 'pages/__commons/TabBar/_redux';

// service
import accountListServices from 'pages/AccountManagement/AccountList/accountListServices';
import { genMashAccNumber } from 'pages/AccountManagement/helpers';
import { IAccounts } from 'pages/AccountManagement/types';
import debtorMaintenanceServices from 'pages/AccountManagement/DebtorMaintenance/debtorMaintenanceServices';

export interface IPayloadSaveAccount {}
export interface ISaveAccountArgs {
  id?: string;
  values: any;
  formikBag: FormikHelpers<any>;
}

export const saveAccount = createAsyncThunk<
  IPayloadSaveAccount,
  ISaveAccountArgs,
  ThunkAPIConfig
>('accountList/saveAccount', async (args, thunkAPI) => {
  const { id, values, formikBag } = args;
  const snapAcc = {
    debtorName: values?.cardHolderName,
    accNumber: {
      accountNumber: genMashAccNumber(values?.accountNumber || ''),
      accountNumberUnMask: values?.accountNumber || ''
    },
    ssn: values?.ssn || '',
    status: 'active',
    originalLoanNbr: values?.originalLoanNbr || '',
    originalLoan: {
      accountNumber: genMashAccNumber(values?.originalLoanNbr || ''),
      accountNumberUnMask: values?.originalLoanNbr || ''
    }
  };
  const { resetForm, setSubmitting } = formikBag;
  const { dispatch, getState } = thunkAPI;
  const { tab } = getState();

  try {
    dispatch(accountManagementActions.setSnapAcc(snapAcc as IAccounts));
    const response = await accountListServices.saveAccount({
      id,
      values: {
        ...values,
        debtorName: values?.cardHolderName,
      },
      formikBag
    });

    await debtorMaintenanceServices.saveDebtor({
      id,
      values: {
        ...values,
        debtorName: values?.cardHolderName,
        associatedAccountType: 'unlink'
      },
      formikBag
    });

    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: id
            ? I18N_TEXT.EDIT_ACCOUNT_SUCCESS
            : I18N_TEXT.ADD_ACCOUNT_SUCCESS
        })
      );

      // close modal
      dispatch(
        accountManagementActions.onToggleAccountFormModal({
          selected: undefined
        })
      );
    });

    // close account detail tab if exists
    const accountDetailTabStoreId = tab.tabs.find(
      tab => tab.tabType === 'accountDetail'
    )?.storeId;

    if (tab.tabStoreIdSelected === accountDetailTabStoreId) {
      dispatch(tabActions.removeTab({ storeId: tab.tabStoreIdSelected }));
    }

    dispatch(accountManagementActions.getDebtors({}));

    // open Modal Link Debtors
    if (!id) {
      dispatch(accountManagementActions.clearListAssociatedDebtors());
      dispatch(accountManagementActions.onSetModalLinkDebtors(true));
    }
    
    resetForm();

    return response;
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: id ? I18N_TEXT.EDIT_ACCOUNT_FAIL : I18N_TEXT.ADD_ACCOUNT_FAIL
      })
    );

    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  } finally {
    setSubmitting(false);
  }
});

export const saveAccountBuilder = (
  builder: ActionReducerMapBuilder<IPayloadSaveAccount>
) => {
  const { pending, fulfilled, rejected } = saveAccount;
  builder
    .addCase(pending, () => {})
    .addCase(fulfilled, () => {})
    .addCase(rejected, () => {});
};
