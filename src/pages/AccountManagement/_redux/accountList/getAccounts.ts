import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { STEP_SEQUENCE } from 'app/constants';
import { checkEmpty, isArrayHasValue } from 'app/helpers';
import accountListServices from 'pages/AccountManagement/AccountList/accountListServices';
import {
  InitialStateAccountManagement,
  PayloadGetAccounts
} from 'pages/AccountManagement/types';

export const getAccounts = createAsyncThunk<
  PayloadGetAccounts,
  { isLoadMore: boolean },
  ThunkAPIConfig
>('getAccounts', async (_, { getState, rejectWithValue }) => {
  try {
    const { searchValue, startSequence, endSequence } =
      getState().accountManagement.accountList;
    const { data } = await accountListServices.getAccountList(
      searchValue,
      startSequence,
      endSequence
    );

    return { data: data.data };
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getAccountsBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { pending, fulfilled, rejected } = getAccounts;
  builder
    .addCase(pending, (draftState, action) => {
      const { isLoadMore } = action.meta.arg;
      if (!isLoadMore) {
        draftState.accountList.isLoading = true;
      } else {
        draftState.accountList.isLoadingLoadMore = true;
      }
    })
    .addCase(fulfilled, (draftState, action) => {
      const { data } = action.payload;
      const { isLoadMore } = action.meta.arg;

      if (!isLoadMore) {
        draftState.accountList.isLoading = false;
      } else {
        draftState.accountList.isLoadingLoadMore = false;
      }

      draftState.accountList.data = checkEmpty([
        isLoadMore,
        data,
        [...draftState.accountList.data, ...data]
      ]);

      draftState.accountList.isEnd = !isArrayHasValue(data);
      draftState.accountList.isLoading = false;

      draftState.accountList.isError = checkEmpty([
        draftState.accountList.isError,
        false
      ]);

      draftState.accountList.endSequence = checkEmpty([
        isLoadMore,
        draftState.accountList.endSequence,
        draftState.accountList.endSequence + STEP_SEQUENCE
      ]);

      draftState.accountList.startSequence = checkEmpty([
        isLoadMore,
        draftState.accountList.startSequence,
        draftState.accountList.startSequence + STEP_SEQUENCE
      ]);
    })
    .addCase(rejected, draftState => {
      draftState.accountList.isLoading = false;
      draftState.accountList.isError = false;
    });
};
