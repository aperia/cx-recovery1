import { createSelector } from '@reduxjs/toolkit';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { filterAndOrderAccounts } from 'pages/AccountManagement/helpers';
import {
  AtScreenComponentAccounts,
  IAccountFormReducer,
  IAccounts,
  TypeStatusAccountList
} from 'pages/AccountManagement/types';

const getAccounts = (states: RootState): IAccounts[] =>
  states.accountManagement.accountList.data;

const getOrderBy = (states: RootState): RefDataValue =>
  states.accountManagement.accountList.orderBy;

const getSortBy = (states: RootState): RefDataValue =>
  states.accountManagement.accountList.sortBy;

const getStatus = (states: RootState): TypeStatusAccountList =>
  states.accountManagement.accountList.status;

const getAccountForm = (states: RootState): IAccountFormReducer => {
  return states.accountManagement.accountList.accountForm;
};

export const selectorAccountList = createSelector(
  getAccounts,
  getStatus,
  getSortBy,
  getOrderBy,
  (
    data: IAccounts[],
    status: TypeStatusAccountList,
    sortBy: RefDataValue,
    orderBy: RefDataValue
  ) => filterAndOrderAccounts(data, status, sortBy.value, orderBy.value)
);

export const selectorIsLoading = createSelector(
  [(states: RootState) => states.accountManagement.accountList.isLoading],
  (isLoading: boolean) => isLoading
);

export const selectorIsSearch = createSelector(
  [(states: RootState) => states.accountManagement.accountList.searchValue],
  (data: AttributeSearchValue[]) => data
);

export const selectorStatus = createSelector(
  getStatus,
  (data: TypeStatusAccountList) => data
);

export const selectorSortBy = createSelector(
  getSortBy,
  (data: RefDataValue) => data
);

export const selectorIsError = createSelector(
  [(states: RootState) => states.accountManagement.accountList.isError],
  (data: boolean) => data
);

export const selectorIsEnd = createSelector(
  [(states: RootState) => states.accountManagement.accountList.isEnd],
  (data: boolean) => data
);

export const selectorIsLoadingLoadMore = createSelector(
  [
    (states: RootState) =>
      states.accountManagement.accountList.isLoadingLoadMore
  ],
  (data: boolean) => data
);

export const selectorAtScreen = createSelector(
  [(states: RootState) => states.accountManagement.accountList.atScreen],
  (data: AtScreenComponentAccounts) => data
);

export const selectorOrderBy = createSelector(
  getOrderBy,
  (data: RefDataValue) => data
);

export const selectorSelectedAccount = createSelector(
  [(states: RootState) => states.accountManagement.accountList.selectedAccount],
  (data: IAccounts) => data
);

export const selectAccountFormIsOpen = createSelector(
  getAccountForm,
  (data: IAccountFormReducer): boolean => data.isOpen
);

export const selectSelectedAccountForm = createSelector(
  getAccountForm,
  (data: IAccountFormReducer): IAccounts | undefined => data.selected
);

export const selectSnapAcc = createSelector(
  [(states: RootState) => states.accountManagement.accountList.snapAcc],
  (data: IAccounts) => data
);