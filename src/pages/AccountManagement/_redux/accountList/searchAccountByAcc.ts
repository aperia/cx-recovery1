import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import accountServices from '../../accountServices';

// Utils
import {
  InitialStateAccountManagement,
  SearchAccountByAccArgs,
  SearchAccountByAccPayload
} from 'pages/AccountManagement/types';

export const searchAccountByAcc = createAsyncThunk<
  SearchAccountByAccPayload,
  SearchAccountByAccArgs,
  ThunkAPIConfig
>('searchAccountByAcc', async (args, thunkAPI) => {
  const { accNumber } = args;

  try {
    const { data } = await accountServices.searchAccountByAcc({ accNumber });

    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const searchAccountByAccBuilder = (
  builder: ActionReducerMapBuilder<InitialStateAccountManagement>
) => {
  const { pending, fulfilled, rejected } = searchAccountByAcc;
  builder
    .addCase(pending, draftState => {
      draftState.manualAssignRecall.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      const { data } = action.payload;
      draftState.common.accSearchValue = data;
      draftState.manualAssignRecall.isLoading = false;
    })
    .addCase(rejected, draftState => {
      draftState.manualAssignRecall.isLoading = false;
    });
};
