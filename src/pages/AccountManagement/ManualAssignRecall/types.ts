export interface ManualRecallTypeData {
  value: boolean;
  label: string;
}

export enum ListManualRecallType {
  ASSIGN = 'assign',
  RECALL = 'recall'
}

export interface ValuesManualProps {
  agencyCode?: string;
  lastRunDate?: string;
  feeCode?: string;
  collectorCode?: string;
  reasonCode?: string;
  statusCode?: string;
  newAgencyCode?: string;
  nextRunDate?: string;
  newFeeCode?: string;
  newCollectorCode?: string;
  newReasonCode?: string;
  newStatusCode?: string;
}
