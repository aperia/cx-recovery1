import { useState, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { accountManagementActions } from 'pages/AccountManagement/_redux';

export const useFindMoreForm = () => {
  const dispatch = useDispatch();
  const [openModalFindAccount, setOpen] = useState(false);

  const handleOpenFindAccount = useCallback(() => {
    setOpen(!openModalFindAccount);
  }, [openModalFindAccount]);

  const onSearchByAcc = useCallback(
    ({ accNumber }: { accNumber: string }) => {
      dispatch(
        accountManagementActions.searchAccountByAcc({
          accNumber
        })
      );
    },
    [dispatch]
  );

  return {
    openModalFindAccount,
    handleOpenFindAccount,
    onSearchByAcc
  };
};
