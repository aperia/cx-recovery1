import { useCallback, useEffect } from 'react';
import { useFormik, FormikProps } from 'formik';

// components
import { InputInformationForm } from '../Form/InformationForm';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import {
  selectorIsOpenModal,
  selectorSearchAcc
} from 'pages/AccountManagement/_redux/manualAssignRecall/selectors';
import { accountGroupActions } from 'pages/AccountGroup/_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import {
  checkNoneValue,
  returnEmptyValue
} from 'pages/AccountManagement/helpers';
import { manualRecallListSchema } from '../helpers';
import { formatTimeDefault } from 'app/helpers';

// types
import { IAccounts, IChangeManuals } from 'pages/AccountManagement/types';
import { ValuesManualProps } from '../types';
import { FormatTime } from 'app/constants/enums';
import { I18N_TEXT } from 'app/constants';

export interface ManualFormHook {
  (): {
    /**
     * Get data from manual item
     */
    formik: FormikProps<ValuesManualProps>;
    searchAcc: IAccounts;
    onSearchByAcc: ({ accNumber }: { accNumber: string }) => void;
    onOpenModal: (accNumber?: string) => void;
    onOpenModalCreateAccount: (account: IAccounts) => void;
    onCloseModal: () => void;
    CURRENT_INFORMATION_FIELD: InputInformationForm[];
    NEW_INFORMATION_FIELD: InputInformationForm[];
  };
}

export const useManualForm: ManualFormHook = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const searchAcc: IAccounts = useSelector(selectorSearchAcc);
  const isOpen = useSelector(selectorIsOpenModal);
  // Ref options data
  const { data: agencyCodeRef } = useGetRefDataQuery('agencyCode');
  const { data: feeCodeRef } = useGetRefDataQuery('feeCode');
  const { data: collectorCodeRef } = useGetRefDataQuery('collectorCode');
  const { data: reasonCodeRef } = useGetRefDataQuery('reasonCode');
  const { data: statusCodeRef } = useGetRefDataQuery('statusCode');

  /**
   * Simulate data field for current data
   */
  const CURRENT_INFORMATION_FIELD: InputInformationForm[] = [
    {
      label: t(I18N_TEXT.AGENCY_CODE),
      name: 'agencyCode',
      options: agencyCodeRef?.agencyCode,
      readOnly: true
    },
    {
      label: t(I18N_TEXT.LAST_RUN_DATE),
      name: 'lastRunDate',
      isDatePicker: true,
      readOnly: true
    },
    {
      label: t(I18N_TEXT.FEE_CODE),
      name: 'feeCode',
      options: feeCodeRef?.feeCode,
      readOnly: true
    },
    {
      label: t(I18N_TEXT.COLLECTOR_CODE),
      name: 'collectorCode',
      options: collectorCodeRef?.collectorCode,
      readOnly: true
    },
    {
      label: t(I18N_TEXT.REASON_CODE),
      name: 'reasonCode',
      options: reasonCodeRef?.reasonCode,
      readOnly: true
    },
    {
      label: t(I18N_TEXT.STATUS_CODE),
      name: 'statusCode',
      options: statusCodeRef?.statusCode,
      readOnly: true
    },
    {
      label: 'credit',
      name: 'credit',
      isCredit: true,
      readOnly: true
    }
  ];

  /**
   * Simulate data field for new data
   */
  const NEW_INFORMATION_FIELD: InputInformationForm[] = [
    {
      label: t(I18N_TEXT.AGENCY_CODE),
      name: 'newAgencyCode',
      options: checkNoneValue(searchAcc?.agencyCode, agencyCodeRef?.agencyCode),
      require: true
    },
    {
      label: t(I18N_TEXT.NEXT_RUN_DATE),
      name: 'nextRunDate',
      isDatePicker: true,
      readOnly: true
    },
    {
      label: t(I18N_TEXT.FEE_CODE),
      name: 'newFeeCode',
      options: checkNoneValue(searchAcc?.feeCode, feeCodeRef?.feeCode)
    },
    {
      label: t(I18N_TEXT.COLLECTOR_CODE),
      name: 'newCollectorCode',
      options: checkNoneValue(
        searchAcc?.collectorCode,
        collectorCodeRef?.collectorCode
      )
    },
    {
      label: t(I18N_TEXT.REASON_CODE),
      name: 'newReasonCode',
      options: checkNoneValue(searchAcc?.reasonCode, reasonCodeRef?.reasonCode)
    },
    {
      label: t(I18N_TEXT.STATUS_CODE),
      name: 'newStatusCode',
      options: checkNoneValue(searchAcc?.statusCode, statusCodeRef?.statusCode)
    },
    {
      label: 'newCredit',
      name: 'newCredit',
      isCredit: true
    }
  ];

  const formik = useFormik<ValuesManualProps>({
    initialValues: {
      agencyCode: searchAcc?.agencyCode,
      lastRunDate: searchAcc?.lastRunDate,
      feeCode: returnEmptyValue(searchAcc?.feeCode),
      collectorCode: returnEmptyValue(searchAcc?.collectorCode),
      reasonCode: returnEmptyValue(searchAcc?.reasonCode),
      statusCode: returnEmptyValue(searchAcc?.statusCode),
      nextRunDate: formatTimeDefault(
        new Date().toLocaleDateString(),
        FormatTime.Date
      ),
      newFeeCode: searchAcc?.feeCode,
      newCollectorCode: searchAcc?.collectorCode,
      newReasonCode: searchAcc?.reasonCode,
      newStatusCode: searchAcc?.statusCode
    },
    validateOnBlur: true,
    validationSchema: manualRecallListSchema,
    enableReinitialize: true,
    onSubmit: values => {
      dispatch(
        accountManagementActions.addManualAssign({
          accNumber: searchAcc.id,
          accountData: values as IChangeManuals
        })
      );
    }
  });

  const onSearchByAcc = ({ accNumber }: { accNumber: string }) => {
    dispatch(accountManagementActions.searchAccountByAcc({ accNumber }));
  };

  const onOpenModal = useCallback(
    accNumber => {
      accNumber &&
        dispatch(accountManagementActions.searchAccountByAcc({ accNumber }));
      dispatch(accountManagementActions.onSetModal(true));
    },
    [dispatch]
  );

  const onOpenModalCreateAccount = useCallback(
    (account: IAccounts) =>
      dispatch(
        accountGroupActions.setModalCreateAccountGroup({
          isOpenedModal: true,
          modalProps: {
            account
          }
        })
      ),
    [dispatch]
  );

  const onCloseModal = useCallback(() => {
    dispatch(accountManagementActions.onSetModal(false));
    dispatch(accountManagementActions.onSearchByAccReset());
  }, [dispatch]);

  useEffect(() => {
    !isOpen && dispatch(accountManagementActions.onSearchByAccReset());
  }, [dispatch, isOpen]);

  return {
    searchAcc,
    formik,
    onOpenModal,
    onOpenModalCreateAccount,
    onCloseModal,
    onSearchByAcc,
    CURRENT_INFORMATION_FIELD,
    NEW_INFORMATION_FIELD
  };
};
