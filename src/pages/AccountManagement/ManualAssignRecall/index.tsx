import React, {
  useCallback,
  useEffect,
  useLayoutEffect,
  useRef,
  useState
} from 'react';
import { genAmtId } from 'app/_libraries/_dls/utils';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Components
import ManualRecallType from './ManualRecallType';
import ManualRecallItem from './ManualRecallItem';
import {
  Button,
  InlineMessage,
  SimpleBar
} from 'app/_libraries/_dls/components';
import { NoDataGrid } from 'app/components';
import If from 'pages/__commons/If';

// hooks
import { useManualForm } from './hooks/useManualForm';
import {
  selectorAssignRecallList,
  selectorIsLoading
} from '../_redux/manualAssignRecall/selectors';

// Utils
import { I18N_TEXT } from 'app/constants';
import { DATA_TEST_ID_ACCOUNT } from '../constant';
import { IManuals } from '../types';
import classNames from 'classnames';
import { useSelector } from 'app/hooks';
import { checkCurrentAgencyCode } from 'pages/__commons/helpers';

const ManualRecallList: React.FC = () => {
  const { t } = useTranslation();
  const divRef = useRef<HTMLDivElement | null>(null);
  const [isAssign, setIsAssign] = useState<boolean>(true);
  const [isRecall, setIsRecall] = useState<boolean>(true);
  const [listItem, setListItem] = useState<IManuals[]>([]);

  const isLoading: boolean = useSelector(selectorIsLoading);
  const assignRecallList: IManuals[] = useSelector(selectorAssignRecallList);

  const { onOpenModal } = useManualForm();

  const onChangeType = useCallback(
    (typeAssignRecall: 'assign' | 'recall') =>
      typeAssignRecall === 'assign'
        ? setIsAssign(!isAssign)
        : setIsRecall(!isRecall),
    [isAssign, isRecall]
  );

  useEffect(() => {
    if (isAssign && isRecall) setListItem(assignRecallList);
    else if (isAssign)
      setListItem(
        assignRecallList.filter(
          item => checkCurrentAgencyCode(item.agencyCode) === 'assign'
        )
      );
    else if (isRecall)
      setListItem(
        assignRecallList.filter(
          item => checkCurrentAgencyCode(item.agencyCode) === 'recall'
        )
      );
    else setListItem([]);
  }, [isAssign, isRecall, assignRecallList]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 193px)`);
  }, []);

  return (
    <div ref={divRef}>
      <SimpleBar>
        <div
          className={classNames('px-24 pb-24 pt-22 mx-auto max-width-lg', {
            loading: isLoading,
            'pt-24': !listItem.length
          })}
        >
          <div className="row align-items-center">
            <div className="col-lg-6 col-md-12">
              <h5 data-testid={genAmtId(DATA_TEST_ID_ACCOUNT, 'title', '')}>
                {t(I18N_TEXT.MANUAL_ASSIGN_RECALL_REQUESTS_LIST)}
              </h5>
            </div>
            <div className="col-md-6 order-lg-1 order-md-2 my-md-16 my-lg-0 text-right">
              <If condition={listItem.length > 0}>
                <Button
                  className="text-capitalize mr-n8"
                  size="sm"
                  variant="outline-primary"
                  onClick={() => onOpenModal()}
                >
                  {t(I18N_TEXT.ADD_MANUAL_ASSIGN_RECALL_REQUESTS)}
                </Button>
              </If>
            </div>
            {assignRecallList.length > 0 && (
              <>
                <div className="col-md-6 order-lg-2 order-md-1 my-16">
                  <ManualRecallType
                    label="Type"
                    assignValue={isAssign}
                    recallValue={isRecall}
                    onChangeType={onChangeType}
                  />
                </div>
                <div className="col-md-12 mb-0 order-md-3">
                  <InlineMessage className="mb-0" variant="warning">
                    {t(I18N_TEXT.MODIFIED_WARNING)}
                  </InlineMessage>
                </div>
              </>
            )}
          </div>
          {listItem.length > 0 ? (
            listItem.map((item: IManuals) => (
              <ManualRecallItem key={item.id} data={item} />
            ))
          ) : (
            <NoDataGrid text={t(I18N_TEXT.NO_REQUEST_TO_DISPLAY)}>
              <Button
                className="mt-24"
                variant="outline-primary"
                size="sm"
                onClick={() => onOpenModal()}
              >
                {t(I18N_TEXT.ADD_MANUAL_ASSIGN_RECALL_REQUESTS)}
              </Button>
            </NoDataGrid>
          )}
        </div>
      </SimpleBar>
    </div>
  );
};

export default ManualRecallList;
