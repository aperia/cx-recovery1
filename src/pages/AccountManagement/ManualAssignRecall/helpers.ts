import * as yup from 'yup';

export const manualRecallListSchema = yup.object().shape({
  newAgencyCode: yup.string().required('txt_agency_code_is_required')
});
