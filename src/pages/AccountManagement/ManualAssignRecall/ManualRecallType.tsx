import React from 'react';

// Components
import { CheckBox } from 'app/_libraries/_dls/components';

// Utils
import { ListManualRecallType } from '../types';

interface ManualRecallTypeProps {
  label: string;
  assignValue: boolean;
  recallValue: boolean;
  onChangeType: (type: 'assign' | 'recall') => void;
}

const ManualRecallType: React.FC<ManualRecallTypeProps> = ({
  label,
  assignValue,
  recallValue,
  onChangeType
}) => {
  return (
    <div className="d-flex align-items-center">
      <span className="color-grey text-capitalize mr-4 fw-500">{label}:</span>
      <CheckBox onChange={() => onChangeType('assign')} className="ml-24">
        <CheckBox.Input checked={assignValue} />
        <CheckBox.Label className="text-capitalize">
          {ListManualRecallType.ASSIGN}
        </CheckBox.Label>
      </CheckBox>
      <CheckBox onChange={() => onChangeType('recall')} className="ml-24">
        <CheckBox.Input checked={recallValue} />
        <CheckBox.Label className="text-capitalize">
          {ListManualRecallType.RECALL}
        </CheckBox.Label>
      </CheckBox>
    </div>
  );
};

export default ManualRecallType;
