import React from 'react';

// Components
import { I18N_TEXT } from 'app/constants';
import {
  Modal,
  ModalBody,
  ModalHeader,
  ModalTitle,
  ModalFooter,
  Button
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import AccountList from '../AccountList';
import { selectorSelectedAccount } from '../_redux/accountList/selector';
import { useSelector } from 'app/hooks';
import { isEmpty } from 'app/helpers';

interface FindMoreProps {
  openModalFindAccount: () => void;
  onSearch: ({ accNumber }: { accNumber: string }) => void;
}

const FindAccount: React.FC<FindMoreProps> = ({
  onSearch,
  openModalFindAccount
}) => {
  const { t } = useTranslation();
  const selectedAccount = useSelector(selectorSelectedAccount);

  const handleSubmit = () => {
    onSearch({
      accNumber: selectedAccount.accNumber.accountNumberUnMask
    });
    openModalFindAccount();
  };

  return (
    <Modal lg show={true} loading={false} enforceFocus={false}>
      <ModalHeader
        border
        onHide={openModalFindAccount}
        closeButton
        className="border-bottom"
      >
        <ModalTitle className="text-capitalize">
          {t(I18N_TEXT.FIND_ACCOUNT)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody className="find-account">
        <AccountList atScreen="findAccount" />
      </ModalBody>
      <ModalFooter>
        <Button
          onClick={openModalFindAccount}
          variant="secondary"
          className="text-capitalize"
        >
          {t(I18N_TEXT.CANCEL)}
        </Button>
        <Button
          variant="primary"
          onClick={handleSubmit}
          className="text-capitalize"
          disabled={isEmpty(selectedAccount)}
        >
          {t(I18N_TEXT.CONTINUE)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default FindAccount;
