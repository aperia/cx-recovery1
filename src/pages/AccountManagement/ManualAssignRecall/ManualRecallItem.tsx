import React, { useCallback, useState } from 'react';

// Hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Components
import { View } from 'app/_libraries/_dof/core';
import { Button, Popover } from 'app/_libraries/_dls/components';

// Utils
import { I18N_TEXT } from 'app/constants';
import { IManuals } from '../types';
import classNames from 'classnames';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import {
  convertValueDescription,
  getRefDataFromCode
} from 'pages/__commons/helpers';

interface ManualRecallItemProps {
  data: IManuals;
}

const ManualRecallItem: React.FC<ManualRecallItemProps> = ({ data }) => {
  const { t } = useTranslation();
  const [open, setOpen] = useState(false);

  const { data: agencyCodeRef } = useGetRefDataQuery('agencyCode');
  const { data: feeCodeRef } = useGetRefDataQuery('feeCode');
  const { data: collectorCodeRef } = useGetRefDataQuery('collectorCode');
  const { data: reasonCodeRef } = useGetRefDataQuery('reasonCode');
  const { data: statusCodeRef } = useGetRefDataQuery('statusCode');

  const customDataView = {
    ...data,
    type: data.agencyCode,
    agencyCode: convertValueDescription(
      getRefDataFromCode(agencyCodeRef?.agencyCode, data.agencyCode)
    ),
    feeCode: convertValueDescription(
      getRefDataFromCode(feeCodeRef?.feeCode, data.feeCode)
    ),
    collectorCode: convertValueDescription(
      getRefDataFromCode(collectorCodeRef?.collectorCode, data.collectorCode)
    ),
    reasonCode: convertValueDescription(
      getRefDataFromCode(reasonCodeRef?.reasonCode, data.reasonCode)
    ),
    statusCode: convertValueDescription(
      getRefDataFromCode(statusCodeRef?.statusCode, data.statusCode)
    ),
    newAgencyCode: convertValueDescription(
      getRefDataFromCode(agencyCodeRef?.agencyCode, data.newAgencyCode)
    ),
    newFeeCode: convertValueDescription(
      getRefDataFromCode(feeCodeRef?.feeCode, data.newFeeCode)
    ),
    newCollectorCode: convertValueDescription(
      getRefDataFromCode(collectorCodeRef?.collectorCode, data.newCollectorCode)
    ),
    newReasonCode: convertValueDescription(
      getRefDataFromCode(reasonCodeRef?.reasonCode, data.newReasonCode)
    ),
    newStatusCode: convertValueDescription(
      getRefDataFromCode(statusCodeRef?.statusCode, data.newStatusCode)
    )
  } as IManuals;

  const handleOpenView = useCallback(() => {
    setOpen(!open);
  }, [open]);

  return (
    <div className="card br-radius-8 p-16 mt-16">
      <div className="row align-items-center">
        <div className="col-xl-10 col-lg-11 col-md-12">
          <View
            formKey={`manualRecallList-${data.id}`}
            descriptor="manualRecallList"
            value={{
              ...data,
              status: data.agencyCode
            }}
          />
        </div>
        <div className="col-xl-2 col-lg-1 text-right button-md-view">
          <Popover
            triggerClassName="mr-n8"
            placement="left"
            id={data.id}
            opened={open}
            size="lg"
            onVisibilityChange={handleOpenView}
            element={
              <View
                formKey={`viewManualAssignRecall- ${data.id}`}
                descriptor="viewManualAssignRecall"
                value={customDataView}
              />
            }
          >
            <Button
              size="sm"
              className={classNames({
                active: open
              })}
              variant="outline-primary"
              onClick={handleOpenView}
              onBlur={handleOpenView}
            >
              {t(I18N_TEXT.VIEW)}
            </Button>
          </Popover>
        </div>
      </div>
    </div>
  );
};

export default ManualRecallItem;
