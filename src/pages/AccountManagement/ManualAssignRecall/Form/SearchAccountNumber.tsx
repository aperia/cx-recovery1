import React, { useState, useEffect, useCallback } from 'react';
import { Button, InlineMessage, TextBox } from 'app/_libraries/_dls/components';
import { I18N_TEXT, MAX_LENGTH_16 } from 'app/constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { IAccounts } from '../../types';
import { MESSAGE } from '../../constant';
import classNames from 'classnames';
import { useFindMoreForm } from '../hooks/useFindMoreForm';
import FindAccount from '../FindAccount';

interface SearchAccountNumberType {
  className?: string;
  searchAcc: IAccounts;
  onSearch: ({ accNumber }: { accNumber: string }) => void;
}

const SearchAccountNumber: React.FC<SearchAccountNumberType> = ({
  className = '',
  searchAcc,
  onSearch
}) => {
  const { t } = useTranslation();
  const [value, setValue] = useState<string>('');
  const [error, setError] = useState<boolean>(false);
  const { openModalFindAccount, handleOpenFindAccount } = useFindMoreForm();

  const onChangeValue = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const valueEvent = event.target.value;
      const regex = /^[0-9]*$/g;
      if (regex.test(valueEvent)) {
        setValue(valueEvent);
      }
    },
    []
  );

  const onSearchHandler = useCallback(
    ({ accNumber }: { accNumber: string }) => {
      onSearch({ accNumber });
    },
    [onSearch]
  );

  const onFindMoreHandler = useCallback(() => {
    setError(false);
    handleOpenFindAccount();
  }, [handleOpenFindAccount]);

  const onBlurSearch = () => {
    value.length < MAX_LENGTH_16 ? setError(true) : setError(false);
  };

  useEffect(() => {
    if (searchAcc) {
      setValue('');
    }
  }, [searchAcc]);

  return (
    <>
      <div className={className}>
        {!searchAcc && (
          <InlineMessage variant="danger">
            {t(I18N_TEXT.ACCOUNT_NUMBER_NOT_EXIST)}
          </InlineMessage>
        )}
        <div className="d-flex align-items-center w-100 group-find-account">
          <div className="d-flex">
            <TextBox
              value={value}
              size="sm"
              placeholder={t(I18N_TEXT.ENTER_ACCOUNT_NUMBER)}
              className={classNames('w-320px', !searchAcc && 'dls-error')}
              maxLength={MAX_LENGTH_16}
              error={{
                message: t(MESSAGE.ACCOUNT_NUMBER_SEARCH),
                status: error
              }}
              errorTooltipProps={{
                variant: 'error'
              }}
              onChange={onChangeValue}
              onBlur={onBlurSearch}
              allowAutoFocus
            />
            <Button
              variant="primary"
              size="sm"
              className="ml-8 mr-16 text-capitalize"
              type="submit"
              disabled={value.length !== MAX_LENGTH_16}
              onClick={() => onSearchHandler({ accNumber: value })}
            >
              {t(I18N_TEXT.CONTINUE)}
            </Button>
          </div>
          <div className="d-flex align-items-center">
            <span>or</span>
            <Button
              onClick={onFindMoreHandler}
              size="sm"
              variant="outline-primary"
              className="ml-8"
            >
              {t(I18N_TEXT.FIND_ACCOUNT)}
            </Button>
          </div>
        </div>
      </div>
      {openModalFindAccount && (
        <FindAccount
          onSearch={onSearch}
          openModalFindAccount={handleOpenFindAccount}
        />
      )}
    </>
  );
};

export default SearchAccountNumber;
