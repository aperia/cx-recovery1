import React from 'react';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { DatePickerControl, ComboBoxControl } from 'app/components';
import { IAccounts, IManuals } from 'pages/AccountManagement/types';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import { getRefDataFromCode } from 'pages/__commons/helpers';
import { useFormikContext } from 'formik';
import { ValuesManualProps } from '../types';

export interface InputInformationForm {
  label: string;
  name: string;
  options?: RefDataValue[];
  readOnly?: boolean;
  isDatePicker?: boolean;
  isCredit?: boolean;
  require?: boolean;
}

interface InformationFormProps {
  heading?: string;
  data: InputInformationForm[];
  accData?: IAccounts | IManuals;
}

const InformationForm: React.FC<InformationFormProps> = ({
  heading,
  data,
  accData
}) => {
  const { t } = useTranslation();

  const { data: typeRef } = useGetRefDataQuery('type');

  const { values } = useFormikContext<ValuesManualProps>();

  return (
    <>
      <h6 className="color-grey">{t(heading)}</h6>
      {data?.map(item => {
        if (item.isDatePicker) {
          return (
            <DatePickerControl
              key={item.name}
              label={item.label}
              name={item.name}
              className="mt-16"
              readOnly={item.readOnly}
              required={item.require}
            />
          );
        } else if (item.isCredit) {
          const checkReadOnly = item.readOnly
            ? values.statusCode
            : values.newStatusCode;
          return (
            <p className="color-grey-l24 fs-12 mt-8">{`Type: ${
              getRefDataFromCode(typeRef?.type, checkReadOnly)?.description ||
              checkReadOnly
            } • Credit Bureau: ${accData?.credit} `}</p>
          );
        } else {
          return (
            <ComboBoxControl
              id={item.label}
              key={item.name}
              label={item.label}
              name={item.name}
              className="mt-16 text-capitalize"
              readOnly={item.readOnly}
              options={item.options || []}
              required={item.require}
              isCombine
            />
          );
        }
      })}
    </>
  );
};

export default InformationForm;
