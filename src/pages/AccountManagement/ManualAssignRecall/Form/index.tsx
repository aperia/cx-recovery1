import React, { useLayoutEffect, useRef, useState } from 'react';

// Components
import {
  Button,
  Modal,
  ModalBody,
  ModalHeader,
  ModalFooter,
  ModalTitle,
  SimpleBar,
  Tooltip,
  Icon
} from 'app/_libraries/_dls/components';
import { AccDetailSnapshot } from './AccDetailSnapshot';
import { AssignmentDetailsForm } from './AssignmentDetailsForm';
import SearchAccountNumber from './SearchAccountNumber';
// import { InputInformationForm } from './InformationForm';

// Hooks
import { FormikProvider } from 'formik';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// Utils
import { I18N_TEXT } from 'app/constants';
import NoteCode from 'pages/__commons/NoteCode';
import { NoDataGrid } from 'app/components';
import { isEmpty } from 'app/helpers';
import classNames from 'classnames';
import { useManualForm } from '../hooks/useManualForm';
import { useSelector } from 'app/hooks';
import { selectorIsOpenModal } from 'pages/AccountManagement/_redux/manualAssignRecall/selectors';

export interface ManualAssignRecallReqFormProps {
  isSearch?: boolean;
}

export const ManualAssignRecallReqForm: React.FC<ManualAssignRecallReqFormProps> =
  ({ isSearch }) => {
    const { t } = useTranslation();
    const contentRef = useRef<HTMLDivElement | null>(null);
    const contentLeftRef = useRef<HTMLDivElement | null>(null);
    const open = useSelector(selectorIsOpenModal);

    const {
      onCloseModal,
      formik,
      searchAcc,
      onSearchByAcc,
      CURRENT_INFORMATION_FIELD,
      NEW_INFORMATION_FIELD
    } = useManualForm();

    const [collapse, isCollapse] = useState(false);
    const [widthWindow, setWidthWindow] = useState(0);

    const handleClick = () => {
      isCollapse(!collapse);
    };

    useLayoutEffect(() => {
      contentRef.current &&
        isSearch &&
        (contentRef.current.style.height = `calc(100% - 61px)`);
      contentLeftRef.current &&
        isSearch &&
        (contentLeftRef.current.style.height = `calc(100vh - 191px)`);
    }, [isSearch, searchAcc]);

    useLayoutEffect(() => {
      const handleResize = () => {
        setWidthWindow(window.innerWidth);

        widthWindow > 1023 ? isCollapse(false) : isCollapse(true);
      };

      handleResize();

      window.addEventListener('resize', handleResize);
      return () => {
        window.removeEventListener('resize', handleResize);
      };
    }, [widthWindow]);

    const formAccount = () => (
      <div className="h-100">
        {isSearch && (
          <div className="d-flex border-bottom">
            <SearchAccountNumber
              className="max-width-lg mx-auto px-24 py-16 flex-1"
              searchAcc={searchAcc}
              onSearch={onSearchByAcc}
            />
            <div className="w-408px w-md-0"></div>
          </div>
        )}
        {isEmpty(searchAcc) ? (
          <div className="py-40">
            <NoDataGrid text={t(I18N_TEXT.ENTER_ACCOUNT_NUMBER_DESCRIPTION)} />
          </div>
        ) : (
          <div ref={contentRef} className="content-block">
            <div ref={contentLeftRef} className="content-left">
              <SimpleBar>
                <AccDetailSnapshot accData={searchAcc} />
                <FormikProvider value={formik}>
                  <AssignmentDetailsForm
                    accData={searchAcc}
                    currentDataField={CURRENT_INFORMATION_FIELD}
                    newDataField={NEW_INFORMATION_FIELD}
                  />
                </FormikProvider>
              </SimpleBar>

              <ModalFooter border>
                <Button variant="secondary" onClick={onCloseModal}>
                  {t(I18N_TEXT.CANCEL)}
                </Button>
                <Button
                  variant="primary"
                  onClick={formik.submitForm}
                  disabled={!isEmpty(formik.errors) || !formik.dirty}
                >
                  {t(I18N_TEXT.SUBMIT)}
                </Button>
              </ModalFooter>
            </div>
            <div
              className={classNames('content-right', { collapsed: collapse })}
            >
              <span className="collapse-button" onClick={handleClick}>
                <Tooltip
                  triggerClassName="d-flex"
                  variant="primary"
                  placement="left"
                  element={
                    collapse ? t(I18N_TEXT.NOTES) : t(I18N_TEXT.COLLAPSE)
                  }
                >
                  <Icon name={collapse ? 'comment' : 'push-right'} />
                </Tooltip>
              </span>
              <NoteCode />
            </div>
          </div>
        )}
      </div>
    );

    return (
      <Modal full show={open} loading={false} enforceFocus={false}>
        <ModalHeader
          border
          onHide={onCloseModal}
          closeButton
          className="border-bottom"
        >
          <ModalTitle className="d-flex align-items-center">
            {t(I18N_TEXT.ADD_MANUAL_ASSIGN_RECALL_REQUESTS)}
          </ModalTitle>
        </ModalHeader>
        <ModalBody noPadding className="overflow-hidden">
          {formAccount()}
        </ModalBody>
        <ModalFooter
          className={classNames({ 'd-none': !isEmpty(searchAcc) })}
          border
        >
          <Button variant="secondary" onClick={onCloseModal}>
            {t(I18N_TEXT.CANCEL)}
          </Button>
          <Button
            variant="primary"
            onClick={formik.submitForm}
            disabled={!isEmpty(formik.errors) || isEmpty(searchAcc)}
          >
            {t(I18N_TEXT.SUBMIT)}
          </Button>
        </ModalFooter>
      </Modal>
    );
  };
