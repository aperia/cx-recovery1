import React from 'react';
import { IAccounts } from '../../types';
import { View } from 'app/_libraries/_dof/core';
import ApiErrorDetail from 'pages/__commons/ApiErrorDetail';
import { addManualAssign } from 'pages/AccountManagement/_redux/manualAssignRecall/addManualAssign';
import { getLast4SSN } from 'pages/AccountManagement/ManageAssociatedDebtors/helpers';

export interface AccDetailSnapshotProps {
  accData: IAccounts;
}

export const AccDetailSnapshot: React.FC<AccDetailSnapshotProps> = ({
  accData
}) => {
  return (
    <div className="bg-light-l20 border-bottom">
      <div className="p-24 max-width-lg mx-auto">
        <ApiErrorDetail
          forAction={[addManualAssign.rejected.type]}
          className="mb-16"
        />
        <View
          formKey={`manualAssignRecallReq-${accData?.id}`}
          descriptor="manualAssignRecallReq"
          value={{ ...accData, last4SSN: getLast4SSN(accData?.ssn || '') }}
        />
      </div>
    </div>
  );
};
