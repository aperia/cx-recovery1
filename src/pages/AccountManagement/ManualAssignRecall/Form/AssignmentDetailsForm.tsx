import React from 'react';
import { Icon } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_TEXT } from 'app/constants';
import { IAccounts, IManuals } from '../../types';
import InformationForm, { InputInformationForm } from './InformationForm';

export interface AssignmentDetailsFormProps {
  accData: IAccounts | IManuals;
  currentDataField: InputInformationForm[];
  newDataField: InputInformationForm[];
}

export const AssignmentDetailsForm: React.FC<AssignmentDetailsFormProps> = ({
  accData,
  currentDataField,
  newDataField
}) => {
  const { t } = useTranslation();
  return (
    <div className="p-24 max-width-lg mx-auto">
      <h5 className="text-capitalize">
        {t(I18N_TEXT.ASSIGNMENT_DETAILS)}
      </h5>
      <div className="mt-16 assignment-detail">
        <div className="detail-left">
          <InformationForm
            accData={accData}
            heading={t(I18N_TEXT.CURRENT_INFORMATION)}
            data={currentDataField}
          />
        </div>

        <div className="detail-center">
          <Icon name="arrow-right" size="6x" className="color-grey-l16" />
        </div>
        <div className="detail-right">
          <InformationForm
            accData={accData}
            heading={t(I18N_TEXT.NEW_INFORMATION)}
            data={newDataField}
          />
        </div>
      </div>
    </div>
  );
};
