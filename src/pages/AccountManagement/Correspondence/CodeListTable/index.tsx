import React, { Fragment, useMemo } from 'react';

// components
import {
  ColumnType,
  Grid,
  Pagination,
  Button,
  pageSizeInfo
} from 'app/_libraries/_dls/components';
import If from 'pages/__commons/If';
import FailedApiReload from 'app/components/FailedApiReload';
import { NoDataGrid, SearchNodata } from 'app/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import {
  columnLetterCodes,
  columnActionCodes,
  columnSeriesCodes
} from './constants';
import { I18N_TEXT } from 'app/constants';
import { ILetterCode, LetterType } from 'pages/AccountManagement/types';
import { isEmpty } from 'lodash';
import { PAGE_SIZE } from 'pages/AccountActivity/constants';
import { checkEmpty } from 'app/helpers';

interface CodeListTableProps {
  data: ILetterCode[];
  sortBy: ISortType;
  isError: boolean;
  isLoading: boolean;
  valueSearch: string;
  totalItem: number;
  currentPageSize: number;
  currentPage: number;
  typeCode: string;
  getLetterCodes: () => void;
  handleSort: (value: ISortType) => void;
  onPageChange: (page: number) => void;
  onPageSizeChange: (pageInfo: pageSizeInfo) => void;
  onClearAndReset: () => void;
}

const CodeList: React.FC<CodeListTableProps> = ({
  data = [],
  isError = false,
  isLoading = false,
  valueSearch,
  totalItem = 0,
  currentPageSize = PAGE_SIZE[0],
  currentPage = 1,
  typeCode = LetterType.GROUP_LETTER,
  getLetterCodes,
  handleSort,
  onPageChange,
  onPageSizeChange,
  onClearAndReset
}) => {
  const { t } = useTranslation();

  const columns = useMemo(() => {
    switch (typeCode) {
      case LetterType.GROUP_LETTER:
        return columnActionCodes(t);
      case LetterType.SINGLE_LETTER:
        return columnLetterCodes(t);
      case LetterType.SERIES_LETTER:
        return columnSeriesCodes(t);
      default:
        return columnActionCodes(t);
    }
  }, [t, typeCode]);

  if (isError)
    return <FailedApiReload id="getLetterCodes" onReload={getLetterCodes} />;

  if (!data.length && !checkEmpty([valueSearch, false, true]) && !isLoading) {
    return (
      <NoDataGrid text={t(I18N_TEXT.NO_CORRESPONDENCE_TO_DISPLAY)}></NoDataGrid>
    );
  }

  if (!data.length && !isLoading)
    return (
      <SearchNodata
        text={t(I18N_TEXT.ADJUST_YOUR_SEARCH_FILTER)}
        onClearSearch={() => onClearAndReset()}
      />
    );

  return (
    <Fragment>
      <div className={'text-right mb-16 mr-n8'}>
        <If condition={!isEmpty(valueSearch)}>
          <Button
            size="sm"
            onClick={() => onClearAndReset()}
            variant="outline-primary"
          >
            {t(I18N_TEXT.CLEAR_AND_RESET)}
          </Button>
        </If>
      </div>
      <Grid
        onSortChange={handleSort}
        data={data as unknown as ColumnType[]}
        columns={columns}
      />

      <If condition={totalItem > 10}>
        <div className="pt-16">
          <Pagination
            totalItem={totalItem}
            pageSize={PAGE_SIZE}
            pageNumber={currentPage}
            pageSizeValue={currentPageSize}
            compact
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        </div>
      </If>
    </Fragment>
  );
};
export default CodeList;
