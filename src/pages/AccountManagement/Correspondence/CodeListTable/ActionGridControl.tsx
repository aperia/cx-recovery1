import React, { Fragment } from 'react';
import { useDispatch } from 'react-redux';

// components
import { Button } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// actions
import { accountManagementActions } from 'pages/AccountManagement/_redux';

// types
import { ILetterCode } from 'pages/AccountManagement/types';

// constants
import { I18N_TEXT } from 'app/constants';
interface ActionGridControlProps {
  item: ILetterCode;
}

const ActionGridControl: React.FC<ActionGridControlProps> = ({ item }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const openSendCorrespondence = () => {
    // hard code open view form send correspondence
    dispatch(
      accountManagementActions.onChangeCollapseSendCorrespondence({
        collapse: false,
        item
      })
    );
  };

  return (
    <Fragment>
      <Button
        size="sm"
        variant="outline-primary"
        onClick={openSendCorrespondence}
      >
        {t(I18N_TEXT.SEND_CORRESPONDENCE)}
      </Button>
    </Fragment>
  );
};
export default ActionGridControl;
