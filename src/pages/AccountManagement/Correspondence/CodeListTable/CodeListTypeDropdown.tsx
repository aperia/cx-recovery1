import React, { useCallback } from 'react';
import { batch, useDispatch, useSelector } from 'react-redux';

// component
import { DropdownList } from 'app/_libraries/_dls/components';

// redux
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { selectorTypeCode } from 'pages/AccountManagement/_redux/correspondence/modal/codeList/selector';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_TEXT } from 'app/constants';
import { dropDownTypeLetterCode } from './constants';

interface LetterTypeDropdownProps {}

const LetterTypeDropdown: React.FC<LetterTypeDropdownProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const value: RefDataValue = useSelector(selectorTypeCode);

  const handleChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      batch(() => {
        dispatch(accountManagementActions.onChangeLetterCodeType(e.value));
        dispatch(accountManagementActions.onLetterCodeClearAndReset());
        dispatch(accountManagementActions.onSendCorrespondenceClearAndReset());
      });
    },
    [dispatch]
  );

  return (
    <div className="mt-16 max-width-lg w-300px">
      <DropdownList
        textField="description"
        value={value}
        onChange={handleChange}
        label={t(I18N_TEXT.TYPE_CORRESPONDENCE)}
        textFieldRender={itemValue => (
          <>{`${itemValue.value} - ${t(itemValue.description)}`}</>
        )}
      >
        {dropDownTypeLetterCode.map(item => (
          <DropdownList.Item
            key={item.value}
            label={`${item.value} - ${t(item.description)}`}
            value={item}
          />
        ))}
      </DropdownList>
    </div>
  );
};
export default LetterTypeDropdown;
