import React, { useState, useEffect, ChangeEvent } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// component
import { TextSearch } from 'app/_libraries/_dls/components';

// redux
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { selectorValueSearch } from 'pages/AccountManagement/_redux/correspondence/modal/codeList/selector';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_TEXT } from 'app/constants';
import { isEmpty } from 'app/helpers';

interface LetterCodeSearchProps {}

const LetterCodeSearch: React.FC<LetterCodeSearchProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const valueSearch: string = useSelector(selectorValueSearch);

  const [value, setValue] = useState<string>('');

  useEffect(() => {
    if (isEmpty(valueSearch)) {
      setValue('');
    }
  }, [valueSearch]);

  const handleSearch = (valueStr: string) => {
    setValue(valueStr);
    dispatch(accountManagementActions.onChangeLetterCodeSearchValue(valueStr));
  };

  const handleChange = (event: ChangeEvent<HTMLInputElement>) => {
    setValue(event.target.value);
  };

  const handleClear = () => {
    setValue('');
  };

  return (
    <TextSearch
      placeholder={t(I18N_TEXT.TYPE_TO_SEARCH)}
      dataTestId="search-entitlement"
      onSearch={handleSearch}
      onChange={handleChange}
      onClear={handleClear}
      value={value}
    />
  );
};
export default LetterCodeSearch;
