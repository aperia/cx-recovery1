import React from 'react';
import { I18N_TEXT } from 'app/constants';
import { ILetterCode } from 'pages/AccountManagement/types';
import ActionGridControl from './ActionGridControl';

export const dropDownTypeLetterCode = [
  {
    description: 'txt_group_of_letters',
    value: 'A'
  },
  {
    description: 'txt_single_letter',
    value: 'L'
  },
  {
    description: 'txt_series_of_letter',
    value: 'S'
  }
];

export const columnSeriesCodes = (t: any) => {
  return [
    {
      id: 'letterCode',
      Header: t(I18N_TEXT.SERIES_CODE),
      isSort: true,
      accessor: 'letterCode',
      width: 120
    },
    {
      id: 'description',
      Header: t(I18N_TEXT.DESCRIPTION),
      isSort: true,
      accessor: 'description'
    },
    {
      id: 'action',
      Header: t(I18N_TEXT.ACTION),
      accessor: (item: ILetterCode) => <ActionGridControl item={item} />,
      width: 200,
      isFixedRight: true,
      cellProps: { className: 'text-center' },
      cellBodyProps: { className: 'text-center' },
      className: 'td-sm'
    }
  ];
};

export const columnActionCodes = (t: any) => {
  return [
    {
      id: 'letterCode',
      Header: t(I18N_TEXT.ACTION_CODE),
      isSort: true,
      accessor: 'letterCode',
      width: 120
    },
    {
      id: 'description',
      Header: t(I18N_TEXT.DESCRIPTION),
      isSort: true,
      accessor: 'description'
    },
    {
      id: 'action',
      Header: t(I18N_TEXT.ACTION),
      accessor: (item: ILetterCode) => <ActionGridControl item={item} />,
      width: 200,
      isFixedRight: true,
      cellProps: { className: 'text-center' },
      cellBodyProps: { className: 'text-center' },
      className: 'td-sm'
    }
  ];
};

export const columnLetterCodes = (t: any) => {
  return [
    {
      id: 'letterCode',
      Header: t(I18N_TEXT.LETTER_CODE),
      isSort: true,
      accessor: 'letterCode',
      width: 120
    },
    {
      id: 'description',
      Header: t(I18N_TEXT.DESCRIPTION),
      isSort: true,
      accessor: 'description'
    },
    {
      id: 'letterVariables',
      Header: t(I18N_TEXT.LETTER_VARIABLE),
      isSort: true,
      accessor: 'letterVariables',
      width: 146
    },
    {
      id: 'action',
      Header: t(I18N_TEXT.ACTION),
      accessor: (item: ILetterCode) => <ActionGridControl item={item} />,
      width: 200,
      isFixedRight: true,
      cellProps: { className: 'text-center' },
      cellBodyProps: { className: 'text-center' },
      className: 'td-sm'
    }
  ];
};
