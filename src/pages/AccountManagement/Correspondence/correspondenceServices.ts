import {
  IPayloadCorrespondence,
  IPayloadPendingCorrespondence,
  IPayloadSendCorrespondence
} from '../types';
import { correspondenceController } from '../__mock__/correspondenceController';

const correspondenceServices = {
  getLetterCode(params: IPayloadCorrespondence) {
    return correspondenceController.getLetterCode(params);
  },
  getPendingCorrespondence(params: IPayloadPendingCorrespondence) {
    return correspondenceController.getPendingCorrespondence(params);
  },
  removePendingCorrespondence(id: string) {
    return correspondenceController.removePendingCorrespondence(id);
  },
  sendCorrespondence(payload: IPayloadSendCorrespondence) {
    return correspondenceController.sendPendingCorrespondence(payload);
  }
};

export default correspondenceServices;
