import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helper
import { I18N_TEXT } from 'app/constants';
import { IPendingCorrespondence } from 'pages/AccountManagement/types';
import { mappingCodeType, mappingReceivingDebtor } from '../helper';

export interface DeletePendingCorrespondenceModalProps {
  item: IPendingCorrespondence;
}

const DeletePendingCorrespondenceModal: React.FC<DeletePendingCorrespondenceModalProps> =
  ({ item }) => {
    const { t } = useTranslation();

    return (
      <>
        <p>{t(I18N_TEXT.MSG_CONFIRM_DELETE_PENDING_CORRESPONDENCE)}</p>
        <div className="bg-light-l20 border br-light-l04 br-radius-8 p-16 mt-16">
          <div className="d-flex">
            <div className="mr-24">
              <p>{t(I18N_TEXT.LETTER_CODE)}:</p>
              <p className="mt-8">{t(I18N_TEXT.LETTER_TYPE)}:</p>
              <p className="mt-8">{t(I18N_TEXT.SEND_DATE)}:</p>
              <p className="mt-8">{t(I18N_TEXT.ACCOUNT_NUMBER)}:</p>
              <p className="mt-8">{t(I18N_TEXT.RECEIVING_DEBTOR)}:</p>
            </div>
            <div className="fw-500">
              <p>{`${item.code} - ${item.description}`}</p>
              <p className="mt-8">{mappingCodeType(item.type, t)}</p>
              <p className="mt-8">{item.sendDate}</p>
              <p className="mt-8">{item.accNumber.accountNumberUnMask}</p>
              <p className="mt-8">{mappingReceivingDebtor(item.receiver)}</p>
            </div>
          </div>
        </div>
      </>
    );
  };

export default DeletePendingCorrespondenceModal;
