import React from 'react';

// components
import AccountItem from './accountItem';

// types
import { IDebtorItem } from 'pages/AccountDetail/BankruptcyDeceaseRecord/types';

export interface ReceiverAccountProps {
  debtorList: IDebtorItem[];
}

const ReceiverAccount: React.FC<ReceiverAccountProps> = ({
  debtorList = []
}) => {
  return (
    <div className="card br-radius-8 p-0">
      {debtorList.map((item: IDebtorItem, index: number) => {
        return <AccountItem key={index.toString()} item={item} index={index} />;
      })}
    </div>
  );
};

export default ReceiverAccount;
