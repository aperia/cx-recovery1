import React, { useCallback } from 'react';

// components
import { CheckBox, Tooltip } from 'app/_libraries/_dls/components';

// redux
import { accountManagementActions } from 'pages/AccountManagement/_redux';

// helpers
import { useDispatch, useSelector } from 'react-redux';

import classNames from 'classnames';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// type
import {
  ILinkDebtor,
  SendCorrespondenceType
} from 'pages/AccountManagement/types';
import { I18N_TEXT } from 'app/constants';
import { selectorDebtorSelected } from 'pages/AccountManagement/_redux/correspondence/modal/sendCorrespondence/selector';
import { IDebtorItem } from 'pages/AccountDetail/BankruptcyDeceaseRecord/types';
import { mappingRelationValue } from 'pages/AccountManagement/ManageAssociatedDebtors/helpers';

export interface FoundAccountProps {
  item: IDebtorItem;
  index: number;
}

const FoundAccount: React.FC<FoundAccountProps> = ({ item, index }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const selectedAccounts: ILinkDebtor[] = useSelector(selectorDebtorSelected);

  const handleSelectedAccounts = useCallback(
    (obj: IDebtorItem) => {
      const indexAccount = selectedAccounts.findIndex(
        (account: ILinkDebtor) => obj.id === account.id
      );
      const _selectedAccounts = [...selectedAccounts];

      if (indexAccount !== -1) {
        _selectedAccounts.splice(indexAccount, 1);
      } else {
        _selectedAccounts.push(obj);
      }

      dispatch(
        accountManagementActions.onSelectReceiverSendCorrespondence(
          _selectedAccounts
        )
      );
    },
    [dispatch, selectedAccounts]
  );

  const renderItemAccount = (obj: IDebtorItem) => {
    const checked = selectedAccounts.some(
      (account: ILinkDebtor) => obj.id === account.id
    );

    const disabledCheck = obj.sendCorrespondence === SendCorrespondenceType.B;

    return (
      <div
        key={index.toString()}
        className={classNames(
          'card-group-checkbox',
          disabledCheck && 'disabled',
          checked && 'active'
        )}
      >
        <div>
          <p>{item.debtorName}</p>

          <p className="color-grey mt-4">{`${mappingRelationValue(
            item.relation
          )} • ${t(I18N_TEXT.LAST_4_SSN)}: ${item.last4SSN}`}</p>
        </div>

        {disabledCheck ? (
          <Tooltip
            element={t(I18N_TEXT.MSG_NEVER_RECEIVER_CORRESPONDENCE)}
            placement="top-start"
            variant="default"
          >
            <CheckBox>
              <CheckBox.Input disabled={disabledCheck} />
            </CheckBox>
          </Tooltip>
        ) : (
          <CheckBox onChange={() => handleSelectedAccounts(obj)}>
            <CheckBox.Input checked={checked} />
          </CheckBox>
        )}
      </div>
    );
  };

  const renderLayout = () => {
    return <>{renderItemAccount(item)}</>;
  };

  return <>{renderLayout()}</>;
};

export default FoundAccount;
