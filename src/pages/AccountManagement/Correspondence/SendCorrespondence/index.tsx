import React, { useEffect, useMemo, useState, useCallback } from 'react';
import { useAccountDetail, useSelector } from 'app/hooks';
import { useDispatch } from 'react-redux';
import { mappingCodeType } from '../helper';

// components
import { Button } from 'app/_libraries/_dls/components';
import { DatePickerControl, TextBoxControl } from 'app/components';

import ReceiverAccount from './ReceiverAccount';

// redux
import {
  selectorCollapseSendCorrespondence,
  selectorDataSendCorrespondence,
  selectorDebtorSelected
} from 'pages/AccountManagement/_redux/correspondence/modal/sendCorrespondence/selector';
import {
  selectIsDiscardFormModal,
  selectModalCorrespondenceIsOpen
} from 'pages/AccountManagement/_redux/correspondence/selector';
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { FormikProvider, useFormik } from 'formik';
import { selectDebtorList } from 'pages/AccountDetail/BankruptcyDeceaseRecord/_redux/selectors';

// types
import { I18N_TEXT, MAX_LENGTH_40 } from 'app/constants';
import {
  ILetterCode,
  ILinkDebtor,
  LetterType,
  RequestValuesType,
  SendCorrespondenceType
} from 'pages/AccountManagement/types';
import {
  FORM_NAMES,
  validationSchema,
  validationLetterYesSchema
} from './constants';
import { formatTimeDefault, isEmpty, isTouchedForm } from 'app/helpers';
import { FormatTime } from 'app/constants/enums';
import { IDebtorItem } from 'pages/AccountDetail/BankruptcyDeceaseRecord/types';
import { selectorTypeCode } from 'pages/AccountManagement/_redux/correspondence/modal/codeList/selector';

export interface SendCorrespondenceProps {
  isCollapse: (collapse: boolean) => void;
  accountNumber: string;
  accNumber: {
    accountNumber: string;
    accountNumberUnMask: string;
  };
}
const SendCorrespondence: React.FC<SendCorrespondenceProps> = ({
  isCollapse,
  accountNumber,
  accNumber
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  const data: ILetterCode | undefined = useSelector(
    selectorDataSendCorrespondence
  );
  const selectedAccounts: ILinkDebtor[] = useSelector(selectorDebtorSelected);
  const isOpen = useSelector<boolean>(selectModalCorrespondenceIsOpen);
  const isCheckDiscardForm = useSelector<boolean>(selectIsDiscardFormModal);
  const collapse: boolean = useSelector(selectorCollapseSendCorrespondence);
  const typeCodeSelected: RefDataValue = useSelector(selectorTypeCode);

  const debtorList: IDebtorItem[] = useSelector(state =>
    selectDebtorList(state, storeId)
  );

  const [defaultCheckBox, setDefaultCheckbox] = useState<any[]>([]);

  const isChangeDefault: boolean = useMemo(() => {
    const accountIds = selectedAccounts.map(obj => obj.id);
    if (typeCodeSelected.value !== LetterType.SINGLE_LETTER) return false;
    if (accountIds.length !== defaultCheckBox.length) return true;
    const checkOrderExits = accountIds.findIndex(
      obj => defaultCheckBox.indexOf(obj) === -1
    );

    return checkOrderExits > -1;
  }, [defaultCheckBox, typeCodeSelected, selectedAccounts]);

  useEffect(() => {
    if (!isOpen) {
      setDefaultCheckbox([]);
    }
  }, [isOpen]);

  useEffect(() => {
    if (debtorList?.length === 0) return;
    const _selectedAccounts = debtorList.filter(
      obj => obj.sendCorrespondence === SendCorrespondenceType.Y
    );

    const listIds = _selectedAccounts.map(obj => obj.id);

    setDefaultCheckbox(listIds);
    dispatch(
      accountManagementActions.onSelectReceiverSendCorrespondence(
        _selectedAccounts
      )
    );
  }, [dispatch, debtorList, data]);

  const handleSubmit = () => {
    if (!data) return;

    const sendDate = values.sendDate
      ? formatTimeDefault(values.sendDate, FormatTime.Date)
      : formatTimeDefault(new Date(), FormatTime.Date);

    const requestData = {
      accountNumber,
      accNumber,
      code: data.letterCode,
      description: data.description,
      type: data.type,
      sendDate: sendDate || '',
      lastPaymentAmount: values.lastPaymentAmount,
      receiver:
        selectedAccounts.map((item, index: number) => ({
          id: index.toString(),
          debtorName: item.debtorName,
          relation: item.relation
        })) || []
    };

    dispatch(accountManagementActions.sendCorrespondence(requestData));
  };

  const validationRule = useMemo(() => {
    if (data?.letterVariables === 'Yes') {
      return validationLetterYesSchema;
    }

    return validationSchema;
  }, [data?.letterVariables]);

  const formik = useFormik<RequestValuesType>({
    initialValues: {
      sendDate: new Date()
    },
    validationSchema: validationRule,
    validateOnBlur: true,
    validateOnMount: false,
    isInitialValid: true,
    onSubmit: handleSubmit
  });

  const { values, isValid, touched, errors, submitForm, resetForm } = formik;

  const handleCancelForm = useCallback(() => {
    resetForm();
    isCollapse(true);
    if (isCheckDiscardForm === true) {
      dispatch(accountManagementActions.onSetModalCorrespondence(false));
    }
  }, [dispatch, resetForm, isCollapse, isCheckDiscardForm]);

  const handleCancel = useCallback(() => {
    if (isTouchedForm(touched) || isChangeDefault) {
      dispatch(
        rootModalActions.open({
          title: I18N_TEXT.UNSAVED_CHANGE,
          body: I18N_TEXT.DISCARD_CHANGE_BODY,
          btnCancelText: I18N_TEXT.CONTINUE_EDITING,
          btnConfirmText: I18N_TEXT.DISCARD_CHANGE,
          onConfirm: handleCancelForm,
          onCancel: () => {
            dispatch(accountManagementActions.onCheckFormDiscard(false));
          }
        })
      );
      return;
    }

    handleCancelForm();
  }, [dispatch, handleCancelForm, touched, isChangeDefault]);

  useEffect(() => {
    if (isCheckDiscardForm) {
      handleCancel();
    }
  }, [dispatch, isCheckDiscardForm, handleCancel]);

  const checkToucheLastPayment = useMemo(() => {
    if (data?.letterVariables !== 'Yes') return true;
    return touched?.lastPaymentAmount;
  }, [data?.letterVariables, touched]);

  const hasSelectedAccount: boolean = useMemo(() => {
    return selectedAccounts?.length > 0;
  }, [selectedAccounts]);

  // reset form when select new code
  useEffect(() => {
    resetForm();
  }, [resetForm, data?.id]);

  const renderTitleCodeList = useMemo(() => {
    switch (data?.type) {
      case LetterType.GROUP_LETTER:
        return <b>{t(I18N_TEXT.ACTION_CODE)}:</b>;
      case LetterType.SINGLE_LETTER:
        return <b>{t(I18N_TEXT.LETTER_CODE)}:</b>;
      case LetterType.SERIES_LETTER:
        return <b>{t(I18N_TEXT.SERIES_CODE)}:</b>;
      default:
        break;
    }
  }, [t, data?.type]);

  if (!isOpen) return <></>;
  if (collapse) return <></>;

  return (
    <div className="p-24">
      <h5>{t(I18N_TEXT.SEND_CORRESPONDENCE)}</h5>
      <h6 className="color-grey mt-24">
        {t(I18N_TEXT.CORRESPONDENCE_INFORMATION)}
      </h6>
      <FormikProvider value={formik}>
        <div className="d-flex mt-16">
          <div className="mr-24">
            <p className="color-grey fw-500">
              {t(I18N_TEXT.TYPE_CORRESPONDENCE)}:
            </p>
            <p className="color-grey fw-500 mt-16">{renderTitleCodeList}</p>
            {data?.letterVariables === 'Yes' && (
              <p className="color-grey fw-500 mt-16">
                {t(I18N_TEXT.SEQUENCE_NUMBER)}:
              </p>
            )}
            <p className="color-grey fw-500 mt-16">
              {t(FORM_NAMES.sendDate.label)}:
              <span className="color-red-d12 ml-4">*</span>
            </p>
          </div>
          <div className="flex-1">
            <p>{mappingCodeType(data?.type || '', t)}</p>
            <p className="mt-16">{`${data?.letterCode} - ${data?.description}`}</p>
            {data?.letterVariables === 'Yes' && (
              <p className="mt-16">{data?.sequenceNumber}</p>
            )}
            <DatePickerControl
              className="mt-12"
              name={FORM_NAMES.sendDate.name}
              required
              size="small"
              pastDateDisabledText={t(
                I18N_TEXT.MSG_SEND_DATE_CAN_NOT_PAST_DATE
              )}
              minDaysFromToday={0}
            />
          </div>
        </div>
        {data?.letterVariables === 'Yes' && (
          <>
            <h6 className="mt-24 mb-16 color-grey">{t(I18N_TEXT.VARIABLES)}</h6>
            <TextBoxControl
              required
              name={FORM_NAMES.lastPaymentAmount.name}
              label={t(I18N_TEXT.LAST_PAYMENT_AMOUNT)}
              maxLength={MAX_LENGTH_40}
            />
          </>
        )}
      </FormikProvider>
      <h6 className="mt-24 mb-16 color-grey">{t(I18N_TEXT.RECEIVER)}</h6>
      <ReceiverAccount debtorList={debtorList} />
      <div className="text-right mt-24">
        <Button size="sm" variant="secondary" onClick={handleCancel}>
          {t(I18N_TEXT.CANCEL)}
        </Button>
        <Button
          size="sm"
          variant="primary"
          disabled={
            !hasSelectedAccount ||
            !checkToucheLastPayment ||
            !isValid ||
            !isEmpty(errors)
          }
          onClick={submitForm}
        >
          {t(I18N_TEXT.SUBMIT)}
        </Button>
      </div>
    </div>
  );
};
export default SendCorrespondence;
