import * as Yup from 'yup';

export const FORM_NAMES = {
  sendDate: {
    name: 'sendDate',
    label: 'txt_send_date'
  },
  lastPaymentAmount: {
    name: 'lastPaymentAmount',
    label: 'txt_variables',
    placeholder: 'txt_last_payment_amount'
  }
};

export const validationLetterYesSchema = Yup.object().shape({
  [FORM_NAMES.sendDate.name]: Yup.string().required(
    'txt_send_date_is_required'
  ),
  [FORM_NAMES.lastPaymentAmount.name]: Yup.string().required(
    'txt_last_payment_amount_is_required'
  )
});

export const validationSchema = Yup.object().shape({
  [FORM_NAMES.sendDate.name]: Yup.string().required('txt_send_date_is_required')
});

export const TODAY = new Date().toString();
