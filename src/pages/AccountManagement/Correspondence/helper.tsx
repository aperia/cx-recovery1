import { IReceiverDebtor } from '../types';
import { dropDownTypeLetterCode } from './CodeListTable/constants';

export const mappingReceivingDebtor = (receiver: IReceiverDebtor[]) => {
  const arrayDebtor = receiver.map(obj => obj.debtorName);
  return arrayDebtor.join(', ').trim();
};

export const mappingCodeType = (type: string, t: any) => {
  if (!type) return '';
  
  const typeCode = dropDownTypeLetterCode.find(obj => obj.value === type);

  if (typeCode) {
    return `${typeCode.value} - ${t(typeCode.description)}`;
  }

  return type;
};
