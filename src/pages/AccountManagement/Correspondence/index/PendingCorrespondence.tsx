import React, { useEffect } from 'react';
import classnames from 'classnames';

// components
import { SimpleBar } from 'app/_libraries/_dls/components';

import PendingCorrespondenceGrid from '../PendingCorrespondence/Grid';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { I18N_TEXT } from 'app/constants';
import { usePendingCorresPondence } from 'pages/AccountManagement/Correspondence/hooks/usePendingCorresPondence';

interface CodeListProps {}

const CodeList: React.FC<CodeListProps> = ({}) => {
  const { t } = useTranslation();

  const {
    data,
    isError,
    valueSearch,
    totalItem,
    isLoading,
    currentPageSize,
    currentPage,
    onClearAndReset,
    onPageChange,
    onPageSizeChange,
    getPendingCorrespondence
  } = usePendingCorresPondence();

  useEffect(() => {
    getPendingCorrespondence();
  }, [getPendingCorrespondence, currentPageSize, currentPage]);

  return (
    <div>
      <SimpleBar>
        <div
          className={classnames('px-24 pb-24 pt-22 max-width-lg mx-auto', {
            loading: isLoading
          })}
        >
          <div className="d-flex justify-content-between mb-24 align-items-center">
            <h5>{t(I18N_TEXT.PENDING_CORRESPONDENCE_LIST)}</h5>
          </div>
          <PendingCorrespondenceGrid
            isModal
            data={data}
            isError={isError}
            valueSearch={valueSearch}
            totalItem={totalItem}
            isLoading={isLoading}
            currentPageSize={currentPageSize}
            currentPage={currentPage}
            getModalPendingCorrespondence={getPendingCorrespondence}
            onClearAndReset={onClearAndReset}
            onPageChange={onPageChange}
            onPageSizeChange={onPageSizeChange}
          />
        </div>
      </SimpleBar>
    </div>
  );
};
export default CodeList;
