import React, { useCallback, useEffect, useMemo } from 'react';
import classnames from 'classnames';

// components
import { ModalFooter, SimpleBar } from 'app/_libraries/_dls/components';

import CodeListTable from '../CodeListTable';
import LetterTypeDropdown from '../CodeListTable/CodeListTypeDropdown';
import LetterCodeSearch from '../CodeListTable/CodeListSearch';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { I18N_TEXT } from 'app/constants';
import { useCodeList } from 'pages/AccountManagement/Correspondence/hooks/useCodeList';
import { LetterType } from 'pages/AccountManagement/types';
import SendCorrespondence from '../SendCorrespondence';
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { useDispatch } from 'react-redux';

interface CodeListProps {
  accountNumber: string;
  accNumber: {
    accountNumber: string;
    accountNumberUnMask: string;
  };
}

const CodeList: React.FC<CodeListProps> = ({ accountNumber, accNumber }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const {
    data,
    isError,
    valueSearch,
    totalItem,
    isLoading,
    currentPageSize,
    currentPage,
    sortBy,
    typeCode,
    collapse,
    getLetterCodes,
    onClearAndReset,
    onPageChange,
    onPageSizeChange,
    handleSort,
    setCollapse
  } = useCodeList();

  useEffect(() => {
    getLetterCodes();
  }, [
    getLetterCodes,
    currentPageSize,
    currentPage,
    sortBy,
    valueSearch,
    typeCode
  ]);

  const handleCancel = useCallback(() => {
    dispatch(accountManagementActions.onSetModalCorrespondence(false));
  }, [dispatch]);

  const renderTitleCodeList = useMemo(() => {
    switch (typeCode.value) {
      case LetterType.GROUP_LETTER:
        return <h5>{t(I18N_TEXT.ACTION_CODE_LIST)}</h5>;
      case LetterType.SINGLE_LETTER:
        return <h5>{t(I18N_TEXT.LETTER_CODE_LIST)}</h5>;
      case LetterType.SERIES_LETTER:
        return <h5>{t(I18N_TEXT.SERIES_CODE_LIST)}</h5>;
      default:
        break;
    }
  }, [t, typeCode.value]);

  return (
    <div className="content-block content-correspondence overflow-hidden">
      <div className="content-left">
        <SimpleBar>
          <div
            className={classnames('px-24 pb-24 pt-22 max-width-lg mx-auto', {
              loading: isLoading
            })}
          >
            <div className="border-bottom pb-24 mb-26">
              <h5>{t(I18N_TEXT.CORRESPONDENCES)}</h5>
              <LetterTypeDropdown />
            </div>
            <div className="d-flex justify-content-between mb-24 align-items-center">
              {renderTitleCodeList}
              <LetterCodeSearch />
            </div>
            <CodeListTable
              data={data}
              isError={isError}
              sortBy={sortBy}
              valueSearch={valueSearch}
              totalItem={totalItem}
              isLoading={isLoading}
              currentPageSize={currentPageSize}
              currentPage={currentPage}
              getLetterCodes={getLetterCodes}
              onClearAndReset={onClearAndReset}
              handleSort={handleSort}
              onPageChange={onPageChange}
              onPageSizeChange={onPageSizeChange}
              typeCode={typeCode.value}
            />
          </div>
        </SimpleBar>
        <ModalFooter
          border
          cancelButtonText={t(I18N_TEXT.CLOSE)}
          onCancel={handleCancel}
        ></ModalFooter>
      </div>
      <div
        className={classnames('content-right', {
          collapsed: collapse
        })}
      >
        <SimpleBar>
          <SendCorrespondence
            accountNumber={accountNumber}
            accNumber={accNumber}
            isCollapse={setCollapse}
          />
        </SimpleBar>
      </div>
    </div>
  );
};
export default CodeList;
