import React, { useCallback, useEffect } from 'react';
import { batch, useDispatch } from 'react-redux';

// components
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  HorizontalTabs
} from 'app/_libraries/_dls/components';
import CodeList from './CodeList';
import PendingCorrespondence from './PendingCorrespondence';

// hooks
import { useAccountDetail, useSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// selectors

// actions
import { accountManagementActions } from 'pages/AccountManagement/_redux';

// types
import { CORRESPONDENCE_TAB } from 'pages/AccountManagement/types';

// constants
import { I18N_TEXT } from 'app/constants';
import {
  selectModalCorrespondenceIsOpen,
  selectModalCurrentTab
} from '../../_redux/correspondence/selector';
import { bankruptcyDeceaseRecordActions } from 'pages/AccountDetail/BankruptcyDeceaseRecord/_redux';

// helpers

export interface CorrespondenceListModalProps {
  accountNumber: string;
  accNumber: {
    accountNumber: string;
    accountNumberUnMask: string;
  };
}

const CorrespondenceListModal: React.FC<CorrespondenceListModalProps> = ({
  accountNumber,
  accNumber
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { storeId } = useAccountDetail();
  const isOpen = useSelector<boolean>(selectModalCorrespondenceIsOpen);
  const activeKey = useSelector<CORRESPONDENCE_TAB>(selectModalCurrentTab);

  useEffect(() => {
    if (!isOpen) return;
    dispatch(bankruptcyDeceaseRecordActions.getDebtorList({ storeId }));
  }, [dispatch, storeId, isOpen]);

  const handleCancel = useCallback(() => {
    if (activeKey === CORRESPONDENCE_TAB.LETTER_CODE_LIST) {
      dispatch(accountManagementActions.onCheckFormDiscard(true));
    } else {
      dispatch(accountManagementActions.onSetModalCorrespondence(false));
    }
  }, [dispatch, activeKey]);

  const handleOnSelectTab = (tab: any) => {
    if (!tab) return;
    if(tab === activeKey) return;
    
    batch(() => {
      dispatch(accountManagementActions.onChangeTabCorrespondence(tab));
      dispatch(accountManagementActions.onResetChangeTabCorrespondence());
      dispatch(accountManagementActions.onSendCorrespondenceClearAndReset());

      if (tab === CORRESPONDENCE_TAB.PENDING_CORRESPONDENCE) {
        dispatch(
          accountManagementActions.onChangeModalPendingCorrespondenceSearchValue(
            [
              {
                key: 'accountNumber',
                value: accNumber.accountNumberUnMask
              }
            ]
          )
        );
      }
    });
  };

  useEffect(() => {
    if (isOpen) return;
    dispatch(accountManagementActions.onResetModalCorrespondence());
    dispatch(accountManagementActions.onSendCorrespondenceClearAndReset());
  }, [isOpen, dispatch]);

  if (!isOpen) return <></>;

  return (
    <Modal show={isOpen} loading={false} enforceFocus={false} full>
      <ModalHeader closeButton onHide={handleCancel}>
        <ModalTitle className="text-capitalize">
          {t(I18N_TEXT.CORRESPONDENCE)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody noPadding className="overflow-hidden">
        <HorizontalTabs
          level2
          defaultActiveKey={CORRESPONDENCE_TAB.LETTER_CODE_LIST}
          activeKey={activeKey}
          onSelect={handleOnSelectTab}
          mountOnEnter
          unmountOnExit
        >
          <HorizontalTabs.Tab
            title={t(I18N_TEXT.CORRESPONDENCES)}
            key={CORRESPONDENCE_TAB.LETTER_CODE_LIST}
            eventKey={CORRESPONDENCE_TAB.LETTER_CODE_LIST}
          >
            <CodeList accountNumber={accountNumber} accNumber={accNumber} />
          </HorizontalTabs.Tab>
          <HorizontalTabs.Tab
            title={t(I18N_TEXT.PENDING_CORRESPONDENCES)}
            key={CORRESPONDENCE_TAB.PENDING_CORRESPONDENCE}
            eventKey={CORRESPONDENCE_TAB.PENDING_CORRESPONDENCE}
          >
            <PendingCorrespondence />
          </HorizontalTabs.Tab>
        </HorizontalTabs>
      </ModalBody>
      <ModalFooter
        className="d-md-none d-lg-flex"
        border
        cancelButtonText={t(I18N_TEXT.CLOSE)}
        onCancel={handleCancel}
      ></ModalFooter>
    </Modal>
  );
};

export default CorrespondenceListModal;
