import React, { useEffect, useLayoutEffect, useRef } from 'react';
import classnames from 'classnames';

// components
import { SimpleBar } from 'app/_libraries/_dls/components';

import PendingCorrespondenceGrid from '../PendingCorrespondence/Grid';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { I18N_TEXT } from 'app/constants';
import { usePendingCorresPondenceModal } from 'pages/AccountManagement/Correspondence/hooks/usePendingCorresPondenceModal';

interface CodeListProps {}

const CodeList: React.FC<CodeListProps> = ({}) => {
  const { t } = useTranslation();
  const divRef = useRef<HTMLDivElement | null>(null);

  const {
    data,
    isError,
    valueSearch,
    totalItem,
    isLoading,
    currentPageSize,
    currentPage,
    onClearAndReset,
    onPageChange,
    onPageSizeChange,
    getModalPendingCorrespondence
  } = usePendingCorresPondenceModal();

  useEffect(() => {
    getModalPendingCorrespondence();
  }, [getModalPendingCorrespondence, currentPageSize, currentPage]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 176px)`);
  }, []);

  return (
    <div ref={divRef}>
      <SimpleBar>
        <div
          className={classnames('px-24 pb-24 pt-22 max-width-lg mx-auto', {
            loading: isLoading
          })}
        >
          <div className="d-flex justify-content-between align-items-center">
            <h5>{t(I18N_TEXT.PENDING_CORRESPONDENCE_LIST)}</h5>
          </div>
          <PendingCorrespondenceGrid
            isModal
            data={data}
            isError={isError}
            valueSearch={valueSearch}
            totalItem={totalItem}
            isLoading={isLoading}
            currentPageSize={currentPageSize}
            currentPage={currentPage}
            getModalPendingCorrespondence={getModalPendingCorrespondence}
            onClearAndReset={onClearAndReset}
            onPageChange={onPageChange}
            onPageSizeChange={onPageSizeChange}
          />
        </div>
      </SimpleBar>
    </div>
  );
};
export default CodeList;
