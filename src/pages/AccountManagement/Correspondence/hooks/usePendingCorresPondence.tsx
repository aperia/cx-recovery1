import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// redux
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import {
  selectorModalList,
  selectorIsError,
  selectorValueSearch,
  selectorTotalPage,
  selectorPageSize,
  selectorCurrentPage,
  selectorIsLoading
} from 'pages/AccountManagement/_redux/correspondence/pendingCorrespondence/selector';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

// types
import { IPendingCorrespondence } from 'pages/AccountManagement/types';
import { I18N_TEXT } from 'app/constants';

// component
import {
  AttributeSearchValue,
  pageSizeInfo
} from 'app/_libraries/_dls/components';
import DeletePendingCorrespondenceModal from '../DeletePendingCorrespondenceModal';

export const usePendingCorresPondence = () => {
  const dispatch = useDispatch();

  const data: IPendingCorrespondence[] = useSelector(selectorModalList);
  const isError: boolean = useSelector(selectorIsError);
  const isLoading: boolean = useSelector(selectorIsLoading);
  const valueSearch: AttributeSearchValue[] = useSelector(selectorValueSearch);
  const totalItem: number = useSelector(selectorTotalPage);
  const currentPageSize: number = useSelector(selectorPageSize);
  const currentPage: number = useSelector(selectorCurrentPage);

  const getPendingCorrespondence = useCallback(() => {
    dispatch(accountManagementActions.getPendingCorrespondence({}));
  }, [dispatch]);

  const handleSearch = (value: AttributeSearchValue[]) => {
    dispatch(
      accountManagementActions.onChangePendingCorrespondenceSearchValue(value)
    );
  };

  const onPageChange = useCallback(
    (page: number) => {
      dispatch(
        accountManagementActions.onChangePendingCorrespondenceCurrentPage(page)
      );
    },
    [dispatch]
  );

  const onPageSizeChange = useCallback(
    (pageInfo: pageSizeInfo) => {
      dispatch(
        accountManagementActions.onChangePendingCorrespondencePageSize(
          pageInfo.size
        )
      );
    },
    [dispatch]
  );

  const onClearAndReset = useCallback(() => {
    dispatch(accountManagementActions.onPendingCorrespondenceClearAndReset());
  }, [dispatch]);

  const showConfirmDelete = useCallback(
    (data: IPendingCorrespondence) => {
      dispatch(
        rootModalActions.open({
          autoClose: false,
          title: I18N_TEXT.TITLE_CONFIRM_DELETE_PENDING_CORRESPONDENCE,
          size: 'sm',
          body: <DeletePendingCorrespondenceModal item={data} />,
          btnConfirmText: I18N_TEXT.DELETE,
          btnCancelText: I18N_TEXT.CANCEL,
          onConfirm: () => {
            dispatch(rootModalActions.loading(true));
            dispatch(
              accountManagementActions.removePendingCorrespondence({
                id: data.id
              })
            );
          },
          onCancel: () => {
            dispatch(rootModalActions.close());
          }
        })
      );
    },
    [dispatch]
  );

  return {
    data,
    isError,
    isLoading,
    totalItem,
    currentPageSize,
    valueSearch,
    currentPage,
    getPendingCorrespondence,
    handleSearch,
    onPageChange,
    onPageSizeChange,
    onClearAndReset,
    showConfirmDelete
  };
};
