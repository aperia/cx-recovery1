import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// redux
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import {
  selectorCodeList,
  selectorIsError,
  selectorValueSearch,
  selectorTotalPage,
  selectorPageSize,
  selectorCurrentPage,
  selectorSortBy,
  selectorIsLoading,
  selectorTypeCode
} from 'pages/AccountManagement/_redux/correspondence/modal/codeList/selector';
import { selectorCollapseSendCorrespondence } from 'pages/AccountManagement/_redux/correspondence/modal/sendCorrespondence/selector';

// types
import { ILetterCode } from 'pages/AccountManagement/types';

// component
import { pageSizeInfo } from 'app/_libraries/_dls/components';

export const useCodeList = () => {
  const dispatch = useDispatch();

  const data: ILetterCode[] = useSelector(selectorCodeList);
  const isError: boolean = useSelector(selectorIsError);
  const isLoading: boolean = useSelector(selectorIsLoading);
  const valueSearch: string = useSelector(selectorValueSearch);
  const totalItem: number = useSelector(selectorTotalPage);
  const currentPageSize: number = useSelector(selectorPageSize);
  const currentPage: number = useSelector(selectorCurrentPage);
  const sortBy: ISortType = useSelector(selectorSortBy);
  const typeCode: RefDataValue = useSelector(selectorTypeCode);
  const collapse: boolean = useSelector(selectorCollapseSendCorrespondence);

  const setCollapse = useCallback(
    (open: boolean, item?: ILetterCode) => {
      dispatch(
        accountManagementActions.onChangeCollapseSendCorrespondence({
          collapse: open,
          item
        })
      );
    },
    [dispatch]
  );

  const getLetterCodes = useCallback(() => {
    dispatch(accountManagementActions.getLetterCode({}));
  }, [dispatch]);

  const handleSearch = (value: string) => {
    dispatch(accountManagementActions.onChangeLetterCodeSearchValue(value));
  };

  const handleSort = (value: ISortType) => {
    dispatch(accountManagementActions.onChangeLetterCodeSortBy(value));
  };

  const onPageChange = useCallback(
    (page: number) => {
      dispatch(accountManagementActions.onChangeLetterCodeCurrentPage(page));
    },
    [dispatch]
  );

  const onPageSizeChange = useCallback(
    (pageInfo: pageSizeInfo) => {
      dispatch(
        accountManagementActions.onChangeLetterCodePageSize(pageInfo.size)
      );
    },
    [dispatch]
  );

  const onClearAndReset = useCallback(() => {
    dispatch(accountManagementActions.onLetterCodeClearAndReset());
  }, [dispatch]);

  return {
    data,
    isError,
    isLoading,
    totalItem,
    currentPageSize,
    valueSearch,
    currentPage,
    sortBy,
    typeCode,
    collapse,
    getLetterCodes,
    handleSearch,
    handleSort,
    onPageChange,
    onPageSizeChange,
    onClearAndReset,
    setCollapse
  };
};
