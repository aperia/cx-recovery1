import { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// redux
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import {
  selectorModalList,
  selectorIsError,
  selectorValueSearch,
  selectorTotalPage,
  selectorPageSize,
  selectorCurrentPage,
  selectorIsLoading
} from 'pages/AccountManagement/_redux/correspondence/modal/pendingCorrespondence/selector';

// types
import { IPendingCorrespondence } from 'pages/AccountManagement/types';

// component
import {
  AttributeSearchValue,
  pageSizeInfo
} from 'app/_libraries/_dls/components';

export const usePendingCorresPondenceModal = () => {
  const dispatch = useDispatch();

  const data: IPendingCorrespondence[] = useSelector(selectorModalList);
  const isError: boolean = useSelector(selectorIsError);
  const isLoading: boolean = useSelector(selectorIsLoading);
  const valueSearch: AttributeSearchValue[] = useSelector(selectorValueSearch);
  const totalItem: number = useSelector(selectorTotalPage);
  const currentPageSize: number = useSelector(selectorPageSize);
  const currentPage: number = useSelector(selectorCurrentPage);

  const getModalPendingCorrespondence = useCallback(() => {
    dispatch(accountManagementActions.getModalPendingCorrespondence({}));
  }, [dispatch]);

  const handleSearch = (value: AttributeSearchValue[]) => {
    dispatch(
      accountManagementActions.onChangeModalPendingCorrespondenceSearchValue(
        value
      )
    );
  };

  const onPageChange = useCallback(
    (page: number) => {
      dispatch(
        accountManagementActions.onChangeModalPendingCorrespondenceCurrentPage(
          page
        )
      );
    },
    [dispatch]
  );

  const onPageSizeChange = useCallback(
    (pageInfo: pageSizeInfo) => {
      dispatch(
        accountManagementActions.onChangeModalPendingCorrespondencePageSize(
          pageInfo.size
        )
      );
    },
    [dispatch]
  );

  const onClearAndReset = useCallback(() => {
    dispatch(
      accountManagementActions.onModalPendingCorrespondenceClearAndReset()
    );
  }, [dispatch]);

  return {
    data,
    isError,
    isLoading,
    totalItem,
    currentPageSize,
    valueSearch,
    currentPage,
    getModalPendingCorrespondence,
    handleSearch,
    onPageChange,
    onPageSizeChange,
    onClearAndReset
  };
};
