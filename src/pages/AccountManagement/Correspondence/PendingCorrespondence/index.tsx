import React, { useEffect, useLayoutEffect, useRef } from 'react';
import classnames from 'classnames';

// components
import { SimpleBar } from 'app/_libraries/_dls/components';
import PendingCorrespondenceGrid from './Grid';
import SearchPendingCorrespondence from './Search';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { I18N_TEXT } from 'app/constants';
import { usePendingCorresPondence } from 'pages/AccountManagement/Correspondence/hooks/usePendingCorresPondence';

interface PendingCorrespondenceProps {}

const PendingCorrespondence: React.FC<PendingCorrespondenceProps> = ({}) => {
  const { t } = useTranslation();
  const divRef = useRef<HTMLDivElement | null>(null);

  const {
    data,
    isError,
    valueSearch,
    totalItem,
    isLoading,
    currentPageSize,
    currentPage,
    onClearAndReset,
    onPageChange,
    onPageSizeChange,
    getPendingCorrespondence
  } = usePendingCorresPondence();

  useEffect(() => {
    getPendingCorrespondence();
  }, [getPendingCorrespondence, valueSearch, currentPageSize, currentPage]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 193px)`);
  }, []);

  return (
    <div ref={divRef}>
      <SimpleBar>
        <div
          className={classnames(
            'px-24 pb-24 pt-lg-16 pt-md-22 max-width-lg mx-auto',
            {
              loading: isLoading
            }
          )}
        >
          <div className="header-account">
            <h5 className="header-account-title">
              {t(I18N_TEXT.PENDING_CORRESPONDENCE_LIST)}
            </h5>
            <SearchPendingCorrespondence />
          </div>
          <PendingCorrespondenceGrid
            isModal={false}
            data={data}
            isError={isError}
            valueSearch={valueSearch}
            totalItem={totalItem}
            isLoading={isLoading}
            currentPageSize={currentPageSize}
            currentPage={currentPage}
            getModalPendingCorrespondence={getPendingCorrespondence}
            onClearAndReset={onClearAndReset}
            onPageChange={onPageChange}
            onPageSizeChange={onPageSizeChange}
          />
        </div>
      </SimpleBar>
    </div>
  );
};
export default PendingCorrespondence;
