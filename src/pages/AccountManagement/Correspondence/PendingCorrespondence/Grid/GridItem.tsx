import React, { useCallback } from 'react';
import { View } from 'app/_libraries/_dof/core';

// component
import { Button } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { usePendingCorresPondence } from '../../hooks/usePendingCorresPondence';

// constant
import { I18N_TEXT } from 'app/constants';
import {
  IPendingCorrespondence,
  IReceiverDebtor
} from 'pages/AccountManagement/types';
import { dropDownTypeLetterCode } from 'pages/AccountManagement/Correspondence/CodeListTable/constants';
import { mappingRelationValue } from 'pages/AccountManagement/ManageAssociatedDebtors/helpers';
import classNames from 'classnames';
import { genMashAccNumber } from 'pages/AccountManagement/helpers';

interface GridItemProps {
  item: IPendingCorrespondence;
  index: number;
}

const GridItem: React.FC<GridItemProps> = ({ item, index }) => {
  const { t } = useTranslation();

  const { showConfirmDelete } = usePendingCorresPondence();

  const mappingReceivingDebtor = (receiver: IReceiverDebtor[]) => {
    const arrayDebtor = receiver.map(obj => obj.debtorName);

    return {
      value: arrayDebtor.join(', ').trim(),
      description: (
        <>
          {receiver.map((obj, idx: number) => (
            <li key={idx.toString()} className={classNames(idx > 0 && 'mt-16')}>
              <div className="d-inline-flex flex-column ml-n2">
                <p>{obj.debtorName}</p>
                <p className="color-grey mt-4">
                  {mappingRelationValue(obj.relation)}
                </p>
              </div>
            </li>
          ))}
        </>
      )
    };
  };

  const mappingCodeType = useCallback(
    (type: string) => {
      const typeCode = dropDownTypeLetterCode.find(obj => obj.value === type);

      if (typeCode) {
        return `${typeCode.value} - ${t(typeCode.description)}`;
      }

      return type;
    },
    [t]
  );

  // format data for dof view
  const dataFormat = () => {
    return {
      ...item,
      accountNumber: genMashAccNumber(item.accNumber.accountNumberUnMask),
      accNumber: {
        ...item.accNumber,
        accountNumber: genMashAccNumber(item.accNumber.accountNumberUnMask)
      },
      type: mappingCodeType(item.type),
      receiver: mappingReceivingDebtor(item.receiver)
    };
  };

  const handleDeleteItem = useCallback(() => {
    if (!item) return;
    showConfirmDelete(item);
  }, [item, showConfirmDelete]);

  return (
    <div
      className="card br-radius-8 px-16 pb-16 pt-12 mt-16"
      key={index.toString()}
    >
      <div className="row">
        <div className="w-xl-25 w-md-80 order-xl-1 order-md-1">
          <p className="fw-500">{`${item.code} - ${item.description}`}</p>
        </div>
        <div className="w-xl-65 w-md-100 order-xl-2 order-md-3 mt-md-4 mt-xl-0">
          <View
            key={item.id.toString()}
            id={`pendingCorrespondence-${item.id}`}
            formKey={`pendingCorrespondence-${item.id}`}
            descriptor="pendingCorrespondence"
            value={dataFormat()}
          />
        </div>
        <div className="w-xl-10 w-md-20 order-xl-3 order-md-2 text-right">
          <Button
            size="sm"
            className="mr-n8 mt-n8"
            variant="outline-danger"
            onClick={handleDeleteItem}
          >
            {t(I18N_TEXT.DELETE)}
          </Button>
        </div>
      </div>
    </div>
  );
};

export default GridItem;
