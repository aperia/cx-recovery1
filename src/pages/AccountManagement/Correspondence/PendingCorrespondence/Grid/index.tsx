import React, { Fragment } from 'react';
import { checkEmpty, isEmpty } from 'app/helpers';
import If from 'pages/__commons/If';

// component
import {
  AttributeSearchValue,
  Button,
  pageSizeInfo,
  Pagination
} from 'app/_libraries/_dls/components';
import { NoDataGrid, SearchNodata } from 'app/components';
import FailedApiReload from 'app/components/FailedApiReload';
import GridItem from './GridItem';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constant
import { I18N_TEXT } from 'app/constants';
import { PAGE_SIZE } from 'pages/AccountActivity/constants';
import { IPendingCorrespondence } from 'pages/AccountManagement/types';

interface PendingCorrespondenceGridProps {
  isModal?: boolean;
  data: IPendingCorrespondence[];
  isError: boolean;
  isLoading: boolean;
  valueSearch: AttributeSearchValue[];
  totalItem: number;
  currentPageSize: number;
  currentPage: number;
  getModalPendingCorrespondence: () => void;
  onPageChange: (page: number) => void;
  onPageSizeChange: (pageInfo: pageSizeInfo) => void;
  onClearAndReset: () => void;
}

const PendingCorrespondenceGrid: React.FC<PendingCorrespondenceGridProps> = ({
  data = [],
  isModal = false,
  isError = false,
  isLoading = false,
  valueSearch,
  totalItem = 0,
  currentPageSize = PAGE_SIZE[0],
  currentPage = 1,
  getModalPendingCorrespondence,
  onPageChange,
  onPageSizeChange,
  onClearAndReset
}) => {
  const { t } = useTranslation();

  if (isError)
    return (
      <FailedApiReload
        id="getQueueList"
        onReload={getModalPendingCorrespondence}
      />
    );

  if (
    !data.length &&
    isModal === false &&
    checkEmpty([valueSearch, false, true]) &&
    !isLoading
  )
    return (
      <SearchNodata
        text={t(I18N_TEXT.ADJUST_YOUR_SEARCH_FILTER)}
        onClearSearch={onClearAndReset}
      />
    );

  if (!data.length && !isLoading) {
    return (
      <NoDataGrid
        text={t(I18N_TEXT.NO_PENDING_CORRESPONDENCE_TO_DISPLAY)}
      ></NoDataGrid>
    );
  }

  return (
    <Fragment>
      <If condition={!isEmpty(valueSearch) && isModal === false}>
        <div className="d-flex justify-content-between align-items-center mr-n8 mt-16">
          <p>{t(I18N_TEXT.RESULT_FOUNDED, { count: data.length })}</p>
          <Button
            size="sm"
            variant="outline-primary"
            onClick={() => onClearAndReset()}
          >
            {t(I18N_TEXT.CLEAR_AND_RESET)}
          </Button>
        </div>
      </If>

      {data.map((item, index: number) => (
        <GridItem key={index.toString()} item={item} index={index} />
      ))}

      <If condition={totalItem > 10}>
        <div className="pt-16">
          <Pagination
            totalItem={totalItem}
            pageSize={PAGE_SIZE}
            pageNumber={currentPage}
            pageSizeValue={currentPageSize}
            compact
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        </div>
      </If>
    </Fragment>
  );
};

export default PendingCorrespondenceGrid;
