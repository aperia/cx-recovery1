import React from 'react';
import {
  AttributeSearchData,
  InputControl
} from 'app/_libraries/_dls/components';
import { I18N_TEXT, MAX_LENGTH_16, MAX_LENGTH_4 } from 'app/constants';

import { ATTRIBUTE_SEARCH_FIELD } from '../constants';

export const getInitialAttrData = (t: any) => {
  const accountNumberField: AttributeSearchData = {
    groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
    name: t(I18N_TEXT.ACCOUNT_NUMBER),
    key: ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER,
    description: t(I18N_TEXT.ACCOUNT_NUMBER_DESCRIPTION),
    component: props => (
      <InputControl
        placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
        mode="number"
        maxLength={MAX_LENGTH_16}
        {...props}
      />
    )
  };
  const letterCodeField: AttributeSearchData = {
    groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
    name: t(I18N_TEXT.LETTER_CODE),
    key: ATTRIBUTE_SEARCH_FIELD.LETTER_CODE,
    description: t(I18N_TEXT.LETTER_CODE_DESCRIPTION),
    component: props => (
      <InputControl
        placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
        maxLength={MAX_LENGTH_4}
        {...props}
      />
    )
  };
  return {
    data: [accountNumberField, letterCodeField]
  };
};
