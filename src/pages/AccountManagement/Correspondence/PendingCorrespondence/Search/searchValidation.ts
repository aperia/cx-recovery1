import { stringValidate } from 'app/helpers/stringValidate';
import { ATTRIBUTE_SEARCH_FIELD, MESSAGE } from '../constants';
import { AttrSearchPendingCorrespondenceModel } from 'pages/AccountManagement/types';

export const validateAccountNumber = (
  data: AttrSearchPendingCorrespondenceModel,
  t: any
) => {
  const accountNumberValue = data.accountNumber?.value;
  if (data.accountNumber) {
    const methodValid = stringValidate(accountNumberValue);
    const isValidMinLength = accountNumberValue && methodValid.minLength(16);

    if (!isValidMinLength) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER]: t(
          MESSAGE.ACCOUNT_NUMBER_SEARCH
        )
      };
    }
  }
};

export const validateLetterCode = (
  data: AttrSearchPendingCorrespondenceModel,
  t: any
) => {
  const letterCodeValue = data.letterCode?.value;
  if (data.letterCode) {
    const methodValid = stringValidate(letterCodeValue);
    const isValidMinLength =
    letterCodeValue &&
      methodValid.isSpecialAlphanumeric() &&
      methodValid.maxLength(4);

    if (!isValidMinLength) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.LETTER_CODE]: t(MESSAGE.LETTER_CODE_SEARCH)
      };
    }
  }
};

export const attrValidations = (
  data: AttrSearchPendingCorrespondenceModel,
  t: any
) => {
  const validateList = [validateAccountNumber, validateLetterCode];
  const runAllValidates = validateList.map(fn => {
    return fn(data, t);
  }, []);

  const errors: Record<string, string> = runAllValidates.reduce(
    (accumulator, current) => {
      if (!current) return accumulator;
      return { ...accumulator, ...current };
    },
    {}
  );
  return Object.keys(errors).length ? errors : undefined;
};
