import { I18N_TEXT } from 'app/constants';
import { NoCombineField } from 'app/_libraries/_dls/components';

export enum ATTRIBUTE_SEARCH_FIELD {
  ACCOUNT_NUMBER = 'accountNumber',
  LETTER_CODE = 'letterCode'
}

export const MESSAGE = {
  ACCOUNT_NUMBER_SEARCH: 'txt_account_number_must_be_16_digit',
  LETTER_CODE_SEARCH: 'txt_letter_code_is_required'
};

export const FIELD_NOT_CONSTRAINT: string[] = [
  ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER,
  ATTRIBUTE_SEARCH_FIELD.LETTER_CODE
];

export const NO_COMBINE_FIELDS: NoCombineField[] = [
  {
    field: ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER,
    filterMessage: I18N_TEXT.ACCOUNT_NUMBER_CORRESPONDENCE_CANNOT_COMBINED
  },
  {
    field: ATTRIBUTE_SEARCH_FIELD.LETTER_CODE,
    filterMessage: I18N_TEXT.LETTER_CODE_CANNOT_COMBINED
  }
];
