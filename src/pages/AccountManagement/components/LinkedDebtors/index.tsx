import { I18N_TEXT } from 'app/constants';
import { Button, Icon, Popover } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import classNames from 'classnames';
import React from 'react';

export interface ILinkedDebtorsProps {
  values: RefDataValue[];
}

const LinkedDebtors: React.FC<ILinkedDebtorsProps> = ({ values }) => {
  const { t } = useTranslation();

  return (
    <div className="d-flex">
      <span className="color-grey">{t(I18N_TEXT.LINKED_DEBTORS)}:</span>
      <div className="d-flex align-items-center ml-4">
        <span className="mr-8">{values?.length}</span>
        <Popover
          placement="top-start"
          size="auto"
          element={values?.map((item, index) => (
            <div
              key={item.value}
              className={classNames('d-flex', { 'mt-16': index !== 0 })}
            >
              <span>•</span>
              <div className="ml-12">
                <span>
                  {item.value}
                  <br />
                  <div className="color-grey mt-4">{item.description}</div>
                </span>
              </div>
            </div>
          ))}
        >
          <Button size="sm" variant="icon-secondary">
            <Icon name="information" />
          </Button>
        </Popover>
      </div>
    </div>
  );
};

export default LinkedDebtors;
