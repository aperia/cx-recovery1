import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';

// components
import {
  Button,
  InlineMessage,
  pageSizeInfo,
  Pagination
} from 'app/_libraries/_dls/components';
import { AssociatedDebtorItem } from 'pages/AccountManagement/ManageAssociatedDebtors/AssociatedDebtorItem';
import ApiErrorDetail from 'pages/__commons/ApiErrorDetail';

// redux
import { deleteAssociatedsDebtor } from 'pages/AccountManagement/_redux/associatedDebtor/deleteAssociatedDebtor';
import { accountManagementActions } from 'pages/AccountManagement/_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { isEmpty } from 'app/helpers';
import { checkNextPage } from 'pages/__commons/helpers';

// constants & types
import { ILinkDebtor } from 'pages/AccountManagement/types';
import { I18N_TEXT } from 'app/constants';
import { DEFAULT_PRIMARY_DEBTOR } from 'pages/AccountManagement/ManageAssociatedDebtors/constants';

interface ListDebtorProps {
  listLinkDebtors: ILinkDebtor[];
  currentPage: number;
  totalItem: number;
  debtorName: string;
  onToggleForm: () => void;
  showPageSize: number;
  setShowPageSize: (page: number) => void;
}
const ListLinkDebtor: React.FC<ListDebtorProps> = ({
  listLinkDebtors = [],
  currentPage = 1,
  totalItem = 0,
  debtorName = '',
  onToggleForm,
  showPageSize,
  setShowPageSize
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const handlePageChange = useCallback(
    (pageSelected: number) => {
      dispatch(
        accountManagementActions.getAssociatedDebtors({
          page: checkNextPage({
            currentPage: pageSelected,
            totalItem
          })
        })
      );
    },
    [dispatch, totalItem]
  );

  const onPageSizeChange = useCallback(
    (pageInfo: pageSizeInfo) => {
      setShowPageSize(pageInfo.size);
      dispatch(
        accountManagementActions.getAssociatedDebtors({
          page: 1,
          pageSize: pageInfo.size
        })
      );
    },
    [dispatch, setShowPageSize]
  );

  if (!isEmpty(listLinkDebtors)) {
    return (
      <>
        <ApiErrorDetail
          forAction={[deleteAssociatedsDebtor.rejected.type]}
          className="mb-24"
        />
        <div className="d-flex justify-content-between align-items-center mr-n8">
          <h4 className="text-capitalize">{t(I18N_TEXT.ASSOCIATED_DEBTORS)}</h4>
          <Button
            className="text-capitalize"
            variant="outline-primary"
            size="sm"
            onClick={onToggleForm}
          >
            {t(I18N_TEXT.LINK_DEBTOR)}
          </Button>
        </div>
        {listLinkDebtors?.map((item: ILinkDebtor) => (
          <AssociatedDebtorItem
            key={item.id}
            currentPage={currentPage}
            {...item}
          />
        ))}
        <div className="pt-16">
          <Pagination
            totalItem={totalItem}
            pageSize={[5, 10, 15]}
            pageNumber={currentPage}
            pageSizeValue={showPageSize}
            compact
            onChangePage={handlePageChange}
            onChangePageSize={onPageSizeChange}
          />
        </div>
      </>
    );
  }
  return (
    <>
      <h5 className="text-capitalize">{t(I18N_TEXT.ASSOCIATED_DEBTORS)}</h5>
      <InlineMessage variant="warning" className="mt-24">
        <p>
          {t(I18N_TEXT.ASSOCIATED_DEBTOR_INLINE_MSG)}{' '}
          <span>
            <Button
              className="text-capitalize"
              variant="outline-primary"
              size="sm"
              onClick={() =>
                dispatch(
                  accountManagementActions.triggerAddPrimaryLinkDebtors({
                    ...DEFAULT_PRIMARY_DEBTOR,
                    debtorName: debtorName
                  })
                )
              }
            >
              {t(I18N_TEXT.LINK_PRIMARY_DEBTOR)}
            </Button>
          </span>
        </p>
      </InlineMessage>
    </>
  );
};
export default ListLinkDebtor;
