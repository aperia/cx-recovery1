import React, { useMemo, useState } from 'react';

// components
import { AccDetailSnapshotDebtor } from 'pages/AccountManagement/ManageAssociatedDebtors/AccDetailSnapshotDebtor';
import { useLinkDebtorForm } from 'pages/AccountManagement/ManageAssociatedDebtors/hooks/useLinkDebtorForm';
import LinkDebtorModal from 'pages/AccountManagement/ManageAssociatedDebtors/LinkDebtorModal';
import ListLinkDebtor from './ListLinkDebtor';

// hooks
import { useSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import {
  selectedFormActionLinkDebtors,
  selectedLinkDebtorsCurrentPage,
  selectedLinkDebtorsData,
  selectedLinkDebtorsTotalData,
  selectorAllLinkDebtor,
  selectorLinkAnotherDebtor
} from 'pages/AccountManagement/_redux/associatedDebtor/selectors';
import { selectorDebtors } from 'pages/AccountManagement/_redux/debtorMaintenance/selectors';
import { selectorSearchAcc } from 'pages/AccountManagement/_redux/manualAssignRecall/selectors';
import { selectSnapAcc } from 'pages/AccountManagement/_redux/accountList/selector';

// constants && types
import { I18N_TEXT } from 'app/constants';
import {
  Debtors,
  FormAction,
  IAccounts,
  ILinkDebtor
} from 'pages/AccountManagement/types';

// helpers
import { isExistPrimaryDebtor } from 'pages/AccountManagement/ManageAssociatedDebtors/helpers';
import { isEmpty } from 'app/helpers';

export interface AssociatedDebtorsProps {
  fromAddAccount?: boolean;
}

const AssociatedDebtors: React.FC<AssociatedDebtorsProps> = ({
  fromAddAccount = false
}) => {
  const [showPageSize, setShowPageSize] = useState(5);
  const { t } = useTranslation();

  const formAction: FormAction = useSelector(selectedFormActionLinkDebtors);
  const totalItem: number = useSelector(selectedLinkDebtorsTotalData);
  const currentPage: number = useSelector(selectedLinkDebtorsCurrentPage);
  const listLinkDebtors: ILinkDebtor[] = useSelector(selectedLinkDebtorsData);

  const listAllLinkDebtors: ILinkDebtor[] = useSelector(selectorAllLinkDebtor);

  const debtorList: Debtors[] = useSelector(selectorDebtors);

  const selectLinkAnotherDebtor: boolean = useSelector(
    selectorLinkAnotherDebtor
  );

  const searchAcc: IAccounts = useSelector(selectorSearchAcc);

  const snapAcc = useSelector(selectSnapAcc);

  const checkEmptySeacrchAcc = useMemo(() => {
    return isEmpty(searchAcc);
  }, [searchAcc]);

  const {
    open: formOpen,
    values,
    validationSchema,
    onSubmit,
    onCancel,
    onToggleForm
  } = useLinkDebtorForm({
    accNumber: searchAcc?.accNumber?.accountNumberUnMask,
    pageSize: showPageSize
  });

  return (
    <>
      {fromAddAccount && (
        <AccDetailSnapshotDebtor
          accData={checkEmptySeacrchAcc ? snapAcc : searchAcc}
        />
      )}
      <div className="px-24 pt-24 max-width-lg mx-auto overlap-next-action">
        <ListLinkDebtor
          listLinkDebtors={listLinkDebtors}
          currentPage={currentPage}
          totalItem={totalItem}
          debtorName={
            checkEmptySeacrchAcc ? snapAcc?.debtorName : searchAcc?.debtorName
          }
          onToggleForm={onToggleForm}
          showPageSize={showPageSize}
          setShowPageSize={setShowPageSize}
        />
      </div>
      <LinkDebtorModal
        inforDebtor={{
          accNumber: searchAcc?.accNumber?.accountNumberUnMask,
          ssn: searchAcc?.ssn
        }}
        debtorList={debtorList}
        linkAnotherDebtorProp={selectLinkAnotherDebtor}
        open={formOpen}
        value={values}
        action={formAction}
        title={
          values.id
            ? t(I18N_TEXT.EDIT_DEBTOR_RELATION)
            : t(I18N_TEXT.LINK_DEBTOR)
        }
        validationSchema={validationSchema}
        onCancel={onCancel}
        onSubmit={onSubmit}
        existPrimaryDebtor={isExistPrimaryDebtor(listAllLinkDebtors)}
        isAddPrmary={
          !isExistPrimaryDebtor(listLinkDebtors) && currentPage === 1
        }
      />
    </>
  );
};

export default AssociatedDebtors;
