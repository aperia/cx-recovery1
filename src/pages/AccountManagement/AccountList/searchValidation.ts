import { stringValidate } from 'app/helpers/stringValidate';
import { ATTRIBUTE_SEARCH_FIELD, MESSAGE } from '../constant';
import { AttributeSearchFieldModel } from '../types';

export const validateAccountNumber = (
  data: AttributeSearchFieldModel,
  t: any
) => {
  const accountNumberValue = data.accountNumber?.value;
  if (data.accountNumber) {
    const methodValid = stringValidate(accountNumberValue);
    const isValidMinLength = accountNumberValue && methodValid.minLength(16);

    if (!isValidMinLength) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER]: t(
          MESSAGE.ACCOUNT_NUMBER_SEARCH
        )
      };
    }
  }
};

export const validateOriginalLoan = (
  data: AttributeSearchFieldModel,
  t: any
) => {
  const originalLoanValue = data.originalLoanNumber?.value;
  if (data.originalLoanNumber) {
    const methodValid = stringValidate(originalLoanValue);
    const isValidMinLength = originalLoanValue && methodValid.minLength(20);

    if (!isValidMinLength) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.ORIGINAL_LOAN_NUMBER]: t(
          MESSAGE.ORIGINAL_LOAN_NUMBER_SEARCH
        )
      };
    }
  }
};

export const validateDebtorName = (data: AttributeSearchFieldModel, t: any) => {
  if (data.debtorName && !data.debtorName?.value) {
    return { [ATTRIBUTE_SEARCH_FIELD.DEBTOR_NAME]: t(MESSAGE.DEBTOR_NAME) };
  }
};

export const validateSSN = (data: AttributeSearchFieldModel, t: any) => {
  const ssnValue = data.ssn?.value;
  if (data.ssn) {
    const methodValid = stringValidate(ssnValue?.trim());
    const isRequire = methodValid.isRequire();
    if (!isRequire || ssnValue?.includes('_')) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.SSN]: t(MESSAGE.SSN)
      };
    }
  }
};

export const validateAgencyCode = (data: AttributeSearchFieldModel, t: any) => {
  if (data.agencyCode && !data.agencyCode?.value) {
    return { [ATTRIBUTE_SEARCH_FIELD.AGENCY_CODE]: t(MESSAGE.AGENCY_CODE) };
  }
};

export const validateCalendarCode = (
  data: AttributeSearchFieldModel,
  t: any
) => {
  if (data.calendarCode && !data.calendarCode?.value) {
    return { [ATTRIBUTE_SEARCH_FIELD.CALENDAR_CODE]: t(MESSAGE.CALENDAR_CODE) };
  }
};

export const validateReasonCode = (data: AttributeSearchFieldModel, t: any) => {
  if (data.reasonCode && !data.reasonCode?.value) {
    return { [ATTRIBUTE_SEARCH_FIELD.REASON_CODE]: t(MESSAGE.REASON_CODE) };
  }
};

export const attrValidations = (data: AttributeSearchFieldModel, t: any) => {
  const validateList = [
    validateAccountNumber,
    validateOriginalLoan,
    validateDebtorName,
    validateSSN,
    validateAgencyCode,
    validateCalendarCode,
    validateReasonCode
  ];

  const runAllValidates = validateList.map(fn => {
    return fn(data, t);
  }, []);

  const errors: Record<string, string> = runAllValidates.reduce(
    (accumulator, current) => {
      if (!current) return accumulator;
      return { ...accumulator, ...current };
    },
    {}
  );
  return Object.keys(errors).length ? errors : undefined;
};
