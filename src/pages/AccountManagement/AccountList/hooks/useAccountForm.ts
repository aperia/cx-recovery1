import { useDispatch } from 'react-redux';

// hooks
import useValidation from './useValidation';
import { useSelector } from 'app/hooks';

import { accountManagementActions } from 'pages/AccountManagement/_redux';

// selectors
import { selectSelectedAccountForm } from './../../_redux/accountList/selector';

// types
import { IAccounts } from 'pages/AccountManagement/types';

// constants
import { InterestRate, RecoveryIndicator } from '../constants';

// helpers
import { useFormik } from 'formik';
import useRefData from 'app/hooks/useRefData';
import { getDescriptionFromCode } from 'pages/__commons/helpers';

const useAccountForm = () => {
  const dispatch = useDispatch();
  const agencyCodeOptions = useRefData('agencyCode');
  const bankruptcyOptions = useRefData('bankruptcy');
  const { validationSchema } = useValidation();
  const selected = useSelector<IAccounts | undefined>(
    selectSelectedAccountForm
  );

  const selectedId = selected?.id;

  const defaultValues = {
    interestRateType: InterestRate.FIGURE,
    recoveryIndicatorType: RecoveryIndicator.CHARGED_OFF_ACCOUNT
  };

  // these extra fields only displayed in edit mode
  const extraFields = selectedId
    ? {
        bankruptcy: getDescriptionFromCode(bankruptcyOptions, 'B'),
        previousAgencyCode: getDescriptionFromCode(
          agencyCodeOptions,
          selected?.agencyCode
        )
      }
    : {};

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      ...defaultValues,
      ...(selected ? { ...selected, ...extraFields } : {})
    },
    onSubmit: (values, formikBag) => {
      dispatch(
        accountManagementActions.saveAccount({
          id: selectedId,
          values,
          formikBag
        })
      );
    },
    validationSchema
  });

  return { formik };
};

export default useAccountForm;
