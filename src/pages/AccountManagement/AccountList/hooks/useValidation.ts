import { isEmpty } from 'app/helpers';
// helpers
import * as Yup from 'yup';

// constants
import { CUSTOM, I18N_TEXT } from 'app/constants';
import { ACCOUNT_FIELD } from '../constants';

const useValidation = () => {
  const validationSchema = Yup.object().shape({
    // Main
    [ACCOUNT_FIELD.ACCOUNT_NUMBER.name]: Yup.string().required(
      I18N_TEXT.ACCOUNT_NUMBER_REQUIRED
    ),
    [ACCOUNT_FIELD.CARD_HOLDER_NAME.name]: Yup.string().required(
      I18N_TEXT.CARD_HOLDER_NAME_REQUIRED
    ),
    [ACCOUNT_FIELD.SSN.name]: Yup.string().required(I18N_TEXT.SSN_REQUIRED),
    [ACCOUNT_FIELD.ORIGINAL_LOAN_NUMBER.name]: Yup.string().required(
      I18N_TEXT.ORIGINAL_LOAN_NUMBER_REQUIRED
    ),
    [ACCOUNT_FIELD.CREDIT_BUREAU_ACCOUNT_NUMBER.name]: Yup.string().required(
      I18N_TEXT.CREDIT_BUREAU_ACCOUNT_NUMBER_REQUIRED
    ),
    [ACCOUNT_FIELD.ORIGINAL_LOAN_AMOUNT.name]: Yup.string().required(
      I18N_TEXT.ORIGINAL_LOAN_AMOUNT_REQUIRED
    ),

    // General
    [ACCOUNT_FIELD.REASON_CODE.name]: Yup.string().required(
      I18N_TEXT.REASON_CODE_REQUIRED
    ),
    [ACCOUNT_FIELD.COLLECTOR_CODE.name]: Yup.string().required(
      I18N_TEXT.COLLECTOR_CODE_REQUIRED
    ),
    [ACCOUNT_FIELD.TYPE_CODE.name]: Yup.string().required(
      I18N_TEXT.TYPE_CODE_REQUIRED
    ),
    [ACCOUNT_FIELD.PORTFOLIO_TYPE.name]: Yup.string().required(
      I18N_TEXT.PORTFOLIO_TYPE_REQUIRED
    ),
    [ACCOUNT_FIELD.STATUS_CODE.name]: Yup.string().required(
      I18N_TEXT.STATUS_CODE_REQUIRED
    ),
    [ACCOUNT_FIELD.OPENED_DATE.name]: Yup.string().required(
      I18N_TEXT.OPENED_DATE_REQUIRED
    ),
    [ACCOUNT_FIELD.DELINQUENCY_START_DATE.name]: Yup.string().required(
      I18N_TEXT.DELINQUENCY_START_DATE_REQUIRED
    ),
    [ACCOUNT_FIELD.LOAD_DATE.name]: Yup.string().required(
      I18N_TEXT.LOAD_DATE_REQUIRED
    ),
    [ACCOUNT_FIELD.CHARGED_OFF_DATE.name]: Yup.string().required(
      I18N_TEXT.CHARGED_OFF_DATE_REQUIRED
    ),

    // Agency
    [ACCOUNT_FIELD.AGENCY_CODE.name]: Yup.string().required(
      I18N_TEXT.AGENCY_CODE_REQUIRED
    ),

    // Payment
    [ACCOUNT_FIELD.PAYMENT_SCHEDULE_FREQUENCY.name]: Yup.string().when(
      [ACCOUNT_FIELD.PAYMENT_SCHEDULE_AMOUNT.name],
      (value: string, schema: Yup.AnySchema) =>
        !isEmpty(value)
          ? schema.required(I18N_TEXT.PAYMENT_SCHEDULE_FREQUENCY_REQUIRED)
          : schema
    ),
    [ACCOUNT_FIELD.PAYMENT_SCHEDULE_FREQUENCY_CUSTOM.name]: Yup.string().when(
      [ACCOUNT_FIELD.PAYMENT_SCHEDULE_FREQUENCY.name],
      (value: string, schema: Yup.AnySchema) =>
        value === CUSTOM
          ? schema.required(
              I18N_TEXT.PAYMENT_SCHEDULE_FREQUENCY_CUSTOM_REQUIRED
            )
          : schema
    ),

    // Financial
    [ACCOUNT_FIELD.RESPONSIBILITY_CENTER.name]: Yup.string().required(
      I18N_TEXT.RESPONSIBILITY_CENTER_REQUIRED
    ),
    [ACCOUNT_FIELD.CLASS_1.name]: Yup.string().required(
      I18N_TEXT.CLASS_1_REQUIRED
    ),

    [ACCOUNT_FIELD.RESERVE_1.name]: Yup.string().when(
      [ACCOUNT_FIELD.CLASS_1.name],
      (value: string, schema: Yup.AnySchema) =>
        !isEmpty(value) ? schema.required(I18N_TEXT.RESERVE_1_REQUIRED) : schema
    ),
    [ACCOUNT_FIELD.RESERVE_1_AMOUNT.name]: Yup.string().when(
      [ACCOUNT_FIELD.CLASS_1.name],
      (value: string, schema: Yup.AnySchema) =>
        !isEmpty(value)
          ? schema.required(I18N_TEXT.RESERVE_1_AMOUNT_REQUIRED)
          : schema
    ),

    [ACCOUNT_FIELD.RESERVE_2.name]: Yup.string().when(
      [ACCOUNT_FIELD.CLASS_2.name],
      (value: string, schema: Yup.AnySchema) =>
        !isEmpty(value) ? schema.required(I18N_TEXT.RESERVE_2_REQUIRED) : schema
    ),
    [ACCOUNT_FIELD.RESERVE_2_AMOUNT.name]: Yup.string().when(
      [ACCOUNT_FIELD.CLASS_2.name],
      (value: string, schema: Yup.AnySchema) =>
        !isEmpty(value)
          ? schema.required(I18N_TEXT.RESERVE_2_AMOUNT_REQUIRED)
          : schema
    ),

    [ACCOUNT_FIELD.RESERVE_3.name]: Yup.string().when(
      [ACCOUNT_FIELD.CLASS_3.name],
      (value: string, schema: Yup.AnySchema) =>
        !isEmpty(value) ? schema.required(I18N_TEXT.RESERVE_3_REQUIRED) : schema
    ),
    [ACCOUNT_FIELD.RESERVE_3_AMOUNT.name]: Yup.string().when(
      [ACCOUNT_FIELD.CLASS_3.name],
      (value: string, schema: Yup.AnySchema) =>
        !isEmpty(value)
          ? schema.required(I18N_TEXT.RESERVE_3_AMOUNT_REQUIRED)
          : schema
    ),
    [ACCOUNT_FIELD.OTHER.name]: Yup.string().required(I18N_TEXT.OTHER_REQUIRED),
    [ACCOUNT_FIELD.OTHER_AMOUNT.name]: Yup.string().when(
      [ACCOUNT_FIELD.OTHER.name],
      (value: string, schema: Yup.AnySchema) =>
        !isEmpty(value)
          ? schema.required(I18N_TEXT.OTHER_AMOUNT_REQUIRED)
          : schema
    ),
    [ACCOUNT_FIELD.INTEREST.name]: Yup.string().required(
      I18N_TEXT.INTEREST_REQUIRED
    ),
    [ACCOUNT_FIELD.INTEREST_AMOUNT.name]: Yup.string().when(
      [ACCOUNT_FIELD.INTEREST.name],
      (value: string, schema: Yup.AnySchema) =>
        !isEmpty(value)
          ? schema.required(I18N_TEXT.INTEREST_AMOUNT_REQUIRED)
          : schema
    ),
    [ACCOUNT_FIELD.COST.name]: Yup.string().required(I18N_TEXT.COST_REQUIRED),
    [ACCOUNT_FIELD.COST_AMOUNT.name]: Yup.string().when(
      [ACCOUNT_FIELD.COST.name],
      (value: string, schema: Yup.AnySchema) =>
        !isEmpty(value)
          ? schema.required(I18N_TEXT.COST_AMOUNT_REQUIRED)
          : schema
    ),

    // Miscellaneous
    [ACCOUNT_FIELD.COMPLIANCE_CODE.name]: Yup.string().required(
      I18N_TEXT.COMPLIANCE_CODE_REQUIRED
    ),
    [ACCOUNT_FIELD.CREDIT_BUREAU.name]: Yup.string().required(
      I18N_TEXT.CREDIT_BUREAU_REQUIRED
    )
  });

  return {
    validationSchema
  };
};

export default useValidation;
