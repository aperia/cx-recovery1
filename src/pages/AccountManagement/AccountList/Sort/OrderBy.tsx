import React, { useCallback } from 'react';

// component
import OrderByAccounts from 'pages/__commons/AccountList/OrderByAccounts';

// redux
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { selectorOrderBy } from 'pages/AccountManagement/_redux/accountList/selector';
import { useDispatch } from 'react-redux';

// hooks
import { useSelector } from 'app/hooks';

interface OrderByAccountListProps {}

const OrderByAccountList: React.FC<OrderByAccountListProps> = () => {
  const dispatch = useDispatch();
  const value: RefDataValue = useSelector(selectorOrderBy);

  const handleChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      dispatch(accountManagementActions.onChangeOrderBy(e.value));
    },
    [dispatch]
  );

  return <OrderByAccounts value={value} handleChange={handleChange} />;
};
export default OrderByAccountList;
