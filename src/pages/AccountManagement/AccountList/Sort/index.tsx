import React from 'react';

// component
import StatusAccountList from './Status';
import SortByAccountList from './SortBy';
import OrderByAccountList from './OrderBy';
import ResponsiveSortOrder from './ResponsiveSortOrder';

// redux
import { useSelector } from 'react-redux';
import { selectorAccountList } from 'pages/AccountManagement/_redux/accountList/selector';

// helpers
import classNames from 'classnames';

interface SortAccountListProps {}

const SortAccountList: React.FC<SortAccountListProps> = () => {
  const accounts = useSelector(selectorAccountList);

  return (
    <div className="flex-1 d-flex justify-content-between align-items-center">
      <StatusAccountList />
      <div className="d-flex mr-n8">
        <div
          className={classNames(
            'd-lg-none d-block',
            accounts.length ? 'mr-md-n8' : 'mr-md-4'
          )}
        >
          <ResponsiveSortOrder />
        </div>

        <div className="d-none d-lg-block">
          <div className="d-flex align-items-center">
            <SortByAccountList />
            <OrderByAccountList />
          </div>
        </div>
      </div>
    </div>
  );
};
export default SortAccountList;
