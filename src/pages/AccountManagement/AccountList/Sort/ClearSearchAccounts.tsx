import React from 'react';

// component
import { Button } from 'app/_libraries/_dls/components';

// redux
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { useDispatch, useSelector } from 'react-redux';
import { selectorAccountList } from 'pages/AccountManagement/_redux/accountList/selector';

// hooks
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';

// helper
import { I18N_TEXT } from 'app/constants';
import { checkEmpty } from 'app/helpers';
import { IAccounts } from 'pages/AccountManagement/types';

interface ClearSearchAccountsProps {}

const ClearSearchAccounts: React.FC<ClearSearchAccountsProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const accounts: IAccounts[] = useSelector(selectorAccountList);

  const onClick = () => {
    dispatch(accountManagementActions.onClearAndReset());
  };

  if (!checkEmpty([accounts, false])) return null;

  return (
    <Button
      className="ml-16"
      variant={'outline-primary'}
      size={'sm'}
      onClick={onClick}
    >
      {t(I18N_TEXT.CLEAR_AND_RESET)}
    </Button>
  );
};
export default ClearSearchAccounts;
