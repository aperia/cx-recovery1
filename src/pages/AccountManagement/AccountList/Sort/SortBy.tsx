import React, { useCallback } from 'react';

// component
import SortByAccounts from 'pages/__commons/AccountList/SortByAccounts';

// redux
import { selectorSortBy } from 'pages/AccountManagement/_redux/accountList/selector';
import { useDispatch, useSelector } from 'react-redux';
import { accountManagementActions } from 'pages/AccountManagement/_redux';

// helper
import { dropdownSortAccounts } from 'app/constants';

interface SortByAccountListProps {}

const SortByAccountList: React.FC<SortByAccountListProps> = () => {
  const dispatch = useDispatch();
  const value: RefDataValue = useSelector(selectorSortBy);

  const handleChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      dispatch(accountManagementActions.onChangeSortAccounts(e.value));
    },
    [dispatch]
  );

  return (
    <SortByAccounts
      value={value}
      dropdownSortAccounts={dropdownSortAccounts}
      handleChange={handleChange}
    />
  );
};
export default SortByAccountList;
