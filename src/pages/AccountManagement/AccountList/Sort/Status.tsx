import React from 'react';

// component
import StatusAccounts from 'pages/__commons/AccountList/StatusAccounts';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { selectorStatus } from 'pages/AccountManagement/_redux/accountList/selector';

// helper
import { TypeStatusAccountList } from 'pages/AccountManagement/types';

interface StatusAccountListProps {}

const StatusAccountList: React.FC<StatusAccountListProps> = () => {
  const dispatch = useDispatch();
  const value: TypeStatusAccountList = useSelector(selectorStatus);

  const handleChange = (value: string) => {
    dispatch(
      accountManagementActions.onChangeStatus(value as TypeStatusAccountList)
    );
  };

  return <StatusAccounts value={value} handleChange={handleChange} />;
};
export default StatusAccountList;
