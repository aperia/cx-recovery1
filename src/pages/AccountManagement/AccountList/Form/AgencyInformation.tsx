import React, { useCallback } from 'react';

// components
import {
  DatePickerControl,
  DropdownControl,
  generateDescription,
  TextBoxControl
} from 'app/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelector } from 'app/hooks';
import useRefData from 'app/hooks/useRefData';

// constants
import { I18N_TEXT, MAX_LENGTH_15 } from 'app/constants';

// types
import { IAccounts } from 'pages/AccountManagement/types';

// selectors
import { selectSelectedAccountForm } from 'pages/AccountManagement/_redux/accountList/selector';

export interface AgencyInformationProps {}

const AgencyInformation: React.FC<AgencyInformationProps> = () => {
  const { t } = useTranslation();
  const agencyCodeOptions = useRefData('agencyCode');
  const letterSeriesOptions = useRefData('letterSeries');
  const agencyFeeCodeOptions = useRefData('agencyFeeCode');
  const agencyLevelOptions = useRefData('agencyLevel');

  const selected = useSelector<IAccounts | undefined>(
    selectSelectedAccountForm
  );
  const editMode = !!selected?.id;

  const renderExtraFields = useCallback(() => {
    const { agencyCode: selectedAgencyCode = '' } = selected || {};
    const itemValue =
    agencyCodeOptions.find(item => item.value === selectedAgencyCode) ||
      ({} as RefDataValue);

    const displayValue = generateDescription(itemValue, true);
    
    return (
      <>
        <TextBoxControl
          name="previousAgencyCode"
          label={t(I18N_TEXT.PREVIOUS_AGENCY_CODE)}
          className="col-6 col-lg-3 mt-16"
          value={displayValue}
          readOnly
        />
      </>
    );
  }, [agencyCodeOptions, selected, t]);

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.AGENCY_INFORMATION)}</h5>
      </div>

      <DropdownControl
        name="agencyCode"
        label={t(I18N_TEXT.AGENCY_CODE)}
        className="col-6 col-lg-3 mt-16"
        required
        options={agencyCodeOptions}
        isCombine
      />

      <DropdownControl
        name="letterSeries"
        label={t(I18N_TEXT.LETTER_SERIES)}
        className="col-6 col-lg-3 mt-16"
        options={letterSeriesOptions}
        isCombine
      />

      <DropdownControl
        name="agencyFeeCode"
        label={t(I18N_TEXT.AGENCY_FEE_CODE)}
        className="col-6 col-lg-3 mt-16 text-capitalize"
        options={agencyFeeCodeOptions}
        isCombine
      />

      <DatePickerControl
        name="dateToAgency"
        label={t(I18N_TEXT.DATE_TO_AGENCY)}
        className="col-6 col-lg-3 mt-16 text-transform-none"
      />

      <DropdownControl
        name="agencyLevel"
        label={t(I18N_TEXT.AGENCY_LEVEL)}
        className="col-6 col-lg-3 mt-16"
        options={agencyLevelOptions}
        isCombine
      />

      <TextBoxControl
        name="caseNumber"
        label={t(I18N_TEXT.CASE_NUMBER)}
        className="col-6 col-lg-3 mt-16"
        maxLength={MAX_LENGTH_15}
      />

      {editMode && renderExtraFields()}
    </div>
  );
};

export default React.memo(AgencyInformation);
