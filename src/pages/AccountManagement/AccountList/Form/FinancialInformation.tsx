import React, { useCallback } from 'react';

// components
import { GroupRadioControl } from 'app/components/FormControl/GroupRadio';
import {
  ComboBoxControl,
  DropdownControl,
  TextBoxControl
} from 'app/components';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useRefData from 'app/hooks/useRefData';

// constants
import { I18N_TEXT, MAX_LENGTH_10, MAX_LENGTH_15 } from 'app/constants';

// helpers
import { useFormikContext } from 'formik';
import { isEmpty } from 'lodash';
import { getPOICFromReserveCode } from 'pages/AccountManagement/helpers';

export interface FinancialInformationProps {}

const FinancialInformation: React.FC<FinancialInformationProps> = () => {
  const { t } = useTranslation();
  const classOptions = useRefData('class');
  const reserveOptions = useRefData('reserve');
  const reservePOICOptions = useRefData('reservePOIC');
  const recoveryIndicatorTypeOptions = useRefData('recoveryIndicatorType');

  const { handleChange, setFieldValue, values } = useFormikContext();

  const { class1, class2, class3, other, interest, cost } = values as any;

  const handleValueChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      // trigger change formik's value
      const item = e.value;
      const event = {
        ...e,
        target: {
          ...e.target,
          value: item.value
        }
      };

      handleChange(event);

      // set POIC correspond to selected reserve code
      const poicValue = getPOICFromReserveCode(reservePOICOptions, item?.value);
      const reserve1POICFieldName = `${e.target.name}Poic`;
      setFieldValue(reserve1POICFieldName, poicValue || '', false);
    },
    [handleChange, reservePOICOptions, setFieldValue]
  );

  const handleCheckboxChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      handleChange(event);
    },
    [handleChange]
  );

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.FINANCIAL_INFORMATION)}</h5>
      </div>
      <div className="col-12 mt-16">
        <div className="d-flex">
          <p className="fw-500 color-grey mr-24">
            {t(I18N_TEXT.RECOVERY_INDICATOR)}:
          </p>
          <GroupRadioControl
            name="recoveryIndicatorType"
            direction="horizontal"
            options={recoveryIndicatorTypeOptions}
            onValueChange={handleCheckboxChange}
          />
        </div>
      </div>

      <TextBoxControl
        name="responsibilityCenter"
        label={t(I18N_TEXT.RESPONSIBILITY_CENTER)}
        className="col-6 mt-16"
        maxLength={MAX_LENGTH_10}
        required
      />

      <FormikNumericControl
        name="highestStatementBalance"
        label={t(I18N_TEXT.HIGHEST_STATEMENT_BALANCE)}
        className="col-6 mt-16"
        maxLength={MAX_LENGTH_15}
        format="c2"
      />

      <DropdownControl
        name="class1"
        label={t(I18N_TEXT.CLASS_1)}
        className="w-50 w-lg-20 mt-16"
        required
        options={classOptions}
        isCombine
      />

      <DropdownControl
        name="subClass1"
        label={t(I18N_TEXT.SUB_CLASS_1)}
        className="w-50 w-lg-20 mt-16"
        options={classOptions}
        isCombine
        disabled={isEmpty(class1)}
      />

      <ComboBoxControl
        required={!isEmpty(class1)}
        disabled={isEmpty(class1)}
        name="reserve1"
        className="w-35 w-lg-20 mt-16 mx-auto"
        label={t(I18N_TEXT.RESERVE_1)}
        options={reserveOptions}
        placeholder={t(I18N_TEXT.TYPE_AT_LEAST_3_CHARS)}
        isCombine
        onValueChange={handleValueChange}
      />

      <TextBoxControl
        name="reserve1Poic"
        label={t(I18N_TEXT.RESERVE_1_POIC)}
        className="w-30 w-lg-20 mt-16"
        readOnly
      />

      <FormikNumericControl
        name="reserve1Amount"
        label={t(I18N_TEXT.RESERVE_1_AMOUNT)}
        className="w-35 w-lg-20 mt-16"
        required={!isEmpty(class1)}
        disabled={isEmpty(class1)}
        maxLength={MAX_LENGTH_15}
        format="c2"
      />

      <DropdownControl
        name="class2"
        label={t(I18N_TEXT.CLASS_2)}
        className="w-50 w-lg-20 mt-16"
        options={classOptions}
        isCombine
      />

      <DropdownControl
        name="subClass2"
        label={t(I18N_TEXT.SUB_CLASS_2)}
        className="w-50 w-lg-20 mt-16"
        options={classOptions}
        isCombine
        disabled={isEmpty(class2)}
      />

      <ComboBoxControl
        required={!isEmpty(class2)}
        disabled={isEmpty(class2)}
        name="reserve2"
        className="w-35 w-lg-20 mt-16"
        label={t(I18N_TEXT.RESERVE_2)}
        options={reserveOptions}
        placeholder={t(I18N_TEXT.TYPE_AT_LEAST_3_CHARS)}
        isCombine
        onValueChange={handleValueChange}
      />

      <TextBoxControl
        name="reserve2Poic"
        label={t(I18N_TEXT.RESERVE_2_POIC)}
        className="w-30 w-lg-20 mt-16"
        readOnly
      />

      <FormikNumericControl
        name="reserve2Amount"
        label={t(I18N_TEXT.RESERVE_2_AMOUNT)}
        className="w-35 w-lg-20 mt-16"
        required={!isEmpty(class2)}
        disabled={isEmpty(class2)}
        maxLength={MAX_LENGTH_15}
        format="c2"
      />

      <DropdownControl
        name="class3"
        label={t(I18N_TEXT.CLASS_3)}
        className="w-50 w-lg-20 mt-16"
        options={classOptions}
        isCombine
      />

      <DropdownControl
        name="subClass3"
        label={t(I18N_TEXT.SUB_CLASS_3)}
        className="w-50 w-lg-20 mt-16"
        options={classOptions}
        isCombine
        disabled={isEmpty(class3)}
      />

      <ComboBoxControl
        required={!isEmpty(class3)}
        disabled={isEmpty(class3)}
        name="reserve3"
        className="w-35 w-lg-20 mt-16"
        label={t(I18N_TEXT.RESERVE_3)}
        options={reserveOptions}
        placeholder={t(I18N_TEXT.TYPE_AT_LEAST_3_CHARS)}
        isCombine
        onValueChange={handleValueChange}
      />

      <TextBoxControl
        name="reserve3Poic"
        label={t(I18N_TEXT.RESERVE_3_POIC)}
        className="w-30 w-lg-20 mt-16"
        readOnly
      />

      <FormikNumericControl
        name="reserve3Amount"
        label={t(I18N_TEXT.RESERVE_3_AMOUNT)}
        className="w-35 w-lg-20 mt-16"
        required={!isEmpty(class3)}
        disabled={isEmpty(class3)}
        maxLength={MAX_LENGTH_15}
        format="c2"
      />

      <div className="d-none d-lg-block w-lg-40"></div>
      <DropdownControl
        name="other"
        label={t(I18N_TEXT.OTHER)}
        className="w-35 w-lg-20 mt-16"
        required
        options={reserveOptions}
        isCombine
        onValueChange={handleValueChange}
      />

      <TextBoxControl
        name="otherPoic"
        label={t(I18N_TEXT.OTHER_POIC)}
        className="w-30 w-lg-20 mt-16"
        readOnly
      />

      <FormikNumericControl
        name="otherAmount"
        label={t(I18N_TEXT.OTHER_AMOUNT)}
        className="w-35 w-lg-20 mt-16"
        maxLength={MAX_LENGTH_15}
        format="c2"
        required={!isEmpty(other)}
        disabled={isEmpty(other)}
      />

      <div className="d-none d-lg-block w-lg-40"></div>
      <DropdownControl
        name="interest"
        label={t(I18N_TEXT.INTEREST)}
        className="w-35 w-lg-20 mt-16"
        required
        options={reserveOptions}
        isCombine
        onValueChange={handleValueChange}
      />

      <TextBoxControl
        name="interestPoic"
        label={t(I18N_TEXT.INTEREST_POIC)}
        className="w-30 w-lg-20 mt-16"
        readOnly
      />

      <FormikNumericControl
        name="interestAmount"
        label={t(I18N_TEXT.INTEREST_AMOUNT)}
        className="w-35 w-lg-20 mt-16"
        maxLength={MAX_LENGTH_15}
        format="c2"
        required={!isEmpty(interest)}
        disabled={isEmpty(interest)}
      />

      <div className="d-none d-lg-block w-lg-40"></div>
      <DropdownControl
        name="cost"
        label={t(I18N_TEXT.COST)}
        className="w-35 w-lg-20 mt-16"
        required
        options={reserveOptions}
        isCombine
        onValueChange={handleValueChange}
      />

      <TextBoxControl
        name="costPoic"
        label={t(I18N_TEXT.COST_POIC)}
        className="w-30 w-lg-20 mt-16"
        readOnly
      />

      <FormikNumericControl
        name="costAmount"
        label={t(I18N_TEXT.COST_AMOUNT)}
        className="w-35 w-lg-20 mt-16"
        maxLength={MAX_LENGTH_15}
        format="c2"
        required={!isEmpty(cost)}
        disabled={isEmpty(cost)}
      />
    </div>
  );
};

export default React.memo(FinancialInformation);
