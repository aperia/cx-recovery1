import React from 'react';

// components
import { DropdownControl, TextBoxControl } from 'app/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useRefData from 'app/hooks/useRefData';

// constants
import {
  I18N_TEXT,
  MAX_LENGTH_2,
  MAX_LENGTH_3,
  MAX_LENGTH_5,
  MAX_LENGTH_25,
  NUMERIC_REPLACE_REGEX
} from 'app/constants';

export interface MiscellaneousInformationProps {}

const MiscellaneousInformation: React.FC<MiscellaneousInformationProps> =
  () => {
    const { t } = useTranslation();

    const complianceCodeOptions = useRefData('complianceCode');
    const creditBureauOptions = useRefData('creditBureau');
    const specialCommentOptions = useRefData('specialComment');

    return (
      <div className="row">
        <div className="col-12">
          <h5>{t(I18N_TEXT.MISCELLANEOUS_INFORMATION)}</h5>
        </div>

        <DropdownControl
          name="complianceCode"
          label={t(I18N_TEXT.COMPLIANCE_CODE)}
          className="col-6 col-lg-4 mt-16"
          required
          options={complianceCodeOptions}
          isCombine
        />

        <DropdownControl
          name="creditBureau"
          label={t(I18N_TEXT.CREDIT_BUREAU)}
          className="col-6 col-lg-4 mt-16"
          required
          options={creditBureauOptions}
          isCombine
        />

        <TextBoxControl
          name="reference"
          label={t(I18N_TEXT.REFERENCE)}
          className="col-6 col-lg-4 mt-16"
          maxLength={MAX_LENGTH_25}
        />

        <DropdownControl
          name="specialComment"
          label={t(I18N_TEXT.SPECIAL_COMMENT)}
          className="col-6 col-lg-4 mt-16"
          options={specialCommentOptions}
          isCombine
        />

        <div className="col-12 col-lg-8">
          <div className="row">
            <TextBoxControl
              name="frsic"
              label={t(I18N_TEXT.FRSIC)}
              className="col-3 mt-16 text-uppercase"
              maxLength={MAX_LENGTH_5}
            />

            <TextBoxControl
              name="sic"
              label={t(I18N_TEXT.SIC)}
              className="col-3 mt-16 text-uppercase"
              maxLength={MAX_LENGTH_5}
            />

            <TextBoxControl
              name="cycle"
              label={t(I18N_TEXT.CYCLE)}
              className="col-3 mt-16"
              maxLength={MAX_LENGTH_2}
              regex={NUMERIC_REPLACE_REGEX}
            />

            <TextBoxControl
              name="term_duration"
              label={t(I18N_TEXT.TERM_DURATION)}
              className="col-3 mt-16"
              maxLength={MAX_LENGTH_3}
              regex={NUMERIC_REPLACE_REGEX}
            />
          </div>
        </div>
      </div>
    );
  };

export default React.memo(MiscellaneousInformation);
