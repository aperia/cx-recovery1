import React, { useCallback } from 'react';

// components
import {
  DatePickerControl,
  DropdownControl,
  TextBoxControl
} from 'app/components';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';
import FormikTextAreaControl from 'app/components/FormControl/FormikTextAreaControl';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelector } from 'app/hooks';
import useRefData from 'app/hooks/useRefData';

// constants
import {
  I18N_TEXT,
  MAX_LENGTH_15,
  MAX_LENGTH_17,
  MAX_LENGTH_20,
  MAX_LENGTH_400,
  MAX_LENGTH_5
} from 'app/constants';

// types
import { IAccounts } from 'pages/AccountManagement/types';

// selectors
import { selectSelectedAccountForm } from 'pages/AccountManagement/_redux/accountList/selector';

export interface GeneralInformationProps {}

const GeneralInformation: React.FC<GeneralInformationProps> = () => {
  const { t } = useTranslation();
  const reasonOptions = useRefData('externalStatusReason');
  const miscellaneousOptions = useRefData('miscellaneous');
  const statusCodeOptions = useRefData('statusCode');
  const collectorCodeOptions = useRefData('accountCollectorCode');
  const typeCodeOptions = useRefData('typeCode');
  const portfolioTypeOptions = useRefData('portfolioType');
  const officerOptions = useRefData('officer');
  const statusTypeOptions = useRefData('statusType');

  const selected = useSelector<IAccounts | undefined>(
    selectSelectedAccountForm
  );
  const editMode = !!selected?.id;

  const renderExtraFields = useCallback(() => {
    return (
      <>
        <TextBoxControl
          name="inLitigation"
          label={t('txt_in_litigation')}
          className="col-6 col-lg-3 mt-16"
          readOnly
        />

        <TextBoxControl
          name="bankruptcy"
          label={t('txt_bankruptcy')}
          className="col-6 col-lg-3 mt-16"
          readOnly
        />

        <TextBoxControl
          name="deceased"
          label={t('txt_deceased')}
          className="col-6 col-lg-3 mt-16"
          readOnly
        />

        <TextBoxControl
          name="fraud"
          label={t('txt_fraud')}
          className="col-6 col-lg-3 mt-16"
          readOnly
        />
      </>
    );
  }, [t]);

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.GENERAL_INFORMATION)}</h5>
      </div>

      <DropdownControl
        name="reasonCode"
        label={t(I18N_TEXT.REASON_CODE)}
        className="col-6 col-lg-4 mt-16"
        required
        options={reasonOptions}
        isCombine
      />

      <DropdownControl
        name="miscellaneous1"
        label={t(I18N_TEXT.MISCELLANEOUS_1)}
        className="col-6 col-lg-4 mt-16"
        options={miscellaneousOptions}
        isCombine
      />

      <DropdownControl
        name="miscellaneous2"
        label={t(I18N_TEXT.MISCELLANEOUS_2)}
        className="col-6 col-lg-4 mt-16"
        options={miscellaneousOptions}
        isCombine
      />

      <DropdownControl
        name="collectorCode"
        label={t(I18N_TEXT.COLLECTOR_CODE)}
        className="col-6 col-lg-3 mt-16"
        required
        options={collectorCodeOptions}
        isCombine
      />

      <DropdownControl
        name="typeCode"
        label={t(I18N_TEXT.TYPE_CODE)}
        className="col-6 col-lg-3 mt-16"
        required
        options={typeCodeOptions}
        isCombine
      />

      <DropdownControl
        name="portfolioType"
        label={t(I18N_TEXT.PORTFOLIO_TYPE)}
        className="col-6 col-lg-3 mt-16"
        required
        options={portfolioTypeOptions}
        isCombine
      />

      <DropdownControl
        name="officer"
        label={t(I18N_TEXT.OFFICER)}
        className="col-6 col-lg-3 mt-16"
        options={officerOptions}
        isCombine
      />

      {editMode && renderExtraFields()}

      <DropdownControl
        name="statusCode"
        label={t(I18N_TEXT.STATUS_CODE)}
        className="col-6 col-lg-4 mt-16"
        required
        options={statusCodeOptions}
        isCombine
      />

      <DropdownControl
        name="statusType"
        label={t(I18N_TEXT.STATUS_TYPE)}
        className="col-6 col-lg-4 mt-16"
        options={statusTypeOptions}
      />

      <DatePickerControl
        name="statusDate"
        label={t(I18N_TEXT.STATUS_DATE)}
        className="col-6 col-lg-4 mt-16 text-transform-none"
      />

      <DatePickerControl
        name="openedDate"
        label={t(I18N_TEXT.OPENED_DATE)}
        className="col-6 col-lg-4 mt-16 text-transform-none"
        required
      />

      <DatePickerControl
        name="delinquencyStartDate"
        label={t(I18N_TEXT.DELINQUENCY_START_DATE)}
        className="col-6 col-lg-4 mt-16 text-transform-none"
        required
      />

      <DatePickerControl
        name="loadDate"
        label={t(I18N_TEXT.LOAD_DATE)}
        className="col-6 col-lg-4 mt-16 text-transform-none"
        required
      />

      <FormikNumericControl
        name="preChargedOffAmount"
        label={t(I18N_TEXT.PRE_CHARGED_OFF_AMOUNT)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_15}
        format="c2"
      />

      <FormikNumericControl
        name="ChargedOffAmount"
        label={t(I18N_TEXT.CHARGED_OFF_AMOUNT)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_15}
        format="c2"
      />

      <DatePickerControl
        name="chargedOffDate"
        label={t(I18N_TEXT.CHARGED_OFF_DATE)}
        className="col-6 col-lg-4 mt-16 text-transform-none"
        required
      />

      <div className="col-12 col-lg-6">
        <div className="row">
          <TextBoxControl
            name="shortField1"
            label={t(I18N_TEXT.SHORT_FIELD_1)}
            className="col-6 mt-16"
            maxLength={MAX_LENGTH_5}
          />
          <TextBoxControl
            name="shortField2"
            label={t(I18N_TEXT.SHORT_FIELD_2)}
            className="col-6 mt-16"
            maxLength={MAX_LENGTH_5}
          />
          <DatePickerControl
            name="dateField1"
            label={t(I18N_TEXT.DATE_FIELD_1)}
            className="col-6 mt-16"
          />
          <DatePickerControl
            name="dateField2"
            label={t(I18N_TEXT.DATE_FIELD_2)}
            className="col-6 mt-16"
          />
          <TextBoxControl
            name="longField1"
            label={t(I18N_TEXT.LONG_FIELD_1)}
            className="col-6 mt-16"
            maxLength={MAX_LENGTH_20}
          />
          <TextBoxControl
            name="longField2"
            label={t(I18N_TEXT.LONG_FIELD_2)}
            className="col-6 mt-16"
            maxLength={MAX_LENGTH_20}
          />

          <FormikNumericControl
            name="amount1"
            label={t(I18N_TEXT.AMOUNT_1)}
            className="col-6 mt-16"
            maxLength={MAX_LENGTH_17}
            format="c2"
          />

          <FormikNumericControl
            name="amount2"
            label={t(I18N_TEXT.AMOUNT_2)}
            className="col-6 mt-16"
            maxLength={MAX_LENGTH_17}
            format="c2"
          />
        </div>
      </div>

      <div className="col-12 col-lg-6 mt-16">
        <FormikTextAreaControl
          className="h-lg-140px"
          name="comment"
          label={t(I18N_TEXT.COMMENT)}
          maxLength={MAX_LENGTH_400}
          // TODO: regex={ALPHA_NUMERIC_REPLACE_REGEX}
          disabledEnter
        />
      </div>
    </div>
  );
};

export default React.memo(GeneralInformation);
