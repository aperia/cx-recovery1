import React, { useCallback } from 'react';

// components
import {
  DatePickerControl,
  DropdownControl,
  TextBoxControl
} from 'app/components';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';
import { GroupRadioControl } from 'app/components/FormControl/GroupRadio';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useRefData from 'app/hooks/useRefData';

// constants
import {
  CUSTOM,
  I18N_TEXT,
  MAX_LENGTH_13,
  MAX_LENGTH_15,
  MAX_LENGTH_3,
  NUMERIC_REPLACE_REGEX
} from 'app/constants';

// helpers
import { useFormikContext } from 'formik';
import { InterestRate } from '../constants';
import { isEmpty } from 'lodash';

export interface PaymentInformationProps {}

const PaymentInformation: React.FC<PaymentInformationProps> = () => {
  const { t } = useTranslation();
  const interestRateTypeOptions = useRefData('interestRateType');
  const interestRateCodeOptions = useRefData('interestRateCode');
  const paymentScheduleFrequencyOptions = useRefData(
    'paymentScheduleFrequency'
  );

  const { handleChange, values } = useFormikContext();

  const { interestRateType, paymentScheduleAmount, paymentScheduleFrequency } =
    values as any;

  const renderInterestRate = useCallback(() => {
    if (interestRateType === InterestRate.FIGURE) {
      return (
        <FormikNumericControl
          name="interestRate"
          label={t(I18N_TEXT.INTEREST_RATE)}
          className="col-6 col-lg-3 mt-16"
          maxLength={MAX_LENGTH_15}
          format="p2"
        />
      );
    }

    return (
      <DropdownControl
        name="interestRate"
        label={t(I18N_TEXT.INTEREST_RATE_CODE)}
        className="col-6 col-lg-3 mt-16"
        options={interestRateCodeOptions}
        isCombine
      />
    );
  }, [interestRateType, interestRateCodeOptions, t]);

  const renderPaymentScheduleFrequencyCustom = useCallback(() => {
    if ((paymentScheduleFrequency + '').toLowerCase() === CUSTOM) {
      return (
        <TextBoxControl
          name="paymentScheduleFrequencyCustom"
          label={t(I18N_TEXT.COUNT)}
          className="col-6 col-lg-3 mt-16"
          maxLength={MAX_LENGTH_3}
          regex={NUMERIC_REPLACE_REGEX}
          required
        />
      );
    }

    return <div className="d-none d-lg-block col-lg-3"></div>;
  }, [paymentScheduleFrequency, t]);

  const handleCheckboxChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      handleChange(event);
    },
    [handleChange]
  );

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.PAYMENT_INFORMATION)}</h5>
      </div>
      <div className="col-12 mt-16">
        <div className="d-flex">
          <p className="fw-500 color-grey mr-24">
            {t(I18N_TEXT.INTEREST_RATE)}:
          </p>
          <GroupRadioControl
            name="interestRateType"
            direction="horizontal"
            options={interestRateTypeOptions}
            onValueChange={handleCheckboxChange}
          />
        </div>
      </div>

      {renderInterestRate()}

      <DatePickerControl
        name="interestStartDate"
        label={t(I18N_TEXT.INTEREST_START_DATE)}
        className="col-6 col-lg-3 mt-16 text-transform-none"
      />

      <DatePickerControl
        name="interestThroughDate"
        label={t(I18N_TEXT.INTEREST_THROUGH_DATE)}
        className="col-6 col-lg-3 mt-16 text-transform-none"
      />

      <div className="d-none d-lg-block col-lg-3"></div>

      <FormikNumericControl
        name="paymentScheduleAmount"
        label={t(I18N_TEXT.PAYMENT_SCHEDULE_AMOUNT)}
        className="col-6 col-lg-3 mt-16"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />

      <DatePickerControl
        name="paymentScheduleStartDate"
        label={t(I18N_TEXT.PAYMENT_SCHEDULE_START_DATE)}
        className="col-6 col-lg-3 mt-16 text-transform-none"
      />

      <DropdownControl
        name="paymentScheduleFrequency"
        label={t(I18N_TEXT.PAYMENT_SCHEDULE_FREQUENCY)}
        className="col-6 col-lg-3 mt-16"
        options={paymentScheduleFrequencyOptions}
        required={!isEmpty(paymentScheduleAmount)}
      />

      {renderPaymentScheduleFrequencyCustom()}
    </div>
  );
};

export default React.memo(PaymentInformation);
