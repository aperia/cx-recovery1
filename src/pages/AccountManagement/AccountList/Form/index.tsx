import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { FormikProvider } from 'formik';

// components
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import MainInformation from './MainInformation';
import GeneralInformation from './GeneralInformation';
import AgencyInformation from './AgencyInformation';
import PaymentInformation from './PaymentInformation';
import FinancialInformation from './FinancialInformation';
import MiscellaneousInformation from './MiscellaneousInformation';

// hooks
import { useSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useAccountForm from '../hooks/useAccountForm';

// selectors
import {
  selectAccountFormIsOpen,
  selectSelectedAccountForm
} from 'pages/AccountManagement/_redux/accountList/selector';

// actions
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

// types
import { IAccounts } from 'pages/AccountManagement/types';

// constants
import { I18N_TEXT } from 'app/constants';

// helpers
import { isTouchedForm } from 'app/helpers';

export interface AccountFormProps {}
const AccountForm: React.FC<AccountFormProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const isOpen = useSelector<boolean>(selectAccountFormIsOpen);
  const selected = useSelector<IAccounts | undefined>(
    selectSelectedAccountForm
  );
  const editMode = !!selected?.id;

  const { formik } = useAccountForm();

  const {
    submitForm,
    resetForm,
    setSubmitting,
    isSubmitting,
    isValid,
    touched,
    dirty
  } = formik;

  const handleCancelForm = useCallback(() => {
    resetForm();
    setSubmitting(false);

    dispatch(
      accountManagementActions.onToggleAccountFormModal({
        selected: undefined
      })
    );

    dispatch(accountManagementActions.onSearchByAccReset());
  }, [dispatch, resetForm, setSubmitting]);

  const handleCancel = useCallback(() => {
    if (dirty && isTouchedForm(touched)) {
      dispatch(
        rootModalActions.open({
          title: I18N_TEXT.UNSAVED_CHANGE,
          body: I18N_TEXT.DISCARD_CHANGE_BODY,
          btnCancelText: I18N_TEXT.CONTINUE_EDITING,
          btnConfirmText: I18N_TEXT.DISCARD_CHANGE,
          onConfirm: handleCancelForm
        })
      );
      return;
    }

    handleCancelForm();
  }, [dirty, touched, handleCancelForm, dispatch]);

  const handleSubmit = useCallback(() => {
    setSubmitting(true);
    submitForm();
  }, [setSubmitting, submitForm]);

  return (
    <FormikProvider value={formik}>
      <Modal show={isOpen} loading={false} enforceFocus={false} full>
        <ModalHeader
          border
          closeButton
          className="border-bottom"
          onHide={handleCancel}
        >
          <ModalTitle className="text-capitalize">
            {editMode ? t(I18N_TEXT.EDIT_ACCOUNT) : t(I18N_TEXT.ADD_ACCOUNT)}
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <div className="max-width-lg mx-auto">
            <MainInformation />
            <div className="mt-24">
              <GeneralInformation />
            </div>
            <div className="mt-24">
              <AgencyInformation />
            </div>
            <div className="mt-24">
              <PaymentInformation />
            </div>
            <div className="mt-24">
              <FinancialInformation />
            </div>
            <div className="mt-24">
              <MiscellaneousInformation />
            </div>
          </div>
        </ModalBody>
        <ModalFooter
          border
          cancelButtonText={t(I18N_TEXT.CANCEL)}
          okButtonText={editMode ? t(I18N_TEXT.SAVE) : t(I18N_TEXT.SUBMIT)}
          onCancel={handleCancel}
          onOk={handleSubmit}
          disabledOk={
            !editMode && (isSubmitting || !(isTouchedForm(touched) && isValid))
          }
        ></ModalFooter>
      </Modal>
    </FormikProvider>
  );
};

export default AccountForm;
