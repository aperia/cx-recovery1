import React, { useCallback } from 'react';

// components
import { MaskedTextBoxControl, TextBoxControl } from 'app/components';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelector } from 'app/hooks';

// constants
import {
  ALPHA_NUMERIC_SPACE_REPLACE_REGEX,
  I18N_TEXT,
  MAX_LENGTH_15,
  MAX_LENGTH_16,
  MAX_LENGTH_20,
  MAX_LENGTH_40,
  NUMERIC_REPLACE_REGEX,
  PLACE_HOLDER_SSN,
  SSN_MASK_INPUT_REGEX
} from 'app/constants';

// types
import { IAccounts } from 'pages/AccountManagement/types';

// selectors
import { selectSelectedAccountForm } from 'pages/AccountManagement/_redux/accountList/selector';

// helpers
import { useFormikContext } from 'formik';
import { ACCOUNT_FIELD } from '../constants';

export interface MainInformationProps {}

const MainInformation: React.FC<MainInformationProps> = () => {
  const { t } = useTranslation();
  const { handleChange, setFieldValue } = useFormikContext();
  const selected = useSelector<IAccounts | undefined>(
    selectSelectedAccountForm
  );
  const editMode = !!selected?.id;

  const handleValueChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      // trigger change formik's value
      handleChange(event);

      // pre-filled value with account number value
      const { value: newValue } = event.target;

      setFieldValue(ACCOUNT_FIELD.ORIGINAL_LOAN_NUMBER.name, newValue, true);
      setFieldValue(
        ACCOUNT_FIELD.CREDIT_BUREAU_ACCOUNT_NUMBER.name,
        newValue,
        true
      );
    },
    [handleChange, setFieldValue]
  );

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.MAIN_INFORMATION)}</h5>
      </div>

      <TextBoxControl
        name="accountNumber"
        label={t(I18N_TEXT.ACCOUNT_NUMBER)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_16}
        regex={NUMERIC_REPLACE_REGEX}
        onValueChange={handleValueChange}
        required={!editMode}
        readOnly={editMode}
      />

      <TextBoxControl
        name="cardHolderName"
        label={t(I18N_TEXT.CARD_HOLDER_NAME)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_40}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
        required
      />

      <MaskedTextBoxControl
        name="ssn"
        label={t(I18N_TEXT.SSN)}
        className="col-6 col-lg-4 mt-16"
        mask={SSN_MASK_INPUT_REGEX}
        placeholder={PLACE_HOLDER_SSN}
        required={!editMode}
        readOnly={editMode}
      />

      <TextBoxControl
        name="originalLoanNbr"
        label={t(I18N_TEXT.ORIGINAL_LOAN_NUMBER)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_20}
        regex={NUMERIC_REPLACE_REGEX}
        required
      />

      <TextBoxControl
        name="creditBureauAccNbr"
        label={t(I18N_TEXT.CREDIT_BUREAU_ACCOUNT_NUMBER)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_20}
        regex={NUMERIC_REPLACE_REGEX}
        required
      />

      <FormikNumericControl
        name="originalLoanAmount"
        label={t(I18N_TEXT.ORIGINAL_LOAN_AMOUNT)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_15}
        format="c2"
        required
      />
    </div>
  );
};

export default React.memo(MainInformation);
