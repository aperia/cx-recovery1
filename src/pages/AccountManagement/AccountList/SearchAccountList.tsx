import React, { useEffect, useCallback } from 'react';

// components
import {
  AttributeSearchEvent,
  AttributeSearchValue
} from 'app/_libraries/_dls/components';

import SearchAccountListSearch from 'pages/__commons/AccountList/SearchAccounts';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { accountManagementActions } from '../_redux';
import { selectorIsSearch } from '../_redux/accountList/selector';

// hooks
import { isEmpty, isEqual } from 'app/helpers';
import { tabActions } from 'pages/__commons/TabBar/_redux';
import { I18N_TEXT } from 'app/constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
interface SearchAccountListProps {
  small?: boolean;
  screen?: 'Home' | 'AccountManagement';
}

let keepSearchValues: AttributeSearchValue[] = [];
let passSearchValues: AttributeSearchValue[] = [];

const SearchAccountList: React.FC<SearchAccountListProps> = ({
  small,
  screen = 'AccountManagement'
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const [searchValues, setSearchValues] = React.useState<
    AttributeSearchValue[]
  >([]);

  const originalSearch = useSelector(selectorIsSearch);

  useEffect(() => {
    if (!isEqual(originalSearch, keepSearchValues)) {
      setSearchValues(originalSearch);
      keepSearchValues = originalSearch;
    }

    if (originalSearch?.length && passSearchValues?.length) {
      setSearchValues(originalSearch);
      keepSearchValues = originalSearch;
      passSearchValues = [];
    }
  }, [originalSearch, searchValues, screen]);

  const handleChange = (event: AttributeSearchEvent) => {
    const { value } = event;
    setSearchValues(value);
  };

  const handleClear = () => setSearchValues([]);

  const handleOpenTabSearchResult = useCallback(
    (value: string) => {
      dispatch(
        tabActions.addTab({
          title: `${value} - ${t(I18N_TEXT.SEARCH_ACCOUNT_TO_VIEW)}`,
          storeId: 'search_result',
          tabType: 'searchResult',
          iconName: 'search-result'
        })
      );
    },
    [t, dispatch]
  );

  const handleSearch = (
    event: AttributeSearchEvent,
    error?: Record<string, string>
  ) => {
    if (!isEmpty(error) || !event.value.length) return;
    batch(() => {
      dispatch(accountManagementActions.onChangeSearchValue(event.value));
      keepSearchValues = event.value;
    });

    if (screen === 'Home') {
      let valueString = '';

      event.value.forEach(obj => {
        valueString = obj.value;
      });
      passSearchValues = event.value;

      handleOpenTabSearchResult(valueString);
    }
  };

  return (
    <SearchAccountListSearch
      small={small}
      searchValues={searchValues}
      handleClear={handleClear}
      handleChange={handleChange}
      handleSearch={handleSearch}
    />
  );
};
export default SearchAccountList;
