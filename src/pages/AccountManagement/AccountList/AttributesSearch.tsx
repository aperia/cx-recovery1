import React from 'react';
import {
  AttributeSearchData,
  InputControl,
  MaskedTextBoxControl
} from 'app/_libraries/_dls/components';
import {
  I18N_TEXT,
  MAX_LENGTH_16,
  MAX_LENGTH_20,
  MAX_LENGTH_40,
  PLACE_HOLDER_SSN,
  SSN_MASK_INPUT_REGEX
} from 'app/constants';

import { ATTRIBUTE_SEARCH_FIELD } from '../constant';

export const getInitialAttrData = (t: any) => {
  const debtorNameField: AttributeSearchData = {
    groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
    name: t(I18N_TEXT.DEBTOR_NAME),
    key: ATTRIBUTE_SEARCH_FIELD.DEBTOR_NAME,
    description: t(I18N_TEXT.DEBTOR_NAME_DESCRIPTION),
    component: props => (
      <InputControl
        placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
        maxLength={MAX_LENGTH_40}
        {...props}
      />
    )
  };
  const accountNumberField: AttributeSearchData = {
    groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
    name: t(I18N_TEXT.ACCOUNT_NUMBER),
    key: ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER,
    description: t(I18N_TEXT.ACCOUNT_NUMBER_DESCRIPTION),
    component: props => (
      <InputControl
        placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
        mode="number"
        maxLength={MAX_LENGTH_16}
        {...props}
      />
    )
  };
  const ssnField: AttributeSearchData = {
    groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
    name: t(I18N_TEXT.SSN),
    key: ATTRIBUTE_SEARCH_FIELD.SSN,
    description: t(I18N_TEXT.NINE_DIGITS),
    component: props => (
      <MaskedTextBoxControl
        placeholder={PLACE_HOLDER_SSN}
        mask={SSN_MASK_INPUT_REGEX}
        {...props}
      />
    )
  };
  const originalLoanNumberField: AttributeSearchData = {
    groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
    name: t(I18N_TEXT.ORIGINAL_LOAN_NUMBER),
    key: ATTRIBUTE_SEARCH_FIELD.ORIGINAL_LOAN_NUMBER,
    description: t(I18N_TEXT.ORIGINAL_LOAN_NUMBER_MUST_BE_20),
    component: props => (
      <InputControl
        mode="number"
        placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
        maxLength={MAX_LENGTH_20}
        {...props}
      />
    )
  };
  return {
    data: [
      debtorNameField,
      accountNumberField,
      ssnField,
      originalLoanNumberField
    ]
  };
};
