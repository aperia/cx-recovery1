import { I18N_TEXT } from 'app/constants';

export const ACCOUNT_FIELD = {
  // Main
  ACCOUNT_NUMBER: { name: 'accountNumber', label: I18N_TEXT.ACCOUNT_NUMBER },
  CARD_HOLDER_NAME: {
    name: 'cardHolderName',
    label: I18N_TEXT.CARD_HOLDER_NAME
  },
  SSN: { name: 'ssn', label: I18N_TEXT.SSN },
  ORIGINAL_LOAN_NUMBER: {
    name: 'originalLoanNbr',
    label: I18N_TEXT.ORIGINAL_LOAN_NUMBER
  },
  CREDIT_BUREAU_ACCOUNT_NUMBER: {
    name: 'creditBureauAccNbr',
    label: I18N_TEXT.CREDIT_BUREAU_ACCOUNT_NUMBER
  },
  ORIGINAL_LOAN_AMOUNT: {
    name: 'originalLoanAmount',
    label: I18N_TEXT.ORIGINAL_LOAN_AMOUNT
  },
  // General
  REASON_CODE: { name: 'reasonCode', label: I18N_TEXT.REASON_CODE },
  COLLECTOR_CODE: { name: 'collectorCode', label: I18N_TEXT.COLLECTOR_CODE },
  TYPE_CODE: { name: 'typeCode', label: I18N_TEXT.TYPE_CODE },
  PORTFOLIO_TYPE: { name: 'portfolioType', label: I18N_TEXT.PORTFOLIO_TYPE },
  STATUS_CODE: { name: 'statusCode', label: I18N_TEXT.STATUS_CODE },
  OPENED_DATE: { name: 'openedDate', label: I18N_TEXT.OPENED_DATE },
  DELINQUENCY_START_DATE: {
    name: 'delinquencyStartDate',
    label: I18N_TEXT.DELINQUENCY_START_DATE
  },
  LOAD_DATE: { name: 'loadDate', label: I18N_TEXT.LOAD_DATE },
  CHARGED_OFF_DATE: {
    name: 'chargedOffDate',
    label: I18N_TEXT.CHARGED_OFF_DATE
  },

  // Agency
  AGENCY_CODE: {
    name: 'agencyCode',
    label: I18N_TEXT.AGENCY_CODE
  },

  // Payment
  PAYMENT_SCHEDULE_AMOUNT: {
    name: 'paymentScheduleAmount',
    label: I18N_TEXT.PAYMENT_SCHEDULE_AMOUNT
  },
  PAYMENT_SCHEDULE_FREQUENCY: {
    name: 'paymentScheduleFrequency',
    label: I18N_TEXT.PAYMENT_SCHEDULE_FREQUENCY
  },
  PAYMENT_SCHEDULE_FREQUENCY_CUSTOM: {
    name: 'paymentScheduleFrequencyCustom',
    label: I18N_TEXT.COUNT
  },

  // Financial
  RESPONSIBILITY_CENTER: {
    name: 'responsibilityCenter',
    label: I18N_TEXT.RESPONSIBILITY_CENTER
  },
  CLASS_1: {
    name: 'class1',
    label: I18N_TEXT.CLASS_1
  },
  CLASS_2: {
    name: 'class2',
    label: I18N_TEXT.CLASS_2
  },
  CLASS_3: {
    name: 'class3',
    label: I18N_TEXT.CLASS_3
  },
  RESERVE_1: {
    name: 'reserve1',
    label: I18N_TEXT.RESERVE_1
  },
  RESERVE_1_POIC: {
    name: 'reserve1Poic',
    label: I18N_TEXT.RESERVE_1_POIC
  },
  RESERVE_1_AMOUNT: {
    name: 'reserve1Amount',
    label: I18N_TEXT.RESERVE_1_AMOUNT
  },
  RESERVE_2: {
    name: 'reserve2',
    label: I18N_TEXT.RESERVE_2
  },
  RESERVE_2_POIC: {
    name: 'reserve2Poic',
    label: I18N_TEXT.RESERVE_2_POIC
  },
  RESERVE_2_AMOUNT: {
    name: 'reserve2Amount',
    label: I18N_TEXT.RESERVE_2_AMOUNT
  },
  RESERVE_3: {
    name: 'reserve3',
    label: I18N_TEXT.RESERVE_3
  },
  RESERVE_3_POIC: {
    name: 'reserve3Poic',
    label: I18N_TEXT.RESERVE_3_POIC
  },
  RESERVE_3_AMOUNT: {
    name: 'reserve3Amount',
    label: I18N_TEXT.RESERVE_3_AMOUNT
  },
  OTHER: {
    name: 'other',
    label: I18N_TEXT.OTHER
  },
  OTHER_AMOUNT: {
    name: 'otherAmount',
    label: I18N_TEXT.OTHER_AMOUNT
  },
  INTEREST: {
    name: 'interest',
    label: I18N_TEXT.INTEREST
  },
  INTEREST_AMOUNT: {
    name: 'interestAmount',
    label: I18N_TEXT.INTEREST_AMOUNT
  },
  COST: {
    name: 'cost',
    label: I18N_TEXT.COST
  },
  COST_AMOUNT: {
    name: 'costAmount',
    label: I18N_TEXT.COST_AMOUNT
  },

  // Miscellaneous
  COMPLIANCE_CODE: {
    name: 'complianceCode',
    label: I18N_TEXT.COMPLIANCE_CODE
  },
  CREDIT_BUREAU: {
    name: 'creditBureau',
    label: I18N_TEXT.CREDIT_BUREAU
  }
};

export enum RecoveryIndicator {
  PRE_CHARGE_OFF_ACCOUNT = 'P',
  CHARGED_OFF_ACCOUNT = 'R'
}

export enum InterestRate {
  FIGURE = 'figure',
  CODE = 'code'
}
