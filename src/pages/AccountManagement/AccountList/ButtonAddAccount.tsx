import React, { useCallback } from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';

// redux
import { useDispatch } from 'react-redux';
import { accountManagementActions } from 'pages/AccountManagement/_redux';

// helpers
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { I18N_TEXT } from 'app/constants';

interface ButtonAddAccountProps {
  classNameProps?: string;
}

const ButtonAddAccount: React.FC<ButtonAddAccountProps> = ({
  classNameProps
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const handleShowAddForm = useCallback(() => {
    dispatch(
      accountManagementActions.onToggleAccountFormModal({
        selected: undefined
      })
    );
  }, [dispatch]);

  return (
    <div className={classNameProps}>
      <Button size="sm" variant="outline-primary" onClick={handleShowAddForm}>
        {t(I18N_TEXT.ADD_ACCOUNT)}
      </Button>
    </div>
  );
};
export default ButtonAddAccount;
