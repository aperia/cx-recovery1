import React, {
  Fragment,
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef
} from 'react';

// components
import { SimpleBar } from 'app/_libraries/_dls/components';
import SortAccountList from './Sort';
import Accounts from './Accounts';
import FailedApiReload from 'app/components/FailedApiReload';
import AccountForm from './Form';
import ButtonAddAccount from './ButtonAddAccount';
import { NoDataGrid, SearchNodata } from 'app/components';
import AccountGroupFormCreate from 'pages/AccountGroup/FormCreateAccountGroup';
import SearchAccountList from 'pages/AccountManagement/AccountList/SearchAccountList';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { accountManagementActions } from '../_redux';
import {
  selectorAccountList,
  selectorIsError,
  selectorIsLoading,
  selectorIsSearch,
  selectSnapAcc
} from '../_redux/accountList/selector';
import { selectorIsOpenModalLinkDebtors } from '../_redux/associatedDebtor/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { checkEmpty } from 'app/helpers';
import { I18N_TEXT } from 'app/constants';
import classNames from 'classnames';
import { AtScreenComponentAccounts } from '../types';
import { ManageAssociatedDebtors } from '../ManageAssociatedDebtors';

interface AccountListProps {
  atScreen: AtScreenComponentAccounts;
  title?: string;
}

const AccountList: React.FC<AccountListProps> = ({
  atScreen = 'accountMaintenance',
  title = I18N_TEXT.ACCOUNT_LIST
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const divRef = useRef<HTMLDivElement | null>(null);

  const accounts = useSelector(selectorAccountList);
  const originalSearch = useSelector(selectorIsSearch);
  const isError = useSelector(selectorIsError);
  const isLoading = useSelector(selectorIsLoading);

  const openLinkDebtors = useSelector(selectorIsOpenModalLinkDebtors);

  const snapAcc = useSelector(selectSnapAcc);

  const handleCloseModalLinkDebtors = useCallback(() => {
    // auto search by accNumber
    dispatch(
      accountManagementActions.triggerAutoSearchByAccNumber(
        snapAcc?.accNumber?.accountNumberUnMask || ''
      )
    );
    dispatch(accountManagementActions.onSetModalLinkDebtors(false));
  }, [dispatch, snapAcc]);

  const getAccounts = useCallback(() => {
    batch(() => {
      dispatch(accountManagementActions.getAccounts({ isLoadMore: false }));
    });
  }, [dispatch]);

  useEffect(() => {
    return () => {
      dispatch(accountManagementActions.removeAccountList());
    };
  }, [dispatch]);

  useEffect(() => {
    dispatch(accountManagementActions.onChangeAtScreen(atScreen));
  }, [dispatch, atScreen]);

  useLayoutEffect(() => {
    atScreen === 'accountMaintenance' && divRef.current && divRef.current.style
      ? (divRef.current.style.height = `calc(100vh - 193px)`)
      : (divRef.current!.style.height = '');
  }, [atScreen]);

  useEffect(() => {
    if (originalSearch.length) {
      getAccounts();
    }
  }, [getAccounts, originalSearch]);

  const onClearSearch = useCallback(() => {
    dispatch(accountManagementActions.onClearAndReset());
  }, [dispatch]);

  const renderAccounts = useMemo(() => {
    if (isError)
      return (
        <FailedApiReload id="failure-api-get-accounts" onReload={getAccounts} />
      );

    if (!checkEmpty([originalSearch, false, true]) && !isLoading) {
      return (
        <>
          {atScreen === 'accountMaintenance' && (
            <ButtonAddAccount classNameProps="text-right mt-16 mr-n8" />
          )}
          <NoDataGrid text={I18N_TEXT.SEARCH_ACCOUNT_TO_VIEW} />
        </>
      );
    }

    return (
      <Fragment>
        <div className="mt-16 d-flex justify-content-between align-items-center">
          <SortAccountList />
          {atScreen === 'accountMaintenance' && (
            <ButtonAddAccount classNameProps="mr-n8 ml-24" />
          )}
        </div>
        {accounts.length ? (
          <Accounts />
        ) : (
          <SearchNodata
            text={t(I18N_TEXT.ADJUST_YOUR_SEARCH_FILTER)}
            onClearSearch={onClearSearch}
          />
        )}
      </Fragment>
    );
  }, [
    atScreen,
    isError,
    getAccounts,
    originalSearch,
    isLoading,
    accounts.length,
    t,
    onClearSearch
  ]);

  const renderMainLayout = () => {
    return (
      <>
        <div className="header-account">
          <h5 className="header-account-title">{t(title)}</h5>
          <SearchAccountList />
        </div>
        {renderAccounts}
        <AccountForm />
        <ManageAssociatedDebtors
          open={openLinkDebtors}
          onHide={handleCloseModalLinkDebtors}
        />
        <AccountGroupFormCreate />
      </>
    );
  };

  return (
    <div ref={divRef}>
      {atScreen === 'accountMaintenance' ? (
        <SimpleBar>
          <div
            className={classNames(
              'px-24 pb-24 pt-lg-16 pt-md-24 max-width-lg mx-auto',
              {
                loading: isLoading
              }
            )}
          >
            {renderMainLayout()}
          </div>
        </SimpleBar>
      ) : (
        renderMainLayout()
      )}
    </div>
  );
};
export default AccountList;
