import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { ISaveAccountArgs } from '../_redux/accountList/saveAccount';
import { accountListController } from '../__mock__/accountListController';

const accountListServices = {
  getAccountList(
    searchValue: AttributeSearchValue[],
    startSequence: number,
    endSequence: number
  ) {
    return accountListController.getAccountList(
      searchValue,
      startSequence,
      endSequence
    );
  },
  saveAccount(args: ISaveAccountArgs) {
    return accountListController.saveAccount(args);
  }
};

export default accountListServices;
