import React, { useCallback } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// components
import { View } from 'app/_libraries/_dof/core';
import InfinityLoad from '../LoadMoreAccounts';
import { ManageAssociatedDebtors } from '../../ManageAssociatedDebtors';
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';
import MoreButton from 'pages/__commons/MoreButton';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useAccountGroup from 'pages/AccountGroup/hooks/useAccountGroup';

// redux
import { selectorAccountList } from '../../_redux/accountList/selector';
import { selectorIsOpenModalLinkDebtors } from 'pages/AccountManagement/_redux/associatedDebtor/selectors';
import { accountManagementActions } from 'pages/AccountManagement/_redux';

// helpers
import { IAccounts } from '../../types';
import { I18N_TEXT } from 'app/constants';
import { tabActions } from 'pages/__commons/TabBar/_redux';
import { getLast4SSN } from 'pages/AccountManagement/ManageAssociatedDebtors/helpers';
import { useManualForm } from 'pages/AccountManagement/ManualAssignRecall/hooks/useManualForm';
import { STORE_ACCOUNT_GROUP_KEY } from 'pages/AccountGroup/constants';
import { genMashAccNumber } from 'pages/AccountManagement/helpers';

export interface ItemAccountProps {}

const ItemAccount: React.FC<ItemAccountProps> = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const data: IAccounts[] = useSelector(selectorAccountList);
  const openLinkDebtors = useSelector(selectorIsOpenModalLinkDebtors);

  const { onOpenModal, onOpenModalCreateAccount } = useManualForm();
  const { showConfirmRemoveAccGroup } = useAccountGroup();

  const handleOpenAccountDetail = useCallback(
    (account: IAccounts, props?: MagicKeyValue) => {
      const { id: storeId, accountNumber, debtorName } = account;

      dispatch(
        tabActions.addTab({
          title: `${debtorName} - ${accountNumber}`,
          storeId,
          tabType: 'accountDetail',
          iconName: 'file',
          props: {
            ...props,
            storeId,
            debtorName,
            accountNumber
          }
        })
      );
    },
    [dispatch]
  );

  const handleOpenAccountGroup = useCallback(
    (account: IAccounts, props?: MagicKeyValue) => {
      const { accountNumber, groupId } = account;
      const storeId = `${STORE_ACCOUNT_GROUP_KEY}_${groupId}`;
      dispatch(
        tabActions.addTab({
          title: `${t(I18N_TEXT.ACCOUNT_GROUP_TITLE)} - ${accountNumber}`,
          storeId,
          tabType: 'accountGroup',
          iconName: 'file',
          props: {
            ...props,
            storeId,
            groupId
          }
        })
      );
    },
    [dispatch, t]
  );

  const handleCloseModalLinkDebtors = () => {
    dispatch(accountManagementActions.onSetModalLinkDebtors(false));
  };

  const handleShowEditForm = useCallback(
    (account: IAccounts) => {
      dispatch(
        accountManagementActions.onToggleAccountFormModal({
          selected: account
        })
      );
    },
    [dispatch]
  );

  const handleShowConfirmRemove = useCallback(
    (account: IAccounts) => {
      const { id: accountId, debtorName, groupId, accountNumber } = account;
      const storeId = `${STORE_ACCOUNT_GROUP_KEY}_${groupId}`;

      if (!groupId) return;

      showConfirmRemoveAccGroup({
        accountId,
        debtorName,
        groupId,
        accountNumber,
        storeId
      });
    },
    [showConfirmRemoveAccGroup]
  );

  const renderLayout = () => {
    return data.map((item, index) => (
      <div key={index} className="card br-radius-8 px-16 pb-16 pt-12 mt-16">
        <div className="mb-8 d-flex justify-content-between align-items-center">
          <p className="fw-500">
            {item.debtorName}
            {item?.numberOfGroup && item?.numberOfGroup > 0 ? (
              <Tooltip
                placement="top"
                variant="primary"
                triggerClassName="cursor-pointer ml-8"
                element={t(I18N_TEXT.VIEW_ACCOUNT_GROUP)}
              >
                <Button
                  size="sm"
                  variant="secondary"
                  className="btn-icon-text"
                  onClick={() => handleOpenAccountGroup(item)}
                >
                  <Icon name="account-group" />
                  <span className="ml-4">{item.numberOfGroup}</span>
                </Button>
              </Tooltip>
            ) : (
              <></>
            )}
          </p>
          <div className="d-flex">
            <Button
              className="text-capitalize"
              size="sm"
              variant="outline-primary"
              onClick={() => handleOpenAccountDetail(item)}
            >
              {t(I18N_TEXT.VIEW)}
            </Button>
            <Button
              className="text-capitalize"
              size="sm"
              variant="outline-primary"
              onClick={() => handleShowEditForm(item)}
            >
              {t(I18N_TEXT.EDIT)}
            </Button>
            <div className="ml-8 mr-n8">
              <MoreButton
                listBtn={[
                  {
                    label: I18N_TEXT.ADD_MANUAL_ASSIGN_RECALL_REQUESTS,
                    value: I18N_TEXT.ADD_MANUAL_ASSIGN_RECALL_REQUESTS,
                    event: () => onOpenModal(item.accNumber.accountNumberUnMask)
                  },
                  ...(!item.groupId
                    ? [
                        {
                          label: I18N_TEXT.CREATE_ACCOUNT_GROUP,
                          value: I18N_TEXT.CREATE_ACCOUNT_GROUP,
                          event: () => onOpenModalCreateAccount(item)
                        }
                      ]
                    : []),
                  {
                    label: I18N_TEXT.MANAGE_ASSOCIATED_DEBTORS,
                    value: I18N_TEXT.MANAGE_ASSOCIATED_DEBTORS,
                    event: () =>
                      handleOpenAccountDetail(item, {
                        defaultTabId: 'AssociatedDebtors'
                      })
                  },
                  {
                    label: I18N_TEXT.MANAGE_BANKRUPTCY_DECEASE_RECORD,
                    value: I18N_TEXT.MANAGE_BANKRUPTCY_DECEASE_RECORD,
                    event: () =>
                      handleOpenAccountDetail(item, {
                        defaultTabId: 'bankruptcyDeceaseRecord'
                      })
                  },
                  {
                    label: I18N_TEXT.MANAGE_FINANCIAL_INFORMATION,
                    value: I18N_TEXT.MANAGE_FINANCIAL_INFORMATION,
                    event: () =>
                      handleOpenAccountDetail(item, {
                        defaultTabId: 'financialInformation'
                      })
                  },
                  {
                    label: I18N_TEXT.MANAGE_SUIT_INFORMATION,
                    value: I18N_TEXT.MANAGE_SUIT_INFORMATION,
                    event: () =>
                      handleOpenAccountDetail(item, {
                        defaultTabId: 'suitInformation'
                      })
                  },
                  ...(item.groupId
                    ? [
                        {
                          label: I18N_TEXT.REMOVE_ACCOUNT_FROM_GROUP,
                          value: I18N_TEXT.REMOVE_ACCOUNT_FROM_GROUP,
                          event: () => handleShowConfirmRemove(item)
                        }
                      ]
                    : [])
                ]}
              />
            </div>
          </div>
        </div>
        <View
          id={`accountAccountList-${index}`}
          formKey={`accountAccountList-${index}`}
          descriptor="accountAccountList"
          value={{
            ...item,
            accountNumber: genMashAccNumber(item.accNumber.accountNumberUnMask),
            accNumber: {
              ...item.accNumber,
              accountNumber: genMashAccNumber(
                item.accNumber.accountNumberUnMask
              )
            },
            originalLoan: {
              ...item.originalLoan,
              accountNumber: genMashAccNumber(
                item.originalLoan.accountNumberUnMask
              )
            },
            originalLoanNbr: item.originalLoan,
            last4SSN: getLast4SSN(item.ssn)
          }}
        />
      </div>
    ));
  };

  return (
    <div className="mt-16">
      {renderLayout()}
      <ManageAssociatedDebtors
        open={openLinkDebtors}
        onHide={handleCloseModalLinkDebtors}
      />
      <InfinityLoad />
    </div>
  );
};

export default ItemAccount;
