import React from 'react';

// components
import ItemAccount from './itemAccount';
import FoundAccount from './FoundAccount';
import InfinityLoad from '../LoadMoreAccounts';

// helpers
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { I18N_TEXT } from 'app/constants';
import { useSelector } from 'react-redux';
import {
  selectorAccountList,
  selectorAtScreen
} from '../../_redux/accountList/selector';
import { AtScreenComponentAccounts, IAccounts } from '../../types';
import ClearAndResetAccounts from './ClearAndReset';

export interface ListAccountProps {}

const Accounts: React.FC<ListAccountProps> = () => {
  const { t } = useTranslation();
  const data: IAccounts[] = useSelector(selectorAccountList);
  const atScreen: AtScreenComponentAccounts = useSelector(selectorAtScreen);

  const renderLayout = () => {
    if (atScreen === 'accountMaintenance') return <ItemAccount />;
    return <FoundAccount />;
  };

  return (
    <div className="mt-16">
      <div className="d-flex justify-content-between align-items-center mr-n8">
        <p>{t(I18N_TEXT.RESULT_FOUNDED, { count: data.length })}</p>
        <ClearAndResetAccounts />
      </div>
      {renderLayout()}
      <InfinityLoad />
    </div>
  );
};

export default Accounts;
