import React, { useCallback } from 'react';

// component
import ClearAndReset from 'pages/__commons/AccountList/ClearAndReset';

// redux
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { useDispatch } from 'react-redux';

interface ClearAndResetAccountsProps {}

const ClearAndResetAccounts: React.FC<ClearAndResetAccountsProps> = () => {
  const dispatch = useDispatch();

  const onClick = useCallback(() => {
    dispatch(accountManagementActions.onClearAndReset());
  }, [dispatch]);

  return <ClearAndReset onClick={onClick} />;
};
export default ClearAndResetAccounts;
