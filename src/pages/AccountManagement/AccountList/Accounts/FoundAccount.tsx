import React from 'react';

// components
import { View } from 'app/_libraries/_dof/core';

// helpers
import { useDispatch, useSelector } from 'react-redux';
import {
  selectorAccountList,
  selectorSelectedAccount
} from '../../_redux/accountList/selector';
import { IAccounts } from '../../types';
import { Radio } from 'app/_libraries/_dls/components';
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import classNames from 'classnames';
import { genMashAccNumber } from 'pages/AccountManagement/helpers';

export interface FoundAccountProps {}

const FoundAccount: React.FC<FoundAccountProps> = () => {
  const data: IAccounts[] = useSelector(selectorAccountList);
  const dispatch = useDispatch();
  const selectedAccount = useSelector(selectorSelectedAccount);

  const handleChangeRadio = (item: IAccounts) => {
    dispatch(accountManagementActions.onChangeSelectorAccount(item));
  };

  const renderLayout = () => {
    return data.map((item, index) => {
      const checked =
        item.accountNumber === selectedAccount.accountNumber ? true : false;
      return (
        <div
          key={index}
          onClick={() => handleChangeRadio(item)}
          className={classNames(
            'card card-blue pt-12 mt-16',
            checked && 'active'
          )}
        >
          <div className="d-flex">
            <div className="pl-8 pr-16">
              <Radio id={`radio-${index}`}>
                <Radio.Input
                  checked={checked}
                  onChange={() => handleChangeRadio(item)}
                />
              </Radio>
            </div>
            <div className="flex-1">
              <p className="fw-500 mb-8">{item.debtorName}</p>
              <View
                id={`accountAccountList-${index}`}
                formKey={`accountAccountList-${index}`}
                descriptor="accountAccountList"
                value={{
                  ...item,
                  accountNumber: genMashAccNumber(
                    item.accNumber.accountNumberUnMask
                  ),
                  accNumber: {
                    ...item.accNumber,
                    accountNumber: genMashAccNumber(
                      item.accNumber.accountNumberUnMask
                    )
                  }
                }}
              />
            </div>
          </div>
        </div>
      );
    });
  };

  return <>{renderLayout()}</>;
};

export default FoundAccount;
