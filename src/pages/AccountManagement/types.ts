import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { ordersDropdown } from 'app/constants';
import { dropdownSortAccounts } from 'app/constants';
import { dropDownTypeLetterCode } from './Correspondence/CodeListTable/constants';
import { DEFAULT_PAGE_SIZE } from 'app/_libraries/_dls/components/Pagination/constants';

export declare type AtScreenComponentAccounts =
  | 'accountMaintenance'
  | 'findAccount';

export declare type TypeStatusAccountList = 'active' | 'purged';

export declare type AccountManagementTabID =
  | 'debtorMaintenance'
  | 'accountMaintenance'
  | 'assignRecallList'
  | 'pendingCorrespondence';

export const sortByDefault = dropdownSortAccounts[1];

export const orderByDefault = ordersDropdown[0];

export const statusDefault = 'active';
export interface AccountManagementTabs {
  id: AccountManagementTabID;
  title: string;
}

export interface AccountManagementContentTabs {
  id: AccountManagementTabID;
  Component: any;
}

export const AccountListInitialState: AccountListReducer = {
  sortBy: sortByDefault,
  orderBy: orderByDefault,
  status: statusDefault,
  isLoading: false,
  isLoadingLoadMore: false,
  isEnd: false,
  isError: false,
  startSequence: 0,
  endSequence: 25,
  searchValue: [],
  atScreen: 'accountMaintenance',
  data: [],
  selectedAccount: {} as IAccounts,
  accountForm: {
    isOpen: false,
    isLoading: false,
    isError: false,
    selected: undefined
  },
  snapAcc: {} as IAccounts
};

export interface Debtors extends MagicKeyValue {
  id: string;
  debtorName: string;
  ssn: string;
  addressLineOne: string;
  addressLineTwo: string;
  zipCode: string;
  state: string;
  dob: string;
  maritalStatus: string;
  homePhone: string;
  mobilePhone: string;
  county: string;
  addressFlag: string;
  piid: string;
}

export const DebtorMaintenanceInitialState: DebtorMaintenance = {
  debtorList: {
    data: [],
    isLoading: false,
    sortBy: {} as ISortType,
    isError: false,
    valueSearch: ''
  },
  viewDetailOpen: false,
  addressHistory: [],
  debtorForm: {
    isOpen: false,
    isLoading: false,
    isError: false,
    selected: undefined
  }
};

export const ManualListInitialState: ManualListReducer = {
  sortBy: sortByDefault,
  orderBy: orderByDefault,
  status: statusDefault,
  isLoading: false,
  isLoadingLoadMore: false,
  isEnd: false,
  isError: false,
  startSequence: 0,
  endSequence: 25,
  data: [],
  isOpenModal: false
};
export interface PayloadGetAccounts {
  data: IAccounts[];
}
export interface IAccounts {
  id: string;
  debtorName: string;
  accountNumber: string;
  accNumber: {
    accountNumber: string;
    accountNumberUnMask: string;
  };
  ssn: string;
  originalLoanNbr: string;
  originalLoan: {
    accountNumber: string;
    accountNumberUnMask: string;
  };
  status: string;
  agencyCode: string;
  feeCode: string;
  lastRunDate: string;
  lastActivityDate: string;
  reasonCode: string;
  collectorCode: string;
  statusCode: string;
  type: string;
  credit: string;
  groupId?: string;
  numberOfGroup?: number;
  accountType: string;
  accountClass: string;
  paymentSchedule: string;
  paymentScheduleFrequency: string;
  paymentScheduleStartDate: string;
  paymentAmount: string;
  statusCodeDate: string;
  dateOfMostRecentMemo: string;
  lastPaymentDate: string;
  dateAccountOpened: string;
  lowestFinancialHierachyReportingLevel: string;
  collectorAssignedCode: string;
  agencyAssignmentDate: string;
  masterCalendarCode: string;
  masterCalendarDate: string;
  agencyFees: string;
  accountBalance: string;
}

export interface IManuals extends IChangeManuals {
  id: string;
  debtorName: string;
  accountNumber: string;
  accNumber: {
    accountNumber: string;
    accountNumberUnMask: string;
  };
  ssn: string;
  originalLoanNbr: string;
  originalLoan: {
    accountNumber: string;
    accountNumberUnMask: string;
  };
  status: string;
  lastActivityDate: string;
  type: string;
  credit: string;
}

export interface IChangeManuals {
  agencyCode: string;
  lastRunDate: string;
  feeCode: string;
  collectorCode: string;
  reasonCode: string;
  statusCode: string;
  credit: string;
  newAgencyCode: string;
  nextRunDate: string;
  newFeeCode: string;
  newCollectorCode: string;
  newReasonCode: string;
  newStatusCode: string;
  newCredit: string;
}

export interface CommonTypeAccountManagement {
  activeTab: AccountManagementTabID;
  accSearchValue: IAccounts;
}

export interface ICommonDebtorForm {
  className?: string;
}

export interface DebtorList {
  data: Debtors[];
  isLoading: boolean;
  sortBy: ISortType;
  isError: boolean;
  valueSearch: string;
}
export interface IDebtorFormReducer {
  isOpen: boolean;
  isLoading: boolean;
  isError: boolean;
  selected?: Debtors;
}

export interface DebtorMaintenance {
  debtorList: DebtorList;
  viewDetailOpen: boolean;
  selectedDebtor?: Debtors;
  addressHistory: IAddressHistory[];
  debtorForm: IDebtorFormReducer;
}

export interface InitialStateAccountManagement {
  common: CommonTypeAccountManagement;
  accountList: AccountListReducer;
  manualAssignRecall: ManualListReducer;
  debtorMaintenance: DebtorMaintenance;
  manageAssociatedDebtors: AssociatedDebtorReducer;
  correspondence: CorrespondenceReducer;
}

export interface IAccountFormReducer {
  isOpen: boolean;
  isLoading: boolean;
  isError: boolean;
  selected?: IAccounts;
}
export interface AccountListReducer {
  sortBy: RefDataValue;
  orderBy: RefDataValue;
  status: TypeStatusAccountList;
  isLoading: boolean;
  isError: boolean;
  startSequence: number;
  endSequence: number;
  isLoadingLoadMore: boolean;
  isEnd: boolean;
  searchValue: AttributeSearchValue[];
  data: IAccounts[];
  selectedAccount: IAccounts;
  atScreen: AtScreenComponentAccounts;
  accountForm: IAccountFormReducer;
  snapAcc: IAccounts;
}

export interface ManualListReducer {
  sortBy: RefDataValue;
  orderBy: RefDataValue;
  status: TypeStatusAccountList;
  isLoading: boolean;
  isError: boolean;
  isOpenModal: boolean;
  startSequence: number;
  endSequence: number;
  isLoadingLoadMore: boolean;
  isEnd: boolean;
  data: IManuals[];
}

export type FieldType<T> = {
  value?: T;
};

export type AttrCommonModel = FieldType<string>;

export interface AttributeSearchFieldModel {
  debtorName?: AttrCommonModel;
  accountNumber?: AttrCommonModel;
  ssn?: AttrCommonModel;
  originalLoanNumber?: AttrCommonModel;
  agencyCode?: AttrCommonModel;
  calendarCode?: AttrCommonModel;
  reasonCode?: AttrCommonModel;
}

export interface ManualAssignRecallPayload {
  data: IManuals[];
}

export interface ManualAssignRecallArgs {
  accNumber: string;
  accountData: IChangeManuals;
}

export interface SearchAccountByAccArgs {
  accNumber: string;
}

export interface SearchAccountByAccPayload {
  data: IAccounts;
}

export interface AddManualAssignRecallPayload {
  data: IManuals[];
}

export interface ManualRecallTypeData {
  value: boolean;
  label: string;
}

export enum ListManualRecallType {
  ASSIGN = 'assign',
  RECALL = 'recall'
}

export interface IAddressHistory {
  id: string;
  date: string;
  time: string;
  address: string;
  city: string;
  state: string;
  zipCode: string;
  county: string;
  mobileNumber: string;
  homePhone: string;
  workPhone: string;
}

export interface IGetAddressHistoryPayload {
  data: IAddressHistory[];
}
export interface ILinkDebtor {
  id?: string;
  debtorName: string;
  last4SSN: string;
  relation: string;
  ecoa: string;
  sendCorrespondence: string;
  piid: string;
  ciicd: string;
  isBankruptcy: boolean;
  isDecease: boolean;
}

export type FormAction = 'ADD' | 'EDIT';

export const DEFAULT_LINK_DEBTOR: ILinkDebtor = {
  debtorName: '',
  relation: '',
  last4SSN: '',
  ecoa: '',
  sendCorrespondence: '',
  piid: '',
  ciicd: '',
  isBankruptcy: false,
  isDecease: false
};

export interface AssociatedDebtorReducer {
  sortBy: RefDataValue;
  orderBy: RefDataValue;
  status: TypeStatusAccountList;
  isLoading: boolean;
  isError: boolean;
  isOpenModal: boolean;
  startSequence: number;
  endSequence: number;
  isLoadingLoadMore: boolean;
  isEnd: boolean;
  data: ILinkDebtor[];
  accSearchValue: IAccounts;

  formOpen: boolean;
  formValues: ILinkDebtor;
  formAction: FormAction;
  totalItem: number;
  currentPage: number;
  linkAnotherDebtor: boolean;
  allAssociatedsDebtors: ILinkDebtor[];
}

export const AssociatedDebtorsInitialState: AssociatedDebtorReducer = {
  sortBy: sortByDefault,
  orderBy: orderByDefault,
  status: statusDefault,
  isLoading: false,
  isLoadingLoadMore: false,
  isEnd: false,
  isError: false,
  startSequence: 0,
  endSequence: 25,
  data: [],
  accSearchValue: {} as IAccounts,
  isOpenModal: false,

  formOpen: false,
  formValues: DEFAULT_LINK_DEBTOR,
  formAction: 'ADD',
  totalItem: 0,
  currentPage: 1,
  linkAnotherDebtor: false,
  allAssociatedsDebtors: []
};

export interface IDeleteDebtorArgs {
  id: string;
}

export enum AssociatedAccountType {
  LINK = 'link',
  UNLINK = 'unlink'
}

// Correspondence
export declare type letterVariableType = 'Yes' | 'No';
export enum SendCorrespondenceType {
  B = 'B',
  N = 'N',
  Y = 'Y'
}

export enum LetterType {
  GROUP_LETTER = 'A',
  SINGLE_LETTER = 'L',
  SERIES_LETTER = 'S'
}
export interface ILetterCode {
  id?: string;
  type: string;
  letterCode: string;
  description: string;
  sequenceNumber?: number;
  letterVariables?: letterVariableType;
}

export interface CodeList {
  isLoading: boolean;
  isError: boolean;
  type: RefDataValue;
  sortBy: ISortType;
  searchValue: string;
  totalItem: number;
  currentPage: number;
  pageSize: number;
  data: ILetterCode[];
}

export interface IPayloadGetCodeListReturn {
  data: {
    data: ILetterCode[];
    totalItem: number;
    currentPage: number;
    pageSize: number;
  };
}

// pending correspondence
export interface IReceiverDebtor {
  id?: string;
  debtorName: string;
  relation: string;
}

export interface IPendingCorrespondence {
  id: string;
  code: string;
  description: string;
  type: string;
  sendDate: string | Date;
  accountNumber: string;
  accNumber: {
    accountNumber: string;
    accountNumberUnMask: string;
  };
  receiver: IReceiverDebtor[];
  letterVariables?: letterVariableType;
}

export interface PendingCorrespondenceList {
  sortBy: RefDataValue;
  orderBy: RefDataValue;
  isLoading: boolean;
  isError: boolean;
  searchValue: AttributeSearchValue[];
  totalItem: number;
  currentPage: number;
  pageSize: number;
  data: IPendingCorrespondence[];
}

export interface SendCorrespondence {
  collapse: boolean;
  itemCode?: ILetterCode;
  debtorListSelected: ILinkDebtor[];
}
export interface IPayloadGetPendingCorrespondenceReturn {
  data: {
    data: IPendingCorrespondence[];
    totalItem: number;
    currentPage: number;
    pageSize: number;
  };
}

export enum CORRESPONDENCE_TAB {
  LETTER_CODE_LIST = 'LETTER_CODE_LIST',
  PENDING_CORRESPONDENCE = 'PENDING_CORRESPONDENCE'
}

export const codeListDefault: CodeList = {
  isLoading: false,
  isError: false,
  type: dropDownTypeLetterCode[1],
  sortBy: {
    id: 'letterCode',
    order: 'asc'
  },
  searchValue: '',
  totalItem: 0,
  currentPage: 1,
  pageSize: DEFAULT_PAGE_SIZE,
  data: []
};

export const sendCorrespondenceDefault: SendCorrespondence = {
  collapse: true,
  itemCode: undefined,
  debtorListSelected: []
};

export const pendingCorrespondenceDefault: PendingCorrespondenceList = {
  isLoading: false,
  isError: false,
  sortBy: { value: 'sendDate', description: 'sendDate' },
  orderBy: ordersDropdown[1],
  searchValue: [],
  totalItem: 0,
  currentPage: 1,
  pageSize: DEFAULT_PAGE_SIZE,
  data: []
};

export interface IPayloadPendingCorrespondence {
  sortBy?: string;
  orderBy?: string;
  searchValue: AttributeSearchValue[];
  pageSize: number;
  page: number;
}

export interface IPayloadCorrespondence {
  sortBy?: ISortType;
  type: string;
  searchValue: string;
  pageSize: number;
  page: number;
}

export interface CorrespondenceReducer {
  modal: {
    isOpenModal: boolean;
    isDiscardForm: boolean;
    currentTab: CORRESPONDENCE_TAB;
    codeList: CodeList;
    pendingCorrespondence: PendingCorrespondenceList;
    sendCorrespondence: SendCorrespondence;
  };
  pendingCorrespondence: PendingCorrespondenceList;
}

export const CorrespondenceInitialState: CorrespondenceReducer = {
  modal: {
    isOpenModal: false,
    isDiscardForm: false,
    currentTab: CORRESPONDENCE_TAB.LETTER_CODE_LIST,
    codeList: codeListDefault,
    pendingCorrespondence: pendingCorrespondenceDefault,
    sendCorrespondence: sendCorrespondenceDefault
  },
  pendingCorrespondence: pendingCorrespondenceDefault
};

export interface IChangeFlyoutSendCorrespondence {
  collapse: boolean;
  item?: ILetterCode;
}

export interface SendCorrespondenceRequest {
  sendDate: string;
  lastPaymentAmount?: string | number
}

export type RequestValuesType = SendCorrespondenceRequest | MagicKeyValue;

export interface IPayloadSendCorrespondence {
  id?: string;
  accountNumber: string;
  accNumber: {
    accountNumber: string;
    accountNumberUnMask: string;
  };
  code: string;
  description: string;
  type: string;
  sendDate: string | Date;
  receiver: IReceiverDebtor[];
  lastPaymentAmount?: string | number
}

export interface AttrSearchPendingCorrespondenceModel {
  accountNumber?: AttrCommonModel;
  letterCode?: AttrCommonModel;
}
