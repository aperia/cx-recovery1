import { IChangeManuals } from 'pages/AccountManagement/types';
import { accountController } from './__mock__/accountControll';

const accountServices = {
  getManualAssignRecall() {
    return accountController.getManualAssignRecall();
  },
  addManualAssignRecall({
    accNumber,
    accountData
  }: {
    accNumber: string;
    accountData: IChangeManuals;
  }) {
    return accountController.addManualAssignRecall({
      accNumber,
      accountData
    });
  },
  searchAccountByAcc({ accNumber }: { accNumber: string }) {
    return accountController.searchAccountByAcc({ accNumber });
  }
};

export default accountServices;
