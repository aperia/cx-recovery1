import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// types
import { Debtors } from '../types';

// helper
import { I18N_TEXT } from 'app/constants';
import If from 'pages/__commons/If';

export interface DebtorConfirmBodyProps {
  data?: Debtors;
}

const DebtorConfirmBody: React.FC<DebtorConfirmBodyProps> = ({ data }) => {
  const { t } = useTranslation();

  return (
    <If condition={!!data}>
      <>
        <p>{t(I18N_TEXT.DELETE_DEBTOR_CONFIRM_BODY)}</p>
        <p className="mt-16">
          Debtor: <strong>{data?.debtorName}</strong>
        </p>
      </>
    </If>
  );
};

export default DebtorConfirmBody;
