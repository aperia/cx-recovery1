import React, { Fragment } from 'react';
import { I18N_TEXT } from 'app/constants';
import { Debtors } from '../types';
import ActionsGridDebtor from './actions';
import { getLast4SSN } from '../ManageAssociatedDebtors/helpers';

export const sortDebtorsDefault: ISortType = {
  id: 'debtorName',
  order: 'asc'
};
export const ACCOUNT_INFO_VIEW = 'debtorDetailAccountInformation';
export const EMPLOYER_INFO_VIEW = 'debtorDetailEmployerInformation';
export const GET_ADDRESS_HISTORY = 'debtorMaintenance/getAddressHistory';

export const columnDebtorMaintenance = (t: any) => {
  return [
    {
      id: 'debtorName',
      Header: t(I18N_TEXT.DEBTOR_NAME),
      isSort: true,
      accessor: 'debtorName',
      width: 200
    },
    {
      id: 'Last4SSN',
      Header: t(I18N_TEXT.LAST_4_SSN),
      accessor: (item: Debtors) => getLast4SSN(item?.ssn),
      width: 200,
      isSort: true
    },
    {
      id: 'address',
      Header: t(I18N_TEXT.ADDRESS),
      accessor: (item: Debtors) => (
        <Fragment>
          {`
            ${item?.addressLineOne && `${item?.addressLineOne},`}
            ${item?.addressLineTwo && `${item?.addressLineTwo},`}
            ${item.state} ${item.zipCode} 
          `}
        </Fragment>
      ),
      isSort: true,
      width: 400,
      autoWidth: true
    },
    {
      id: 'action',
      Header: t(I18N_TEXT.ACTIONS),
      accessor: (item: Debtors) => <ActionsGridDebtor debtor={item} />,
      width: 190,
      isFixedRight: true,
      cellProps: { className: 'text-center' },
      cellBodyProps: { className: 'text-center' },
      className: 'td-sm'
    }
  ];
};

export const LINKED_DEBTORS: RefDataValue[] = [
  {
    value: 'Amelia Jackie',
    description: 'Primary Debtor'
  },
  {
    value: 'Ashley Thompson',
    description: 'Legal Relationship'
  },
  {
    value: 'John Halvin',
    description: 'Co-Maker'
  }
];

export const EMPLOYER_DATA: MagicKeyValue = {
  name: 'Anna Taylor',
  workPhone: '(932) 499-3333',
  miscellaneousHomeEmail: 'anna.taylor@gmail.com',
  address: '3342 Us Hwy, Jacksonville\nWA 43245',
  comment: 'Only available from 4 PM weekdays, please don’t call over weekends'
};

export const DEBTOR_FIELD = {
  DEBTOR_NAME: { name: 'debtorName', label: I18N_TEXT.NAME },
  DEBTOR_SSN: {
    name: 'ssn',
    label: I18N_TEXT.SSN
  },
  MISCELLANEOUS_HOME_EMAIL: {
    name: 'eMiscellaneous',
    label: I18N_TEXT.MISCELLANEOUS_HOME_EMAIL
  },
  RELATION: { name: 'relation', label: I18N_TEXT.RELATION },
  ECOA: { name: 'ecoa', label: I18N_TEXT.ECOA },
  SEND_CORRESPONDENCE: {
    name: 'sendCorrespondence',
    label: I18N_TEXT.SEND_CORRESPONDENCE
  },
  ASSOCIATED_ACCOUNT_TYPE: {
    name: 'associatedAccountType',
    label: I18N_TEXT.ASSOCIATED_ACCOUNT
  }
};

export enum RELATION {
  PRIMARY_DEBTOR = 'A',
  LEGAL_RELATIONSHIP = 'L',
  CO_MAKER = 'S',
  AUTHORIZED_USER = 'U'
}
