import { checkEmpty, orderBy } from 'app/helpers';
import { Debtors } from '../types';
import { sortDebtorsDefault } from './constants';

export const sortDebtors = (
  data: Debtors[],
  sortParam: ISortType
): Debtors[] => {
  const sortBy = checkEmpty([sortParam, sortDebtorsDefault, sortParam]);

  return orderBy(
    data,
    (item: Debtors) => {
      if (sortBy?.id === 'address')
        return (
          item?.addressLineOne &&
          item?.addressLineTwo &&
          item.zipCode &&
          item.state
        );
      return item[sortBy!.id];
    },
    sortBy!.order || 'asc'
  );
};

export const searchDebtors = (
  data: Debtors[],
  valueSearch: string
): Debtors[] => {
  if (!valueSearch || !data.length) return data;

  return data.filter(
    item =>
      item.debtorName
        ?.toLowerCase()
        .includes(valueSearch.toLocaleLowerCase()) ||
      item.addressLineOne
        ?.toLowerCase()
        .includes(valueSearch.toLocaleLowerCase()) ||
      item.addressLineTwo
        ?.toLowerCase()
        .includes(valueSearch.toLocaleLowerCase()) ||
      item.ssn?.toLowerCase().includes(valueSearch.toLocaleLowerCase()) ||
      item.zipCode?.toLowerCase().includes(valueSearch.toLocaleLowerCase()) ||
      item.state?.toLowerCase().includes(valueSearch.toLocaleLowerCase())
  );
};
