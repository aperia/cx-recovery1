import { RELATION } from './../constants';
import { useDispatch } from 'react-redux';

// hooks
import useValidation from './useValidation';
import { useSelector } from 'app/hooks';

import { accountManagementActions } from 'pages/AccountManagement/_redux';

// selectors
import { selectSelectedDebtor } from 'pages/AccountManagement/_redux/debtorMaintenance/selectors';

// types
import { AssociatedAccountType, Debtors } from 'pages/AccountManagement/types';

// helpers
import { useFormik } from 'formik';

const useDebtorForm = () => {
  const dispatch = useDispatch();
  const { validationSchema } = useValidation();
  const selected = useSelector<Debtors | undefined>(selectSelectedDebtor);
  const selectedId = selected?.id;

  const defaultValues = {
    associatedAccountType: AssociatedAccountType.LINK
  };

  const mockDebtor = {
    debtorName: selected ? selected.debtorName : 'Smith jackie',
    ssn: '543-13-4567',
    salutation: '123',
    dateOfBirth: '10/12/2021',
    maritalStatus: 'D',
    mobilePhone: '0123456789',
    homePhone: '0123456789',
    addressLine1: '4180 Us Hwy',
    addressLine2: 'Jacksonville',
    debtorCity: 'Maricopa',
    debtorState: 'AA',
    debtorZipCode: '0123456789',
    country: 'Maricopa',
    addressFlag: 'B',

    //Employer
    eName: 'Smith Love',
    ePhone: '0123456789',
    eMiscellaneous: 'info@gmail.com',
    eAddress: '4180 Us Hwy',
    eCity: 'Maricopa',
    eState: 'AL',
    eZipCode: '1234567899',
    eShortField1: '',
    eShortField2: '',
    eDateField1: '10/12/2021',
    eDateField2: '10/12/2021',
    eLongField1: '',
    eLongField2: '',
    eComment: 'This is a comment',

    //Associated Account
    accNumber: {
      accountNumber: '022002••••••1490',
      accountNumberUnMask: '0220022004341490'
    },
    legalRelationship: 'Ashley Thompson',
    coMaker: 'John Halvin',
    authorizedUsers: 'Derrick Grant, Dane Horn, Cassie Lord',
    relation: RELATION.LEGAL_RELATIONSHIP,
    ecoa: '1',
    sendCorrespondence: 'B'
  };

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      ...defaultValues,
      ...(selected ? { ...mockDebtor } : {})
    },
    onSubmit: (values, formikBag) => {
      dispatch(
        accountManagementActions.saveDebtor({
          id: selectedId,
          values,
          formikBag
        })
      );
    },
    validationSchema
  });

  return { formik };
};

export default useDebtorForm;
