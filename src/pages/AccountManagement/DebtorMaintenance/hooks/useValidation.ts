import { AssociatedAccountType } from 'pages/AccountManagement/types';
// helpers
import * as Yup from 'yup';
import { I18N_TEXT } from 'app/constants';
import { DEBTOR_FIELD } from '../constants';

const useValidation = () => {
  const validationSchema = Yup.object().shape({
    [DEBTOR_FIELD.DEBTOR_NAME.name]: Yup.string().required(
      I18N_TEXT.DEBTOR_NAME_REQUIRED
    ),
    [DEBTOR_FIELD.DEBTOR_SSN.name]: Yup.string().required(
      I18N_TEXT.DEBTOR_SSN_REQUIRED
    ),
    [DEBTOR_FIELD.MISCELLANEOUS_HOME_EMAIL.name]: Yup.string().email(
      I18N_TEXT.INVALID_FORMAT
    ),
    [DEBTOR_FIELD.ASSOCIATED_ACCOUNT_TYPE.name]:
      Yup.mixed<AssociatedAccountType>().oneOf(
        Object.values(AssociatedAccountType)
      ),
    [DEBTOR_FIELD.RELATION.name]: Yup.string().when(
      [DEBTOR_FIELD.ASSOCIATED_ACCOUNT_TYPE.name],
      {
        is: AssociatedAccountType.LINK,
        then: Yup.string().required(I18N_TEXT.RELATION_IS_REQUIRED)
      }
    ),
    [DEBTOR_FIELD.ECOA.name]: Yup.string().when(
      [DEBTOR_FIELD.ASSOCIATED_ACCOUNT_TYPE.name],
      {
        is: AssociatedAccountType.LINK,
        then: Yup.string().required(I18N_TEXT.ECOA_IS_REQUIRED)
      }
    ),
    [DEBTOR_FIELD.SEND_CORRESPONDENCE.name]: Yup.string().when(
      [DEBTOR_FIELD.ASSOCIATED_ACCOUNT_TYPE.name],
      {
        is: AssociatedAccountType.LINK,
        then: Yup.string().required(I18N_TEXT.SEND_CORRESPONDENCE_IS_REQUIRED)
      }
    )
  });

  return {
    validationSchema
  };
};

export default useValidation;
