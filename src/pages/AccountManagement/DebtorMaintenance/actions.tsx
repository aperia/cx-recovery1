import React, { Fragment, useCallback } from 'react';
import { useDispatch } from 'react-redux';

// components
import { Button, Tooltip } from 'app/_libraries/_dls/components';
import ConditionalWrapper from 'pages/__commons/ConditionalWrapper';
import DebtorConfirmBody from './DebtorConfirmBody';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// actions
import { accountManagementActions } from '../_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

// types
import { Debtors } from '../types';

// constants
import { I18N_TEXT } from 'app/constants';
interface ActionsGridDebtorProps {
  debtor: Debtors;
}

const ActionsGridDebtor: React.FC<ActionsGridDebtorProps> = ({ debtor }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const openDetail = () => {
    dispatch(accountManagementActions.toggleDebtorDetailModal({ debtor }));
  };

  const handleShowEditForm = useCallback(() => {
    dispatch(
      accountManagementActions.onToggleFormModal({
        selected: debtor
      })
    );
  }, [debtor, dispatch]);

  const hasAssociatedAccounts = useCallback(() => {
    return +debtor.id % 2 === 0;
  }, [debtor]);

  const handleShowConfirmDelete = useCallback(() => {
    dispatch(
      rootModalActions.open({
        autoClose: false,
        title: I18N_TEXT.DELETE_DEBTOR_CONFIRM_TITLE,
        body: <DebtorConfirmBody data={debtor} />,
        btnConfirmText: I18N_TEXT.DELETE,
        btnCancelText: I18N_TEXT.CANCEL,
        onConfirm: () => {
          dispatch(rootModalActions.loading(true));

          dispatch(
            accountManagementActions.deleteDebtor({
              id: debtor.id
            })
          );
        },
        onCancel: () => {
          dispatch(rootModalActions.close());
        }
      })
    );
  }, [debtor, dispatch]);

  return (
    <Fragment>
      <ConditionalWrapper
        condition={hasAssociatedAccounts()}
        wrapper={children => (
          <Tooltip
            triggerClassName="mr-8"
            element={<span>{t(I18N_TEXT.DISABLED_DELETE_DEBTOR)}</span>}
          >
            {children}
          </Tooltip>
        )}
      >
        <Button
          size="sm"
          variant="outline-danger"
          disabled={hasAssociatedAccounts()}
          onClick={handleShowConfirmDelete}
        >
          {t(I18N_TEXT.DELETE)}
        </Button>
      </ConditionalWrapper>

      <Button size="sm" variant="outline-primary" onClick={handleShowEditForm}>
        {t(I18N_TEXT.EDIT)}
      </Button>
      <Button size="sm" variant="outline-primary" onClick={openDetail}>
        {t(I18N_TEXT.VIEW)}
      </Button>
    </Fragment>
  );
};
export default ActionsGridDebtor;
