import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';

// components
import SearchAccountNumber from 'pages/AccountManagement/ManualAssignRecall/Form/SearchAccountNumber';
import { GroupRadioControl } from 'app/components/FormControl/GroupRadio';
import { DropdownControl } from 'app/components';
import { View } from 'app/_libraries/_dof/core';
import If from 'pages/__commons/If';

// hooks
import { useSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useRefData from 'app/hooks/useRefData';
import { useFormikContext } from 'formik';

// selectors
import { selectorSearchAcc } from 'pages/AccountManagement/_redux/manualAssignRecall/selectors';
import { selectSelectedDebtor } from 'pages/AccountManagement/_redux/debtorMaintenance/selectors';

// types
import {
  AssociatedAccountType,
  Debtors,
  IAccounts
} from 'pages/AccountManagement/types';
import { ICommonDebtorForm } from 'pages/AccountManagement/types';

// constants
import { I18N_TEXT } from 'app/constants';
import { RELATION } from '../constants';

// helpers
import { isEmpty } from 'app/helpers';
import { accountManagementActions } from 'pages/AccountManagement/_redux';

export interface AssociatedAccountProps extends ICommonDebtorForm {}
const AssociatedAccount: React.FC<AssociatedAccountProps> = ({}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const searchAcc: IAccounts = useSelector(selectorSearchAcc);

  const relationOptions = useRefData('relation');
  const ecoaOptions = useRefData('ecoa');
  const sendCorrespondenceOptions = useRefData('sendCorrespondence');
  const associatedAccountOptions = useRefData('associatedAccountType');
  const selected = useSelector<Debtors | undefined>(selectSelectedDebtor);
  const editMode = !!selected?.id;

  const hasSearchAccValue = !isEmpty(searchAcc);
  const { values, setValues, handleChange } = useFormikContext<any>();
  const { relation } = values;

  const onSearchByAcc = useCallback(
    ({ accNumber }: { accNumber: string }) => {
      dispatch(
        accountManagementActions.searchAccountByAcc({
          accNumber
        })
      );
      if (editMode) {
        setValues((prev: any) => ({
          ...prev,
          relation: undefined,
          ecoa: '',
          sendCorrespondence: ''
        }));
      }
    },
    [dispatch, editMode, setValues]
  );

  const { associatedAccountType } = values;

  const isEditPrimaryRelation =
    editMode && relation === RELATION.PRIMARY_DEBTOR;

  const renderGroupControl = useCallback(() => {
    const required = associatedAccountType === AssociatedAccountType.LINK;

    const disabled = !editMode && !hasSearchAccValue;

    return (
      <div className="row">
        <DropdownControl
          name="relation"
          label={t(I18N_TEXT.RELATION)}
          className="col-6 col-lg-4 mt-16"
          options={relationOptions}
          required={required}
          disabled={disabled}
          readOnly={isEditPrimaryRelation}
          isCombine
        />

        <DropdownControl
          name="ecoa"
          label={t(I18N_TEXT.ECOA)}
          className="col-6 col-lg-4 mt-16"
          options={ecoaOptions}
          required={required}
          disabled={disabled}
          readOnly={isEditPrimaryRelation}
          isCombine
        />

        <DropdownControl
          name="sendCorrespondence"
          label={t(I18N_TEXT.SEND_CORRESPONDENCE)}
          className="col-12 col-lg-4 mt-16"
          options={sendCorrespondenceOptions}
          required={required}
          disabled={disabled}
          readOnly={isEditPrimaryRelation}
          isCombine
        />
      </div>
    );
  }, [
    associatedAccountType,
    ecoaOptions,
    editMode,
    hasSearchAccValue,
    isEditPrimaryRelation,
    relationOptions,
    sendCorrespondenceOptions,
    t
  ]);

  const handleCheckboxChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      handleChange(event);
    },
    [handleChange]
  );

  return (
    <>
      <h5 className="mt-24 mb-16">{t(I18N_TEXT.ASSOCIATED_ACCOUNT)}</h5>

      <If condition={!editMode || !isEditPrimaryRelation}>
        <GroupRadioControl
          direction="horizontal"
          name="associatedAccountType"
          options={associatedAccountOptions}
          onValueChange={handleCheckboxChange}
        />

        <If
          condition={
            values.associatedAccountType === AssociatedAccountType.LINK
          }
        >
          <div className="bg-light-l20 border br-light-l04 br-radius-4 p-16 mt-16">
            <SearchAccountNumber
              searchAcc={searchAcc}
              onSearch={onSearchByAcc}
            />

            <If condition={(!editMode && hasSearchAccValue) || editMode}>
              <div className="mt-16 bg-white p-16 br-radius-4 border br-light-l04">
                <View
                  formKey="associatedAccountInfo"
                  descriptor="associatedAccountInfo"
                  value={editMode && isEmpty(searchAcc) ? values : searchAcc}
                />
              </div>
            </If>

            {renderGroupControl()}
          </div>
        </If>
      </If>
      <If condition={isEditPrimaryRelation}>
        <div className="bg-light-l20 border br-light-l04 br-radius-4 p-16 mt-16">
          <View
            formKey="associatedAccountInfo"
            descriptor="associatedAccountInfo"
            value={values}
          />
        </div>
        {renderGroupControl()}
      </If>
    </>
  );
};

export default AssociatedAccount;
