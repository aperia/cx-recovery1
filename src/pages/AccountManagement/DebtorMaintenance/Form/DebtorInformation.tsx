import React from 'react';

// components
import {
  DatePickerControl,
  DropdownControl,
  MaskedTextBoxControl,
  TextBoxControl
} from 'app/components';

// hooks
import { useSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useRefData from 'app/hooks/useRefData';

// selectors
import { selectSelectedDebtor } from 'pages/AccountManagement/_redux/debtorMaintenance/selectors';

// constants
import {
  I18N_TEXT,
  MAX_LENGTH_10,
  MAX_LENGTH_15,
  MAX_LENGTH_25,
  MAX_LENGTH_35,
  MAX_LENGTH_40,
  MAX_LENGTH_70,
  NUMERIC_REPLACE_REGEX,
  PHONE_NUMBER_MASK_INPUT_REGEX,
  PLACE_HOLDER_PHONE_NUMBER,
  PLACE_HOLDER_SSN,
  SSN_MASK_INPUT_REGEX
} from 'app/constants';

// types
import { Debtors, ICommonDebtorForm } from 'pages/AccountManagement/types';

export interface DebtorInformationProps extends ICommonDebtorForm {}

const DebtorInformation: React.FC<DebtorInformationProps> = ({
  className = ''
}) => {
  const { t } = useTranslation();

  const stateOptions = useRefData('state');
  const maritalStatusOptions = useRefData('maritalStatus');
  const addressFlagOptions = useRefData('addressFlag');

  const selected = useSelector<Debtors | undefined>(selectSelectedDebtor);
  const selectedId = selected?.id;

  return (
    <div className={`row ${className}`}>
      <div className="col-12">
        <h5>{t(I18N_TEXT.DEBTOR_INFORMATION)}</h5>
      </div>

      <TextBoxControl
        name="debtorName"
        label={t(I18N_TEXT.NAME)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_40}
        readOnly={!!selectedId}
        required={!selectedId}
      />

      <MaskedTextBoxControl
        name="ssn"
        label={t(I18N_TEXT.SSN)}
        className="col-6 col-lg-4 mt-16"
        mask={SSN_MASK_INPUT_REGEX}
        placeholder={PLACE_HOLDER_SSN}
        readOnly={!!selectedId}
        required={!selectedId}
      />

      <TextBoxControl
        name="salutation"
        label={t(I18N_TEXT.SALUTATION)}
        className="col-4 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_35}
      />

      <DatePickerControl
        name="dateOfBirth"
        label={t(I18N_TEXT.DATE_OF_BIRTH)}
        className="col-4 col-lg-3 mt-16 text-transform-none"
      />

      <DropdownControl
        name="maritalStatus"
        label={t(I18N_TEXT.MARITAL_STATUS)}
        className="col-4 col-lg-3 mt-16"
        options={maritalStatusOptions}
        isCombine
      />

      <MaskedTextBoxControl
        name="mobilePhone"
        label={t(I18N_TEXT.MOBILE_PHONE)}
        className="col-6 col-lg-3 mt-16"
        mask={PHONE_NUMBER_MASK_INPUT_REGEX}
        placeholder={PLACE_HOLDER_PHONE_NUMBER}
      />

      <MaskedTextBoxControl
        name="homePhone"
        label={t(I18N_TEXT.HOME_PHONE)}
        className="col-6 col-lg-3 mt-16"
        mask={PHONE_NUMBER_MASK_INPUT_REGEX}
        placeholder={PLACE_HOLDER_PHONE_NUMBER}
      />

      <TextBoxControl
        name="addressLine1"
        label={t(I18N_TEXT.ADDRESS_LINE_1)}
        className="col-12 col-lg-6 mt-16"
        maxLength={MAX_LENGTH_70}
      />

      <TextBoxControl
        name="addressLine2"
        label={t(I18N_TEXT.ADDRESS_LINE_2)}
        className="col-12 col-lg-6 mt-16"
        maxLength={MAX_LENGTH_70}
      />

      <div className="col-12 col-lg-6">
        <div className="row">
          <TextBoxControl
            name="debtorCity"
            label={t(I18N_TEXT.CITY)}
            className="col-4 mt-16"
            maxLength={MAX_LENGTH_25}
          />

          <DropdownControl
            name="debtorState"
            label={t(I18N_TEXT.STATE)}
            className="col-4 mt-16"
            options={stateOptions}
            isCombine
          />

          <TextBoxControl
            name="debtorZipCode"
            label={t(I18N_TEXT.ZIP_CODE)}
            className="col-4 mt-16"
            maxLength={MAX_LENGTH_10}
            regex={NUMERIC_REPLACE_REGEX}
          />
        </div>
      </div>

      <div className="col-12 col-lg-6">
        <div className="row">
          <TextBoxControl
            name="country"
            label={t(I18N_TEXT.COUNTRY)}
            className="col-6 mt-16"
            maxLength={MAX_LENGTH_15}
          />

          <DropdownControl
            name="addressFlag"
            label={t(I18N_TEXT.ADDRESS_FLAG)}
            className="col-6 mt-16"
            options={addressFlagOptions}
            isCombine
          />
        </div>
      </div>
    </div>
  );
};

export default DebtorInformation;
