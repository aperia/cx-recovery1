import React from 'react';

// Component
import {
  DatePickerControl,
  DropdownControl,
  MaskedTextBoxControl,
  TextBoxControl
} from 'app/components';
import FormikTextAreaControl from 'app/components/FormControl/FormikTextAreaControl';

// Utils
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useRefData from 'app/hooks/useRefData';

import {
  EMAIL_REPLACE_REGEX,
  I18N_TEXT,
  MAX_LENGTH_10,
  MAX_LENGTH_25,
  MAX_LENGTH_300,
  MAX_LENGTH_40,
  MAX_LENGTH_5,
  MAX_LENGTH_50,
  MAX_LENGTH_70,
  NUMERIC_REPLACE_REGEX,
  PHONE_NUMBER_MASK_INPUT_REGEX,
  PLACE_HOLDER_PHONE_NUMBER
} from 'app/constants';

// types
import { ICommonDebtorForm } from 'pages/AccountManagement/types';

export interface EmployerInformationProps extends ICommonDebtorForm {}

const EmployerInformation: React.FC<EmployerInformationProps> = ({
  className = ''
}) => {
  const { t } = useTranslation();
  const stateOptions = useRefData('state');

  return (
    <div className={`row ${className}`}>
      <div className="col-12">
        <h5>{t(I18N_TEXT.EMPLOYER_INFORMATION)}</h5>
      </div>
      <TextBoxControl
        name="eName"
        label={t(I18N_TEXT.NAME)}
        className="col-4 mt-16"
        maxLength={MAX_LENGTH_40}
      />

      <MaskedTextBoxControl
        name="ePhone"
        label={t(I18N_TEXT.WORK_PHONE)}
        className="col-4 mt-16"
        mask={PHONE_NUMBER_MASK_INPUT_REGEX}
        placeholder={PLACE_HOLDER_PHONE_NUMBER}
      />

      <TextBoxControl
        name="eMiscellaneous"
        label={t(I18N_TEXT.MISCELLANEOUS_HOME_EMAIL)}
        className="col-4 mt-16"
        maxLength={MAX_LENGTH_50}
        regex={EMAIL_REPLACE_REGEX}
      />
      <TextBoxControl
        name="eAddress"
        label={t(I18N_TEXT.ADDRESS)}
        className="col-12 col-lg-6 mt-16"
        maxLength={MAX_LENGTH_70}
      />

      <div className="col-12 col-lg-6">
        <div className="row">
          <TextBoxControl
            name="eCity"
            label={t(I18N_TEXT.CITY)}
            className="col-4 mt-16"
            maxLength={MAX_LENGTH_25}
          />
          <DropdownControl
            name="eState"
            label={t(I18N_TEXT.STATE)}
            className="col-4 mt-16"
            options={stateOptions}
            isCombine
          />
          <TextBoxControl
            name="eZipCode"
            label={t(I18N_TEXT.ZIP_CODE)}
            className="col-4 mt-16"
            maxLength={MAX_LENGTH_10}
            regex={NUMERIC_REPLACE_REGEX}
          />
        </div>
      </div>

      <div className="col-12 col-lg-6">
        <div className="row">
          <TextBoxControl
            name="eShortField1"
            label={t(I18N_TEXT.SHORT_FIELD_1)}
            className="col-6 mt-16"
            maxLength={MAX_LENGTH_5}
          />
          <TextBoxControl
            name="eShortField2"
            label={t(I18N_TEXT.SHORT_FIELD_2)}
            className="col-6 mt-16"
            maxLength={MAX_LENGTH_5}
          />
          <DatePickerControl
            name="eDateField1"
            label={t(I18N_TEXT.DATE_FIELD_1)}
            className="col-6 mt-16"
          />
          <DatePickerControl
            name="eDateField2"
            label={t(I18N_TEXT.DATE_FIELD_2)}
            className="col-6 mt-16"
          />
          <TextBoxControl
            name="eLongField1"
            label={t(I18N_TEXT.LONG_FIELD_1)}
            className="col-6 mt-16"
            maxLength={MAX_LENGTH_25}
          />
          <TextBoxControl
            name="eLongField2"
            label={t(I18N_TEXT.LONG_FIELD_2)}
            className="col-6 mt-16"
            maxLength={MAX_LENGTH_25}
          />
        </div>
      </div>

      <div className="col-12 col-lg-6 mt-16">
        <FormikTextAreaControl
          className="h-lg-140px"
          name="eComment"
          label={t(I18N_TEXT.COMMENT)}
          maxLength={MAX_LENGTH_300}
          disabledEnter
        />
      </div>
    </div>
  );
};

export default EmployerInformation;
