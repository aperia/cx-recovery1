import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';

// components
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import DebtorInformation from './DebtorInformation';
import EmployerInformation from './EmployerInformation';
import AssociatedAccount from './AssociatedAccount';
import ReplaceDebtorModal from 'pages/AccountManagement/ManageAssociatedDebtors/ReplaceDebtorModal';

// constants
import { I18N_TEXT } from 'app/constants';

// hooks
import { useSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useDebtorForm from '../hooks/useDebtorForm';

// selectors
import {
  selectDebtorFormIsOpen,
  selectSelectedDebtor
} from 'pages/AccountManagement/_redux/debtorMaintenance/selectors';

// actions
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

// types
import { Debtors, ICommonDebtorForm } from 'pages/AccountManagement/types';

// helpers
import { FormikProvider } from 'formik';
import { isTouchedForm } from 'app/helpers';
import { RELATION } from '../constants';

export interface DebtorFormControlProps extends ICommonDebtorForm {}

const DebtorFormControl: React.FC<DebtorFormControlProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const isOpen = useSelector<boolean>(selectDebtorFormIsOpen);
  const selected = useSelector<Debtors | undefined>(selectSelectedDebtor);
  const editMode = !!selected?.id;

  const { formik } = useDebtorForm();

  const {
    submitForm,
    resetForm,
    setSubmitting,
    isSubmitting,
    isValid,
    touched,
    values,
    initialValues
  } = formik;

  const { accNumber, relation } = values;

  const handleCancelForm = useCallback(() => {
    resetForm();
    setSubmitting(false);

    dispatch(
      accountManagementActions.onToggleFormModal({
        selected: undefined
      })
    );

    dispatch(accountManagementActions.onSearchByAccReset());
  }, [dispatch, resetForm, setSubmitting]);

  const handleCancel = useCallback(() => {
    if (isTouchedForm(touched)) {
      dispatch(
        rootModalActions.open({
          title: I18N_TEXT.UNSAVED_CHANGE,
          body: I18N_TEXT.DISCARD_CHANGE_BODY,
          btnCancelText: I18N_TEXT.CONTINUE_EDITING,
          btnConfirmText: I18N_TEXT.DISCARD_CHANGE,
          onConfirm: handleCancelForm
        })
      );
      return;
    }

    handleCancelForm();
  }, [touched, dispatch, handleCancelForm]);

  const handleSubmit = useCallback(() => {
    const { relation: initialRelation = '' } = initialValues;
    const { relation: currentRelation = '' } = values;
    const isDirtyRelation = currentRelation !== initialRelation;

    if (
      editMode &&
      accNumber &&
      isDirtyRelation &&
      (relation === RELATION.LEGAL_RELATIONSHIP ||
        relation === RELATION.CO_MAKER)
    ) {
      dispatch(
        rootModalActions.open({
          size: 'sm',
          className: 'modal-debtor',
          title: I18N_TEXT.CONFIRM_REPLACE_EXIST_RELATION,
          body: (
            <ReplaceDebtorModal
              accNumber={accNumber?.accountNumberUnMask}
              selectedRelation={relation}
            />
          ),
          btnConfirmText: I18N_TEXT.REPLACE,
          onConfirm: () => {
            setSubmitting(true);
            submitForm();
          }
        })
      );

      return;
    }

    setSubmitting(true);
    submitForm();
  }, [
    accNumber,
    dispatch,
    editMode,
    initialValues,
    relation,
    setSubmitting,
    submitForm,
    values
  ]);

  return (
    <FormikProvider value={formik}>
      <Modal show={isOpen} loading={false} enforceFocus={false} full>
        <ModalHeader
          border
          closeButton
          className="border-bottom"
          onHide={handleCancel}
        >
          <ModalTitle className="text-capitalize">
            {editMode ? t(I18N_TEXT.EDIT_DEBTOR) : t(I18N_TEXT.ADD_DEBTOR)}
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <div className="max-width-lg mx-auto">
            <DebtorInformation />
            <EmployerInformation className="mt-24" />
            <AssociatedAccount />
          </div>
        </ModalBody>
        <ModalFooter
          border
          cancelButtonText={t(I18N_TEXT.CANCEL)}
          okButtonText={editMode ? t(I18N_TEXT.SAVE) : t(I18N_TEXT.SUBMIT)}
          onCancel={handleCancel}
          onOk={handleSubmit}
          disabledOk={
            !editMode && (isSubmitting || !(isTouchedForm(touched) && isValid))
          }
        ></ModalFooter>
      </Modal>
    </FormikProvider>
  );
};

export default DebtorFormControl;
