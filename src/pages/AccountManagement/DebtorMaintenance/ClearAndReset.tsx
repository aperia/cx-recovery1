import React, { useEffect, useState } from 'react';

// components
import { TextSearch } from 'app/_libraries/_dls/components';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { accountManagementActions } from '../_redux';
import { selectorValueSearch } from '../_redux/debtorMaintenance/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { I18N_TEXT } from 'app/constants';

const ClearAndReset: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const [search, setSearch] = useState('');
  const valueSearch: string = useSelector(selectorValueSearch);

  useEffect(() => {
    setSearch(valueSearch);
  }, [valueSearch]);

  const handleSearch = (value: string) => {
    dispatch(accountManagementActions.onChangeSearchDebtors(value));
  };

  const handleChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    setSearch(event.target.value);
  };

  const handleClear = () => {
    setSearch('');
  };

  return (
    <TextSearch
      value={search}
      placeholder={t(I18N_TEXT.TYPE_TO_SEARCH)}
      dataTestId="search-entitlement"
      onSearch={handleSearch}
      onChange={handleChange}
      onClear={handleClear}
    />
  );
};
export default ClearAndReset;
