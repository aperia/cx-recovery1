import React, {
  Fragment,
  useCallback,
  useEffect,
  useLayoutEffect,
  useMemo,
  useRef
} from 'react';

// components
import {
  ColumnType,
  Grid,
  Pagination,
  SimpleBar,
  TextSearch,
  Button
} from 'app/_libraries/_dls/components';
import If from 'pages/__commons/If';
import FailedApiReload from 'app/components/FailedApiReload';
import { NoDataGrid, SearchNodata } from 'app/components';
import DebtorFormControl from './Form';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { accountManagementActions } from '../_redux';
import {
  selectorDebtors,
  selectorIsError,
  selectorValueSearch
} from '../_redux/debtorMaintenance/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { usePagination } from 'app/hooks/usePagination';

// helpers
import { columnDebtorMaintenance } from './constants';
import { I18N_TEXT } from 'app/constants';
import { Debtors } from '../types';
import classnames from 'classnames';
import { isEmpty } from 'lodash';
import DebtorDetail from './DebtorDetail';

interface DebtorMaintenanceProps {}

const DebtorMaintenance: React.FC<DebtorMaintenanceProps> = ({}) => {
  const { t } = useTranslation();
  const divRef = useRef<HTMLDivElement | null>(null);
  const dispatch = useDispatch();

  const data: Debtors[] = useSelector(selectorDebtors);
  const isError: boolean = useSelector(selectorIsError);
  const valueSearch: string = useSelector(selectorValueSearch);

  const getDebtors = useCallback(() => {
    dispatch(accountManagementActions.getDebtors({}));
  }, [dispatch]);

  useEffect(() => {
    getDebtors();
  }, [getDebtors]);

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 193px)`);
  }, []);

  const handleSearch = (value: string) => {
    dispatch(accountManagementActions.onChangeSearchDebtors(value));
  };

  const handleSort = (value: ISortType) => {
    dispatch(accountManagementActions.onChangeSortDebtors(value));
  };

  const handleShowAddForm = useCallback(() => {
    dispatch(
      accountManagementActions.onToggleFormModal({
        selected: undefined
      })
    );
  }, [dispatch]);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(data);

  const columns = useMemo(() => columnDebtorMaintenance(t), [t]);

  const renderLayout = () => {
    if (isError)
      return <FailedApiReload id="getDebtors" onReload={getDebtors} />;

    if (!data.length && valueSearch)
      return (
        <SearchNodata
          text={t(I18N_TEXT.ADJUST_YOUR_SEARCH_FILTER)}
          onClearSearch={() => handleSearch('')}
        />
      );

    if (!data.length)
      return (
        <NoDataGrid text={t(I18N_TEXT.NO_DEBTORS_TO_DISPLAY)}>
          <Button variant="outline-primary">{t(I18N_TEXT.ADD_DEBTOR)}</Button>
        </NoDataGrid>
      );

    return (
      <Fragment>
        <div className={'text-right mb-16 mr-n8'}>
          <Button
            size="sm"
            variant="outline-primary"
            onClick={handleShowAddForm}
          >
            {t(I18N_TEXT.ADD_DEBTOR)}
          </Button>
          <If condition={!isEmpty(valueSearch)}>
            <Button
              size="sm"
              onClick={() => handleSearch('')}
              variant="outline-primary"
            >
              {t(I18N_TEXT.CLEAR_AND_RESET)}
            </Button>
          </If>
        </div>
        <Grid
          onSortChange={handleSort}
          data={gridData as unknown as ColumnType[]}
          columns={columns}
        />

        <If condition={data.length > 10}>
          <div className="pt-16">
            <Pagination
              totalItem={total}
              pageSize={pageSize}
              pageNumber={currentPage}
              pageSizeValue={currentPageSize}
              compact
              onChangePage={onPageChange}
              onChangePageSize={onPageSizeChange}
            />
          </div>
        </If>

        <DebtorFormControl />
      </Fragment>
    );
  };

  return (
    <div ref={divRef}>
      <SimpleBar>
        <div className={classnames('px-24 pb-24 pt-22 max-width-lg mx-auto')}>
          <div className="d-flex justify-content-between mb-16 align-items-center">
            <h5>{t(I18N_TEXT.DEBTOR_LIST)}</h5>

            <TextSearch
              placeholder={t(I18N_TEXT.TYPE_TO_SEARCH)}
              dataTestId="search-entitlement"
              onSearch={handleSearch}
            />
          </div>
          {renderLayout()}
        </div>
      </SimpleBar>
      <DebtorDetail />
    </div>
  );
};
export default DebtorMaintenance;
