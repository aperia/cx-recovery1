import { IDeleteDebtorArgs } from '../types';
import { ISaveDebtorArgs } from '../_redux/debtorMaintenance/saveDebtor';
import { debtorMaintenanceController } from '../__mock__/debtorMaintenanceController';

const debtorMaintenanceServices = {
  getDebtors() {
    return debtorMaintenanceController.getDebtor();
  },
  deleteDebtor({ id }: IDeleteDebtorArgs) {
    return debtorMaintenanceController.deleteDebtor({ id });
  },
  saveDebtor(args: ISaveDebtorArgs) {
    return debtorMaintenanceController.saveDebtor(args);
  }
};

export default debtorMaintenanceServices;
