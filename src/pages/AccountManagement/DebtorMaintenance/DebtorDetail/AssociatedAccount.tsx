import Label from 'app/components/Label';
import MaskAccNbr from 'app/components/MaskAccNbr';
import { I18N_TEXT } from 'app/constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import LinkedDebtors from 'pages/AccountManagement/components/LinkedDebtors';
import React from 'react';
import { LINKED_DEBTORS } from '../constants';

const AssociatedAccount: React.FC = () => {
  const { t } = useTranslation();

  return (
    <>
      <h5 className="mt-24">{t(I18N_TEXT.ASSOCIATED_ACCOUNT)}</h5>
      <div className="p-16 border br-light-l04 rounded-lg mt-16">
        <div className="row">
          <div className="col-lg-4">
            <MaskAccNbr
              label={t(I18N_TEXT.ACCOUNT_NUMBER)}
              value={{
                accountNumberUnMask: '0220022004341490',
                accountNumber: '022002••••••1490'
              }}
            />
          </div>
          <div className="col-xl-3 col-lg-8 col-md-5 order-xl-1 order-lg-1 order-md-2 mt-md-16 mt-lg-0">
            <Label label={t(I18N_TEXT.RELATION)} value="A - Primary Debtor" />
          </div>
          <div className="col-xl-5 col-lg-8 col-md-7 order-xl-2 order-lg-3 order-md-3 mt-md-16 mt-lg-4 mt-xl-0">
            <Label label="ECOA" value="Q - Do not report" />
          </div>
          <div className="col-lg-4 order-xl-3 order-lg-2 order-md-1 mt-4">
            <LinkedDebtors values={LINKED_DEBTORS} />
          </div>
          <div className="col-xl-5 col-lg-8 offset-xl-3 offset-lg-4 order-md-4 mt-4">
            <Label
              label={t(I18N_TEXT.SEND_CORRESPONDENCE)}
              value="B - This person never receives correspondence."
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default AssociatedAccount;
