import { I18N_TEXT } from 'app/constants';
import { formatTime } from 'app/helpers';
import { usePagination } from 'app/hooks/usePagination';
import { ColumnType, Grid } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { IAddressHistory } from 'pages/AccountManagement/types';
import { selectedAddressHistory } from 'pages/AccountManagement/_redux/debtorMaintenance/selectors';
import React from 'react';
import { useSelector } from 'react-redux';

const AddressHistory: React.FC = () => {
  const { t } = useTranslation();
  const addressHistory: IAddressHistory[] = useSelector(selectedAddressHistory);

  const ADDRESS_HISTORY_COLUMNS: ColumnType<IAddressHistory>[] = [
    {
      id: 'changeDateTime',
      Header: t(I18N_TEXT.CHANGE_DATE_TIME),
      isSort: true,
      accessor: (item: IAddressHistory) => (
        <>
          <div>{formatTime(`${item.date} ${item.time}`).date}</div>
          <div className="color-grey">
            {formatTime(`${item.date} ${item.time}`).fullTimeMeridiem}
          </div>
        </>
      ),
      width: 168
    },
    {
      id: 'address',
      Header: t(I18N_TEXT.ADDRESS),
      isSort: true,
      accessor: ({ address, city, state, zipCode }: IAddressHistory) =>
        `${address}, ${city}, ${state} ${zipCode}`,
      width: 340,
      autoWidth: true
    },
    {
      id: 'county',
      Header: t(I18N_TEXT.COUNTY),
      isSort: true,
      accessor: 'county',
      width: 168
    },
    {
      id: 'mobileNumber',
      Header: t(I18N_TEXT.MOBILE_PHONE),
      isSort: true,
      accessor: 'mobileNumber',
      width: 175
    },
    {
      id: 'homePhone',
      Header: t(I18N_TEXT.HOME_PHONE),
      isSort: true,
      accessor: 'homePhone',
      width: 175
    },
    {
      id: 'workPhone',
      Header: t(I18N_TEXT.WORK_PHONE),
      isSort: true,
      accessor: 'workPhone',
      width: 175
    }
  ];

  const { gridData } = usePagination(addressHistory);

  return (
    <>
      <h5 className="mt-24">{t(I18N_TEXT.ADDRESS_HISTORY)}</h5>
      <Grid
        className="mt-16"
        columns={ADDRESS_HISTORY_COLUMNS}
        data={gridData}
      />
    </>
  );
};

export default AddressHistory;
