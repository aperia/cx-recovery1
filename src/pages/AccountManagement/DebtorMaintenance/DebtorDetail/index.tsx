import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';

// components
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  Tooltip
} from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';
import AddressHistory from './AddressHistory';
import AssociatedAccount from './AssociatedAccount';
import ConditionalWrapper from 'pages/__commons/ConditionalWrapper';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// types
import { Debtors } from 'pages/AccountManagement/types';

// actions
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

// selectors
import {
  selectedDebtorDetail,
  selectedViewDetailOpen
} from 'pages/AccountManagement/_redux/debtorMaintenance/selectors';

// constants
import { I18N_TEXT } from 'app/constants';
import {
  ACCOUNT_INFO_VIEW,
  EMPLOYER_DATA,
  EMPLOYER_INFO_VIEW
} from '../constants';
import DebtorConfirmBody from '../DebtorConfirmBody';

const DebtorDetail: React.FC = () => {
  const isOpen: boolean = useSelector(selectedViewDetailOpen);
  const selectedDebtor: Debtors | undefined = useSelector(selectedDebtorDetail);
  const selectedId = selectedDebtor?.id || '0';
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const [address, setAddress] = useState('');

  useEffect(() => {
    dispatch(accountManagementActions.getAddressHistory());
  }, [dispatch]);

  useEffect(() => {
    if (selectedDebtor) {
      const { addressLineOne, addressLineTwo, state, zipCode } = selectedDebtor;
      setAddress(`${addressLineOne}, ${addressLineTwo}\n${state} ${zipCode}`);
    }
  }, [selectedDebtor]);

  const toggleModal = useCallback(() => {
    dispatch(accountManagementActions.toggleDebtorDetailModal({}));
  }, [dispatch]);

  const handleShowEditForm = useCallback(() => {
    dispatch(
      accountManagementActions.onToggleFormModal({
        selected: selectedDebtor
      })
    );

    toggleModal();
  }, [dispatch, selectedDebtor, toggleModal]);

  const hasAssociatedAccounts = useCallback(() => {
    return +selectedId % 2 === 0;
  }, [selectedId]);

  const handleShowConfirmDelete = useCallback(() => {
    dispatch(
      rootModalActions.open({
        autoClose: false,
        title: I18N_TEXT.DELETE_DEBTOR_CONFIRM_TITLE,
        body: <DebtorConfirmBody data={selectedDebtor} />,
        btnConfirmText: I18N_TEXT.DELETE,
        btnCancelText: I18N_TEXT.CANCEL,
        onConfirm: () => {
          dispatch(rootModalActions.loading(true));

          dispatch(
            accountManagementActions.deleteDebtor({
              id: selectedId
            })
          );
        },
        onCancel: () => {
          dispatch(rootModalActions.close());
        }
      })
    );
  }, [dispatch, selectedDebtor, selectedId]);

  return (
    <Modal show={isOpen} full>
      <ModalHeader border closeButton onHide={toggleModal}>
        <ModalTitle>{t(I18N_TEXT.DEBTOR_DETAILS)}</ModalTitle>
      </ModalHeader>
      <ModalBody noPadding>
        <div className="bg-light-l20 border-bottom p-24">
          <View
            id={ACCOUNT_INFO_VIEW}
            formKey={ACCOUNT_INFO_VIEW}
            descriptor={ACCOUNT_INFO_VIEW}
            value={{
              ...selectedDebtor,
              address
            }}
          />
          <div className="position-absolute-top-right d-flex pt-24 pr-16">
            <ConditionalWrapper
              condition={hasAssociatedAccounts()}
              wrapper={children => (
                <Tooltip
                  triggerClassName="mr-8"
                  element={<span>{t(I18N_TEXT.DISABLED_DELETE_DEBTOR)}</span>}
                >
                  {children}
                </Tooltip>
              )}
            >
              <Button
                size="sm"
                variant="outline-danger"
                disabled={hasAssociatedAccounts()}
                onClick={handleShowConfirmDelete}
              >
                {t(I18N_TEXT.DELETE)}
              </Button>
            </ConditionalWrapper>
            <Button
              size="sm"
              variant="outline-primary"
              onClick={handleShowEditForm}
            >
              {t(I18N_TEXT.EDIT)}
            </Button>
          </div>
        </div>
        <div className="p-24">
          <View
            id={EMPLOYER_INFO_VIEW}
            formKey={EMPLOYER_INFO_VIEW}
            descriptor={EMPLOYER_INFO_VIEW}
            value={EMPLOYER_DATA}
          />
          <AssociatedAccount />
          <AddressHistory />
        </div>
      </ModalBody>
      <ModalFooter border>
        <Button onClick={toggleModal} variant="secondary">
          {t(I18N_TEXT.CLOSE)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default DebtorDetail;
