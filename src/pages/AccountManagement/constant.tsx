import React from 'react';
import { I18N_TEXT } from 'app/constants';
import { NoCombineField } from 'app/_libraries/_dls/components';
import AccountList from './AccountList';
import ManualRecallList from './ManualAssignRecall';
import DebtorMaintenance from './DebtorMaintenance';
import PendingCorrespondence from './Correspondence/PendingCorrespondence';
import { AccountManagementContentTabs, AccountManagementTabs } from './types';

export const DATA_TEST_ID_ACCOUNT = 'accounts';
export const GET_MANUAL_RECALL = 'getManualRecall';

export const tabsAccountManagement: AccountManagementTabs[] = [
  { id: 'accountMaintenance', title: I18N_TEXT.ACCOUNT_MAINTENANCE },
  {
    id: 'debtorMaintenance',
    title: I18N_TEXT.DEBTOR_MAINTENANCE
  },
  {
    id: 'assignRecallList',
    title: I18N_TEXT.MANUAL_ASSIGN_RECALL_REQUESTS
  },
  {
    id: 'pendingCorrespondence',
    title: I18N_TEXT.PENDING_CORRESPONDENCE
  }
];

export const componentAccountManagement: AccountManagementContentTabs[] = [
  {
    id: 'accountMaintenance',
    Component: () => {
      return <AccountList atScreen="accountMaintenance" />;
    }
  },
  {
    id: 'assignRecallList',
    Component: ManualRecallList
  },
  {
    id: 'debtorMaintenance',
    Component: DebtorMaintenance
  },
  {
    id: 'pendingCorrespondence',
    Component: PendingCorrespondence
  }
];

export enum ATTRIBUTE_SEARCH_FIELD {
  DEBTOR_NAME = 'debtorName',
  ACCOUNT_NUMBER = 'accountNumber',
  SSN = 'ssn',
  ORIGINAL_LOAN_NUMBER = 'originalLoanNumber',
  AGENCY_CODE = 'agencyCode',
  CALENDAR_CODE = 'calendarCode',
  REASON_CODE = 'reasonCode'
}

export const MESSAGE = {
  SSN: 'txt_ssn_must_be_9_digit',
  ACCOUNT_NUMBER_SEARCH: 'txt_account_number_must_be_16_digit',
  ORIGINAL_LOAN_NUMBER_SEARCH: 'txt_original_loan_number_must_be_20_digit',
  DEBTOR_NAME: 'txt_debtor_name_is_required',
  AGENCY_CODE: 'txt_agency_code_is_required',
  CALENDAR_CODE: 'txt_calendar_code_is_required',
  REASON_CODE: 'txt_reason_code_is_required'
};

export const FIELD_NOT_CONSTRAINT: string[] = [
  ATTRIBUTE_SEARCH_FIELD.SSN,
  ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER,
  ATTRIBUTE_SEARCH_FIELD.DEBTOR_NAME,
  ATTRIBUTE_SEARCH_FIELD.ORIGINAL_LOAN_NUMBER
];

export const NO_COMBINE_FIELDS: NoCombineField[] = [
  {
    field: ATTRIBUTE_SEARCH_FIELD.DEBTOR_NAME,
    filterMessage: I18N_TEXT.DEBTOR_NAME_CANNOT_COMBINED
  },
  {
    field: ATTRIBUTE_SEARCH_FIELD.ACCOUNT_NUMBER,
    filterMessage: I18N_TEXT.ACCOUNT_NUMBER_CANNOT_COMBINED
  },
  {
    field: ATTRIBUTE_SEARCH_FIELD.SSN,
    filterMessage: I18N_TEXT.SSN_CANNOT_COMBINED
  },
  {
    field: ATTRIBUTE_SEARCH_FIELD.ORIGINAL_LOAN_NUMBER,
    filterMessage: I18N_TEXT.ORIGINAL_LOAN_CANNOT_COMBINED
  }
];
