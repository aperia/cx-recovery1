import codeList from './codeList.json';
import pendingCorrespondence from './pendingCorrespondence.json';
import { orderBy } from 'app/helpers';

// types
import {
  ILetterCode,
  IPayloadCorrespondence,
  IPayloadPendingCorrespondence,
  IPayloadSendCorrespondence,
  IPendingCorrespondence
} from '../types';

// helpers
import { Repository } from 'app/utils/repositories/concretes/Repository';

//
const letterCodeRepository = new Repository<ILetterCode>(
  JSON.parse(JSON.stringify(codeList))
);

let pendingCorrespondenceRepository: IPendingCorrespondence[] = JSON.parse(
  JSON.stringify(pendingCorrespondence)
);

const TIME_DELAY = 600;
const isHaveEmpty = window.location.href.includes('empty');
const isHaveError = window.location.href.includes('error');

export const correspondenceController = {
  getLetterCode(params: IPayloadCorrespondence) {
    try {
      const searchValue = params.searchValue;
      const type = params.type;

      const { data: letterCodeData } = letterCodeRepository.getAll();

      let mappingData: ILetterCode[] = letterCodeData || [];

      if (type) {
        mappingData = letterCodeData.filter(item =>
          item?.type?.toLowerCase().includes(type.toLocaleLowerCase())
        );
      }

      let filterData: ILetterCode[] = mappingData;

      if (searchValue) {
        filterData = filterData.filter(
          item =>
            item?.letterCode
              ?.toLowerCase()
              .includes(searchValue.toLocaleLowerCase()) ||
            item?.description
              ?.toLowerCase()
              .includes(searchValue.toLocaleLowerCase()) ||
            item?.letterVariables
              ?.toLowerCase()
              .includes(searchValue.toLocaleLowerCase())
        );
      }

      if (params.sortBy) {
        filterData = orderBy(
          filterData,
          params.sortBy.id,
          params.sortBy.order as 'asc' | 'desc'
        );
      }

      const ITEMS_PER_PAGE = params.pageSize;
      const totalItem = filterData.length;
      let startIndex = (params.page - 1) * ITEMS_PER_PAGE;
      let endIndex = startIndex + (ITEMS_PER_PAGE - 1);

      let listData = filterData.slice(startIndex, endIndex + 1);

      if (listData.length === 0) {
        startIndex = (params.page - 2) * ITEMS_PER_PAGE;
        endIndex = startIndex + (ITEMS_PER_PAGE - 1);
        listData = filterData.slice(startIndex, endIndex + 1);
      }
      const currentPage = Math.ceil((startIndex - 1) / ITEMS_PER_PAGE + 1);

      return new Promise<any>(function (resolve, reject) {
        setTimeout(() => {
          resolve({
            success: true,
            data: {
              data: isHaveEmpty ? [] : listData,
              totalItem,
              currentPage: currentPage > 0 ? currentPage : 1,
              pageSize: params.pageSize
            }
          });
        }, TIME_DELAY);
      });
    } catch (err) {
      console.error('err', err);
      return new Promise<any>(function (resolve, reject) {
        setTimeout(() => {
          resolve({
            success: true,
            data: {
              data: [],
              totalItem: 0,
              currentPage: params.page,
              pageSize: params.pageSize
            }
          });
        }, TIME_DELAY);
      });
    }
  },
  getPendingCorrespondence(params: IPayloadPendingCorrespondence) {
    try {
      let filterData: IPendingCorrespondence[] =
        pendingCorrespondenceRepository || [];

      if (params.searchValue.length > 0) {
        const ky = params.searchValue[0].key;
        const value = params.searchValue[0].value;

        if (ky === 'accountNumber') {
          filterData = pendingCorrespondenceRepository.filter(item =>
            `${item?.accNumber.accountNumberUnMask?.toLocaleLowerCase()}`.includes(
              value.toLocaleLowerCase()
            )
          );
        }

        if (ky === 'letterCode') {
          filterData = pendingCorrespondenceRepository.filter(item =>
            `${item?.code?.toLocaleLowerCase()}`.includes(
              value.toLocaleLowerCase()
            )
          );
        }
      }

      if (params.sortBy) {
        filterData = orderBy(
          filterData,
          params.sortBy,
          params.orderBy as 'asc' | 'desc'
        );
      }

      const ITEMS_PER_PAGE = params.pageSize;
      const totalItem = filterData.length;
      let startIndex = (params.page - 1) * ITEMS_PER_PAGE;
      let endIndex = startIndex + (ITEMS_PER_PAGE - 1);

      let listData = filterData.slice(startIndex, endIndex + 1);

      if (listData.length === 0) {
        startIndex = (params.page - 2) * ITEMS_PER_PAGE;
        endIndex = startIndex + (ITEMS_PER_PAGE - 1);
        listData = filterData.slice(startIndex, endIndex + 1);
      }
      const currentPage = Math.ceil((startIndex - 1) / ITEMS_PER_PAGE + 1);

      return new Promise<any>(function (resolve, reject) {
        setTimeout(() => {
          resolve({
            success: true,
            data: {
              data: isHaveEmpty ? [] : listData,
              totalItem,
              currentPage: currentPage > 0 ? currentPage : 1,
              pageSize: params.pageSize
            }
          });
        }, TIME_DELAY);
      });
    } catch (err) {
      console.error('err', err);
      return new Promise<any>(function (resolve, reject) {
        setTimeout(() => {
          resolve({
            success: true,
            data: {
              data: [],
              totalItem: 0,
              currentPage: params.page,
              pageSize: params.pageSize
            }
          });
        }, TIME_DELAY);
      });
    }
  },
  removePendingCorrespondence(id: string) {
    try {
      if (isHaveError)
        return Promise.reject({
          success: false,
          message: 'DELETE FAILED'
        });

      pendingCorrespondenceRepository = pendingCorrespondenceRepository.filter(
        obj => obj.id !== id
      );

      return Promise.resolve({
        success: true,
        data: pendingCorrespondenceRepository
      });
    } catch (err) {
      return Promise.reject({
        success: false,
        data: pendingCorrespondenceRepository
      });
    }
  },
  sendPendingCorrespondence(payload: IPayloadSendCorrespondence) {
    try {
      if (isHaveError)
        return Promise.reject({
          success: false,
          message: 'SEND CORRESPONDENCE FAILED'
        });

      pendingCorrespondenceRepository = [
        ...pendingCorrespondenceRepository,
        {
          id: pendingCorrespondenceRepository.length.toString(),
          ...payload
        }
      ];

      return Promise.resolve({
        success: true,
        data: pendingCorrespondenceRepository
      });
    } catch (err) {
      return Promise.reject({
        success: false,
        data: pendingCorrespondenceRepository
      });
    }
  }
};
