import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import accountList from './accountList.json';

// types
import { IAccounts } from '../types';
import { ISaveAccountArgs } from '../_redux/accountList/saveAccount';

// helpers
import { maskedNumber } from './../../../app/helpers/formatMask';
import { Repository } from 'app/utils/repositories/concretes/Repository';

//
import { accountGroupListData } from 'pages/AccountGroup/__mock__/accountGroupController';
import { IDataGroupList } from 'pages/AccountGroup/types';

const isHaveError = window.location.href.includes('error');

const accountRepository = new Repository<IAccounts>(
  JSON.parse(JSON.stringify(accountList))
);

export const accountListController = {
  getAccountList(
    searchValue: AttributeSearchValue[],
    startSequence: number,
    endSequence: number
  ) {
    const ky = searchValue[0].key;

    const value = searchValue[0].value;

    let filterData: IAccounts[] = [];

    const { data: accountListData } = accountRepository.getAll();

    if (ky === 'accountNumber') {
      filterData = accountListData.filter((item: any) =>
        item?.accNumber?.accountNumberUnMask?.includes(value)
      );
    }

    if (ky === 'originalLoanNumber') {
      filterData = accountListData.filter((item: any) =>
        item?.originalLoan?.accountNumberUnMask?.includes(value)
      );
    }

    if (ky === 'ssn') {
      filterData = accountListData.filter((item: any) =>
        item?.ssn.includes(value.slice(7))
      );
    }

    if (ky === 'debtorName') {
      filterData = accountListData.filter((item: any) =>
        item?.debtorName?.toLowerCase().includes(value.toLowerCase())
      );
    }
    const sliceData = filterData.slice(
      startSequence === 0 ? 0 : startSequence - 1,
      endSequence
    );

    const result = sliceData.map(item => {
      const accountOfGroup = accountGroupListData.find(
        (obj: IDataGroupList) =>
          obj.accNumber.accountNumber === item.accountNumber
      );

      let accGroupList = [];

      if (accountOfGroup) {
        accGroupList = accountGroupListData.filter(
          (obj: IDataGroupList) => obj.groupId === accountOfGroup.groupId
        );
      }

      return {
        ...item,
        groupId: accountOfGroup?.groupId,
        numberOfGroup: accGroupList?.length
      };
    });

    return Promise.resolve({ data: { success: true, data: result } });
  },
  saveAccount: (args: ISaveAccountArgs) => {
    const { id, values } = args;

    if (isHaveError) {
      return Promise.reject({});
    }

    const { accountNumber, originalLoanNbr, statusType } = values;

    const maskedAccountNumber = maskedNumber({ str: accountNumber });
    const maskedOriginalLoanNumber = maskedNumber({
      str: originalLoanNbr
    });
    const accountModel = {
      ...values,
      accNumber: {
        accountNumber: maskedAccountNumber,
        accountNumberUnMask: accountNumber
      },
      accountNumber: accountNumber,
      originalLoan: {
        accountNumber: maskedOriginalLoanNumber,
        accountNumberUnMask: originalLoanNbr
      },
      status: statusType === 'B' ? 'purged' : 'active'
    };

    const { data } = id
      ? accountRepository.update(id, accountModel)
      : accountRepository.add(accountModel);

    return Promise.resolve({
      data: { success: true, data }
    });
  }
};
