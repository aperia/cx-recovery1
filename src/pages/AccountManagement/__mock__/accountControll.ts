import {
  IAccounts,
  IChangeManuals,
  IManuals
} from 'pages/AccountManagement/types';
import manualAssignRecallList from './manualAssignRecallList.json';
import accountList from './accountList.json';

const isHaveError = window.location.href.includes('error');

let manualAssignRecallData: IManuals[] = JSON.parse(
  JSON.stringify(manualAssignRecallList)
);

const accountListData: IAccounts[] = JSON.parse(JSON.stringify(accountList));

export const accountController = {
  getManualAssignRecall() {
    return Promise.resolve({
      data: { success: true, data: manualAssignRecallData }
    });
  },
  searchAccountByAcc({ accNumber }: { accNumber: string }) {
    try {
      const searchData = accountListData.filter(
        (item: IAccounts) => item.accNumber.accountNumberUnMask === accNumber
      );
      return Promise.resolve({
        data: { success: true, data: searchData[0] }
      });
    } catch (error) {
      return Promise.reject({
        data: { success: false, message: 'error' }
      });
    }
  },
  addManualAssignRecall({
    accNumber,
    accountData
  }: {
    accNumber: string;
    accountData: IChangeManuals;
  }) {
    try {
      const currentData = accountListData.filter(
        (item: IAccounts) => item.id === accNumber
      )[0];

      if (isHaveError)
        return Promise.reject({
          data: {
            success: false,
            message: 'Manual assign/recall request failed to add.'
          }
        });

      // Case Add
      manualAssignRecallData = [
        ...manualAssignRecallData,
        {
          ...currentData,
          ...accountData,
          id: Date.now().toString()
        }
      ];

      return Promise.resolve({
        data: { success: true, data: manualAssignRecallData }
      });
    } catch (error) {
      return Promise.reject({
        data: { success: false, message: 'error' }
      });
    }
  }
};
