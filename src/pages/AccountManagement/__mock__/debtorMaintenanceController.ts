// mocks
import data from './debtorList.json';
import addressHistory from './addressHistory.json';

// types
import {
  IAccounts,
  IAddressHistory,
  IDeleteDebtorArgs
} from 'pages/AccountManagement/types';
import { ISaveDebtorArgs } from './../_redux/debtorMaintenance/saveDebtor';

// helpers
import { Repository } from 'app/utils/repositories/concretes/Repository';

const debtorRepository = new Repository<IAccounts>(
  JSON.parse(JSON.stringify(data))
);

const addressHistoryRepository = new Repository<IAddressHistory>(
  JSON.parse(JSON.stringify(addressHistory))
);

const isHaveError = window.location.href.includes('error');
const isHaveEmpty = window.location.href.includes('empty');

export const debtorMaintenanceController = {
  getDebtor() {
    const { data } = debtorRepository.getAll();

    return Promise.resolve({
      data: { success: true, data: isHaveEmpty ? [] : data }
    });
  },
  getAddressHistory() {
    const { data } = addressHistoryRepository.getAll();
    return Promise.resolve({
      data: { success: true, data }
    });
  },
  deleteDebtor: ({ id }: IDeleteDebtorArgs) => {
    if (isHaveError) {
      return Promise.reject({});
    }

    const { data } = debtorRepository.delete(id);

    return Promise.resolve({
      data: { success: true, data }
    });
  },
  saveDebtor: (args: ISaveDebtorArgs) => {
    const { id, values } = args;

    if (isHaveError) {
      return Promise.reject({});
    }

    const { data } = id
      ? debtorRepository.update(id, values)
      : debtorRepository.add(values);

    return Promise.resolve({
      data: { success: true, data }
    });
  }
};
