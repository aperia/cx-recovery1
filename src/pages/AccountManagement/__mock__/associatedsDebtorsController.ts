import { ILinkDebtor } from '../types';
import associatedsDebtors from './associatedsDebtors.json';

let associatedsDebtorsData = JSON.parse(JSON.stringify(associatedsDebtors));

const TIME_DELAY = 600;
const isHaveError = window.location.href.includes('error');
const isHaveEmpty = window.location.href.includes('empty');

export const associatedDebtorController = {
  getAssociatedDebtors({
    page,
    pageSize = 5
  }: {
    page: number;
    pageSize?: number;
  }) {
    const ITEMS_PER_PAGE = pageSize;
    const totalItem = associatedsDebtorsData.length;

    let startIndex = (page - 1) * ITEMS_PER_PAGE;
    let endIndex = startIndex + (ITEMS_PER_PAGE - 1);

    const primaryDebtor = associatedsDebtorsData.find(
      (item: ILinkDebtor) => item.relation === 'A'
    );

    associatedsDebtorsData = associatedsDebtorsData.filter(
      (item: ILinkDebtor) => item.id !== primaryDebtor.id
    );

    associatedsDebtorsData = associatedsDebtorsData.sort(
      (first: ILinkDebtor, second: ILinkDebtor) =>
        first.debtorName.localeCompare(second.debtorName) // Sort default by alphabet debtorName
    );
    associatedsDebtorsData = [primaryDebtor, ...associatedsDebtorsData]; // display in the top of Associated Debtor List

    let listData = associatedsDebtorsData.slice(startIndex, endIndex + 1);

    if (listData.length === 0) {
      startIndex = (page - 2) * ITEMS_PER_PAGE;
      endIndex = startIndex + (ITEMS_PER_PAGE - 1);
      listData = associatedsDebtorsData.slice(startIndex, endIndex + 1);
    }
    const currentPage = Math.ceil((startIndex - 1) / ITEMS_PER_PAGE + 1);

    return new Promise<any>(function (resolve, reject) {
      setTimeout(() => {
        resolve({
          data: {
            success: true,
            associatedsDebtors: isHaveEmpty ? [] : listData,
            allAssociatedsDebtors: associatedsDebtorsData,
            totalItem,
            currentPage
          }
        });
      }, TIME_DELAY);
    });
  },
  addLinkDebtor(linkDebtor: ILinkDebtor) {
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: 'ADD FAILED'
      });
    const indexOfExistData = associatedsDebtorsData.findIndex(
      (item: ILinkDebtor) =>
        item.relation === linkDebtor.relation &&
        (linkDebtor.relation === 'L' || linkDebtor.relation === 'S')
    );

    if (indexOfExistData !== -1) {
      associatedsDebtorsData = associatedsDebtorsData.filter(
        (item: ILinkDebtor) =>
          item.id !== associatedsDebtorsData[indexOfExistData]?.id
      );
    }

    if (linkDebtor.relation === 'A') {
      associatedsDebtorsData = [
        {
          ...linkDebtor,
          id: Date.now().toString()
        }
      ];

      return Promise.resolve({
        success: true
      });
    }
    associatedsDebtorsData = [
      ...associatedsDebtorsData,
      {
        ...linkDebtor,
        id: Date.now().toString()
      }
    ];
    return Promise.resolve({
      success: true
    });
  },
  editLinkDebtor(id: string, dataChange: ILinkDebtor) {
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: 'EDIT FAILED'
      });

    const indexOfExistData = associatedsDebtorsData.findIndex(
      (item: ILinkDebtor) =>
        item.relation === dataChange.relation &&
        (dataChange.relation === 'L' || dataChange.relation === 'S')
    );

    const indexOfDataChange = associatedsDebtorsData.findIndex(
      (item: ILinkDebtor) => item.id === id
    );

    if (
      indexOfExistData !== -1 &&
      dataChange.relation !==
        associatedsDebtorsData[indexOfDataChange]?.relation
    ) {
      associatedsDebtorsData = associatedsDebtorsData.filter(
        (item: ILinkDebtor) =>
          item.id !== associatedsDebtorsData[indexOfExistData]?.id
      );
    }

    associatedsDebtorsData = [
      ...associatedsDebtorsData.slice(0, indexOfDataChange),
      { id, ...dataChange },
      ...associatedsDebtorsData.slice(indexOfDataChange + 1)
    ];
    return Promise.resolve({
      success: true
    });
  },
  deleteLinkDebtor(id: string) {
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: 'DELETE FAILED'
      });
    associatedsDebtorsData = associatedsDebtorsData.filter(
      (item: ILinkDebtor) => item.id !== id
    );
    return Promise.resolve({ success: true, data: associatedsDebtorsData });
  }
};
