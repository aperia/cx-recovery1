import { I18N_TEXT } from 'app/constants';
import React from 'react';
// component
import AccountList from '../AccountList';

interface SearchResultProps {}

const SearchResult: React.FC<SearchResultProps> = () => {
  return (
    <AccountList
      atScreen={'accountMaintenance'}
      title={I18N_TEXT.SEARCH_RESULT}
    />
  );
};

export default SearchResult;
