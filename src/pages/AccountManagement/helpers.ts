import { IAccounts } from './types';
import { checkEmpty, orderBy, isUndefined, isEmpty } from 'app/helpers';

export const filterAndOrderAccounts = (
  data: IAccounts[],
  status: string,
  sort: string,
  order: string
) => {
  if (!data.length) [];

  const filterData = data.filter(r => r.status === status);

  if (!checkEmpty([filterData, false])) return [];

  return orderBy(filterData, sort, order as 'asc' | 'desc');
};

export const checkNoneValue = (
  currentData: string,
  options?: RefDataValue[]
) => {
  if (isUndefined(options)) return [];

  return isEmpty(currentData)
    ? options
    : options.filter(item => item.description !== 'None');
};

export const returnEmptyValue = (value: string) =>
  value === '' ? 'None' : value;

export const getPOICFromReserveCode = (
  reservePOICOptions: MagicKeyValue[],
  reserveCode?: string
): string => {
  if (!reserveCode) return '';

  if (!reservePOICOptions.length) return '';

  const poicDictionary = {
    CCCP: 'P',
    IINT: 'I',
    PCCP: 'P',
    PINT: 'I'
  };

  const item = reservePOICOptions.find(
    item =>
      item.value === poicDictionary[reserveCode as keyof typeof poicDictionary]
  );

  if (!item) return '';

  return item.description;
};
export const genMashAccNumber = (accNumber: string) => {
  if (!accNumber) return '';
  return (
    accNumber.slice(0, 6) + '••••••' + accNumber.slice(accNumber.length - 4)
  );
};

export const genPIID = () => {
  return Date.now().toString() + Math.round(Math.random() * 1000).toString();
};
