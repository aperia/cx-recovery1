// components
import AssociatedDebtors from 'pages/AccountManagement/components/AssociatedDebtors';
import { I18N_TEXT } from 'app/constants';
import BankruptcyDeceaseRecord from './BankruptcyDeceaseRecord';
import Overview from './Overview';
import AgencyInfo from './Overview/AgencyInfo';
import FinancialInfo from './Overview/FinancialInfo';
import GeneralInfo from './Overview/GeneralInfo';
import MiscellaneousInfo from './Overview/MiscellaneousInfo';
import PaymentInfo from './Overview/PaymentInfo';

import FinancialInformation from './FinacialInfomation';
import DemoGraphics from './FinacialInfomation/DemoGraphics';
import Statement from './FinacialInfomation/Statement';
import Creditor from './FinacialInfomation/Creditor';
import SuitInformation from './SuitInformation';


// types
import { IVerticalTabOverview } from './types';


export const tabList = [
  {
    key: 'Overview',
    eventKey: 'Overview',
    title: 'txt_overview',
    component: Overview
  },
  {
    key: 'AssociatedDebtors',
    eventKey: 'AssociatedDebtors',
    title: 'txt_associated_debtors',
    component: AssociatedDebtors
  },
  {
    key: 'bankruptcyDeceaseRecord',
    eventKey: 'bankruptcyDeceaseRecord',
    title: I18N_TEXT.BANKRUPTCY_DECEASE,
    component: BankruptcyDeceaseRecord
  },
  {
    key: 'financialInformation',
    eventKey: 'financialInformation',
    title: I18N_TEXT.FINANCIAL_INFORMATION,
    component: FinancialInformation
  },
  {
    key: 'suitInformation',
    eventKey: 'suitInformation',
    title: I18N_TEXT.SUIT_INFORMATION,
    component: SuitInformation
  }
];

export const VerticalTabOverview: IVerticalTabOverview[] = [
  {
    title: 'txt_general_info',
    component: GeneralInfo,
    id: 'generalInfo'
  },
  {
    title: 'txt_agency_info',
    component: AgencyInfo,
    id: 'agencyInfo'
  },
  {
    title: 'txt_payment_info',
    component: PaymentInfo,
    id: 'paymentInfo'
  },
  {
    title: 'txt_financial_info',
    component: FinancialInfo,
    id: 'financialInfo'
  },
  {
    title: 'txt_miscellaneous_info',
    component: MiscellaneousInfo,
    id: 'miscellaneousInfo'
  }
];

export const tabFinancialList = [
  {
    key: 'Demographics',
    eventKey: 'Demographics',
    title: 'txt_demographics',
    component: DemoGraphics
  },
  {
    key: 'Statement',
    eventKey: 'Statement',
    title: 'txt_statement',
    component: Statement
  },
  {
    key: 'Creditor',
    eventKey: 'Creditor',
    title: 'txt_creditor',
    component: Creditor
  }
]