import React, { useMemo, useState, useEffect, useCallback } from 'react';
import { batch, useDispatch } from 'react-redux';
import { isEqual } from 'app/helpers';

// hook
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { takeTabData } from 'pages/__commons/TabBar/_redux/selectors';

// redux
import {
  selectorBaseAccount,
  selectorCalendarCode,
  selectorCurrentQueueId,
  selectorCurrentAction
} from 'pages/QueueDetail/_redux/accountList/selectors';
import { getAccountDetail } from '../_redux/selectors';
import { queueDetailActions } from 'pages/QueueDetail/_redux';
import { tabActions } from 'pages/__commons/TabBar/_redux';

// component
import FloatingDropdownButton from 'app/_libraries/_dls/components/FloatingDropdownButton';

// types & constant
import { DEFAULT_NEXT_ACTIONS, prepareButtons } from './constants';
import { useSelector, useSelectorId } from 'app/hooks';
import { I18N_TEXT } from 'app/constants';
import { IAccounts } from 'pages/AccountManagement/types';
import { QueueAccount, WorkQueueType } from 'pages/AccountActivity/types';
import { TabState } from 'pages/__commons/TabBar/types';
import { STORE_ID_QUEUE_LIST } from 'pages/AccountActivity/constants';

export interface NextActionsProps {}

const NextActions: React.FC<NextActionsProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const defaultActions = useMemo(() => {
    return prepareButtons(DEFAULT_NEXT_ACTIONS, t);
  }, [t]);

  const { tabs } = useSelectorId<TabState>(takeTabData);

  const queueId: string = useSelectorId(selectorCurrentQueueId) || '';
  const calendarCode: string | undefined = useSelectorId(selectorCalendarCode);
  const currentAction: string | undefined = useSelectorId(
    selectorCurrentAction
  );
  const baseAccount: string | undefined = useSelector(states => {
    if (!queueId) return undefined;
    return selectorBaseAccount(states, queueId);
  });
  const dataSnapshot = useSelectorId(getAccountDetail);

  const [action, setAction] = useState<string>(defaultActions[0].value);
  const [actionList, setActionList] = useState<RefDataValue[]>(defaultActions);

  const dropdownItems = useMemo(
    () =>
      actionList.map((item, index: number) => {
        return (
          <FloatingDropdownButton.Item
            key={index}
            label={item.description}
            value={item.value}
          />
        );
      }),
    [actionList]
  );

  useEffect(() => {
    if (!currentAction) return;
    setAction(currentAction);
  }, [currentAction]);

  useEffect(() => {
    if (queueId) {
      let buttons = [...DEFAULT_NEXT_ACTIONS] as string[];

      if (
        baseAccount &&
        baseAccount !== dataSnapshot?.accNumber?.accountNumberUnMask
      ) {
        buttons = [
          I18N_TEXT.NEXT_ACCOUNT,
          I18N_TEXT.PREVIOUS_ACCOUNT,
          I18N_TEXT.QUEUE_DETAILS,
          ...DEFAULT_NEXT_ACTIONS
        ];
      } else {
        buttons = [
          I18N_TEXT.NEXT_ACCOUNT,
          I18N_TEXT.QUEUE_DETAILS,
          ...DEFAULT_NEXT_ACTIONS
        ];
      }

      const newActionList: RefDataValue[] = buttons.map(value => ({
        value: value,
        description: t(value)
      }));

      if (!isEqual(actionList, newActionList)) {
        setActionList(newActionList);
        setAction(newActionList[0].value);
      }
    }
  }, [t, actionList, baseAccount, dataSnapshot, action, queueId]);

  const handleOpenAccountDetail = useCallback(
    (account: IAccounts) => {
      if (!account) return;
      const { id: storeId, accountNumber, debtorName } = account;

      const storedIdExits = tabs.findIndex(
        obj => obj.storeId === storeId && dataSnapshot.id !== storeId
      );

      if (storedIdExits > -1) {
        return dispatch(
          tabActions.removeTab({
            storeId: dataSnapshot.id
          })
        );
      }

      dispatch(
        tabActions.updateTabPops({
          title: `${debtorName} - ${accountNumber}`,
          storeId: dataSnapshot.id,
          newStoreId: storeId,
          props: {
            storeId: storeId,
            debtorName,
            accountNumber
          }
        })
      );
    },
    [dispatch, tabs, dataSnapshot]
  );

  const handleChangeAccount = useCallback(
    (workType: WorkQueueType) => {
      if (!queueId) return;
      const requestData = {
        params: {
          queueId: queueId,
          workType,
          calendarCode: calendarCode,
          currentAccount: dataSnapshot?.accNumber?.accountNumberUnMask
        },
        callback: handleOpenAccountDetail
      };
      dispatch(queueDetailActions.getAccountToWork(requestData));
    },
    [dispatch, queueId, calendarCode, dataSnapshot, handleOpenAccountDetail]
  );

  const handleOpenQueueList = () => {
    batch(() => {
      dispatch(
        tabActions.removeTab({
          storeId: dataSnapshot.id
        })
      );
      dispatch(
        tabActions.addTab({
          title: t(I18N_TEXT.QUEUE_LIST),
          storeId: STORE_ID_QUEUE_LIST,
          tabType: 'queueList',
          iconName: 'file'
        })
      );
    });
  };

  const handleOpenSearchPage = () => {
    batch(() => {
      dispatch(
        tabActions.removeTab({
          storeId: dataSnapshot.id
        })
      );

      dispatch(
        tabActions.addTab({
          title: t('txt_home'),
          storeId: 'home',
          tabType: 'home',
          iconName: 'file'
        })
      );
    });
  };

  const handleCallbackOpenQueueDetail = useCallback(
    (account: IAccounts, detail?: QueueAccount | null) => {
      if (!account) return;
      if (!detail) return;
      const { id } = account;
      const storeId = `QUEUE_DETAIL_${id}`;
      batch(() => {
        dispatch(
          tabActions.removeTab({
            storeId: dataSnapshot.id
          })
        );
        dispatch(
          tabActions.addTab({
            title: `${queueId} - ${account.debtorName}`,
            storeId,
            tabType: 'queueDetail',
            iconName: 'file',
            props: {
              queueDetail: detail
            }
          })
        );
      });
    },
    [dispatch, dataSnapshot, queueId]
  );

  const handleOpenQueueDetail = useCallback(() => {
    if (!queueId) return;
    const requestData = {
      params: {
        queueId: queueId,
        workType: 'current_account' as WorkQueueType,
        currentAccount: dataSnapshot?.accNumber?.accountNumberUnMask
      },
      callback: handleCallbackOpenQueueDetail
    };
    dispatch(queueDetailActions.getAccountToWork(requestData));
  }, [dispatch, queueId, dataSnapshot, handleCallbackOpenQueueDetail]);

  const handleOnClick = () => {
    switch (action) {
      case I18N_TEXT.NEXT_ACCOUNT:
        handleChangeAccount('next_account');
        break;
      case I18N_TEXT.PREVIOUS_ACCOUNT:
        handleChangeAccount('previous_account');
        break;
      case I18N_TEXT.QUEUE_LIST:
        handleOpenQueueList();
        break;
      case I18N_TEXT.SEARCH_PAGE:
        handleOpenSearchPage();
        break;
      case I18N_TEXT.QUEUE_DETAILS:
        handleOpenQueueDetail();
        break;
      default:
        break;
    }
  };

  const handleChangeDropdown = (event: DropdownBaseChangeEvent) => {
    const actionValue = event.target.value;

    setAction(actionValue);
    dispatch(queueDetailActions.onChangeCurrentAction(actionValue));
  };

  return (
    <FloatingDropdownButton
      value={action}
      textField="value"
      textFieldRender={itemValue => t(itemValue)}
      popupBaseProps={{
        popupBaseClassName: 'inside-infobar'
      }}
      onChange={handleChangeDropdown}
      onClickNavigation={handleOnClick}
    >
      {dropdownItems}
    </FloatingDropdownButton>
  );
};

export default NextActions;
