import { I18N_TEXT } from 'app/constants';

export const DEFAULT_NEXT_ACTIONS: string[] = [
  I18N_TEXT.QUEUE_LIST,
  I18N_TEXT.SEARCH_PAGE
];

export const prepareButtons = (buttons: string[], t: any): RefDataValue[] => {
  return buttons.map(value => ({ value: value, description: t(value) }));
};
