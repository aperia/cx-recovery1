import { createAsyncThunk } from '@reduxjs/toolkit';
import { GET_TRANSACTION_LIST } from '../constants';
import { IGetTransactionListPayload } from '../types';

export const getTransactionList = createAsyncThunk<
  IGetTransactionListPayload,
  undefined,
  ThunkAPIConfig
>(GET_TRANSACTION_LIST, async _ => {
  return {
    data: []
  };
});
