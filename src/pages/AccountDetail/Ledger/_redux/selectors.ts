import { createSelector } from 'reselect';
import { DEFAULT_FILTER_DATA } from '../constants';
import { ILedgerState } from '../types';

const getLedgerState = (states: RootState, storeId: string) =>
  states.ledger[storeId];

export const selectLedgerShow = createSelector(
  getLedgerState,
  (data: ILedgerState) => data?.show || false
);

export const selectLedgerFilterData = createSelector(
  getLedgerState,
  (data: ILedgerState) => data?.filterData || DEFAULT_FILTER_DATA
);

export const selectLedgerFilterShow = createSelector(
  getLedgerState,
  (data: ILedgerState) => data?.filterShow || false
);
