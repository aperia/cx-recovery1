import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { set } from 'lodash';
import { initialState } from '../constants';
import { IFilterData } from '../types';

const { actions, reducer } = createSlice({
  name: 'ledger',
  initialState: initialState,
  reducers: {
    toggleShow: (draftState, action: PayloadAction<{ storeId: string }>) => {
      const { storeId } = action.payload;
      set(draftState, `${storeId}.show`, !draftState[storeId]?.show);
    },
    setFilter: (
      draftState,
      action: PayloadAction<IFilterData & { storeId: string }>
    ) => {
      const { storeId, ...payload } = action.payload;
      set(draftState, `${storeId}.filterData`, payload);
    },
    setFilterShow: (
      draftState,
      action: PayloadAction<{ show: boolean; storeId: string }>
    ) => {
      const { storeId, show } = action.payload;
      set(draftState, `${storeId}.filterShow`, show);
    },
    toggleFilter: (draftState, action: PayloadAction<{ storeId: string }>) => {
      const { storeId } = action.payload;
      set(
        draftState,
        `${storeId}.filterShow`,
        !draftState[storeId]?.filterShow
      );
    }
  }
});

const combineActions = {
  ...actions
};

export { combineActions as ledgerActions, reducer };
