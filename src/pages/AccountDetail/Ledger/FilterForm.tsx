import {
  CheckboxControl,
  DatePickerControl,
  TextBoxControl
} from 'app/components';
import { I18N_TEXT } from 'app/constants';
import {
  Button,
  DateRangePicker,
  DateRangePickerValue,
  Radio
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { FormikProvider } from 'formik';
import React from 'react';
import { useLedgerTransactionFilterForm } from './helpers';

const FilterForm: React.FC = () => {
  const { t } = useTranslation();
  const formik = useLedgerTransactionFilterForm();
  const { values, setFieldValue, handleChange, submitForm, handleReset } =
    formik;

  return (
    <FormikProvider value={formik}>
      <h4>{t(I18N_TEXT.FILTER)}</h4>
      <div className="d-flex mt-16">
        <p className="fw-500 color-grey">{t(I18N_TEXT.TRANS_TYPE)}:</p>
        <CheckboxControl
          name="charge"
          label={t(I18N_TEXT.CHARGE)}
          className="ml-24"
        />
        <CheckboxControl
          name="recovery"
          label={t(I18N_TEXT.RECOVERY)}
          className="ml-24"
        />
      </div>
      <div className="d-flex mt-16">
        <p className="fw-500 color-grey">{t(I18N_TEXT.TRANS_DATE)}:</p>
        <div className="ml-24">
          <Radio className="d-inline-block mr-24">
            <Radio.Input
              checked={values.transDateType === 'Y'}
              onChange={() => setFieldValue('transDateType', 'Y')}
            />
            <Radio.Label>{t(I18N_TEXT.SPECIFIC_DATE)}</Radio.Label>
          </Radio>

          <Radio className="d-inline-block">
            <Radio.Input
              checked={values.transDateType === 'N'}
              onChange={() => setFieldValue('transDateType', 'N')}
            />
            <Radio.Label>{t(I18N_TEXT.DATE_RANGE)}</Radio.Label>
          </Radio>
        </div>
      </div>
      <div className="mt-12">
        {values.transDateType === 'Y' ? (
          <DatePickerControl label={t(I18N_TEXT.DATE)} name="transDate" />
        ) : (
          <DateRangePicker
            name="transDate"
            label={t(I18N_TEXT.DATE_RANGE)}
            onChange={handleChange}
            value={values.transDate as DateRangePickerValue}
          />
        )}
      </div>
      <TextBoxControl
        className="mt-16"
        name="description"
        maxLength={25}
        label={t(I18N_TEXT.TRANSACTION_DESCRIPTION)}
      />
      <div className="d-flex mt-24">
        <Button
          variant="secondary"
          size="sm"
          className="ml-auto"
          onClick={handleReset}
        >
          {t(I18N_TEXT.RESET_TO_DEFAULT)}
        </Button>
        <Button variant="primary" size="sm" onClick={submitForm}>
          {t(I18N_TEXT.APPLY)}
        </Button>
      </div>
    </FormikProvider>
  );
};

export default FilterForm;
