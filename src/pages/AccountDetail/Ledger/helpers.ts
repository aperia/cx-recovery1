import { formatTime } from 'app/helpers';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { useFormik } from 'formik';
import { isDate, isEmpty, isEqual, isUndefined } from 'lodash';
import { batch, useDispatch } from 'react-redux';
import { DEFAULT_FILTER_DATA } from './constants';
import { IFilterData, ITransaction } from './types';
import { ledgerActions } from './_redux';
import { selectLedgerFilterData } from './_redux/selectors';

export const useLedgerTransactionFilterForm = () => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const filterData = useSelectorId(selectLedgerFilterData);

  const handleSubmit = (values: IFilterData) => {
    batch(() => {
      dispatch(ledgerActions.setFilter({ ...values, storeId }));
      dispatch(ledgerActions.toggleFilter({ storeId }));
    });
  };

  const handleReset = () => {
    setValues(DEFAULT_FILTER_DATA);
  };

  const formik = useFormik<IFilterData>({
    initialValues: filterData,
    enableReinitialize: true,
    onSubmit: handleSubmit
  });

  const { setValues } = formik;

  return {
    ...formik,
    handleReset
  };
};

export const filterLedgerTransaction = (
  data: ITransaction[],
  filter: IFilterData
) => {
  if (isEqual(filter, DEFAULT_FILTER_DATA)) return data;
  return data.filter((item: ITransaction) => {
    const { recovery, charge, description, transDate } = filter;
    // filter description
    if (
      !isEmpty(description) &&
      !isEqual(item.description.toLowerCase(), description?.toLowerCase())
    ) {
      return false;
    }
    // filter date
    if (!isUndefined(transDate)) {
      if (isDate(transDate)) {
        if (
          formatTime(item.transDate.toString()).date !==
          formatTime(transDate.toString()).date
        )
          return false;
      } else if (isDate(transDate.start) && isDate(transDate.end)) {
        const startDate = formatTime(transDate.start.toString()).date;
        const endDate = formatTime(transDate.end.toString()).date;
        if (
          new Date(item.transDate) < new Date(`${startDate} 00:00:00`) ||
          new Date(item.transDate) > new Date(`${endDate} 23:59:59`)
        )
          return false;
      }
    }
    if (recovery) {
      if (item.transType === 'Recovery') {
        return true;
      }
    }
    if (charge) {
      if (item.transType === 'Charge') {
        return true;
      }
    }
    return false;
  });
};

export const calculateStatistic = (data: ITransaction[]) => {
  if (isEmpty(data))
    return {
      totalCharges: '0',
      totalRecoveries: '0',
      totalRemainingBalance: '0'
    };
  const totalCharges = data
    .reduce((prev, current) => {
      if (current.transType === 'Charge') return prev + sumTransAmount(current);
      return prev;
    }, 0)
    .toString();
  const totalRecoveries = data
    .reduce((prev, current) => {
      if (current.transType === 'Recovery')
        return prev + sumTransAmount(current);
      return prev;
    }, 0)
    .toString();
  return {
    totalCharges,
    totalRecoveries,
    totalRemainingBalance: (
      Number(totalCharges) - Number(totalRecoveries)
    ).toString()
  };
};

export const sumTransAmount = (data: ITransaction) =>
  Number(data.agencyFee || 0) +
  Number(data.principal || 0) +
  Number(data.interest || 0) +
  Number(data.otherIncome || 0) +
  Number(data.costs || 0);
