import React, { useMemo } from 'react';
import { I18N_TEXT } from 'app/constants';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { Button, Icon, Popover } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import classNames from 'classnames';
import { isEmpty, isEqual } from 'lodash';
import { useDispatch } from 'react-redux';
import { DEFAULT_FILTER_DATA } from './constants';
import FilterForm from './FilterForm';
import { ledgerActions } from './_redux';
import {
  selectLedgerFilterData,
  selectLedgerFilterShow
} from './_redux/selectors';
import If from 'pages/__commons/If';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import { ITransaction } from './types';
import { filterLedgerTransaction } from './helpers';

const LedgerTransactionFilter: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  const filterData = useSelectorId(selectLedgerFilterData);
  const filterShow = useSelectorId(selectLedgerFilterShow);
  const { data: transactions } = useGetRefDataQuery('transactionList');
  const transactionData = useMemo(
    () => (transactions?.transactionList || []) as unknown as ITransaction[],
    [transactions?.transactionList]
  );
  const filterTransactions = useMemo(
    () => filterLedgerTransaction(transactionData, filterData),
    [filterData, transactionData]
  );

  const handlePopoverVisibility = (isOpened: boolean) => {
    dispatch(
      ledgerActions.setFilterShow({
        show: isOpened,
        storeId
      })
    );
  };

  const handleClearSearch = () => {
    dispatch(
      ledgerActions.setFilter({
        ...DEFAULT_FILTER_DATA,
        storeId
      })
    );
  };

  return (
    <div className="text-right">
      <div>
        <Popover
          size="md"
          placement="bottom"
          opened={filterShow}
          onVisibilityChange={handlePopoverVisibility}
          element={<FilterForm />}
          triggerClassName="mr-n8"
        >
          <Button
            size="sm"
            variant="outline-primary"
            onClick={_ => handlePopoverVisibility(!filterShow)}
            className={classNames(filterShow && 'active')}
          >
            {!isEqual(filterData, DEFAULT_FILTER_DATA) && (
              <div className="filter-button" />
            )}
            <Icon name="filter" />
            <span className="ml-4">{t(I18N_TEXT.FILTER)}</span>
          </Button>
        </Popover>
      </div>
      <If
        condition={
          !isEqual(filterData, DEFAULT_FILTER_DATA) &&
          !isEmpty(filterTransactions)
        }
      >
        <Button
          size="sm"
          variant="outline-primary"
          className="mr-n8 mt-16"
          onClick={handleClearSearch}
        >
          {t(I18N_TEXT.CLEAR_AND_RESET)}
        </Button>
      </If>
    </div>
  );
};

export default LedgerTransactionFilter;
