import { I18N_TEXT } from 'app/constants';
import { Button, Popover } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import classNames from 'classnames';
import React, { useMemo, useState } from 'react';
import { LEDGER_DETAIL } from './constants';

const Detail: React.FC<{ data: MagicKeyValue }> = ({ data }) => {
  const { t } = useTranslation();
  const [openPopover, setOpenPopover] = useState(false);

  const DetailData = useMemo(
    () => (
      <div className="mt-n8">
        {LEDGER_DETAIL.map(item => (
          <div className="d-flex mt-8" key={item.value}>
            <div className="w-45 fw-500 mr-16">{t(item.description)}</div>
            <div className="w-55">
              <span className="form-group-static__text">
                {data[item.value]}
              </span>
            </div>
          </div>
        ))}
      </div>
    ),
    [data, t]
  );

  return (
    <Popover
      placement="bottom"
      opened={openPopover}
      onVisibilityChange={setOpenPopover}
      element={DetailData}
      size="lg"
    >
      <Button
        variant="outline-primary"
        onClick={_ => setOpenPopover(!openPopover)}
        className={classNames(openPopover && 'active')}
        size="sm"
      >
        {t(I18N_TEXT.VIEW)}
      </Button>
    </Popover>
  );
};

export default Detail;
