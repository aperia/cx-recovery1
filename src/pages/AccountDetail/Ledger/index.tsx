import { SearchNodata } from 'app/components';
import { I18N_TEXT } from 'app/constants';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { usePagination } from 'app/hooks/usePagination';
import {
  ColumnType,
  Grid,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  Pagination,
  SimpleBar
} from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { View } from 'app/_libraries/_dof/core';
import { isEmpty } from 'lodash';
import If from 'pages/__commons/If';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import React, { useMemo } from 'react';
import { useDispatch } from 'react-redux';
import {
  DEFAULT_FILTER_DATA,
  getTransactionListColumns,
  LEDGER_INFO,
  LEDGER_TRANSACTION_STATISTIC,
  MOCK_LEDGER_INFO
} from './constants';
import LedgerTransactionFilter from './Filter';
import { calculateStatistic, filterLedgerTransaction } from './helpers';
import { ITransaction } from './types';
import { ledgerActions } from './_redux';
import { selectLedgerFilterData, selectLedgerShow } from './_redux/selectors';

const Ledger: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const show: boolean = useSelectorId(selectLedgerShow);
  const columns: ColumnType<ITransaction>[] = getTransactionListColumns(t);
  const { data: transactions } = useGetRefDataQuery('transactionList');
  const transactionData = useMemo(
    () => (transactions?.transactionList || []) as unknown as ITransaction[],
    [transactions?.transactionList]
  );
  const filter = useSelectorId(selectLedgerFilterData);
  const filterTransactions = useMemo(
    () => filterLedgerTransaction(transactionData, filter),
    [filter, transactionData]
  );

  const handleToggleModal = () => {
    dispatch(ledgerActions.toggleShow({ storeId }));
    if (!show) {
      handleClearSearch();
    }
  };

  const handleClearSearch = () => {
    dispatch(
      ledgerActions.setFilter({
        ...DEFAULT_FILTER_DATA,
        storeId
      })
    );
  };

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(filterTransactions);

  return (
    <Modal show={show} full enforceFocus={false}>
      <ModalHeader closeButton border onHide={handleToggleModal}>
        <ModalTitle>{t(I18N_TEXT.LEDGER)}</ModalTitle>
      </ModalHeader>
      <ModalBody className="p-0">
        <SimpleBar>
          <div className="bg-light-l20 border-bottom">
            <div className="p-24 max-width-lg mx-auto">
              <View
                id={LEDGER_INFO}
                formKey={LEDGER_INFO}
                descriptor={LEDGER_INFO}
                value={MOCK_LEDGER_INFO}
              />
            </div>
          </div>
          <div className="p-24 max-width-lg mx-auto">
            <div className="d-flex align-items-center">
              <h5>{t(I18N_TEXT.TRANSACTION_LIST)}</h5>
              <div className="ml-auto">
                <LedgerTransactionFilter />
              </div>
            </div>
            <If condition={isEmpty(filterTransactions)}>
              <SearchNodata
                text={t('txt_filter_no_results_items_payment')}
                onClearSearch={handleClearSearch}
              />
            </If>
            <If condition={!isEmpty(filterTransactions)}>
              <div className="p-16 bg-light-l20 br-radius-8 border br-light-l04 mt-16">
                <View
                  id={LEDGER_TRANSACTION_STATISTIC}
                  formKey={LEDGER_TRANSACTION_STATISTIC}
                  descriptor={LEDGER_TRANSACTION_STATISTIC}
                  value={calculateStatistic(filterTransactions)}
                />
              </div>
              <Grid columns={columns} className="mt-16" data={gridData} />
            </If>
            <If condition={filterTransactions.length > 10}>
              <div className="pt-16">
                <Pagination
                  totalItem={total}
                  pageSize={pageSize}
                  pageNumber={currentPage}
                  pageSizeValue={currentPageSize}
                  compact
                  onChangePage={onPageChange}
                  onChangePageSize={onPageSizeChange}
                />
              </div>
            </If>
          </div>
        </SimpleBar>
      </ModalBody>
      <ModalFooter
        border
        cancelButtonText={t(I18N_TEXT.CLOSE)}
        onCancel={handleToggleModal}
      />
    </Modal>
  );
};

export default Ledger;
