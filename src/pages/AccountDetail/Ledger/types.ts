import { DateRangePickerValue } from 'app/_libraries/_dls/components';

export interface ILedgerInitialState {
  [storeId: string]: ILedgerState;
}

export interface ILedgerState {
  show: boolean;
  transactions: ITransaction[];
  filterData?: IFilterData;
  filterShow?: boolean;
}

export type ITransType = 'charge' | 'recovery';
export type ITransDateType = 'specific' | 'range';

export interface ITransaction {
  transDate: Date;
  description: string;
  transType: string;
  agencyFee: string;
  principal: string;
  interest: string;
  otherIncome: string;
  costs: string;
  totalTransAmount: string;
}

export interface IGetTransactionListPayload {
  data: ITransaction[];
}

export interface IFilterData {
  charge?: boolean;
  recovery?: boolean;
  transDateType?: string;
  transDate?: Date | DateRangePickerValue;
  description?: string;
}
