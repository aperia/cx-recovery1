import React from 'react';
import { I18N_TEXT } from 'app/constants';
import { Badge, ColumnType } from 'app/_libraries/_dls/components';
import { IFilterData, ILedgerInitialState, ITransaction } from './types';
import { formatCurrency } from 'app/helpers';
import Detail from './Detail';
import { sumTransAmount } from './helpers';

export const LEDGER_INFO = 'ledgerInfo';
export const LEDGER_TRANSACTION_STATISTIC = 'ledgerTransactionStatistic';
export const GET_TRANSACTION_LIST = 'getTransactionList';

export const initialState: ILedgerInitialState = {};

export const MOCK_LEDGER_INFO = {
  generalLedger: '1 - First Data Account',
  paymentSchedule: 'Y - Yes, the account has a payment schedule.',
  paymentScheduleAmount: '800',
  paymentScheduleStartDate: '02/20/2022'
};

export const MOCK_LEDGER_TRANSACTION_STATISTIC = {
  totalCharges: '25000',
  totalRecoveries: '16500',
  totalRemainingBalance: '8500'
};

export const MOCK_LEDGER_TRANSACTION_DETAIL = {
  transactionTypeCode: 'C - This is a charge',
  financialTransactionCode: 'CCCP - Charge-off Principal',
  reportingAgencyCode: 'FDR - First Data Resources',
  batchCreationDate: '07/21/2021',
  sendBatchDate: '',
  poic: 'P - Transaction posted to principal portion of the account’s outstanding balance'
};

export const DEFAULT_FILTER_DATA: IFilterData = {
  charge: true,
  recovery: true,
  transDateType: 'Y',
  description: '',
  transDate: undefined
};

export const TRANSACTION_TYPE_CODE: Record<string, Record<string, string>> = {
  payments: {
    D: 'Direct payment',
    G: 'Gross payment',
    N: 'Net payment',
    S: 'Gross payment from a service',
    T: 'Net payment from a service'
  },
  adjustments: {
    C: 'This is a charge',
    R: 'This is a recovery'
  },
  disbursements: {
    C: 'Cost is not due the agency of record',
    D: 'Cost is due the agency of record'
  }
};

export const getTransactionListColumns = (
  t: Function
): ColumnType<ITransaction>[] => [
  {
    id: 'transDate',
    Header: t(I18N_TEXT.TRANS_DATE),
    accessor: 'transDate',
    width: 115
  },
  {
    id: 'description',
    Header: t(I18N_TEXT.DESCRIPTION),
    accessor: 'description',
    width: 165,
    autoWidth: true
  },
  {
    id: 'transType',
    Header: t(I18N_TEXT.TRANS_TYPE),
    accessor: (data: ITransaction) => (
      <Badge
        color={data.transType.toLowerCase() === 'charge' ? 'orange' : 'green'}
      >
        {data.transType}
      </Badge>
    ),
    width: 108
  },
  {
    id: 'agencyFee',
    Header: t(I18N_TEXT.AGENCY_FEE),
    accessor: (data: ITransaction) =>
      data.agencyFee && formatCurrency(data.agencyFee, 2),
    width: 125,
    cellProps: { className: 'text-right' }
  },
  {
    id: 'principal',
    Header: t(I18N_TEXT.PRINCIPAL),
    accessor: (data: ITransaction) =>
      data.principal && formatCurrency(data.principal, 2),
    width: 125,
    cellProps: { className: 'text-right' }
  },
  {
    id: 'interest',
    Header: t(I18N_TEXT.INTEREST),
    accessor: (data: ITransaction) =>
      data.interest && formatCurrency(data.interest, 2),
    width: 125,
    cellProps: { className: 'text-right' }
  },
  {
    id: 'otherIncome',
    Header: t(I18N_TEXT.OTHER_INCOME),
    accessor: (data: ITransaction) =>
      data.otherIncome && formatCurrency(data.otherIncome, 2),
    width: 125,
    cellProps: { className: 'text-right' }
  },
  {
    id: 'costs',
    Header: t(I18N_TEXT.COSTS),
    accessor: (data: ITransaction) =>
      data.costs && formatCurrency(data.costs, 2),
    width: 125,
    cellProps: { className: 'text-right' }
  },
  {
    id: 'totalTransAmount',
    Header: t(I18N_TEXT.TOTAL_TRANS_AMOUNT),
    accessor: (data: ITransaction) =>
      formatCurrency(sumTransAmount(data).toString(), 2),
    isFixedRight: true,
    width: 125,
    cellProps: { className: 'text-right' }
  },
  {
    id: 'action',
    Header: t(I18N_TEXT.ACTION),
    accessor: _ => <Detail data={MOCK_LEDGER_TRANSACTION_DETAIL} />,
    isFixedRight: true,
    width: 87,
    cellProps: { className: 'text-center' },
    cellBodyProps: { className: 'td-sm' }
  }
];

export const LEDGER_DETAIL: RefDataValue[] = [
  {
    value: 'transactionTypeCode',
    description: I18N_TEXT.TRANSACTION_TYPE_CODE
  },
  {
    value: 'reportingAgencyCode',
    description: I18N_TEXT.REPORTING_AGENCY_CODE
  },
  {
    value: 'batchCreationDate',
    description: I18N_TEXT.BATCH_CREATION_DATE
  },
  {
    value: 'sendBatchDate',
    description: I18N_TEXT.SEND_BATCH_DATE
  },
  {
    value: 'financialTransactionCode',
    description: I18N_TEXT.FINANCIAL_TRANSACTION_CODE
  },
  {
    value: 'poic',
    description: I18N_TEXT.POIC
  }
];
