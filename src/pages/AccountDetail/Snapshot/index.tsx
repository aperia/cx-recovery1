import React, { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
// component
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';
import { accountDetailActions } from '../_redux';
import { getAccountDetail, getLoadingSnapShot } from '../_redux/selectors';
import CorrespondenceListModal from 'pages/AccountManagement/Correspondence/CorrespondenceModal';

// redux
import { useAccountDetail, useSelector, useSelectorId } from 'app/hooks';
import { tabActions } from 'pages/__commons/TabBar/_redux';
import { getTimeUpdate } from 'pages/AccountGroup/_redux/accountGroup/selectors';

// helpers
import classnames from 'classnames';
import { IAccounts } from 'pages/AccountManagement/types';
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { I18N_TEXT } from 'app/constants';
import { selectorAccountList } from 'pages/AccountManagement/_redux/accountList/selector';
import { getLast4SSN } from 'pages/AccountManagement/ManageAssociatedDebtors/helpers';

// hooks
import useAccountGroup from 'pages/AccountGroup/hooks/useAccountGroup';
import { useManualForm } from 'pages/AccountManagement/ManualAssignRecall/hooks/useManualForm';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { STORE_ACCOUNT_GROUP_KEY } from 'pages/AccountGroup/constants';
import { genMashAccNumber } from 'pages/AccountManagement/helpers';

// constants
interface SnapshotAccountDetailProps {
  debtorName: string;
}

const SnapshotAccountDetail: React.FC<SnapshotAccountDetailProps> = ({
  debtorName
}) => {
  const { storeId, accNumber } = useAccountDetail();
  const { t } = useTranslation();
  const dataSnapshot = useSelectorId(getAccountDetail);
  const data: IAccounts[] = useSelector(selectorAccountList);
  const timeUpdateAccGroup = useSelectorId(getTimeUpdate);
  const account = data.find(
    (item: IAccounts) => item.accNumber.accountNumberUnMask === accNumber
  );

  const loading = useSelectorId(getLoadingSnapShot);
  const dispatch = useDispatch();

  const { showConfirmRemoveAccGroup } = useAccountGroup();
  const { onOpenModalCreateAccount } = useManualForm();

  const handleShowEditForm = useCallback(
    (account: IAccounts) => {
      dispatch(
        accountManagementActions.onToggleAccountFormModal({
          selected: account
        })
      );
    },
    [dispatch]
  );

  const handleGetAccountDetail = useCallback(() => {
    dispatch(
      accountDetailActions.getAccount({
        accNumber: accNumber,
        storeId
      })
    );
  }, [dispatch, storeId, accNumber]);

  useEffect(() => {
    handleGetAccountDetail();
  }, [handleGetAccountDetail]);

  useEffect(() => {
    if (!timeUpdateAccGroup) return;
    handleGetAccountDetail();
  }, [timeUpdateAccGroup, handleGetAccountDetail]);

  const handleShowConfirmRemove = useCallback(
    (accountItem: IAccounts) => {
      const { id: accountId, groupId, accountNumber } = accountItem;
      const storeGroupId = `${STORE_ACCOUNT_GROUP_KEY}_${groupId}`;

      if (!groupId) return;

      showConfirmRemoveAccGroup({
        accountId,
        debtorName,
        groupId,
        accountNumber,
        storeId: storeGroupId
      });
    },
    [showConfirmRemoveAccGroup, debtorName]
  );

  const handleOpenAccountGroup = useCallback(
    (account: IAccounts, props?: MagicKeyValue) => {
      const { accountNumber, groupId } = account;
      const storeId = `${STORE_ACCOUNT_GROUP_KEY}_${groupId}`;
      dispatch(
        tabActions.addTab({
          title: `${t(I18N_TEXT.ACCOUNT_GROUP_TITLE)} - ${accountNumber}`,
          storeId,
          tabType: 'accountGroup',
          iconName: 'file',
          props: {
            ...props,
            storeId,
            groupId
          }
        })
      );
    },
    [dispatch, t]
  );

  return (
    <>
      <div className={classnames('bg-light-l20 border-bottom', { loading })}>
        <div className="px-24 pt-24 pb-69 max-width-lg mx-auto">
          <div className="d-flex justify-content-between align-items-center">
            <h3>
              {debtorName}
              {account?.groupId || dataSnapshot?.groupId ? (
                <Tooltip
                  placement="top"
                  variant="primary"
                  triggerClassName="cursor-pointer ml-16"
                  element={t(I18N_TEXT.VIEW_ACCOUNT_GROUP)}
                >
                  <Button
                    variant="secondary"
                    className="btn-icon-text"
                    onClick={() =>
                      handleOpenAccountGroup(account || dataSnapshot)
                    }
                  >
                    <Icon name="account-group" />
                    <span className="ml-4">{dataSnapshot?.numberOfGroup}</span>
                  </Button>
                </Tooltip>
              ) : (
                <></>
              )}
            </h3>
            <div>
              {account?.groupId || dataSnapshot?.groupId ? (
                <Button
                  size="sm"
                  variant="outline-danger"
                  onClick={() =>
                    handleShowConfirmRemove(account || dataSnapshot)
                  }
                >
                  {t(I18N_TEXT.REMOVE_ACCOUNT_FROM_GROUP)}
                </Button>
              ) : (
                <Button
                  size="sm"
                  variant="outline-primary"
                  onClick={() =>
                    onOpenModalCreateAccount(account || dataSnapshot)
                  }
                >
                  {t(I18N_TEXT.CREATE_ACCOUNT_GROUP)}
                </Button>
              )}
              <Button
                size="sm"
                className="mr-n8"
                variant="outline-primary"
                onClick={() => handleShowEditForm(account || dataSnapshot)}
              >
                {t(I18N_TEXT.EDIT_ACCOUNT)}
              </Button>
            </div>
          </div>
          <View
            id={`snapshotAccountDetail-${storeId}`}
            formKey={`snapshotAccountDetail-${storeId}`}
            descriptor={`snapshotAccountDetail`}
            value={{
              ...dataSnapshot,
              ...account,
              accNumber: {
                ...dataSnapshot?.accNumber,
                accountNumber: genMashAccNumber(
                  dataSnapshot?.accNumber?.accountNumberUnMask
                )
              },
              originalLoan: {
                ...dataSnapshot?.originalLoan,
                accountNumber: genMashAccNumber(
                  dataSnapshot?.originalLoan?.accountNumberUnMask
                )
              },
              ...(account?.ssn ? { last4SSN: getLast4SSN(account.ssn) } : {})
            }}
          />
        </div>
      </div>
      <CorrespondenceListModal
        accountNumber={account?.accountNumber || dataSnapshot?.accountNumber}
        accNumber={account?.accNumber || dataSnapshot?.accNumber}
      />
    </>
  );
};

export default SnapshotAccountDetail;
