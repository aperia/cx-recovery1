import paymentJson from './paymentInfo.json';
import agencyJson from './agencyInfo.json';
import miscellaneousJson from './miscellaneous.json';
import generalJson from './generalInfo.json';
import financial from './financiaInfo.json';
import { IFinancialInfo, IGeneralInfo } from '../types';

const paymentInfo: any[] = JSON.parse(JSON.stringify(paymentJson));
const agencyInfo: any[] = JSON.parse(JSON.stringify(agencyJson));
const miscellaneousInfo: any[] = JSON.parse(JSON.stringify(miscellaneousJson));
const generalInfo: IGeneralInfo = JSON.parse(JSON.stringify(generalJson));
const financialInfo: IFinancialInfo = JSON.parse(JSON.stringify(financial));

export const OverviewController = {
  getPaymentInfo() {
    return Promise.resolve({ data: { success: true, data: paymentInfo } });
  },
  getAgencyInfo() {
    return Promise.resolve({ data: { success: true, data: agencyInfo } });
  },
  getMiscellaneousInfo() {
    return Promise.resolve({
      data: { success: true, data: miscellaneousInfo }
    });
  },
  getGeneralInfo() {
    return Promise.resolve({
      data: { success: true, data: generalInfo }
    });
  },
  getFinancialInfo() {
    return Promise.resolve({
      data: { success: true, data: financialInfo }
    });
  }
};
