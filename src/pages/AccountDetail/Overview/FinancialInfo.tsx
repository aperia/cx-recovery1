import React, { useEffect } from 'react';
import { View } from 'app/_libraries/_dof/core';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { useDispatch } from 'react-redux';
import { accountDetailOverviewActions as actions } from './_redux';
import { selectorFinancialInfo } from './_redux/selectors';
import { IFinancialInfo } from './types';

const FinancialInfo: React.FC = ({}) => {
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();
  const data: IFinancialInfo = useSelectorId(selectorFinancialInfo);

  useEffect(() => {
    dispatch(
      actions.getFinancialInfo({
        storeId
      })
    );
  }, [dispatch, storeId]);

  return (
    <View
      id={`financialInfo-${storeId}`}
      formKey={`financialInfo-${storeId}`}
      descriptor={`financialInfo`}
      value={data}
    />
  );
};

export default FinancialInfo;
