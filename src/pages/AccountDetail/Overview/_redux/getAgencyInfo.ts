import OverviewServices from '../services';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { set } from 'lodash';
import { IInitialStateAccDetailOverview } from '../types';

export const getAgencyInfo = createAsyncThunk<
  any,
  { storeId: string },
  ThunkAPIConfig
>('getAgencyInfo', async (args, { rejectWithValue }) => {
  try {
    const { data } = await OverviewServices.getAgencyInfo();

    return { data: data.data };
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getAgencyInfoBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateAccDetailOverview>
) => {
  const { pending, fulfilled, rejected } = getAgencyInfo;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.agencyInfo.isLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      set(draftState, `${storeId}.agencyInfo.isLoading`, false);
      set(draftState, `${storeId}.agencyInfo.data`, data);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.agencyInfo.isLoading`, false);
    });
};
