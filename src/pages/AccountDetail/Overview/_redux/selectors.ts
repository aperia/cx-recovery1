import { createSelector } from '@reduxjs/toolkit';
import {
  IAccountDetailOverview,
  IAgencyInfo,
  IFinancialInfo,
  IGeneralInfo,
  IMiscellaneous,
  IPaymentInfo
} from '../types';

export const selectorPaymentInfo = createSelector(
  (states: RootState, storeId: string) =>
    states.accountDetailOverview[storeId]?.paymentInfo?.data,
  (data: IPaymentInfo) => data
);

export const selectorFinancialInfo = createSelector(
  (states: RootState, storeId: string) =>
    states.accountDetailOverview[storeId]?.financialInfo?.data,
  (data: IFinancialInfo) => data
);

export const selectorGeneralInfo = createSelector(
  (states: RootState, storeId: string) =>
    states.accountDetailOverview[storeId]?.generalInfo?.data,
  (data: IGeneralInfo) => data
);

export const selectorMiscellaneousInfo = createSelector(
  (states: RootState, storeId: string) =>
    states.accountDetailOverview[storeId]?.miscellaneousInfo?.data,
  (data: IMiscellaneous) => data
);

export const selectorAgencyInfo = createSelector(
  (states: RootState, storeId: string) =>
    states.accountDetailOverview[storeId]?.agencyInfo?.data,
  (data: IAgencyInfo) => data
);

export const selectorLoadingOverview = createSelector(
  (states: RootState, storeId: string) => states.accountDetailOverview[storeId],
  (data: IAccountDetailOverview) =>
    data?.financialInfo?.isLoading ||
    data?.miscellaneousInfo?.isLoading ||
    data?.agencyInfo?.isLoading ||
    data?.paymentInfo?.isLoading ||
    data?.generalInfo?.isLoading
);
