import { createSlice } from '@reduxjs/toolkit';
import { IInitialStateAccDetailOverview } from '../types';
import { getPaymentInfo, getPaymentInfoBuilder } from './getPaymentInfo';
import { getAgencyInfo, getAgencyInfoBuilder } from './getAgencyInfo';
import {
  getMiscellaneousInfo,
  getMiscellaneousInfoBuilder
} from './getMiscellaneousInfo';

import { getGeneralInfo, getGeneralInfoBuilder } from './getGeneralInfo';

import { getFinancialInfo, getFinancialInfoBuilder } from './getFinancialInfo';

const initialState: IInitialStateAccDetailOverview = {};

export const { actions, reducer } = createSlice({
  name: 'accountDetailOverview',
  initialState: initialState,
  reducers: {},
  extraReducers: builder => {
    getPaymentInfoBuilder(builder);
    getAgencyInfoBuilder(builder);
    getMiscellaneousInfoBuilder(builder);
    getGeneralInfoBuilder(builder);
    getFinancialInfoBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getFinancialInfo,
  getAgencyInfo,
  getMiscellaneousInfo,
  getPaymentInfo,
  getGeneralInfo
};

export { combineActions as accountDetailOverviewActions };
