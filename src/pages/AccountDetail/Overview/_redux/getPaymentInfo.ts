import OverviewServices from '../services';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { set } from 'lodash';
import { IInitialStateAccDetailOverview } from '../types';

export const getPaymentInfo = createAsyncThunk<
  any,
  { storeId: string },
  ThunkAPIConfig
>('getPaymentInfo', async (args, { rejectWithValue }) => {
  try {
    const { data } = await OverviewServices.getPaymentInfo();

    return { data: data.data };
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getPaymentInfoBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateAccDetailOverview>
) => {
  const { pending, fulfilled, rejected } = getPaymentInfo;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.paymentInfo.isLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      set(draftState, `${storeId}.paymentInfo.isLoading`, false);
      set(draftState, `${storeId}.paymentInfo.data`, data);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.paymentInfo.isLoading`, false);
    });
};
