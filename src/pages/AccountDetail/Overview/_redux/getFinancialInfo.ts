import OverviewServices from '../services';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { set } from 'lodash';
import { IFinancialInfo, IInitialStateAccDetailOverview } from '../types';

export const getFinancialInfo = createAsyncThunk<
  { data: IFinancialInfo },
  { storeId: string },
  ThunkAPIConfig
>('getFinancialInfo', async (args, { rejectWithValue }) => {
  try {
    const { data } = await OverviewServices.getFinancialInfo();

    return { data: data.data };
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getFinancialInfoBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateAccDetailOverview>
) => {
  const { pending, fulfilled, rejected } = getFinancialInfo;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.financialInfo.isLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      set(draftState, `${storeId}.financialInfo.isLoading`, false);
      set(draftState, `${storeId}.financialInfo.data`, data);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.financialInfo.isLoading`, false);
    });
};
