import OverviewServices from '../services';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { set } from 'lodash';
import { IInitialStateAccDetailOverview } from '../types';

export const getMiscellaneousInfo = createAsyncThunk<
  any,
  { storeId: string },
  ThunkAPIConfig
>('getMiscellaneousInfo', async (args, { rejectWithValue }) => {
  try {
    const { data } = await OverviewServices.getMiscellaneousInfo();

    return { data: data.data };
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getMiscellaneousInfoBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateAccDetailOverview>
) => {
  const { pending, fulfilled, rejected } = getMiscellaneousInfo;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.miscellaneousInfo.isLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      set(draftState, `${storeId}.miscellaneousInfo.isLoading`, false);
      set(draftState, `${storeId}.miscellaneousInfo.data`, data);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.miscellaneousInfo.isLoading`, false);
    });
};
