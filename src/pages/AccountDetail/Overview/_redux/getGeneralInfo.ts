import OverviewServices from '../services';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { set } from 'lodash';
import { IGeneralInfo, IInitialStateAccDetailOverview } from '../types';

export const getGeneralInfo = createAsyncThunk<
  { data: IGeneralInfo },
  { storeId: string },
  ThunkAPIConfig
>('getGeneralInfo', async (args, { rejectWithValue }) => {
  try {
    const { data } = await OverviewServices.getGeneralInfo();

    return { data: data.data };
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getGeneralInfoBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateAccDetailOverview>
) => {
  const { pending, fulfilled, rejected } = getGeneralInfo;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.generalInfo.isLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      set(draftState, `${storeId}.generalInfo.isLoading`, false);
      set(draftState, `${storeId}.generalInfo.data`, data);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.generalInfo.isLoading`, false);
    });
};
