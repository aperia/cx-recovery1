import React, { useEffect } from 'react';
import { View } from 'app/_libraries/_dof/core';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { useDispatch } from 'react-redux';
import { selectorAgencyInfo } from './_redux/selectors';
import { IAgencyInfo } from './types';
import { accountDetailOverviewActions } from './_redux';

const AgencyInfo: React.FC = ({}) => {
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();
  const data: IAgencyInfo = useSelectorId(selectorAgencyInfo);

  useEffect(() => {
    dispatch(
      accountDetailOverviewActions.getAgencyInfo({
        storeId
      })
    );
  }, [dispatch, storeId]);

  return (
    <View
      id={`agencyInfo-${storeId}`}
      formKey={`agencyInfo-${storeId}`}
      descriptor={`agencyInfo`}
      value={data}
    />
  );
};

export default AgencyInfo;
