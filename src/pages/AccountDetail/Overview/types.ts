export interface IAccountDetailOverview {
  generalInfo: {
    data: IGeneralInfo;
    isLoading: boolean;
  };
  agencyInfo: {
    data: IAgencyInfo;
    isLoading: boolean;
  };
  paymentInfo: {
    data: IPaymentInfo;
    isLoading: boolean;
  };
  financialInfo: {
    data: IFinancialInfo;
    isLoading: boolean;
  };
  miscellaneousInfo: {
    data: IMiscellaneous;
    isLoading: boolean;
  };
}

export interface IInitialStateAccDetailOverview {
  [storeId: string]: IAccountDetailOverview;
}

export interface IAgencyInfo {
  agencyCode: string;
  letterSeries: string;
  agencyFeeCode: string;
  dateToAgency: string;
  agencyLevel: string;
  agencyCaseNumber: string;
  previousAgencyCode: string;
}

export interface IPaymentInfo {
  interestRateCode: string;
  interestRate: string;
  interestStartDate: string;
  interestThroughDate: string;
  paymentScheduleAmount: string;
  paymentScheduleStartDate: string;
  paymentScheduleFrequency: string;
}
export interface IMiscellaneous {
  termDuration: string;
  cycle: string;
  sic: string;
  frsic: string;
  specialComment: string;
  reference: string;
  creditBureau: string;
  complianceCode: string;
}

export interface IGeneralInfo {
  reasonCode: string;
  miscellaneousCodeOne: string;
  miscellaneousCodeTwo: string;
  collectorCode: string;
  typeCode: string;
  portfolioType: string;
  officer: string;
  inLitigation: string;
  bankruptcy: string;
  deceased: string;
  fraud: string;
  statusCode: string;
  statusType: string;
  statusDate: string;
  openedDate: string;
  delinquencyStartDate: string;
  loadDate: string;
  preChangedOffMount: string;
  chargedOffAmount: string;
  chargedOffDate: string;
  shortFieldOne: string;
  dateFieldOne: string;
  amountOne: string;
  longFieldOne: string;
  shortFieldTwo: string;
  dateFieldTwo: string;
  amountTwo: string;
  longFieldTwo: string;
  comment: string;
}

export interface IFinancialInfo {
  recoveryIndicator: string;
  responsibilityCenter: string;
  highestStatementBalance: string;
  classOne: string;
  subClassOne: string;
  reserveOne: string;
  reserveOnePOIC: string;
  reserveOneAmount: string;
  classTwo: string;
  subClassTwo: string;
  reserveTwo: string;
  reserveTwoPOIC: string;
  classTwoAmount: string;
  classThree: string;
  subClassThree: string;
  reserveThree: string;
  reserveThreePOIC: string;
  reserveThreeAmount: string;
  other: string;
  otherPOIC: string;
  otherAmount: string;
  interest: string;
  interestPOIC: string;
  interestAmount: string;
  cost: string;
  costPOIC: string;
  amount: string;
}
