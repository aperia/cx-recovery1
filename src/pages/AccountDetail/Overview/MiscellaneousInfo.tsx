import React, { useEffect } from 'react';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { View } from 'app/_libraries/_dof/core';
import { selectorMiscellaneousInfo } from './_redux/selectors';
import { useDispatch } from 'react-redux';
import { accountDetailOverviewActions } from './_redux';
import { IMiscellaneous } from './types';

const MiscellaneousInfo: React.FC = ({}) => {
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();
  const data: IMiscellaneous = useSelectorId(selectorMiscellaneousInfo);

  useEffect(() => {
    dispatch(
      accountDetailOverviewActions.getMiscellaneousInfo({
        storeId
      })
    );
  }, [dispatch, storeId]);

  return (
    <View
      id={`miscellaneousInfo-${storeId}`}
      formKey={`miscellaneousInfo-${storeId}`}
      descriptor={`miscellaneousInfo`}
      value={data}
    />
  );
};

export default MiscellaneousInfo;
