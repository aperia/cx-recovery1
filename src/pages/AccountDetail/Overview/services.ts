import { OverviewController } from './__mock__/controller';

const OverviewServices = {
  getPaymentInfo: () => {
    return OverviewController.getPaymentInfo();
  },
  getAgencyInfo: () => {
    return OverviewController.getAgencyInfo();
  },
  getMiscellaneousInfo: () => {
    return OverviewController.getMiscellaneousInfo();
  },
  getGeneralInfo: () => {
    return OverviewController.getGeneralInfo();
  },
  getFinancialInfo: () => {
    return OverviewController.getFinancialInfo();
  }
};
export default OverviewServices;
