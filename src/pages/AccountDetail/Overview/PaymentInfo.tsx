import React, { useEffect } from 'react';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { View } from 'app/_libraries/_dof/core';
import { selectorPaymentInfo } from './_redux/selectors';
import { accountDetailOverviewActions as actions } from './_redux';
import { useDispatch } from 'react-redux';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { IPaymentInfo } from './types';

const PaymentInfo: React.FC = ({}) => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const data: IPaymentInfo = useSelectorId(selectorPaymentInfo);

  useEffect(() => {
    dispatch(
      actions.getPaymentInfo({
        storeId
      })
    );
  }, [dispatch, storeId]);

  return (
    <View
      id={`paymentInfo-${storeId}`}
      formKey={`paymentInfo-${storeId}`}
      descriptor={`paymentInfo`}
      value={{
        ...data,
        paymentScheduleFrequency: t('txt_count_day', {
          count: Number(data?.paymentScheduleFrequency)
        })
      }}
    />
  );
};

export default PaymentInfo;
