import React, { useState } from 'react';

// components
import { SimpleBar, Tabs } from 'app/_libraries/_dls/components';

// redux
import { selectorLoadingOverview } from './_redux/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelectorId } from 'app/hooks';

// helpers
import { VerticalTabOverview } from '../constants';
import { idVerticalTab } from '../types';
import classnames from 'classnames';

const Overview: React.FC<any> = ({}) => {
  const { t } = useTranslation();
  const [collapsed, setCollapsed] = useState(false);
  const [activeKey, setActiveKey] = useState<idVerticalTab>('generalInfo');
  const loading = useSelectorId(selectorLoadingOverview);

  const handleToggleCollapse = (collapsed: boolean) => {
    setCollapsed(collapsed);
  };

  const handleActiveKey = (activeKey: string | null) => {
    setActiveKey(activeKey! as idVerticalTab);
  };

  return (
    <div className="px-24 pt-24 max-width-lg mx-auto overlap-next-action">
      <h4 className="mb-16">{t('txt_overview')}</h4>
      <Tabs
        defaultActiveKey="generalInfo"
        expandMessage="Expand Menu"
        collapseMessage="Collapse Menu"
        collapsed={collapsed}
        onToggleCollapsed={handleToggleCollapse}
        activeKey={activeKey}
        onSelect={handleActiveKey}
        unmountOnExit
      >
        <Tabs.Nav>
          {VerticalTabOverview.map(({ id, title }) => (
            <Tabs.Nav.Item key={id}>
              <Tabs.Nav.Link eventKey={id}>{t(title)}</Tabs.Nav.Link>
            </Tabs.Nav.Item>
          ))}
        </Tabs.Nav>
        <Tabs.Content>
          {VerticalTabOverview.map(({ id, component: Component, title }) => (
            <Tabs.Pane eventKey={id} key={id}>
              <div className={classnames('h-100', { loading })}>
                <SimpleBar>
                  <div className="p-24">
                    <h5>{t(title)}</h5>
                    <Component />
                  </div>
                </SimpleBar>
              </div>
            </Tabs.Pane>
          ))}
        </Tabs.Content>
      </Tabs>
    </div>
  );
};

export default Overview;
