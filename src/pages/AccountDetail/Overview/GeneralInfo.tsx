import React, { useEffect } from 'react';
import { View } from 'app/_libraries/_dof/core';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { useDispatch } from 'react-redux';
import { selectorGeneralInfo } from './_redux/selectors';
import { IGeneralInfo } from './types';
import { accountDetailOverviewActions as actions } from './_redux';

const GeneralInfo: React.FC = ({}) => {
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();
  const data: IGeneralInfo = useSelectorId(selectorGeneralInfo);

  useEffect(() => {
    dispatch(
      actions.getGeneralInfo({
        storeId
      })
    );
  }, [dispatch, storeId]);

  return (
    <View
      id={`generalInfo-${storeId}`}
      formKey={`generalInfo-${storeId}`}
      descriptor={`generalInfo`}
      value={data}
    />
  );
};

export default GeneralInfo;
