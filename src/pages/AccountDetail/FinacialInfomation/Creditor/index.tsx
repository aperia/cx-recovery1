import React, { useEffect } from 'react';

// components
import DeleteCreditor from './DeleteCreditor';
import ViewCreditor from './ViewCreditor';
import NoCreditor from './NoCreditor';
import FormCreditor from './FormCreditor';

// hooks
import { useAccountDetail, useSelectorId } from 'app/hooks';

// redux
import { useDispatch } from 'react-redux';
import { financialInformationActions } from '../_redux';
import { selectIsDisplayNoData } from '../_redux/creditor/selectors';

export const initialValuesForm = {
  creditorName: '',
  loanPurpose: '',
  monthlyPaymentAmount: '',
  debtBalance: '',
  pastDueAmount: '',
  finalJudgment:
    'N - No, this creditor does not hold a final judgment on the debtor'
};

const Creditor: React.FC = () => {
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();
  const isNodata: boolean = useSelectorId(selectIsDisplayNoData);

  useEffect(() => {
    dispatch(financialInformationActions.getCreditor({ storeId }));
  }, [dispatch, storeId]);

  return (
    <>
      {isNodata ? <NoCreditor /> : <ViewCreditor />}
      <DeleteCreditor />
      <FormCreditor />
    </>
  );
};

export default Creditor;
