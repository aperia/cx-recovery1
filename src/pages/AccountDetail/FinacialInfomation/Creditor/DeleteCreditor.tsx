import React from 'react';

// components
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import ApiErrorDetail from 'pages/__commons/ApiErrorDetail';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useSelectorId } from 'app/hooks';

// redux
import { useDispatch } from 'react-redux';
import { financialInformationActions } from '../_redux';
import { deleteCreditor } from '../_redux/creditor/deleteCreditor';
import {
  selectorIsLoadingDeleteModal,
  selectorIsOpenDeleteModal
} from '../_redux/creditor/selectors';

// utils
import { I18N_TEXT } from 'app/constants';

const DeleteCreditor: React.FC = () => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();

  const isOpenDeleteModal: boolean = useSelectorId(selectorIsOpenDeleteModal);
  const isLoading: boolean = useSelectorId(selectorIsLoadingDeleteModal);

  if (!isOpenDeleteModal) return null;

  const handleCancel = () => {
    dispatch(financialInformationActions.handleOpenDeleteModal({ storeId }));
  };

  const handleDelete = () => {
    dispatch(financialInformationActions.deleteCreditor({ storeId }));
  };

  return (
    <Modal loading={isLoading} show={isOpenDeleteModal}>
      <ModalHeader border closeButton onHide={handleCancel}>
        <ModalTitle>{t(I18N_TEXT.CONFIRM_DELETE_CREDITOR)}</ModalTitle>
      </ModalHeader>
      <ModalBody>
        <>
          <ApiErrorDetail
            forAction={[deleteCreditor.rejected.type]}
            className="mb-24"
          />
          <p>{t(I18N_TEXT.ARE_YOU_WANT_TO_DELETE_CREDITOR)}</p>
        </>
      </ModalBody>
      <ModalFooter>
        <Button onClick={handleCancel} variant="secondary">
          {t(I18N_TEXT.CANCEL)}
        </Button>
        <Button onClick={handleDelete} variant="danger" className="ml-16">
          {t(I18N_TEXT.DELETE)}
        </Button>
      </ModalFooter>
    </Modal>
  );
};

export default DeleteCreditor;
