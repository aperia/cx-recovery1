import React from 'react';

// components
import { useAccountDetail } from 'app/hooks';
import { NoDataGrid } from 'app/components';
import Button from 'app/_libraries/_dls/components/Button';

// redux
import { financialInformationActions } from '../_redux';
import { useDispatch } from 'react-redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { I18N_TEXT } from 'app/constants';

const NoCreditor: React.FC = () => {
  const { storeId } = useAccountDetail();
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const handleOpen = () => {
    dispatch(financialInformationActions.handleOpenFormModal({ storeId }));
  };

  return (
    <NoDataGrid>
      <Button className="mt-24" onClick={handleOpen} variant="outline-primary">
        {t(I18N_TEXT.ADD_FINANCIAL_CREDITOR)}
      </Button>
    </NoDataGrid>
  );
};

export default NoCreditor;
