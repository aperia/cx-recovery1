import React, { useEffect } from 'react';

// components
import { Button } from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useSelectorId } from 'app/hooks';

// redux
import { batch, useDispatch } from 'react-redux';
import { financialInformationActions } from '../_redux';
import {
  selectCreditor,
  selectorIsLoadingCreditor
} from '../_redux/creditor/selectors';

// utils
import { I18N_TEXT } from 'app/constants';
import { ICreditor } from '../types';
import classnames from 'classnames';

const ViewCreditor: React.FC = () => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();

  const value: ICreditor = useSelectorId(selectCreditor);
  const isLoading: boolean = useSelectorId(selectorIsLoadingCreditor);

  useEffect(() => {
    dispatch(financialInformationActions.getCreditor({ storeId }));
  }, [dispatch, storeId]);

  const handleOpenDelete = () => {
    dispatch(financialInformationActions.handleOpenDeleteModal({ storeId }));
  };

  const handleOpenForm = () => {
    batch(() => {
      dispatch(financialInformationActions.handleOpenFormModal({ storeId }));
      dispatch(
        financialInformationActions.setDataForm({ storeId, data: value })
      );
    });
  };

  return (
    <div className={classnames(`mt-24`, { loading: isLoading })}>
      <div className="d-flex justify-content-between mt-24">
        <h5>{t(I18N_TEXT.FINANCIAL_CREDITOR)}</h5>
        <div className="m-n8">
          <Button size="sm" onClick={handleOpenDelete} variant="outline-danger">
            {t(I18N_TEXT.DELETE)}
          </Button>
          <Button onClick={handleOpenForm} size="sm" variant="outline-primary">
            {t(I18N_TEXT.EDIT)}
          </Button>
        </div>
      </div>
      <div>
        <View
          id={`FinancialInfoCreditor-${storeId}`}
          formKey={`FinancialInfoCreditor-${storeId}`}
          descriptor={`FinancialInfoCreditor`}
          value={value}
        />
      </div>
    </div>
  );
};

export default ViewCreditor;
