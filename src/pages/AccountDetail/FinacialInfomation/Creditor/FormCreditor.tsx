import React, { useEffect } from 'react';

// components
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import ApiErrorDetail from 'pages/__commons/ApiErrorDetail';
import { GroupRadioControl } from 'app/components/FormControl/GroupRadio';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';
import { TextBoxControl } from 'app/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useSelectorId } from 'app/hooks';

// redux
import { deleteCreditor } from '../_redux/creditor/deleteCreditor';
import {
  selectDataForm,
  selectorIsOpenFormModal
} from '../_redux/creditor/selectors';
import { useDispatch } from 'react-redux';
import { financialInformationActions } from '../_redux';

// utils
import { I18N_TEXT } from 'app/constants';
import { Formik } from 'formik';
import { ICreditor } from '../types';
import { isEmpty } from 'app/helpers';
import { initialValuesForm } from '.';

const FormCreditor: React.FC = () => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();

  const isOpen: boolean = useSelectorId(selectorIsOpenFormModal);
  const dataForm: ICreditor = useSelectorId(selectDataForm);

  useEffect(() => {
    if (isOpen) return undefined;
    dispatch(financialInformationActions.cleanDataForm({ storeId }));
  }, [dispatch, isOpen, storeId]);

  const handleOpen = () => {
    dispatch(financialInformationActions.handleOpenFormModal({ storeId }));
  };

  const handleSubmit = (data: ICreditor) => {
    dispatch(financialInformationActions.addEditCreditor({ storeId, data }));
  };

  if (!isOpen) return null;

  return (
    <Modal md show={isOpen}>
      <ModalHeader border onHide={handleOpen} closeButton>
        <ModalTitle>
          {t(
            isEmpty(dataForm)
              ? I18N_TEXT.ADD_FINANCIAL_CREDITOR
              : I18N_TEXT.EDIT_FINANCIAL_CREDITOR
          )}
        </ModalTitle>
      </ModalHeader>
      <Formik
        initialValues={isEmpty(dataForm) ? initialValuesForm : dataForm}
        onSubmit={handleSubmit}
      >
        {propsFormik => (
          <>
            <ModalBody>
              <>
                <ApiErrorDetail
                  forAction={[deleteCreditor.rejected.type]}
                  className="mb-24"
                />
                <div className="row">
                  <div className="col-6">
                    <TextBoxControl
                      name={'creditorName'}
                      label={t(I18N_TEXT.CREDITOR_NAME)}
                      value={propsFormik?.values?.creditorName}
                      maxLength={40}
                    />
                  </div>
                  <div className="col-6">
                    <TextBoxControl
                      name={'loanPurpose'}
                      label={t(I18N_TEXT.LOAN_PURPOSE)}
                      value={propsFormik?.values?.loanPurpose}
                      maxLength={5}
                    />
                  </div>
                  <div className="col-4">
                    <FormikNumericControl
                      id={'monthlyPaymentAmount'}
                      name={'monthlyPaymentAmount'}
                      label={t(I18N_TEXT.MONTHLY_PAYMENT_AMOUNT)}
                      value={propsFormik?.values?.monthlyPaymentAmount}
                      className="mt-16"
                      maxLength={13}
                    />
                  </div>
                  <div className="col-4">
                    <FormikNumericControl
                      id={'debtBalance'}
                      name={'debtBalance'}
                      label={t(I18N_TEXT.DEBT_BALANCE)}
                      value={propsFormik?.values?.debtBalance}
                      className="mt-16"
                      maxLength={15}
                    />
                  </div>
                  <div className="col-4">
                    <FormikNumericControl
                      id={'pastDueAmount'}
                      name={'pastDueAmount'}
                      label={t(I18N_TEXT.PAST_DUE_AMOUNT)}
                      value={propsFormik?.values?.pastDueAmount}
                      className="mt-16"
                      maxLength={13}
                    />
                  </div>
                </div>
                <div className="d-flex mt-16 ">
                  <p className="fw-500 mr-24">{t(I18N_TEXT.FINAL_JUDGMENT)}:</p>
                  <GroupRadioControl
                    name={'finalJudgment'}
                    options={[
                      {
                        value:
                          'Y - Yes, this creditor holds a final judgment on the debtor',
                        description:
                          'Y - Yes, this creditor holds a final judgment on the debtor'
                      },
                      {
                        value:
                          'N - No, this creditor does not hold a final judgment on the debtor',
                        description:
                          'N - No, this creditor does not hold a final judgment on the debtor'
                      }
                    ]}
                  />
                </div>
              </>
            </ModalBody>
            <ModalFooter>
              <Button onClick={handleOpen} variant="secondary">
                {t(I18N_TEXT.CANCEL)}
              </Button>
              <Button onClick={propsFormik.submitForm} variant="primary">
                {t(isEmpty(dataForm) ? I18N_TEXT.SUBMIT : I18N_TEXT.SAVE)}
              </Button>
            </ModalFooter>
          </>
        )}
      </Formik>
    </Modal>
  );
};

export default FormCreditor;
