import { ICreditor } from './types';
import { creditorController } from './__mock__/creditorController';

const financialInformationService = {
  getCreditor: () => {
    return creditorController.getCreditor();
  },
  deleteCreditor: () => {
    return creditorController.deleteCreditor();
  },
  addEditCreditor: (data: ICreditor) => {
    return creditorController.addEditCreditor(data);
  }
};
export default financialInformationService;
