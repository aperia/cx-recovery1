import { formatTime, isEmpty } from 'app/helpers';
import { BATCH_TYPE_PAYMENTS } from 'pages/BatchManagement/constants';
import { IAddressData } from '../BankruptcyDeceaseRecord/types';
import {
  DEMOGRAPHICS_DEBTOR_INFO_VIEW,
  DEMOGRAPHICS_SPOUSE_INFO_VIEW
} from './DemoGraphics/constants';
import { IDemoGraphics } from './types';

export const formatDataDemographics = (id: string, data: any) => {
  if (id === DEMOGRAPHICS_DEBTOR_INFO_VIEW) {
    return {
      salaryTypeDebtor: {
        label:
          data.salaryTypeDebtor === 'N'
            ? 'txt_net_monthly_salary'
            : 'txt_gross_monthly_salary',
        value:
          data.salaryTypeDebtor === 'N'
            ? data.netMonthlySalaryDebtor
            : data.grossMonthlySalaryDebtor
      },
      contractTypeDebtor: mappingType(data.contractTypeDebtor, CONTRACT_TYPE)
    };
  }
  if (id === DEMOGRAPHICS_SPOUSE_INFO_VIEW) {
    return {
      salaryTypeSpouse: {
        label:
          data.salaryTypeSpouse === 'N'
            ? 'txt_net_monthly_salary'
            : 'txt_gross_monthly_salary',
        value:
          data.salaryTypeSpouse === 'N'
            ? data.netMonthlySalarySpouse
            : data.grossMonthlySalarySpouse
      },
      contractTypeSpouse: mappingType(data.contractTypeSpouse, CONTRACT_TYPE)
    };
  }

  return {
    supportType: mappingType(data.supportType, SUPPORT_TYPE),
    supportMethod: mappingType(data.supportMethod, SUPPORT_METHOD)
  };
};

export const getTimeBetween2Dates = ({
  date1,
  date2
}: {
  date1: string;
  date2: string;
}) => {
  return Math.floor(
    (new Date(date2).getTime() - new Date(date1).getTime()) / (1000 * 3600 * 24)
  );
};

export const getNextDateWith = (date: Date, nextDays: number) => {
  return new Date(date.setDate(date.getDate() + nextDays));
};

const formatAddress = ({
  addressLine1,
  addressLine2,
  city,
  state,
  zipCode
}: IAddressData) => {
  let result = '';
  addressLine1 && (result += addressLine1);
  addressLine2 && (result += ` ${addressLine2}`);
  city && (result += `\n${city}`);
  state && (result += `, ${state}`);
  zipCode && (result += ` ${zipCode}`);
  return result;
};

export const formatDataSubmitting = (values: any) => {
  return {
    debtorInformation: {
      primaryDebtorName: values.primaryDebtorName,
      primaryDebtorHomePhone: values.primaryDebtorHomePhone,
      primaryDebtorAddress: formatAddress({
        addressLine1: values.addressLine1,
        addressLine2: values.addressLine2,
        city: values.city,
        state: values.state,
        zipCode: values.zipCode
      }),
      employerNameDebtor: values.employerNameDebtor,
      employerPhoneDebtor: values.employerPhoneDebtor,
      employerAddressDebtor: formatAddress({
        addressLine1: values.addressEmploymentDebtor,
        city: values.cityEmploymentDebtor,
        state: values.stateEmploymentDebtor,
        zipCode: values.zipCodeEmploymentDebtor
      }),
      contractTypeDebtor: values.contractTypeDebtor,
      startDateDebtor: formatTime(values.startDateDebtor)?.date || '',
      lastPayDateDebtor: formatTime(values.lastPayDateDebtor)?.date || '',
      salaryTypeDebtor: values.salaryTypeDebtor,
      nextPayDateDebtor: formatTime(values.nextPayDateDebtor)?.date || '',
      netMonthlySalaryDebtor: values.netMonthlySalaryDebtor,
      grossMonthlySalaryDebtor: values.grossMonthlySalaryDebtor,
      deductionDebtor: values.deductionDebtor,
      comment: values.comment,
      addressLine1: values.addressLine1,
      addressLine2: values.addressLine2,
      state: values.state,
      city: values.city,
      zipCode: values.zipCode,
      stateEmploymentDebtor: values.stateEmploymentDebtor,
      cityEmploymentDebtor: values.cityEmploymentDebtor,
      zipCodeEmploymentDebtor: values.zipCodeEmploymentDebtor,
      addressEmploymentDebtor: values.addressEmploymentDebtor
    },
    spouseInformation: {
      spouseName: values.spouseName,
      homePhoneSpouse: values.homePhoneSpouse,
      spouseAddress: formatAddress({
        addressLine1: values.spouseAddressLine1,
        addressLine2: values.spouseAddressLine2,
        city: values.citySpouse,
        state: values.stateSpouse,
        zipCode: values.zipCodeSpouse
      }),
      employerNameSpouse: values.employerNameSpouse,
      employerPhoneSpouse: values.employerPhoneSpouse,
      employerAddressSpouse: formatAddress({
        addressLine1: values.addressEmploymentSpouse,
        city: values.cityEmploymentSpouse,
        state: values.stateEmploymentSpouse,
        zipCode: values.zipCodeEmploymentSpouse
      }),
      contractTypeSpouse: values.contractTypeSpouse,

      startDateSpouse: formatTime(values.startDateSpouse)?.date || '',
      lastPayDateSpouse: formatTime(values.lastPayDateSpouse)?.date || '',
      nextPayDateSpouse: formatTime(values.nextPayDateSpouse)?.date || '',

      salaryTypeSpouse: values.salaryTypeSpouse,
      netMonthlySalarySpouse: values.netMonthlySalarySpouse,
      grossMonthlySalarySpouse: values.grossMonthlySalarySpouse,
      deductionSpouse: values.deductionSpouse,

      spouseAddressLine1: values.spouseAddressLine1,
      spouseAddressLine2: values.spouseAddressLine2,
      stateSpouse: values.stateSpouse,
      citySpouse: values.citySpouse,
      zipCodeSpouse: values.zipCodeSpouse,

      stateEmploymentSpouse: values.stateEmploymentSpouse,
      cityEmploymentSpouse: values.cityEmploymentSpouse,
      zipCodeEmploymentSpouse: values.zipCodeEmploymentSpouse,
      addressEmploymentSpouse: values.addressEmploymentSpouse
    },
    childrenInformation: {
      numberOfChildren: values.numberOfChildren || '0',
      child1Age: values.child1Age,
      child2Age: values.child2Age,
      child3Age: values.child3Age,
      child4Age: values.child4Age,
      child5Age: values.child5Age,
      child6Age: values.child6Age,
      child7Age: values.child7Age,
      child8Age: values.child8Age,
      child9Age: values.child9Age,
      child10Age: values.child10Age,
      supportPersonName: values.supportPersonName,
      supportAmount: values.supportAmount,
      supportCountry: values.supportCountry,
      supportType: values.supportType,
      supportMethod: values.supportMethod
    }
  };
};

const SUPPORT_TYPE = 'supportType';
const SUPPORT_METHOD = 'supportMethod';
const SALARY_TYPE = 'salaryType';
const CONTRACT_TYPE = 'contractType';

const mappingType = (value: string, id: string) => {
  switch (id) {
    case CONTRACT_TYPE:
      if (value === 'None') return value;
      return value === 'F' ? 'Full-time' : 'Part-time';

    case SALARY_TYPE:
      return value === 'G' ? 'Gross' : 'Net';

    case SUPPORT_TYPE:
      if (value === 'None' || isEmpty(value)) return value;
      return value === BATCH_TYPE_PAYMENTS
        ? 'P - The debtor pays court-awarded support payments.'
        : 'R - The debtor receives court-awarded support payments.';

    case SUPPORT_METHOD:
      if (value === 'None' || isEmpty(value)) return value;
      return value === 'C'
        ? 'C - The debtor pays or receives payments through a court.'
        : 'D - The debtor pays or receives direct payments.';
    default:
      return '';
  }
};

export const genInitValues = (data: IDemoGraphics) => {
  if (isEmpty(data)) return {};
  return {
    ...data.debtorInformation,
    ...data.spouseInformation,
    ...data.childrenInformation
  };
};
