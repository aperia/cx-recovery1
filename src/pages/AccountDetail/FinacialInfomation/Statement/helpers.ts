import { CUSTOM } from 'app/constants';
import { IFinancialStatement, ISnapshotStatement } from '../types';

// helpers
import { pick } from 'app/helpers/lodash';
import { pickBy, identity } from 'lodash';
import { formatTime } from 'app/helpers';
import { formatAddress } from 'pages/AccountDetail/BankruptcyDeceaseRecord/helpers';
import {
  ACCEPT_OR_REJECT_TYPE_REF_DATA,
  CHILDCARE_PAYMENT_FREQUENCY_REF_DATA,
  FINAL_JUDGMENT_TYPE_REF_DATA,
  OWNER_OR_RENTER_REF_DATA,
  PAYMENT_SCHEDULE_FREQUENCY_REF_DATA,
  REIMBURSEMENT_REF_DATA
} from './constants';
import {
  checkPluralValue,
  getDescriptionFromCode
} from 'pages/__commons/helpers';
import { SuffixDate } from 'pages/AgencyDetails/types';

export const prepareSnapshot = (
  data?: IFinancialStatement
): ISnapshotStatement | undefined => {
  let result;

  if (data) {
    const homeInfo = pickBy(
      // pick homeInfo fields
      pick(data, [
        'paymentAmount',
        'montageOrRentDueDate',
        'paidTo',
        'ownerOrRenterType'
      ]),
      identity // remove empty fields
    );

    const automobileInfo = pickBy(
      // pick automobileInfo fields
      pick(data, [
        'brand',
        'model',
        'modelYear',
        'licensePlateNumber',
        'monthlyGas',
        'monthlyInsurance',
        'title',
        'driverLicense',
        'reimbursementType',
        'reimbursementAmount',
        'ai_lienholder1',
        'monthlyPaymentAmount1',
        'loanBalance1',
        'ai_lienholder2',
        'monthlyPaymentAmount2',
        'loanBalance2'
      ]),
      identity // remove empty fields
    );

    const monthlyAveragePayments = pickBy(
      // pick monthlyAveragePayments fields
      pick(data, [
        'electric',
        'gas',
        'water',
        'phone',
        'cableTV',
        'lunch',
        'food'
      ]),
      identity // remove empty fields
    );

    const childcareInfo = pickBy(
      // pick childcareInfo fields
      pick(data, [
        'childcareProviderName',
        'childcarePaymentFrequency',
        'childcareAmount'
      ]),
      identity // remove empty fields
    );

    const otherAssets = pickBy(
      // pick otherAssets fields
      pick(data, [
        'lifeInsurance',
        'ira',
        'stockAndBondsValue',
        'numberOfBoats',
        'numberOfRecreationalVehicles',
        'finalJudgmentType'
      ]),
      identity // remove empty fields
    );

    const comment = pickBy(
      // pick comment fields
      pick(data, ['comment']),
      identity // remove empty fields
    );

    const bankInfo = pickBy(
      // pick bankInfo fields
      pick(data, ['bankInstitution1', 'bankInstitution2', 'bankInstitution3']),
      identity // remove empty fields
    );

    const previousAddresses = pickBy(
      // pick previousAddresses fields
      pick(data, [
        'addressLine1_1',
        'addressLine2_1',
        'city_1',
        'state_1',
        'zipCode_1',

        'addressLine1_2',
        'addressLine2_2',
        'city_2',
        'state_2',
        'zipCode_2',

        'addressLine1_3',
        'addressLine2_3',
        'city_3',
        'state_3',
        'zipCode_3',

        'addressLine1_4',
        'addressLine2_4',
        'city_4',
        'state_4',
        'zipCode_4'
      ]),
      identity // remove empty fields
    );

    const relativesInfo = pickBy(
      // pick relativesInfo fields
      pick(data, ['relative1', 'relative2', 'relative3', 'relative4']),
      identity // remove empty fields
    );

    const referencesInfo = pickBy(
      // pick referencesInfo fields
      pick(data, ['reference1', 'reference2', 'reference3', 'reference4']),
      identity // remove empty fields
    );

    const lienInfo = pickBy(
      // pick lienInfo fields
      pick(data, [
        'lienholder1',
        'lienholder2',
        'lienholder3',
        'lienholder4',
        'lienholder5',
        'lienholder6',
        'paymentScheduleFrequency',
        'paymentScheduleFrequencyCustom',
        'paymentScheduleStartDate',
        'paymentScheduleAmount',
        'realEstate',
        'acceptOrRejectType'
      ]),
      identity // remove empty fields
    );

    const { montageOrRentDueDate, ownerOrRenterType } = homeInfo;
    const { childcarePaymentFrequency } = childcareInfo;
    const { finalJudgmentType } = otherAssets;
    const {
      paymentScheduleFrequency,
      paymentScheduleStartDate,
      acceptOrRejectType
    } = lienInfo;

    const address1 = formatAddress({
      addressLine1: previousAddresses?.addressLine1_1,
      addressLine2: previousAddresses?.addressLine2_1,
      city: previousAddresses?.city_1,
      state: previousAddresses?.state_1,
      zipCode: previousAddresses?.zipCode_1
    });

    const address2 = formatAddress({
      addressLine1: previousAddresses?.addressLine1_2,
      addressLine2: previousAddresses?.addressLine2_2,
      city: previousAddresses?.city_2,
      state: previousAddresses?.state_2,
      zipCode: previousAddresses?.zipCode_2
    });

    const address3 = formatAddress({
      addressLine1: previousAddresses?.addressLine1_3,
      addressLine2: previousAddresses?.addressLine2_3,
      city: previousAddresses?.city_3,
      state: previousAddresses?.state_3,
      zipCode: previousAddresses?.zipCode_3
    });

    const address4 = formatAddress({
      addressLine1: previousAddresses?.addressLine1_4,
      addressLine2: previousAddresses?.addressLine2_4,
      city: previousAddresses?.city_4,
      state: previousAddresses?.state_4,
      zipCode: previousAddresses?.zipCode_4
    });
    result = {
      ...data,

      homeInfo: {
        ...homeInfo,
        ...(ownerOrRenterType
          ? {
              ownerOrRenterType: getDescriptionFromCode(
                OWNER_OR_RENTER_REF_DATA,
                ownerOrRenterType,
                false
              )
            }
          : {}),
        ...(montageOrRentDueDate
          ? {
              montageOrRentDueDate: formatTime(montageOrRentDueDate || '').date
            }
          : {})
      },
      automobileInfo: {
        ...automobileInfo,
        reimbursementType: getDescriptionFromCode(
          REIMBURSEMENT_REF_DATA,
          automobileInfo.reimbursementType,
          false
        )
      },
      monthlyAveragePayments,
      childcareInfo: {
        ...childcareInfo,
        ...(childcarePaymentFrequency
          ? {
              childcarePaymentFrequency: getDescriptionFromCode(
                CHILDCARE_PAYMENT_FREQUENCY_REF_DATA,
                childcarePaymentFrequency,
                false
              )
            }
          : {})
      },
      otherAssets: {
        ...otherAssets,
        ...(finalJudgmentType
          ? {
              finalJudgmentType: getDescriptionFromCode(
                FINAL_JUDGMENT_TYPE_REF_DATA,
                otherAssets.finalJudgmentType,
                false
              )
            }
          : {})
      },
      comment,
      bankInfo,
      previousAddresses: {
        ...(address1 ? { address1 } : {}),
        ...(address2 ? { address2 } : {}),
        ...(address3 ? { address3 } : {}),
        ...(address4 ? { address4 } : {})
      },
      relativesInfo,
      referencesInfo,
      lienInfo: {
        ...lienInfo,

        ...(paymentScheduleFrequency
          ? {
              paymentScheduleFrequency:
                lienInfo.paymentScheduleFrequency === CUSTOM
                  ? checkPluralValue(
                      lienInfo?.paymentScheduleFrequencyCustom || '',
                      SuffixDate.DAY
                    )
                  : getDescriptionFromCode(
                      PAYMENT_SCHEDULE_FREQUENCY_REF_DATA,
                      lienInfo.paymentScheduleFrequency,
                      false
                    )
            }
          : {}),
        ...(acceptOrRejectType
          ? {
              acceptOrRejectType: getDescriptionFromCode(
                ACCEPT_OR_REJECT_TYPE_REF_DATA,
                lienInfo.acceptOrRejectType,
                false
              )
            }
          : {}),
        ...(paymentScheduleStartDate
          ? {
              paymentScheduleStartDate: formatTime(
                paymentScheduleStartDate || ''
              ).date
            }
          : {})
      }
    } as ISnapshotStatement;
  }
  return result;
};
