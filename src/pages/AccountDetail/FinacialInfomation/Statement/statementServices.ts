import { statementController } from './__mock__/statementController';

const statementServices = {
  saveStatement(args: any) {
    return statementController.saveStatement(args);
  },
  getStatement(args: any) {
    return statementController.getStatement(args);
  },
  deleteStatement({ id }: any) {
    return statementController.deleteStatement({ id });
  }
};

export default statementServices;
