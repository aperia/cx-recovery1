// mocks
import { Repository } from 'app/utils/repositories/concretes/Repository';

// types
import { IFinancialStatement } from '../../types';

const isHaveError = window.location.href.includes('error');
const isHaveEmpty = window.location.href.includes('empty');
const data = [] as IFinancialStatement[];

const statementRepository = new Repository<IFinancialStatement>(
  JSON.parse(JSON.stringify(data))
);

const getResult = (data: IFinancialStatement[]) => {
  let result;

  if (isHaveEmpty) return result;

  if (Array.isArray(data) && data.length) {
    result = data[0];
  }

  return result;
};

export const statementController = {
  saveStatement: (args: any) => {
    const { id, values } = args;

    if (isHaveError) {
      return Promise.reject({});
    }

    const { data } = id
      ? statementRepository.update(id, values)
      : statementRepository.add(values);

    return Promise.resolve({
      data: { success: true, data: getResult(data) }
    });
  },
  getStatement(args: any) {
    const { data } = statementRepository.getAll();

    return Promise.resolve({
      data: { success: true, data: getResult(data) }
    });
  },
  deleteStatement: ({ id }: any) => {
    if (isHaveError) {
      return Promise.reject({});
    }

    const { data } = statementRepository.delete(id);

    return Promise.resolve({
      data: { success: true, data: getResult(data) }
    });
  }
};
