import React, { useCallback } from 'react';

// components
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';
import { DatePickerControl, TextBoxControl } from 'app/components';
import { GroupRadioControl } from 'app/components/FormControl/GroupRadio';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import {
  I18N_TEXT,
  ALPHA_NUMERIC_SPACE_REPLACE_REGEX,
  MAX_LENGTH_13,
  MAX_LENGTH_40
} from 'app/constants';

// helpers
import { useFormikContext } from 'formik';
import { OWNER_OR_RENTER_REF_DATA } from '../constants';

const HomeInformation = () => {
  const { t } = useTranslation();
  const ownerOrRenterTypeOptions = OWNER_OR_RENTER_REF_DATA;

  const { handleChange } = useFormikContext();

  const handleCheckboxChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      handleChange(event);
    },
    [handleChange]
  );

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.HOME_INFO)}</h5>
      </div>

      <FormikNumericControl
        name="paymentAmount"
        label={t(I18N_TEXT.PAYMENT_AMOUNT)}
        className="col-6 col-lg-4 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />

      <DatePickerControl
        name="montageOrRentDueDate"
        label={t(I18N_TEXT.MORTGAGE_OR_RENT_DUE_DATE)}
        className="col-6 col-lg-4 mt-16 text-transform-none"
      />

      <TextBoxControl
        name="paidTo"
        label={t(I18N_TEXT.PAID_TO)}
        className="col-6 col-lg-4 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_40}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />
      <div className="col-12 mt-16">
        <div className="d-flex ">
          <p className="fw-500 color-grey mr-24">
            {t(I18N_TEXT.OWNER_OR_RENTER)}:
          </p>
          <GroupRadioControl
            name="ownerOrRenterType"
            direction="horizontal"
            options={ownerOrRenterTypeOptions}
            onValueChange={handleCheckboxChange}
          />
        </div>
      </div>
    </div>
  );
};

export default HomeInformation;
