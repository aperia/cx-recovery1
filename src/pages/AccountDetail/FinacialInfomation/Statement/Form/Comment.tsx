import React from 'react';

// components
import FormikTextAreaControl from 'app/components/FormControl/FormikTextAreaControl';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { MAX_LENGTH_140, I18N_TEXT } from 'app/constants';

const Comment = () => {
  const { t } = useTranslation();

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.COMMENT)}</h5>
      </div>

      <div className="col-12 col-lg-8 mt-16">
        <FormikTextAreaControl
          className="h-lg-140px"
          name="comment"
          label={t(I18N_TEXT.COMMENT)}
          maxLength={MAX_LENGTH_140}
          disabledEnter
        />
      </div>
    </div>
  );
};

export default Comment;
