import React from 'react';

// components
import { TextBoxControl } from 'app/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import {
  I18N_TEXT,
  ALPHA_NUMERIC_SPACE_REPLACE_REGEX,
  MAX_LENGTH_35
} from 'app/constants';

const RelativesInformation = () => {
  const { t } = useTranslation();

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.RELATIVES_INFO)}</h5>
      </div>

      <p className="col-12 mt-8">{t(I18N_TEXT.SUBTITLE_RELATIVES_INFO)}</p>

      <TextBoxControl
        name="relative1"
        label={`${t(I18N_TEXT.RELATIVE_1)}`}
        className="col-6 mt-16"
        maxLength={MAX_LENGTH_35}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <TextBoxControl
        name="relative2"
        label={`${t(I18N_TEXT.RELATIVE_2)}`}
        className="col-6 mt-16"
        maxLength={MAX_LENGTH_35}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <TextBoxControl
        name="relative3"
        label={`${t(I18N_TEXT.RELATIVE_3)}`}
        className="col-6 mt-16"
        maxLength={MAX_LENGTH_35}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <TextBoxControl
        name="relative4"
        label={`${t(I18N_TEXT.RELATIVE_4)}`}
        className="col-6 mt-16"
        maxLength={MAX_LENGTH_35}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />
    </div>
  );
};

export default RelativesInformation;
