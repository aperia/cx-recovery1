import React, { useCallback } from 'react';

// components
import { TextBoxControl } from 'app/components';
import { GroupRadioControl } from 'app/components/FormControl/GroupRadio';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';

// constants
import {
  I18N_TEXT,
  NUMERIC_REPLACE_REGEX,
  MAX_LENGTH_13,
  MAX_LENGTH_17,
  MAX_LENGTH_2
} from 'app/constants';

// helpers
import { useFormikContext } from 'formik';
import { FINAL_JUDGMENT_TYPE_REF_DATA } from '../constants';

const OtherAssets = () => {
  const { t } = useTranslation();

  const finalJudgmentTypeOptions = FINAL_JUDGMENT_TYPE_REF_DATA;

  const { handleChange } = useFormikContext();

  const handleCheckboxChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      handleChange(event);
    },
    [handleChange]
  );

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.OTHER_ASSETS)}</h5>
      </div>

      <FormikNumericControl
        name="lifeInsurance"
        label={t(I18N_TEXT.LIFE_INSURANCE)}
        className="col-4 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />

      <FormikNumericControl
        name="ira"
        label={t(I18N_TEXT.IRA)}
        className="col-4 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />

      <FormikNumericControl
        name="stockAndBondsValue"
        label={t(I18N_TEXT.STOCK_AND_BONDS_VALUE)}
        className="col-4 mt-16"
        maxLength={MAX_LENGTH_17}
        format="c2"
      />

      <TextBoxControl
        name="numberOfBoats"
        label={t(I18N_TEXT.NUMBER_OF_BOATS)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_2}
        regex={NUMERIC_REPLACE_REGEX}
      />

      <TextBoxControl
        name="numberOfRecreationalVehicles"
        label={t(I18N_TEXT.NUMBER_OF_RECREATIONAL_VEHICLES)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_2}
        regex={NUMERIC_REPLACE_REGEX}
      />

      <div className="col-12 mt-16">
        <div className="d-flex ">
          <p className="fw-500 color-grey mr-24">
            {t(I18N_TEXT.FINAL_JUDGMENT)}:
          </p>
          <GroupRadioControl
            name="finalJudgmentType"
            options={finalJudgmentTypeOptions}
            onValueChange={handleCheckboxChange}
          />
        </div>
      </div>
    </div>
  );
};

export default OtherAssets;
