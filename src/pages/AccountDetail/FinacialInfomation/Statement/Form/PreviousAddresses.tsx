import React from 'react';

// components
import { DropdownControl, TextBoxControl } from 'app/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useRefData from 'app/hooks/useRefData';

// constants
import {
  I18N_TEXT,
  ALPHA_NUMERIC_SPACE_REPLACE_REGEX,
  MAX_LENGTH_10,
  MAX_LENGTH_25,
  MAX_LENGTH_35,
  NUMERIC_REPLACE_REGEX
} from 'app/constants';

const PreviousAddresses = () => {
  const { t } = useTranslation();
  const stateOptions = useRefData('state');

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.PREVIOUS_ADDRESS)}</h5>
      </div>

      <div className="col-12 mt-16">
        <p className="color-grey fw-500">{`${t(I18N_TEXT.ADDRESS)} 1:`}</p>
      </div>

      <div className="col-12 col-lg-6">
        <div className="row">
          <TextBoxControl
            name="addressLine1_1"
            label={`${t(I18N_TEXT.ADDRESS_LINE_1)}`}
            className="col-6 mt-8"
            maxLength={MAX_LENGTH_35}
            regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
          />

          <TextBoxControl
            name="addressLine2_1"
            label={`${t(I18N_TEXT.ADDRESS_LINE_2)}`}
            className="col-6 mt-8"
            maxLength={MAX_LENGTH_35}
            regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
          />
        </div>
      </div>

      <div className="col-12 col-lg-6">
        <div className="row">
          <TextBoxControl
            name="city_1"
            label={t(I18N_TEXT.CITY)}
            className="col-4 mt-lg-8 mt-md-16"
            maxLength={MAX_LENGTH_25}
            regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
          />

          <DropdownControl
            name="state_1"
            label={t(I18N_TEXT.STATE)}
            className="col-4 mt-lg-8 mt-md-16"
            options={stateOptions}
            isCombine
          />

          <TextBoxControl
            name="zipCode_1"
            label={t(I18N_TEXT.ZIP_CODE)}
            className="col-4 mt-lg-8 mt-md-16"
            maxLength={MAX_LENGTH_10}
            regex={NUMERIC_REPLACE_REGEX}
          />
        </div>
      </div>

      <div className="col-12 mt-16">
        <p className="color-grey fw-500">{`${t(I18N_TEXT.ADDRESS)} 2:`}</p>
      </div>

      <div className="col-12 col-lg-6">
        <div className="row">
          <TextBoxControl
            name="addressLine1_2"
            label={`${t(I18N_TEXT.ADDRESS_LINE_1)}`}
            className="col-6 mt-8"
            maxLength={MAX_LENGTH_35}
            regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
          />

          <TextBoxControl
            name="addressLine2_2"
            label={`${t(I18N_TEXT.ADDRESS_LINE_2)}`}
            className="col-6 mt-8"
            maxLength={MAX_LENGTH_35}
            regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
          />
        </div>
      </div>

      <div className="col-12 col-lg-6">
        <div className="row">
          <TextBoxControl
            name="city_2"
            label={t(I18N_TEXT.CITY)}
            className="col-4 mt-lg-8 mt-md-16"
            maxLength={MAX_LENGTH_25}
            regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
          />

          <DropdownControl
            name="state_2"
            label={t(I18N_TEXT.STATE)}
            className="col-4 mt-lg-8 mt-md-16"
            options={stateOptions}
            isCombine
          />

          <TextBoxControl
            name="zipCode_2"
            label={t(I18N_TEXT.ZIP_CODE)}
            className="col-4 mt-lg-8 mt-md-16"
            maxLength={MAX_LENGTH_10}
            regex={NUMERIC_REPLACE_REGEX}
          />
        </div>
      </div>

      <div className="col-12 mt-16">
        <p className="color-grey fw-500">{`${t(I18N_TEXT.ADDRESS)} 3:`}</p>
      </div>

      <div className="col-12 col-lg-6">
        <div className="row">
          <TextBoxControl
            name="addressLine1_3"
            label={`${t(I18N_TEXT.ADDRESS_LINE_1)}`}
            className="col-6 mt-8"
            maxLength={MAX_LENGTH_35}
            regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
          />

          <TextBoxControl
            name="addressLine2_3"
            label={`${t(I18N_TEXT.ADDRESS_LINE_2)}`}
            className="col-6 mt-8"
            maxLength={MAX_LENGTH_35}
            regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
          />
        </div>
      </div>

      <div className="col-12 col-lg-6">
        <div className="row">
          <TextBoxControl
            name="city_3"
            label={t(I18N_TEXT.CITY)}
            className="col-4 mt-lg-8 mt-md-16"
            maxLength={MAX_LENGTH_25}
            regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
          />

          <DropdownControl
            name="state_3"
            label={t(I18N_TEXT.STATE)}
            className="col-4 mt-lg-8 mt-md-16"
            options={stateOptions}
            isCombine
          />

          <TextBoxControl
            name="zipCode_3"
            label={t(I18N_TEXT.ZIP_CODE)}
            className="col-4 mt-lg-8 mt-md-16"
            maxLength={MAX_LENGTH_10}
            regex={NUMERIC_REPLACE_REGEX}
          />
        </div>
      </div>

      <div className="col-12 mt-16">
        <p className="color-grey fw-500">{`${t(I18N_TEXT.ADDRESS)} 4:`}</p>
      </div>

      <div className="col-12 col-lg-6">
        <div className="row">
          <TextBoxControl
            name="addressLine1_4"
            label={`${t(I18N_TEXT.ADDRESS_LINE_1)}`}
            className="col-6 mt-8"
            maxLength={MAX_LENGTH_35}
            regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
          />

          <TextBoxControl
            name="addressLine2_4"
            label={`${t(I18N_TEXT.ADDRESS_LINE_2)}`}
            className="col-6 mt-8"
            maxLength={MAX_LENGTH_35}
            regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
          />
        </div>
      </div>

      <div className="col-12 col-lg-6">
        <div className="row">
          <TextBoxControl
            name="city_4"
            label={t(I18N_TEXT.CITY)}
            className="col-4 mt-lg-8 mt-md-16"
            maxLength={MAX_LENGTH_25}
            regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
          />

          <DropdownControl
            name="state_4"
            label={t(I18N_TEXT.STATE)}
            className="col-4 mt-lg-8 mt-md-16"
            options={stateOptions}
            isCombine
          />

          <TextBoxControl
            name="zipCode_4"
            label={t(I18N_TEXT.ZIP_CODE)}
            className="col-4 mt-lg-8 mt-md-16"
            maxLength={MAX_LENGTH_10}
            regex={NUMERIC_REPLACE_REGEX}
          />
        </div>
      </div>
    </div>
  );
};

export default PreviousAddresses;
