import React from 'react';

// components
import { TextBoxControl } from 'app/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import {
  I18N_TEXT,
  ALPHA_NUMERIC_SPACE_REPLACE_REGEX,
  MAX_LENGTH_35
} from 'app/constants';

const ReferencesInformation = () => {
  const { t } = useTranslation();

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.REFERENCES_INFO)}</h5>
      </div>

      <p className="col-12 mt-8">{t('txt_subtitle_references_info')}</p>

      <TextBoxControl
        name="reference1"
        label={`${t(I18N_TEXT.REFERENCE_1)}`}
        className="col-6 mt-16"
        maxLength={MAX_LENGTH_35}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <TextBoxControl
        name="reference2"
        label={`${t(I18N_TEXT.REFERENCE_2)}`}
        className="col-6 mt-16"
        maxLength={MAX_LENGTH_35}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <TextBoxControl
        name="reference3"
        label={`${t(I18N_TEXT.REFERENCE_3)}`}
        className="col-6 mt-16"
        maxLength={MAX_LENGTH_35}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <TextBoxControl
        name="reference4"
        label={`${t(I18N_TEXT.REFERENCE_4)}`}
        className="col-6 mt-16"
        maxLength={MAX_LENGTH_35}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />
    </div>
  );
};

export default ReferencesInformation;
