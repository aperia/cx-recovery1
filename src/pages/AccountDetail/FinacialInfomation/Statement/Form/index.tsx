import React, { useCallback, useRef, useState } from 'react';
import { useDispatch } from 'react-redux';

// components
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle,
  SimpleBar
} from 'app/_libraries/_dls/components';
import HomeInformation from './HomeInformation';
import AutomobileInformation from './AutomobileInformation';
import MonthlyAveragePayments from './MonthlyAveragePayments';
import ChildcareInformation from './ChildcareInformation';
import OtherAssets from './OtherAssets';
import Comment from './Comment';
import BankInformation from './BankInformation';
import PreviousAddresses from './PreviousAddresses';
import RelativesInformation from './RelativesInformation';
import ReferencesInformation from './ReferencesInformation';
import LienInformation from './LienInformation';
import JumpLinks from 'app/components/JumpLinks';

// hooks
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useFinancialStatementForm from '../hooks/useFinancialStatementForm';

// actions
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { financialInformationActions } from '../../_redux';

// selectors
import {
  selectSelectedStatementForm,
  selectStatementFormIsOpen
} from '../../_redux/statement/selectors';

// constants
import { I18N_TEXT } from 'app/constants';
import { STATEMENT_SECTIONS } from '../constants';

// types
import { IFinancialStatement } from '../../types';

// helpers
import { debounce, isTouchedForm } from 'app/helpers';
import { FormikProvider } from 'formik';

export interface FinancialStatementFormProps {}
const FinancialStatementForm: React.FC<FinancialStatementFormProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  const simpleBarRef = useRef<any>();
  const [scrollDistance, setScrollDistance] = useState(0);

  const isOpen = useSelectorId<boolean>(selectStatementFormIsOpen);
  const selected = useSelectorId<IFinancialStatement | undefined>(
    selectSelectedStatementForm
  );

  const editMode = !!selected?.id;
  const { formik } = useFinancialStatementForm();

  const {
    submitForm,
    resetForm,
    setSubmitting,
    isSubmitting,
    isValid,
    touched,
    dirty
  } = formik;

  const handleCancelForm = useCallback(() => {
    resetForm();
    setSubmitting(false);

    dispatch(
      financialInformationActions.onToggleStatementFormModal({
        storeId,
        isOpen: false
      })
    );
  }, [dispatch, resetForm, setSubmitting, storeId]);

  const handleCancel = useCallback(() => {
    if (dirty && isTouchedForm(touched)) {
      dispatch(
        rootModalActions.open({
          title: I18N_TEXT.UNSAVED_CHANGE,
          body: I18N_TEXT.DISCARD_CHANGE_BODY,
          btnCancelText: I18N_TEXT.CONTINUE_EDITING,
          btnConfirmText: I18N_TEXT.DISCARD_CHANGE,
          onConfirm: handleCancelForm
        })
      );
      return;
    }

    handleCancelForm();
  }, [dirty, touched, handleCancelForm, dispatch]);

  const handleSubmit = useCallback(() => {
    setSubmitting(true);
    submitForm();
  }, [setSubmitting, submitForm]);

  const handleUpdateScrollPosition = debounce(
    () => {
      const scrollPosition = simpleBarRef?.current?.scrollTop;

      if (!isNaN(+scrollPosition)) {
        setScrollDistance(scrollPosition);
      }
    },
    300,
    { leading: false, trailing: true }
  );

  const handleSimpleBarOnScroll = useCallback(() => {
    handleUpdateScrollPosition();
  }, [handleUpdateScrollPosition]);

  return (
    <FormikProvider value={formik}>
      <Modal show={isOpen} loading={false} enforceFocus={false} full>
        <ModalHeader
          border
          closeButton
          className="border-bottom"
          onHide={handleCancel}
        >
          <ModalTitle className="text-capitalize">
            {editMode ? t('txt_edit_statement') : t('txt_add_statement')}
          </ModalTitle>
        </ModalHeader>
        <ModalBody noPadding>
          <div className="h-100">
            <SimpleBar
              scrollableNodeProps={{ ref: simpleBarRef }}
              onScroll={handleSimpleBarOnScroll}
            >
              <div className="d-flex">
                <div className="flex-1">
                  <div className="p-24 max-width-lg mx-auto">
                    <div id={STATEMENT_SECTIONS.HOME_INFO}>
                      <HomeInformation />
                    </div>
                    <div
                      id={STATEMENT_SECTIONS.AUTOMOBILE_INFO}
                      className="mt-24"
                    >
                      <AutomobileInformation />
                    </div>
                    <div
                      id={STATEMENT_SECTIONS.MONTHLY_AVERAGE_PAYMENTS}
                      className="mt-24"
                    >
                      <MonthlyAveragePayments />
                    </div>
                    <div
                      id={STATEMENT_SECTIONS.CHILDCARE_INFO}
                      className="mt-24"
                    >
                      <ChildcareInformation />
                    </div>
                    <div id={STATEMENT_SECTIONS.OTHER_ASSETS} className="mt-24">
                      <OtherAssets />
                    </div>
                    <div id={STATEMENT_SECTIONS.COMMENT} className="mt-24">
                      <Comment />
                    </div>
                    <div id={STATEMENT_SECTIONS.BANK_INFO} className="mt-24">
                      <BankInformation />
                    </div>
                    <div
                      id={STATEMENT_SECTIONS.PREVIOUS_ADDRESSES}
                      className="mt-24"
                    >
                      <PreviousAddresses />
                    </div>
                    <div
                      id={STATEMENT_SECTIONS.RELATIVES_INFO}
                      className="mt-24"
                    >
                      <RelativesInformation />
                    </div>
                    <div
                      id={STATEMENT_SECTIONS.REFERENCES_INFO}
                      className="mt-24"
                    >
                      <ReferencesInformation />
                    </div>
                    <div id={STATEMENT_SECTIONS.LIEN_INFO} className="mt-24">
                      <LienInformation />
                    </div>
                  </div>
                </div>

                <JumpLinks
                  className="jump-links statement-jump-content"
                  scrollDistance={scrollDistance}
                  initActiveIndex="0"
                >
                  <JumpLinks.Item
                    title="txt_home_info"
                    to={STATEMENT_SECTIONS.HOME_INFO}
                  />
                  <JumpLinks.Item
                    title="txt_automobile_info"
                    to={STATEMENT_SECTIONS.AUTOMOBILE_INFO}
                  />
                  <JumpLinks.Item
                    title="txt_monthly_average_payments"
                    to={STATEMENT_SECTIONS.MONTHLY_AVERAGE_PAYMENTS}
                  />
                  <JumpLinks.Item
                    title="txt_childcare_info"
                    to={STATEMENT_SECTIONS.CHILDCARE_INFO}
                  />
                  <JumpLinks.Item
                    title="txt_other_assets"
                    to={STATEMENT_SECTIONS.OTHER_ASSETS}
                  />
                  <JumpLinks.Item title="txt_comment" to="comment" />
                  <JumpLinks.Item
                    title="txt_bank_info"
                    to={STATEMENT_SECTIONS.BANK_INFO}
                  />
                  <JumpLinks.Item
                    title="txt_previous_addresses"
                    to={STATEMENT_SECTIONS.PREVIOUS_ADDRESSES}
                  />
                  <JumpLinks.Item
                    title="txt_relatives_info"
                    to={STATEMENT_SECTIONS.RELATIVES_INFO}
                  />

                  <JumpLinks.Item
                    title="txt_references_info"
                    to={STATEMENT_SECTIONS.REFERENCES_INFO}
                  />
                  <JumpLinks.Item
                    title="txt_lien_info"
                    to={STATEMENT_SECTIONS.LIEN_INFO}
                  />
                </JumpLinks>
              </div>
            </SimpleBar>
          </div>
        </ModalBody>
        <ModalFooter
          border
          cancelButtonText={t(I18N_TEXT.CANCEL)}
          okButtonText={editMode ? t(I18N_TEXT.SAVE) : t(I18N_TEXT.SUBMIT)}
          onCancel={handleCancel}
          onOk={handleSubmit}
          disabledOk={isSubmitting || !isValid}
        ></ModalFooter>
      </Modal>
    </FormikProvider>
  );
};

export default FinancialStatementForm;
