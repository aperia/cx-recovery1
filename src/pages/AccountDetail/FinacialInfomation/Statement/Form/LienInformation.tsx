import React, { useCallback } from 'react';

// components
import {
  DatePickerControl,
  DropdownControl,
  TextBoxControl
} from 'app/components';
import If from 'pages/__commons/If';
import { GroupRadioControl } from 'app/components/FormControl/GroupRadio';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import {
  ALPHA_NUMERIC_SPACE_REPLACE_REGEX,
  CUSTOM,
  I18N_TEXT,
  MAX_LENGTH_13,
  MAX_LENGTH_20,
  MAX_LENGTH_3,
  MAX_LENGTH_40,
  NUMERIC_REPLACE_REGEX
} from 'app/constants';

// helpers
import { useFormikContext } from 'formik';
import {
  ACCEPT_OR_REJECT_TYPE_REF_DATA,
  PAYMENT_SCHEDULE_FREQUENCY_REF_DATA,
  SchedulePaymentType,
  SCHEDULE_PAYMENT_TYPE_REF_DATA
} from '../constants';

const LienInformation = () => {
  const { t } = useTranslation();

  const schedulePaymentTypeOptions = SCHEDULE_PAYMENT_TYPE_REF_DATA;
  const acceptOrRejectTypeOptions = ACCEPT_OR_REJECT_TYPE_REF_DATA;
  const paymentScheduleFrequencyOptions = PAYMENT_SCHEDULE_FREQUENCY_REF_DATA;

  const { handleChange, values, setFieldValue, handleBlur, setValues } =
    useFormikContext();

  const { schedulePaymentType, paymentScheduleFrequency } = values as any;

  const handlePaymentScheduleFrequencyChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      // trigger change formik's value
      const item = e.value as any;
      const event = {
        ...e,
        target: {
          ...e.target,
          value: item.value
        }
      };

      handleChange(event);

      setFieldValue('paymentScheduleFrequencyCustom', '', true);
    },
    [handleChange, setFieldValue]
  );

  const handleCheckboxChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      handleChange(event);
    },
    [handleChange]
  );

  const handleSchedulePaymentTypeChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      handleChange(event);
      handleBlur(event);

      const { value } = event.target;

      // reset these fields when schedulePaymentType equal No
      if (value === SchedulePaymentType.NO) {
        const newValues = {
          values: {
            ...(values as any),
            schedulePaymentType: SchedulePaymentType.NO,
            paymentScheduleFrequency: undefined,
            paymentScheduleFrequencyCustom: undefined,
            paymentScheduleStartDate: undefined,
            paymentScheduleAmount: undefined,
            realEstate: undefined,
            acceptOrRejectType: undefined
          }
        };

        setValues(newValues);
      }
    },
    [handleBlur, handleChange, setValues, values]
  );

  const renderPaymentScheduleFrequency = useCallback(() => {
    return (
      <div className="col-6">
        <div className="row">
          <If
            condition={(paymentScheduleFrequency + '').toLowerCase() !== CUSTOM}
          >
            <DropdownControl
              name="paymentScheduleFrequency"
              label={t(I18N_TEXT.SCHEDULED_PAYMENT_FREQUENCY)}
              className="col-12 mt-16"
              options={paymentScheduleFrequencyOptions}
              onValueChange={handlePaymentScheduleFrequencyChange}
              required
            />
          </If>

          <If
            condition={(paymentScheduleFrequency + '').toLowerCase() === CUSTOM}
          >
            <DropdownControl
              name="paymentScheduleFrequency"
              label={t(I18N_TEXT.SCHEDULED_PAYMENT_FREQUENCY)}
              className="col-8 mt-16"
              options={paymentScheduleFrequencyOptions}
              required
            />
            <TextBoxControl
              name="paymentScheduleFrequencyCustom"
              label={t(I18N_TEXT.COUNT)}
              className="col-4 mt-16"
              maxLength={MAX_LENGTH_3}
              regex={NUMERIC_REPLACE_REGEX}
              suffixOnFocus={t(I18N_TEXT.DAYS).toLowerCase()}
              required
            />
          </If>
        </div>
      </div>
    );
  }, [
    handlePaymentScheduleFrequencyChange,
    paymentScheduleFrequency,
    paymentScheduleFrequencyOptions,
    t
  ]);

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.LIEN_INFO)}</h5>
      </div>

      <p className="col-12 mt-8">{t('txt_subtitle_lien_info')}</p>

      <TextBoxControl
        name="lienholder1"
        label={t(I18N_TEXT.LIEN_HOLDER_1)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_20}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <TextBoxControl
        name="lienholder2"
        label={t(I18N_TEXT.LIEN_HOLDER_2)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_20}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <TextBoxControl
        name="lienholder3"
        label={t(I18N_TEXT.LIEN_HOLDER_3)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_20}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <TextBoxControl
        name="lienholder4"
        label={t(I18N_TEXT.LIEN_HOLDER_4)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_20}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <TextBoxControl
        name="lienholder5"
        label={t(I18N_TEXT.LIEN_HOLDER_5)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_20}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <TextBoxControl
        name="lienholder6"
        label={t(I18N_TEXT.LIEN_HOLDER_6)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_20}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <div className="col-12 mt-16">
        <div className="d-flex">
          <p className="fw-500 color-grey mr-24">
            {t(I18N_TEXT.SCHEDULED_PAYMENT)}:
          </p>
          <GroupRadioControl
            name="schedulePaymentType"
            direction="horizontal"
            options={schedulePaymentTypeOptions}
            onValueChange={handleSchedulePaymentTypeChange}
          />
        </div>
      </div>

      <If condition={schedulePaymentType === SchedulePaymentType.YES}>
        <div className="col-12">
          <div className="col-12 bg-light-l20 border br-light-l04 br-radius-4 p-16 mt-16">
            <div className="row">
              {renderPaymentScheduleFrequency()}

              <DatePickerControl
                name="paymentScheduleStartDate"
                label={t(I18N_TEXT.SCHEDULED_PAYMENT_START_DATE)}
                className="col-6 mt-16 text-transform-none"
                required
              />

              <FormikNumericControl
                name="paymentScheduleAmount"
                label={t(I18N_TEXT.SCHEDULED_PAYMENT_AMOUNT)}
                className="col-6 mt-16"
                maxLength={MAX_LENGTH_13}
                format="c2"
                required
              />

              <TextBoxControl
                name="realEstate"
                label={t(I18N_TEXT.REAL_ESTATE)}
                className="col-6 mt-16"
                maxLength={MAX_LENGTH_40}
                regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
                required
              />

              <div className="col-12 mt-16">
                <div className="d-flex">
                  <p className="fw-500 color-grey mr-24">
                    {t(I18N_TEXT.ACCEPT_OR_REJECT)}:
                    <span className="ml-4 color-red">*</span>
                  </p>
                  <GroupRadioControl
                    name="acceptOrRejectType"
                    direction="horizontal"
                    options={acceptOrRejectTypeOptions}
                    onValueChange={handleCheckboxChange}
                  />
                </div>
              </div>
            </div>
          </div>
        </div>
      </If>
    </div>
  );
};

export default LienInformation;
