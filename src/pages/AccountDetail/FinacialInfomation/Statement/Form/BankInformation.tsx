import React from 'react';

// components
import { TextBoxControl } from 'app/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import {
  ALPHA_NUMERIC_SPACE_REPLACE_REGEX,
  I18N_TEXT,
  MAX_LENGTH_20
} from 'app/constants';

const BankInformation = () => {
  const { t } = useTranslation();

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.BANK_INFO)}</h5>
      </div>
      <p className="col-12 mt-8">{t('txt_subtitle_bank_info')}</p>

      <TextBoxControl
        name="bankInstitution1"
        label={t(I18N_TEXT.BANK_INSTITUTION_1)}
        className="col-6 mt-16"
        maxLength={MAX_LENGTH_20}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />
      <TextBoxControl
        name="bankInstitution2"
        label={t(I18N_TEXT.BANK_INSTITUTION_2)}
        className="col-6 mt-16"
        maxLength={MAX_LENGTH_20}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />
      <TextBoxControl
        name="bankInstitution3"
        label={t(I18N_TEXT.BANK_INSTITUTION_3)}
        className="col-6 mt-16"
        maxLength={MAX_LENGTH_20}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />
    </div>
  );
};

export default BankInformation;
