import React from 'react';

// components
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_TEXT, MAX_LENGTH_13 } from 'app/constants';

const MonthlyAveragePayments = () => {
  const { t } = useTranslation();

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.MONTHLY_AVERAGE_PAYMENTS)}</h5>
      </div>

      <FormikNumericControl
        name="electric"
        label={t(I18N_TEXT.ELECTRIC)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />

      <FormikNumericControl
        name="gas"
        label={t(I18N_TEXT.GAS)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />

      <FormikNumericControl
        name="water"
        label={t(I18N_TEXT.WATER)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />

      <FormikNumericControl
        name="phone"
        label={t(I18N_TEXT.PHONE)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />

      <FormikNumericControl
        name="cableTV"
        label={t(I18N_TEXT.CABLE_TV)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />

      <FormikNumericControl
        name="lunch"
        label={t(I18N_TEXT.LUNCH)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />

      <FormikNumericControl
        name="food"
        label={t(I18N_TEXT.FOOD)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />
    </div>
  );
};

export default MonthlyAveragePayments;
