import React, { useCallback } from 'react';

// components
import { TextBoxControl } from 'app/components';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';
import { GroupRadioControl } from 'app/components/FormControl/GroupRadio';
// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import {
  I18N_TEXT,
  ALPHA_NUMERIC_REPLACE_REGEX,
  ALPHA_NUMERIC_SPACE_REPLACE_REGEX,
  MAX_LENGTH_10,
  MAX_LENGTH_13,
  MAX_LENGTH_15,
  MAX_LENGTH_4,
  MAX_LENGTH_40,
  MAX_LENGTH_5,
  NUMERIC_REPLACE_REGEX
} from 'app/constants';

// helpers
import { useFormikContext } from 'formik';
import { REIMBURSEMENT_REF_DATA } from '../constants';

const AutomobileInformation = () => {
  const { t } = useTranslation();
  const reimbursementTypeOptions = REIMBURSEMENT_REF_DATA;

  const { handleChange, setFieldValue, values } = useFormikContext();

  const { reimbursementType } = values as any;

  const handleCheckboxChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      handleChange(event);
      const { name, value } = event.target;

      if (name === 'reimbursementType') {
        if (value.toLowerCase() === 'n') {
          setFieldValue('reimbursementAmount', '', true);
        }
      }
    },
    [handleChange, setFieldValue]
  );

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.AUTOMOBILE_INFO)}</h5>
      </div>

      <TextBoxControl
        name="brand"
        label={t(I18N_TEXT.BRAND)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_5}
        regex={ALPHA_NUMERIC_REPLACE_REGEX}
      />

      <TextBoxControl
        name="model"
        label={t(I18N_TEXT.MODEL)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_5}
        regex={ALPHA_NUMERIC_REPLACE_REGEX}
      />

      <TextBoxControl
        name="modelYear"
        label={t(I18N_TEXT.MODEL_YEAR)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_4}
        regex={NUMERIC_REPLACE_REGEX}
      />

      <TextBoxControl
        name="licensePlateNumber"
        label={t(I18N_TEXT.LICENSE_PLATE_NUMBER)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_10}
        regex={ALPHA_NUMERIC_REPLACE_REGEX}
      />

      <FormikNumericControl
        name="monthlyGas"
        label={t(I18N_TEXT.MONTHLY_GAS)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />

      <FormikNumericControl
        name="monthlyInsurance"
        label={t(I18N_TEXT.MONTHLY_INSURANCE)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />

      <TextBoxControl
        name="title"
        label={t(I18N_TEXT.TITLE)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_15}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <TextBoxControl
        name="driverLicense"
        label={t(I18N_TEXT.DRIVER_LICENSE)}
        className="col-4 col-lg-3 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_15}
        regex={NUMERIC_REPLACE_REGEX}
      />

      <div className="col-12 mt-16">
        <div className="d-flex ">
          <p className="fw-500 color-grey mr-24">
            {t(I18N_TEXT.REIMBURSEMENT)}:
          </p>
          <GroupRadioControl
            name="reimbursementType"
            direction="horizontal"
            options={reimbursementTypeOptions}
            onValueChange={handleCheckboxChange}
          />
        </div>
      </div>

      <FormikNumericControl
        name="reimbursementAmount"
        label={t(I18N_TEXT.REIMBURSEMENT_AMOUNT)}
        className="col-8 col-lg-6 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
        disabled={(reimbursementType + '').toLowerCase() === 'n'}
      />
      <div className="col-4 col-lg-6"></div>

      <TextBoxControl
        name="ai_lienholder1"
        label={t(I18N_TEXT.LIEN_HOLDER_1)}
        className="col-4 mt-16"
        maxLength={MAX_LENGTH_40}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <FormikNumericControl
        name="monthlyPaymentAmount1"
        label={t(I18N_TEXT.MONTHLY_PAYMENT_AMOUNT_1)}
        className="col-4 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />

      <FormikNumericControl
        name="loanBalance1"
        label={t(I18N_TEXT.LOAN_BALANCE_1)}
        className="col-4 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_15}
        format="c2"
      />

      <TextBoxControl
        name="ai_lienholder2"
        label={t(I18N_TEXT.LIEN_HOLDER_2)}
        className="col-4 mt-16"
        maxLength={MAX_LENGTH_40}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <FormikNumericControl
        name="monthlyPaymentAmount2"
        label={t(I18N_TEXT.MONTHLY_PAYMENT_AMOUNT_2)}
        className="col-4 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />

      <FormikNumericControl
        name="loanBalance2"
        label={t(I18N_TEXT.LOAN_BALANCE_2)}
        className="col-4 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_15}
        format="c2"
      />
    </div>
  );
};

export default AutomobileInformation;
