import React from 'react';

// components
import { DropdownControl, TextBoxControl } from 'app/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import {
  I18N_TEXT,
  ALPHA_NUMERIC_SPACE_REPLACE_REGEX,
  MAX_LENGTH_13,
  MAX_LENGTH_40
} from 'app/constants';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';
import { CHILDCARE_PAYMENT_FREQUENCY_REF_DATA } from '../constants';

const ChildcareInformation = () => {
  const { t } = useTranslation();
  const childcarePaymentFrequencyOptions = CHILDCARE_PAYMENT_FREQUENCY_REF_DATA;

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.CHILDCARE_INFO)}</h5>
      </div>

      <TextBoxControl
        name="childcareProviderName"
        label={t(I18N_TEXT.CHILDCARE_PROVIDER_NAME)}
        className="col-6 col-lg-4 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_40}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <DropdownControl
        name="childcarePaymentFrequency"
        label={t(I18N_TEXT.CHILDCARE_PAYMENT_FREQUENCY)}
        className="col-6 col-lg-4 mt-16"
        options={childcarePaymentFrequencyOptions}
      />

      <FormikNumericControl
        name="childcareAmount"
        label={t(I18N_TEXT.CHILDCARE_AMOUNT)}
        className="col-6 col-lg-4 mt-16 text-capitalize"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />
    </div>
  );
};

export default ChildcareInformation;
