import React, { useCallback, useEffect, useState } from 'react';
import { useDispatch } from 'react-redux';

// components
import { NoDataGrid } from 'app/components';
import { Button } from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';
import If from 'pages/__commons/If';
import FinancialStatementForm from './Form';
import SectionControl from 'app/components/SectionControl';
import StatementConfirmBody from './StatementConfirmBody';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useSelectorId } from 'app/hooks';

// actions
import { financialInformationActions } from '../_redux';
import {
  selectIsLoadingStatement,
  selectSelectedStatementForm,
  selectSnapshotStatementForm
} from '../_redux/statement/selectors';

// constants
import { I18N_TEXT } from 'app/constants';
import { STATEMENT_VIEW_SECTIONS } from './constants';

// types
import { IFinancialStatement, ISnapshotStatement } from '../types';

// helpers
import { isEmpty, isUndefined } from 'lodash';
import classnames from 'classnames';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

const Statement: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId, accNumber } = useAccountDetail();

  const loading = useSelectorId<boolean>(selectIsLoadingStatement);

  const selected = useSelectorId<IFinancialStatement | undefined>(
    selectSelectedStatementForm
  );

  const snapShotData = useSelectorId<ISnapshotStatement | undefined>(
    selectSnapshotStatementForm
  );

  const [expandViewAll, setExpandViewAll] = useState(true);

  const handleShowEditForm = useCallback(() => {
    dispatch(
      financialInformationActions.onToggleStatementFormModal({
        storeId,
        selected,
        isOpen: true
      })
    );
  }, [dispatch, selected, storeId]);

  const handleShowConfirmDelete = useCallback(() => {
    dispatch(
      rootModalActions.open({
        autoClose: false,
        title: I18N_TEXT.DELETE_STATEMENT_CONFIRM_TITLE,
        body: <StatementConfirmBody data={selected} />,
        btnConfirmText: I18N_TEXT.DELETE,
        btnCancelText: I18N_TEXT.CANCEL,
        onConfirm: () => {
          dispatch(rootModalActions.loading(true));

          dispatch(
            financialInformationActions.deleteStatement({
              storeId,
              accNumber,
              id: selected?.id || ''
            })
          );
        },
        onCancel: () => {
          dispatch(rootModalActions.close());
        }
      })
    );
  }, [accNumber, dispatch, selected, storeId]);

  useEffect(() => {
    dispatch(
      financialInformationActions.getStatement({
        accNumber: accNumber,
        storeId
      })
    );
  }, [dispatch, storeId, accNumber]);

  return (
    <div className={classnames('', { loading })}>
      <If condition={isEmpty(snapShotData)}>
        <div className="d-flex justify-content-center align-items-center w-100">
          <NoDataGrid text={I18N_TEXT.NO_DATA_TO_DISPLAY}>
            <Button
              className="text-capitalize mt-24"
              variant="outline-primary"
              size="sm"
              onClick={() => {
                dispatch(
                  financialInformationActions.onToggleStatementFormModal({
                    storeId,
                    isOpen: true
                  })
                );
              }}
            >
              {t('txt_add_statement')}
            </Button>
          </NoDataGrid>
        </div>
      </If>

      <If condition={!isEmpty(snapShotData)}>
        <>
          <div className="d-flex align-items-center pt-24">
            <h5>{t('txt_financial_statement')}</h5>
            <span className="ml-auto">
              <Button
                size="sm"
                variant="outline-danger"
                onClick={handleShowConfirmDelete}
              >
                {t(I18N_TEXT.DELETE)}
              </Button>
              <Button
                size="sm"
                variant="outline-primary"
                onClick={handleShowEditForm}
              >
                {t(I18N_TEXT.EDIT)}
              </Button>
              <Button
                size="sm"
                className="mr-n8"
                variant="outline-primary"
                onClick={() => setExpandViewAll(!expandViewAll)}
              >
                {t(
                  expandViewAll ? I18N_TEXT.COLLAPSE_ALL : I18N_TEXT.EXPAND_ALL
                )}
              </Button>
            </span>
          </div>
          {STATEMENT_VIEW_SECTIONS.map(section => {
            return (
              <div key={section.id} className="mt-24">
                <SectionControl
                  title={t(section.title)}
                  sectionKey={`section-${section.viewId}`}
                  classNamesView="ml-24"
                  isExpand={expandViewAll}
                  isTitleNormal={true}
                >
                  {/* {
                    !isEmpty(
                      snapShotData![section.id as keyof ISnapshotStatement]
                    ) ? (
                      <View
                      id={section.viewId}
                      formKey={section.viewId}
                      descriptor={section.viewId}
                      value={snapShotData}
                    />
                    ) : t(I18N_TEXT.NO_DATA_TO_DISPLAY)
                  } */}
                  <If
                    condition={
                      isUndefined(snapShotData) ||
                      (!isUndefined(snapShotData) &&
                        isEmpty(
                          snapShotData[section.id as keyof ISnapshotStatement]
                        ))
                    }
                  >
                    <p className="mt-16 color-grey">
                      {t(I18N_TEXT.NO_DATA_TO_DISPLAY)}
                    </p>
                  </If>
                  <If
                    condition={
                      !isUndefined(snapShotData) &&
                      !isEmpty(
                        snapShotData[section.id as keyof ISnapshotStatement]
                      )
                    }
                  >
                    <View
                      id={section.viewId}
                      formKey={section.viewId}
                      descriptor={section.viewId}
                      value={snapShotData}
                    />
                  </If>
                </SectionControl>
              </div>
            );
          })}
        </>
      </If>

      <FinancialStatementForm />
    </div>
  );
};

export default Statement;
