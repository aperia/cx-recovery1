import { I18N_TEXT } from 'app/constants';

export const STATEMENT_FIELD = {
  // Lien Information
  SCHEDULE_PAYMENT: {
    name: 'schedulePaymentType'
    // label: I18N_TEXT.SCHEDULE_PAYMENT
  },
  PAYMENT_SCHEDULE_FREQUENCY: {
    name: 'paymentScheduleFrequency',
    label: I18N_TEXT.PAYMENT_SCHEDULE_FREQUENCY
  },
  PAYMENT_SCHEDULE_FREQUENCY_CUSTOM: {
    name: 'paymentScheduleFrequencyCustom',
    label: I18N_TEXT.COUNT
  },
  PAYMENT_SCHEDULE_START_DATE: {
    name: 'paymentScheduleStartDate',
    label: I18N_TEXT.PAYMENT_SCHEDULE_START_DATE
  },
  PAYMENT_SCHEDULE_AMOUNT: {
    name: 'paymentScheduleAmount',
    label: I18N_TEXT.PAYMENT_SCHEDULE_AMOUNT
  },
  REAL_ESTATE: {
    name: 'realEstate'
    // label: I18N_TEXT.REAL_ESTATE
  },
  ACCEPT_OR_REJECT_TYPE: {
    name: 'acceptOrRejectType'
    // label: I18N_TEXT.ACCEPT_OR_REJECT_TYPE
  }
};

export enum SchedulePaymentType {
  YES = 'Y',
  NO = 'N'
}

export const STATEMENT_VIEW_SECTIONS = [
  {
    id: 'homeInfo',
    viewId: 'fsHomeInfo',
    title: 'txt_home_info'
  },
  {
    id: 'automobileInfo',
    viewId: 'fsAutomobileInfo',
    title: 'txt_automobile_info'
  },
  {
    id: 'monthlyAveragePayments',
    viewId: 'fsMonthlyAveragePayments',
    title: 'txt_monthly_average_payments'
  },
  {
    id: 'childcareInfo',
    viewId: 'fsChildcareInfo',
    title: 'txt_childcare_info'
  },
  {
    id: 'otherAssets',
    viewId: 'fsOtherAssets',
    title: 'txt_other_assets'
  },
  {
    id: 'comment',
    viewId: 'fsComment',
    title: 'txt_comment'
  },
  {
    id: 'bankInfo',
    viewId: 'fsBankInfo',
    title: 'txt_bank_info'
  },
  {
    id: 'previousAddresses',
    viewId: 'fsPreviousAddresses',
    title: 'txt_previous_addresses'
  },
  {
    id: 'relativesInfo',
    viewId: 'fsRelativesInfo',
    title: 'txt_relatives_info'
  },
  {
    id: 'referencesInfo',
    viewId: 'fsReferencesInfo',
    title: 'txt_references_info'
  },
  {
    id: 'lienInfo',
    viewId: 'fsLienInfo',
    title: 'txt_lien_info'
  }
];

export const OWNER_OR_RENTER_REF_DATA = [
  {
    value: 'B',
    description: 'B - The debtor owns a home',
    show: true
  },
  {
    value: 'R',
    description: 'R - The debtor rents a home',
    show: true
  }
];

export const REIMBURSEMENT_REF_DATA = [
  {
    value: 'Y',
    description: 'Y - Yes, the debtor receives reimbursement.',
    show: true
  },
  {
    value: 'N',
    description:
      'N - No, the debtor does not receive any reimbursement for car expenses.',
    show: true
  }
];

export const CHILDCARE_PAYMENT_FREQUENCY_REF_DATA = [
  { value: 'daily', description: 'Daily', show: true },
  { value: 'monthly', description: 'Monthly', show: true },
  { value: 'yearly', description: 'Yearly', show: true }
];

export const FINAL_JUDGMENT_TYPE_REF_DATA = [
  {
    value: 'Y',
    description:
      'Y - Yes, the debtor holds a final judgment against someone and receives payments.',
    show: true
  },
  {
    value: 'N',
    description: 'N - No, the debtor does not hold a final judgment.',
    show: true
  }
];

export const SCHEDULE_PAYMENT_TYPE_REF_DATA = [
  {
    value: SchedulePaymentType.YES,
    description: 'Yes',
    show: true
  },
  {
    value: SchedulePaymentType.NO,
    description: 'No',
    show: true
  }
];

export const PAYMENT_SCHEDULE_FREQUENCY_REF_DATA = [
  { value: 'once', description: 'Once', show: true },
  { value: '1', description: '1 day', show: true },
  { value: '7', description: '7 days', show: true },
  { value: '14', description: '14 days', show: true },
  { value: '30', description: '30 days', show: true },
  { value: '60', description: '60 days', show: true },
  { value: 'custom', description: 'Custom', show: true }
];

export const ACCEPT_OR_REJECT_TYPE_REF_DATA = [
  {
    value: 'A',
    description: 'A - The lien payment plan is accepted',
    show: true
  },
  {
    value: 'R',
    description: 'R - The lien payment plan is rejected',
    show: true
  }
];

export const STATEMENT_SECTIONS = {
  HOME_INFO: 'home_info',
  AUTOMOBILE_INFO: 'automobile_info',
  MONTHLY_AVERAGE_PAYMENTS: 'monthly_average_payments',
  CHILDCARE_INFO: 'childcare_info',
  OTHER_ASSETS: 'other_assets',
  COMMENT: 'comment',
  BANK_INFO: 'bank_info',
  PREVIOUS_ADDRESSES: 'previous_addresses',
  RELATIVES_INFO: 'relatives_info',
  REFERENCES_INFO: 'references_info',
  LIEN_INFO: 'lien_info'
};
