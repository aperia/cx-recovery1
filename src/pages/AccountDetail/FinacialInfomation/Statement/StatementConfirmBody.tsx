import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// types
import { IFinancialStatement } from '../types';

// helper
import { I18N_TEXT } from 'app/constants';
import If from 'pages/__commons/If';

export interface StatementConfirmBodyProps {
  data?: IFinancialStatement;
}

const StatementConfirmBody: React.FC<StatementConfirmBodyProps> = ({
  data
}) => {
  const { t } = useTranslation();

  return (
    <If condition={!!data}>
      <>
        <p>{t(I18N_TEXT.DELETE_STATEMENT_CONFIRM_BODY)}</p>
      </>
    </If>
  );
};

export default StatementConfirmBody;
