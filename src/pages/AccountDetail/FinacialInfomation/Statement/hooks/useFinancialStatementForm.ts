import { useDispatch } from 'react-redux';

// hooks
import useValidation from './useValidation';
import { useAccountDetail, useSelectorId } from 'app/hooks';

// actions
import { financialInformationActions } from '../../_redux';

// selectors
import { selectSelectedStatementForm } from '../../_redux/statement/selectors';

// types
import { IFinancialStatement } from '../../types';

// constants

// helpers
import { useFormik } from 'formik';
import { SchedulePaymentType } from '../constants';

const useFinancialStatementForm = () => {
  const dispatch = useDispatch();
  const { storeId, accNumber } = useAccountDetail();
  const { validationSchema } = useValidation();

  const selected = useSelectorId<IFinancialStatement | undefined>(
    selectSelectedStatementForm
  );

  const selectedId = selected?.id;

  const defaultValues = {
    reimbursementType: 'N',
    finalJudgmentType: 'N',
    schedulePaymentType: SchedulePaymentType.NO
  };

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      ...defaultValues,
      ...(selected ? { ...selected } : {})
    },
    onSubmit: (values, formikBag) => {
      dispatch(
        financialInformationActions.saveStatement({
          storeId,
          accNumber,
          id: selectedId,
          values,
          formikBag
        })
      );
    },
    validationSchema
  });

  return { formik };
};

export default useFinancialStatementForm;
