// helpers
import * as Yup from 'yup';

// constants
import { CUSTOM, I18N_TEXT } from 'app/constants';
import { SchedulePaymentType, STATEMENT_FIELD } from '../constants';

const useValidation = () => {
  const validationSchema = Yup.object().shape({
    // Lien Information
    [STATEMENT_FIELD.PAYMENT_SCHEDULE_FREQUENCY.name]: Yup.string().when(
      [STATEMENT_FIELD.SCHEDULE_PAYMENT.name],
      {
        is: SchedulePaymentType.YES,
        then: Yup.string().required(
          I18N_TEXT.SCHEDULED_PAYMENT_FREQUENCY_REQUIRED
        )
      }
    ),
    [STATEMENT_FIELD.PAYMENT_SCHEDULE_FREQUENCY_CUSTOM.name]: Yup.string().when(
      [STATEMENT_FIELD.PAYMENT_SCHEDULE_FREQUENCY.name],
      (value: string, schema: Yup.AnySchema) =>
        value === CUSTOM
          ? schema.required(
              I18N_TEXT.PAYMENT_SCHEDULE_FREQUENCY_CUSTOM_REQUIRED
            )
          : schema
    ),
    [STATEMENT_FIELD.PAYMENT_SCHEDULE_START_DATE.name]: Yup.string().when(
      [STATEMENT_FIELD.SCHEDULE_PAYMENT.name],
      {
        is: SchedulePaymentType.YES,
        then: Yup.string().required(
          I18N_TEXT.SCHEDULED_PAYMENT_START_DATE_REQUIRED
        )
      }
    ),
    [STATEMENT_FIELD.PAYMENT_SCHEDULE_AMOUNT.name]: Yup.string().when(
      [STATEMENT_FIELD.SCHEDULE_PAYMENT.name],
      {
        is: SchedulePaymentType.YES,
        then: Yup.string().required(I18N_TEXT.SCHEDULED_PAYMENT_AMOUNT_REQUIRED)
      }
    ),
    [STATEMENT_FIELD.REAL_ESTATE.name]: Yup.string().when(
      [STATEMENT_FIELD.SCHEDULE_PAYMENT.name],
      {
        is: SchedulePaymentType.YES,
        then: Yup.string().required(I18N_TEXT.REAL_ESTATE_REQUIRED)
      }
    ),
    [STATEMENT_FIELD.ACCEPT_OR_REJECT_TYPE.name]: Yup.string().when(
      [STATEMENT_FIELD.SCHEDULE_PAYMENT.name],
      {
        is: SchedulePaymentType.YES,
        then: Yup.string().required(I18N_TEXT.ACCEPT_OR_REJECT_TYPE_REQUIRED)
      }
    )
  });

  return {
    validationSchema
  };
};

export default useValidation;
