import React from 'react';
//components
import { HorizontalTabs } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_TEXT } from 'app/constants';
import { tabFinancialList } from '../constants';

interface FinancialInformationProps {
  defaultTabId: string;
}
const FinancialInformation: React.FC<FinancialInformationProps> = ({
  defaultTabId
}) => {
  const { t } = useTranslation();
  const renderListTab = tabFinancialList.map(
    ({ key, eventKey, title, component: Component }: MagicKeyValue) => {
      return (
        <HorizontalTabs.Tab key={key} eventKey={eventKey} title={t(title)}>
          <Component />
        </HorizontalTabs.Tab>
      );
    }
  );
  const activeTabId: string = defaultTabId || tabFinancialList[0].eventKey;

  return (
    <div className="px-24 pt-24 max-width-lg mx-auto overlap-next-action">
      <h4 className="mb-16">{t(I18N_TEXT.FINANCIAL_INFORMATION)}</h4>
      <HorizontalTabs
        defaultActiveKey={activeTabId}
        mountOnEnter
        unmountOnExit
        level2
      >
        {renderListTab}
      </HorizontalTabs>
    </div>
  );
};

export default FinancialInformation;
