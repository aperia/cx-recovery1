import React, { useMemo, useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import classNames from 'classnames';

// components
import { NoDataGrid } from 'app/components';
import { Button } from 'app/_libraries/_dls/components';
import FinancialModal from './Form';
import { View } from 'app/_libraries/_dof/core';
import DeleteDemographicsModal from './DeleteModalBody';
import SectionControl from 'app/components/SectionControl';
import ChildTextView from './ChildTextView';

// redux
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { financialInformationActions } from '../_redux';
import {
  selectorDemographicsData,
  selectorExpandViewAllDemographics,
  selectorIsOpenModalFinancial,
  selectorLoadingDemographicsData
} from '../_redux/demographics/selectors';

// hooks
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { isEmpty } from 'app/helpers';

// constants & types
import { BREAK_POINT_VIEWPORT, I18N_TEXT } from 'app/constants';
import {
  DEMOGRAPHICS_CHILDREN_INFO_VIEW,
  DEMOGRAPHICS_VIEW_SECTIONS
} from './constants';
import { RootModalOptions } from 'pages/__commons/RootModal/types';
import { IChildrenInformation, IDemoGraphics } from '../types';

import { formatDataDemographics } from '../helpers';
import { useViewport } from 'app/hooks/useViewport';

const DemoGraphics: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const { width } = useViewport();
  const openAddModal: boolean = useSelectorId(selectorIsOpenModalFinancial);
  const expandViewAll: boolean = useSelectorId(
    selectorExpandViewAllDemographics
  );
  const demographicsData: IDemoGraphics = useSelectorId(
    selectorDemographicsData
  );
  const loading: boolean = useSelectorId(selectorLoadingDemographicsData);

  useEffect(() => {
    dispatch(financialInformationActions.getDemographics({ storeId }));
  }, [dispatch, storeId]);

  const showModalDelete = useCallback(() => {
    const options: RootModalOptions = {
      autoClose: false,
      size: 'xs',
      title: t(I18N_TEXT.CONFIRM_DELETE_DEMOGRAPHICS),
      body: <DeleteDemographicsModal />,
      onConfirm: () => {
        dispatch(financialInformationActions.deleteDemographics({ storeId }));
      },
      btnConfirmText: t(I18N_TEXT.DELETE),
      onCancel: () => dispatch(rootModalActions.close())
    };
    dispatch(rootModalActions.open(options));
  }, [t, dispatch, storeId]);

  const triggerEditDemographics = useCallback(() => {
    dispatch(
      financialInformationActions.onSetModalFinancial({
        storeId,
        isOpen: true
      })
    );
  }, [dispatch, storeId]);

  const renderHeadingChildInfoComponent = useCallback(
    (childInfoData: IChildrenInformation) => {
      const {
        numberOfChildren,
        child1Age,
        child2Age,
        child3Age,
        child4Age,
        child5Age,
        child6Age,
        child7Age,
        child8Age,
        child9Age,
        child10Age
      } = childInfoData;
      const textValueList = [
        child1Age,
        child2Age,
        child3Age,
        child4Age,
        child5Age,
        child6Age,
        child7Age,
        child8Age,
        child9Age,
        child10Age
      ];
      return (
        <>
          <ChildTextView
            label={t(I18N_TEXT.NUMBER_OF_CHILDREN)}
            textValue={numberOfChildren}
            className="col-12 mt-16 pl-0"
          />
          <div className="col-12 divider-dashed mt-12"></div>

          {Array.apply(0, Array(Number(numberOfChildren)))?.map((_, index) =>
            !isEmpty(textValueList[index]) ? (
              <ChildTextView
                label={t(`txt_child_age`, { numberOfChildren: index + 1 })}
                textValue={textValueList[index]}
                className={classNames('col-4 mt-16 d-inline-block pl-0', {
                  'col-lg-3': width > BREAK_POINT_VIEWPORT.BR_1024PX
                })}
              />
            ) : null
          )}
          {Number(numberOfChildren) > 0 ? (
            <div className="col-12 divider-dashed mt-12"></div>
          ) : null}
        </>
      );
    },
    [t, width]
  );

  const DemographicsDataView = useMemo(() => {
    if (isEmpty(demographicsData)) {
      return (
        <NoDataGrid text={t(I18N_TEXT.NO_DATA_TO_DISPLAY)}>
          <Button
            className="mt-24"
            variant="outline-primary"
            size="sm"
            onClick={() =>
              dispatch(
                financialInformationActions.onSetModalFinancial({
                  storeId,
                  isOpen: true
                })
              )
            }
          >
            {t(I18N_TEXT.ADD_FINANCIAL_DEMOGRAPHICS)}
          </Button>
        </NoDataGrid>
      );
    }
    return (
      <>
        <div className="d-flex align-items-center pt-24">
          <>
            <h5>{t(I18N_TEXT.FINANCIAL_DEMOGRAPHICS)}</h5>
            <div className="ml-auto mr-n8">
              <Button
                size="sm"
                variant="outline-danger"
                onClick={showModalDelete}
              >
                {t(I18N_TEXT.DELETE)}
              </Button>
              <Button
                size="sm"
                variant="outline-primary"
                onClick={triggerEditDemographics}
              >
                {t(I18N_TEXT.EDIT)}
              </Button>
              <Button
                size="sm"
                variant="outline-primary"
                onClick={() =>
                  dispatch(
                    financialInformationActions.onSetExpandViewAll({
                      storeId,
                      isExpand: !expandViewAll
                    })
                  )
                }
              >
                {t(
                  expandViewAll ? I18N_TEXT.COLLAPSE_ALL : I18N_TEXT.EXPAND_ALL
                )}
              </Button>
            </div>
          </>
        </div>
        {DEMOGRAPHICS_VIEW_SECTIONS.map(section => {
          return (
            <div key={section.id} className="mt-24">
              <SectionControl
                title={t(section.title)}
                sectionKey={`section-${section.viewId}`}
                classNamesView="ml-24"
                isExpand={expandViewAll}
                isTitleNormal={true}
              >
                {section.viewId === DEMOGRAPHICS_CHILDREN_INFO_VIEW
                  ? renderHeadingChildInfoComponent(
                      demographicsData![
                        section.dataField as keyof IDemoGraphics
                      ] as IChildrenInformation
                    )
                  : null}
                <View
                  id={section.viewId}
                  formKey={section.viewId}
                  descriptor={section.viewId}
                  value={{
                    ...demographicsData![
                      section.dataField as keyof IDemoGraphics
                    ],
                    ...formatDataDemographics(section.viewId, {
                      ...demographicsData![
                        section.dataField as keyof IDemoGraphics
                      ]
                    })
                  }}
                />
              </SectionControl>
            </div>
          );
        })}
      </>
    );
  }, [
    t,
    dispatch,
    showModalDelete,
    triggerEditDemographics,
    expandViewAll,
    demographicsData,
    storeId,
    renderHeadingChildInfoComponent
  ]);

  return (
    <div className={classNames({ loading })}>
      {DemographicsDataView}
      <FinancialModal
        open={openAddModal}
        storeId={storeId}
        isAdd={isEmpty(demographicsData)}
      />
    </div>
  );
};

export default DemoGraphics;
