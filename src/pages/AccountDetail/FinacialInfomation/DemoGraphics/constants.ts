import { I18N_TEXT } from 'app/constants';

export const DEMOGRAPHICS_DEBTOR_INFO_VIEW = 'demographicsDebtorInfo';
export const DEMOGRAPHICS_SPOUSE_INFO_VIEW = 'demographicsSpouseInfo';
export const DEMOGRAPHICS_CHILDREN_INFO_VIEW = 'demographicsChildrenInfo';

export const DEMOGRAPHICS_VIEW_SECTIONS = [
  {
    id: 'debtorInformation',
    viewId: DEMOGRAPHICS_DEBTOR_INFO_VIEW,
    title: I18N_TEXT.DEBTOR_INFORMATION,
    dataField: 'debtorInformation'
  },
  {
    id: 'spouseInformation',
    viewId: DEMOGRAPHICS_SPOUSE_INFO_VIEW,
    title: I18N_TEXT.SPOUSE_INFORMATION,
    dataField: 'spouseInformation'
  },
  {
    id: 'childrenInformation',
    viewId: DEMOGRAPHICS_CHILDREN_INFO_VIEW,
    title: I18N_TEXT.CHILDREN_INFORMATION,
    dataField: 'childrenInformation'
  }
];

export const DEMOGRAPHICS_FORM = {
  DEBTOR_INFO: {
    PRIMARY_DEBTOR: {
      primaryDebtorName: {
        name: 'primaryDebtorName',
        label: 'txt_primary_debtor_name'
      },
      addressLine1: {
        name: 'addressLine1',
        label: 'txt_address_line_1'
      },
      addressLine2: {
        name: 'addressLine2',
        label: 'txt_address_line_2'
      },
      city: {
        name: 'city',
        label: 'txt_city'
      },
      state: {
        name: 'state',
        label: 'txt_state'
      },
      zipCode: {
        name: 'zipCode',
        label: 'txt_zip_code'
      },
      homePhone: {
        name: 'primaryDebtorHomePhone',
        label: 'txt_home_phone'
      }
    },
    EMPLOYMENT: {
      employerName: {
        name: 'employerNameDebtor',
        label: 'txt_employer_name'
      },
      employerPhone: {
        name: 'employerPhoneDebtor',
        label: 'txt_employer_phone'
      },
      employerAddress: {
        name: 'addressEmploymentDebtor',
        label: 'txt_address'
      },
      contractType: {
        name: 'contractTypeDebtor',
        label: 'txt_contract_type',
        options: {
          contractType: [
            {
              value: 'F',
              description: 'txt_full_time'
            },
            {
              value: 'P',
              description: 'txt_part_time'
            },
            {
              value: 'None',
              description: 'txt_none'
            }
          ]
        }
      },
      salaryType: {
        name: 'salaryTypeDebtor',
        label: 'txt_salary_type',
        options: {
          salaryType: [
            {
              value: 'G',
              description: 'txt_gross'
            },
            {
              value: 'N',
              description: 'txt_net'
            }
          ]
        }
      },
      startDate: {
        name: 'startDateDebtor',
        label: 'txt_start_date'
      },
      lastPayDate: {
        name: 'lastPayDateDebtor',
        label: 'txt_last_pay_date'
      },
      nextPayDate: {
        name: 'nextPayDateDebtor',
        label: 'txt_next_pay_date'
      },
      city: {
        name: 'cityEmploymentDebtor',
        label: 'txt_city'
      },
      state: {
        name: 'stateEmploymentDebtor',
        label: 'txt_state'
      },
      zipCode: {
        name: 'zipCodeEmploymentDebtor',
        label: 'txt_zip_code'
      },
      netMonthlySalary: {
        name: 'netMonthlySalaryDebtor',
        label: 'txt_net_monthly_salary'
      },
      grossMonthlySalary: {
        name: 'grossMonthlySalaryDebtor',
        label: 'txt_gross_monthly_salary'
      },
      deduction: {
        name: 'deductionDebtor',
        label: 'txt_deduction'
      },
      comment: {
        name: 'comment',
        label: 'txt_comment'
      }
    }
  },
  SPOUSE_INFO: {
    SPOUSE: {
      spouseName: {
        name: 'spouseName',
        label: 'txt_spouse_name'
      },
      addressLine1: {
        name: 'spouseAddressLine1',
        label: 'txt_address_line_1'
      },
      addressLine2: {
        name: 'spouseAddressLine2',
        label: 'txt_address_line_2'
      },
      city: {
        name: 'citySpouse',
        label: 'txt_city'
      },
      state: {
        name: 'stateSpouse',
        label: 'txt_state'
      },
      zipCode: {
        name: 'zipCodeSpouse',
        label: 'txt_zip_code'
      },
      homePhone: {
        name: 'homePhoneSpouse',
        label: 'txt_home_phone'
      }
    },
    EMPLOYMENT: {
      employerName: {
        name: 'employerNameSpouse',
        label: 'txt_employer_name'
      },
      employerPhone: {
        name: 'employerPhoneSpouse',
        label: 'txt_employer_phone'
      },
      employerAddress: {
        name: 'addressEmploymentSpouse',
        label: 'txt_address'
      },
      contractType: {
        name: 'contractTypeSpouse',
        label: 'txt_contract_type',
        options: {
          contractType: [
            {
              value: 'F',
              description: 'txt_full_time'
            },
            {
              value: 'P',
              description: 'txt_part_time'
            },
            {
              value: 'None',
              description: 'txt_none'
            }
          ]
        }
      },
      salaryType: {
        name: 'salaryTypeSpouse',
        label: 'txt_salary_type',
        options: {
          salaryType: [
            {
              value: 'G',
              description: 'txt_gross'
            },
            {
              value: 'N',
              description: 'txt_net'
            }
          ]
        }
      },
      startDate: {
        name: 'startDateSpouse',
        label: 'txt_start_date'
      },
      lastPayDate: {
        name: 'lastPayDateSpouse',
        label: 'txt_last_pay_date'
      },
      nextPayDate: {
        name: 'nextPayDateSpouse',
        label: 'txt_next_pay_date'
      },
      city: {
        name: 'cityEmploymentSpouse',
        label: 'txt_city'
      },
      state: {
        name: 'stateEmploymentSpouse',
        label: 'txt_state'
      },
      zipCode: {
        name: 'zipCodeEmploymentSpouse',
        label: 'txt_zip_code'
      },
      netMonthlySalary: {
        name: 'netMonthlySalarySpouse',
        label: 'txt_net_monthly_salary'
      },
      grossMonthlySalary: {
        name: 'grossMonthlySalarySpouse',
        label: 'txt_gross_monthly_salary'
      },
      deduction: {
        name: 'deductionSpouse',
        label: 'txt_deduction'
      }
    }
  },
  CHILDREN_INFO: {
    numberOfChildren: {
      name: 'numberOfChildren',
      label: 'txt_number_of_children'
    },
    child1Age: {
      name: 'child1Age',
      label: 'txt_child_1_age'
    },
    child2Age: {
      name: 'child2Age',
      label: 'txt_child_2_age'
    },
    child3Age: {
      name: 'child3Age',
      label: 'txt_child_3_age'
    },
    child4Age: {
      name: 'child4Age',
      label: 'txt_child_4_age'
    },
    child5Age: {
      name: 'child5Age',
      label: 'txt_child_5_age'
    },
    child6Age: {
      name: 'child6Age',
      label: 'txt_child_6_age'
    },
    child7Age: {
      name: 'child7Age',
      label: 'txt_child_7_age'
    },
    child8Age: {
      name: 'child8Age',
      label: 'txt_child_8_age'
    },
    child9Age: {
      name: 'child9Age',
      label: 'txt_child_9_age'
    },
    child10Age: {
      name: 'child10Age',
      label: 'txt_child_10_age'
    },
    supportAmount: {
      name: 'supportAmount',
      label: 'txt_support_amount'
    },
    supportPersonName: {
      name: 'supportPersonName',
      label: 'txt_support_person_name'
    },
    supportCountry: {
      name: 'supportCountry',
      label: 'txt_support_country'
    },
    supportType: {
      name: 'supportType',
      label: 'txt_support_type',
      options: {
        supportType: [
          {
            value: 'P',
            description: 'txt_support_type_option_1'
          },
          {
            value: 'R',
            description: 'txt_support_type_option_2'
          },
          {
            value: 'None',
            description: 'txt_none'
          }
        ]
      }
    },
    supportMethod: {
      name: 'supportMethod',
      label: 'txt_support_method',
      options: {
        supportMethod: [
          {
            value: 'C',
            description: 'txt_support_method_option_1'
          },
          {
            value: 'D',
            description: 'txt_support_method_option_2'
          },
          {
            value: 'None',
            description: 'txt_none'
          }
        ]
      }
    }
  }
};

export const DEFAULT_VALUES_DEMOGRAPHICS_FORM = {
  // debtor
  primaryDebtorName: 'Smith Jackie',
  addressLine1: '2501 Main St',
  addressLine2: '',
  city: 'San Francisco',
  state: 'CA',
  zipCode: '97208',
  primaryDebtorHomePhone: '(123) 456-7890',

  employerNameDebtor: '',
  employerPhoneDebtor: '',
  employerAddressDebtor: '',
  addressEmploymentDebtor: '',
  cityEmploymentDebtor: '',
  stateEmploymentDebtor: '',
  zipCodeEmploymentDebtor: '',

  contractTypeDebtor: 'F',
  salaryTypeDebtor: 'G',
  startDateDebtor: '',
  lastPayDateDebtor: '',
  nextPayDateDebtor: '',
  deductionDebtor: '',
  comment: '',

  // spouse
  spouseName: '',
  spouseAddressLine1: '',
  spouseAddressLine2: '',
  citySpouse: '',
  stateSpouse: '',
  zipCodeSpouse: '',
  homePhoneSpouse: '',
  employerNameSpouse: '',
  employerPhoneSpouse: '',
  employerAddressSpouse: '',
  contractTypeSpouse: 'F',
  salaryTypeSpouse: 'G',
  startDateSpouse: '',
  lastPayDateSpouse: '',
  nextPayDateSpouse: '',

  addressEmploymentSpouse: '',
  cityEmploymentSpouse: '',
  stateEmploymentSpouse: '',
  zipCodeEmploymentSpouse: '',
  deductionSpouse: '',

  // children
  numberOfChildren: '',
  child1Age: '',
  child2Age: '',
  child3Age: '',
  child4Age: '',
  child5Age: '',
  child6Age: '',
  child7Age: '',
  child8Age: '',
  child9Age: '',
  child10Age: '',
  supportAmount: '',
  supportPersonName: '',
  supportCountry: '',
  supportType: '',
  supportMethod: '',

  grossMonthlySalaryDebtor: '',
  grossMonthlySalarySpouse: '',
  netMonthlySalaryDebtor: '',
  netMonthlySalarySpouse: '',

  primaryDebtorAddress: '',
  spouseAddress: ''
};
