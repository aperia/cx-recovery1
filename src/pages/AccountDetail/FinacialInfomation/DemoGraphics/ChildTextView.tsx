import { TruncateText } from 'app/_libraries/_dls/components';
import React from 'react';

interface ChildTextViewProps {
  label: string;
  textValue: string;
  className?: string;
}

const ChildTextView: React.FC<ChildTextViewProps> = ({
  label,
  textValue,
  className
}) => {
  return (
    <div className={className}>
      <span className="form-group-static__label">
        <TruncateText>{label}</TruncateText>
      </span>

      <div className={'form-group-static__text pre-line'}>{textValue}</div>
    </div>
  );
};

export default ChildTextView;
