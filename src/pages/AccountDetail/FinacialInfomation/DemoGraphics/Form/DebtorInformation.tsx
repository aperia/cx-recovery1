import React, { useCallback, useMemo } from 'react';
import { useFormikContext } from 'formik';

// components
import {
  DatePickerControl,
  DropdownControl,
  MaskedTextBoxControl,
  TextBoxControl
} from 'app/components';
import { GroupRadioControl } from 'app/components/FormControl/GroupRadio';
import FormikTextAreaControl from 'app/components/FormControl/FormikTextAreaControl';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useRefData from 'app/hooks/useRefData';

//constants & types
import {
  I18N_TEXT,
  MAX_LENGTH_10,
  MAX_LENGTH_13,
  MAX_LENGTH_140,
  MAX_LENGTH_25,
  MAX_LENGTH_35,
  MAX_LENGTH_40,
  NUMERIC_REPLACE_REGEX,
  PHONE_NUMBER_MASK_INPUT_REGEX,
  PLACE_HOLDER_PHONE_NUMBER
} from 'app/constants';
import { DEMOGRAPHICS_FORM } from '../constants';
import { getTimeBetween2Dates } from '../../helpers';
import { TODAY } from 'pages/AgencyManagement/components/SaveAgencyRequestModal/constants';

interface DebtorInformationProps {
  values: any;
}
const DebtorInformation: React.FC<DebtorInformationProps> = ({ values }) => {
  const { t } = useTranslation();
  const stateOptions = useRefData('state');
  const { handleChange } = useFormikContext();

  const renderSalaryField = useMemo(() => {
    if (values.salaryTypeDebtor === 'N') {
      return (
        <FormikNumericControl
          name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.netMonthlySalary.name}
          label={t(
            DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.netMonthlySalary.label
          )}
          className="col-6"
          maxLength={MAX_LENGTH_13}
          format="c2"
        />
      );
    }
    return (
      <FormikNumericControl
        name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.grossMonthlySalary.name}
        label={t(
          DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.grossMonthlySalary.label
        )}
        className="col-6"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />
    );
  }, [values.salaryTypeDebtor, t]);

  const getDaysWithLastPayDateAndToDay = useMemo(() => {
    return getTimeBetween2Dates({
      date1: values.lastPayDateDebtor,
      date2: TODAY
    });
  }, [values.lastPayDateDebtor]);

  const handleCheckboxChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      handleChange(event);
    },
    [handleChange]
  );

  return (
    <>
      <h5>{t(I18N_TEXT.DEBTOR_INFORMATION)}</h5>

      {/* Primary Debtor */}
      <h6 className="mt-16 color-grey">{t(I18N_TEXT.PRIMARY_DEBTOR)}</h6>
      <div className="row">
        <TextBoxControl
          name={
            DEMOGRAPHICS_FORM.DEBTOR_INFO.PRIMARY_DEBTOR.primaryDebtorName.name
          }
          label={
            DEMOGRAPHICS_FORM.DEBTOR_INFO.PRIMARY_DEBTOR.primaryDebtorName.label
          }
          className="col-lg-3 col-md-6 order-lg-1 order-md-1 mt-16"
          maxLength={MAX_LENGTH_40}
          readOnly
        />
        <div className="col-lg-9 order-lg-2 order-md-3">
          <div className="row">
            <TextBoxControl
              name={
                DEMOGRAPHICS_FORM.DEBTOR_INFO.PRIMARY_DEBTOR.addressLine1.name
              }
              label={
                DEMOGRAPHICS_FORM.DEBTOR_INFO.PRIMARY_DEBTOR.addressLine1.label
              }
              className="col-6  mt-16"
              maxLength={MAX_LENGTH_35}
              readOnly
            />

            <TextBoxControl
              name={
                DEMOGRAPHICS_FORM.DEBTOR_INFO.PRIMARY_DEBTOR.addressLine2.name
              }
              label={
                DEMOGRAPHICS_FORM.DEBTOR_INFO.PRIMARY_DEBTOR.addressLine2.label
              }
              className="col-6 mt-16"
              maxLength={MAX_LENGTH_35}
              readOnly
            />
          </div>
        </div>

        <TextBoxControl
          name={DEMOGRAPHICS_FORM.DEBTOR_INFO.PRIMARY_DEBTOR.city.name}
          label={DEMOGRAPHICS_FORM.DEBTOR_INFO.PRIMARY_DEBTOR.city.label}
          className="col-4 col-lg-3 order-lg-3 order-md-4 mt-16"
          maxLength={MAX_LENGTH_25}
          readOnly
        />

        <DropdownControl
          name={DEMOGRAPHICS_FORM.DEBTOR_INFO.PRIMARY_DEBTOR.state.name}
          label={DEMOGRAPHICS_FORM.DEBTOR_INFO.PRIMARY_DEBTOR.state.label}
          className="col-4 col-lg-3 order-lg-4 order-md-5 mt-16"
          options={stateOptions}
          isCombine
          readOnly
        />

        <TextBoxControl
          name={DEMOGRAPHICS_FORM.DEBTOR_INFO.PRIMARY_DEBTOR.zipCode.name}
          label={DEMOGRAPHICS_FORM.DEBTOR_INFO.PRIMARY_DEBTOR.zipCode.label}
          className="col-4 col-lg-3 order-lg-5 order-md-6 mt-16"
          maxLength={MAX_LENGTH_10}
          regex={NUMERIC_REPLACE_REGEX}
          readOnly
        />

        <MaskedTextBoxControl
          name={DEMOGRAPHICS_FORM.DEBTOR_INFO.PRIMARY_DEBTOR.homePhone.name}
          label={DEMOGRAPHICS_FORM.DEBTOR_INFO.PRIMARY_DEBTOR.homePhone.label}
          className="col-6 col-lg-3 order-lg-6 order-md-2 mt-16"
          mask={PHONE_NUMBER_MASK_INPUT_REGEX}
          placeholder={PLACE_HOLDER_PHONE_NUMBER}
          readOnly
        />
      </div>

      {/* Employment */}
      <h6 className="mt-24 color-grey">{t(I18N_TEXT.EMPLOYMENT)}</h6>
      <div className="row">
        <TextBoxControl
          name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.employerName.name}
          label={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.employerName.label}
          className="col-6 col-lg-3 mt-16 order-lg-1 order-md-1"
          maxLength={MAX_LENGTH_40}
        />

        <TextBoxControl
          name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.employerAddress.name}
          label={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.employerAddress.label}
          className="col-12 col-lg-6 mt-16 order-lg-2 order-md-3"
          maxLength={MAX_LENGTH_35}
        />
        <div className="col-lg-3 order-lg-3"></div>
        <TextBoxControl
          name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.city.name}
          label={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.city.label}
          className="col-4 col-lg-3 mt-16 order-lg-4 order-md-4"
          maxLength={MAX_LENGTH_25}
        />

        <DropdownControl
          name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.state.name}
          label={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.state.label}
          className="col-4 col-lg-3 mt-16 order-lg-5 order-md-5"
          options={stateOptions}
          isCombine
        />

        <TextBoxControl
          name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.zipCode.name}
          label={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.zipCode.label}
          className="col-4 col-lg-3 mt-16 order-lg-6 order-md-6"
          regex={NUMERIC_REPLACE_REGEX}
          maxLength={MAX_LENGTH_10}
        />
        <MaskedTextBoxControl
          name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.employerPhone.name}
          label={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.employerPhone.label}
          className="col-6 col-lg-3 mt-16 order-lg-7 order-md-2"
          mask={PHONE_NUMBER_MASK_INPUT_REGEX}
          placeholder={PLACE_HOLDER_PHONE_NUMBER}
        />
      </div>
      <div className="row">
        <div className="col-12 col-lg-6 mt-16 order-xl-1 order-md-1">
          <div className="d-flex">
            <p className="fw-500 color-grey mr-24">
              {t(I18N_TEXT.CONTRACT_TYPE)}:
            </p>
            <GroupRadioControl
              name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.contractType.name}
              direction="horizontal"
              options={
                DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.contractType.options[
                  'contractType'
                ]
              }
            />
          </div>
        </div>
        <div className="col-12 col-lg-6 mt-16 order-xl-2 order-md-3">
          <div className="d-flex">
            <p className="fw-500 color-grey mr-24">
              {t(I18N_TEXT.SALARY_TYPE)}:
            </p>
            <GroupRadioControl
              name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.salaryType.name}
              direction="horizontal"
              options={
                DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.salaryType.options[
                  'salaryType'
                ]
              }
              onValueChange={handleCheckboxChange}
            />
          </div>
        </div>

        <div className="col-xl-6 col-12 order-xl-3 order-md-2 mt-md-16">
          <div className="row">
            <DatePickerControl
              name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.startDate.name}
              label={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.startDate.label}
              className="col-4"
            />
            <DatePickerControl
              name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.lastPayDate.name}
              label={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.lastPayDate.label}
              className="col-4"
              futureDateDisabledText={t(
                I18N_TEXT.LAST_PAY_DATE_CANNOT_BE_FUTURE_DATE
              )}
              maxDaysFromToday={1}
            />
            <DatePickerControl
              name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.nextPayDate.name}
              label={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.nextPayDate.label}
              className="col-4"
              sameOrBeforeDisabledText={t(
                I18N_TEXT.NEXT_PAY_DATE_CANNOT_BE_BEFORE_OR_THE_SAME
              )}
              minDaysFromToday={-(getDaysWithLastPayDateAndToDay - 1)}
            />
          </div>
        </div>
        <div className="col-xl-6 col-12 order-xl-4 order-md-4 mt-md-16">
          <div className="row">
            {renderSalaryField}
            <FormikNumericControl
              name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.deduction.name}
              label={t(
                DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.deduction.label
              )}
              className="col-6"
              maxLength={MAX_LENGTH_13}
              format="c2"
            />
          </div>
        </div>
      </div>
      {/* Commemt */}
      <h6 className="mt-24 color-grey">{t(I18N_TEXT.COMMENT)}</h6>
      <div className="row mt-16">
        <div className="col-md-12 col-lg-6">
          <FormikTextAreaControl
            className="h-lg-140px"
            name={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.comment.name}
            label={DEMOGRAPHICS_FORM.DEBTOR_INFO.EMPLOYMENT.comment.label}
            maxLength={MAX_LENGTH_140}
            // TODO: regex={ALPHA_NUMERIC_REPLACE_REGEX}
            disabledEnter
          />
        </div>
      </div>
    </>
  );
};

export default React.memo(DebtorInformation);
