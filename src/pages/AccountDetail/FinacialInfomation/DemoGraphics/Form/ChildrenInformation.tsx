import React, { useCallback, useMemo, useRef } from 'react';
import { useFormikContext } from 'formik';

// components
import {
  ComboBoxControl,
  DropdownControl,
  TextBoxControl
} from 'app/components';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';

// hooks
import useRefData from 'app/hooks/useRefData';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_TEXT, MAX_LENGTH_13, MAX_LENGTH_40 } from 'app/constants';
import { DEMOGRAPHICS_FORM } from '../constants';
import { GroupRadioControl } from 'app/components/FormControl/GroupRadio';

import { isEmpty } from 'app/helpers';

interface ChildrenInformationProps {
  values: any;
}
const ChildrenInformation: React.FC<ChildrenInformationProps> = ({
  values
}) => {
  const { t } = useTranslation();
  const numberOfChildrenOptions = useRefData('numberOfChildren');
  const formChildrenRef = useRef<HTMLDivElement | null>(null);

  const childAgeOptions = useRefData('childAge');
  const { handleChange, setValues } = useFormikContext();

  const renderChildAgeField = useMemo(() => {
    if (!isEmpty(values.numberOfChildren) && values.numberOfChildren !== '0') {
      return (
        <>
          {Array.apply(0, Array(Number(values.numberOfChildren))).map(
            (_, index) => (
              <DropdownControl
                key={index}
                name={`child${index + 1}Age`}
                label={t(`txt_child_age`, { numberOfChildren: index + 1 })}
                className="col-4 col-lg-3 mt-16"
                options={childAgeOptions}
                required
              />
            )
          )}
        </>
      );
    }
    return null;
  }, [t, values.numberOfChildren, childAgeOptions]);

  const handleNumberOfChildrenChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      // trigger change formik's value
      const item = e.value;
      const event = {
        ...e,
        target: {
          ...e.target,
          value: item.value
        }
      };

      const currentValue = Number(item.value);

      const oldValue = Number(values.numberOfChildren);

      setTimeout(() => {
        formChildrenRef.current &&
          formChildrenRef.current.scrollIntoView({
            behavior: 'smooth',
            block: 'end'
          });
      }, 300);

      if (currentValue > oldValue) return;
      const obj: any = {};
      const startIndex = currentValue === 0 ? 0 : currentValue;

      for (let i = startIndex; i < oldValue; i++) {
        if (i + 1 > currentValue) {
          obj[`child${i + 1}Age`] = '';
        }
      }
      setValues({ ...values, ...obj });

      handleChange(event);
    },
    [handleChange, setValues, values]
  );

  return (
    <div ref={formChildrenRef}>
      <h5>{t(I18N_TEXT.CHILDREN_INFORMATION)}</h5>
      <ComboBoxControl
        name={DEMOGRAPHICS_FORM.CHILDREN_INFO.numberOfChildren.name}
        label={DEMOGRAPHICS_FORM.CHILDREN_INFO.numberOfChildren.label}
        className="col-6 col-lg-3 mt-16 px-0"
        options={numberOfChildrenOptions}
        onValueChange={handleNumberOfChildrenChange}
      />
      {!isEmpty(values.numberOfChildren) && values.numberOfChildren !== '0' ? (
        <div className="bg-light-l20 border br-light-l04 br-radius-4 p-16 mt-16 mx-auto">
          <p>{t(I18N_TEXT.CHILDREN_INFO_MSG_LABEL)}</p>
          <div className="row">{renderChildAgeField}</div>

          <div className="divider-dashed mt-16"></div>
          <div className="row">
            <FormikNumericControl
              name={DEMOGRAPHICS_FORM.CHILDREN_INFO.supportAmount.name}
              label={t(DEMOGRAPHICS_FORM.CHILDREN_INFO.supportAmount.label)}
              className="col-4 col-lg-3 mt-16 order-lg-1 order-md-1"
              maxLength={MAX_LENGTH_13}
              format="c2"
            />
            <TextBoxControl
              name={DEMOGRAPHICS_FORM.CHILDREN_INFO.supportPersonName.name}
              label={DEMOGRAPHICS_FORM.CHILDREN_INFO.supportPersonName.label}
              className="col-12 col-lg-5 mt-16 order-lg-2 order-md-3"
              maxLength={MAX_LENGTH_40}
            />
            <TextBoxControl
              name={DEMOGRAPHICS_FORM.CHILDREN_INFO.supportCountry.name}
              label={DEMOGRAPHICS_FORM.CHILDREN_INFO.supportCountry.label}
              className="col-8 col-lg-4 mt-16 order-lg-3 order-md-2"
              maxLength={MAX_LENGTH_40}
            />
          </div>
          <div className="row mt-16">
            <div className="col-12 col-lg-6">
              <div className="d-flex">
                <p className="fw-500 color-grey mr-24">
                  {t(I18N_TEXT.SUPPORT_TYPE)}:
                </p>
                <GroupRadioControl
                  name={DEMOGRAPHICS_FORM.CHILDREN_INFO.supportType.name}
                  direction="vertical"
                  options={
                    DEMOGRAPHICS_FORM.CHILDREN_INFO.supportType.options[
                      'supportType'
                    ]
                  }
                />
              </div>
            </div>
            <div className="col-12 col-lg-6">
              <div className="d-flex">
                <p className="fw-500 color-grey mr-24">
                  {t(I18N_TEXT.SUPPORT_METHOD)}:
                </p>
                <GroupRadioControl
                  name={DEMOGRAPHICS_FORM.CHILDREN_INFO.supportMethod.name}
                  direction="vertical"
                  options={
                    DEMOGRAPHICS_FORM.CHILDREN_INFO.supportMethod.options[
                      'supportMethod'
                    ]
                  }
                />
              </div>
            </div>
          </div>
        </div>
      ) : null}
    </div>
  );
};

export default React.memo(ChildrenInformation);
