import React, { useCallback, useMemo } from 'react';
import { useFormikContext } from 'formik';

// components
import {
  DatePickerControl,
  DropdownControl,
  MaskedTextBoxControl,
  TextBoxControl
} from 'app/components';
import { GroupRadioControl } from 'app/components/FormControl/GroupRadio';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';

// hooks
import useRefData from 'app/hooks/useRefData';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constant
import {
  I18N_TEXT,
  MAX_LENGTH_10,
  MAX_LENGTH_13,
  MAX_LENGTH_25,
  MAX_LENGTH_35,
  MAX_LENGTH_40,
  NUMERIC_REPLACE_REGEX,
  PHONE_NUMBER_MASK_INPUT_REGEX,
  PLACE_HOLDER_PHONE_NUMBER
} from 'app/constants';
import { DEMOGRAPHICS_FORM } from '../constants';
import { TODAY } from 'pages/AgencyManagement/components/SaveAgencyRequestModal/constants';

import { getTimeBetween2Dates } from '../../helpers';

interface SpouseInformationProps {
  values: any;
}
const SpouseInformation: React.FC<SpouseInformationProps> = ({ values }) => {
  const { t } = useTranslation();
  const stateOptions = useRefData('state');
  const { handleChange } = useFormikContext();

  const renderSalaryField = useMemo(() => {
    if (values.salaryTypeSpouse === 'N') {
      return (
        <FormikNumericControl
          name={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.netMonthlySalary.name}
          label={t(
            DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.netMonthlySalary.label
          )}
          className="col-4 col-lg-6"
          maxLength={MAX_LENGTH_13}
          format="c2"
        />
      );
    }
    return (
      <FormikNumericControl
        name={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.grossMonthlySalary.name}
        label={t(
          DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.grossMonthlySalary.label
        )}
        className="col-4 col-lg-6"
        maxLength={MAX_LENGTH_13}
        format="c2"
      />
    );
  }, [values.salaryTypeSpouse, t]);

  const getDaysWithLastPayDateAndToDay = useMemo(() => {
    return getTimeBetween2Dates({
      date1: values.lastPayDateSpouse,
      date2: TODAY
    });
  }, [values.lastPayDateSpouse]);

  const handleCheckboxChange = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      handleChange(event);
    },
    [handleChange]
  );

  return (
    <>
      <h5>{t(I18N_TEXT.SPOUSE_INFORMATION)}</h5>

      <h6 className="mt-16 color-grey">{t(I18N_TEXT.SPOUSE)}</h6>

      <div className="row">
        <TextBoxControl
          name={DEMOGRAPHICS_FORM.SPOUSE_INFO.SPOUSE.spouseName.name}
          label={DEMOGRAPHICS_FORM.SPOUSE_INFO.SPOUSE.spouseName.label}
          className="col-lg-3 col-md-6 order-lg-1 order-md-1 mt-16"
          maxLength={MAX_LENGTH_40}
        />
        <div className="col-lg-9 order-lg-2 order-md-3">
          <div className="row">
            <TextBoxControl
              name={DEMOGRAPHICS_FORM.SPOUSE_INFO.SPOUSE.addressLine1.name}
              label={DEMOGRAPHICS_FORM.SPOUSE_INFO.SPOUSE.addressLine1.label}
              className="col-6 mt-16"
              maxLength={MAX_LENGTH_35}
            />

            <TextBoxControl
              name={DEMOGRAPHICS_FORM.SPOUSE_INFO.SPOUSE.addressLine2.name}
              label={DEMOGRAPHICS_FORM.SPOUSE_INFO.SPOUSE.addressLine2.label}
              className="col-6 mt-16"
              maxLength={MAX_LENGTH_35}
            />
          </div>
        </div>

        <TextBoxControl
          name={DEMOGRAPHICS_FORM.SPOUSE_INFO.SPOUSE.city.name}
          label={DEMOGRAPHICS_FORM.SPOUSE_INFO.SPOUSE.city.label}
          className="col-4 col-lg-3 order-lg-3 order-md-4 mt-16"
          maxLength={MAX_LENGTH_25}
        />

        <DropdownControl
          name={DEMOGRAPHICS_FORM.SPOUSE_INFO.SPOUSE.state.name}
          label={DEMOGRAPHICS_FORM.SPOUSE_INFO.SPOUSE.state.label}
          className="col-4 col-lg-3 order-lg-4 order-md-5 mt-16"
          options={stateOptions}
          isCombine
        />

        <TextBoxControl
          name={DEMOGRAPHICS_FORM.SPOUSE_INFO.SPOUSE.zipCode.name}
          label={DEMOGRAPHICS_FORM.SPOUSE_INFO.SPOUSE.zipCode.label}
          className="col-4 col-lg-3 order-lg-5 order-md-6 mt-16"
          maxLength={MAX_LENGTH_10}
          regex={NUMERIC_REPLACE_REGEX}
        />
        <MaskedTextBoxControl
          name={DEMOGRAPHICS_FORM.SPOUSE_INFO.SPOUSE.homePhone.name}
          label={DEMOGRAPHICS_FORM.SPOUSE_INFO.SPOUSE.homePhone.label}
          className="col-6 col-lg-3 order-lg-6 order-md-2 mt-16"
          mask={PHONE_NUMBER_MASK_INPUT_REGEX}
          placeholder={PLACE_HOLDER_PHONE_NUMBER}
        />
      </div>
      {/* Employment */}
      <h6 className="mt-16 color-grey">{t(I18N_TEXT.EMPLOYMENT)}</h6>
      <div className="row">
        <TextBoxControl
          name={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.employerName.name}
          label={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.employerName.label}
          className="col-6 col-lg-3 mt-16 order-lg-1 order-md-1"
          maxLength={MAX_LENGTH_40}
        />

        <TextBoxControl
          name={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.employerAddress.name}
          label={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.employerAddress.label}
          className="col-12 col-lg-6 mt-16 order-lg-2 order-md-3"
          maxLength={MAX_LENGTH_35}
        />
        <div className="col-lg-3 order-lg-3"></div>
        <TextBoxControl
          name={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.city.name}
          label={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.city.label}
          className="col-4 col-lg-3 mt-16 order-lg-4 order-md-4"
          maxLength={MAX_LENGTH_25}
        />

        <DropdownControl
          name={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.state.name}
          label={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.state.label}
          className="col-4 col-lg-3 mt-16 order-lg-5 order-md-5"
          options={stateOptions}
          isCombine
        />

        <TextBoxControl
          name={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.zipCode.name}
          label={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.zipCode.label}
          className="col-4 col-lg-3 mt-16 order-lg-6 order-md-6"
          maxLength={MAX_LENGTH_10}
          regex={NUMERIC_REPLACE_REGEX}
        />
        <MaskedTextBoxControl
          name={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.employerPhone.name}
          label={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.employerPhone.label}
          className="col-6 col-lg-3 mt-16 order-lg-7 order-md-2"
          mask={PHONE_NUMBER_MASK_INPUT_REGEX}
          placeholder={PLACE_HOLDER_PHONE_NUMBER}
        />
      </div>
      <div className="row">
        <div className="col-12 col-lg-6 mt-16 order-xl-1 order-md-1">
          <div className="d-flex">
            <p className="fw-500 color-grey mr-24">
              {t(I18N_TEXT.CONTRACT_TYPE)}:
            </p>
            <GroupRadioControl
              name={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.contractType.name}
              direction="horizontal"
              options={
                DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.contractType.options[
                  'contractType'
                ]
              }
            />
          </div>
        </div>
        <div className="col-12 col-lg-6 mt-16 order-xl-2 order-md-3">
          <div className="d-flex">
            <p className="fw-500 color-grey mr-24">
              {t(I18N_TEXT.SALARY_TYPE)}:
            </p>
            <GroupRadioControl
              name={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.salaryType.name}
              direction="horizontal"
              options={
                DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.salaryType.options[
                  'salaryType'
                ]
              }
              onValueChange={handleCheckboxChange}
            />
          </div>
        </div>

        <div className="col-xl-6 col-12 order-xl-3 order-md-2 mt-md-16">
          <div className="row">
            <DatePickerControl
              name={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.startDate.name}
              label={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.startDate.label}
              className="col-4"
            />
            <DatePickerControl
              name={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.lastPayDate.name}
              label={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.lastPayDate.label}
              className="col-4"
              futureDateDisabledText={t(
                I18N_TEXT.LAST_PAY_DATE_CANNOT_BE_FUTURE_DATE
              )}
              maxDaysFromToday={1}
            />
            <DatePickerControl
              name={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.nextPayDate.name}
              label={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.nextPayDate.label}
              className="col-4"
              sameOrBeforeDisabledText={t(
                I18N_TEXT.NEXT_PAY_DATE_CANNOT_BE_BEFORE_OR_THE_SAME
              )}
              minDaysFromToday={-(getDaysWithLastPayDateAndToDay - 1)}
            />
          </div>
        </div>
        <div className="col-xl-6 col-12 order-xl-4 order-md-4 mt-md-16">
          <div className="row">
            {renderSalaryField}
            <FormikNumericControl
              name={DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.deduction.name}
              label={t(
                DEMOGRAPHICS_FORM.SPOUSE_INFO.EMPLOYMENT.deduction.label
              )}
              className="col-4 col-lg-6"
              maxLength={MAX_LENGTH_13}
              format="c2"
            />
          </div>
        </div>
      </div>
    </>
  );
};

export default React.memo(SpouseInformation);
