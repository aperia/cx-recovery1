import React, { useCallback } from 'react';
import { FormikProvider, useFormik } from 'formik';
import { useDispatch } from 'react-redux';

// components
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import ChildrenInformation from './ChildrenInformation';
import DebtorInformation from './DebtorInformation';
import SpouseInformation from './SpouseInformation';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelectorId } from 'app/hooks';
import { useDemoForm } from '../hooks/useDemoForm';

// redux
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { financialInformationActions } from '../../_redux';
import { selectorDemographicsData } from '../../_redux/demographics/selectors';

// constants & types
import { I18N_TEXT } from 'app/constants';
import { IDemoGraphics } from '../../types';

import { genInitValues } from '../../helpers';
import { isTouchedForm, isFunction, isEqual, formatTime } from 'app/helpers';

interface FinancialModalProps {
  open: boolean;
  storeId: string;
  isAdd: boolean;
}
const FinancialModal: React.FC<FinancialModalProps> = ({
  open,
  storeId,
  isAdd
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const data: IDemoGraphics = useSelectorId(selectorDemographicsData);

  const { initialValues, validationSchema, onSubmit } = useDemoForm();

  const formik = useFormik<any>({
    enableReinitialize: true,
    initialValues: initialValues,
    validationSchema,
    validateOnBlur: true,
    validateOnMount: false,
    onSubmit: values => {
      isFunction(onSubmit) && onSubmit(values);
      isAdd && resetForm();
    }
  });

  const { isValid, dirty, values, touched, submitForm, resetForm } = formik;

  const handleCancelForm = useCallback(() => {
    resetForm();
    dispatch(
      financialInformationActions.onSetModalFinancial({
        storeId,
        isOpen: false
      })
    );
  }, [dispatch, resetForm, storeId]);

  const handleCancel = useCallback(() => {
    const isDataChange = !isEqual(
      {
        ...genInitValues(data)
      },
      {
        ...values,
        startDateDebtor: formatTime(values.startDateDebtor)?.date || '',
        lastPayDateDebtor: formatTime(values.lastPayDateDebtor)?.date || '',
        nextPayDateDebtor: formatTime(values.nextPayDateDebtor)?.date || '',
        startDateSpouse: formatTime(values.startDateSpouse)?.date || '',
        lastPayDateSpouse: formatTime(values.lastPayDateSpouse)?.date || '',
        nextPayDateSpouse: formatTime(values.nextPayDateSpouse)?.date || ''
      }
    );

    if (isDataChange && dirty && isTouchedForm(touched)) {
      dispatch(
        rootModalActions.open({
          title: I18N_TEXT.UNSAVED_CHANGE,
          body: I18N_TEXT.DISCARD_CHANGE_BODY,
          btnCancelText: I18N_TEXT.CONTINUE_EDITING,
          btnConfirmText: I18N_TEXT.DISCARD_CHANGE,
          onConfirm: handleCancelForm
        })
      );
      return;
    }

    handleCancelForm();
  }, [dirty, touched, handleCancelForm, dispatch, values, data]);

  return (
    <FormikProvider value={formik}>
      <Modal full show={open} loading={false} enforceFocus={false}>
        <ModalHeader
          border
          onHide={handleCancel}
          closeButton
          className="border-bottom"
        >
          <ModalTitle className="d-flex align-items-center">
            <h4>
              {t(
                isAdd
                  ? I18N_TEXT.ADD_FINANCIAL_DEMOGRAPHICS
                  : I18N_TEXT.EDIT_FINANCIAL_DEMOGRAPHICS
              )}
            </h4>
          </ModalTitle>
        </ModalHeader>
        <ModalBody noPadding>
          <div className="p-24 max-width-lg mx-auto">
            <DebtorInformation values={values} />
            <div className="mt-24">
              <SpouseInformation values={values} />
            </div>
            <div className="mt-24">
              <ChildrenInformation values={values} />
            </div>
          </div>
        </ModalBody>
        <ModalFooter
          border
          cancelButtonText={t(I18N_TEXT.CANCEL)}
          okButtonText={t(isAdd ? I18N_TEXT.SUBMIT : I18N_TEXT.SAVE)}
          onCancel={handleCancel}
          disabledOk={!isValid}
          onOk={submitForm}
        />
      </Modal>
    </FormikProvider>
  );
};

export default FinancialModal;
