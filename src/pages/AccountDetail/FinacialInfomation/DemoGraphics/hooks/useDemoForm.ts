import * as Yup from 'yup';
import { useDispatch } from 'react-redux';

import { useAccountDetail, useSelectorId } from 'app/hooks';
import { selectorDemographicsData } from '../../_redux/demographics/selectors';
import { financialInformationActions } from '../../_redux';

import { I18N_TEXT } from 'app/constants';
import {
  DEFAULT_VALUES_DEMOGRAPHICS_FORM,
  DEMOGRAPHICS_FORM
} from '../constants';

import { IDemoGraphics } from '../../types';
import { formatDataSubmitting, genInitValues } from '../../helpers';
import { isEmpty } from 'app/helpers';

import { useTranslation } from 'app/_libraries/_dls/hooks';

export const useDemoForm = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();

  const data: IDemoGraphics = useSelectorId(selectorDemographicsData);

  const validationSchema = Yup.object().shape({
    [DEMOGRAPHICS_FORM.CHILDREN_INFO.child1Age.name]: Yup.string().when(
      [DEMOGRAPHICS_FORM.CHILDREN_INFO.numberOfChildren.name],
      (value: string, schema: Yup.AnySchema) =>
        Number(value) > 0
          ? schema.required(
              t(I18N_TEXT.CHILD_AGE_REQUIRED, { numberOfChildren: 1 })
            )
          : schema
    ),
    [DEMOGRAPHICS_FORM.CHILDREN_INFO.child2Age.name]: Yup.string().when(
      [DEMOGRAPHICS_FORM.CHILDREN_INFO.numberOfChildren.name],
      (value: string, schema: Yup.AnySchema) =>
        Number(value) > 1
          ? schema.required(
              t(I18N_TEXT.CHILD_AGE_REQUIRED, { numberOfChildren: 2 })
            )
          : schema
    ),
    [DEMOGRAPHICS_FORM.CHILDREN_INFO.child3Age.name]: Yup.string().when(
      [DEMOGRAPHICS_FORM.CHILDREN_INFO.numberOfChildren.name],
      (value: string, schema: Yup.AnySchema) =>
        Number(value) > 2
          ? schema.required(
              t(I18N_TEXT.CHILD_AGE_REQUIRED, { numberOfChildren: 3 })
            )
          : schema
    ),
    [DEMOGRAPHICS_FORM.CHILDREN_INFO.child4Age.name]: Yup.string().when(
      [DEMOGRAPHICS_FORM.CHILDREN_INFO.numberOfChildren.name],
      (value: string, schema: Yup.AnySchema) =>
        Number(value) > 3
          ? schema.required(
              t(I18N_TEXT.CHILD_AGE_REQUIRED, { numberOfChildren: 4 })
            )
          : schema
    ),
    [DEMOGRAPHICS_FORM.CHILDREN_INFO.child5Age.name]: Yup.string().when(
      [DEMOGRAPHICS_FORM.CHILDREN_INFO.numberOfChildren.name],
      (value: string, schema: Yup.AnySchema) =>
        Number(value) > 4
          ? schema.required(
              t(I18N_TEXT.CHILD_AGE_REQUIRED, { numberOfChildren: 5 })
            )
          : schema
    ),
    [DEMOGRAPHICS_FORM.CHILDREN_INFO.child6Age.name]: Yup.string().when(
      [DEMOGRAPHICS_FORM.CHILDREN_INFO.numberOfChildren.name],
      (value: string, schema: Yup.AnySchema) =>
        Number(value) > 5
          ? schema.required(
              t(I18N_TEXT.CHILD_AGE_REQUIRED, { numberOfChildren: 6 })
            )
          : schema
    ),
    [DEMOGRAPHICS_FORM.CHILDREN_INFO.child7Age.name]: Yup.string().when(
      [DEMOGRAPHICS_FORM.CHILDREN_INFO.numberOfChildren.name],
      (value: string, schema: Yup.AnySchema) =>
        Number(value) > 6
          ? schema.required(
              t(I18N_TEXT.CHILD_AGE_REQUIRED, { numberOfChildren: 7 })
            )
          : schema
    ),
    [DEMOGRAPHICS_FORM.CHILDREN_INFO.child8Age.name]: Yup.string().when(
      [DEMOGRAPHICS_FORM.CHILDREN_INFO.numberOfChildren.name],
      (value: string, schema: Yup.AnySchema) =>
        Number(value) > 7
          ? schema.required(
              t(I18N_TEXT.CHILD_AGE_REQUIRED, { numberOfChildren: 8 })
            )
          : schema
    ),
    [DEMOGRAPHICS_FORM.CHILDREN_INFO.child9Age.name]: Yup.string().when(
      [DEMOGRAPHICS_FORM.CHILDREN_INFO.numberOfChildren.name],
      (value: string, schema: Yup.AnySchema) =>
        Number(value) > 8
          ? schema.required(
              t(I18N_TEXT.CHILD_AGE_REQUIRED, { numberOfChildren: 9 })
            )
          : schema
    ),
    [DEMOGRAPHICS_FORM.CHILDREN_INFO.child10Age.name]: Yup.string().when(
      [DEMOGRAPHICS_FORM.CHILDREN_INFO.numberOfChildren.name],
      (value: string, schema: Yup.AnySchema) =>
        Number(value) > 9
          ? schema.required(
              t(I18N_TEXT.CHILD_AGE_REQUIRED, { numberOfChildren: 10 })
            )
          : schema
    )
  });

  const onSubmit = (values: IDemoGraphics) => {
    const formatedData = formatDataSubmitting(values);
    if (isEmpty(data)) {
      // ADD
      dispatch(
        financialInformationActions.addDemographics({
          storeId,
          newData: formatedData as IDemoGraphics
        })
      );
    } else {
      // EDIT ...
      dispatch(
        financialInformationActions.editDemographics({
          storeId,
          changeData: {...values, ...formatedData} as IDemoGraphics
        })
      );
    }
  };

  return {
    initialValues: isEmpty(data)
      ? { ...DEFAULT_VALUES_DEMOGRAPHICS_FORM }
      : genInitValues(data),
    onSubmit,
    validationSchema
  };
};
