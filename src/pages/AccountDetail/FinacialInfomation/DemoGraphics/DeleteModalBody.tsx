import { I18N_TEXT } from 'app/constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import React from 'react';

const DeleteDemographicsModal: React.FC = () => {
  const { t } = useTranslation();
  return (
    <>
      <p className="mt-4">{t(I18N_TEXT.DELETE_MESSAGE_DEMOGRAPHICS_MODAL)}</p>
    </>
  );
};

export default DeleteDemographicsModal;
