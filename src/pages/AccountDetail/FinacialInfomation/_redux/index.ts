import { createSlice } from '@reduxjs/toolkit';
import {
  onSetModalFinancial,
  onSetExpandViewAll
} from './demographics/actions';
import {
  getDemographics,
  getDemographicsBuilder
} from './demographics/getDemographics';
import {
  deleteDemographics,
  deleteDemographicsBuilder
} from './demographics/deleteDemographics';
import {
  addDemographics,
  addDemographicsBuilder
} from './demographics/addDemographics';
import {
  editDemographics,
  editDemographicsBuilder
} from './demographics/editDemographics';

// types
import { InitialStateFinancialInformation } from '../types';

// statement actions
import { onToggleStatementFormModal } from './statement/actions';
import { saveStatement, saveStatementBuilder } from './statement/saveStatement';
import { getStatement, getStatementBuilder } from './statement/getStatement';
import {
  deleteStatement,
  deleteStatementBuilder
} from './statement/deleteStatement';

// actions creditor
import { getCreditorBuilder, getCreditor } from './creditor/getCreditor';
import {
  handleOpenDeleteModal,
  handleOpenFormModal,
  setDataForm,
  cleanDataForm
} from './creditor/actions';
import {
  deleteCreditor,
  deleteCreditorBuilder
} from './creditor/deleteCreditor';

import {
  addEditCreditor,
  addEditCreditorBuilder
} from './creditor/addEditForm';

const initialFinancialInformationState: InitialStateFinancialInformation = {};

const { actions, reducer } = createSlice({
  name: 'financialInformation',
  initialState: initialFinancialInformationState,
  reducers: {
    // demographics
    onSetModalFinancial,
    onSetExpandViewAll,
    // creditor
    handleOpenDeleteModal,

    // statement
    onToggleStatementFormModal,
    handleOpenFormModal,
    setDataForm,
    cleanDataForm
  },
  extraReducers: builder => {
    //demographics
    getDemographicsBuilder(builder);
    deleteDemographicsBuilder(builder);

    addDemographicsBuilder(builder);
    editDemographicsBuilder(builder);
    // creditor
    getCreditorBuilder(builder);
    saveStatementBuilder(builder);
    deleteCreditorBuilder(builder);
    addEditCreditorBuilder(builder);
    getStatementBuilder(builder);
    deleteStatementBuilder(builder);
  }
});

const combineActions = {
  ...actions,

  // demographics
  getDemographics,
  deleteDemographics,

  // statement
  saveStatement,
  getStatement,
  deleteStatement,
  addDemographics,
  editDemographics,

  // creditor
  addEditCreditor,
  getCreditor,
  deleteCreditor,
  
};

export { combineActions as financialInformationActions, reducer };
