import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { ICreditor, InitialStateFinancialInformation } from '../../types';
import financialInformationService from '../../financialInfoService';
import { isEmpty, set } from 'lodash';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { financialInformationActions } from '..';
import { I18N_TEXT } from 'app/constants';

export const addEditCreditor = createAsyncThunk<
  unknown,
  { storeId: string; data: ICreditor },
  ThunkAPIConfig
>('addEditCreditor', async (args, { getState, dispatch, rejectWithValue }) => {
  const { storeId, data } = args;
  const { financialInformation } = getState();
  const {
    ADD_CREDITOR_FAILURE,
    UPDATE_CREDITOR_SUCCESS,
    ADD_CREDITOR_SUCCESS,
    UPDATE_CREDITOR_FAILURE
  } = I18N_TEXT;
  try {
    const message = !isEmpty(financialInformation[storeId]?.creditor?.dataForm)
      ? UPDATE_CREDITOR_SUCCESS
      : ADD_CREDITOR_SUCCESS;
    await financialInformationService.addEditCreditor(data);
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'success',
        message
      })
    );
    dispatch(financialInformationActions.getCreditor({ storeId }));
  } catch (error) {
    const message = !isEmpty(financialInformation[storeId]?.creditor?.dataForm)
      ? UPDATE_CREDITOR_FAILURE
      : ADD_CREDITOR_FAILURE;
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message
      })
    );
    return rejectWithValue(error);
  }
});

export const addEditCreditorBuilder = (
  builder: ActionReducerMapBuilder<InitialStateFinancialInformation>
) => {
  const { pending, fulfilled, rejected } = addEditCreditor;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.creditor.isLoadingModal`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.creditor.isLoadingModal`, false);
      set(draftState, `${storeId}.creditor.isOpenForm`, false);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.creditor.isLoadingModal`, true);
    });
};
