import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { InitialStateFinancialInformation } from '../../types';
import financialInformationService from '../../financialInfoService';
import { set } from 'lodash';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { financialInformationActions } from '..';
import { I18N_TEXT } from 'app/constants';

export const deleteCreditor = createAsyncThunk<
  unknown,
  { storeId: string },
  ThunkAPIConfig
>('deleteCreditor', async (args, { dispatch, rejectWithValue }) => {
  try {
    const { storeId } = args;
    await financialInformationService.deleteCreditor();
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'success',
        message: I18N_TEXT.DELETE_CREDITOR_SUCCESS
      })
    );
    dispatch(financialInformationActions.getCreditor({ storeId }));
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.DELETE_CREDITOR_FAILURE
      })
    );
    return rejectWithValue(error);
  }
});

export const deleteCreditorBuilder = (
  builder: ActionReducerMapBuilder<InitialStateFinancialInformation>
) => {
  const { pending, fulfilled, rejected } = deleteCreditor;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.creditor.isLoadingModal`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.creditor.isLoadingModal`, false);
      set(draftState, `${storeId}.creditor.isOpenDeleteModal`, false);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.creditor.isLoadingModal`, true);
    });
};
