import { PayloadAction } from '@reduxjs/toolkit';
import { isEmpty, set } from 'lodash';
import { InitialStateFinancialInformation, ICreditor } from '../../types';

export const handleOpenDeleteModal = (
  draftState: InitialStateFinancialInformation,
  action: PayloadAction<{ storeId: string }>
) => {
  const { storeId } = action.payload;
  const data = !draftState[storeId]?.creditor?.isOpenDeleteModal;
  set(draftState, `${storeId}.creditor.isOpenDeleteModal`, data);
};

export const handleOpenFormModal = (
  draftState: InitialStateFinancialInformation,
  action: PayloadAction<{ storeId: string }>
) => {
  const { storeId } = action.payload;
  const data = !draftState[storeId]?.creditor?.isOpenForm;
  set(draftState, `${storeId}.creditor.isOpenForm`, data);
};

export const setDataForm = (
  draftState: InitialStateFinancialInformation,
  action: PayloadAction<{ storeId: string; data: ICreditor }>
) => {
  const { storeId, data } = action.payload;
  set(draftState, `${storeId}.creditor.dataForm`, data);
};

export const cleanDataForm = (
  draftState: InitialStateFinancialInformation,
  action: PayloadAction<{ storeId: string }>
) => {
  const { storeId } = action.payload;
  if (!isEmpty(draftState[storeId]?.creditor?.dataForm)) {
    const data = {} as ICreditor;
    set(draftState, `${storeId}.creditor.dataForm`, data);
  }
};
