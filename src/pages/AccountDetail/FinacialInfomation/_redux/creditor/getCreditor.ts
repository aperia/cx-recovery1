import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { ICreditor, InitialStateFinancialInformation } from '../../types';
import financialInformationService from '../../financialInfoService';
import { set } from 'lodash';

export const getCreditor = createAsyncThunk<
  { data: ICreditor },
  { storeId: string },
  ThunkAPIConfig
>('getCreditor', async (_, { rejectWithValue }) => {
  try {
    const { data } = await financialInformationService.getCreditor();

    return { data: data.data };
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getCreditorBuilder = (
  builder: ActionReducerMapBuilder<InitialStateFinancialInformation>
) => {
  const { pending, fulfilled, rejected } = getCreditor;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.creditor.isLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      set(draftState, `${storeId}.creditor.isLoading`, false);
      set(draftState, `${storeId}.creditor.data`, data);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.creditor.isLoading`, true);
    });
};
