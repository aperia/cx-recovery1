import { createSelector } from '@reduxjs/toolkit';
import { isEmpty } from 'app/helpers';
import { ICreditor } from '../../types';

export const getCreditor: OutputSelectorFunction<ICreditor> = (
  states,
  storeId
) => {
  return states.financialInformation[storeId]?.creditor?.data;
};

export const getIsLoading: OutputSelectorFunction<boolean> = (
  states,
  storeId
) => {
  return states.financialInformation[storeId]?.creditor?.isLoading;
};

export const selectIsDisplayNoData = createSelector(
  getCreditor,
  getIsLoading,
  (data: ICreditor, isLoading: boolean) => isEmpty(data) && !isLoading
);

export const selectDataForm = createSelector(
  [
    (states: RootState, storeId: string) =>
      states.financialInformation[storeId]?.creditor?.dataForm
  ],
  (dataForm: ICreditor) => dataForm
);

export const selectCreditor = createSelector(
  [
    (states: RootState, storeId: string): ICreditor =>
      states.financialInformation[storeId]?.creditor?.data
  ],
  (data: ICreditor) => data
);

export const selectorIsOpenDeleteModal = createSelector(
  [
    (states: RootState, storeId: string): boolean =>
      states.financialInformation[storeId]?.creditor?.isOpenDeleteModal
  ],
  (data: boolean) => data
);

export const selectorIsOpenFormModal = createSelector(
  [
    (states: RootState, storeId: string) =>
      states.financialInformation[storeId]?.creditor?.isOpenForm
  ],
  (data: boolean) => data
);

export const selectorIsLoadingDeleteModal = createSelector(
  [
    (states: RootState, storeId: string) =>
      states.financialInformation[storeId]?.creditor?.isLoading
  ],
  (data: boolean) => data
);

export const selectorIsLoadingCreditor = createSelector(
  getIsLoading,
  (data: boolean) => data
);
