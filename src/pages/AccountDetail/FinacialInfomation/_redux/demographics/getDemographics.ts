import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { IDemoGraphics, InitialStateFinancialInformation } from '../../types';
import { demographicsController } from '../../__mock__/demographicsController';
import { set } from 'app/helpers';
interface IPayloadGetDemographics {
  data: IDemoGraphics;
}
const GET_DEMOGRAPHICS = 'getDemographics';

export const getDemographics = createAsyncThunk<
  IPayloadGetDemographics,
  { storeId: string },
  ThunkAPIConfig
>(GET_DEMOGRAPHICS, async (_, thunkAPI) => {
  try {
    const { data } = await demographicsController.getDemographics();
    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const getDemographicsBuilder = (
  builder: ActionReducerMapBuilder<InitialStateFinancialInformation>
) => {
  const { pending, fulfilled, rejected } = getDemographics;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      
      set(draftState, `${storeId}.demoGraphics.isLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      set(draftState, `${storeId}.demoGraphics.isLoading`, false);
      set(draftState, `${storeId}.demoGraphics.expandViewAll`, true);
      set(draftState, `${storeId}.demoGraphics.data`, action.payload);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      set(draftState, `${storeId}.demoGraphics.isLoading`, false);
    });
};
