import { PayloadAction } from '@reduxjs/toolkit';
import { InitialStateFinancialInformation } from '../../types';

export const onSetModalFinancial = (
  draftState: InitialStateFinancialInformation,
  action: PayloadAction<{ storeId: string; isOpen: boolean }>
) => {
  const { storeId, isOpen } = action.payload;
  draftState[storeId].demoGraphics.isOpenModal = isOpen;
};
export const onSetExpandViewAll = (
  draftState: InitialStateFinancialInformation,
  action: PayloadAction<{ storeId: string; isExpand: boolean }>
) => {
  const { storeId, isExpand } = action.payload;
  draftState[storeId].demoGraphics.expandViewAll = isExpand;
};
