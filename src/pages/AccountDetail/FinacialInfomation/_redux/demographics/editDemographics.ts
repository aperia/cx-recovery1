import { createAsyncThunk, ActionReducerMapBuilder } from '@reduxjs/toolkit';
import { IDemoGraphics, InitialStateFinancialInformation } from '../../types';

import { set } from 'app/helpers';
import { demographicsController } from '../../__mock__/demographicsController';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { I18N_TEXT } from 'app/constants';
import { financialInformationActions } from '..';
import { batch } from 'react-redux';

const EDIT_DEMOGRAPHICS = 'editDemographics';

export const editDemographics = createAsyncThunk<
  void,
  { storeId: string; changeData: IDemoGraphics },
  ThunkAPIConfig
>(EDIT_DEMOGRAPHICS, async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;
  try {
    const { changeData, storeId } = args;
    const { success } = await demographicsController.editDemographics(changeData);
    if (success) {
      batch(() => {
        dispatch(financialInformationActions.getDemographics({ storeId }));
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_TEXT.UPDATE_DEMOGRAPHICS_SUCCESS
          })
        );
        dispatch(
          financialInformationActions.onSetModalFinancial({
            storeId,
            isOpen: false
          })
        );
      });
    }
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.UPDATE_DEMOGRAPHICS_FAIL
      })
    );
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const editDemographicsBuilder = (
  builder: ActionReducerMapBuilder<InitialStateFinancialInformation>
) => {
  const { pending, rejected, fulfilled } = editDemographics;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.demographics.isLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      set(draftState, `${storeId}.demographics.isLoading`, false);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      set(draftState, `${storeId}.demographics.isLoading`, false);
    });
};
