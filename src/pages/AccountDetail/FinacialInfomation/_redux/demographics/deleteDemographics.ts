import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { I18N_TEXT } from 'app/constants';

import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { IDemoGraphics, InitialStateFinancialInformation } from '../../types';
import { demographicsController } from '../../__mock__/demographicsController';
import { set } from 'app/helpers';

const DELETE_DEMOGRAPHICS = 'deleteDemographics';

export const deleteDemographics = createAsyncThunk<
  void,
  { storeId: string },
  ThunkAPIConfig
>(DELETE_DEMOGRAPHICS, async (_, thunkAPI) => {
  const { dispatch } = thunkAPI;
  try {
    const { success } = await demographicsController.deleteDemographics();
    if (success) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.DELETE_DEMOGRAPHICS_SUCCESS
        })
      );
      dispatch(rootModalActions.close());
    }
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.DELETE_DEMOGRAPHICS_FAIL
      })
    );
    dispatch(rootModalActions.close());
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const deleteDemographicsBuilder = (
  builder: ActionReducerMapBuilder<InitialStateFinancialInformation>
) => {
  const { pending, fulfilled, rejected } = deleteDemographics;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      set(draftState, `${storeId}.demoGraphics.isLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId].demoGraphics.isLoading = false;
      set(draftState, `${storeId}.demoGraphics.isLoading`, false);
      set(draftState, `${storeId}.demoGraphics.data`, {} as IDemoGraphics);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      set(draftState, `${storeId}.demoGraphics.isLoading`, false);
    });
};
