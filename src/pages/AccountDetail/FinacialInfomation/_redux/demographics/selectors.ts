import { createSelector } from '@reduxjs/toolkit';
import { IDemoGraphics } from '../../types';

export const selectorIsOpenModalFinancial = createSelector(
  [
    (states: RootState, storeId: string): boolean =>
      states?.financialInformation[storeId]?.demoGraphics?.isOpenModal
  ],
  (data: boolean) => data
);

export const selectorExpandViewAllDemographics = createSelector(
  [
    (states: RootState, storeId: string): boolean =>
      states.financialInformation[storeId]?.demoGraphics?.expandViewAll
  ],
  (data: boolean) => data
);

export const selectorDemographicsData = createSelector(
  [
    (states: RootState, storeId: string): IDemoGraphics =>
      states.financialInformation[storeId]?.demoGraphics?.data
  ],
  (data: IDemoGraphics) => data
);

export const selectorLoadingDemographicsData = createSelector(
  [
    (states: RootState, storeId: string): boolean =>
      states.financialInformation[storeId]?.demoGraphics?.isLoading
  ],
  (data: boolean): boolean => data
);