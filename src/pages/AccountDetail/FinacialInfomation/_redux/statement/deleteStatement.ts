import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { financialInformationActions } from '..';

// constants
import { API_ERROR, I18N_TEXT } from 'app/constants';

// service
import statementServices from '../../Statement/statementServices';

// types
import {
  IFinancialStatement,
  InitialStateFinancialInformation
} from '../../types';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

export interface IPayloadDeleteStatement {
  data?: IFinancialStatement;
}
export interface IDeleteStatementArgs {
  id: string;
  storeId: string;
  accNumber: string;
}

export const deleteStatement = createAsyncThunk<
  IPayloadDeleteStatement,
  IDeleteStatementArgs,
  ThunkAPIConfig
>('financialInformation/deleteStatement', async (args, thunkAPI) => {
  const { storeId, accNumber, id } = args;
  const { dispatch } = thunkAPI;

  try {
    const { data } = await statementServices.deleteStatement({ id });
    batch(async () => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.DELETE_STATEMENT_SUCCESS
        })
      );

      await dispatch(
        financialInformationActions.getStatement({
          accNumber,
          storeId
        })
      );

      dispatch(rootModalActions.close());
    });

    return { data: data.data };
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.DELETE_STATEMENT_FAIL
      })
    );

    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  } finally {
    dispatch(rootModalActions.close());
  }
});

export const deleteStatementBuilder = (
  builder: ActionReducerMapBuilder<InitialStateFinancialInformation>
) => {
  const { pending, fulfilled, rejected } = deleteStatement;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      const { form } = draftState[storeId].statement;

      draftState[storeId] = {
        ...draftState[storeId],
        statement: {
          form: {
            ...form,
            isLoading: true,
            isError: false
          }
        }
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      const { form } = draftState[storeId].statement;

      draftState[storeId] = {
        ...draftState[storeId],
        statement: {
          form: {
            ...form,
            isLoading: false,
            isError: false,
            selected: data
          }
        }
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      const { form } = draftState[storeId].statement;

      draftState[storeId] = {
        ...draftState[storeId],
        statement: {
          form: {
            ...form,
            isLoading: false,
            isError: true
          }
        }
      };
    });
};
