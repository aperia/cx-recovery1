import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { financialInformationActions } from '..';

// constants
import { API_ERROR, I18N_TEXT } from 'app/constants';
import { FormikHelpers } from 'formik';

// service
import statementServices from '../../Statement/statementServices';

// types
import {
  IFinancialStatement,
  InitialStateFinancialInformation
} from '../../types';

export interface IPayloadSaveStatement {
  data?: IFinancialStatement;
}
export interface ISaveStatementArgs {
  id?: string;
  values: any;
  formikBag: FormikHelpers<any>;
  storeId: string;
  accNumber: string;
}

export const saveStatement = createAsyncThunk<
  IPayloadSaveStatement,
  ISaveStatementArgs,
  ThunkAPIConfig
>('financialInformation/saveStatement', async (args, thunkAPI) => {
  const { storeId, accNumber, id, values, formikBag } = args;
  const { resetForm, setSubmitting } = formikBag;
  const { dispatch } = thunkAPI;

  try {
    const response = await statementServices.saveStatement({
      id,
      values
    });

    batch(async () => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: id
            ? I18N_TEXT.EDIT_STATEMENT_SUCCESS
            : I18N_TEXT.ADD_STATEMENT_SUCCESS
        })
      );

      await dispatch(
        financialInformationActions.getStatement({
          accNumber,
          storeId
        })
      );

      // close modal

      dispatch(
        financialInformationActions.onToggleStatementFormModal({
          storeId,
          isOpen: false
        })
      );
    });

    resetForm();

    return response.data;
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: id
          ? I18N_TEXT.EDIT_STATEMENT_FAIL
          : I18N_TEXT.ADD_STATEMENT_FAIL
      })
    );

    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  } finally {
    setSubmitting(false);
  }
});

export const saveStatementBuilder = (
  builder: ActionReducerMapBuilder<InitialStateFinancialInformation>
) => {
  const { pending, fulfilled, rejected } = saveStatement;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      const { form } = draftState[storeId].statement;

      draftState[storeId] = {
        ...draftState[storeId],
        statement: {
          form: {
            ...form,
            isLoading: true,
            isError: false
          }
        }
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      const { form } = draftState[storeId].statement;

      draftState[storeId] = {
        ...draftState[storeId],
        statement: {
          form: {
            ...form,
            isLoading: false,
            isError: false,
            selected: data
          }
        }
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      const { form } = draftState[storeId].statement;

      draftState[storeId] = {
        ...draftState[storeId],
        statement: {
          form: {
            ...form,
            isLoading: false,
            isError: true
          }
        }
      };
    });
};
