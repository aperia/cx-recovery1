import { createSelector } from '@reduxjs/toolkit';

// types
import {
  IFinancialStatement,
  ISnapshotStatement,
  IStatementFormReducer
} from '../../types';

const getStatementForm = (
  state: RootState,
  storeId: string
): IStatementFormReducer => {
  return state.financialInformation[storeId]?.statement?.form;
};

export const selectStatementFormIsOpen = createSelector(
  getStatementForm,
  (data: IStatementFormReducer): boolean => !!data?.isOpen
);

export const selectIsLoadingStatement = createSelector(
  getStatementForm,
  (data: IStatementFormReducer): boolean => !!data?.isLoading
);

export const selectSelectedStatementForm = createSelector(
  getStatementForm,
  (data: IStatementFormReducer): IFinancialStatement | undefined =>
    data?.selected
);

export const selectSnapshotStatementForm = createSelector(
  getStatementForm,
  (data: IStatementFormReducer): ISnapshotStatement | undefined =>
    data?.snapshot
);
