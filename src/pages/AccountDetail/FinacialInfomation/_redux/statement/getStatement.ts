import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// constants
import { API_ERROR } from 'app/constants';

// service
import statementServices from '../../Statement/statementServices';

// helpers
import { prepareSnapshot } from '../../Statement/helpers';

// types
import {
  IFinancialStatement,
  InitialStateFinancialInformation
} from '../../types';

export interface IPayloadGetStatement {
  data?: IFinancialStatement;
}
export interface IGetStatementArgs {
  storeId: string;
  accNumber: string;
}

export const getStatement = createAsyncThunk<
  IPayloadGetStatement,
  IGetStatementArgs,
  ThunkAPIConfig
>('financialInformation/getStatement', async (args, thunkAPI) => {
  const { storeId, accNumber } = args;

  try {
    const response = await statementServices.getStatement({
      storeId,
      accNumber
    });

    return response.data;
  } catch (error) {
    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  }
});

export const getStatementBuilder = (
  builder: ActionReducerMapBuilder<InitialStateFinancialInformation>
) => {
  const { pending, fulfilled, rejected } = getStatement;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      const { form } = draftState[storeId]?.statement || {
        form: {
          isOpen: false,
          isLoading: false,
          isError: false
        }
      };

      draftState[storeId] = {
        ...draftState[storeId],
        statement: {
          form: {
            ...form,
            isLoading: true,
            isError: false
          }
        }
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      const snapshot = prepareSnapshot(data);

      const { form } = draftState[storeId].statement;

      draftState[storeId] = {
        ...draftState[storeId],
        statement: {
          form: {
            ...form,
            isLoading: false,
            isError: false,
            selected: data,
            snapshot
          }
        }
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      const { form } = draftState[storeId].statement;

      draftState[storeId] = {
        ...draftState[storeId],
        statement: {
          form: {
            ...form,
            isLoading: false,
            isError: true,
            snapshot: undefined
          }
        }
      };
    });
};
