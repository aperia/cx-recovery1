import { PayloadAction } from '@reduxjs/toolkit';
import { isBoolean } from 'lodash';
import {
  IFinancialStatement,
  InitialStateFinancialInformation
} from '../../types';

export const onToggleStatementFormModal = (
  draftState: InitialStateFinancialInformation,
  action: PayloadAction<{
    storeId: string;
    isOpen?: boolean;
    selected?: IFinancialStatement;
  }>
) => {
  const { storeId, isOpen, selected } = action.payload;

  const { form } = draftState[storeId]?.statement || {
    form: {
      isOpen: false,
      isLoading: false,
      isError: false
    }
  };

  draftState[storeId] = {
    ...draftState[storeId],
    statement: {
      form: {
        ...form,
        ...(selected ? selected : form?.selected),
        isOpen: isBoolean(isOpen) ? isOpen : !form.isOpen
      }
    }
  };
};
