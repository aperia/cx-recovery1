import { IDemoGraphics } from '../types';
import { demographics } from './demographics.json';

let demographicsData = JSON.parse(JSON.stringify(demographics));

const isHaveError = window.location.href.includes('error');
const isHaveEmpty = window.location.href.includes('empty');

export const demographicsController = {
  getDemographics() {
    return Promise.resolve({
      success: true,
      data: isHaveEmpty ? {} : { ...demographicsData }
    });
  },

  addDemographics(demographics: IDemoGraphics) {
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: 'ADD FAILED'
      });

    demographicsData = { ...demographics };
    return Promise.resolve({
      success: true
    });
  },
  editDemographics(dataChange: IDemoGraphics) {
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: 'EDIT FAILED'
      });

    demographicsData = { ...dataChange };
    return Promise.resolve({
      success: true
    });
  },
  deleteDemographics() {
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: 'DELETE FAILED'
      });
    demographicsData = {};

    return Promise.resolve({ success: true });
  }
};
