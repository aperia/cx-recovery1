import { ICreditor } from '../types';
import creditorJson from './creditor.json';

let creditor: ICreditor = JSON.parse(JSON.stringify(creditorJson));

export const creditorController = {
  getCreditor() {
    return Promise.resolve({ data: { success: true, data: creditor } });
  },
  deleteCreditor() {
    creditor = {} as ICreditor;
    return Promise.resolve({ data: { success: true } });
  },
  addEditCreditor(value: ICreditor) {
    creditor = value;
    return Promise.resolve({ data: { success: true } });
  }
};
