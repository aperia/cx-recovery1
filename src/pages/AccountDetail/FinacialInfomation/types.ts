export interface ICreditor {
  creditorName: string;
  loanPurpose: string;
  monthlyPaymentAmount: string;
  debtBalance: string;
  pastDueAmount: string;
  finalJudgment: string;
}

export interface IFinancialStatement {
  id?: string;
  paymentAmount?: string;
  montageOrRentDueDate?: string;
  paidTo?: string;
  ownerOrRenterType?: string;
}
export interface ISnapshotStatement extends IFinancialStatement {}

export interface IStatementFormReducer {
  isOpen: boolean;
  isLoading: boolean;
  isError: boolean;
  selected?: any;
  snapshot?: any;
}

export interface IStatement {
  form: IStatementFormReducer;
}

export interface IStateCreditor {
  isOpenDeleteModal: boolean;
  isLoading: boolean;
  isError: boolean;
  isLoadingModal: boolean;
  isOpenForm: boolean;
  dataForm: ICreditor;
  data: ICreditor;
}

export interface IDebtorInformation {
  primaryDebtorName: string;
  primaryDebtorHomePhone: string;
  primaryDebtorAddress: string;
  employerNameDebtor: string;
  employerAddressDebtor: string;
  employerPhoneDebtor: string;
  contractTypeDebtor: string;
  startDateDebtor: string;
  lastPayDateDebtor: string;
  nextPayDateDebtor: string;
  salaryTypeDebtor: string;
  netMonthlySalaryDebtor: string;
  grossMonthlySalaryDebtor: string;
  deductionDebtor: string;
  comment: string;

  addressLine1: string;
  addressLine2: string;
  city: string;
  state: string;
  zipCode: string;

  addressEmploymentDebtor: string;

  cityEmploymentDebtor: string;
  stateEmploymentDebtor: string;
  zipCodeEmploymentDebtor: string;
}

export interface ISpouseInformation {
  spouseName: string;
  homePhoneSpouse: string;
  spouseAddress: string;
  employerNameSpouse: string;
  employerAddressSpouse: string;
  employerPhoneSpouse: string;
  contractTypeSpouse: string;
  startDateSpouse: string;
  lastPayDateSpouse: string;
  nextPayDateSpouse: string;
  salaryTypeSpouse: string;
  netMonthlySalarySpouse: string;
  grossMonthlySalarySpouse: string;
  deductionSpouse: string;

  spouseAddressLine1: string;
  spouseAddressLine2: string;
  citySpouse: string;
  stateSpouse: string;
  zipCodeSpouse: string;

  addressEmploymentSpouse: string;
  
  cityEmploymentSpouse: string;
  stateEmploymentSpouse: string;
  zipCodeEmploymentSpouse: string;
}

export interface IChildrenInformation {
  numberOfChildren: string;
  child1Age: string;
  child2Age: string;
  child3Age: string;
  child4Age: string;
  child5Age: string;
  child6Age: string;
  child7Age: string;
  child8Age: string;
  child9Age: string;
  child10Age: string;
  supportPersonName: string;
  supportAmount: string;
  supportCountry: string;
  supportType: string;
  supportMethod: string;
}

export interface IDemoGraphics {
  debtorInformation: IDebtorInformation;
  spouseInformation: ISpouseInformation;
  childrenInformation: IChildrenInformation;
}
export interface IStateDemographics {
  isOpenModal: boolean;
  expandViewAll: boolean;
  isLoading: boolean;
  data: IDemoGraphics;
}
export interface InitialStateFinancialInformation {
  [storeId: string]: IFinancialInformation;
}

export interface IFinancialInformation {
  demoGraphics: IStateDemographics;
  statement: IStatement;
  creditor: IStateCreditor;
}
