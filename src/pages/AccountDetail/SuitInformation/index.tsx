import React, { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelectorId, useAccountDetail } from 'app/hooks';

// components
import If from 'pages/__commons/If';
import { NoDataGrid } from 'app/components';
import { Button } from 'app/_libraries/_dls/components';
import SuitInformationForm from './Form';
import SuitInfoConfirmBody from './SuitInfoConfirmBody';

// actions
import { suitInformationActions } from './_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

// selectors
import {
  selectFlagMiscellaneous,
  selectIsLoadingSuitInfo,
  selectSelectedSuitInfoForm,
  selectSnapshotSuitInfoForm
} from './_redux/selectors';

// types
import { ISnapshotSuitInfo, ISuitInfo } from './types';

// constants
import { I18N_TEXT } from 'app/constants';

// helpers
import classnames from 'classnames';
import { isEmpty } from 'app/helpers';
import usePrepareSnapshot from './hooks/usePrepareSnapshot';
import { View } from 'app/_libraries/_dof/core';
import { prepareDateViewMiscellaneous } from './helpers';

export interface SuitInformationProps {}
const SuitInformation: React.FC<SuitInformationProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId, accNumber } = useAccountDetail();
  const loading = useSelectorId<boolean>(selectIsLoadingSuitInfo);

  const selected = useSelectorId<ISuitInfo | undefined>(
    selectSelectedSuitInfoForm
  );

  const snapShot = useSelectorId<ISnapshotSuitInfo | undefined>(
    selectSnapshotSuitInfoForm
  );
  const hasMiscellaneousData = useSelectorId<boolean>(selectFlagMiscellaneous);

  const snapShotData = usePrepareSnapshot(snapShot, storeId);

  const handleShowEditForm = useCallback(() => {
    dispatch(
      suitInformationActions.onToggleSuitInfoFormModal({
        storeId,
        selected,
        isOpen: true
      })
    );
  }, [dispatch, selected, storeId]);

  const handleShowConfirmDelete = useCallback(() => {
    dispatch(
      rootModalActions.open({
        autoClose: false,
        title: I18N_TEXT.DELETE_SUIT_INFO_CONFIRM_TITLE,
        body: <SuitInfoConfirmBody data={selected} />,
        btnConfirmText: I18N_TEXT.DELETE,
        btnCancelText: I18N_TEXT.CANCEL,
        onConfirm: () => {
          dispatch(rootModalActions.loading(true));

          dispatch(
            suitInformationActions.deleteSuitInfo({
              storeId,
              accNumber,
              id: selected?.id || ''
            })
          );
        },
        onCancel: () => {
          dispatch(rootModalActions.close());
        }
      })
    );
  }, [accNumber, dispatch, selected, storeId]);

  useEffect(() => {
    dispatch(
      suitInformationActions.getSuitInfo({
        accNumber: accNumber,
        storeId
      })
    );
  }, [dispatch, storeId, accNumber]);

  return (
    <div className={classnames('h-100', { loading })}>
      <div className="px-24 pt-24 max-width-lg mx-auto overlap-next-action">
        <div className="d-flex align-items-center">
          <h4>{t(I18N_TEXT.SUIT_INFORMATION)}</h4>

          <If condition={!isEmpty(snapShotData)}>
            <div className="ml-auto">
              <Button
                size="sm"
                variant="outline-danger"
                onClick={handleShowConfirmDelete}
              >
                {t(I18N_TEXT.DELETE)}
              </Button>
              <Button
                className="mr-n8"
                size="sm"
                variant="outline-primary"
                onClick={handleShowEditForm}
              >
                {t(I18N_TEXT.EDIT)}
              </Button>
            </div>
          </If>
        </div>
        {!isEmpty(snapShotData) && (
          <>
            <View
              id={`suitInformationView-${storeId}`}
              formKey={`suitInformationView-${storeId}`}
              descriptor="suitInformationView"
              value={{
                ...snapShotData
              }}
            />
            {hasMiscellaneousData ? (
              <View
                id={`suitInformationMiscellaneousView-${storeId}`}
                formKey={`suitInformationMiscellaneousView-${storeId}`}
                descriptor="suitInformationMiscellaneousView"
                value={{
                  ...prepareDateViewMiscellaneous(snapShotData)
                }}
              />
            ) : (
              <p className="color-grey mt-16">
                {t(I18N_TEXT.NO_DATA_TO_DISPLAY)}
              </p>
            )}
          </>
        )}

        <If condition={isEmpty(snapShotData)}>
          <NoDataGrid text={I18N_TEXT.NO_DATA_TO_DISPLAY}>
            <Button
              className="text-capitalize mt-24"
              variant="outline-primary"
              size="sm"
              onClick={() => {
                dispatch(
                  suitInformationActions.onToggleSuitInfoFormModal({
                    storeId,
                    isOpen: true
                  })
                );
              }}
            >
              {t(I18N_TEXT.ADD_SUIT_INFORMATION)}
            </Button>
          </NoDataGrid>
        </If>

        <SuitInformationForm />
      </div>
    </div>
  );
};

export default React.memo(SuitInformation);
