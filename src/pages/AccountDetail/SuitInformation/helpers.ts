import { isEmpty } from 'app/helpers';
import { IAddressData } from '../BankruptcyDeceaseRecord/types';
export const prepareDateViewMiscellaneous = (dataView: any) => {
  if (isEmpty(dataView)) return {};
  return {
    shortField1: dataView?.shortField1,
    shortField2: dataView?.shortField2,
    dateField1: dataView?.dateField1,
    dateField2: dataView?.dateField2,
    longField1: dataView?.longField1,
    longField2: dataView?.longField2,
    amount1: dataView?.amount1,
    amount2: dataView?.amount2
  };
};

export const formatAddressPlaintiff = ({
  addressLine1,
  addressLine2,
  city,
  state,
  zipCode
}: IAddressData) => {
  let result = '';
  addressLine1 && (result += addressLine1);
  addressLine2 && (result += ` \n${addressLine2}`);
  city && (result += `\n${city}`);
  state && (result += `, ${state}`);
  zipCode && (result += ` ${zipCode}`);
  return result;
};