import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { saveSuitInfo, saveSuitInfoBuilder } from './saveSuitInfo';
import { getSuitInfo, getSuitInfoBuilder } from './getSuitInfo';
import { deleteSuitInfo, deleteSuitInfoBuilder } from './deleteSuitInfo';

// types
import { ISuitInfoState, ToggleSuitInfoFormModalArgs } from './../types';
import { IInitialStateSuitInfo } from '../types';

// helpers
import { isBoolean } from 'app/helpers';

const initialState: IInitialStateSuitInfo = {};

export const { actions, reducer } = createSlice({
  name: 'suitInformation',
  initialState: initialState,
  reducers: {
    onToggleSuitInfoFormModal: (
      draftState: IInitialStateSuitInfo,
      action: PayloadAction<ToggleSuitInfoFormModalArgs>
    ) => {
      const {
        storeId,
        isOpen: isOpenProp,
        selected: selectedProp
      } = action.payload;

      const { isOpen, selected } =
        draftState[storeId] ||
        ({
          isOpen: false,
          isLoading: false,
          isError: false
        } as ISuitInfoState);

      draftState[storeId] = {
        ...draftState[storeId],
        ...(selectedProp ? selectedProp : selected),
        isOpen: isBoolean(isOpenProp) ? isOpenProp : !isOpen
      };
    },
    setFlagMiscellaneous: (
      draftState: IInitialStateSuitInfo,
      action: PayloadAction<{ storeId: string; hasData: boolean }>
    ) => {
      const { storeId, hasData } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        hasMiscellaneousData: hasData
      };
    }
  },
  extraReducers: builder => {
    saveSuitInfoBuilder(builder);
    getSuitInfoBuilder(builder);
    deleteSuitInfoBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  saveSuitInfo,
  getSuitInfo,
  deleteSuitInfo
};

export { combineActions as suitInformationActions };
