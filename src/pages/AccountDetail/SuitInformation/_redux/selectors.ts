import { createSelector } from '@reduxjs/toolkit';

// types
import { ISnapshotSuitInfo, ISuitInfo, ISuitInfoState } from '../types';

const getStatementState = (
  state: RootState,
  storeId: string
): ISuitInfoState => {
  return state.suitInformation[storeId];
};

export const selectSuitInfoFormIsOpen = createSelector(
  getStatementState,
  (data: ISuitInfoState): boolean => !!data?.isOpen
);

export const selectIsLoadingSuitInfo = createSelector(
  getStatementState,
  (data: ISuitInfoState): boolean => !!data?.isLoading
);

export const selectSelectedSuitInfoForm = createSelector(
  getStatementState,
  (data: ISuitInfoState): ISuitInfo | undefined => data?.selected
);

export const selectSnapshotSuitInfoForm = createSelector(
  getStatementState,
  (data: ISuitInfoState): ISnapshotSuitInfo | undefined => data?.snapshot
);

export const selectFlagMiscellaneous = createSelector(
  getStatementState,
  (data: ISuitInfoState): boolean => !!data?.hasMiscellaneousData
);
