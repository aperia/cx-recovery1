import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { suitInformationActions } from '.';

// constants
import { API_ERROR, I18N_TEXT } from 'app/constants';
import { FormikHelpers } from 'formik';

// service
import suitInfoServices from '../suitInfoServices';

// types
import { IInitialStateSuitInfo, ISuitInfo } from '../types';

export interface IPayloadSuitInfo {
  data?: ISuitInfo;
}
export interface ISuitInfoArgs {
  id?: string;
  values: any;
  formikBag: FormikHelpers<any>;
  storeId: string;
  accNumber: string;
}

export const saveSuitInfo = createAsyncThunk<
  IPayloadSuitInfo,
  ISuitInfoArgs,
  ThunkAPIConfig
>('suitInformation/saveSuitInfo', async (args, thunkAPI) => {
  const { storeId, accNumber, id, values, formikBag } = args;

  const { resetForm, setSubmitting } = formikBag;
  const { dispatch } = thunkAPI;

  try {
    const response = await suitInfoServices.saveSuitInfo({
      id,
      values
    });

    batch(async () => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: id
            ? I18N_TEXT.EDIT_SUIT_INFO_SUCCESS
            : I18N_TEXT.ADD_SUIT_INFO_SUCCESS
        })
      );

      await dispatch(
        suitInformationActions.getSuitInfo({
          accNumber,
          storeId
        })
      );

      // close modal

      dispatch(
        suitInformationActions.onToggleSuitInfoFormModal({
          storeId,
          isOpen: false
        })
      );
    });

    resetForm();

    return response.data;
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: id
          ? I18N_TEXT.EDIT_SUIT_INFO_FAIL
          : I18N_TEXT.ADD_SUIT_INFO_FAIL
      })
    );

    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  } finally {
    setSubmitting(false);
  }
});

export const saveSuitInfoBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateSuitInfo>
) => {
  const { pending, fulfilled, rejected } = saveSuitInfo;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true,
        isError: false
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: false,
        selected: data
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: true
      };
    });
};
