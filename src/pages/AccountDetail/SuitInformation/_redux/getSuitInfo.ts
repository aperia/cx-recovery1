import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// constants
import { API_ERROR } from 'app/constants';

// service
import suitInfoServices from 'pages/AccountDetail/SuitInformation/suitInfoServices';

// types
import {
  IInitialStateSuitInfo,
  ISuitInfo
} from 'pages/AccountDetail/SuitInformation/types';

export interface IPayloadGetSuitInfo {
  data?: ISuitInfo;
}
export interface IGetSuitInfoArgs {
  storeId: string;
  accNumber: string;
}

export const getSuitInfo = createAsyncThunk<
  IPayloadGetSuitInfo,
  IGetSuitInfoArgs,
  ThunkAPIConfig
>('suitInformation/getSuitInfo', async (args, thunkAPI) => {
  const { storeId, accNumber } = args;

  try {
    const response = await suitInfoServices.getSuitInfo({
      storeId,
      accNumber
    });

    return response.data;
  } catch (error) {
    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  }
});

export const getSuitInfoBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateSuitInfo>
) => {
  const { pending, fulfilled, rejected } = getSuitInfo;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true,
        isError: false
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: false,
        selected: data,
        snapshot: data
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: true,
        snapshot: undefined
      };
    });
};
