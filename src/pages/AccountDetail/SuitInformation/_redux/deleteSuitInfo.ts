import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { suitInformationActions } from '.';

// constants
import { API_ERROR, I18N_TEXT } from 'app/constants';

// service
import suitInfoServices from '../suitInfoServices';

// types
import {
  IInitialStateSuitInfo,
  ISuitInfo
} from 'pages/AccountDetail/SuitInformation/types';

export interface IPayloadDeleteSuitInfo {
  data?: ISuitInfo;
}
export interface IDeleteSuitInfoArgs {
  id: string;
  storeId: string;
  accNumber: string;
}

export const deleteSuitInfo = createAsyncThunk<
  IPayloadDeleteSuitInfo,
  IDeleteSuitInfoArgs,
  ThunkAPIConfig
>('suitInformation/deleteSuitInfo', async (args, thunkAPI) => {
  const { storeId, accNumber, id } = args;
  const { dispatch } = thunkAPI;

  try {
    const { data } = await suitInfoServices.deleteSuitInfo({ id });
    batch(async () => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.DELETE_SUIT_INFO_SUCCESS
        })
      );

      await dispatch(
        suitInformationActions.getSuitInfo({
          accNumber,
          storeId
        })
      );
    });

    return { data: data.data };
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.DELETE_SUIT_INFO_FAIL
      })
    );

    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  } finally {
    dispatch(rootModalActions.close());
  }
});

export const deleteSuitInfoBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateSuitInfo>
) => {
  const { pending, fulfilled, rejected } = deleteSuitInfo;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true,
        isError: false
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: false,
        selected: data
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: true
      };
    });
};
