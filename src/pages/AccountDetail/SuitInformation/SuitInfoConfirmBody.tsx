import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// types
import { ISuitInfo } from 'pages/AccountDetail/SuitInformation/types';

// helper
import { I18N_TEXT } from 'app/constants';
import If from 'pages/__commons/If';

export interface SuitInfoConfirmBodyProps {
  data?: ISuitInfo;
}

const SuitInfoConfirmBody: React.FC<SuitInfoConfirmBodyProps> = ({ data }) => {
  const { t } = useTranslation();

  return (
    <If condition={!!data}>
        <p>{t(I18N_TEXT.DELETE_SUIT_INFO_CONFIRM_BODY)}</p>
    </If>
  );
};

export default SuitInfoConfirmBody;
