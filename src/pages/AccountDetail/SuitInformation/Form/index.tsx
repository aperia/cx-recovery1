import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';

// components
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import PlaintiffInformation from './PlaintiffInformation';
import CourtInformation from './CourtInformation';
import Miscellaneous from './Miscellaneous';

// actions
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { suitInformationActions } from '../_redux';

// selectors
import {
  selectSelectedSuitInfoForm,
  selectSuitInfoFormIsOpen
} from '../_redux/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import useSuitInformationForm from '../hooks/useSuitInformationForm';

// types
import { ISuitInfo } from '../types';

// helpers
import { FormikProvider } from 'formik';
import { isTouchedForm } from 'app/helpers';

// constants
import { I18N_TEXT } from 'app/constants';

export interface SuitInformationFormProps {}
const SuitInformationForm: React.FC<SuitInformationFormProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  const isOpen = useSelectorId<boolean>(selectSuitInfoFormIsOpen);
  const selected = useSelectorId<ISuitInfo | undefined>(
    selectSelectedSuitInfoForm
  );

  const editMode = !!selected?.id;
  const { formik } = useSuitInformationForm();

  const {
    submitForm,
    resetForm,
    setSubmitting,
    isSubmitting,
    isValid,
    touched,
    dirty
  } = formik;

  const handleCancelForm = useCallback(() => {
    resetForm();
    setSubmitting(false);

    dispatch(
      suitInformationActions.onToggleSuitInfoFormModal({
        storeId,
        isOpen: false
      })
    );
  }, [dispatch, resetForm, setSubmitting, storeId]);

  const handleCancel = useCallback(() => {
    if (dirty && isTouchedForm(touched)) {
      return dispatch(
        rootModalActions.open({
          title: I18N_TEXT.UNSAVED_CHANGE,
          body: I18N_TEXT.DISCARD_CHANGE_BODY,
          btnCancelText: I18N_TEXT.CONTINUE_EDITING,
          btnConfirmText: I18N_TEXT.DISCARD_CHANGE,
          onConfirm: handleCancelForm
        })
      );
    }

    handleCancelForm();
  }, [dirty, touched, handleCancelForm, dispatch]);

  const handleSubmit = useCallback(() => {
    setSubmitting(true);
    submitForm();
  }, [setSubmitting, submitForm]);

  return (
    <FormikProvider value={formik}>
      <Modal lg show={isOpen} loading={false} enforceFocus={false}>
        <ModalHeader border closeButton onHide={handleCancel}>
          <ModalTitle className="text-capitalize">
            {editMode
              ? t(I18N_TEXT.EDIT_SUIT_INFORMATION)
              : t(I18N_TEXT.ADD_SUIT_INFORMATION)}
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <PlaintiffInformation />
          <CourtInformation />
          <Miscellaneous />
        </ModalBody>
        <ModalFooter
          cancelButtonText={t(I18N_TEXT.CANCEL)}
          okButtonText={editMode ? t(I18N_TEXT.SAVE) : t(I18N_TEXT.SUBMIT)}
          onCancel={handleCancel}
          onOk={handleSubmit}
          disabledOk={
            !editMode && (isSubmitting || !(isTouchedForm(touched) && isValid))
          }
        ></ModalFooter>
      </Modal>
    </FormikProvider>
  );
};

export default React.memo(SuitInformationForm);
