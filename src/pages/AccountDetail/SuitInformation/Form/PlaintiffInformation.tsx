import React from 'react';

// components
import { DropdownControl, TextBoxControl } from 'app/components';

// hooks
import useRefData from 'app/hooks/useRefData';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import {
  ALPHA_NUMERIC_SPACE_REPLACE_REGEX,
  I18N_TEXT,
  MAX_LENGTH_10,
  MAX_LENGTH_25,
  MAX_LENGTH_35,
  MAX_LENGTH_40,
  NUMERIC_REPLACE_REGEX
} from 'app/constants';

export interface PlaintiffInformationProps {}
const PlaintiffInformation: React.FC<PlaintiffInformationProps> = () => {
  const { t } = useTranslation();
  const stateOptions = useRefData('state');

  return (
    <div className="row">
      <div className="col-12">
        <h5>{t(I18N_TEXT.PLAINTIFF_INFO)}</h5>
      </div>

      <TextBoxControl
        name="plaintiffName"
        label={t(I18N_TEXT.PLAINTIFF_NAME)}
        className="col-12 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_40}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
        required
      />

      <TextBoxControl
        name="addressLine1"
        label={`${t(I18N_TEXT.ADDRESS_LINE_1)}`}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_35}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
        required
      />

      <TextBoxControl
        name="addressLine2"
        label={`${t(I18N_TEXT.ADDRESS_LINE_2)}`}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_35}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <TextBoxControl
        name="city"
        label={t(I18N_TEXT.CITY)}
        className="col-4 mt-16"
        maxLength={MAX_LENGTH_25}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
        required
      />

      <DropdownControl
        name="state"
        label={t(I18N_TEXT.STATE)}
        className="col-4 mt-16"
        options={stateOptions}
        isCombine
        required
      />

      <TextBoxControl
        name="zipCode"
        label={t(I18N_TEXT.ZIP_CODE)}
        className="col-4 mt-16"
        maxLength={MAX_LENGTH_10}
        regex={NUMERIC_REPLACE_REGEX}
        required
      />
    </div>
  );
};

export default PlaintiffInformation;
