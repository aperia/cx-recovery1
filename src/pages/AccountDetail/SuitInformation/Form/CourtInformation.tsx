import React, { useMemo } from 'react';

// components
import { DatePickerControl, TextBoxControl } from 'app/components';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { useFormikContext } from 'formik';

// constants
import {
  ALPHA_NUMERIC_SPACE_REPLACE_REGEX,
  I18N_TEXT,
  MAX_LENGTH_10,
  MAX_LENGTH_12,
  MAX_LENGTH_15,
  MAX_LENGTH_30,
  MAX_LENGTH_4
} from 'app/constants';
import { formatCurrency } from 'app/helpers';

export interface CourtInformationProps {}
const CourtInformation: React.FC<CourtInformationProps> = () => {
  const { t } = useTranslation();
  const { values, handleChange } = useFormikContext();

  const {
    principalAmount = 0,
    otherIncome = 0,
    interestAmount = 0,
    costAmount = 0
  } = values as any;

  const totalValue = useMemo(() => {
    if (
      isNaN(+principalAmount) ||
      isNaN(+otherIncome) ||
      isNaN(+interestAmount) ||
      isNaN(+costAmount)
    ) {
      return '0';
    }

    const total =
      +principalAmount + +otherIncome + +interestAmount + +costAmount;

    return formatCurrency(total + '');
  }, [principalAmount, otherIncome, interestAmount, costAmount]);

  return (
    <div className="row mt-24">
      <div className="col-12">
        <h5>{t(I18N_TEXT.COURT_INFO)}</h5>
      </div>

      <TextBoxControl
        name="courtName"
        label={t(I18N_TEXT.COURT_NAME)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_30}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
        required
      />

      <TextBoxControl
        name="county"
        label={t(I18N_TEXT.COUNTY)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_15}
        required
      />

      <TextBoxControl
        name="sheriffName"
        label={t(I18N_TEXT.SHERIFF_NAME)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_30}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
      />

      <DatePickerControl
        name="summonsDate"
        label={t(I18N_TEXT.SUMMONS_DATE)}
        className="col-6 col-lg-4 mt-16"
      />

      <TextBoxControl
        name="docket"
        label={t(I18N_TEXT.DOCKET)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_10}
        required
      />

      <DatePickerControl
        name="judgmentDate"
        label={t(I18N_TEXT.JUDGMENT_DATE)}
        className="col-6 col-lg-4 mt-16"
        required
      />

      <TextBoxControl
        name="bookPage"
        label={t(I18N_TEXT.BOOK_PAGE)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_12}
      />

      <TextBoxControl
        name="claimNumber"
        label={t(I18N_TEXT.CLAIM_NUMBER)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_4}
        required
      />

      <div className="col-12 mt-16">
        <div className="divider-dashed" />
      </div>

      <FormikNumericControl
        name="principalAmount"
        label={t(I18N_TEXT.PRINCIPAL_AMOUNT)}
        className="col-6 col-lg-3 mt-16"
        maxLength={MAX_LENGTH_15}
        format="c2"
        onValueChange={handleChange}
        required
      />

      <FormikNumericControl
        name="otherIncome"
        label={t(I18N_TEXT.OTHER_INCOME)}
        className="col-6 col-lg-3 mt-16"
        maxLength={MAX_LENGTH_15}
        format="c2"
        onValueChange={handleChange}
        required
      />

      <FormikNumericControl
        name="interestAmount"
        label={t(I18N_TEXT.INTEREST_AMOUNT)}
        className="col-6 col-lg-3 mt-16"
        maxLength={MAX_LENGTH_15}
        format="c2"
        onValueChange={handleChange}
        required
      />

      <FormikNumericControl
        name="costAmount"
        label={t(I18N_TEXT.COST_AMOUNT)}
        className="col-6 col-lg-3 mt-16"
        maxLength={MAX_LENGTH_15}
        format="c2"
        onValueChange={handleChange}
        required
      />

      <div className="col-12 mt-16">
        <span className="color-grey fw-500 mr-8">Total:</span>
        <span>{totalValue}</span>
      </div>
    </div>
  );
};

export default CourtInformation;
