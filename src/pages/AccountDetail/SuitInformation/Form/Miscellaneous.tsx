import React from 'react';

// components
import { DatePickerControl, TextBoxControl } from 'app/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import {
  I18N_TEXT,
  MAX_LENGTH_17,
  MAX_LENGTH_20,
  MAX_LENGTH_5
} from 'app/constants';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';

export interface MiscellaneousProps {}
const Miscellaneous: React.FC<MiscellaneousProps> = () => {
  const { t } = useTranslation();

  return (
    <div className="row mt-24">
      <div className="col-12">
        <h5>{t(I18N_TEXT.MISCELLANEOUS)}</h5>
      </div>

      <TextBoxControl
        name="shortField1"
        label={t(I18N_TEXT.SHORT_FIELD_1)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_5}
      />

      <TextBoxControl
        name="shortField2"
        label={t(I18N_TEXT.SHORT_FIELD_2)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_5}
      />
      <div className="col-12 col-lg-4"></div>

      <DatePickerControl
        name="dateField1"
        label={t(I18N_TEXT.DATE_FIELD_1)}
        className="col-6 col-lg-4 mt-16"
      />
      <DatePickerControl
        name="dateField2"
        label={t(I18N_TEXT.DATE_FIELD_2)}
        className="col-6 col-lg-4 mt-16"
      />
      <div className="col-12 col-lg-4"></div>

      <TextBoxControl
        name="longField1"
        label={t(I18N_TEXT.LONG_FIELD_1)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_20}
      />
      <TextBoxControl
        name="longField2"
        label={t(I18N_TEXT.LONG_FIELD_2)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_20}
      />
      <div className="col-12 col-lg-4"></div>

      <FormikNumericControl
        name="amount1"
        label={t(I18N_TEXT.AMOUNT_1)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_17}
        format="c2"
      />

      <FormikNumericControl
        name="amount2"
        label={t(I18N_TEXT.AMOUNT_2)}
        className="col-6 col-lg-4 mt-16"
        maxLength={MAX_LENGTH_17}
        format="c2"
      />
    </div>
  );
};

export default Miscellaneous;
