// mocks
import { Repository } from 'app/utils/repositories/concretes/Repository';

// types
import { ISuitInfo } from '../types';

const isHaveError = window.location.href.includes('error');
const isHaveEmpty = window.location.href.includes('empty');
const data = [] as ISuitInfo[];

const suitInfoRepository = new Repository<ISuitInfo>(
  JSON.parse(JSON.stringify(data))
);

const getResult = (data: ISuitInfo[]) => {
  let result;

  if (isHaveEmpty) return result;

  if (Array.isArray(data) && data.length) {
    result = data[0];
  }

  return result;
};

export const suitInfoController = {
  saveSuitInfo: (args: any) => {
    const { id, values } = args;

    if (isHaveError) {
      return Promise.reject({});
    }

    const { data } = id
      ? suitInfoRepository.update(id, values)
      : suitInfoRepository.add(values);

    return Promise.resolve({
      data: { success: true, data: getResult(data) }
    });
  },
  getSuitInfo(args: any) {
    const { data } = suitInfoRepository.getAll();

    return Promise.resolve({
      data: { success: true, data: getResult(data) }
    });
  },
  deleteSuitInfo: ({ id }: any) => {
    if (isHaveError) {
      return Promise.reject({});
    }

    const { data } = suitInfoRepository.delete(id);

    return Promise.resolve({
      data: { success: true, data: getResult(data) }
    });
  }
};
