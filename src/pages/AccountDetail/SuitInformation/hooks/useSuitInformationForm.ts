import { useDispatch } from 'react-redux';

// hooks
import useValidation from './useValidation';
import { useAccountDetail, useSelectorId } from 'app/hooks';

// actions
import { suitInformationActions } from '../_redux';

// selectors
import { selectSelectedSuitInfoForm } from '../_redux/selectors';

// types
import { ISuitInfo } from '../types';

// helpers
import { useFormik } from 'formik';

const useSuitInformationForm = () => {
  const dispatch = useDispatch();
  const { storeId, accNumber } = useAccountDetail();
  const { validationSchema } = useValidation();

  const selected = useSelectorId<ISuitInfo | undefined>(
    selectSelectedSuitInfoForm
  );

  const selectedId = selected?.id;

  const defaultValues = {};

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      ...defaultValues,
      ...(selected ? { ...selected } : {})
    },
    onSubmit: (values, formikBag) => {
      dispatch(
        suitInformationActions.saveSuitInfo({
          storeId,
          accNumber,
          id: selectedId,
          values,
          formikBag
        })
      );
    },
    validationSchema
  });

  return { formik };
};

export default useSuitInformationForm;
