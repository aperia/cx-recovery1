import { useMemo } from 'react';
import { useDispatch } from 'react-redux';

// types
import { ISnapshotSuitInfo, ISuitInfo } from '../types';

// helpers
import { pick } from 'app/helpers/lodash';
import { pickBy, identity } from 'lodash';
import { formatTime, isEmpty } from 'app/helpers';
import { suitInformationActions } from '../_redux';
import { formatAddressPlaintiff } from '../helpers';

const usePrepareSnapshot = (
  data?: ISuitInfo,
  storeId?: string
): ISnapshotSuitInfo | undefined => {
  const dispatch = useDispatch();

  return useMemo(() => {
    if (!data) return;

    /** pick fields for sections goes here */
    const plaintiffInfo = pickBy(
      // pick plaintiffInfo fields
      pick(data, [
        'plaintiffName',
        'addressLine1',
        'addressLine2',
        'city',
        'state',
        'zipCode'
      ]),
      identity // remove empty fields
    );

    // pick courtInfo fields
    const courtInfo = pickBy(
      pick(data, [
        'courtName',
        'country',
        'sheriffName',
        'summonsDate',
        'docket',
        'judgmentDate',
        'bookPage',
        'claimNumber',
        'principalAmount',
        'otherIncome',
        'interestAmount',
        'costAmount'
      ]),
      identity
    );
    // pick miscellaneous fields

    const miscellaneous = pickBy(
      pick(data, [
        'shortField1',
        'shortField2',
        'dateField1',
        'dateField2',
        'longField1',
        'longField2',
        'amount1',
        'amount2'
      ]),
      identity
    );
    /** prepare custom sections goes here */
    const { addressLine1, addressLine2, city, state, zipCode } = plaintiffInfo;
    const {
      principalAmount,
      otherIncome,
      interestAmount,
      costAmount,
      summonsDate,
      judgmentDate
    } = courtInfo;
    const { dateField1, dateField2 } = miscellaneous;

    dispatch(
      suitInformationActions.setFlagMiscellaneous({
        storeId: storeId || '',
        hasData: !isEmpty(miscellaneous)
      })
    );
    const address = formatAddressPlaintiff({
      addressLine1,
      addressLine2,
      city,
      state,
      zipCode
    });
    /** add new custom sections goes here */
    return {
      ...data,
      //plaintiffInfo
      ...plaintiffInfo,
      ...(address ? { address } : {}),
      //courtInfo
      ...courtInfo,
      summonsDate: formatTime(summonsDate || '')?.date || '',
      judgmentDate: formatTime(judgmentDate || '')?.date || '',
      total: (
        Number(principalAmount) +
        Number(otherIncome) +
        Number(interestAmount) +
        Number(costAmount)
      ).toString(),
      //miscellaneous
      ...miscellaneous,
      dateField1: formatTime(dateField1 || '')?.date || '',
      dateField2: formatTime(dateField2 || '')?.date || ''
    } as ISnapshotSuitInfo;
  }, [data, dispatch, storeId]);
};

export default usePrepareSnapshot;
