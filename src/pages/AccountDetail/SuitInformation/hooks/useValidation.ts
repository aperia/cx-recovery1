// helpers
import * as Yup from 'yup';

// constants
import { I18N_TEXT } from 'app/constants';
import { SUIT_INFO_FIELD } from '../constants';

const useValidation = () => {
  const validationSchema = Yup.object().shape({
    // PlaintiffInformation
    [SUIT_INFO_FIELD.PLAINTIFF_NAME.name]: Yup.string().required(
      I18N_TEXT.PLAINTIFF_NAME_REQUIRED
    ),
    [SUIT_INFO_FIELD.ADDRESS_LINE_1.name]: Yup.string().required(
      I18N_TEXT.ADDRESS_LINE_1_REQUIRED
    ),
    [SUIT_INFO_FIELD.CITY.name]: Yup.string().required(I18N_TEXT.CITY_REQUIRED),
    [SUIT_INFO_FIELD.STATE.name]: Yup.string().required(
      I18N_TEXT.STATE_REQUIRED
    ),
    [SUIT_INFO_FIELD.ZIP_CODE.name]: Yup.string().required(
      I18N_TEXT.ZIP_CODE_REQUIRED
    ),

    // CourtInformation
    [SUIT_INFO_FIELD.COURT_NAME.name]: Yup.string().required(
      I18N_TEXT.COURT_NAME_REQUIRED
    ),
    [SUIT_INFO_FIELD.COUNTY.name]: Yup.string().required(
      I18N_TEXT.COUNTY_REQUIRED
    ),
    [SUIT_INFO_FIELD.DOCKET.name]: Yup.string().required(
      I18N_TEXT.DOCKET_REQUIRED
    ),
    [SUIT_INFO_FIELD.JUDGMENT_DATE.name]: Yup.string().required(
      I18N_TEXT.JUDGMENT_DATE_REQUIRED
    ),
    [SUIT_INFO_FIELD.CLAIM_NUMBER.name]: Yup.string().required(
      I18N_TEXT.CLAIM_NUMBER_REQUIRED
    ),
    [SUIT_INFO_FIELD.PRINCIPAL_AMOUNT.name]: Yup.string().required(
      I18N_TEXT.PRINCIPAL_AMOUNT_REQUIRED
    ),
    [SUIT_INFO_FIELD.OTHER_INCOME.name]: Yup.string().required(
      I18N_TEXT.OTHER_INCOME_REQUIRED
    ),
    [SUIT_INFO_FIELD.INTEREST_AMOUNT.name]: Yup.string().required(
      I18N_TEXT.INTEREST_AMOUNT_REQUIRED
    ),
    [SUIT_INFO_FIELD.COST_AMOUNT.name]: Yup.string().required(
      I18N_TEXT.COST_AMOUNT_REQUIRED
    )
  });

  return {
    validationSchema
  };
};

export default useValidation;
