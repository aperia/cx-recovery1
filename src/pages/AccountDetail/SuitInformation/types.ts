export interface ISuitInfo {
  id?: string;
}
export interface ISnapshotSuitInfo {
}

export interface ISuitInfoState {
  isOpen: boolean;
  isLoading: boolean;
  isError: boolean;
  selected?: ISuitInfo;
  snapshot?: ISnapshotSuitInfo;
  hasMiscellaneousData?: boolean;
}

export interface IInitialStateSuitInfo {
  [storeId: string]: ISuitInfoState;
}
export interface ToggleSuitInfoFormModalArgs {
  storeId: string;
  isOpen?: boolean;
  selected?: ISuitInfo;
}
