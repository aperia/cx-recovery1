import { suitInfoController } from './__mock__/suitInfoController';

const suitInfoServices = {
  saveSuitInfo(args: any) {
    return suitInfoController.saveSuitInfo(args);
  },
  getSuitInfo(args: any) {
    return suitInfoController.getSuitInfo(args);
  },
  deleteSuitInfo({ id }: any) {
    return suitInfoController.deleteSuitInfo({ id });
  }
};

export default suitInfoServices;
