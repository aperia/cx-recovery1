import { I18N_TEXT } from 'app/constants';

export const SUIT_INFO_FIELD = {
  // PlaintiffInformation
  PLAINTIFF_NAME: { name: 'plaintiffName', label: I18N_TEXT.PLAINTIFF_NAME },
  ADDRESS_LINE_1: { name: 'addressLine1', label: I18N_TEXT.ADDRESS_LINE_1 },
  CITY: { name: 'city', label: I18N_TEXT.CITY },
  STATE: { name: 'state', label: I18N_TEXT.STATE },
  ZIP_CODE: { name: 'zipCode', label: I18N_TEXT.ZIP_CODE },

  // CourtInformation
  COURT_NAME: { name: 'courtName', label: I18N_TEXT.COURT_NAME },
  COUNTY: { name: 'county', label: I18N_TEXT.COUNTY },
  SHERIFF_NAME: { name: 'sheriffName', label: I18N_TEXT.SHERIFF_NAME },
  DOCKET: { name: 'docket', label: I18N_TEXT.DOCKET },
  JUDGMENT_DATE: { name: 'judgmentDate', label: I18N_TEXT.JUDGMENT_DATE },
  CLAIM_NUMBER: { name: 'claimNumber', label: I18N_TEXT.CLAIM_NUMBER },
  PRINCIPAL_AMOUNT: {
    name: 'principalAmount',
    label: I18N_TEXT.PRINCIPAL_AMOUNT
  },
  OTHER_INCOME: { name: 'otherIncome', label: I18N_TEXT.OTHER_INCOME },
  INTEREST_AMOUNT: {
    name: 'interestAmount',
    label: I18N_TEXT.INTEREST_AMOUNT
  },
  COST_AMOUNT: {
    name: 'costAmount',
    label: I18N_TEXT.COST_AMOUNT
  }
};
