import { accountDetailController } from './__mock__/accountListController';

const accountDetailService = {
  getAccount: (accNumber: string) => {
    return accountDetailController.getAccount(accNumber);
  }
};
export default accountDetailService;
