import { associatedDebtorController } from '../../AccountManagement/__mock__/associatedsDebtorsController';

const bankruptcyDeceaseRecordServices = {
  getDebtorList: ({ page = 1, pageSize = 10 }) => {
    return associatedDebtorController.getAssociatedDebtors({ page, pageSize });
  }
};

export default bankruptcyDeceaseRecordServices;
