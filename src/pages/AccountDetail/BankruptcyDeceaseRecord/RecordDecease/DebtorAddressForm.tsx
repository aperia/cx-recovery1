import { ComboBoxControl, TextBoxControl } from 'app/components';
import { I18N_TEXT, NUMERIC_REPLACE_REGEX } from 'app/constants';
import { Button, SimpleBar } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { FormikProvider } from 'formik';
import React, { useLayoutEffect, useRef } from 'react';
import { useRecordDeceaseForm } from '../hooks/useRecordDeceaseForm';

const DebtorAddressForm: React.FC = () => {
  const { t } = useTranslation();
  const divRef = useRef<HTMLDivElement | null>(null);

  const { formik, stateData, handleChangeComboBox, handleClearAll } =
    useRecordDeceaseForm('debtorAddress');

  const { submitForm } = formik;

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 262px)`);
  }, []);

  return (
    <FormikProvider value={formik}>
      <div ref={divRef}>
        <SimpleBar>
          <div className="p-24 max-width-lg mx-auto">
            <div className="d-flex">
              <h5>{t(I18N_TEXT.DEBTOR_ADDRESS)}</h5>
              <span className="ml-auto" onClick={handleClearAll}>
                <Button className="mr-n8" variant="outline-primary" size="sm">
                  {t(I18N_TEXT.CLEAR_ALL)}
                </Button>
              </span>
            </div>
            <h6 className="mt-24 color-grey">{t(I18N_TEXT.DECEASED_DEBTOR)}</h6>
            <p className="mt-16">
              {t('txt_provide_the_last_address_of_the_deceased_debtor')}
            </p>
            <div className="row">
              <TextBoxControl
                readOnly
                className="col-lg-3 mt-16"
                label={t(I18N_TEXT.DECEASED_PERSON_NAME)}
                name="deceasedDebtorName"
              />
              <TextBoxControl
                className="col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_1)}
                name="deceasedDebtorAddressLine1"
              />
              <TextBoxControl
                className="col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_2)}
                name="deceasedDebtorAddressLine2"
              />
            </div>
            <div className="row">
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.CITY)}
                name="deceasedDebtorCity"
              />
              <ComboBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.STATE)}
                name="deceasedDebtorState"
                options={stateData?.state as RefDataValue[]}
                onChange={handleChangeComboBox}
                isCombine
              />
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.ZIP_CODE)}
                name="deceasedDebtorZipCode"
                maxLength={10}
              />
            </div>
            <h6 className="mt-24 color-grey">{t(I18N_TEXT.COURT)}</h6>
            <div className="row">
              <TextBoxControl
                className="col-lg-3 mt-16"
                label={t(I18N_TEXT.COURT_NAME)}
                name="courtName"
                maxLength={40}
              />
              <TextBoxControl
                className="col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_1)}
                name="courtAddressLine1"
              />
              <TextBoxControl
                className="col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_2)}
                name="courtAddressLine2"
              />
            </div>
            <div className="row">
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.CITY)}
                name="courtCity"
              />
              <ComboBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.STATE)}
                name="courtState"
                options={stateData?.state as RefDataValue[]}
                onChange={handleChangeComboBox}
                isCombine
              />
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.ZIP_CODE)}
                name="courtZipCode"
                maxLength={10}
              />
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.CASE_NUMBER)}
                name="courtCaseNumber"
                regex={NUMERIC_REPLACE_REGEX}
                maxLength={20}
              />
            </div>
          </div>
        </SimpleBar>
      </div>
      <div className="w-100 border-top px-24 py-16 text-right">
        <Button variant="primary" onClick={submitForm}>
          {t(I18N_TEXT.SAVE)}
        </Button>
      </div>
    </FormikProvider>
  );
};

export default DebtorAddressForm;
