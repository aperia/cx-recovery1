// components
import React, { Suspense, useCallback, useMemo, useState } from 'react';
import LazyLoadingFallback from 'app/components/LazyLoadingFallback';
import {
  ModalBody,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import Modal from 'app/_libraries/_dls/components/Modal/Modal';
import ExtraStaticTabs from 'app/_libraries/_dls/components/VerticalTabs';
import Overview from '../RecordBankruptcy/Overview';

// constants
import { I18N_TEXT } from 'app/constants';
import { RelationType } from 'app/constants/enums';
import { IDebtorItem } from '../types';
import { RECORD_DECEASE_TABS } from '../constants';

// redux
import { useDispatch } from 'react-redux';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { bankruptcyDeceaseRecordActions } from '../_redux';
import {
  selectSelectedAssociatedDebtor,
  selectIsDirtyForm,
  selectRecordDeceaseShow
} from '../_redux/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { isNull } from 'app/helpers';
import DebtorAddressForm from './DebtorAddressForm';

const RecordDecease: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const show: boolean = useSelectorId(selectRecordDeceaseShow);
  const associatedDebtor: IDebtorItem | undefined = useSelectorId(
    selectSelectedAssociatedDebtor
  );
  const isPrimary = useMemo(
    () => associatedDebtor?.relation === RelationType[3],
    [associatedDebtor?.relation]
  );
  const isDirtyForm = useSelectorId(selectIsDirtyForm);
  const [activeTabKey, setActiveTabKey] = useState(RECORD_DECEASE_TABS[0].id);

  const toggleRecordDecease = useCallback(() => {
    dispatch(
      bankruptcyDeceaseRecordActions.toggleRecordDecease({
        storeId
      })
    );
  }, [dispatch, storeId]);

  const confirmHideModal = useCallback(
    (callback: () => void) => {
      if (isDirtyForm) {
        dispatch(
          rootModalActions.open({
            title: I18N_TEXT.UNSAVED_CHANGE,
            body: I18N_TEXT.DISCARD_CHANGE_BODY,
            btnCancelText: I18N_TEXT.CONTINUE_EDITING,
            btnConfirmText: I18N_TEXT.DISCARD_CHANGE,
            onConfirm: callback
          })
        );
        return;
      }
      callback();
    },
    [dispatch, isDirtyForm]
  );

  const handleChangeTab = (eventKey: string | null) => {
    if (activeTabKey === eventKey) return;
    if (isNull(eventKey)) return;
    confirmHideModal(() => {
      setActiveTabKey(eventKey);
    });
  };

  return (
    <Modal show={show} full>
      <ModalHeader
        closeButton
        border
        onHide={() => confirmHideModal(toggleRecordDecease)}
      >
        <ModalTitle>{t(I18N_TEXT.RECORD_DECEASE)}</ModalTitle>
      </ModalHeader>
      <ModalBody noPadding className="overflow-hidden">
        <Overview />
        {isPrimary ? (
          <ExtraStaticTabs
            className="scrolled-nav"
            mountOnEnter
            unmountOnExit
            hideCollapsedButton={true}
            activeKey={activeTabKey}
            onSelect={handleChangeTab}
          >
            <div className="tab-left">
              <ExtraStaticTabs.Nav>
                {RECORD_DECEASE_TABS.map(({ id, title }) => {
                  return (
                    <ExtraStaticTabs.Nav.Item key={id}>
                      <ExtraStaticTabs.Nav.Link eventKey={id}>
                        {t(title)}
                      </ExtraStaticTabs.Nav.Link>
                    </ExtraStaticTabs.Nav.Item>
                  );
                })}
              </ExtraStaticTabs.Nav>
            </div>
            <ExtraStaticTabs.Content>
              {RECORD_DECEASE_TABS.map(({ id, Component }) => (
                <ExtraStaticTabs.Pane eventKey={id} key={id}>
                  <Suspense fallback={<LazyLoadingFallback />}>
                    <Component />
                  </Suspense>
                </ExtraStaticTabs.Pane>
              ))}
            </ExtraStaticTabs.Content>
          </ExtraStaticTabs>
        ) : (
          <DebtorAddressForm />
        )}
      </ModalBody>
    </Modal>
  );
};

export default RecordDecease;
