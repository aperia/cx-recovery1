import {
  ComboBoxControl,
  MaskedTextBoxControl,
  TextBoxControl
} from 'app/components';
import {
  I18N_TEXT,
  NUMERIC_REPLACE_REGEX,
  PHONE_NUMBER_MASK_INPUT_REGEX,
  PLACE_HOLDER_PHONE_NUMBER
} from 'app/constants';
import { Button, DatePicker, SimpleBar } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { FormikProvider } from 'formik';
import { isEmpty, isUndefined } from 'app/helpers';
import React, { useLayoutEffect, useRef } from 'react';
import { getDateData } from '../helpers';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';
import { useRecordDeceaseForm } from '../hooks/useRecordDeceaseForm';

const AccountForm: React.FC = () => {
  const { t } = useTranslation();
  const divRef = useRef<HTMLDivElement | null>(null);

  const {
    formik,
    stateData,
    paymentScheduleFrequencyData,
    handleChangeComboBox,
    handleClearAll
  } = useRecordDeceaseForm('account');

  const { values, errors, handleChange, submitForm } = formik;

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 262px)`);
  }, []);

  return (
    <FormikProvider value={formik}>
      <div ref={divRef}>
        <SimpleBar>
          <div className="p-24 max-width-lg mx-auto">
            <div className="d-flex">
              <h5>{t(I18N_TEXT.ACCOUNT)}</h5>
              <span className="ml-auto" onClick={handleClearAll}>
                <Button className="mr-n8" variant="outline-primary" size="sm">
                  {t(I18N_TEXT.CLEAR_ALL)}
                </Button>
              </span>
            </div>
            <h6 className="mt-24 color-grey">{t(I18N_TEXT.EXECUTOR)}</h6>
            <div className="row">
              <TextBoxControl
                className="col-lg-3 mt-16"
                label={t(I18N_TEXT.EXECUTOR_NAME)}
                name="executorName"
                maxLength={40}
              />
              <TextBoxControl
                className="col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_1)}
                name="executorAddressLine1"
              />
              <TextBoxControl
                className="col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_2)}
                name="executorAddressLine2"
              />
            </div>
            <div className="row">
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.CITY)}
                name="executorCity"
              />
              <ComboBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.STATE)}
                name="executorState"
                options={stateData?.state as RefDataValue[]}
                onChange={handleChangeComboBox}
                isCombine
              />
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.ZIP_CODE)}
                name="executorZipCode"
                maxLength={10}
              />
              <MaskedTextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.EXECUTOR_PHONE)}
                name="executorPhone"
                mask={PHONE_NUMBER_MASK_INPUT_REGEX}
                placeholder={PLACE_HOLDER_PHONE_NUMBER}
              />
            </div>
            <h6 className="mt-24 color-grey">{t(I18N_TEXT.ATTORNEY)}</h6>
            <div className="row">
              <TextBoxControl
                className="col-lg-3 mt-16"
                label={t(I18N_TEXT.ATTORNEY_NAME)}
                name="attorneyName"
                maxLength={40}
              />
              <TextBoxControl
                className="col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_1)}
                name="attorneyAddressLine1"
              />
              <TextBoxControl
                className="col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_2)}
                name="attorneyAddressLine2"
              />
            </div>
            <div className="row">
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.CITY)}
                name="attorneyCity"
              />
              <ComboBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.STATE)}
                name="attorneyState"
                options={stateData?.state as RefDataValue[]}
                onChange={handleChangeComboBox}
                isCombine
              />
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.ZIP_CODE)}
                name="attorneyZipCode"
                maxLength={10}
              />
              <MaskedTextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.ATTORNEY_PHONE)}
                name="attorneyPhone"
                mask={PHONE_NUMBER_MASK_INPUT_REGEX}
                placeholder={PLACE_HOLDER_PHONE_NUMBER}
              />
            </div>
            <h6 className="mt-24 color-grey">
              {t(I18N_TEXT.GENERAL_INFORMATION)}
            </h6>
            <div className="row">
              <div className="col-md-12 col-lg-4 mt-16">
                <DatePicker
                  label={t(I18N_TEXT.DATE_OF_DEATH)}
                  value={getDateData(values.dateOfDeath)}
                  onChange={handleChange}
                  name="dateOfDeath"
                />
              </div>
              <div className="col-md-12 col-lg-4 mt-16">
                <DatePicker
                  label={t(I18N_TEXT.PROOF_OF_CLAIM_DATE)}
                  value={getDateData(values.proofOfClaimDate)}
                  onChange={handleChange}
                  name="proofOfClaimDate"
                />
              </div>
            </div>
            <div className="row">
              <FormikNumericControl
                className="col-lg-3 mt-16"
                label={t(I18N_TEXT.PAYMENT_AMOUNT)}
                name="paymentAmount"
                format="c2"
                maxLength={13}
              />
              <ComboBoxControl
                className="col-lg-4 mt-16"
                name="paymentScheduleFrequency"
                label={t(I18N_TEXT.PAYMENT_SCHEDULE_FREQUENCY)}
                options={
                  (paymentScheduleFrequencyData?.bankruptcyDeceasePaymentScheduleFrequency ||
                    []) as RefDataValue[]
                }
                onChange={handleChangeComboBox}
              />
              {values.paymentScheduleFrequency === 'custom' && (
                <TextBoxControl
                  required
                  className="col-lg-2 mt-16"
                  name="paymentScheduleFrequencyCount"
                  label={t(I18N_TEXT.COUNT)}
                  suffixOnFocus={t(I18N_TEXT.DAYS).toLowerCase()}
                  maxLength={3}
                  regex={NUMERIC_REPLACE_REGEX}
                  error={{
                    status: !isUndefined(errors.paymentScheduleFrequencyCount),
                    message: errors.paymentScheduleFrequencyCount as string
                  }}
                />
              )}
              <div className="col mt-16">
                <DatePicker
                  label={t(I18N_TEXT.START_DATE)}
                  value={getDateData(values.startDate)}
                  onChange={handleChange}
                  name="startDate"
                />
              </div>
            </div>
            <div className="row">
              <TextBoxControl
                className="col-md-6 col-lg-4 mt-16"
                label={t(I18N_TEXT.ASSETS)}
                name="assets"
                maxLength={30}
              />
              <TextBoxControl
                className="col-md-6 col-lg-4 mt-16"
                label={t(I18N_TEXT.COURT_DISTRICT_STATE)}
                name="courtDistrictState"
                maxLength={30}
              />
              <FormikNumericControl
                className="col-md-6 col-lg-4 mt-16"
                label={t(I18N_TEXT.BALANCE_DEATH)}
                name="balanceDeath"
                maxLength={15}
                format="c2"
              />
            </div>
            <div className="row">
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.SHORT_FIELD_1)}
                name="shortField1"
                maxLength={5}
              />
              <div className="col-md-6 col-lg-3 mt-16">
                <DatePicker
                  label={t(I18N_TEXT.DATE_FIELD_1)}
                  value={getDateData(values.dateField1)}
                  onChange={handleChange}
                  name="dateField1"
                />
              </div>
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.LONG_FIELD_1)}
                name="longField1"
                maxLength={20}
              />
              <FormikNumericControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.AMOUNT_1)}
                name="amount1"
                format="c2"
                maxLength={17}
              />
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.SHORT_FIELD_2)}
                name="shortField2"
                maxLength={5}
              />
              <div className="col-md-6 col-lg-3 mt-16">
                <DatePicker
                  label={t(I18N_TEXT.DATE_FIELD_2)}
                  value={getDateData(values.dateField2)}
                  onChange={handleChange}
                  name="dateField2"
                />
              </div>
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.LONG_FIELD_2)}
                name="longField2"
                maxLength={20}
              />
              <FormikNumericControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.AMOUNT_2)}
                name="amount2"
                format="c2"
                maxLength={17}
              />
            </div>
          </div>
        </SimpleBar>
      </div>
      <div className="w-100 border-top px-24 py-16 text-right">
        <Button
          variant="primary"
          onClick={submitForm}
          disabled={!isEmpty(errors)}
        >
          {t(I18N_TEXT.SAVE)}
        </Button>
      </div>
    </FormikProvider>
  );
};

export default AccountForm;
