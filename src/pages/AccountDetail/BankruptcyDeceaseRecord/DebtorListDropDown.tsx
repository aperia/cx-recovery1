import { I18N_TEXT } from 'app/constants';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { DropdownList, InlineMessage } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { mappingRelationValue } from 'pages/AccountManagement/ManageAssociatedDebtors/helpers';
import React, { useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { IDebtorItem } from './types';
import { bankruptcyDeceaseRecordActions } from './_redux';
import {
  selectDebtorList,
  selectSelectedAssociatedDebtor
} from './_redux/selectors';

const DebtorListDropDown: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const debtorList: IDebtorItem[] = useSelectorId(selectDebtorList);
  const selectedDebtor: IDebtorItem | undefined = useSelectorId(
    selectSelectedAssociatedDebtor
  );

  const handleSelectDebtor = (event: DropdownBaseChangeEvent) => {
    dispatch(
      bankruptcyDeceaseRecordActions.selectAssociatedDebtor({
        data: event.target.value,
        storeId
      })
    );
  };

  useEffect(() => {
    dispatch(bankruptcyDeceaseRecordActions.getDebtorList({ storeId }));
  }, [dispatch, storeId]);

  const renderDebtorItem = useCallback(
    (debtor: MagicKeyValue) => {
      return (
        <div className="d-flex align-items-center justify-content-between w-100">
          <div>
            {debtor.label && (
              <div className="pt-6">
                <h6 className="color-grey-l16">{debtor.label}</h6>
              </div>
            )}
            <div className="color-grey-d20">
              {debtor.debtorName} • {mappingRelationValue(debtor.relation)}
            </div>
            {debtor.last4SSN && (
              <div className="mt-6">
                {t(I18N_TEXT.LAST_4_SSN)}: {debtor.last4SSN}
              </div>
            )}
          </div>
          <div className="d-flex ">
            {debtor.isBankruptcy && (
              <InlineMessage
                variant="warning"
                className="px-8 py-2 fs-12 m-0 border-0 br-radius-4"
              >
                <span>{t(I18N_TEXT.BANKRUPTCY)}</span>
              </InlineMessage>
            )}
            {debtor.isDecease && (
              <InlineMessage
                variant="danger"
                className="px-8 py-2 fs-12 m-0 ml-4 border-0 br-radius-4"
              >
                <span>{t(I18N_TEXT.DECEASE)}</span>
              </InlineMessage>
            )}
          </div>
        </div>
      );
    },
    [t]
  );

  return (
    <DropdownList
      onChange={handleSelectDebtor}
      className="w-471px"
      textFieldRender={(item: IDebtorItem) => (
        <>
          {renderDebtorItem({
            ...item,
            last4SSN: '',
            label: t(I18N_TEXT.DEBTOR)
          })}
        </>
      )}
      value={selectedDebtor}
    >
      {debtorList.map(debtor => (
        <DropdownList.Item key={debtor.id} value={debtor} variant="custom">
          {renderDebtorItem(debtor)}
        </DropdownList.Item>
      ))}
    </DropdownList>
  );
};

export default DebtorListDropDown;
