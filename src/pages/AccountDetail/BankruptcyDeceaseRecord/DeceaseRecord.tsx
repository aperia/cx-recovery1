// components
import React, { useCallback, useEffect, useMemo } from 'react';
import { NoDataGrid } from 'app/components';
import SectionControl from 'app/components/SectionControl';
import { Button } from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';

// constants
import { I18N_TEXT } from 'app/constants';
import { DECEASE_VIEW_SECTIONS } from './constants';
import { IDebtorItem, IDeceaseData } from './types';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { isEmpty, isEqual, isUndefined } from 'app/helpers';
import classnames from 'classnames';

// redux
import { useDispatch } from 'react-redux';
import {
  selectDeceaseRecord,
  selectRecordLoading,
  selectSelectedAssociatedDebtor
} from './_redux/selectors';
import { bankruptcyDeceaseRecordActions } from './_redux';
import RecordDecease from './RecordDecease';
import { formatDeceaseAddressData } from './helpers';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { INITIAL_ADD_DECEASE_DATA } from './RecordDecease/constants';
import { useAccountDetail, useSelectorId } from 'app/hooks';

const DeceaseRecord: React.FC = () => {
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const dispatch = useDispatch();
  const isLoading: boolean = useSelectorId(selectRecordLoading);
  const data: IDeceaseData | undefined = useSelectorId(selectDeceaseRecord);
  const selectedDebtor: IDebtorItem | undefined = useSelectorId(
    selectSelectedAssociatedDebtor
  );
  const isEmptyData =
    isEmpty(data) || isUndefined(data) || !selectedDebtor?.isDecease;

  const toggleRecordDecease = useCallback(() => {
    dispatch(bankruptcyDeceaseRecordActions.toggleRecordDecease({ storeId }));
  }, [dispatch, storeId]);

  const handleRemoveDecease = useCallback(() => {
    dispatch(
      rootModalActions.open({
        title: I18N_TEXT.CONFIRM_REMOVE_DECEASE,
        body: I18N_TEXT.CONFIRM_REMOVE_DECEASE_MESSAGE,
        btnCancelText: I18N_TEXT.CANCEL,
        btnConfirmText: I18N_TEXT.REMOVE,
        onConfirm: () => {
          dispatch(
            bankruptcyDeceaseRecordActions.deleteDeceaseRecord({ storeId })
          );
        }
      })
    );
  }, [dispatch, storeId]);

  const handleEditDecease = useCallback(() => {
    dispatch(
      bankruptcyDeceaseRecordActions.toggleRecordDecease({ storeId, data })
    );
  }, [data, dispatch, storeId]);

  useEffect(() => {
    dispatch(
      bankruptcyDeceaseRecordActions.getDeceaseRecord({
        selectedDebtor: selectedDebtor,
        storeId
      })
    );
  }, [dispatch, selectedDebtor, storeId]);

  const RecordDataView = useMemo(() => {
    if (isUndefined(selectedDebtor)) return null;

    if (isEmptyData) {
      return (
        <div className="mt-n24">
          <NoDataGrid text={t(I18N_TEXT.NOT_IN_DECEASE_SITUATION)}>
            <Button
              className="text-capitalize mt-24"
              variant="outline-primary"
              size="sm"
              onClick={toggleRecordDecease}
            >
              {t(I18N_TEXT.RECORD_DECEASE)}
            </Button>
          </NoDataGrid>
        </div>
      );
    }

    const formatData = formatDeceaseAddressData(data!, selectedDebtor);

    return (
      <div className="mt-n4">
        {DECEASE_VIEW_SECTIONS.map(section => {
          if (section.id === 'account' && selectedDebtor.relation !== 'A')
            return null;
          const dataField = section.dataField as keyof IDeceaseData;
          const { id, title, viewId } = section;
          return (
            <div key={id} className="mt-24">
              <SectionControl
                title={t(title)}
                sectionKey={`section-${viewId}`}
                classNamesView="ml-24"
                isExpand={true}
                isTitleNormal={true}
              >
                {isEmpty(formatData![dataField]) ||
                isEqual(
                  formatData![dataField],
                  INITIAL_ADD_DECEASE_DATA[dataField]
                ) ? (
                  <p className="mt-16">{t(I18N_TEXT.NO_DATA_TO_DISPLAY)}</p>
                ) : (
                  <View
                    id={viewId}
                    formKey={viewId}
                    descriptor={viewId}
                    value={formatData![dataField]}
                  />
                )}
              </SectionControl>
            </div>
          );
        })}
      </div>
    );
  }, [data, isEmptyData, selectedDebtor, t, toggleRecordDecease]);

  return (
    <div className={classnames('py-24', { loading: isLoading })}>
      <div className="d-flex align-items-center">
        {!isEmptyData && (
          <>
            <h5>{t(I18N_TEXT.DECEASED_INFORMATION)}</h5>
            <span className="ml-auto">
              <Button
                size="sm"
                variant="outline-danger"
                onClick={handleRemoveDecease}
              >
                {t(I18N_TEXT.REMOVE_DECEASE)}
              </Button>
              <Button
                size="sm"
                className="mr-n8"
                variant="outline-primary"
                onClick={handleEditDecease}
              >
                {t(I18N_TEXT.EDIT)}
              </Button>
            </span>
          </>
        )}
      </div>
      {RecordDataView}
      <RecordDecease />
    </div>
  );
};

export default DeceaseRecord;
