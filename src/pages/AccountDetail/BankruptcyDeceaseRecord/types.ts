import { ILinkDebtor } from '../../AccountManagement/types';

export interface IInitBankruptcyDeceaseRecordState {
  [storeId: string]: IBankruptcyDeceaseRecordState;
}

export interface IBankruptcyDeceaseRecordState {
  debtorList?: IDebtorItem[];
  bankruptcyData?: IBankruptcyData;
  deceaseData?: IDeceaseData;
  loading?: boolean;
  recordLoading?: boolean;
  selectedAssociatedDebtor?: IDebtorItem;
  saveBankruptcyShow?: boolean;
  saveBankruptcyLoading?: boolean;
  recordDeceaseShow?: boolean;
  recordDeceaseLoading?: boolean;
  isDirtyForm?: boolean;
}

export interface IBankruptcyData {
  account: MagicKeyValue;
  debtorAddress: MagicKeyValue;
  debtorCase: MagicKeyValue;
}

export interface IDeceaseData {
  account: MagicKeyValue;
  debtorAddress: MagicKeyValue;
}

export interface IDebtorItem extends ILinkDebtor {}

export interface IGetDebtorListPayload {
  data: IDebtorItem[];
}

export interface IGetDebtorListArgs {
  storeId: string;
}

export interface IGetBankruptcyRecordPayload {
  data: IBankruptcyData;
}

export interface IGetBankruptcyRecordArgs {
  selectedDebtor?: IDebtorItem;
  storeId: string;
}

export interface IGetDeceaseRecordPayload {
  data: IDeceaseData;
}

export interface IGetDeceaseRecordArgs {
  selectedDebtor?: IDebtorItem;
  storeId: string;
}

export interface IRecordBankruptcyData {
  account: MagicKeyValue;
  debtorAddress: MagicKeyValue;
  debtorCase: MagicKeyValue;
}

export interface IRecordDeceaseData {
  account: MagicKeyValue;
  debtorAddress: MagicKeyValue;
}

export interface ISaveBankruptcyRecordPayload {
  data: MagicKeyValue;
}

export interface ISaveBankruptcyRecordArgs {
  data: MagicKeyValue;
  storeId: string;
}

export interface ISaveDeceaseRecordPayload {
  data: MagicKeyValue;
}

export interface ISaveDeceaseRecordArgs {
  data: MagicKeyValue;
  storeId: string;
}

export interface IAddressData {
  addressLine1?: string;
  addressLine2?: string;
  city?: string;
  state?: string;
  zipCode?: string;
}

export interface IDeleteBankruptcyArgs {
  storeId: string;
}

export interface IDeleteDeceaseArgs {
  storeId: string;
}
