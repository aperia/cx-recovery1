import bankruptcyRecord from './bankruptcyRecord.json';
import deceaseRecord from './deceaseRecord.json';

let bankruptcyRecordData = JSON.parse(JSON.stringify(bankruptcyRecord));
let deceaseRecordData = JSON.parse(JSON.stringify(deceaseRecord));

export const bankruptcyDeceaseController = {
  getBankruptcyByDebtorId(id: string, accountId: string) {
    return Promise.resolve({
      success: true,
      data: bankruptcyRecordData.find(
        (item: MagicKeyValue) =>
          item.debtorId === id && item.accountId === accountId
      )
    });
  },
  getDeceaseByDebtorId(id: string, accountId: string) {
    return Promise.resolve({
      success: true,
      data: deceaseRecordData.find(
        (item: MagicKeyValue) =>
          item.debtorId === id && item.accountId === accountId
      )
    });
  },
  saveBankruptcyRecord({
    debtorId,
    accountId,
    item
  }: {
    debtorId: string;
    accountId: string;
    item: MagicKeyValue;
  }) {
    try {
      const existIndex = bankruptcyRecordData.findIndex(
        (item: MagicKeyValue) =>
          item.debtorId === debtorId && item.accountId === accountId
      );
      if (existIndex >= 0) {
        bankruptcyRecordData[existIndex] = {
          ...bankruptcyRecordData[existIndex],
          ...item
        };
      } else {
        bankruptcyRecordData = [
          ...bankruptcyRecordData,
          {
            debtorId,
            ...item,
            accountId
          }
        ];
      }

      return Promise.resolve({
        success: true
      });
    } catch (error) {
      return Promise.reject(error);
    }
  },
  saveDeceaseRecord({
    debtorId,
    accountId,
    item
  }: {
    debtorId: string;
    accountId: string;
    item: MagicKeyValue;
  }) {
    try {
      const existIndex = deceaseRecordData.findIndex(
        (item: MagicKeyValue) =>
          item.debtorId === debtorId && item.accountId === accountId
      );
      if (existIndex >= 0) {
        deceaseRecordData[existIndex] = {
          ...deceaseRecordData[existIndex],
          ...item
        };
      } else {
        deceaseRecordData = [
          ...deceaseRecordData,
          {
            debtorId,
            ...item,
            accountId
          }
        ];
      }

      return Promise.resolve({
        success: true
      });
    } catch (error) {
      return Promise.reject(error);
    }
  },
  deleteBankruptcyRecord({
    debtorId,
    accountId
  }: {
    debtorId: string;
    accountId: string;
  }) {
    try {
      const existIndex = bankruptcyRecordData.findIndex(
        (item: MagicKeyValue) =>
          item.debtorId === debtorId && item.accountId === accountId
      );

      if (existIndex === -1) throw { message: 'Index not found' };

      bankruptcyRecordData = (bankruptcyRecordData as MagicKeyValue[]).filter(
        (_, index) => index !== existIndex
      );

      return Promise.resolve({
        success: true
      });
    } catch (error) {
      return Promise.reject(error);
    }
  },
  deleteDeceaseRecord({
    debtorId,
    accountId
  }: {
    debtorId: string;
    accountId: string;
  }) {
    try {
      const existIndex = deceaseRecordData.findIndex(
        (item: MagicKeyValue) =>
          item.debtorId === debtorId && item.accountId === accountId
      );

      if (existIndex === -1) throw { message: 'Index not found' };

      deceaseRecordData = (deceaseRecordData as MagicKeyValue[]).filter(
        (_, index) => index !== existIndex
      );

      return Promise.resolve({
        success: true
      });
    } catch (error) {
      return Promise.reject(error);
    }
  }
};
