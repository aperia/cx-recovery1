import { useFormik } from 'formik';
import { isEmpty, isUndefined } from 'app/helpers';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import { useDispatch } from 'react-redux';
import { IDebtorItem, IDeceaseData } from '../types';
import {
  selectDeceaseRecord,
  selectSelectedAssociatedDebtor
} from '../_redux/selectors';
import { ACCOUNT_VALIDATION_SCHEMA } from '../RecordBankruptcy/constants';
import { useCallback, useEffect, useMemo } from 'react';
import { bankruptcyDeceaseRecordActions } from '../_redux';
import { formatDateData } from '../RecordBankruptcy/helpers';
import { INITIAL_ADD_DECEASE_DATA } from '../RecordDecease/constants';
import { useAccountDetail, useSelectorId } from 'app/hooks';

export const useRecordDeceaseForm = (ky: keyof IDeceaseData) => {
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const recordDeceaseData: IDeceaseData | undefined =
    useSelectorId(selectDeceaseRecord);
  const associatedDebtor: IDebtorItem | undefined = useSelectorId(
    selectSelectedAssociatedDebtor
  );
  const { data: stateData } = useGetRefDataQuery('state');
  const { data: paymentScheduleFrequencyData } = useGetRefDataQuery(
    'bankruptcyDeceasePaymentScheduleFrequency'
  );

  const validationSchema = useMemo(() => {
    if (ky === 'account') return ACCOUNT_VALIDATION_SCHEMA;
    return;
  }, [ky]);

  const handleChangeComboBox = (e: DropdownBaseChangeEvent) => {
    const event = {
      ...e,
      target: {
        ...e.target,
        value: e.target.value?.value
      }
    };

    handleChange(event);
  };

  const handleSubmit = useCallback(
    (values: MagicKeyValue) => {
      dispatch(
        bankruptcyDeceaseRecordActions.saveDeceaseRecord({
          data: {
            [ky]: {
              ...formatDateData(ky, values)
            }
          },
          storeId
        })
      );
    },
    [dispatch, ky, storeId]
  );

  const initialValues = useMemo(() => {
    const formData =
      isUndefined(recordDeceaseData) || isEmpty(recordDeceaseData[ky])
        ? INITIAL_ADD_DECEASE_DATA[ky]
        : recordDeceaseData[ky];
    if (ky === 'debtorAddress') {
      return {
        ...formData,
        deceasedDebtorName: associatedDebtor?.debtorName
      };
    }
    return formData;
  }, [associatedDebtor?.debtorName, ky, recordDeceaseData]);

  const formik = useFormik({
    initialValues,
    enableReinitialize: true,
    onSubmit: handleSubmit,
    validationSchema: validationSchema
  });

  const { dirty, setValues, setTouched, handleChange } = formik;

  const handleClearAll = useCallback(() => {
    setTouched({}, false);
    setValues(
      ky === 'debtorAddress'
        ? {
            ...INITIAL_ADD_DECEASE_DATA[ky],
            deceasedDebtorName: associatedDebtor?.debtorName
          }
        : { ...INITIAL_ADD_DECEASE_DATA[ky] }
    );
  }, [associatedDebtor?.debtorName, ky, setTouched, setValues]);

  useEffect(() => {
    dispatch(
      bankruptcyDeceaseRecordActions.setDirtyForm({
        isDirty: dirty,
        storeId
      })
    );
  }, [dirty, dispatch, storeId]);

  return {
    stateData,
    paymentScheduleFrequencyData,
    handleChangeComboBox,
    handleClearAll,
    formik
  };
};
