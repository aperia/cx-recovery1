import { I18N_TEXT } from 'app/constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import React from 'react';
import DebtorList from './DebtorList';
import DebtorListDropDown from './DebtorListDropDown';
import RecordTab from './RecordTab';
import { selectDebtorListLoading } from './_redux/selectors';
import classnames from 'classnames';
import { useSelectorId } from 'app/hooks';

const BankruptcyDeceaseRecord: React.FC = () => {
  const { t } = useTranslation();
  const loading = useSelectorId(selectDebtorListLoading);

  return (
    <div className={classnames('h-100', { loading })}>
      <h4 className="p-24 max-width-lg mx-auto">
        {t(I18N_TEXT.BANKRUPTCY_DECEASE_RECORD)}
      </h4>
      <div className="d-block d-lg-none pl-24 mb-24">
        <DebtorListDropDown />
      </div>
      <div className="d-flex max-width-lg mx-auto">
        <div className="w-408px d-none d-lg-block">
          <DebtorList />
        </div>
        <div className="flex-1">
          <RecordTab />
        </div>
      </div>
    </div>
  );
};

export default BankruptcyDeceaseRecord;
