import { cloneDeep, isValidDate } from 'app/helpers';
import {
  IAddressData,
  IBankruptcyData,
  IDebtorItem,
  IDeceaseData
} from './types';

export const formatAddress = ({
  addressLine1,
  addressLine2,
  city,
  state,
  zipCode
}: IAddressData) => {
  let result = '';
  addressLine1 && (result += addressLine1);
  addressLine2 && (result += ` ${addressLine2}`);
  city && (result += `\n${city}`);
  state && (result += `, ${state}`);
  zipCode && (result += ` ${zipCode}`);
  return result;
};

export const getDateData = (dateStr: string) =>
  isValidDate(dateStr) ? new Date(dateStr) : undefined;

export const formatBankruptcyAddressData = (
  data: IBankruptcyData,
  debtor: IDebtorItem
) => {
  const formatData = cloneDeep(data);
  for (const key in formatData) {
    if (!Object.prototype.hasOwnProperty.call(formatData, key)) continue;
    if (key === 'account') {
      formatData[key] = {
        ...formatData[key],
        address: formatAddress(formatData[key]),
        paymentScheduleFrequency:
          formatData[key].paymentScheduleFrequency === 'custom'
            ? `${formatData[key].paymentScheduleFrequencyCount} ${
                formatData[key].paymentScheduleFrequencyCount > 1
                  ? 'days'
                  : 'day'
              }`
            : formatData[key].paymentScheduleFrequency
      };
    } else if (key === 'debtorAddress') {
      const prefixKey = ['debtor', 'trustee', 'attorney'];
      prefixKey.forEach(item => {
        formatData[key] = {
          ...formatData[key],
          [`${item}Address`]: formatAddress({
            addressLine1: formatData[key][`${item}AddressLine1`],
            addressLine2: formatData[key][`${item}AddressLine2`],
            city: formatData[key][`${item}City`],
            state: formatData[key][`${item}State`],
            zipCode: formatData[key][`${item}ZipCode`]
          }),
          debtorName: debtor.debtorName
        };
      });
    }
  }
  return formatData;
};

export const formatDeceaseAddressData = (
  data: IDeceaseData,
  debtor: IDebtorItem
) => {
  const formatData = cloneDeep(data);
  for (const key in formatData) {
    if (!Object.prototype.hasOwnProperty.call(formatData, key)) continue;
    if (key === 'account') {
      const prefixKey = ['executor', 'attorney'];
      prefixKey.forEach(item => {
        formatData[key] = {
          ...formatData[key],
          [`${item}Address`]: formatAddress({
            addressLine1: formatData[key][`${item}AddressLine1`],
            addressLine2: formatData[key][`${item}AddressLine2`],
            city: formatData[key][`${item}City`],
            state: formatData[key][`${item}State`],
            zipCode: formatData[key][`${item}ZipCode`]
          })
        };
      });
      formatData[key] = {
        ...formatData[key],
        paymentScheduleFrequency:
          formatData[key].paymentScheduleFrequency === 'custom'
            ? `${formatData[key].paymentScheduleFrequencyCount} ${
                formatData[key].paymentScheduleFrequencyCount > 1
                  ? 'days'
                  : 'day'
              }`
            : formatData[key].paymentScheduleFrequency
      };
    } else if (key === 'debtorAddress') {
      const prefixKey = ['deceasedDebtor', 'court'];
      prefixKey.forEach(item => {
        formatData[key] = {
          ...formatData[key],
          [`${item}Address`]: formatAddress({
            addressLine1: formatData[key][`${item}AddressLine1`],
            addressLine2: formatData[key][`${item}AddressLine2`],
            city: formatData[key][`${item}City`],
            state: formatData[key][`${item}State`],
            zipCode: formatData[key][`${item}ZipCode`]
          }),
          deceasedDebtorName: debtor.debtorName
        };
      });
    }
  }
  return formatData;
};
