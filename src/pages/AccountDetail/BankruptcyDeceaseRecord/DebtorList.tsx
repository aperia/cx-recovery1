import { I18N_TEXT } from 'app/constants';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { Button, InlineMessage } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import classnames from 'classnames';
import React, { useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { mappingRelationValue } from '../../AccountManagement/ManageAssociatedDebtors/helpers';
import { IDebtorItem } from './types';
import { bankruptcyDeceaseRecordActions } from './_redux';
import {
  selectDebtorList,
  selectSelectedAssociatedDebtor
} from './_redux/selectors';

const DebtorList: React.FC = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const { storeId } = useAccountDetail();
  const debtorList: IDebtorItem[] = useSelectorId(selectDebtorList);
  const selectedDebtor: IDebtorItem | undefined = useSelectorId(
    selectSelectedAssociatedDebtor
  );

  const handleSelectDebtor = (data: IDebtorItem) => {
    dispatch(
      bankruptcyDeceaseRecordActions.selectAssociatedDebtor({ storeId, data })
    );
  };

  useEffect(() => {
    dispatch(bankruptcyDeceaseRecordActions.getDebtorList({ storeId }));
  }, [dispatch, storeId]);

  return (
    <div className="px-24 pb-24 pt-16">
      <h5>{t(I18N_TEXT.DEBTOR_LIST)}</h5>
      <div className="mt-8">
        {debtorList.map((item: IDebtorItem) => {
          const isSelected = item.id === selectedDebtor?.id;

          return (
            <div key={item.id} className="mt-16">
              <Button
                variant={'secondary'}
                className={classnames(
                  'text-left p-16 h-auto w-100 cursor-pointer br-radius-8',
                  {
                    'bg-blue-l40': isSelected,
                    'br-blue': isSelected
                  }
                )}
                onClick={() => {
                  handleSelectDebtor(item);
                }}
              >
                <div className="d-flex align-items-center">
                  <b className="color-grey-d20">{item.debtorName}</b>
                  <div className="ml-auto d-flex">
                    {item.isBankruptcy && (
                      <InlineMessage
                        variant="warning"
                        className="px-8 py-2 fs-12 m-0 border-0 br-radius-4 fw-400"
                      >
                        <span>{t(I18N_TEXT.BANKRUPTCY)}</span>
                      </InlineMessage>
                    )}
                    {item.isDecease && (
                      <InlineMessage
                        variant="danger"
                        className="px-8 py-2 fs-12 m-0 ml-4 border-0 br-radius-4 fw-400"
                      >
                        <span>{t(I18N_TEXT.DECEASE)}</span>
                      </InlineMessage>
                    )}
                  </div>
                </div>
                <div className="d-flex mt-8 fw-400">
                  <span className="color-grey">{t(I18N_TEXT.LAST_4_SSN)}:</span>
                  <span className="color-grey-d20 ml-4">{item.last4SSN}</span>
                  <span className="ml-auto color-grey">
                    {t(mappingRelationValue(item.relation))}
                  </span>
                </div>
              </Button>
            </div>
          );
        })}
      </div>
    </div>
  );
};

export default DebtorList;
