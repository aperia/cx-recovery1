// components
import React, { useCallback, useEffect, useMemo } from 'react';
import { NoDataGrid } from 'app/components';
import { Button, InlineMessage } from 'app/_libraries/_dls/components';
import { View } from 'app/_libraries/_dof/core';
import SectionControl from 'app/components/SectionControl';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { BANKRUPTCY_VIEW_SECTIONS } from './constants';
import { I18N_TEXT } from 'app/constants/i18nText';
import { IBankruptcyData, IDebtorItem } from './types';

// redux
import { useDispatch } from 'react-redux';
import {
  selectBankruptcyRecord,
  selectRecordLoading,
  selectSelectedAssociatedDebtor
} from './_redux/selectors';

// helpers
import classnames from 'classnames';
import { isEmpty, isEqual, isUndefined } from 'app/helpers';
import { bankruptcyDeceaseRecordActions } from './_redux';
import RecordBankruptcy from './RecordBankruptcy';
import { formatBankruptcyAddressData } from './helpers';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { INITIAL_ADD_BANKRUPTCY_DATA } from './RecordBankruptcy/constants';
import { useAccountDetail, useSelectorId } from 'app/hooks';

const BankruptcyRecord: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const isLoading: boolean = useSelectorId(selectRecordLoading);
  const data: IBankruptcyData | undefined = useSelectorId(
    selectBankruptcyRecord
  );
  const selectedDebtor: IDebtorItem | undefined = useSelectorId(
    selectSelectedAssociatedDebtor
  );
  const isEmptyData =
    isEmpty(data) || isUndefined(data) || !selectedDebtor?.isBankruptcy;

  const toggleRecordBankruptcy = useCallback(() => {
    dispatch(
      bankruptcyDeceaseRecordActions.toggleRecordBankruptcy({ storeId })
    );
  }, [dispatch, storeId]);

  const handleRemoveBankruptcy = useCallback(() => {
    dispatch(
      rootModalActions.open({
        title: I18N_TEXT.CONFIRM_REMOVE_BANKRUPTCY,
        body: I18N_TEXT.CONFIRM_REMOVE_BANKRUPTCY_MESSAGE,
        btnCancelText: I18N_TEXT.CANCEL,
        btnConfirmText: I18N_TEXT.REMOVE,
        onConfirm: () => {
          dispatch(
            bankruptcyDeceaseRecordActions.deleteBankruptcyRecord({ storeId })
          );
        }
      })
    );
  }, [dispatch, storeId]);

  const handleEditBankruptcy = useCallback(() => {
    dispatch(
      bankruptcyDeceaseRecordActions.toggleRecordBankruptcy({
        data,
        storeId
      })
    );
  }, [data, dispatch, storeId]);

  useEffect(() => {
    dispatch(
      bankruptcyDeceaseRecordActions.getBankruptcyRecord({
        selectedDebtor: selectedDebtor,
        storeId
      })
    );
  }, [dispatch, selectedDebtor, storeId]);

  const RecordDataView = useMemo(() => {
    if (isUndefined(selectedDebtor)) return null;

    if (isEmptyData) {
      if (selectedDebtor.isDecease)
        return (
          <InlineMessage variant="warning">
            {t(I18N_TEXT.DECEASE_INFO_HAS_BEEN_PROVIDED)}
          </InlineMessage>
        );
      return (
        <div className="mt-n24">
          <NoDataGrid text={t(I18N_TEXT.NOT_IN_BANKRUPTCY_SITUATION)}>
            <Button
              className="text-capitalize mt-24"
              variant="outline-primary"
              size="sm"
              onClick={toggleRecordBankruptcy}
            >
              {t(I18N_TEXT.RECORD_BANKRUPTCY)}
            </Button>
          </NoDataGrid>
        </div>
      );
    }

    const formatData = formatBankruptcyAddressData(data!, selectedDebtor);

    return (
      <div className="mt-n4">
        {BANKRUPTCY_VIEW_SECTIONS.map(section => {
          if (section.id === 'account' && selectedDebtor.relation !== 'A')
            return null;
          const dataField = section.dataField as keyof IBankruptcyData;
          const { id, title, viewId } = section;
          return (
            <div key={id} className="mt-24">
              <SectionControl
                title={t(title)}
                sectionKey={`section-${viewId}`}
                classNamesView="ml-24"
                isExpand={true}
                isTitleNormal={true}
              >
                {isEmpty(formatData![dataField]) ||
                isEqual(
                  formatData![dataField],
                  INITIAL_ADD_BANKRUPTCY_DATA[dataField]
                ) ? (
                  <p className="mt-16">{t(I18N_TEXT.NO_DATA_TO_DISPLAY)}</p>
                ) : (
                  <View
                    id={viewId}
                    formKey={viewId}
                    descriptor={viewId}
                    value={formatData![dataField]}
                  />
                )}
              </SectionControl>
            </div>
          );
        })}
      </div>
    );
  }, [data, isEmptyData, selectedDebtor, t, toggleRecordBankruptcy]);

  return (
    <div className={classnames('py-24', { loading: isLoading })}>
      <div className="d-flex align-items-center">
        {!isEmptyData && (
          <>
            <h5>{t(I18N_TEXT.BANKRUPTCY_INFORMATION)}</h5>
            <span className="ml-auto">
              <Button
                size="sm"
                variant="outline-danger"
                onClick={handleRemoveBankruptcy}
              >
                {t(I18N_TEXT.REMOVE_BANKRUPTCY)}
              </Button>
              <Button
                size="sm"
                className="mr-n8"
                variant="outline-primary"
                onClick={handleEditBankruptcy}
              >
                {t(I18N_TEXT.EDIT)}
              </Button>
            </span>
          </>
        )}
      </div>
      {RecordDataView}
      <RecordBankruptcy />
    </div>
  );
};

export default BankruptcyRecord;
