import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import {
  IGetBankruptcyRecordArgs,
  IGetBankruptcyRecordPayload,
  IBankruptcyData,
  IInitBankruptcyDeceaseRecordState
} from '../types';
import { GET_BANKRUPTCY_RECORD } from '../constants';
import { bankruptcyDeceaseController } from '../__mock__/bankruptcyDeceaseController';
import { set } from 'lodash';

export const getBankruptcyRecord = createAsyncThunk<
  IGetBankruptcyRecordPayload,
  IGetBankruptcyRecordArgs,
  ThunkAPIConfig
>(GET_BANKRUPTCY_RECORD, async (args, { rejectWithValue }) => {
  const { selectedDebtor, storeId } = args;
  if (!selectedDebtor) return rejectWithValue({});
  const { data } = await bankruptcyDeceaseController.getBankruptcyByDebtorId(
    selectedDebtor?.id || '',
    storeId
  );
  return {
    data: data as IBankruptcyData
  };
});

export const getBankruptcyRecordBuilder = (
  builder: ActionReducerMapBuilder<IInitBankruptcyDeceaseRecordState>
) => {
  const { pending, fulfilled, rejected } = getBankruptcyRecord;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.recordLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.recordLoading`, false);
      set(draftState, `${storeId}.bankruptcyData`, action.payload.data);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.recordLoading`, false);
    });
};
