import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import {
  GET_DEBTOR_LIST,
  initialBankruptcyDeceaseRecordState
} from '../constants';
import { INITIAL_ADD_BANKRUPTCY_DATA } from '../RecordBankruptcy/constants';
import { IBankruptcyData, IDebtorItem, IDeceaseData } from '../types';
import { getDebtorList, getDebtorListBuilder } from './getDebtorList';
import {
  getBankruptcyRecord,
  getBankruptcyRecordBuilder
} from './getBankruptcyRecord';
import { getDeceaseRecord, getDeceaseRecordBuilder } from './getDeceaseRecord';
import {
  saveBankruptcyRecord,
  saveBankruptcyRecordBuilder
} from './saveBankruptcyRecord';
import {
  deleteBankruptcyRecord,
  deleteBankruptcyRecordBuilder
} from './deleteBankruptcyRecord';
import {
  saveDeceaseRecord,
  saveDeceaseRecordBuilder
} from './saveDeceaseRecord';
import {
  deleteDeceaseRecord,
  deleteDeceaseRecordBuilder
} from './deleteDeceaseRecord';
import { INITIAL_ADD_DECEASE_DATA } from '../RecordDecease/constants';
import { set } from 'lodash';

const { actions, reducer } = createSlice({
  name: GET_DEBTOR_LIST,
  initialState: initialBankruptcyDeceaseRecordState,
  reducers: {
    selectAssociatedDebtor: (
      draftState,
      action: PayloadAction<{ data: IDebtorItem; storeId: string }>
    ) => {
      const { storeId, data } = action.payload;
      set(draftState, `${storeId}.selectedAssociatedDebtor`, data);
    },
    toggleRecordBankruptcy: (
      draftState,
      action: PayloadAction<{
        data?: IBankruptcyData;
        storeId: string;
      }>
    ) => {
      const { storeId, data } = action.payload;
      if (!draftState[storeId]?.saveBankruptcyShow) {
        set(
          draftState,
          `${storeId}.bankruptcyData`,
          data || INITIAL_ADD_BANKRUPTCY_DATA
        );
      }
      set(
        draftState,
        `${storeId}.saveBankruptcyShow`,
        !draftState[storeId]?.saveBankruptcyShow
      );
      set(draftState, `${storeId}.isDirtyForm`, false);
    },
    toggleRecordDecease: (
      draftState,
      action: PayloadAction<{ data?: IDeceaseData; storeId: string }>
    ) => {
      const { storeId, data } = action.payload;
      if (!draftState[storeId]?.recordDeceaseShow) {
        set(
          draftState,
          `${storeId}.deceaseData`,
          data || INITIAL_ADD_DECEASE_DATA
        );
      }
      set(
        draftState,
        `${storeId}.recordDeceaseShow`,
        !draftState[storeId]?.recordDeceaseShow
      );
      set(draftState, `${storeId}.isDirtyForm`, false);
    },
    setDirtyForm: (
      draftState,
      action: PayloadAction<{ isDirty: boolean; storeId: string }>
    ) => {
      const { storeId, isDirty } = action.payload;
      set(draftState, `${storeId}.isDirtyForm`, isDirty);
    },
    removeStore: () => initialBankruptcyDeceaseRecordState
  },
  extraReducers: builder => {
    getDebtorListBuilder(builder);
    getBankruptcyRecordBuilder(builder);
    getDeceaseRecordBuilder(builder);
    saveBankruptcyRecordBuilder(builder);
    deleteBankruptcyRecordBuilder(builder);
    saveDeceaseRecordBuilder(builder);
    deleteDeceaseRecordBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getDebtorList,
  getBankruptcyRecord,
  getDeceaseRecord,
  saveBankruptcyRecord,
  deleteBankruptcyRecord,
  saveDeceaseRecord,
  deleteDeceaseRecord
};

export { combineActions as bankruptcyDeceaseRecordActions, reducer };
