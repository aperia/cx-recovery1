import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { bankruptcyDeceaseController } from '../__mock__/bankruptcyDeceaseController';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { bankruptcyDeceaseRecordActions } from '.';
import { RECORD_DECEASE_TABS, SAVE_DECEASE_RECORD } from '../constants';
import {
  IInitBankruptcyDeceaseRecordState,
  ISaveDeceaseRecordArgs,
  ISaveDeceaseRecordPayload
} from '../types';
import { set } from 'lodash';

export const saveDeceaseRecord = createAsyncThunk<
  ISaveDeceaseRecordPayload,
  ISaveDeceaseRecordArgs,
  ThunkAPIConfig
>(
  SAVE_DECEASE_RECORD,
  async (args, { dispatch, rejectWithValue, getState }) => {
    try {
      const { data, storeId } = args;
      const { selectedAssociatedDebtor } =
        getState().bankruptcyDeceaseRecord[storeId];
      await bankruptcyDeceaseController.saveDeceaseRecord({
        debtorId: selectedAssociatedDebtor?.id || '',
        item: data,
        accountId: storeId
      });

      batch(() => {
        dispatch(
          bankruptcyDeceaseRecordActions.getDeceaseRecord({
            selectedDebtor: selectedAssociatedDebtor,
            storeId
          })
        );
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message:
              RECORD_DECEASE_TABS.find(item => item.id === Object.keys(data)[0])
                ?.saveSuccessMessage || ''
          })
        );
      });

      return {
        data: data
      };
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const saveDeceaseRecordBuilder = (
  builder: ActionReducerMapBuilder<IInitBankruptcyDeceaseRecordState>
) => {
  const { pending, fulfilled, rejected } = saveDeceaseRecord;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.recordDeceaseLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { debtorList, selectedAssociatedDebtor } = draftState[storeId];
      set(draftState, `${storeId}.recordDeceaseLoading`, false);
      set(draftState, `${storeId}.isDirtyForm`, false);
      set(
        draftState,
        `${storeId}.debtorList`,
        debtorList?.map(debtor => {
          if (debtor.id === selectedAssociatedDebtor?.id) {
            return {
              ...debtor,
              isDecease: true
            };
          }
          return debtor;
        })
      );
      set(draftState, `${storeId}.selectedAssociatedDebtor`, {
        ...selectedAssociatedDebtor,
        isDecease: true
      });
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.recordDeceaseLoading`, false);
    });
};
