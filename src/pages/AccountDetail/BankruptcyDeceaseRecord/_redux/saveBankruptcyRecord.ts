import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { bankruptcyDeceaseController } from '../__mock__/bankruptcyDeceaseController';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { bankruptcyDeceaseRecordActions } from '.';
import { RECORD_BANKRUPTCY_TABS, SAVE_BANKRUPTCY_RECORD } from '../constants';
import {
  IInitBankruptcyDeceaseRecordState,
  ISaveBankruptcyRecordArgs
} from '../types';
import { set } from 'lodash';

export const saveBankruptcyRecord = createAsyncThunk<
  unknown,
  ISaveBankruptcyRecordArgs,
  ThunkAPIConfig
>(
  SAVE_BANKRUPTCY_RECORD,
  async (args, { dispatch, rejectWithValue, getState }) => {
    try {
      const { storeId, data } = args;
      const { selectedAssociatedDebtor } =
        getState().bankruptcyDeceaseRecord[storeId];
      await bankruptcyDeceaseController.saveBankruptcyRecord({
        debtorId: selectedAssociatedDebtor?.id || '',
        item: data,
        accountId: storeId
      });

      return batch(() => {
        dispatch(
          bankruptcyDeceaseRecordActions.getBankruptcyRecord({
            selectedDebtor: selectedAssociatedDebtor,
            storeId
          })
        );
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message:
              RECORD_BANKRUPTCY_TABS.find(
                item => item.id === Object.keys(data)[0]
              )?.saveSuccessMessage || ''
          })
        );
      });
    } catch (error) {
      return rejectWithValue(error);
    }
  }
);

export const saveBankruptcyRecordBuilder = (
  builder: ActionReducerMapBuilder<IInitBankruptcyDeceaseRecordState>
) => {
  const { pending, fulfilled, rejected } = saveBankruptcyRecord;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.saveBankruptcyLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { debtorList, selectedAssociatedDebtor } = draftState[storeId];
      set(draftState, `${storeId}.saveBankruptcyLoading`, false);
      set(draftState, `${storeId}.isDirtyForm`, false);
      set(
        draftState,
        `${storeId}.debtorList`,
        debtorList?.map(debtor => {
          if (debtor.id === selectedAssociatedDebtor?.id) {
            return {
              ...debtor,
              isBankruptcy: true
            };
          }
          return debtor;
        })
      );
      set(draftState, `${storeId}.selectedAssociatedDebtor`, {
        ...selectedAssociatedDebtor,
        isBankruptcy: true
      });
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.saveBankruptcyLoading`, false);
    });
};
