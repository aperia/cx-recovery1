import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import {
  IGetDeceaseRecordArgs,
  IGetDeceaseRecordPayload,
  IInitBankruptcyDeceaseRecordState
} from '../types';
import { GET_DECEASE_RECORD } from '../constants';
import { bankruptcyDeceaseController } from '../__mock__/bankruptcyDeceaseController';
import { set } from 'lodash';

export const getDeceaseRecord = createAsyncThunk<
  IGetDeceaseRecordPayload,
  IGetDeceaseRecordArgs,
  ThunkAPIConfig
>(GET_DECEASE_RECORD, async (args, { rejectWithValue }) => {
  if (!args.selectedDebtor) return rejectWithValue({});
  const { selectedDebtor, storeId } = args;
  const { data } = await bankruptcyDeceaseController.getDeceaseByDebtorId(
    selectedDebtor?.id || '',
    storeId
  );
  return {
    data: data
  };
});

export const getDeceaseRecordBuilder = (
  builder: ActionReducerMapBuilder<IInitBankruptcyDeceaseRecordState>
) => {
  const { pending, fulfilled, rejected } = getDeceaseRecord;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.recordLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.recordLoading`, false);
      set(draftState, `${storeId}.deceaseData`, action.payload.data);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.recordLoading`, false);
    });
};
