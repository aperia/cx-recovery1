import { createSelector } from 'reselect';
import { IBankruptcyDeceaseRecordState } from '../types';

const bankruptcyDeceaseRecordState = (states: RootState, storeId: string) =>
  states.bankruptcyDeceaseRecord[storeId] || {};

export const selectSelectedAssociatedDebtor = createSelector(
  bankruptcyDeceaseRecordState,
  (data: IBankruptcyDeceaseRecordState) => data.selectedAssociatedDebtor
);

export const selectDebtorList = createSelector(
  bankruptcyDeceaseRecordState,
  (data: IBankruptcyDeceaseRecordState) => data.debtorList || []
);

export const selectDebtorListLoading = createSelector(
  bankruptcyDeceaseRecordState,
  (data: IBankruptcyDeceaseRecordState) => data.loading || false
);

export const selectBankruptcyRecord = createSelector(
  bankruptcyDeceaseRecordState,
  (data: IBankruptcyDeceaseRecordState) => data.bankruptcyData
);

export const selectDeceaseRecord = createSelector(
  bankruptcyDeceaseRecordState,
  (data: IBankruptcyDeceaseRecordState) => data.deceaseData
);

export const selectRecordLoading = createSelector(
  bankruptcyDeceaseRecordState,
  (data: IBankruptcyDeceaseRecordState) => data.recordLoading || false
);

export const selectRecordBankruptcyLoading = createSelector(
  bankruptcyDeceaseRecordState,
  (data: IBankruptcyDeceaseRecordState) => data.saveBankruptcyLoading || false
);

export const selectRecordBankruptcyShow = createSelector(
  bankruptcyDeceaseRecordState,
  (data: IBankruptcyDeceaseRecordState) => data.saveBankruptcyShow || false
);

export const selectRecordDeceaseLoading = createSelector(
  bankruptcyDeceaseRecordState,
  (data: IBankruptcyDeceaseRecordState) => data.recordDeceaseLoading || false
);

export const selectRecordDeceaseShow = createSelector(
  bankruptcyDeceaseRecordState,
  (data: IBankruptcyDeceaseRecordState) => data.recordDeceaseShow || false
);

export const selectIsDirtyForm = createSelector(
  bankruptcyDeceaseRecordState,
  (data: IBankruptcyDeceaseRecordState) => data.isDirtyForm || false
);
