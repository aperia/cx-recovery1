import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { I18N_TEXT } from 'app/constants';
import { set } from 'lodash';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { bankruptcyDeceaseRecordActions } from '.';
import { DELETE_BANKRUPTCY_RECORD } from '../constants';
import {
  IDeleteBankruptcyArgs,
  IInitBankruptcyDeceaseRecordState
} from '../types';
import { bankruptcyDeceaseController } from '../__mock__/bankruptcyDeceaseController';

export const deleteBankruptcyRecord = createAsyncThunk<
  unknown,
  IDeleteBankruptcyArgs,
  ThunkAPIConfig
>(
  DELETE_BANKRUPTCY_RECORD,
  async (args, { getState, dispatch, rejectWithValue }) => {
    try {
      const { storeId } = args;
      const { selectedAssociatedDebtor } =
        getState().bankruptcyDeceaseRecord[storeId];

      if (!selectedAssociatedDebtor) throw { message: 'Debtor not found' };

      await bankruptcyDeceaseController.deleteBankruptcyRecord({
        debtorId: selectedAssociatedDebtor!.id!,
        accountId: storeId
      });

      return batch(() => {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'success',
            message: I18N_TEXT.BANKRUPTCY_REMOVED
          })
        );
        dispatch(
          bankruptcyDeceaseRecordActions.getBankruptcyRecord({
            selectedDebtor: selectedAssociatedDebtor,
            storeId
          })
        );
      });
    } catch (error) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_TEXT.BANKRUPTCY_FAILED_TO_REMOVE
        })
      );
      return rejectWithValue(error);
    }
  }
);

export const deleteBankruptcyRecordBuilder = (
  builder: ActionReducerMapBuilder<IInitBankruptcyDeceaseRecordState>
) => {
  const { pending, fulfilled, rejected } = deleteBankruptcyRecord;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.loading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { debtorList, selectedAssociatedDebtor } = draftState[storeId];
      set(draftState, `${storeId}.loading`, false);
      set(
        draftState,
        `${storeId}.debtorList`,
        debtorList?.map(debtor => {
          if (debtor.id === selectedAssociatedDebtor?.id) {
            return {
              ...debtor,
              isBankruptcy: false
            };
          }
          return debtor;
        })
      );
      set(draftState, `${storeId}.selectedAssociatedDebtor`, {
        ...selectedAssociatedDebtor,
        isBankruptcy: false
      });
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.loading`, false);
    });
};
