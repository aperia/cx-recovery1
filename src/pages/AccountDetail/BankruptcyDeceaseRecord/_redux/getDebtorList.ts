import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { set } from 'lodash';
import bankruptcyDeceaseRecordServices from '../bankruptcyDeceaseRecordServices';
import { GET_DEBTOR_LIST } from '../constants';
import {
  IGetDebtorListArgs,
  IGetDebtorListPayload,
  IInitBankruptcyDeceaseRecordState
} from '../types';

export const getDebtorList = createAsyncThunk<
  IGetDebtorListPayload,
  IGetDebtorListArgs,
  ThunkAPIConfig
>(GET_DEBTOR_LIST, async _ => {
  const { data } = await bankruptcyDeceaseRecordServices.getDebtorList({
    page: 1,
    pageSize: 10
  });
  return {
    data: data.associatedsDebtors
  };
});

export const getDebtorListBuilder = (
  builder: ActionReducerMapBuilder<IInitBankruptcyDeceaseRecordState>
) => {
  const { pending, fulfilled, rejected } = getDebtorList;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.loading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      set(draftState, `${storeId}.loading`, false);
      set(draftState, `${storeId}.debtorList`, data);
      set(draftState, `${storeId}.selectedAssociatedDebtor`, data?.[0]);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.loading`, false);
    });
};
