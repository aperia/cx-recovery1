import { I18N_TEXT } from 'app/constants';
import React from 'react';
import { IInitBankruptcyDeceaseRecordState } from './types';

export const GET_DEBTOR_LIST = 'bankruptcyDeceaseRecord/getDebtorList';
export const GET_BANKRUPTCY_RECORD =
  'bankruptcyDeceaseRecord/getBankruptcyRecord';
export const GET_DECEASE_RECORD = 'bankruptcyDeceaseRecord/getDeceaseRecord';
export const BANKRUPTCY_INFO_ACCOUNT_VIEW = 'bankruptcyInfoAccount';
export const BANKRUPTCY_INFO_DEBTOR_ADDRESS_VIEW =
  'bankruptcyInfoDebtorAddress';
export const BANKRUPTCY_INFO_DEBTOR_CASE_VIEW = 'bankruptcyInfoDebtorCase';
export const DECEASED_INFO_ACCOUNT_VIEW = 'deceasedInfoAccount';
export const DECEASED_INFO_DEBTOR_ADDRESS_VIEW = 'deceasedInfoDebtorAddress';
export const SAVE_BANKRUPTCY_RECORD =
  'bankruptcyDeceaseRecord/saveBankruptcyRecord';
export const DELETE_BANKRUPTCY_RECORD =
  'bankruptcyDeceaseRecord/deleteBankruptcyRecord';
export const SAVE_DECEASE_RECORD = 'bankruptcyDeceaseRecord/saveDeceaseRecord';
export const DELETE_DECEASE_RECORD =
  'bankruptcyDeceaseRecord/deleteDeceaseRecord';

export const initialBankruptcyDeceaseRecordState: IInitBankruptcyDeceaseRecordState =
  {};

export const BANKRUPTCY_VIEW_SECTIONS = [
  {
    id: 'account',
    viewId: BANKRUPTCY_INFO_ACCOUNT_VIEW,
    title: I18N_TEXT.ACCOUNT,
    dataField: 'account'
  },
  {
    id: 'debtorAddress',
    viewId: BANKRUPTCY_INFO_DEBTOR_ADDRESS_VIEW,
    title: I18N_TEXT.DEBTOR_ADDRESS,
    dataField: 'debtorAddress'
  },
  {
    id: 'debtorCase',
    viewId: BANKRUPTCY_INFO_DEBTOR_CASE_VIEW,
    title: I18N_TEXT.DEBTOR_CASE,
    dataField: 'debtorCase'
  }
];

export const DECEASE_VIEW_SECTIONS = [
  {
    id: 'account',
    viewId: DECEASED_INFO_ACCOUNT_VIEW,
    title: I18N_TEXT.ACCOUNT,
    dataField: 'account'
  },
  {
    id: 'debtorAddress',
    viewId: DECEASED_INFO_DEBTOR_ADDRESS_VIEW,
    title: I18N_TEXT.DEBTOR_ADDRESS,
    dataField: 'debtorAddress'
  }
];

export const RECORD_BANKRUPTCY_TABS = [
  {
    id: 'account',
    title: I18N_TEXT.ACCOUNT,
    Component: React.lazy(() => import('./RecordBankruptcy/AccountForm')),
    saveSuccessMessage: I18N_TEXT.ACCOUNT_INFORMATION_UPDATED,
    relation: 'A'
  },
  {
    id: 'debtorAddress',
    title: I18N_TEXT.DEBTOR_ADDRESS,
    Component: React.lazy(() => import('./RecordBankruptcy/DebtorAddressForm')),
    saveSuccessMessage: I18N_TEXT.DEBTOR_ADDRESS_INFORMATION_UPDATED
  },
  {
    id: 'debtorCase',
    title: I18N_TEXT.DEBTOR_CASE,
    Component: React.lazy(() => import('./RecordBankruptcy/DebtorCaseForm')),
    saveSuccessMessage: I18N_TEXT.DEBTOR_CASE_INFORMATION_UPDATED
  }
];

export const RECORD_DECEASE_TABS = [
  {
    id: 'account',
    title: I18N_TEXT.ACCOUNT,
    Component: React.lazy(() => import('./RecordDecease/AccountForm')),
    saveSuccessMessage: I18N_TEXT.ACCOUNT_INFORMATION_UPDATED,
    relation: 'A'
  },
  {
    id: 'debtorAddress',
    title: I18N_TEXT.DEBTOR_ADDRESS,
    Component: React.lazy(() => import('./RecordDecease/DebtorAddressForm')),
    saveSuccessMessage: I18N_TEXT.DEBTOR_ADDRESS_INFORMATION_UPDATED
  }
];
