import { I18N_TEXT } from 'app/constants';
import { useSelectorId } from 'app/hooks';
import { TruncateText } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { mappingRelationValue } from 'pages/AccountManagement/ManageAssociatedDebtors/helpers';
import React from 'react';
import { IDebtorItem } from '../types';
import { selectSelectedAssociatedDebtor } from '../_redux/selectors';

const Overview: React.FC = () => {
  const { t } = useTranslation();
  const associatedDebtor: IDebtorItem | undefined = useSelectorId(
    selectSelectedAssociatedDebtor
  );

  return (
    <div className="p-24 bg-light-l20 border-bottom">
      <h4>{associatedDebtor?.debtorName}</h4>
      <div className="row mt-16">
        <div className="col-3 col-xl-2">
          <span className="form-group-static__label">
            <TruncateText>{t(I18N_TEXT.LAST_4_SSN)}</TruncateText>
          </span>
          <div className={'form-group-static__text pre-line'}>
            {associatedDebtor?.last4SSN}
          </div>
        </div>
        <div className="col-3 col-xl-2">
          <span className="form-group-static__label">
            <TruncateText>{t(I18N_TEXT.RELATION)}</TruncateText>
          </span>
          <div className={'form-group-static__text pre-line'}>
            {`${associatedDebtor?.relation} - ${t(
              mappingRelationValue(associatedDebtor?.relation)
            )}`}
          </div>
        </div>
      </div>
    </div>
  );
};

export default Overview;
