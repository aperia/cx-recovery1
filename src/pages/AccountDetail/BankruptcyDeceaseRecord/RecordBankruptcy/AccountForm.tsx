import { ComboBoxControl, TextBoxControl } from 'app/components';
import { I18N_TEXT, NUMERIC_REPLACE_REGEX } from 'app/constants';
import { Button, DatePicker, SimpleBar } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { FormikProvider } from 'formik';
import { isEmpty, isUndefined } from 'app/helpers';
import React, { useLayoutEffect, useRef } from 'react';
import { getDateData } from '../helpers';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';
import { useRecordBankruptcyForm } from '../hooks/useRecordBankruptcyForm';

const AccountForm: React.FC = () => {
  const { t } = useTranslation();
  const divRef = useRef<HTMLDivElement | null>(null);

  const {
    formik,
    stateData,
    paymentScheduleFrequencyData,
    handleChangeComboBox,
    handleClearAll
  } = useRecordBankruptcyForm('account');

  const { values, errors, handleChange, submitForm } = formik;

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 262px)`);
  }, []);

  return (
    <FormikProvider value={formik}>
      <div ref={divRef}>
        <SimpleBar>
          <div className="p-24 max-width-lg mx-auto">
            <div className="d-flex">
              <h5>{t(I18N_TEXT.ACCOUNT)}</h5>
              <span className="ml-auto" onClick={handleClearAll}>
                <Button variant="outline-primary" className="mr-n8" size="sm">
                  {t(I18N_TEXT.CLEAR_ALL)}
                </Button>
              </span>
            </div>
            <div className="row mt-24">
              <TextBoxControl
                className="col-md-6"
                label={t(I18N_TEXT.CASE_NUMBER)}
                name="caseNumber"
                maxLength={20}
              />
              <FormikNumericControl
                className="col-md-6"
                label={t(I18N_TEXT.ORIGINAL_LOAN_AMOUNT)}
                name="loanAmount"
                maxLength={15}
                format="c2"
              />
            </div>
            <h6 className="mt-24 color-grey">
              {t(I18N_TEXT.ORIGINAL_CONTRACT)}
            </h6>
            <div className="row mt-16">
              <TextBoxControl
                className="col-md-6 col-lg-3"
                label={t(I18N_TEXT.TERM)}
                name="term"
                maxLength={3}
              />
              <TextBoxControl
                className="col-md-6 col-lg-3"
                label={t(I18N_TEXT.INSURANCE)}
                name="insurance"
                maxLength={25}
              />
              <ComboBoxControl
                className="col mt-16 mt-lg-0"
                name="paymentScheduleFrequency"
                label={t(I18N_TEXT.PAYMENT_SCHEDULE_FREQUENCY)}
                options={
                  (paymentScheduleFrequencyData?.bankruptcyDeceasePaymentScheduleFrequency ||
                    []) as RefDataValue[]
                }
                onChange={handleChangeComboBox}
              />
              {values.paymentScheduleFrequency === 'custom' && (
                <TextBoxControl
                  required
                  className="col-md-4 col-lg-2 mt-16 mt-lg-0"
                  name="paymentScheduleFrequencyCount"
                  label={t(I18N_TEXT.COUNT)}
                  suffix={t(I18N_TEXT.DAYS).toLowerCase()}
                  maxLength={3}
                  regex={NUMERIC_REPLACE_REGEX}
                  error={{
                    status: !isUndefined(errors.paymentScheduleFrequencyCount),
                    message: errors.paymentScheduleFrequencyCount as string
                  }}
                />
              )}
            </div>
            <div className="row">
              <FormikNumericControl
                className="col-md-6 col-lg-4 mt-16"
                label={t(I18N_TEXT.DAYS_DELINQUENT)}
                name="daysDelinquent"
                maxLength={25}
                format="n0"
              />
              <FormikNumericControl
                className="col-md-6 col-lg-4 mt-16"
                label={t(I18N_TEXT.NON_SUFFICIENT_FUNDS_NUMBER)}
                name="nonSufficientFundsNumber"
                maxLength={4}
                format="n0"
              />
              <div className="col-md-12 col-lg-4 mt-16">
                <DatePicker
                  label={t(I18N_TEXT.LAST_NON_SUFFICIENT_FUNDS_DATE)}
                  value={getDateData(values.lastNonSufficientFundsDate)}
                  onChange={handleChange}
                  name="lastNonSufficientFundsDate"
                />
              </div>
              <div className="col-md-6 col-lg-4 mt-16">
                <DatePicker
                  label={t(I18N_TEXT.LAST_PURCHASE_DATE)}
                  value={getDateData(values.lastPurchasedDate)}
                  onChange={handleChange}
                  name="lastPurchasedDate"
                />
              </div>
              <FormikNumericControl
                className="col-md-6 col-lg-4 mt-16"
                label={t(I18N_TEXT.LAST_PURCHASE_AMOUNT)}
                name="lastPurchasedAmount"
                maxLength={13}
                format="c2"
              />
              <div className="col-md-12 col-lg-4 mt-16">
                <DatePicker
                  label={t(I18N_TEXT.LAST_CASH_ADVANCE_DATE)}
                  value={getDateData(values.lastCashAdvanceDate)}
                  onChange={handleChange}
                  name="lastCashAdvanceDate"
                />
              </div>
              <FormikNumericControl
                className="col-md-6 col-lg-4 mt-16"
                label={t(I18N_TEXT.LAST_CAST_AMOUNT)}
                name="lastCastAmount"
                format="c2"
                maxLength={13}
              />
              <FormikNumericControl
                className="col-md-6 col-lg-4 mt-16"
                label={t(I18N_TEXT.LAST_CAST_ADVANCE_AMOUNT)}
                name="lastCastAdvanceAmount"
                format="c2"
                maxLength={13}
              />
              <TextBoxControl
                className="col-md-12 col-lg-4 mt-16"
                label={t(I18N_TEXT.COURT_AWARDED_REPAYMENT_TERM)}
                name="courtAwardedRepaymentTerm"
                maxLength={3}
              />
              <FormikNumericControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.CURRENT_BALANCE)}
                name="currentBalance"
                format="c2"
                maxLength={13}
              />
              <FormikNumericControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.LEGAL_EXPENSES)}
                name="legalExpenses"
                format="c2"
                maxLength={13}
              />
              <FormikNumericControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.OTHER_EXPENSES)}
                name="otherExpenses"
                format="c2"
                maxLength={13}
              />
              <FormikNumericControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.PAYMENT_AMOUNT)}
                name="paymentAmount"
                format="c2"
                maxLength={13}
              />
              <TextBoxControl
                className="col-lg-3 mt-16"
                label={t(I18N_TEXT.COURT_NAME)}
                name="courtName"
                maxLength={30}
              />
              <TextBoxControl
                className="col-12 col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_1)}
                name="addressLine1"
              />
              <TextBoxControl
                className="col-12 col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_2)}
                name="addressLine2"
              />
            </div>
            <div className="row">
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.CITY)}
                name="city"
              />
              <ComboBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.STATE)}
                name="state"
                options={stateData?.state as RefDataValue[]}
                onChange={handleChangeComboBox}
                isCombine
              />
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.ZIP_CODE)}
                name="zipCode"
              />
            </div>
            <div className="row">
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.SHORT_FIELD_1)}
                name="shortField1"
                maxLength={5}
              />
              <div className="col-md-6 col-lg-3 mt-16">
                <DatePicker
                  label={t(I18N_TEXT.DATE_FIELD_1)}
                  value={getDateData(values.dateField1)}
                  onChange={handleChange}
                  name="dateField1"
                />
              </div>
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.LONG_FIELD_1)}
                name="longField1"
                maxLength={25}
              />
              <FormikNumericControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.AMOUNT_1)}
                name="amount1"
                format="c2"
                maxLength={17}
              />
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.SHORT_FIELD_2)}
                name="shortField2"
                maxLength={5}
              />
              <div className="col-md-6 col-lg-3 mt-16">
                <DatePicker
                  label={t(I18N_TEXT.DATE_FIELD_2)}
                  value={getDateData(values.dateField2)}
                  onChange={handleChange}
                  name="dateField2"
                />
              </div>
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.LONG_FIELD_2)}
                name="longField2"
                maxLength={25}
              />
              <FormikNumericControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.AMOUNT_2)}
                name="amount2"
                format="c2"
                maxLength={17}
              />
            </div>
          </div>
        </SimpleBar>
      </div>
      <div className="w-100 border-top px-24 py-16 text-right">
        <Button
          variant="primary"
          onClick={submitForm}
          disabled={!isEmpty(errors)}
        >
          {t(I18N_TEXT.SAVE)}
        </Button>
      </div>
    </FormikProvider>
  );
};

export default AccountForm;
