export interface IRecordBankruptcyData {
  account?: MagicKeyValue;
  debtorAddress?: MagicKeyValue;
  debtorCase?: MagicKeyValue;
}
