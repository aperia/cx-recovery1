import { I18N_TEXT, NUMERIC_REPLACE_REGEX } from 'app/constants';
import { Button, DatePicker, SimpleBar } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { FormikProvider } from 'formik';
import React, { useLayoutEffect, useRef } from 'react';
import { getDateData } from '../helpers';
import { ComboBoxControl, TextBoxControl } from 'app/components';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';
import { useRecordBankruptcyForm } from '../hooks/useRecordBankruptcyForm';
import { isUndefined } from 'app/helpers';


const DebtorCaseForm: React.FC = () => {
  const { t } = useTranslation();
  const divRef = useRef<HTMLDivElement | null>(null);

  const {
    formik,
    paymentScheduleFrequencyData,
    handleChangeComboBox,
    handleClearAll
  } = useRecordBankruptcyForm('debtorCase');

  const { values, errors, handleChange, submitForm } = formik;

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 262px)`);
  }, []);

  return (
    <>
      <FormikProvider value={formik}>
        <div ref={divRef}>
          <SimpleBar>
            <div className="p-24 max-width-lg mx-auto">
              <div className="d-flex">
                <h5>{t(I18N_TEXT.DEBTOR_CASE)}</h5>
                <span className="ml-auto" onClick={handleClearAll}>
                  <Button className="mr-n8" variant="outline-primary" size="sm">
                    {t(I18N_TEXT.CLEAR_ALL)}
                  </Button>
                </span>
              </div>
              <h6 className="mt-24 color-grey">{t(I18N_TEXT.CASE)}</h6>
              <div className="row">
                <FormikNumericControl
                  className="col-md-6 col-lg-3 mt-16"
                  label={t(I18N_TEXT.CHAPTER)}
                  name="chapter"
                  maxLength={2}
                  format="n0"
                />
                <TextBoxControl
                  className="col-md-6 col-lg-3 mt-16"
                  label={t(I18N_TEXT.CHAPTER_ID)}
                  name="chapterID"
                  maxLength={20}
                />
                <TextBoxControl
                  className="col-md-6 col-lg-3 mt-16"
                  label={t(I18N_TEXT.DISTRICT_AND_STATE)}
                  name="districtAndState"
                  maxLength={30}
                />
                <TextBoxControl
                  className="col-md-6 col-lg-3 mt-16"
                  label={t(I18N_TEXT.COLLATERAL)}
                  name="collateral"
                  maxLength={30}
                />
                <FormikNumericControl
                  className="col-md-6 col-lg-3 mt-16"
                  label={t(I18N_TEXT.PAYMENT_AMOUNT)}
                  name="paymentAmount"
                  format="c2"
                  maxLength={13}
                />
                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.START_DATE)}
                    value={getDateData(values.startDate)}
                    onChange={handleChange}
                    name="startDate"
                  />
                </div>
                <ComboBoxControl
                  className="col-auto mt-16"
                  name="paymentScheduleFrequency"
                  label={t(I18N_TEXT.PAYMENT_SCHEDULE_FREQUENCY)}
                  options={
                    (paymentScheduleFrequencyData?.bankruptcyDeceasePaymentScheduleFrequency ||
                      []) as RefDataValue[]
                  }
                  onChange={handleChangeComboBox}
                />
                {values.paymentScheduleFrequency === 'custom' && (
                  <TextBoxControl
                    required
                    className="col-md-4 col-lg-2 mt-16"
                    name="paymentScheduleFrequencyCount"
                    label={t(I18N_TEXT.COUNT)}
                    suffix={t(I18N_TEXT.DAYS).toLowerCase()}
                    maxLength={3}
                    regex={NUMERIC_REPLACE_REGEX}
                    error={{
                      status: !isUndefined(
                        errors.paymentScheduleFrequencyCount
                      ),
                      message: errors.paymentScheduleFrequencyCount as string
                    }}
                  />
                )}
              </div>
              <h6 className="mt-24 color-grey">{t(I18N_TEXT.CASE_DATES)}</h6>
              <div className="row">
                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.FILE_DATE)}
                    value={getDateData(values.fileDate)}
                    onChange={handleChange}
                    name="fileDate"
                  />
                </div>
                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.FIRST_MEETING_DATE)}
                    value={getDateData(values.firstMeetingDate)}
                    onChange={handleChange}
                    name="firstMeetingDate"
                  />
                </div>
                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.PROOF_OF_CLAIM_DATE)}
                    value={getDateData(values.proofOfClaimDate)}
                    onChange={handleChange}
                    name="proofOfClaimDate"
                  />
                </div>
                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.REAFFIRMATION_DATE)}
                    value={getDateData(values.reaffirmationDate)}
                    onChange={handleChange}
                    name="reaffirmationDate"
                  />
                </div>
                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.DISCHARGE_DATE)}
                    value={getDateData(values.dischargeDate)}
                    onChange={handleChange}
                    name="dischargeDate"
                  />
                </div>
                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.RELIEF_OF_STAY_DATE)}
                    value={getDateData(values.reliefOfStayDate)}
                    onChange={handleChange}
                    name="reliefOfStayDate"
                  />
                </div>
                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.DISMISSAL_DATE)}
                    value={getDateData(values.dismissalDate)}
                    onChange={handleChange}
                    name="dismissalDate"
                  />
                </div>
                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.WITHDRAWN_DATE)}
                    value={getDateData(values.withdrawnDate)}
                    onChange={handleChange}
                    name="withdrawnDate"
                  />
                </div>
              </div>
              <h6 className="mt-24 color-grey">{t(I18N_TEXT.PREVIOUS_CASE)}</h6>
              <div className="row">
                <FormikNumericControl
                  className="col-md-6 col-lg-3 mt-16"
                  label={t(I18N_TEXT.CHAPTER)}
                  name="previousChapter"
                  maxLength={2}
                  format="n0"
                />
                <TextBoxControl
                  className="col-md-6 col-lg-3 mt-16"
                  label={t(I18N_TEXT.CHAPTER_ID)}
                  name="previousChapterID"
                  maxLength={20}
                />
                <TextBoxControl
                  className="col-md-6 col-lg-3 mt-16"
                  label={t(I18N_TEXT.DISTRICT_AND_STATE)}
                  name="previousDistrictAndState"
                  maxLength={30}
                />
              </div>
              <h6 className="mt-24 color-grey">
                {t(I18N_TEXT.PREVIOUS_CASE_DATES)}
              </h6>
              <div className="row">
                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.FILE_DATE)}
                    value={getDateData(values.previousFileDate)}
                    onChange={handleChange}
                    name="previousFileDate"
                  />
                </div>
                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.DISCHARGE_DATE)}
                    value={getDateData(values.previousDischargeDate)}
                    onChange={handleChange}
                    name="previousDischargeDate"
                  />
                </div>
                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.DISMISSAL_DATE)}
                    value={getDateData(values.previousDismissalDate)}
                    onChange={handleChange}
                    name="previousDismissalDate"
                  />
                </div>
                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.CONVERTED_DATE)}
                    value={getDateData(values.previousConvertedDate)}
                    onChange={handleChange}
                    name="previousConvertedDate"
                  />
                </div>
                <TextBoxControl
                  className="col-md-6 col-lg-3 mt-16"
                  label={t(I18N_TEXT.SHORT_FIELD_1)}
                  name="shortField1"
                  maxLength={5}
                />
                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.DATE_FIELD_1)}
                    value={getDateData(values.dateField1)}
                    onChange={handleChange}
                    name="dateField1"
                  />
                </div>
                <TextBoxControl
                  className="col-md-6 col-lg-3 mt-16"
                  label={t(I18N_TEXT.LONG_FIELD_1)}
                  name="longField1"
                  maxLength={25}
                />

                <FormikNumericControl
                  className="col-md-6 col-lg-3 mt-16"
                  label={t(I18N_TEXT.AMOUNT_1)}
                  name="amount1"
                  format="c2"
                  maxLength={17}
                />
                <TextBoxControl
                  className="col-md-6 col-lg-3 mt-16"
                  label={t(I18N_TEXT.SHORT_FIELD_2)}
                  name="shortField2"
                  maxLength={5}
                />

                <div className="col-md-6 col-lg-3 mt-16">
                  <DatePicker
                    label={t(I18N_TEXT.DATE_FIELD_2)}
                    value={getDateData(values.dateField2)}
                    onChange={handleChange}
                    name="dateField2"
                  />
                </div>
                <TextBoxControl
                  className="col-md-6 col-lg-3 mt-16"
                  label={t(I18N_TEXT.LONG_FIELD_2)}
                  name="longField2"
                  maxLength={25}
                />
                <FormikNumericControl
                  className="col-md-6 col-lg-3 mt-16"
                  label={t(I18N_TEXT.AMOUNT_2)}
                  name="amount2"
                  format="c2"
                  maxLength={17}
                />
              </div>
            </div>
          </SimpleBar>
        </div>
        <div className="w-100 border-top px-24 py-16 text-right">
          <Button variant="primary" onClick={submitForm}>
            {t(I18N_TEXT.SAVE)}
          </Button>
        </div>
      </FormikProvider>
    </>
  );
};

export default DebtorCaseForm;
