import {
  ComboBoxControl,
  MaskedTextBoxControl,
  TextBoxControl
} from 'app/components';
import {
  I18N_TEXT,
  PHONE_NUMBER_MASK_INPUT_REGEX,
  PLACE_HOLDER_PHONE_NUMBER
} from 'app/constants';
import { Button, SimpleBar } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { FormikProvider } from 'formik';
import React, { useLayoutEffect, useRef } from 'react';
import { useRecordBankruptcyForm } from '../hooks/useRecordBankruptcyForm';

const DebtorAddressForm: React.FC = () => {
  const { t } = useTranslation();
  const divRef = useRef<HTMLDivElement | null>(null);

  const { formik, stateData, handleChangeComboBox, handleClearAll } =
    useRecordBankruptcyForm('debtorAddress');

  const { submitForm } = formik;

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 262px)`);
  }, []);

  return (
    <FormikProvider value={formik}>
      <div ref={divRef}>
        <SimpleBar>
          <div className="p-24 max-width-lg mx-auto">
            <div className="d-flex">
              <h5>{t(I18N_TEXT.DEBTOR_ADDRESS)}</h5>
              <span className="ml-auto" onClick={handleClearAll}>
                <Button variant="outline-primary" className="mr-n8" size="sm">
                  {t(I18N_TEXT.CLEAR_ALL)}
                </Button>
              </span>
            </div>
            <h6 className="mt-24 color-grey">{t(I18N_TEXT.DEBTOR)}</h6>
            <div className="row">
              <TextBoxControl
                readOnly
                className="col-lg-3 mt-16"
                label={t(I18N_TEXT.DEBTOR_NAME)}
                name="debtorName"
              />
              <TextBoxControl
                className="col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_1)}
                name="debtorAddressLine1"
              />
              <TextBoxControl
                className="col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_2)}
                name="debtorAddressLine2"
              />
            </div>
            <div className="row">
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.CITY)}
                name="debtorCity"
              />
              <ComboBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.STATE)}
                name="debtorState"
                options={stateData?.state as RefDataValue[]}
                onChange={handleChangeComboBox}
                isCombine
              />
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.ZIP_CODE)}
                name="debtorZipCode"
                maxLength={10}
              />
              <MaskedTextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.DEBTOR_PHONE)}
                name="debtorPhone"
                mask={PHONE_NUMBER_MASK_INPUT_REGEX}
                placeholder={PLACE_HOLDER_PHONE_NUMBER}
              />
            </div>
            <h6 className="mt-24 color-grey">{t(I18N_TEXT.TRUSTEE)}</h6>
            <div className="row">
              <TextBoxControl
                className="col-lg-3 mt-16"
                label={t(I18N_TEXT.TRUSTEE_NAME)}
                name="trusteeName"
                maxLength={40}
              />
              <TextBoxControl
                className="col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_1)}
                name="trusteeAddressLine1"
              />
              <TextBoxControl
                className="col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_2)}
                name="trusteeAddressLine2"
              />
            </div>
            <div className="row">
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.CITY)}
                name="trusteeCity"
              />
              <ComboBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.STATE)}
                name="trusteeState"
                options={stateData?.state as RefDataValue[]}
                onChange={handleChangeComboBox}
                isCombine
              />
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.ZIP_CODE)}
                name="trusteeZipCode"
                maxLength={10}
              />
              <MaskedTextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.TRUSTEE_PHONE)}
                name="trusteePhone"
                mask={PHONE_NUMBER_MASK_INPUT_REGEX}
                placeholder={PLACE_HOLDER_PHONE_NUMBER}
              />
            </div>
            <h6 className="mt-24 color-grey">
              {t(I18N_TEXT.OPPOSING_ATTORNEY)}
            </h6>
            <div className="row">
              <TextBoxControl
                className="col-lg-3 mt-16"
                label={t(I18N_TEXT.ATTORNEY_NAME)}
                name="attorneyName"
                maxLength={40}
              />
              <TextBoxControl
                className="col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_1)}
                name="attorneyAddressLine1"
              />
              <TextBoxControl
                className="col-lg mt-16"
                label={t(I18N_TEXT.ADDRESS_LINE_2)}
                name="attorneyAddressLine2"
              />
            </div>
            <div className="row">
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.CITY)}
                name="attorneyCity"
              />
              <ComboBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.STATE)}
                name="attorneyState"
                options={stateData?.state as RefDataValue[]}
                onChange={handleChangeComboBox}
                isCombine
              />
              <TextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.ZIP_CODE)}
                name="attorneyZipCode"
                maxLength={10}
              />
              <MaskedTextBoxControl
                className="col-md-6 col-lg-3 mt-16"
                label={t(I18N_TEXT.ATTORNEY_PHONE)}
                name="attorneyPhone"
                mask={PHONE_NUMBER_MASK_INPUT_REGEX}
                placeholder={PLACE_HOLDER_PHONE_NUMBER}
              />
            </div>
          </div>
        </SimpleBar>
      </div>
      <div className="w-100 border-top px-24 py-16 text-right">
        <Button variant="primary" onClick={submitForm}>
          {t(I18N_TEXT.SAVE)}
        </Button>
      </div>
    </FormikProvider>
  );
};

export default DebtorAddressForm;
