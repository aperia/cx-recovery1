import { formatTime, isValidDate } from 'app/helpers';
import { IRecordBankruptcyData } from '../types';

export const TIME_FIELDS: Record<keyof IRecordBankruptcyData, string[]> = {
  debtorCase: [
    'dischargeDate',
    'dismissalDate',
    'fileDate',
    'firstMeetingDate',
    'previousConvertedDate',
    'previousDischargeDate',
    'previousDismissalDate',
    'previousFileDate',
    'proofOfClaimDate',
    'reaffirmationDate',
    'reliefOfStayDate',
    'withdrawnDate',
    'startDate',
    'dateField1',
    'dateField2'
  ],
  account: [
    'lastNonSufficientFundsDate',
    'lastPurchasedDate',
    'lastCashAdvanceDate',
    'dateField1',
    'dateField2',
    'startDate',
    'dateOfDeath',
    'proofOfClaimDate'
  ],
  debtorAddress: []
};

export const formatDateData = (
  section: keyof IRecordBankruptcyData,
  values: MagicKeyValue
) => {
  const formatData: MagicKeyValue = {};
  for (const key in values) {
    if (TIME_FIELDS[section].includes(key) && isValidDate(values[key])) {
      formatData[key] = formatTime(values[key]).date;
    } else {
      formatData[key] = values[key];
    }
  }
  return formatData;
};
