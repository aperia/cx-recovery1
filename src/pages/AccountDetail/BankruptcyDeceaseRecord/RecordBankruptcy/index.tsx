// components
import React, {
  Suspense,
  useCallback,
  useEffect,
  useMemo,
  useState
} from 'react';
import LazyLoadingFallback from 'app/components/LazyLoadingFallback';
import {
  ModalBody,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import Modal from 'app/_libraries/_dls/components/Modal/Modal';
import ExtraStaticTabs from 'app/_libraries/_dls/components/VerticalTabs';
import Overview from './Overview';

// constants
import { I18N_TEXT } from 'app/constants';
import { RelationType } from 'app/constants/enums';
import { IDebtorItem } from '../types';
import { RECORD_BANKRUPTCY_TABS } from '../constants';

// redux
import { useDispatch } from 'react-redux';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { bankruptcyDeceaseRecordActions } from '../_redux';
import {
  selectRecordBankruptcyShow,
  selectSelectedAssociatedDebtor,
  selectIsDirtyForm
} from '../_redux/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { isNull } from 'app/helpers';

const RecordBankruptcy: React.FC = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const show: boolean = useSelectorId(selectRecordBankruptcyShow);
  const associatedDebtor: IDebtorItem | undefined = useSelectorId(
    selectSelectedAssociatedDebtor
  );
  const isPrimary = useMemo(
    () => associatedDebtor?.relation === RelationType[3],
    [associatedDebtor?.relation]
  );
  const isDirtyForm = useSelectorId(selectIsDirtyForm);
  const [activeTabKey, setActiveTabKey] = useState(
    isPrimary ? RECORD_BANKRUPTCY_TABS[0].id : RECORD_BANKRUPTCY_TABS[1].id
  );

  const toggleRecordBankruptcy = useCallback(() => {
    dispatch(
      bankruptcyDeceaseRecordActions.toggleRecordBankruptcy({ storeId })
    );
  }, [dispatch, storeId]);

  const confirmHideModal = useCallback(
    (callback: () => void) => {
      if (isDirtyForm) {
        dispatch(
          rootModalActions.open({
            title: I18N_TEXT.UNSAVED_CHANGE,
            body: I18N_TEXT.DISCARD_CHANGE_BODY,
            btnCancelText: I18N_TEXT.CONTINUE_EDITING,
            btnConfirmText: I18N_TEXT.DISCARD_CHANGE,
            onConfirm: callback
          })
        );
        return;
      }
      callback();
    },
    [dispatch, isDirtyForm]
  );

  const handleChangeTab = (eventKey: string | null) => {
    if (activeTabKey === eventKey) return;
    if (isNull(eventKey)) return;
    confirmHideModal(() => {
      setActiveTabKey(eventKey);
    });
  };

  useEffect(() => {
    setActiveTabKey(RECORD_BANKRUPTCY_TABS[isPrimary ? 0 : 1].id);
  }, [show, isPrimary]);

  return (
    <Modal show={show} full>
      <ModalHeader
        closeButton
        border
        onHide={() => confirmHideModal(toggleRecordBankruptcy)}
      >
        <ModalTitle>{t(I18N_TEXT.RECORD_BANKRUPTCY)}</ModalTitle>
      </ModalHeader>
      <ModalBody noPadding className="overflow-hidden">
        <Overview />
        <ExtraStaticTabs
          className="scrolled-nav"
          mountOnEnter
          unmountOnExit
          hideCollapsedButton={true}
          activeKey={activeTabKey}
          onSelect={handleChangeTab}
        >
          <div className="tab-left">
            <ExtraStaticTabs.Nav>
              {RECORD_BANKRUPTCY_TABS.map(({ id, title }) => {
                if (id === 'account' && !isPrimary) return null;
                return (
                  <ExtraStaticTabs.Nav.Item key={id}>
                    <ExtraStaticTabs.Nav.Link eventKey={id}>
                      {t(title)}
                    </ExtraStaticTabs.Nav.Link>
                  </ExtraStaticTabs.Nav.Item>
                );
              })}
            </ExtraStaticTabs.Nav>
          </div>
          <ExtraStaticTabs.Content>
            {RECORD_BANKRUPTCY_TABS.map(({ id, Component }) => (
              <ExtraStaticTabs.Pane eventKey={id} key={id}>
                <Suspense fallback={<LazyLoadingFallback />}>
                  <Component />
                </Suspense>
              </ExtraStaticTabs.Pane>
            ))}
          </ExtraStaticTabs.Content>
        </ExtraStaticTabs>
      </ModalBody>
    </Modal>
  );
};

export default RecordBankruptcy;
