import React from 'react';
import { HorizontalTabs } from 'app/_libraries/_dls/components';
import BankruptcyRecord from './BankruptcyRecord';
import DeceaseRecord from './DeceaseRecord';
import { useTranslation } from 'app/_libraries/_dls/hooks';

const TABS = [
  {
    id: 'bankruptcy',
    title: 'txt_bankruptcy',
    component: <BankruptcyRecord />
  },
  {
    id: 'decease',
    title: 'txt_decease',
    component: <DeceaseRecord />
  }
];

const RecordTab: React.FC = () => {
  const { t } = useTranslation();

  return (
    <div className="px-24 border-left h-100">
      <HorizontalTabs level2>
        {TABS.map(item => (
          <HorizontalTabs.Tab
            id={item.id}
            key={item.title}
            eventKey={item.id}
            title={t(item.title)}
          >
            {item.component}
          </HorizontalTabs.Tab>
        ))}
      </HorizontalTabs>
    </div>
  );
};

export default RecordTab;
