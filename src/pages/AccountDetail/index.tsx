import React, { useEffect, useRef, useState } from 'react';

// component
import { HorizontalTabs, SimpleBar } from 'app/_libraries/_dls/components';
import SnapshotAccountDetail from './Snapshot';
import InfoBar from 'app/components/InfoBar';
import NextActionsButton from './NextActions';

// hooks
import { AccountDetailProvider } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { tabList } from './constants';
import { SelectCallback } from 'react-bootstrap/esm/helpers';
import Ledger from './Ledger';

interface AccountDetailProps {
  storeId: string;
  debtorName: string;
  accountNumber: string;
  defaultTabId?: string;
}

const AccountDetail: React.FC<AccountDetailProps> = ({
  storeId,
  debtorName,
  accountNumber,
  defaultTabId = tabList[0].key
}) => {
  const { t } = useTranslation();
  const [activeTabId, setActiveTabId] = useState(defaultTabId);

  const contextValueRef = useRef({
    storeId,
    accNumber: accountNumber
  });

  useEffect(() => {
    setActiveTabId(defaultTabId);
  }, [defaultTabId]);

  const onSelect: SelectCallback = tab => {
    if (!tab) return;
    setActiveTabId(tab);
  };

  const renderListTab = tabList.map(
    ({ key, eventKey, title, component: Component }: MagicKeyValue) => {
      return (
        <HorizontalTabs.Tab key={key} eventKey={eventKey} title={t(title)}>
          <Component />
        </HorizontalTabs.Tab>
      );
    }
  );

  return (
    <AccountDetailProvider value={contextValueRef.current}>
      <div className="d-flex">
        <div className="info-bar-main ">
          <SimpleBar>
            <SnapshotAccountDetail debtorName={debtorName} />
            <div className="overview-horizontal-tabs mt-n45">
              <HorizontalTabs
                defaultActiveKey={tabList[0].key}
                activeKey={activeTabId}
                mountOnEnter
                unmountOnExit
                className="bg-light-l20 max-width-lg mx-auto"
                onSelect={onSelect}
              >
                {renderListTab}
              </HorizontalTabs>
            </div>
          </SimpleBar>
        </div>
        <InfoBar />
        <Ledger />
        <NextActionsButton />
      </div>
    </AccountDetailProvider>
  );
};

export default AccountDetail;
