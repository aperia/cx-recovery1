import { createSelector } from '@reduxjs/toolkit';

export const getAccountDetail = createSelector(
  (states: RootState, storeId: string) =>
    states.accountDetail[storeId]?.snapshot?.data,
  (data: any) => data
);
export const getLoadingSnapShot = createSelector(
  (states: RootState, storeId: string) =>
    states.accountDetail[storeId]?.snapshot?.isLoading,
  (data: any) => data
);
