import { createSlice } from '@reduxjs/toolkit';
import { IInitialStateAccDetail } from '../types';
import { getAccount, getAccountBuilder } from './getAccountDetail';
const initialState: IInitialStateAccDetail = {};

export const { actions, reducer } = createSlice({
  name: 'accountDetail',
  initialState: initialState,
  reducers: {},
  extraReducers: builder => {
    getAccountBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getAccount
};

export { combineActions as accountDetailActions };
