import accountDetailService from '../accountDetailService';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { IInitialStateAccDetail } from '../types';
import { set } from 'lodash';

export const getAccount = createAsyncThunk<
  any,
  { storeId: string; accNumber: string },
  ThunkAPIConfig
>('getAccount', async (args, { rejectWithValue }) => {
  try {
    const { accNumber } = args;
    const { data } = await accountDetailService.getAccount(accNumber);

    return { data: data.data };
  } catch (error) {
    return rejectWithValue(error);
  }
});

export const getAccountBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateAccDetail>
) => {
  const { pending, fulfilled, rejected } = getAccount;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.snapshot.isLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      set(draftState, `${storeId}.snapshot.isLoading`, false);
      set(draftState, `${storeId}.snapshot.data`, data);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `${storeId}.snapshot.isLoading`, false);
    });
};
