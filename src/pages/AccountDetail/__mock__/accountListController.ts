import accountList from './accountList.json';
import { accountGroupListData } from 'pages/AccountGroup/__mock__/accountGroupController';
import { IAccounts } from 'pages/AccountManagement/types';
import { IDataGroupList } from 'pages/AccountGroup/types';

export const accountListData: IAccounts[] = JSON.parse(JSON.stringify(accountList));

export const accountDetailController = {
  getAccount(accNumber: string) {
    const mappingDataGroup = accountGroupListData.find(
      item => item.accNumber.accountNumber === accNumber
    );
    const data = accountListData.find(item => item.accountNumber === accNumber);
    let accGroupList = [];

    if (mappingDataGroup) {
      accGroupList = accountGroupListData.filter(
        (obj: IDataGroupList) => obj.groupId === mappingDataGroup?.groupId
      );
    }

    return Promise.resolve({
      data: {
        success: true,
        data: {
          ...data,
          groupId: mappingDataGroup?.groupId,
          numberOfGroup: accGroupList?.length
        }
      }
    });
  }
};
