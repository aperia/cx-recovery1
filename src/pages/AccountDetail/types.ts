export declare type idVerticalTab =
  | 'generalInfo'
  | 'agencyInfo'
  | 'paymentInfo'
  | 'financialInfo'
  | 'miscellaneousInfo';

export interface IVerticalTabOverview {
  title: string;
  component: React.FC<unknown>;
  id: idVerticalTab;
}

export interface IAccDetail {
  snapshot: {
    data: {};
    isLoading: boolean;
  };
  activeTabId?: string;
}

export interface IInitialStateAccDetail {
  [storeId: string]: IAccDetail;
}
