import React, { useEffect, useState } from 'react';
import { I18N_TEXT } from 'app/constants';
import { HorizontalTabs } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { tabList } from './constants';
import { SelectCallback } from 'react-bootstrap/esm/helpers';

interface BatchManagementProps {
  defaultTabId?: string;
}

const BatchManagement: React.FC<BatchManagementProps> = ({
  defaultTabId = tabList[0].key
}) => {
  const [activeTabId, setActiveTabId] = useState(defaultTabId);

  const { t } = useTranslation();

  const renderListTab = tabList.map(
    ({ key, eventKey, title, component: Component }: MagicKeyValue) => {
      return (
        <HorizontalTabs.Tab key={key} eventKey={eventKey} title={t(title)}>
          <Component />
        </HorizontalTabs.Tab>
      );
    }
  );

  useEffect(() => {
    setActiveTabId(defaultTabId);
  }, [defaultTabId]);

  const onSelect: SelectCallback = tab => {
    if (!tab) return;
    setActiveTabId(tab);
  };

  return (
    <>
      <div className="bg-light-l20">
        <div className="p-24">
          <h3>{t(I18N_TEXT.BATCH_MANAGEMENT)}</h3>
        </div>
      </div>

      <HorizontalTabs
        defaultActiveKey={tabList[0].key}
        activeKey={activeTabId}
        mountOnEnter
        unmountOnExit
        className="bg-light-l20 h-100"
        onSelect={onSelect}
        level2
      >
        {renderListTab}
      </HorizontalTabs>
    </>
  );
};

export default BatchManagement;
