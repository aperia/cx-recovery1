import { I18N_TEXT, ordersDropdown } from 'app/constants';
import { NoCombineField } from 'app/_libraries/_dls/components';
import { TODAY } from 'pages/AgencyManagement/components/SaveAgencyRequestModal/constants';
import { ChargedOffBatch } from './ChargedOffBatch';
import { FinancialBatch } from './FinancialBatch';
import { ItemPaymentList } from './types';

export const tabList = [
  {
    key: 'FinancialBatch',
    eventKey: 'FinancialBatch',
    title: 'txt_financial_batch',
    component: FinancialBatch
  },

  {
    key: 'ChargedOffBatch',
    eventKey: 'ChargedOffBatch',
    title: 'txt_charged_off_batch',
    component: ChargedOffBatch
  }
];

export enum AutoFeeOrSpread {
  YES = 'Yes',
  NO = 'No'
}

export enum TRANS_DATE_FILTER {
  SPECIFIC_DATE = 'Specific Date',
  DATE_RANGE = 'Date Range'
}

export const TRANSACTION_TYPE_DEFAULT = 'D';

export const FINANCIAL_BATCH_FORM = {
  ADD_BATCH: {
    batchNumber: {
      name: 'batchNumber',
      label: 'txt_batch_number'
    },
    batchDate: {
      name: 'batchDate',
      label: 'txt_batch_date'
    },
    createdBy: {
      name: 'createdBy',
      label: 'txt_created_by'
    },
    sourceId: {
      name: 'sourceId',
      label: 'txt_source_id'
    },
    userId: {
      name: 'userId',
      label: 'txt_user_id'
    },
    batchStatus: {
      name: 'batchStatus',
      label: 'txt_batch_status'
    },
    batchType: {
      name: 'batchType',
      label: 'txt_batch_type'
    },
    numberOfItems: {
      name: 'numberOfItems',
      label: 'txt_number_of_items'
    },
    transactionAmount: {
      name: 'transactionAmount',
      label: 'txt_transaction_amount'
    }
  }
};

export const INIT_BATCH_FORM_VALUE = {
  batchNumber: Math.round(Math.random() * 100000000).toString(),
  batchDate: TODAY,
  createdBy: 'O',
  sourceId: '',
  userId: 'Damir Aretas',
  batchStatus: 'O',
  batchType: '',
  numberOfItems: '',
  transactionDate: TODAY,
  transactionType: TRANSACTION_TYPE_DEFAULT,
  transactionAmount: '',
  transactionAmountPaymentEntry: '',
  refference: '',
  autoFee: AutoFeeOrSpread.YES,
  autoSpread: AutoFeeOrSpread.YES,
  nextScheduledPaymentDate: '03/31/2022',
  cash: '30000',
  source: '',
  serviceFee: '',
  manualAgencyFee: '',
  itemsPayment: [] as ItemPaymentList[],
  isMatch: false
};

export const INIT_PAYMENT_ENTRY_FORM_VALUE = {
  transactionDate: TODAY,
  transactionType: TRANSACTION_TYPE_DEFAULT,
  source: '',
  refference: '',
  nextScheduledPaymentDate: '03/31/2022',
  serviceFee: '',
  cash: '30000',
  autoFee: AutoFeeOrSpread.YES,
  manualAgencyFee: '',
  autoSpread: AutoFeeOrSpread.YES,
  transactionAmountPaymentEntry: '',
  subitems: []
};

export const ITEM_PAYMENT_ENTRY_FIELD = {
  TRANSACTION_DATE: {
    name: 'transactionDate',
    label: I18N_TEXT.TRANSACTION_DATE
  },
  TRANSACTION_TYPE: {
    name: 'transactionType',
    label: I18N_TEXT.TRANSACTION_TYPE
  },
  SOURCE: {
    name: 'source',
    label: I18N_TEXT.SOURCE
  },
  REFFERENCE: {
    name: 'refference',
    label: I18N_TEXT.REFFERENCE
  },
  NEXT_SCHEDULED_PAYMENT_DATE: {
    name: 'nextScheduledPaymentDate',
    label: I18N_TEXT.NEXT_SCHEDULED_PAYMENT_DATE
  },
  SENDING_AGENCY: {
    name: 'sendingAgency',
    label: I18N_TEXT.SENDING_AGENCY
  },
  SERVICE_FEE: {
    name: 'serviceFee',
    label: I18N_TEXT.SERVICE_FEE
  },
  CASH: {
    name: 'cash',
    label: I18N_TEXT.CASH
  },
  AUTO_FEE: {
    name: 'autoFee',
    label: I18N_TEXT.AUTO_FEE
  },
  MANUAL_AGENCY_FEE: {
    name: 'manualAgencyFee',
    label: I18N_TEXT.MANUAL_AGENCY_FEE
  },
  AUTO_SPREAD: {
    name: 'autoSpread',
    label: I18N_TEXT.AUTO_SPREAD
  },
  TRANSACTION_AMOUNT: {
    name: 'transactionAmountPaymentEntry',
    label: I18N_TEXT.TRANSACTION_AMOUNT
  },
  FTC: {
    name: 'ftc',
    label: I18N_TEXT.FTC
  },
  TO_PAY_AMOUNT: {
    name: 'toPayAmount',
    label: I18N_TEXT.TO_PAY_AMOUNT
  }
};

export const FILTER_ITEM_PAYMENT_ENTRY_FIELD = {
  ACCOUNT_NUMBER: {
    name: 'accountNumber',
    label: I18N_TEXT.ACCOUNT_NUMBER
  },
  DEBTOR_NAME: {
    name: 'debtorName',
    label: I18N_TEXT.DEBTOR_NAME
  },
  TRANS_DATE: {
    name: 'transDate',
    label: I18N_TEXT.TRANS_DATE
  },
  DATE: {
    name: 'date',
    label: I18N_TEXT.DATE
  },
  DATE_RANGE: {
    name: 'dateRange',
    label: I18N_TEXT.DATE_RANGE
  },
  TRANS_TYPE: {
    name: 'transactionType',
    label: I18N_TEXT.TRANSACTION_TYPE
  }
};

export const POIC_BALANCE_AMOUNT = {
  CCCP: {
    POIC: 'P',
    balanceAmount: '500.00'
  },
  IINIT: {
    POIC: 'O',
    balanceAmount: '400.00'
  },
  PCCP: {
    POIC: 'I',
    balanceAmount: '300.00'
  },
  PINT: {
    POIC: 'C',
    balanceAmount: '200.00'
  }
};
export const GRID_IDS = {
  FTC: 'ftc',
  POIC: 'poic',
  BALANCE_AMOUNT: 'balanceAmount',
  TO_PAY_AMOUNT: 'toPayAmount',
  TRANSACTION_AMOUNT: 'transactionAmount'
};

export const BATCH_TYPE_PAYMENTS = 'P';

export const TRANSACTION_TYPE = {
  G: {
    value: 'G',
    description: 'Gross Payment'
  },
  N: {
    value: 'N',
    description: 'Net Payment'
  },
  S: {
    value: 'S',
    description: 'Gross Payment From a Service'
  },
  T: {
    value: 'T',
    description: 'Net Payment From a Service'
  },
  D: {
    value: 'D',
    description: 'Direct Payments'
  }
};

export const sortItemsPaymentByDefault = {
  description: 'Item Number',
  value: 'itemNumber'
};

export const orderItemsPaymentByDefault = ordersDropdown[0];

export enum BATCH_STATUS {
  OPEN = 'O',
  CLOSED = 'C',
  ERROR = 'E',
  READY_TO_POST = 'R'
}

export enum CREATED_BY {
  OPERATOR = 'O',
  SOURCE = 'S'
}

export enum ATTRIBUTE_SEARCH_FIELD {
  BATCH_NUMBER = 'batchNumber',
  BATCH_DATE = 'batchDate',
  CREATED_BY_SOURCE = 'createBySource',
  CREATED_BY_OPERATOR = 'createByOperator'
}

export const MESSAGE = {
  BATCH_NUMBER: 'txt_batch_number_is_required',
  BATCH_DATE: 'txt_batch_date_is_required',
  CREATED_BY_SOURCE: 'txt_created_by_source_is_required',
  CREATED_BY_OPERATOR: 'txt_created_by_operator_is_required'
};

export const NO_COMBINE_FIELDS_BATCH_SEARCH: NoCombineField[] = [
  {
    field: ATTRIBUTE_SEARCH_FIELD.BATCH_NUMBER,
    filterMessage: I18N_TEXT.BATCH_NUMBER_CANNOT_COMBINE
  },
  {
    field: ATTRIBUTE_SEARCH_FIELD.BATCH_DATE,
    filterMessage: I18N_TEXT.BATCH_DATE_CANNOT_COMBINE
  },
  {
    field: ATTRIBUTE_SEARCH_FIELD.CREATED_BY_OPERATOR,
    filterMessage: I18N_TEXT.CREATED_BY_OPERATOR_CANNOT_COMBINE
  },
  {
    field: ATTRIBUTE_SEARCH_FIELD.CREATED_BY_SOURCE,
    filterMessage: I18N_TEXT.CREATED_BY_SOURCE_CANNOT_COMBINE
  }
];
