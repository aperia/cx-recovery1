import React from 'react';

// redux
import { useDispatch } from 'react-redux';
import { tabActions } from 'pages/__commons/TabBar/_redux';

// helpers
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_TEXT } from 'app/constants';

export interface OpenBatchManagementProps {}

const OpenBatchManagement: React.FC<OpenBatchManagementProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const handleClick = () => {
    dispatch(
      tabActions.addTab({
        title: t(I18N_TEXT.BATCH_MANAGEMENT),
        storeId: 'BATCH_MANAGEMENT',
        tabType: 'batchManagement',
        iconName: 'file'
      })
    );
  };

  return (
    <span
      className="link-header"
      onClick={handleClick}
    >
      {t(I18N_TEXT.BATCH_MANAGEMENT)}
    </span>
  );
};

export default OpenBatchManagement;
