import { ordersDropdown } from 'app/constants';
import { DateRangePickerValue } from 'app/_libraries/_dls/components';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { AttrCommonModel, FormAction } from 'pages/AccountManagement/types';

export interface InitialStateBatchManagement {
  financialBatch: IFinancialBatch;
  chargedOffBatch: IChargedOffBatch;
}

export interface IFinancialBatch {
  isOpenModal: boolean;
  financialBatchList: IBatch[];
  financialBatchDetails: IBatch;
  isLoading: boolean;
  isError: boolean;
  formValue: IBatch;
  formAction: FormAction;
  itemsPayment: ItemsPayment;
  isCollapseItem: boolean;
  filterBatch: IFilterBatch;
}

export interface ItemsPaymentFilterBy {
  accountNumber?: string;
  debtorName?: string;
  date?: Date;
  dateRange?: DateRangePickerValue;
  transactionType?: string[];
}
export interface PaymentEntry {
  transactionDate?: string;
  transactionType?: string;
  source?: string;
  refference?: string;
  nextScheduledPaymentDate?: string;
  sendingAgency?: string;
  serviceFee?: string;
  cash?: string;
  autoFee?: string;
  manualAgencyFee?: string;
  autoSpread?: string;
  transactionAmountPaymentEntry?: string;
  totalToPayAmount?: string;
}
export interface ItemPaymentList extends PaymentEntry {
  itemNumber?: string;
  debtorName?: string;
  accNumber?: {
    accountNumber: string;
    accountNumberUnMask: string;
  };
  subitems?: Subitems[];
  isTouched?: boolean;
}

export interface Subitems {
  subitemID: string;
  ftc: string;
  poic: string;
  balanceAmount: string;
  toPayAmount?: string;
  transactionAmountPaymentEntry?: string;
}
export interface ModalSubitems {
  isOpenModal?: boolean;
  modalProps?: MagicKeyValue;
}

export interface ItemsPayment {
  itemList: {
    sortBy: RefDataValue;
    orderBy: RefDataValue;
    filterBy: ItemsPaymentFilterBy;
    data: ItemPaymentList[];
  };
  selectedItem: ItemPaymentList;
  modalSubitems: ModalSubitems;
}

export interface IChargedOffBatch {}

export interface IBatch extends PaymentEntry {
  id?: string;
  batchNumber: string;
  batchDate: string;
  createdBy: string;
  sourceId?: string;
  userId?: string;
  batchStatus: string;
  batchType: string;
  numberOfItems: string;
  transactionAmount: string;
  currentItemAmount: string;
  currentItemType: string;
  itemsPayment: ItemPaymentList[];
  isMatch: boolean;
}

export interface IFilterBatch {
  batchType: RefDataValue[];
  batchStatus: RefDataValue[];
  sortBy: RefDataValue;
  orderBy: RefDataValue;
  search: AttributeSearchValue[];
}

export const FinancialBatchInitialState = {
  isOpenModal: false,
  isLoading: false,
  isError: false,
  filterBatch: {
    batchType: [],
    batchStatus: [],
    sortBy: {} as RefDataValue,
    orderBy: {} as RefDataValue
  },
  financialBatchList: [] as IBatch[],
  financialBatchDetails: {},
  formValue: {} as IBatch,
  formAction: 'ADD',
  itemsPayment: {
    itemList: {
      sortBy: {},
      orderBy: ordersDropdown[0],
      filterBy: {},
      data: [] as ItemPaymentList[]
    },
    selectedItem: {},
    modalSubitems: {
      isOpenModal: false,
      modalProps: {}
    }
  },
  isCollapseItem: true
};

export interface AttributeSearchFieldModel {
  batchNumber?: AttrCommonModel;
  batchDate?: AttrCommonModel;
  createBySource?: AttrCommonModel;
  createByOperator?: AttrCommonModel;
}

export type BatchStatus = 'Open' | 'Closed' | 'Ready to Post' | 'Error';

export interface IChargedOffBatch {}
