import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { ATTRIBUTE_SEARCH_FIELD } from '../constants';
import { BatchStatus, IBatch } from '../types';
import financialBatch from './financialBatch.json';

let financialBatchData = JSON.parse(JSON.stringify(financialBatch));

const isHaveError = window.location.href.includes('error');

export const financialBatchController = {
  getFinancialBatch() {
    return Promise.resolve({
      data: {
        financialBatch: financialBatchData,
        success: true
      }
    });
  },
  addBatch(newBatch: IBatch) {
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: 'ADD FAILED'
      });

    financialBatchData = [
      ...financialBatchData,
      {
        ...newBatch,
        id: Date.now().toString()
      }
    ];
    return Promise.resolve({
      success: true
    });
  },
  editBatch(id: string, dataChange: IBatch) {
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: 'EDIT FAILED'
      });

    const indexOfDataChange = financialBatchData.findIndex(
      (item: IBatch) => item.id === id
    );

    financialBatchData = [
      ...financialBatchData.slice(0, indexOfDataChange),
      { id, ...dataChange },
      ...financialBatchData.slice(indexOfDataChange + 1)
    ];
    return Promise.resolve({
      success: true
    });
  },
  deleteBatch(id: string) {
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: 'DELETE FAILED'
      });
    financialBatchData = financialBatchData.filter(
      (item: IBatch) => item.id !== id
    );
    return Promise.resolve({ success: true, data: financialBatchData });
  },
  updateBatchStatusById(id: string, batchStatus: BatchStatus) {
    if (isHaveError)
      return Promise.reject({
        success: false,
        message: 'EDIT FAILED'
      });

    const indexOfDataChange = financialBatchData.findIndex(
      (item: IBatch) => item.id === id
    );

    financialBatchData = [
      ...financialBatchData.slice(0, indexOfDataChange),
      { id, ...financialBatchData[indexOfDataChange], batchStatus },
      ...financialBatchData.slice(indexOfDataChange + 1)
    ];
    return Promise.resolve({
      success: true
    });
  },
  searchBatch(attribute: AttributeSearchValue[]) {
    let data = [];
    if (attribute[0].key === ATTRIBUTE_SEARCH_FIELD.BATCH_NUMBER) {
      data = financialBatchData.filter(
        (item: any) => item.batchNumber === attribute[0].value
      );
    } else if (attribute[0].key === ATTRIBUTE_SEARCH_FIELD.BATCH_DATE) {
      data = financialBatchData.filter(
        (item: any) => new Date(item.batchDate).getTime()  === attribute[0].value.getTime()
      );
    } else if (attribute[0].key === ATTRIBUTE_SEARCH_FIELD.CREATED_BY_OPERATOR) {
      data = financialBatchData.filter(
        (item: any) => item.userId === attribute[0].value.value
      );
    } else {
      data = financialBatchData.filter(
        (item: any) => item.sourceId === attribute[0].value.value
      );
    }

    // if (attribute[0].key === 'batchDate') {
    //   data = financialBatchData.filter(
    //     (item: any) => item.batchDate === attribute[0].value
    //   );
    // }
    return Promise.resolve({
      success: true,
      data
    });
  }
};
