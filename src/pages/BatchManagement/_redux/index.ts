import { createSlice } from '@reduxjs/toolkit';

import {
  FinancialBatchInitialState,
  // IFinancialBatch,
  InitialStateBatchManagement
} from '../types';
import { addBatch, addBatchBuilder } from './financialBatch/addBatch';
import { editBatch, editBatchBuilder } from './financialBatch/editBatch';
import { deleteBatch, deleteBatchBuilder } from './financialBatch/deleteBatch';
import {
  updateBatchStatusById,
  updateBatchStatusByIdBuilder
} from './financialBatch/updateBatchStatusById';

// actions Financial Batch
import {
  onSetModalAddBatch,
  setFinancialBatchDetails,
  triggerEditBatch,
  onChangeSortAccounts,
  onChangeOrderBy,
  setFilterBy,
  onSelectedItem,
  setItemsPayment,
  clearItemPayment,
  linkAccountToItem,
  setModalSubitemPayment,
  removeModalSubitemPayment,
  addSubitemPayment,
  editSubitemPayment,
  removeSubitemPayment,
  onChangeCollapseItem,
  onChangeBatchType,
  onChangeBatchStatus,
  onChangeBatchSort,
  onChangeBatchOrder,
  onChangeBatchSearch
} from './financialBatch/actions';

import {
  searchBatch,
  getSearchBatchBuilder
} from './financialBatch/searchBatch';

import {
  getFinancialBatch,
  getFinancialBatchBuilder
} from './financialBatch/getFinancialBatch';

// actions Charged Off Batch

const initialBatchManagementState: InitialStateBatchManagement = {
  financialBatch: FinancialBatchInitialState as any,
  chargedOffBatch: {}
};

const { actions, reducer } = createSlice({
  name: 'batchManagement',
  initialState: initialBatchManagementState,
  reducers: {
    // Financial Batch
    onSetModalAddBatch,
    setFinancialBatchDetails,
    triggerEditBatch,
    onChangeSortAccounts,
    onChangeOrderBy,
    setFilterBy,
    onSelectedItem,
    setItemsPayment,
    clearItemPayment,
    linkAccountToItem,
    setModalSubitemPayment,
    removeModalSubitemPayment,
    addSubitemPayment,
    editSubitemPayment,
    removeSubitemPayment,
    onChangeCollapseItem,
    onChangeBatchType,
    onChangeBatchStatus,
    onChangeBatchSort,
    onChangeBatchOrder,
    onChangeBatchSearch

    // Charged Off Batch
  },
  extraReducers: builder => {
    addBatchBuilder(builder);
    editBatchBuilder(builder);
    deleteBatchBuilder(builder);
    updateBatchStatusByIdBuilder(builder);
    getFinancialBatchBuilder(builder);
    getSearchBatchBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  // Financial Batch
  addBatch,
  editBatch,
  deleteBatch,
  updateBatchStatusById,
  getFinancialBatch,
  searchBatch
  // Charged Off Batch
};

export { combineActions as batchManagementActions, reducer };
