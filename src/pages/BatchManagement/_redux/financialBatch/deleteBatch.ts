import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import { I18N_TEXT } from 'app/constants';
import { IBatch, InitialStateBatchManagement } from '../../types';
import financialBatchServices from 'pages/BatchManagement/FinancialBatch/financialBatchServices';

const DELETE_BATCH = 'deleteBatch';
interface IPayloadDeleteBatch {
  data: IBatch[];
}
export const deleteBatch = createAsyncThunk<
  IPayloadDeleteBatch,
  { id: string },
  ThunkAPIConfig
>(DELETE_BATCH, async ({ id = '' }, thunkAPI) => {
  const { dispatch } = thunkAPI;
  try {
    const { data, success } = await financialBatchServices.deleteBatch(id);
    if (success) {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.DELETE_BATCH_SUCCESS
        })
      );
      dispatch(rootModalActions.close());
    }
    return { data };
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.DELETE_BATCH_FAILED
      })
    );
    dispatch(rootModalActions.close());
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const deleteBatchBuilder = (
  builder: ActionReducerMapBuilder<InitialStateBatchManagement>
) => {
  const { pending, fulfilled, rejected } = deleteBatch;
  builder
    .addCase(pending, draftState => {
      draftState.financialBatch.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      draftState.financialBatch.isLoading = false;

      draftState.financialBatch.financialBatchList = action.payload
        .data as IBatch[];
    })
    .addCase(rejected, draftState => {
      draftState.financialBatch.isLoading = false;
    });
};
