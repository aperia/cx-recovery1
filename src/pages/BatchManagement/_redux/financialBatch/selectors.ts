import { createSelector } from '@reduxjs/toolkit';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { FormAction } from 'pages/AccountManagement/types';
import {
  ItemsPaymentFilterBy,
  IBatch,
  // ItemPaymentDetails,
  ItemPaymentList,
  ModalSubitems
} from 'pages/BatchManagement/types';
import {
  filterItemsPayment,
  sortOrderItems,
  filterBatch
} from 'pages/BatchManagement/FinancialBatch/helpers';

import { isEmpty } from 'app/helpers';

export const getItemPaymentList = (states: RootState): ItemPaymentList[] =>
  states.batchManagement.financialBatch.itemsPayment.itemList.data;

export const getSubitemsModal = (states: RootState): ModalSubitems =>
  states.batchManagement.financialBatch.itemsPayment.modalSubitems;

export const getSelectedItem = (states: RootState): ItemPaymentList =>
  states.batchManagement.financialBatch.itemsPayment.selectedItem;

export const getFinancialBatchDetails = (states: RootState) =>
  states.batchManagement.financialBatch.financialBatchDetails;

const getOrderBy = (states: RootState): RefDataValue =>
  states.batchManagement.financialBatch.itemsPayment.itemList.orderBy;

const getSortBy = (states: RootState): RefDataValue =>
  states.batchManagement.financialBatch.itemsPayment.itemList.sortBy;

const getFilterBy = (states: RootState): ItemsPaymentFilterBy =>
  states.batchManagement.financialBatch.itemsPayment.itemList.filterBy;

export const getBatchStatus = (states: RootState): RefDataValue[] =>
  states.batchManagement.financialBatch.filterBatch.batchStatus;

export const getBatchType = (states: RootState): RefDataValue[] =>
  states.batchManagement.financialBatch.filterBatch.batchType;

export const getSortBatch = (states: RootState): RefDataValue =>
  states.batchManagement.financialBatch.filterBatch.sortBy;

export const getOrderBatch = (states: RootState): RefDataValue =>
  states.batchManagement.financialBatch.filterBatch.orderBy;

export const getBatchList = (states: RootState): IBatch[] =>
  states.batchManagement.financialBatch.financialBatchList;

export const getBatchSearch = (states: RootState): AttributeSearchValue[] =>
  states.batchManagement.financialBatch.filterBatch.search;

export const selectorIsOpenModalAddBatch = createSelector(
  [
    (states: RootState): boolean =>
      states?.batchManagement?.financialBatch?.isOpenModal
  ],
  (data: boolean) => data
);

export const selectorIsLoadingBatch = createSelector(
  [
    (states: RootState): boolean =>
      states?.batchManagement?.financialBatch?.isLoading
  ],
  (data: boolean) => data
);

export const selectorFormValueBatch = createSelector(
  [
    (states: RootState): IBatch =>
      states?.batchManagement?.financialBatch?.formValue
  ],
  (data: IBatch) => data
);

export const selectedFormActionBatch = createSelector(
  [
    (states: RootState): FormAction =>
      states?.batchManagement?.financialBatch?.formAction
  ],
  (data: FormAction) => data
);

export const getFinancialBatchList = createSelector(
  [getBatchList, getBatchStatus, getBatchType, getOrderBatch, getSortBatch],
  (
    data: IBatch[],
    status: RefDataValue[],
    type: RefDataValue[],
    order: RefDataValue,
    sort: RefDataValue
  ) => filterBatch(data, status, type, order, sort)
);

export const selectorIsNoDataBatchList = createSelector(
  [getBatchList, getBatchSearch],
  (data: IBatch[], search: AttributeSearchValue[]) =>
    isEmpty(data) && isEmpty(search)
);

export const selectorIsSearchBatch = createSelector(
  [getBatchSearch],
  (search: AttributeSearchValue[]) => !isEmpty(search)
);

export const selectorSortBy = createSelector(
  getSortBy,
  (data: RefDataValue) => data
);

export const selectorOrderBy = createSelector(
  getOrderBy,
  (data: RefDataValue) => data
);

export const selectorFilterBy = createSelector(
  getFilterBy,
  (data: ItemsPaymentFilterBy) => data
);

export const selectorFinancialBatchDetails = createSelector(
  getFinancialBatchDetails,
  (data: IBatch) => data
);

export const selectorItemPaymentList = createSelector(
  getItemPaymentList,
  (data: ItemPaymentList[]) => data
);

export const selectorItemPaymentListFilterSortOrder = createSelector(
  getItemPaymentList,
  getSortBy,
  getOrderBy,
  getFilterBy,
  (
    data: ItemPaymentList[],
    sortBy: RefDataValue,
    orderBy: RefDataValue,
    filterBy: ItemsPaymentFilterBy
  ) => {
    let _data = [];

    if (!isEmpty(filterBy)) {
      _data = filterItemsPayment(data, filterBy);
    } else {
      _data = data;
    }

    if (!_data.length) [];

    if (!sortBy?.value) return _data;

    const dataAccNoLink = _data.filter(itemPayment => !itemPayment.accNumber);
    const dataAccLink = _data.filter(itemPayment => itemPayment.accNumber);

    return [
      ...sortOrderItems(dataAccLink, sortBy.value, orderBy.value),
      ...dataAccNoLink
    ];
  }
);

export const selectorSelectedItem = createSelector(
  getSelectedItem,
  (data: ItemPaymentList) => data
);

export const selectorSubitemsModal = createSelector(
  getSubitemsModal,
  (data: ModalSubitems) => data
);

export const selectedIsCollapseItem = createSelector(
  (states: RootState) => states.batchManagement.financialBatch.isCollapseItem,
  (data: boolean) => data
);

export const selectorBatchType = createSelector(
  [getBatchType],
  (data: RefDataValue[]) => data
);

export const selectorBatchStatus = createSelector(
  [getBatchStatus],
  (data: RefDataValue[]) => data
);

export const selectorOrderBatch = createSelector(
  [getOrderBatch],
  (data: RefDataValue) => data
);

export const selectorSortBatch = createSelector(
  [getSortBatch],
  (data: RefDataValue) => data
);

export const selectedBatchSearch = createSelector(
  (states: RootState) =>
    states.batchManagement.financialBatch.filterBatch.search,
  (data: AttributeSearchValue[]) => data
);
