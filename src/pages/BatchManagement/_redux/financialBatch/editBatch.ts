import { batch } from 'react-redux';
import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// redux
import { apiErrorDetailActions } from 'pages/__commons/ApiErrorDetail/_redux/reducer';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// constants & types
import {
  IBatch,
  InitialStateBatchManagement
} from 'pages/BatchManagement/types';
import { I18N_TEXT } from 'app/constants';
import { batchManagementActions } from '..';
import financialBatchServices from 'pages/BatchManagement/FinancialBatch/financialBatchServices';

const EDIT_BATCH = 'editBatch';

export const editBatch = createAsyncThunk<
  void,
  {
    id: string;
    dataChange: IBatch;
  },
  ThunkAPIConfig
>(EDIT_BATCH, async (args, { dispatch, rejectWithValue }) => {
  try {
    const { id, dataChange } = args;

    const { success } = await financialBatchServices.editBatch(id, dataChange);

    if (!success) throw new Error('Update Batch failed.');

    batch(() => {
      dispatch(batchManagementActions.onSetModalAddBatch({ isOpen: false }));
      dispatch(batchManagementActions.getFinancialBatch());
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.UPDATE_BATCH_SUCCESS
        })
      );
    });
  } catch (error) {
    if (error?.message === 'EDIT FAILED') {
      dispatch(
        apiErrorDetailActions.setErrValue({
          id: editBatch.rejected.type,
          value: 'Something when wrong when editing!'
        })
      );
    }

    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.UPDATE_BATCH_FAILED
      })
    );
    return rejectWithValue(error);
  }
});

export const editBatchBuilder = (
  builder: ActionReducerMapBuilder<InitialStateBatchManagement>
) => {
  const { pending, fulfilled, rejected } = editBatch;
  builder
    .addCase(pending, draftState => {
      draftState.financialBatch.isLoading = true;
    })
    .addCase(fulfilled, draftState => {
      draftState.financialBatch.isLoading = false;
    })
    .addCase(rejected, draftState => {
      draftState.financialBatch.isLoading = false;
    });
};
