import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { I18N_TEXT } from 'app/constants';

// redux
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { batch } from 'react-redux';
import { apiErrorDetailActions } from 'pages/__commons/ApiErrorDetail/_redux/reducer';

import {
  IBatch,
  InitialStateBatchManagement
} from 'pages/BatchManagement/types';

import { batchManagementActions } from '..';
import financialBatchServices from 'pages/BatchManagement/FinancialBatch/financialBatchServices';

const ADD_BATCH = 'addAssociatedDebtor';

export const addBatch = createAsyncThunk<
  void,
  {
    newBatch: IBatch;
  },
  ThunkAPIConfig
>(ADD_BATCH, async (args, { dispatch, rejectWithValue }) => {
  try {
    const { newBatch } = args;

    const { success } = await financialBatchServices.addBatch(newBatch);

    if (!success) throw new Error('Add Batch failed.');

    batch(() => {
      dispatch(batchManagementActions.onSetModalAddBatch({ isOpen: false }));
      dispatch(batchManagementActions.getFinancialBatch());
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.ADD_BATCH_SUCCESS
        })
      );
    });
  } catch (error) {
    if (error?.message === 'ADD FAILED') {
      dispatch(
        apiErrorDetailActions.setErrValue({
          id: addBatch.rejected.type,
          value: 'Something when wrong when adding!'
        })
      );
    }

    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.ADD_BATCH_FAILED
      })
    );
    return rejectWithValue(error);
  }
});

export const addBatchBuilder = (
  builder: ActionReducerMapBuilder<InitialStateBatchManagement>
) => {
  const { pending, fulfilled, rejected } = addBatch;
  builder
    .addCase(pending, draftState => {
      draftState.financialBatch.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      draftState.financialBatch.isLoading = false;
    })
    .addCase(rejected, draftState => {
      draftState.financialBatch.isLoading = false;
    });
};
