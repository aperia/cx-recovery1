import { PayloadAction } from '@reduxjs/toolkit';
import { set } from 'app/helpers';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { FormAction, IAccounts } from 'pages/AccountManagement/types';
import { TODAY } from 'pages/AgencyManagement/components/SaveAgencyRequestModal/constants';
import {
  AutoFeeOrSpread,
  orderItemsPaymentByDefault,
  TRANSACTION_TYPE_DEFAULT
} from 'pages/BatchManagement/constants';
import {
  ItemsPaymentFilterBy,
  IBatch,
  InitialStateBatchManagement,
  ItemPaymentList,
  ModalSubitems,
  Subitems
} from 'pages/BatchManagement/types';

export const onSetModalAddBatch = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<{ isOpen: boolean; formAction?: FormAction }>
) => {
  const { isOpen, formAction = 'ADD' } = action.payload;
  draftState.financialBatch.isOpenModal = isOpen;
  draftState.financialBatch.formAction = formAction;
};

export const setFinancialBatchDetails = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<IBatch>
) => {
  draftState.financialBatch.financialBatchDetails = action.payload;
};

export const onChangeSortAccounts = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<RefDataValue>
) => {
  draftState.financialBatch.itemsPayment.itemList.sortBy = action.payload;
};

export const onChangeOrderBy = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<RefDataValue>
) => {
  draftState.financialBatch.itemsPayment.itemList.orderBy = action.payload;
};

export const setFilterBy = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<ItemsPaymentFilterBy>
) => {
  draftState.financialBatch.itemsPayment.itemList.filterBy = action.payload;
};

export const triggerEditBatch = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<IBatch>
) => {
  draftState.financialBatch.isOpenModal = true;
  draftState.financialBatch.formValue = {
    ...action.payload,
    ...action.payload.itemsPayment[0]
  };
  draftState.financialBatch.itemsPayment.itemList.data =
    action.payload.itemsPayment;
  draftState.financialBatch.itemsPayment.selectedItem =
    action.payload.itemsPayment[0];
  draftState.financialBatch.formAction = 'EDIT';
};

export const onSelectedItem = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<ItemPaymentList>
) => {
  draftState.financialBatch.itemsPayment.selectedItem = action.payload;
};

export const setItemsPayment = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<ItemPaymentList[]>
) => {
  draftState.financialBatch.itemsPayment.itemList.data = action.payload;
};

export const clearItemPayment = (draftState: InitialStateBatchManagement) => {
  draftState.financialBatch.itemsPayment.itemList = {
    sortBy: {} as RefDataValue,
    orderBy: orderItemsPaymentByDefault,
    filterBy: {},
    data: [] as ItemPaymentList[]
  };
  draftState.financialBatch.itemsPayment.selectedItem = {};
};

export const linkAccountToItem = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<IAccounts>
) => {
  const account = action.payload;
  const selectedItem = draftState.financialBatch.itemsPayment.selectedItem;
  const itemList = draftState.financialBatch.itemsPayment.itemList.data;

  if (
    account?.accNumber.accountNumberUnMask !==
    selectedItem.accNumber?.accountNumberUnMask
  ) {
    const _itemList = itemList;

    const index = _itemList.findIndex(
      item => item.itemNumber === selectedItem.itemNumber
    );

    const _selectedItem = {
      ...selectedItem,
      debtorName: account.debtorName,
      accNumber: account.accNumber,
      transactionDate: selectedItem.transactionDate || TODAY,
      transactionType: selectedItem.transactionType || TRANSACTION_TYPE_DEFAULT,
      autoFee: selectedItem.autoFee || AutoFeeOrSpread.YES,
      autoSpread: selectedItem.autoSpread || AutoFeeOrSpread.YES,
      source: selectedItem.source || '',
      serviceFee: selectedItem.serviceFee || '',
      manualAgencyFee: selectedItem.manualAgencyFee || '',
      isTouched: false
    };
    _itemList[index] = _selectedItem;
    draftState.financialBatch.itemsPayment.selectedItem = _selectedItem;
    draftState.financialBatch.itemsPayment.itemList.data = _itemList;
  }
};

export const setModalSubitemPayment = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<ModalSubitems>
) => {
  draftState.financialBatch.itemsPayment.modalSubitems = action.payload;
};

export const removeModalSubitemPayment = (
  draftState: InitialStateBatchManagement
) => {
  draftState.financialBatch.itemsPayment.modalSubitems = {};
};

export const addSubitemPayment = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<Subitems>
) => {
  const _selectedItem = draftState.financialBatch.itemsPayment.selectedItem;

  // set subitem to selecedItems
  if (_selectedItem.subitems && _selectedItem.subitems.length) {
    _selectedItem.subitems.push(action.payload);
    _selectedItem.totalToPayAmount = _selectedItem.subitems
      .reduce((val, subitem) => val + parseInt(subitem.toPayAmount || '0'), 0)
      .toString();
  } else {
    set(_selectedItem, 'subitems', [action.payload]);
    _selectedItem.totalToPayAmount = action.payload.toPayAmount || '';
  }
  draftState.financialBatch.itemsPayment.selectedItem = _selectedItem;

  // map subitems to itemList
  const indexItemList =
    draftState.financialBatch.itemsPayment.itemList.data.findIndex(
      item => item.itemNumber === _selectedItem.itemNumber
    );
  draftState.financialBatch.itemsPayment.itemList.data[indexItemList] =
    _selectedItem;
};

export const editSubitemPayment = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<Subitems>
) => {
  const { subitemID } = action.payload;
  const _selectedItem = draftState.financialBatch.itemsPayment.selectedItem;

  if (_selectedItem.subitems) {
    // map subitem to selected items
    const subitemsSelectedItem = _selectedItem.subitems;
    const indexSubitemsSelectedItem = subitemsSelectedItem.findIndex(
      subitem => subitem.subitemID === subitemID
    );
    _selectedItem.subitems[indexSubitemsSelectedItem] = action.payload;

    _selectedItem.totalToPayAmount = _selectedItem.subitems
      .reduce((val, subitem) => val + parseInt(subitem.toPayAmount || '0'), 0)
      .toString();

    draftState.financialBatch.itemsPayment.selectedItem = _selectedItem;

    // map subitems to itemList
    const indexItemList =
      draftState.financialBatch.itemsPayment.itemList.data.findIndex(
        item => item.itemNumber === _selectedItem.itemNumber
      );
    draftState.financialBatch.itemsPayment.itemList.data[indexItemList] =
      _selectedItem;
  }
};

export const removeSubitemPayment = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<Subitems>
) => {
  const { subitemID } = action.payload;
  const _selectedItem = draftState.financialBatch.itemsPayment.selectedItem;

  if (_selectedItem.subitems) {
    // map subitem to selected items
    const subitemsSelectedItem = _selectedItem.subitems;
    const indexSubitemsSelectedItem = subitemsSelectedItem.findIndex(
      subitem => subitem.subitemID === subitemID
    );
    _selectedItem.subitems?.splice(indexSubitemsSelectedItem, 1);

    _selectedItem.totalToPayAmount = _selectedItem.subitems
      .reduce((val, subitem) => val + parseInt(subitem.toPayAmount || '0'), 0)
      .toString();

    draftState.financialBatch.itemsPayment.selectedItem = _selectedItem;

    // map subitems to itemList
    const indexItemList =
      draftState.financialBatch.itemsPayment.itemList.data.findIndex(
        item => item.itemNumber === _selectedItem.itemNumber
      );
    draftState.financialBatch.itemsPayment.itemList.data[indexItemList] =
      _selectedItem;
  }
};

export const onChangeCollapseItem = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<{ isCollapse: boolean }>
) => {
  const { isCollapse } = action.payload;
  draftState.financialBatch.isCollapseItem = isCollapse;
};

export const onChangeBatchType = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<{ data: RefDataValue[] }>
) => {
  const { data } = action.payload;

  draftState.financialBatch.filterBatch.batchType = data!;
};

export const onChangeBatchStatus = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<{ data: RefDataValue[] }>
) => {
  const { data } = action.payload;

  draftState.financialBatch.filterBatch.batchStatus = data!;
};

export const onChangeBatchSort = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<{ data: RefDataValue }>
) => {
  const { data } = action.payload;

  draftState.financialBatch.filterBatch.sortBy = data!;
};

export const onChangeBatchOrder = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<{ data: RefDataValue }>
) => {
  const { data } = action.payload;
  draftState.financialBatch.filterBatch.orderBy = data!;
};

export const onChangeBatchSearch = (
  draftState: InitialStateBatchManagement,
  action: PayloadAction<{ data: AttributeSearchValue[] }>
) => {
  const { data } = action.payload;
  draftState.financialBatch.filterBatch.search = data;
};
