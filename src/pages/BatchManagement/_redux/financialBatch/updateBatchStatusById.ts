import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// redux
import { apiErrorDetailActions } from 'pages/__commons/ApiErrorDetail/_redux/reducer';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

// constants & types
import {
  BatchStatus,
  IBatch,
  InitialStateBatchManagement
} from 'pages/BatchManagement/types';
import { I18N_TEXT } from 'app/constants';
import { batchManagementActions } from '..';
import financialBatchServices from 'pages/BatchManagement/FinancialBatch/financialBatchServices';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

const UPDATE_BATCH_STATUS_BY_ID = 'updateBatchStatusById';

export const updateBatchStatusById = createAsyncThunk<
  void,
  {
    id: string;
    batchStatus: BatchStatus;
  },
  ThunkAPIConfig
>(
  UPDATE_BATCH_STATUS_BY_ID,
  async (args, { getState, dispatch, rejectWithValue }) => {
    try {
      const { id, batchStatus } = args;

      const { success } = await financialBatchServices.updateBatchStatusById(
        id,
        batchStatus
      );
      const isCollapseBatch =
        getState().batchManagement.financialBatch.isCollapseItem;

      if (!success) throw new Error('Update Batch failed.');

      dispatch(rootModalActions.close());

      if (!isCollapseBatch) {
        // check if open flyout => set financialBatchDetails to update batchStatus
        await dispatch(batchManagementActions.getFinancialBatch());
        const batchList: IBatch[] =
          getState().batchManagement.financialBatch.financialBatchList;
        const snapshotItem: IBatch =
          getState().batchManagement.financialBatch.financialBatchDetails;
        const item = batchList.find(
          batch => batch.batchNumber === snapshotItem.batchNumber
        ) as IBatch;

        if (item.batchStatus !== snapshotItem.batchStatus) {
          dispatch(
            batchManagementActions.setFinancialBatchDetails(item as IBatch)
          );
        }
      } else {
        dispatch(batchManagementActions.getFinancialBatch());
      }

      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.BATCH_STATUS_UPDATE_SUCCESS
        })
      );
    } catch (error) {
      if (error?.message === 'EDIT FAILED') {
        dispatch(
          apiErrorDetailActions.setErrValue({
            id: updateBatchStatusById.rejected.type,
            value: 'Something when wrong when editing!'
          })
        );
      }

      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_TEXT.BATCH_STATUS_UPDATE_FAILED
        })
      );
      return rejectWithValue(error);
    }
  }
);

export const updateBatchStatusByIdBuilder = (
  builder: ActionReducerMapBuilder<InitialStateBatchManagement>
) => {
  const { pending, fulfilled, rejected } = updateBatchStatusById;
  builder
    .addCase(pending, draftState => {
      draftState.financialBatch.isLoading = true;
    })
    .addCase(fulfilled, draftState => {
      draftState.financialBatch.isLoading = false;
    })
    .addCase(rejected, draftState => {
      draftState.financialBatch.isLoading = false;
    });
};
