import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import financialBatchServices from 'pages/BatchManagement/FinancialBatch/financialBatchServices';
import {
  IBatch,
  InitialStateBatchManagement
} from 'pages/BatchManagement/types';

const GET_FINANCIAL_BATCH = 'getFinancialBatch';

interface IPayloadGetFinancialBatch {
  financialBatch: IBatch[];
}
export const getFinancialBatch = createAsyncThunk<
  IPayloadGetFinancialBatch,
  undefined,
  ThunkAPIConfig
>(GET_FINANCIAL_BATCH, async (_, thunkAPI) => {
  try {
    const { data } = await financialBatchServices.getFinancialBatch();

    return {
      financialBatch: data.financialBatch
    } as IPayloadGetFinancialBatch;
  } catch (error) {
    return thunkAPI.rejectWithValue({ response: error });
  }
});

export const getFinancialBatchBuilder = (
  builder: ActionReducerMapBuilder<InitialStateBatchManagement>
) => {
  const { pending, fulfilled, rejected } = getFinancialBatch;
  builder
    .addCase(pending, draftState => {
      draftState.financialBatch.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      const { financialBatch } = action.payload;

      draftState.financialBatch.financialBatchList = financialBatch;
      draftState.financialBatch.isLoading = false;
    })
    .addCase(rejected, draftState => {
      draftState.financialBatch.isLoading = false;
    });
};
