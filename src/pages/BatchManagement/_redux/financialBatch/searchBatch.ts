import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import financialBatchServices from 'pages/BatchManagement/FinancialBatch/financialBatchServices';
import { InitialStateBatchManagement } from 'pages/BatchManagement/types';

export const searchBatch = createAsyncThunk<any, undefined, ThunkAPIConfig>(
  'searchBatchList',
  async (_, thunkAPI) => {
    try {
      const { search } =
        thunkAPI.getState().batchManagement.financialBatch.filterBatch;
      const { data }: any = await financialBatchServices.searchBatch(search);

      return { data };
    } catch (error) {
      return thunkAPI.rejectWithValue({ response: error });
    }
  }
);

export const getSearchBatchBuilder = (
  builder: ActionReducerMapBuilder<InitialStateBatchManagement>
) => {
  const { pending, fulfilled, rejected } = searchBatch;
  builder
    .addCase(pending, draftState => {})
    .addCase(fulfilled, (draftState, action) => {
      const { data } = action.payload;

      draftState.financialBatch.financialBatchList = data;
    })
    .addCase(rejected, draftState => {});
};
