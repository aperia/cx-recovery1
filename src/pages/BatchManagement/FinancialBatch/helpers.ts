import { ItemsPaymentFilterBy, IBatch, ItemPaymentList } from '../types';
import {
  pick,
  pickBy,
  identity,
  isEmpty,
  formatTime,
  isArray,
  isNaN,
  orderBy
} from 'app/helpers';
import {
  BATCH_TYPE_PAYMENTS,
  FILTER_ITEM_PAYMENT_ENTRY_FIELD,
  TRANSACTION_TYPE
} from '../constants';
import { DROPDOWN_SORT_ITEMS_PAYMENT } from 'app/constants';

export const prepareBatchDataSubmit = (data: IBatch) => {
  let formatValue;
  if (data.createdBy === 'O' && isEmpty(data.sourceId))
    formatValue = pickBy(
      pick(data, [
        'batchNumber',
        'batchDate',
        'createdBy',
        'userId',
        'batchStatus',
        'batchType',
        'numberOfItems',
        'transactionAmount',
        'batchItemAmount',
        'batchItemType',
        'currentItemAmount',
        'currentItemType',
        'itemsPayment'
      ]),
      identity
    );
  if (data.createdBy === 'S') {
    formatValue = pickBy(
      pick(data, [
        'batchNumber',
        'batchDate',
        'createdBy',
        'sourceId',
        'batchStatus',
        'batchType',
        'numberOfItems',
        'transactionAmount',
        'batchItemAmount',
        'batchItemType',
        'currentItemAmount',
        'currentItemType',
        'itemsPayment'
      ]),
      identity
    );
  }
  const currentAmount = sumbArrObjPropertyValue(
    data.itemsPayment,
    'transactionAmountPaymentEntry'
  );
  const isMatch =
    data.itemsPayment?.length >= Number(data.numberOfItems) &&
    Number(currentAmount) >= Number(data.transactionAmount) &&
    data.numberOfItems &&
    data.transactionAmount;

  return {
    ...formatValue,
    batchDate: formatTime((formatValue?.['batchDate'] as string) || '')?.date,
    isMatch,
    currentItemType:
      formatValue?.['batchType'] === BATCH_TYPE_PAYMENTS
        ? (formatValue?.['itemsPayment'] as ItemPaymentList[]).length
        : '',
    currentItemAmount:
      formatValue?.['batchType'] === BATCH_TYPE_PAYMENTS ? '9999' : ''
  } as IBatch;
};

export const getTotalCurrentAmount = (listItem: ItemPaymentList[]) => {
  if (!listItem || !isArray(listItem)) return '';
  const result = listItem.reduce(
    (acc, curr) => (acc += Number(curr?.transactionAmountPaymentEntry)),
    0
  );
  if (isNaN(result)) return '';
  return result.toString();
};

export const sumbArrObjPropertyValue = (
  arr: Array<MagicKeyValue>,
  key: string
) => {
  return arr
    .reduce((val, obj) => val + parseInt(obj[key] || '0'), 0)
    .toString();
};

export const mapTransactionType = (transactionType?: string) => {
  switch (transactionType) {
    case TRANSACTION_TYPE.G.value:
      return TRANSACTION_TYPE.G.description;

    case TRANSACTION_TYPE.N.value:
      return TRANSACTION_TYPE.N.description;

    case TRANSACTION_TYPE.S.value:
      return TRANSACTION_TYPE.S.description;
    case TRANSACTION_TYPE.T.value:
      return TRANSACTION_TYPE.T.description;

    default:
      return TRANSACTION_TYPE.D.description;
  }
};

export const sortOrderItems = (
  data: ItemPaymentList[],
  sort: string,
  order: string
) => {
  if (!data.length) [];
  if (sort === DROPDOWN_SORT_ITEMS_PAYMENT[2].value)
    return orderBy(
      data,
      [obj => new Date(obj['transactionDate'] || '')],
      order as 'asc' | 'desc'
    );
  else if (sort === DROPDOWN_SORT_ITEMS_PAYMENT[0].value) {
    return orderBy(
      data,
      [
        obj =>
          parseInt(obj.transactionAmountPaymentEntry || '0') +
          parseInt(obj.totalToPayAmount || '0')
      ],
      order as 'asc' | 'desc'
    );
  }
  return orderBy(data, sort, order as 'asc' | 'desc');
};

export const filterItemsPayment = (
  data: ItemPaymentList[],
  filterBy: ItemsPaymentFilterBy
) => {
  const _data = data?.filter(itemPayment => {
    if (!itemPayment.accNumber) {
      return true;
    }
    return Object.keys(filterBy).every(key => {
      if (key === FILTER_ITEM_PAYMENT_ENTRY_FIELD.DEBTOR_NAME.name) {
        return itemPayment.debtorName
          ?.toLowerCase()
          .includes(filterBy.debtorName?.toLowerCase() || '');
      }
      if (key === FILTER_ITEM_PAYMENT_ENTRY_FIELD.TRANS_TYPE.name) {
        return filterBy.transactionType?.some(
          (val: string) => itemPayment.transactionType === val
        );
      }
      if (key === FILTER_ITEM_PAYMENT_ENTRY_FIELD.DATE.name) {
        return (
          itemPayment.transactionDate &&
          new Date(itemPayment.transactionDate).getTime() ===
            filterBy.date?.getTime()
        );
      }
      if (key === FILTER_ITEM_PAYMENT_ENTRY_FIELD.DATE_RANGE.name) {
        return (
          itemPayment.transactionDate &&
          filterBy.dateRange?.start &&
          filterBy.dateRange?.end &&
          new Date(itemPayment.transactionDate).getTime() >=
            filterBy.dateRange.start.getTime() &&
          new Date(itemPayment.transactionDate).getTime() <=
            filterBy.dateRange.end.getTime()
        );
      }
      return itemPayment?.accNumber?.accountNumberUnMask?.includes(
        filterBy?.accountNumber || ''
      );
    });
  });
  return _data;
};

export const filterBatch = (
  data: IBatch[],
  status: RefDataValue[],
  type: RefDataValue[],
  order: RefDataValue,
  sort: RefDataValue
): IBatch[] => {
  const typeValues = type.map(item => item.value);
  const statusValues = status.map(item => item.value);
  const filterData = data.filter(
    item =>
      typeValues.includes(item.batchType) &&
      statusValues.includes(item.batchStatus)
  );
  return orderBy(
    filterData,
    sort.value,
    order?.value?.toLowerCase() as 'asc' | 'desc'
  );
};
