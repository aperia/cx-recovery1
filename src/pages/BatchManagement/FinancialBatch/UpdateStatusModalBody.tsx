import React from 'react';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_TEXT } from 'app/constants';

interface UpdateStatusModalBodyProps {
  nextStatus: string;
}
const UpdateStatusModalBody: React.FC<UpdateStatusModalBodyProps> = ({
  nextStatus = ''
}) => {
  const { t } = useTranslation();
  return (
    <p>{t(I18N_TEXT.CONFIRM_UPDATE_BATCH_STATUS_MESSAGE, { nextStatus })}</p>
  );
};

export default UpdateStatusModalBody;
