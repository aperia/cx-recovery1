import React from 'react';

import { SimpleBar } from 'app/_libraries/_dls/components';
import SearchAccountNumber from 'pages/AccountManagement/ManualAssignRecall/Form/SearchAccountNumber';

import { useTranslation } from 'app/_libraries/_dls/hooks';

import { I18N_TEXT } from 'app/constants';

interface ItemDetailsProps {
  isSearch?: boolean;
}
export const ItemDetails: React.FC<ItemDetailsProps> = ({
  isSearch = true
}) => {
  const { t } = useTranslation();
  const onSearchByAcc = () => {};

  return (
    <>
      <SimpleBar>
        <h4>{t(I18N_TEXT.ITEM_DETAILS)}</h4>
        {isSearch && (
          <div className="d-flex max-width-lg mx-auto">
            <SearchAccountNumber
              className="max-width-lg mx-auto px-24 py-16 flex-1"
              searchAcc={{} as any}
              onSearch={onSearchByAcc}
            />
            <div className="w-408px w-md-0"></div>
          </div>
        )}
      </SimpleBar>
    </>
  );
};
