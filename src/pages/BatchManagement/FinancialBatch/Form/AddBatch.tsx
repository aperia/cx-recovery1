import React, { useCallback, useMemo } from 'react';
import { batch, useDispatch } from 'react-redux';

// components
import {
  TextBoxControl,
  DatePickerControl,
  DropdownControl
} from 'app/components';
import { Icon } from 'app/_libraries/_dls/components';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';
import { DropdownWithVariant } from './DropdownWithVariant';

// redux
import { batchManagementActions } from 'pages/BatchManagement/_redux';

// hooks
import useRefData from 'app/hooks/useRefData';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useItemsPaymentForm } from '../hooks/useItemsPaymentForm';

// constants & types
import {
  I18N_TEXT,
  MAX_LENGTH_17,
  MAX_LENGTH_3,
  MAX_LENGTH_8,
  NUMERIC_REPLACE_REGEX
} from 'app/constants';
import {
  BATCH_TYPE_PAYMENTS,
  FINANCIAL_BATCH_FORM,
  INIT_PAYMENT_ENTRY_FORM_VALUE,
  CREATED_BY
} from 'pages/BatchManagement/constants';

// helpers
import { formatCurrency, isEmpty } from 'app/helpers';
import { sumbArrObjPropertyValue } from '../helpers';

interface AddBatchProps {}
export const AddBatch: React.FC<AddBatchProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const createdByOptions = useRefData('createdBy');
  const sourceIdOptions = useRefData('sourceId');
  const batchStatusOptions = useRefData('batchStatus');
  const batchTypeOptions = useRefData('batchType');

  const { itemList, values, handleChange } = useItemsPaymentForm();

  const defaultUserIdOption = useMemo(() => {
    return [
      {
        value: values.userId || '',
        description: values.userId || '',
        show: true
      }
    ];
  }, [values.userId]);

  const handleChangBathType = useCallback(
    (e: DropdownBaseChangeEvent) => {
      const currentValue = e.target.value;

      if (currentValue.value === BATCH_TYPE_PAYMENTS && !itemList?.length) {
        const itemNumber = Math.floor(Math.random() * 1000).toString();
        batch(() => {
          dispatch(
            batchManagementActions.setItemsPayment([
              { ...INIT_PAYMENT_ENTRY_FORM_VALUE, itemNumber }
            ])
          );
          dispatch(
            batchManagementActions.onSelectedItem({
              ...INIT_PAYMENT_ENTRY_FORM_VALUE,
              itemNumber
            })
          );
        });
      }
    },
    [dispatch, itemList]
  );

  const renderFieldBaseOnCreateBy = useCallback(
    (userId: string) => {
      if (values.createdBy === CREATED_BY.OPERATOR) {
        return (
          <DropdownWithVariant
            options={defaultUserIdOption}
            itemCodeValue={userId}
            label={FINANCIAL_BATCH_FORM.ADD_BATCH.userId.label}
            name={FINANCIAL_BATCH_FORM.ADD_BATCH.userId.name}
            className="col-4 col-lg-3 mb-16"
          />
        );
      }
      return (
        <DropdownControl
          name={FINANCIAL_BATCH_FORM.ADD_BATCH.sourceId.name}
          label={FINANCIAL_BATCH_FORM.ADD_BATCH.sourceId.label}
          className="col-4 col-lg-3 mb-16"
          options={sourceIdOptions}
          isCombine
          required
        />
      );
    },
    [defaultUserIdOption, sourceIdOptions, values.createdBy]
  );

  const isMatch = useMemo(() => {
    const currentValue = itemList?.length;
    const currentAmount = sumbArrObjPropertyValue(
      itemList,
      'transactionAmountPaymentEntry'
    );
    return (
      currentValue >= Number(values.numberOfItems) &&
      Number(currentAmount) >= Number(values.transactionAmount) &&
      values.numberOfItems &&
      values.transactionAmount
    );
  }, [itemList, values.numberOfItems, values.transactionAmount]);

  const currentAmountValue = useMemo(() => {
    return (
      Number(
        sumbArrObjPropertyValue(itemList, 'transactionAmountPaymentEntry')
      ) + Number(sumbArrObjPropertyValue(itemList, 'totalToPayAmount'))
    );
  }, [itemList]);

  const varianceValue = useMemo(() => {
    if (Number(values.numberOfItems) - itemList?.length < 0) return 0;
    return Number(values.numberOfItems) - itemList?.length;
  }, [itemList?.length, values.numberOfItems]);

  const varianceAmountValue = useMemo(() => {
    return (
      Number(values.transactionAmount) -
      Number(
        sumbArrObjPropertyValue(itemList, 'transactionAmountPaymentEntry')
      ) -
      Number(sumbArrObjPropertyValue(itemList, 'totalToPayAmount'))
    );
  }, [itemList, values.transactionAmount]);

  const renderBatchVerbiage = useMemo(() => {
    if (values.batchType === BATCH_TYPE_PAYMENTS && !!itemList?.length) {
      if (isMatch) {
        return (
          <>
            <Icon name="success" className="color-green-d16" />
            <span className="color-green-d16 pl-8">
              {t(I18N_TEXT.BATCH_AND_CURRENT_MATCHED)}
            </span>
          </>
        );
      }
      return (
        <>
          <Icon name="warning" className="color-orange-d16" />
          <span className="color-orange-d16 pl-8">
            {t(I18N_TEXT.BATCH_AND_CURRENT_NOT_MATCH)}
          </span>
        </>
      );
    }
  }, [t, isMatch, values.batchType, itemList]);

  return (
    <>
      <div className="row">
        <TextBoxControl
          name={FINANCIAL_BATCH_FORM.ADD_BATCH.batchNumber.name}
          label={FINANCIAL_BATCH_FORM.ADD_BATCH.batchNumber.label}
          className="col-4 col-lg-3 mb-16"
          maxLength={MAX_LENGTH_8}
          readOnly
        />

        <DatePickerControl
          name={FINANCIAL_BATCH_FORM.ADD_BATCH.batchDate.name}
          label={FINANCIAL_BATCH_FORM.ADD_BATCH.batchDate.label}
          className="col-4 col-lg-3 mb-16"
          required
        />

        <DropdownControl
          name={FINANCIAL_BATCH_FORM.ADD_BATCH.createdBy.name}
          label={FINANCIAL_BATCH_FORM.ADD_BATCH.createdBy.label}
          className="col-4 col-lg-3 mb-16"
          options={createdByOptions}
          required
        />

        {renderFieldBaseOnCreateBy(values?.userId || '')}

        <DropdownControl
          name={FINANCIAL_BATCH_FORM.ADD_BATCH.batchStatus.name}
          label={FINANCIAL_BATCH_FORM.ADD_BATCH.batchStatus.label}
          className="col-4 col-lg-3 mb-16"
          options={batchStatusOptions}
          readOnly
        />

        <DropdownControl
          name={FINANCIAL_BATCH_FORM.ADD_BATCH.batchType.name}
          label={FINANCIAL_BATCH_FORM.ADD_BATCH.batchType.label}
          className="col-4 col-lg-3 mb-16"
          options={batchTypeOptions}
          onValueChange={handleChangBathType}
          isCombine
          required
        />

        <TextBoxControl
          name={FINANCIAL_BATCH_FORM.ADD_BATCH.numberOfItems.name}
          label={FINANCIAL_BATCH_FORM.ADD_BATCH.numberOfItems.label}
          className="col-4 col-lg-3 mb-16"
          regex={NUMERIC_REPLACE_REGEX}
          maxLength={MAX_LENGTH_3}
          required
        />

        <FormikNumericControl
          name={FINANCIAL_BATCH_FORM.ADD_BATCH.transactionAmount.name}
          label={t(FINANCIAL_BATCH_FORM.ADD_BATCH.transactionAmount.label)}
          className="col-4 col-lg-3 mb-16"
          onValueChange={handleChange}
          maxLength={MAX_LENGTH_17}
          format="c2"
          required
        />
        <div className="col-12 mb-16">
          <div className="divider-dashed"></div>
        </div>

        <div className="col-lg-3 col-md-4">
          <p className="fw-500 mr-8">
            {t(I18N_TEXT.BATCH)}:
            <span className="mx-8 fw-400 content-empty">
              {values.numberOfItems}
            </span>
            <span className="border-left br-light px-8 fw-400 content-empty">
              {isEmpty(values.transactionAmount)
                ? ''
                : formatCurrency(values.transactionAmount)}
            </span>
          </p>
        </div>

        <div className="col-lg-3 col-md-4">
          <p className="fw-500 mr-8">
            {t(I18N_TEXT.CURRENT)}:
            <span className="mx-8 fw-400 content-empty">
              {values.batchType !== BATCH_TYPE_PAYMENTS || !itemList?.length
                ? ''
                : itemList?.length}
            </span>
            {!isEmpty(
              itemList.filter(
                itemPayment => itemPayment.accNumber?.accountNumber
              )
            ) && (
              <span className="border-left br-light px-8 fw-400 content-empty">
                {values.batchType !== BATCH_TYPE_PAYMENTS ||
                currentAmountValue === 0
                  ? ''
                  : formatCurrency(currentAmountValue.toString())}
              </span>
            )}
          </p>
        </div>
        <div className="col-xl-3 col-lg-2 col-md-4">
          <p className="fw-500 mr-8">
            {t(I18N_TEXT.VARIANCE)}:
            <span className="mx-8 fw-400 content-empty">
              {values.batchType !== BATCH_TYPE_PAYMENTS ||
              isEmpty(values.numberOfItems) ||
              !itemList?.length
                ? ''
                : varianceValue}
            </span>
            {!isEmpty(
              itemList.filter(
                itemPayment => itemPayment.accNumber?.accountNumber
              )
            ) && (
              <span className="border-left br-light px-8 fw-400 content-empty">
                {values.batchType !== BATCH_TYPE_PAYMENTS ||
                currentAmountValue === 0
                  ? ''
                  : formatCurrency(varianceAmountValue.toString())}
              </span>
            )}
          </p>
        </div>
        <div className="col-xl-3 col-lg-4 col-md-6 text-lg-right text-xl-left mt-md-16 mt-lg-0">
          {renderBatchVerbiage}
        </div>
      </div>
    </>
  );
};
