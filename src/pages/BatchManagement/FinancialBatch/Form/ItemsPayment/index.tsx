import React from 'react';

// components
import ItemDetails from './ItemPaymentDetails';
import ItemList from './ItemPaymentList';
import { SimpleBar } from 'app/_libraries/_dls/components';

// redux
import { useSelector } from 'app/hooks';
import { selectorItemPaymentListFilterSortOrder } from 'pages/BatchManagement/_redux/financialBatch/selectors';

// types
import { ItemPaymentList } from 'pages/BatchManagement/types';

export interface ItemsProps {}
const ItemsPayment: React.FC<ItemsProps> = () => {
  const itemListFilterSortOrder: ItemPaymentList[] = useSelector(
    selectorItemPaymentListFilterSortOrder
  );

  return (
    <div className="d-flex h-100">
      <div className="w-320px border-right">
        <SimpleBar>
          <ItemList />
        </SimpleBar>
      </div>
      <div className="flex-1">
        {!!itemListFilterSortOrder.length && (
          <SimpleBar>
            <ItemDetails />
          </SimpleBar>
        )}
      </div>
    </div>
  );
};

export default ItemsPayment;
