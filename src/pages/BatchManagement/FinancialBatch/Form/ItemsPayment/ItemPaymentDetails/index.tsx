import React, { useEffect, useState } from 'react';
import { batch, useDispatch } from 'react-redux';

// components
import { View } from 'app/_libraries/_dof/core';
import { Button } from 'app/_libraries/_dls/components';
import SearchAccountNumber from 'pages/AccountManagement/ManualAssignRecall/Form/SearchAccountNumber';
import PaymentEntry from './PaymentEntry';

// redux
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { batchManagementActions } from 'pages/BatchManagement/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { selectorSearchAcc } from 'pages/AccountManagement/_redux/manualAssignRecall/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useItemsPaymentForm } from 'pages/BatchManagement/FinancialBatch/hooks/useItemsPaymentForm';
import { useSelector } from 'app/hooks';

// constants & type
import { I18N_TEXT } from 'app/constants';
import { NoDataGrid } from 'app/components';
import { INIT_PAYMENT_ENTRY_FORM_VALUE } from 'pages/BatchManagement/constants';
import { IAccounts } from 'pages/AccountManagement/types';
import { ItemPaymentList } from 'pages/BatchManagement/types';

// helpers
import { isEmpty } from 'app/helpers';

export interface ItemDetailsProps {}
const ItemDetails: React.FC<ItemDetailsProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [isShowMore, setIsShowMore] = useState<boolean>(false);

  const searchAcc: IAccounts = useSelector(selectorSearchAcc);

  const {
    itemList,
    itemListFilterSortOrder,
    selectedItem,
    setValues,
    setTouched
  } = useItemsPaymentForm();

  const onSearchByAcc = ({ accNumber }: { accNumber: string }) => {
    if (selectedItem.accNumber?.accountNumber) {
      dispatch(
        rootModalActions.open({
          title: t(I18N_TEXT.CONFIRM_CHANGE_ACCOUNT),
          size: 'sm',
          body: (
            <>
              <span>
                {t(
                  I18N_TEXT.CHANGING_THE_ACCOUNT_NUMBER_WILL_ERASE_THE_INFORMATION_ENTERED_IN_PAYMENT_ENTRY
                )}
              </span>
              <span> {t(I18N_TEXT.ARE_YOU_WANT_TO_CHANGE_THE_ACCOUNT)}</span>
            </>
          ),
          btnConfirmText: t(I18N_TEXT.CHANGE_ACCOUNT),
          btnCancelText: t(I18N_TEXT.CANCEL),
          onConfirm: () => {
            dispatch(
              accountManagementActions.searchAccountByAcc({ accNumber })
            );

            // set init value form Payment Entry in object itemsPayment in formik
            const _itemList = [...itemList];
            const index = _itemList?.findIndex(
              (item: ItemPaymentList) =>
                item.itemNumber === selectedItem.itemNumber
            );
            const _item = {
              ..._itemList[index],
              ...INIT_PAYMENT_ENTRY_FORM_VALUE
            };
            _itemList[index] = _item;

            batch(() => {
              dispatch(batchManagementActions.onSelectedItem(_item));
              dispatch(batchManagementActions.setItemsPayment([..._itemList]));
            });

            setValues((prev: any) => ({
              ...prev,
              ...INIT_PAYMENT_ENTRY_FORM_VALUE
            }));
            setTouched({}, false);
          },
          onCancel: () => {
            dispatch(rootModalActions.close());
          }
        })
      );
    } else {
      dispatch(accountManagementActions.searchAccountByAcc({ accNumber }));
      setValues((prev: any) => ({
        ...prev,
        ...INIT_PAYMENT_ENTRY_FORM_VALUE
      }));
    }
  };

  // change selected item  => get infor account map to item selected
  useEffect(() => {
    if (selectedItem.accNumber) {
      dispatch(
        accountManagementActions.searchAccountByAcc({
          accNumber: selectedItem.accNumber?.accountNumberUnMask
        })
      );
    } else {
      dispatch(accountManagementActions.onSearchByAccReset());
    }
  }, [dispatch, selectedItem.accNumber]);

  // link account to item in item list
  useEffect(() => {
    !isEmpty(searchAcc) &&
      dispatch(batchManagementActions.linkAccountToItem(searchAcc));
  }, [dispatch, searchAcc]);

  useEffect(() => {
    // for case filter Items Payment
    // check selectedItem is in itemListFilterSortOrder
    // if not selectedItem = itemListFilterSortOrder[0]
    const isSelectedItem = itemListFilterSortOrder.some(
      itemFilterSortOrder =>
        itemFilterSortOrder.accNumber?.accountNumberUnMask ===
        selectedItem.accNumber?.accountNumberUnMask
    );
    if (!isSelectedItem && selectedItem.accNumber) {
      dispatch(
        batchManagementActions.onSelectedItem(itemListFilterSortOrder[0])
      );
    }
  }, [dispatch, itemListFilterSortOrder, , selectedItem.accNumber]);

  return (
    <div className="p-24 max-width-lg mx-auto">
      <h4>{t(I18N_TEXT.ITEM_DETAILS)}</h4>
      <h5 className="mt-24 mb-16">{t(I18N_TEXT.LINKED_ACCOUNT)}</h5>
      <SearchAccountNumber searchAcc={searchAcc} onSearch={onSearchByAcc} />
      {isEmpty(searchAcc) ? (
        <NoDataGrid
          text={t(
            I18N_TEXT.ENTER_ACCOUNT_NUMBER_OF_FIND_ACCOUNT_TO_PROCEED_WITH_THE_FORM
          )}
        />
      ) : (
        <>
          <div className="bg-light-l16 br-radius-8 p-16 mt-16">
            <div className="d-flex justify-content-between align-items-center">
              <h5>{searchAcc.debtorName}</h5>
              <Button
                onClick={() => setIsShowMore(!isShowMore)}
                size="sm"
                variant="outline-primary"
                className="mr-n8 text-capitalize"
              >
                {isShowMore ? t(I18N_TEXT.SHOW_LESS) : t(I18N_TEXT.SHOW_MORE)}
              </Button>
            </div>
            <View
              id={`accountItemDetailsPayment`}
              formKey={`accountItemDetailsPayment`}
              descriptor="accountItemDetailsPayment"
              value={searchAcc}
            ></View>

            {isShowMore && (
              <View
                id={`accountItemDetailsPaymentMoreInfo`}
                formKey={`accountItemDetailsPaymentMoreInfo`}
                descriptor="accountItemDetailsPaymentMoreInfo"
                value={searchAcc}
              ></View>
            )}
          </div>
          <PaymentEntry />
        </>
      )}
    </div>
  );
};

export default ItemDetails;
