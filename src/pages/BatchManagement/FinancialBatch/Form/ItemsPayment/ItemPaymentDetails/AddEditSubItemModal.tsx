import React, { useCallback } from 'react';
import { FormikProvider } from 'formik';

// components
import { DropdownControl } from 'app/components';
import {
  Modal,
  ModalBody,
  ModalHeader,
  ModalTitle,
  ModalFooter
} from 'app/_libraries/_dls/components';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useRefData from 'app/hooks/useRefData';
import { useAddEditSubItemModal } from '../../../hooks/useAddEditSubItemModal';

// helpers
import { formatCurrency } from 'app/helpers';

// constants
import { ITEM_PAYMENT_ENTRY_FIELD } from 'pages/BatchManagement/constants';
import { I18N_TEXT, MAX_LENGTH_15 } from 'app/constants';

export interface AddEditSubItemModalProps {}
const AddEditSubItemModal: React.FC<AddEditSubItemModalProps> = () => {
  const { t } = useTranslation();

  const ftcOptions = useRefData('ftc');

  const {
    formik,
    subitemsModal,
    isAddModal,
    onCloseModal,
    mapPOICandBalanceAmount
  } = useAddEditSubItemModal();

  const { values, isSubmitting, isValid, handleChange } = formik;

  const renderPOIC = useCallback(() => {
    const { poic, balanceAmount } = mapPOICandBalanceAmount(values.ftc);
    return (
      <>
        {poic ? (
          <p>{formatCurrency(poic)}</p>
        ) : (
          <p className="content-empty"></p>
        )}
        {balanceAmount ? (
          <p className="mt-8">{formatCurrency(balanceAmount)}</p>
        ) : (
          <p className="content-empty mt-8"></p>
        )}
      </>
    );
  }, [mapPOICandBalanceAmount, values.ftc]);

  const renderAddSubitem = useCallback(() => {
    return (
      <>
        <DropdownControl
          name={ITEM_PAYMENT_ENTRY_FIELD.FTC.name}
          label={t(ITEM_PAYMENT_ENTRY_FIELD.FTC.label)}
          className=""
          options={ftcOptions}
          required
          isCombine
        />
        <div className="bg-light-l20 border br-light-l04 br-radius-8 p-16 my-16">
          <div className="d-flex">
            <div className="mr-24">
              <p className="fw-500 color-grey">{t(I18N_TEXT.POIC)}:</p>
              <p className="fw-500 color-grey mt-8">
                {t(I18N_TEXT.BALANCE_AMOUNT)}:
              </p>
            </div>
            <span className="flex-1">{renderPOIC()}</span>
          </div>
        </div>
        <FormikNumericControl
          name={ITEM_PAYMENT_ENTRY_FIELD.TO_PAY_AMOUNT.name}
          label={t(ITEM_PAYMENT_ENTRY_FIELD.TO_PAY_AMOUNT.label)}
          maxLength={MAX_LENGTH_15}
          onValueChange={handleChange}
          required
        />
        <p className="fs-12 color-grey mt-10">
          {t(
            I18N_TEXT.TO_PAY_AMOUNT_MUST_BE_LESS_THAN_OR_EQUAL_TO_BALANCE_AMOUNT
          )}
        </p>
      </>
    );
  }, [t, ftcOptions, renderPOIC, handleChange]);

  return (
    <FormikProvider value={formik}>
      <Modal xs show={!!subitemsModal.isOpenModal} loading={false}>
        <ModalHeader
          border
          onHide={onCloseModal}
          closeButton
          className="border-bottom"
        >
          <ModalTitle>
            {isAddModal ? t(I18N_TEXT.ADD_SUBITEM) : t(I18N_TEXT.EDIT_SUBITEM)}
          </ModalTitle>
        </ModalHeader>
        <ModalBody>{renderAddSubitem()}</ModalBody>
        <ModalFooter
          cancelButtonText={t(I18N_TEXT.CANCEL)}
          okButtonText={isAddModal ? t(I18N_TEXT.SUBMIT) : t(I18N_TEXT.SAVE)}
          onCancel={onCloseModal}
          onOk={formik.submitForm}
          disabledOk={isSubmitting || !isValid}
        ></ModalFooter>
      </Modal>
    </FormikProvider>
  );
};
export default AddEditSubItemModal;
