import React, { useCallback } from 'react';

// components
import {
  DatePickerControl,
  DropdownControl,
  TextBoxControl
} from 'app/components';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useRefData from 'app/hooks/useRefData';
import { useItemsPaymentForm } from '../../../hooks/useItemsPaymentForm';

// constants
import {
  I18N_TEXT,
  MAX_LENGTH_15,
  MAX_LENGTH_25,
  MAX_LENGTH_6
} from 'app/constants';
import {
  AutoFeeOrSpread,
  ITEM_PAYMENT_ENTRY_FIELD
} from 'pages/BatchManagement/constants';

export interface GeneralInformationProps {}
const GeneralInformation: React.FC<GeneralInformationProps> = ({}) => {
  const { t } = useTranslation();

  const transactionTypeOptions = useRefData('transactionType');
  const autoFeeOrSpreadOptions = useRefData('autoFeeOrSpreadOptions');

  const { values, selectedItem, setValues, handleChangeFormField } =
    useItemsPaymentForm();

  const handleChangeAutoFree = useCallback(
    (event: React.ChangeEvent<HTMLInputElement>) => {
      const valueAutoFree = (event.target as unknown as { value: RefDataValue })
        ?.value?.value;

      // clear manualAgencyFee when value autoFree = Yes
      if (valueAutoFree === AutoFeeOrSpread.YES) {
        setValues((prev: any) => ({
          ...prev,
          manualAgencyFee: ''
        }));
      }
      handleChangeFormField(event);
    },
    [handleChangeFormField, setValues]
  );

  return (
    <div className="mt-16">
      <h6 className="color-grey">{t(I18N_TEXT.GENERAL_INFORMATION)}</h6>
      <div className="row">
        <DatePickerControl
          name={ITEM_PAYMENT_ENTRY_FIELD.TRANSACTION_DATE.name}
          label={t(ITEM_PAYMENT_ENTRY_FIELD.TRANSACTION_DATE.label)}
          className="col-md-6 col-lg-4 col-xl-3 mt-16"
          onValueChange={handleChangeFormField}
          forceUpdateValue
          required
        />
        <DropdownControl
          name={ITEM_PAYMENT_ENTRY_FIELD.TRANSACTION_TYPE.name}
          label={t(ITEM_PAYMENT_ENTRY_FIELD.TRANSACTION_TYPE.label)}
          className="col-md-6 col-lg-4 col-xl-3 mt-16"
          options={transactionTypeOptions}
          onValueChange={handleChangeFormField}
          required
          isCombine
        />
        <TextBoxControl
          name={ITEM_PAYMENT_ENTRY_FIELD.SOURCE.name}
          label={t(ITEM_PAYMENT_ENTRY_FIELD.SOURCE.label)}
          className="col-md-12 col-lg-4 col-xl-3 mt-16"
          onValueChange={handleChangeFormField}
          forceUpdateValue
          maxLength={MAX_LENGTH_6}
        />
        <TextBoxControl
          name={ITEM_PAYMENT_ENTRY_FIELD.REFFERENCE.name}
          label={t(ITEM_PAYMENT_ENTRY_FIELD.REFFERENCE.label)}
          className="col-md-12 col-lg-4 col-xl-3 mt-16"
          onValueChange={handleChangeFormField}
          forceUpdateValue
          maxLength={MAX_LENGTH_25}
        />
        <DatePickerControl
          name={ITEM_PAYMENT_ENTRY_FIELD.NEXT_SCHEDULED_PAYMENT_DATE.name}
          label={t(ITEM_PAYMENT_ENTRY_FIELD.NEXT_SCHEDULED_PAYMENT_DATE.label)}
          className="col-md-12 col-lg-4 col-xl-3 mt-16"
          forceUpdateValue
          hideIcon={true}
          readOnly
        />
        <TextBoxControl
          name={ITEM_PAYMENT_ENTRY_FIELD.SENDING_AGENCY.name}
          label={t(ITEM_PAYMENT_ENTRY_FIELD.SENDING_AGENCY.label)}
          className="col-md-6 col-lg-4 col-xl-3 mt-16"
          onValueChange={handleChangeFormField}
          forceUpdateValue
          readOnly
          maxLength={MAX_LENGTH_25}
        />
        <FormikNumericControl
          name={ITEM_PAYMENT_ENTRY_FIELD.SERVICE_FEE.name}
          label={t(ITEM_PAYMENT_ENTRY_FIELD.SERVICE_FEE.label)}
          className="col-md-6 col-lg-4 col-xl-3 mt-16"
          onValueChange={handleChangeFormField}
          maxLength={MAX_LENGTH_15}
        />
        <FormikNumericControl
          name={ITEM_PAYMENT_ENTRY_FIELD.CASH.name}
          label={t(ITEM_PAYMENT_ENTRY_FIELD.CASH.label)}
          className="col-md-6 col-lg-4 col-xl-3 mt-16"
          onValueChange={handleChangeFormField}
          maxLength={MAX_LENGTH_15}
          readOnly
        />
        <DropdownControl
          name={ITEM_PAYMENT_ENTRY_FIELD.AUTO_FEE.name}
          label={t(ITEM_PAYMENT_ENTRY_FIELD.AUTO_FEE.label)}
          className="col-md-6 col-lg-4 col-xl-3 mt-16"
          options={autoFeeOrSpreadOptions}
          onValueChange={handleChangeAutoFree}
          required
        />
        <FormikNumericControl
          name={ITEM_PAYMENT_ENTRY_FIELD.MANUAL_AGENCY_FEE.name}
          label={t(ITEM_PAYMENT_ENTRY_FIELD.MANUAL_AGENCY_FEE.label)}
          className="col-md-12 col-lg-4 col-xl-3 mt-16"
          onValueChange={handleChangeFormField}
          maxLength={MAX_LENGTH_15}
          forceShowBorderError={
            selectedItem.isTouched &&
            values.autoFee !== autoFeeOrSpreadOptions[0]?.value
          }
          required={values.autoFee !== autoFeeOrSpreadOptions[0]?.value}
          disabled={values.autoFee === autoFeeOrSpreadOptions[0]?.value}
        />
      </div>
    </div>
  );
};

export default GeneralInformation;
