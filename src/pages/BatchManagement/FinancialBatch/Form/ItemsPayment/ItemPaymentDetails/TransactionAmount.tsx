import React, { useCallback, useEffect, useMemo } from 'react';
import { batch, useDispatch } from 'react-redux';
import { useFormikContext } from 'formik';

// components
import { GroupRadioControl } from 'app/components/FormControl/GroupRadio';
import { Button, Grid } from 'app/_libraries/_dls/components';
import { FormikNumericControl } from 'app/components/FormControl/FormikNumericControl';

// redux
import { batchManagementActions } from 'pages/BatchManagement/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useItemsPaymentForm } from 'pages/BatchManagement/FinancialBatch/hooks/useItemsPaymentForm';
import useRefData from 'app/hooks/useRefData';

// helpers
import { formatCurrency, get } from 'app/helpers';
import { getDescriptionFromCode } from 'pages/__commons/helpers';

// constants & types
import { I18N_TEXT, MAX_LENGTH_15 } from 'app/constants';
import {
  AutoFeeOrSpread,
  GRID_IDS,
  ITEM_PAYMENT_ENTRY_FIELD,
  POIC_BALANCE_AMOUNT
} from 'pages/BatchManagement/constants';
import { ItemPaymentList, Subitems } from 'pages/BatchManagement/types';

export interface TransactionAmountProps {}

const TransactionAmount: React.FC<TransactionAmountProps> = ({}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const autoFeeOrSpreadOptions = useRefData('autoFeeOrSpreadOptions');
  const ftcOptions = useRefData('ftc');

  const { selectedItem, values, setValues, handleChangeFormField, itemList } =
    useItemsPaymentForm();

  const { subitems } = selectedItem;

  const { handleChange } = useFormikContext();

  const handleCheckboxChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    // clear transactionAmountPaymentEntry when autoSpread change
    setValues((prev: any) => ({
      ...prev,
      transactionAmountPaymentEntry: ''
    }));

    handleChange(event);
    handleChangeFormField(event);
  };

  const handleSetModalSubitem = useCallback(
    modalProps => {
      dispatch(
        batchManagementActions.setModalSubitemPayment({
          isOpenModal: true,
          modalProps
        })
      );
    },
    [dispatch]
  );

  const handleDeleteSubitem = useCallback(
    (subitem: Subitems) => {
      dispatch(
        rootModalActions.open({
          title: I18N_TEXT.CONFIRM_REMOVE_SUBITEM,
          body: I18N_TEXT.ARE_YOU_SURE_WANT_TO_REMOVE_THIS_SUBITEM,
          btnCancelText: I18N_TEXT.CANCEL,
          btnConfirmText: I18N_TEXT.REMOVE,
          onConfirm: () => {
            dispatch(batchManagementActions.removeSubitemPayment(subitem));
          },
          onCancel: () => {
            dispatch(rootModalActions.close());
          }
        })
      );
    },
    [dispatch]
  );

  const columns = useMemo(
    () => [
      {
        Header: t(I18N_TEXT.FTC),
        accessor: (item: Subitems) => (
          <>
            <p>{item.ftc}</p>
            <p className="color-grey">
              {getDescriptionFromCode(ftcOptions, item.ftc, false)}
            </p>
          </>
        ),
        id: GRID_IDS.FTC,
        isSort: false,
        width: 270,
        autoWidth: true
      },
      {
        Header: t(I18N_TEXT.POIC),
        accessor: GRID_IDS.POIC,
        id: GRID_IDS.POIC,
        width: 98
      },
      {
        Header: t(I18N_TEXT.BALANCE_AMOUNT),
        accessor: (item: Subitems) => (
          <span>{formatCurrency(item.balanceAmount)}</span>
        ),
        id: GRID_IDS.BALANCE_AMOUNT,
        isSort: false,
        width: 190,
        cellProps: { className: 'text-right' }
      },
      ...(values.autoSpread === AutoFeeOrSpread.YES
        ? [
            {
              Header: t(I18N_TEXT.TRANSACTION_AMOUNT),
              accessor: (item: Subitems) => (
                <span>
                  {item.transactionAmountPaymentEntry &&
                    formatCurrency(item.transactionAmountPaymentEntry)}
                </span>
              ),
              id: GRID_IDS.TRANSACTION_AMOUNT,
              isSort: false,
              width: 190,
              cellProps: { className: 'text-right' }
            }
          ]
        : []),
      ...(values.autoSpread === AutoFeeOrSpread.NO
        ? [
            {
              Header: t(I18N_TEXT.TO_PAY_AMOUNT),
              accessor: (item: Subitems) => (
                <>
                  {item.toPayAmount ? (
                    <span>{formatCurrency(item.toPayAmount)}</span>
                  ) : (
                    <span className="content-empty"></span>
                  )}
                </>
              ),
              id: GRID_IDS.TO_PAY_AMOUNT,
              isSort: false,
              width: 190,
              cellProps: { className: 'text-right' }
            }
          ]
        : []),
      ...(values.autoSpread === AutoFeeOrSpread.NO
        ? [
            {
              Header: t(I18N_TEXT.ACTIONS),
              accessor: (item: Subitems) => {
                return (
                  <>
                    <Button
                      size="sm"
                      variant="outline-danger"
                      onClick={() => {
                        handleDeleteSubitem(item);
                      }}
                    >
                      {t(I18N_TEXT.REMOVE)}
                    </Button>
                    <Button
                      size="sm"
                      variant="outline-primary"
                      onClick={() => handleSetModalSubitem(item)}
                    >
                      {t(I18N_TEXT.EDIT)}
                    </Button>
                  </>
                );
              },
              id: 'action',
              cellProps: { className: 'text-center td-sm' },
              isFixedRight: true,
              width: 145
            }
          ]
        : [])
    ],
    [
      t,
      handleDeleteSubitem,
      handleSetModalSubitem,
      values.autoSpread,
      ftcOptions
    ]
  );

  useEffect(() => {
    // transactionAmountPaymentEntry = total toPayAmount when auto spread = No
    if (values.autoSpread === AutoFeeOrSpread.NO) {
      const _transactionAmountPaymentEntry = subitems?.reduce(
        (val, subitem) => val + parseInt(subitem.toPayAmount || '0'),
        0
      );
      if (!!_transactionAmountPaymentEntry) {
        setValues((prev: any) => ({
          ...prev,
          transactionAmountPaymentEntry:
            _transactionAmountPaymentEntry.toString()
        }));
      } else {
        setValues((prev: any) => ({
          ...prev,
          transactionAmountPaymentEntry: ''
        }));
      }
    }
  }, [values.autoSpread, subitems, setValues]);

  const handleChangeTransactionAmount = (event: React.ChangeEvent<any>) => {
    const { name, value: transactionAmountPaymentEntry } = event.target;
    if (transactionAmountPaymentEntry) {
      const _itemList = [...itemList];
      const indexItem = _itemList?.findIndex(
        _item => _item.itemNumber === selectedItem.itemNumber
      );
      const _item: ItemPaymentList = {
        ..._itemList[indexItem],
        [name]: transactionAmountPaymentEntry,
        // hard code ftc details follow value transactionAmountPaymentEntry
        subitems: [
          {
            ftc: 'CCCP',
            poic: get(POIC_BALANCE_AMOUNT, ['CCCP', 'POIC'], ''),
            balanceAmount: get(
              POIC_BALANCE_AMOUNT,
              ['CCCP', 'balanceAmount'],
              ''
            ),
            transactionAmountPaymentEntry: Math.floor(
              Number(transactionAmountPaymentEntry) / 4
            ).toString(),
            subitemID: selectedItem?.subitems?.[0]
              ?.transactionAmountPaymentEntry
              ? selectedItem.subitems[0].subitemID
              : Math.floor(Math.random() * 1000).toString()
          },
          {
            ftc: 'IINIT',
            poic: get(POIC_BALANCE_AMOUNT, ['IINIT', 'POIC'], ''),
            balanceAmount: get(
              POIC_BALANCE_AMOUNT,
              ['IINIT', 'balanceAmount'],
              ''
            ),
            transactionAmountPaymentEntry: Math.floor(
              Number(transactionAmountPaymentEntry) / 4
            ).toString(),
            subitemID: selectedItem?.subitems?.[1]
              ?.transactionAmountPaymentEntry
              ? selectedItem.subitems[1].subitemID
              : Math.floor(Math.random() * 1000).toString()
          },
          {
            ftc: 'PCCP',
            poic: get(POIC_BALANCE_AMOUNT, ['PCCP', 'POIC'], ''),
            balanceAmount: get(
              POIC_BALANCE_AMOUNT,
              ['PCCP', 'balanceAmount'],
              ''
            ),
            transactionAmountPaymentEntry: Math.floor(
              Number(transactionAmountPaymentEntry) / 4
            ).toString(),
            subitemID: selectedItem?.subitems?.[2]
              ?.transactionAmountPaymentEntry
              ? selectedItem.subitems[2].subitemID
              : Math.floor(Math.random() * 1000).toString()
          },
          {
            ftc: 'PINT',
            poic: get(POIC_BALANCE_AMOUNT, ['PINT', 'POIC'], ''),
            balanceAmount: get(
              POIC_BALANCE_AMOUNT,
              ['PINT', 'balanceAmount'],
              ''
            ),
            transactionAmountPaymentEntry: (
              Number(transactionAmountPaymentEntry) -
              3 * Math.floor(Number(transactionAmountPaymentEntry) / 4)
            ).toString(),
            subitemID: selectedItem?.subitems?.[3]
              ?.transactionAmountPaymentEntry
              ? selectedItem.subitems[3].subitemID
              : Math.floor(Math.random() * 1000).toString()
          }
        ]
      };
      _itemList[indexItem] = _item;

      batch(() => {
        dispatch(batchManagementActions.onSelectedItem(_item));
        dispatch(batchManagementActions.setItemsPayment([..._itemList]));
      });
    }
  };

  const renderFTCDetails = () => {
    if (values.autoSpread === AutoFeeOrSpread.YES) {
      if (selectedItem.transactionAmountPaymentEntry) {
        return (
          <div className="mt-24">
            <h6 className="color-grey">{t(I18N_TEXT.FTC_DETAILS)}</h6>
            <div className="mt-16">
              <Grid columns={columns} data={subitems || []} />
            </div>
          </div>
        );
      }
    } else {
      return (
        <div className="mt-24">
          <div className="d-flex justify-content-between align-items-center">
            <h6 className="color-grey">
              {t(I18N_TEXT.FTC_DETAILS)}
              <span className="asterisk color-danger ml-4">*</span>
            </h6>

            <Button
              className="mr-n8"
              variant="outline-primary"
              size="sm"
              onClick={() => handleSetModalSubitem({})}
            >
              {t(I18N_TEXT.ADD_SUBITEM)}
            </Button>
          </div>
          <div className="mt-16">
            {!!subitems?.length ? (
              <Grid columns={columns} data={subitems} />
            ) : (
              t(I18N_TEXT.NO_SUBITEMS_TO_DISPLAY)
            )}
          </div>
        </div>
      );
    }
  };

  return (
    <div className="mt-24">
      <h6 className="color-grey">{t(I18N_TEXT.TRANSACTION_AMOUNT)}</h6>
      <div className="row">
        <div className="col-12 mt-16">
          <div className="d-flex">
            <p className="fw-500 color-grey mr-24">
              {t(I18N_TEXT.AUTO_SPREAD)}:
            </p>
            <GroupRadioControl
              name={ITEM_PAYMENT_ENTRY_FIELD.AUTO_SPREAD.name}
              direction="horizontal"
              options={autoFeeOrSpreadOptions}
              onValueChange={handleCheckboxChange}
              forceUpdateValue
            />
          </div>
        </div>
        <FormikNumericControl
          name={ITEM_PAYMENT_ENTRY_FIELD.TRANSACTION_AMOUNT.name}
          label={t(ITEM_PAYMENT_ENTRY_FIELD.TRANSACTION_AMOUNT.label)}
          className="col-12 col-md-8 col-lg-6 mt-16"
          onValueBlur={handleChangeTransactionAmount}
          forceShowBorderError={
            selectedItem.isTouched && values.autoSpread === AutoFeeOrSpread.YES
          }
          maxLength={MAX_LENGTH_15}
          readOnly={values.autoSpread === AutoFeeOrSpread.NO}
          required={values.autoSpread === AutoFeeOrSpread.YES}
        />
      </div>
      {renderFTCDetails()}
    </div>
  );
};
export default TransactionAmount;
