import React from 'react';

// components
import GeneralInformation from './GeneralInformation';
import TransactionAmount from './TransactionAmount';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_TEXT } from 'app/constants';

export interface PaymentEntryProps {}

const PaymentEntry: React.FC<PaymentEntryProps> = ({}) => {
  const { t } = useTranslation();

  return (
    <>
      <h5 className="mt-24">{t(I18N_TEXT.PAYMENT_ENTRY)}</h5>
      <GeneralInformation />
      <TransactionAmount />
    </>
  );
};

export default PaymentEntry;
