import React, { useCallback, useState } from 'react';

// components
import { Button, Icon, Popover, Tooltip } from 'app/_libraries/_dls/components';
import FilterItemsPaymentPopoverContainer from './FilterItemsPaymentPopoverContainer';

// redux
import { useSelector } from 'react-redux';
import { selectorFilterBy } from 'pages/BatchManagement/_redux/financialBatch/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import classNames from 'classnames';
import { isEmpty } from 'app/helpers';

// constants & types
import { I18N_TEXT } from 'app/constants';
import { ItemsPaymentFilterBy } from 'pages/BatchManagement/types';

export interface FilterItemsPaymentProps {}

const FilterItemsPayment: React.FC<FilterItemsPaymentProps> = () => {
  const { t } = useTranslation();
  const [open, setOpen] = useState<boolean>(false);

  const filterBy: ItemsPaymentFilterBy = useSelector(selectorFilterBy);

  const handleOpenPopover = () => {
    setOpen(!open);
  };

  const handleActivePopover = useCallback(
    (opened: boolean) => {
      setOpen(opened);
    },
    [setOpen]
  );

  return (
    <Tooltip element={t(I18N_TEXT.FILTER)} placement="top" variant="primary">
      <Popover
        size="md"
        opened={open}
        onVisibilityChange={handleOpenPopover}
        placement="bottom-end"
        keepMount
        element={
          <FilterItemsPaymentPopoverContainer
            handleActivePopover={handleActivePopover}
          />
        }
      >
        <Button
          className={classNames({
            asterisk: !isEmpty(filterBy)
          })}
          variant="icon-secondary"
          onClick={handleOpenPopover}
        >
          <Icon name="filter" />
        </Button>
      </Popover>
    </Tooltip>
  );
};
export default FilterItemsPayment;
