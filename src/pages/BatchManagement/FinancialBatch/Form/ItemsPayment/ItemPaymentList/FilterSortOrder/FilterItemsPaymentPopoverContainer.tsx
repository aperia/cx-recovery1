import React, { useCallback, useEffect, useState } from 'react';

// components
import {
  Button,
  DatePicker,
  DateRangePicker,
  MultiSelect,
  Radio,
  TextBox
} from 'app/_libraries/_dls/components';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { batchManagementActions } from 'pages/BatchManagement/_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import useRefData from 'app/hooks/useRefData';

// helpers
import { equal, isDate, isEmpty } from 'app/helpers';

// constants & types
import {
  FILTER_ITEM_PAYMENT_ENTRY_FIELD,
  TRANS_DATE_FILTER
} from 'pages/BatchManagement/constants';
import { ItemsPaymentFilterBy } from 'pages/BatchManagement/types';
import {
  I18N_TEXT,
  MAX_LENGTH_16,
  MAX_LENGTH_40,
  NUMERIC_REPLACE_REGEX
} from 'app/constants';
import { selectorFilterBy } from 'pages/BatchManagement/_redux/financialBatch/selectors';

let keepFilterBy: ItemsPaymentFilterBy = {};

export interface FilterItemsPaymentPopoverContainerProps {
  handleActivePopover: (opened: boolean) => void;
}
const FilterItemsPaymentPopoverContainer: React.FC<FilterItemsPaymentPopoverContainerProps> =
  ({ handleActivePopover }) => {
    const { t } = useTranslation();
    const dispatch = useDispatch();

    const [valuesFilter, setvaluesFilter] = useState<ItemsPaymentFilterBy>({});

    const [valueTransDate, setValueTransDate] = useState<string>(
      TRANS_DATE_FILTER.SPECIFIC_DATE
    );

    const filterBy: ItemsPaymentFilterBy = useSelector(selectorFilterBy);

    const transactionDateFilterOptions = useRefData(
      'transactionDateFilterOptions'
    );
    const transactionTypeOptions = useRefData('transactionType');

    const transactionTypeSelectedValue = transactionTypeOptions?.filter(item =>
      valuesFilter.transactionType?.includes(item.value)
    );

    useEffect(() => {
      if (!equal(filterBy, keepFilterBy)) {
        setvaluesFilter({});
        setValueTransDate(TRANS_DATE_FILTER.SPECIFIC_DATE);
        keepFilterBy = {};
      }
    }, [filterBy]);

    const handleChange = (event: any) => {
      const { name, value } = event.target;
      // for Account Number form field
      if (name === FILTER_ITEM_PAYMENT_ENTRY_FIELD.ACCOUNT_NUMBER.name) {
        const changeValue = NUMERIC_REPLACE_REGEX
          ? value.replace(NUMERIC_REPLACE_REGEX, '')
          : value;
        setvaluesFilter(values => ({ ...values, [name]: changeValue }));
      } else {
        // for Debtor Name, Date, Date Range form field
        setvaluesFilter(values => ({ ...values, [name]: value }));
      }
    };

    const handleChangeTransDate = (val: string) => {
      setValueTransDate(val);
      if (val === TRANS_DATE_FILTER.SPECIFIC_DATE) {
        setvaluesFilter(values => ({
          ...values,
          dateRange: undefined
        }));
      } else {
        setvaluesFilter(values => ({
          ...values,
          date: undefined
        }));
      }
    };

    const handleChangeTransactionType = (e: DropdownBaseChangeEvent) => {
      const currentValue = e.target.value as RefDataValue[];
      const valueMapped = currentValue.map(item => item.value);

      setvaluesFilter(values => ({ ...values, transactionType: valueMapped }));
    };

    const handleFilterPaymentEntry = useCallback(() => {
      // filter empty value in filter Payment entry fields
      const _valuesFilter = Object.fromEntries(
        Object.entries(valuesFilter).filter(
          ([_, v]) => isDate(v) || !isEmpty(v)
        )
      );
      dispatch(batchManagementActions.setFilterBy(_valuesFilter));
      keepFilterBy = _valuesFilter;
      handleActivePopover(false);
    }, [dispatch, valuesFilter, handleActivePopover]);

    const handleResetToDefault = useCallback(() => {
      setvaluesFilter({});
      setValueTransDate(TRANS_DATE_FILTER.SPECIFIC_DATE);
    }, [setvaluesFilter]);

    return (
      <>
        <h4 className="mb-16">{t(I18N_TEXT.FILTER)}</h4>
        <div className="mt-16">
          <TextBox
            name={FILTER_ITEM_PAYMENT_ENTRY_FIELD.ACCOUNT_NUMBER.name}
            value={valuesFilter.accountNumber || ''}
            onChange={handleChange}
            label={t(FILTER_ITEM_PAYMENT_ENTRY_FIELD.ACCOUNT_NUMBER.label)}
            maxLength={MAX_LENGTH_16}
          />
        </div>
        <div className="mt-16">
          <TextBox
            name={FILTER_ITEM_PAYMENT_ENTRY_FIELD.DEBTOR_NAME.name}
            value={valuesFilter.debtorName || ''}
            onChange={handleChange}
            label={t(FILTER_ITEM_PAYMENT_ENTRY_FIELD.DEBTOR_NAME.label)}
            maxLength={MAX_LENGTH_40}
          />
        </div>

        <div className="mt-16">
          <div className="d-flex align-items-center">
            <p className="fw-500 color-grey">
              {`${t(FILTER_ITEM_PAYMENT_ENTRY_FIELD.TRANS_DATE.label)}:`}
            </p>
            <div className="flex-1">
              {transactionDateFilterOptions.map((radioItem, index) => (
                <Radio
                  key={`${name}-${index}`}
                  className="d-inline-block ml-24"
                >
                  <Radio.Input
                    name={FILTER_ITEM_PAYMENT_ENTRY_FIELD.TRANS_DATE.name}
                    checked={valueTransDate === radioItem.value}
                    onChange={() => handleChangeTransDate(radioItem.value)}
                  />
                  <Radio.Label>{t(radioItem.description)}</Radio.Label>
                </Radio>
              ))}
            </div>
          </div>

          <div className="mt-12">
            {valueTransDate === TRANS_DATE_FILTER.SPECIFIC_DATE ? (
              <DatePicker
                defaultView="month"
                name={t(FILTER_ITEM_PAYMENT_ENTRY_FIELD.DATE.name)}
                label={t(FILTER_ITEM_PAYMENT_ENTRY_FIELD.DATE.label)}
                value={valuesFilter.date}
                onChange={handleChange}
              />
            ) : (
              <DateRangePicker
                view="month"
                name={FILTER_ITEM_PAYMENT_ENTRY_FIELD.DATE_RANGE.name}
                label={t(FILTER_ITEM_PAYMENT_ENTRY_FIELD.DATE_RANGE.label)}
                value={valuesFilter.dateRange}
                className="mt-16"
                onChange={handleChange}
              />
            )}
          </div>
          <div className="mt-16">
            <MultiSelect
              name={t(FILTER_ITEM_PAYMENT_ENTRY_FIELD.TRANS_TYPE.name)}
              label={t(FILTER_ITEM_PAYMENT_ENTRY_FIELD.TRANS_TYPE.label)}
              value={transactionTypeSelectedValue}
              onChange={handleChangeTransactionType}
              textField={'description'}
              variant="group"
              popupBaseProps={{
                popupBaseClassName: 'inside-infobar'
              }}
            >
              {transactionTypeOptions.map(item => {
                const isChecked = new Set(valuesFilter.transactionType).has(
                  item.value
                );
                return (
                  <MultiSelect.Item
                    value={item}
                    label={item.description}
                    key={item.value}
                    variant="checkbox"
                    checked={isChecked}
                  />
                );
              })}
            </MultiSelect>
          </div>

          <div className="d-flex justify-content-end mt-24">
            <Button
              onClick={handleResetToDefault}
              size="sm"
              variant="secondary"
            >
              {t(I18N_TEXT.RESET_TO_DEFAULT)}
            </Button>
            <Button
              size="sm"
              onClick={handleFilterPaymentEntry}
              variant="primary"
            >
              {t(I18N_TEXT.APPLY)}
            </Button>
          </div>
        </div>
      </>
    );
  };
export default FilterItemsPaymentPopoverContainer;
