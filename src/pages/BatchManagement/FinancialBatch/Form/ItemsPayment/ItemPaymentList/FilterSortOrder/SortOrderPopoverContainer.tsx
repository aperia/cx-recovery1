import React, { Fragment, useEffect, useMemo, useState } from 'react';
import { batch, useDispatch } from 'react-redux';

// components
import { Button, DropdownList, Radio } from 'app/_libraries/_dls/components';

// redux
import { batchManagementActions } from 'pages/BatchManagement/_redux';
import {
  selectorOrderBy,
  selectorSortBy
} from 'pages/BatchManagement/_redux/financialBatch/selectors';

// hooks
import { useSelector } from 'app/hooks';

// helpers
import { isEmpty, isEqual } from 'app/helpers';

// constants
import {
  DROPDOWN_SORT_ITEMS_PAYMENT,
  I18N_TEXT,
  ordersDropdown
} from 'app/constants';
import { sortItemsPaymentByDefault } from 'pages/BatchManagement/constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { orderByDefault } from 'pages/AccountManagement/types';

let refSortBy: RefDataValue = DROPDOWN_SORT_ITEMS_PAYMENT[1];
let refOrderBy: RefDataValue = orderByDefault;

export interface SortOrderPopoverContainerProps {
  handleActivePopover: (opened: boolean) => void;
}
const SortOrderPopoverContainer: React.FC<SortOrderPopoverContainerProps> = ({
  handleActivePopover
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const sortBy: RefDataValue = useSelector(selectorSortBy);

  const orderBy: RefDataValue = useSelector(selectorOrderBy);

  const [valueChecked, setChecked] = useState('asc');
  const [valueDropdown, setDropdown] = useState<RefDataValue>(
    sortItemsPaymentByDefault
  );

  useEffect(() => {
    if (!isEqual(refSortBy, sortBy)) {
      setDropdown(sortBy);
      refSortBy = sortBy;
    }

    if (refOrderBy.value !== orderBy.value) {
      setChecked(orderBy.value);
      refOrderBy = orderBy;
    }
  }, [orderBy, sortBy]);

  const handleChangeRadio = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.value);
  };

  const handleChangeDropdown = (event: DropdownBaseChangeEvent) => {
    setDropdown(event.target.value);
  };

  const handleResetToDefault = () => {
    if (!isEqual(sortItemsPaymentByDefault, valueDropdown)) {
      setDropdown(sortItemsPaymentByDefault);
    }
    if (orderByDefault.value !== valueChecked) {
      setChecked(orderByDefault.value);
    }
  };

  const handleApply = () => {
    batch(() => {
      if (!isEqual(refSortBy, valueDropdown)) {
        dispatch(batchManagementActions.onChangeSortAccounts(valueDropdown));
      }
      if (refOrderBy.value !== valueChecked) {
        const findValueOrder = ordersDropdown.find(
          item => item.value === valueChecked
        );
        dispatch(batchManagementActions.onChangeOrderBy(findValueOrder!));
      }
    });
    handleActivePopover(false);
  };

  const renderRadioInput = useMemo(() => {
    return ordersDropdown.map(item => {
      const checked = valueChecked === item.value ? true : false;
      return (
        <Radio className="ml-24" key={item.value}>
          <Radio.Input
            id={item.value}
            name={item.value}
            value={item.value}
            onChange={handleChangeRadio}
            checked={checked}
          />
          <Radio.Label>{t(item.description)}</Radio.Label>
        </Radio>
      );
    });
  }, [t, valueChecked]);
  return (
    <Fragment>
      <h4 className="mb-16">{t('txt_sort_and_order')}</h4>
      <DropdownList
        id={'DropdownSortByAccountList'}
        textField="description"
        value={!isEmpty(valueDropdown) ? valueDropdown : null}
        label={t(I18N_TEXT.SORT_BY)}
        onChange={handleChangeDropdown}
      >
        {DROPDOWN_SORT_ITEMS_PAYMENT.map(item => (
          <DropdownList.Item
            key={item.value}
            label={t(item.description)}
            value={item}
          />
        ))}
      </DropdownList>
      <div className="d-flex mt-16">
        <p className="fw-500 color-grey">{t(I18N_TEXT.ORDER_BY)}: </p>
        {renderRadioInput}
      </div>
      <div className="d-flex justify-content-end mt-24">
        <Button onClick={handleResetToDefault} size="sm" variant="secondary">
          {t(I18N_TEXT.RESET_TO_DEFAULT)}
        </Button>
        <Button size="sm" onClick={handleApply} variant="primary">
          {t(I18N_TEXT.APPLY)}
        </Button>
      </div>
    </Fragment>
  );
};

export default SortOrderPopoverContainer;
