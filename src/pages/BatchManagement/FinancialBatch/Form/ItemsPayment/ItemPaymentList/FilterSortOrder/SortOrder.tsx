import React, { useCallback, useState } from 'react';

// components
import SortOrderPopoverContainer from './SortOrderPopoverContainer';

// constants
import { Button, Icon, Popover, Tooltip } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';

export interface SortOrderProps {}
export const SortOrder: React.FC<SortOrderProps> = () => {
  const { t } = useTranslation();

  const [open, setOpen] = useState<boolean>(false);

  const handleOpenPopover = () => {
    setOpen(!open);
  };

  const handleActivePopover = useCallback(
    (opened: boolean) => {
      setOpen(opened);
    },
    [setOpen]
  );

  return (
    <Tooltip
      element={t('txt_sort_and_order')}
      placement="top"
      variant="primary"
    >
      <Popover
        size="md"
        opened={open}
        onVisibilityChange={handleOpenPopover}
        placement="bottom-end"
        keepMount
        element={
          <SortOrderPopoverContainer
            handleActivePopover={handleActivePopover}
          />
        }
      >
        <Button variant="icon-secondary" onClick={handleOpenPopover}>
          <Icon name="sort-by" />
        </Button>
      </Popover>
    </Tooltip>
  );
};
export default SortOrder;
