import React, { useCallback, useMemo } from 'react';

// components
import MaskAccNbr from 'app/components/MaskAccNbr';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { formatCurrency, formatTime } from 'app/helpers';
import { mapTransactionType } from 'pages/BatchManagement/FinancialBatch/helpers';

// constants & types
import { I18N_TEXT } from 'app/constants';
import { ItemPaymentList } from 'pages/BatchManagement/types';

export interface ItemPatchDeleteConfirmModalProps {
  itemPayment: ItemPaymentList;
}

const ItemPatchDeleteConfirmModal: React.FC<ItemPatchDeleteConfirmModalProps> =
  ({ itemPayment }) => {
    const { t } = useTranslation();

    const valueAcc = useMemo(
      () => ({
        accountNumberUnMask: itemPayment.accNumber?.accountNumberUnMask,
        accountNumber: itemPayment.accNumber?.accountNumber
      }),
      [itemPayment.accNumber]
    );

    const renderAmount = useCallback(() => {
      if (itemPayment.transactionAmountPaymentEntry) {
        return formatCurrency(itemPayment.transactionAmountPaymentEntry);
      }
      if (itemPayment.totalToPayAmount) {
        return formatCurrency(itemPayment.totalToPayAmount);
      }
      return <span className="content-empty"></span>;
    }, [itemPayment]);

    return (
      <div>
        <p>{t(I18N_TEXT.ARE_YOU_SURE_WANT_TO_REMOVE_THIS_ITEM)}</p>
        <div className="border br-light-l04 br-radius-8 bg-light-l20 mt-16 p-16">
          <div className="d-flex">
            <div className="mr-24">
              <p>{t(I18N_TEXT.DEBTOR_NAME)}:</p>
              <p className="mt-8">{t(I18N_TEXT.ACCOUNT_NUMBER)}:</p>
              <p className="mt-8">{t(I18N_TEXT.TRANS_DATE)}:</p>
              <p className="mt-8">{t(I18N_TEXT.TRANS_TYPE)}:</p>
              <p className="mt-8">{t(I18N_TEXT.TRANS_AMOUNT)}:</p>
            </div>
            <div className="flex-1 fw-500">
              <p className="content-empty">{itemPayment.debtorName}</p>
              <div className="mt-8">
                {itemPayment.accNumber ? (
                  <MaskAccNbr label="" value={valueAcc} />
                ) : (
                  <p className="content-empty"></p>
                )}
              </div>
              <p className="content-empty mt-8">
                {formatTime(itemPayment.transactionDate || '').date}
              </p>
              <p className="content-empty mt-8">
                {mapTransactionType(itemPayment.transactionType)}
              </p>
              <p className="content-empty mt-8">{renderAmount()}</p>
            </div>
          </div>
        </div>
      </div>
    );
  };

export default ItemPatchDeleteConfirmModal;
