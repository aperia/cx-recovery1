import React, { useCallback, useMemo } from 'react';
import { batch, useDispatch } from 'react-redux';

// components
import { Button, Icon, Tooltip } from 'app/_libraries/_dls/components';
import MaskAccNbr from 'app/components/MaskAccNbr';
import ItemPatchDeleteConfirmModal from './ItemPatchDeleteConfirmModal';
import { accountManagementActions } from 'pages/AccountManagement/_redux';

// redux
import { batchManagementActions } from 'pages/BatchManagement/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useItemsPaymentForm } from '../../../../hooks/useItemsPaymentForm';

// constants
import { I18N_TEXT } from 'app/constants';
import {
  AutoFeeOrSpread,
  INIT_PAYMENT_ENTRY_FORM_VALUE
} from 'pages/BatchManagement/constants';

// types
import { ItemPaymentList } from 'pages/BatchManagement/types';

// helpers
import classNames from 'classnames';
import { formatCurrency, formatTime, isEmpty } from 'app/helpers';
import { mapTransactionType } from 'pages/BatchManagement/FinancialBatch/helpers';

export interface ItemProps {
  itemPayment: ItemPaymentList;
  index: number;
}
const ItemPayment: React.FC<ItemProps> = ({ itemPayment, index }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const {
    setValues,
    setTouched,
    selectedItem,
    itemList,
    itemListFilterSortOrder
  } = useItemsPaymentForm();

  const valueAcc = useMemo(
    () => ({
      accountNumberUnMask: itemPayment.accNumber?.accountNumberUnMask,
      accountNumber: itemPayment.accNumber?.accountNumber
    }),
    [itemPayment.accNumber]
  );

  const handleDeleteItem = useCallback(
    (event: React.MouseEvent<HTMLElement>) => {
      const _itemList = [...itemList];
      const indexItemDelete = _itemList.findIndex(
        item => item.itemNumber === itemPayment.itemNumber
      );
      _itemList.splice(indexItemDelete, 1);

      const _itemListFilterSortOrder = [...itemListFilterSortOrder];
      const indexItemDeleteFilterSortOrder = _itemListFilterSortOrder.findIndex(
        item => item.itemNumber === itemPayment.itemNumber
      );
      _itemListFilterSortOrder.splice(indexItemDeleteFilterSortOrder, 1);

      if (itemPayment.accNumber?.accountNumber) {
        dispatch(
          rootModalActions.open({
            title: I18N_TEXT.CONFIRM_REMOVE_ITEM,
            size: 'xs',
            body: <ItemPatchDeleteConfirmModal itemPayment={itemPayment} />,
            btnCancelText: I18N_TEXT.CANCEL,
            btnConfirmText: I18N_TEXT.REMOVE,
            onConfirm: () => {
              batch(() => {
                if (itemPayment.itemNumber === selectedItem.itemNumber) {
                  if (!_itemList.length) {
                    batch(() => {
                      dispatch(batchManagementActions.clearItemPayment());
                      dispatch(accountManagementActions.onSearchByAccReset());
                    });
                    // have clearItemPayment => no need to setItemsPayment([])
                    return;
                  } else {
                    // check !!_itemListFilterSortOrder.length for case loop filter then delete final item in itemListFilterSortOrder then click button clear and reset
                    if (!!_itemListFilterSortOrder.length) {
                      if (isEmpty(_itemList[indexItemDelete])) {
                        // selected previous itemPayment in itemList when itemDelete in last item list
                        dispatch(
                          batchManagementActions.onSelectedItem(
                            _itemList[indexItemDelete - 1]
                          )
                        );
                      } else {
                        dispatch(
                          batchManagementActions.onSelectedItem(
                            _itemList[indexItemDelete]
                          )
                        );
                      }
                    }
                  }
                }
                dispatch(batchManagementActions.setItemsPayment(_itemList));
              });
            },
            onCancel: () => {
              dispatch(rootModalActions.close());
            }
          })
        );
      } else {
        dispatch(batchManagementActions.setItemsPayment(_itemList));
      }

      event.stopPropagation();
    },
    [
      dispatch,
      itemPayment,
      itemList,
      selectedItem.itemNumber,
      itemListFilterSortOrder
    ]
  );

  const handleSelectedItem = useCallback(() => {
    const _itemList = [...itemList];
    const keepValueForm = _itemList?.find(
      _itemPayment => _itemPayment.itemNumber === itemPayment.itemNumber
    );

    if (!selectedItem.isTouched) {
      // set touch in current item when selected another item
      const indexItemPaymentPrevious = _itemList?.findIndex(
        _itemPayment => _itemPayment.itemNumber === selectedItem.itemNumber
      );

      _itemList[indexItemPaymentPrevious] = {
        ...selectedItem,
        isTouched: true
      };
      dispatch(batchManagementActions.setItemsPayment(_itemList));
    }

    dispatch(batchManagementActions.onSelectedItem(itemPayment));
    setValues((prev: any) => ({
      ...prev,
      ...INIT_PAYMENT_ENTRY_FORM_VALUE,
      ...keepValueForm
    }));
    setTouched({}, false);
  }, [dispatch, setValues, setTouched, itemList, itemPayment, selectedItem]);

  const checkItemNotCompleted = () => {
    if (
      itemPayment.accNumber?.accountNumber &&
      itemPayment.itemNumber !== selectedItem.itemNumber
    ) {
      const isValidmanualAgencyFee =
        itemPayment.autoFee === AutoFeeOrSpread.YES ||
        itemPayment.manualAgencyFee !== '';

      if (itemPayment.autoSpread === AutoFeeOrSpread.YES) {
        return (
          !itemPayment.subitems?.filter(
            _subitem => _subitem.transactionAmountPaymentEntry
          ).length || !isValidmanualAgencyFee
        );
      } else if (itemPayment.autoSpread === AutoFeeOrSpread.NO) {
        return (
          !itemPayment.subitems?.filter(_subitem => _subitem.toPayAmount)
            .length || !isValidmanualAgencyFee
        );
      }
    }
    return false;
  };

  const renderAmount = () => {
    if (itemPayment.accNumber) {
      const transactionAmountPaymentEntry =
        itemPayment?.transactionAmountPaymentEntry;

      const totalToPayAmount = itemPayment.totalToPayAmount;

      if (
        itemPayment.autoSpread === AutoFeeOrSpread.YES &&
        transactionAmountPaymentEntry &&
        parseInt(transactionAmountPaymentEntry)
      ) {
        return formatCurrency(transactionAmountPaymentEntry);
      } else if (
        itemPayment.autoSpread === AutoFeeOrSpread.NO &&
        totalToPayAmount &&
        parseInt(totalToPayAmount)
      ) {
        return formatCurrency(totalToPayAmount);
      } else {
        return <span className="content-empty"></span>;
      }
    } else {
      return <span className="content-empty"></span>;
    }
  };

  const renderItem = () => {
    return (
      <div
        className={classNames(
          'card card-blue px-12 py-8 mb-16 item-payments',
          itemPayment.itemNumber === selectedItem.itemNumber && 'active',
          checkItemNotCompleted() && 'card-error'
        )}
        onClick={handleSelectedItem}
      >
        <div className="d-flex">
          <div className="item-index">{index}</div>
          <div className="flex-1 ml-8">
            <div className="d-flex align-items-center justify-content-between">
              {itemPayment.accNumber ? (
                <div className="fw-500">
                  <MaskAccNbr label="" value={valueAcc} />
                </div>
              ) : (
                <div className="fw-500">{t(I18N_TEXT.NO_ACCOUNT_LINKED)}</div>
              )}
              <Button
                variant="icon-danger"
                size="sm"
                className="mr-n4"
                onClick={handleDeleteItem}
              >
                <Icon name="close" />
              </Button>
            </div>
            <div className="color-grey mt-4">
              {/* item.transactionDate have default value form itemsPayment when edit case*/}
              {itemPayment.accNumber && itemPayment.transactionDate ? (
                formatTime(itemPayment.transactionDate).date +
                ' • ' +
                mapTransactionType(itemPayment.transactionType as string)
              ) : (
                <span className="content-empty"></span>
              )}
            </div>
            <div className="d-flex align-items-center mt-4">
              <Icon
                name="collection-information"
                className="color-grey-l08 mr-8"
              />
              {/* transactionAmountPaymentEntryhave default value form itemsPayment when edit case*/}
              {renderAmount()}
            </div>
          </div>
        </div>
      </div>
    );
  };

  return (
    <>
      {checkItemNotCompleted() ? (
        <Tooltip
          element={t(
            I18N_TEXT.THERE_ARE_REQUIRED_FIELDS_THAT_NEED_TO_BE_COMPLETED
          )}
          placement="top-start"
          triggerClassName="w-100"
          variant="default"
        >
          {renderItem()}
        </Tooltip>
      ) : (
        renderItem()
      )}
    </>
  );
};

export default ItemPayment;
