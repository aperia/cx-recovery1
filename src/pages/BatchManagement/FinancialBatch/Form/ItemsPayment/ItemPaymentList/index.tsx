import React, { useCallback } from 'react';
import { batch, useDispatch } from 'react-redux';

// components
import { Button } from 'app/_libraries/_dls/components';
import ItemPayment from './ItemPayment';
import SortOrder from './FilterSortOrder/SortOrder';
import FilterItemsPayment from './FilterSortOrder/FilterItemsPayment';
import { SearchNodata } from 'app/components';

// redux
import { useSelector } from 'app/hooks';
import { selectorFilterBy } from 'pages/BatchManagement/_redux/financialBatch/selectors';
import { batchManagementActions } from 'pages/BatchManagement/_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useItemsPaymentForm } from '../../../hooks/useItemsPaymentForm';

// helpers
import { isEmpty } from 'app/helpers';

// constants & types
import { I18N_TEXT } from 'app/constants';
import { ItemsPaymentFilterBy } from 'pages/BatchManagement/types';

export interface ItemListProps {}
const ItemList: React.FC<ItemListProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const filterBy: ItemsPaymentFilterBy = useSelector(selectorFilterBy);
  const { itemList, itemListFilterSortOrder, selectedItem } =
    useItemsPaymentForm();

  const handleAddItem = useCallback(() => {
    const _itemList = [...itemList];
    const itemNumber = Math.floor(Math.random() * 1000).toString();

    if (!selectedItem.isTouched) {
      // set touch in current item when selected another item
      const indexItemPaymentPrevious = _itemList?.findIndex(
        _itemPayment => _itemPayment.itemNumber === selectedItem.itemNumber
      );
      _itemList[indexItemPaymentPrevious] = {
        ...selectedItem,
        isTouched: true
      };
    }

    batch(() => {
      dispatch(
        batchManagementActions.setItemsPayment([
          ..._itemList,
          {
            itemNumber
          }
        ])
      );
      dispatch(
        batchManagementActions.onSelectedItem({
          itemNumber
        })
      );
    });
  }, [dispatch, itemList, selectedItem]);

  const onClearSearch = useCallback(() => {
    dispatch(batchManagementActions.setFilterBy({}));
  }, [dispatch]);

  const renderCountItemsPayment = () => {
    return (
      <div className="d-flex justify-content-between align-items-center mt-16">
        <p>
          {t(I18N_TEXT.RESULT_FOUNDED, {
            count: itemListFilterSortOrder.filter(
              itemFilterSortOrder =>
                itemFilterSortOrder.accNumber?.accountNumber
            ).length
          })}
        </p>
        <Button
          onClick={onClearSearch}
          variant="outline-primary"
          className="mr-n8"
          size="sm"
        >
          {t(I18N_TEXT.CLEAR_AND_RESET)}
        </Button>
      </div>
    );
  };

  const renderItemList = () => {
    if (isEmpty(itemListFilterSortOrder)) {
      return (
        <SearchNodata
          text={t(I18N_TEXT.FILTER_NO_RESULTS_ITEMS_PAYMENT)}
          onClearSearch={onClearSearch}
        />
      );
    } else {
      return (
        <>
          {!isEmpty(filterBy) && renderCountItemsPayment()}
          {itemListFilterSortOrder?.map((itemPayment, index) => (
            <ItemPayment
              key={index}
              itemPayment={itemPayment}
              index={index + 1}
            />
          ))}
          <Button
            variant="outline-primary"
            className="ml-n8"
            size="sm"
            onClick={handleAddItem}
          >
            {t(I18N_TEXT.ADD_ITEM)}
          </Button>
        </>
      );
    }
  };

  return (
    <div className="px-24 pb-24 pt-22">
      <div className="d-flex align-items-center justify-content-between mb-24">
        <h4>{t(I18N_TEXT.ITEM_LIST)}</h4>
        <div className="d-flex">
          <FilterItemsPayment />
          <div className="ml-8 mr-n4">
            <SortOrder />
          </div>
        </div>
      </div>
      {renderItemList()}
    </div>
  );
};

export default ItemList;
