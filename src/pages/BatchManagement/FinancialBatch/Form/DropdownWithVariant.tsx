import { DropdownList } from 'app/_libraries/_dls/components';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { getRefDataFromCode } from 'pages/__commons/helpers';
import React, { useMemo } from 'react';

interface DropdownWithVariant {
  options: RefDataValue[];
  itemCodeValue: string;
  label: string;
  name: string;
  className?: string;
}
export const DropdownWithVariant: React.FC<DropdownWithVariant> = ({
  options = [],
  itemCodeValue = '',
  label,
  name,
  className
}) => {
  const { t } = useTranslation();
  const renderItem = useMemo(() => {
    return options.map((item: RefDataValue) => (
      <DropdownList.Item
        variant="bubble"
        label={item.description}
        key={item.value}
        value={item}
      />
    ));
  }, [options]);

  return (
    <div className={className}>
      <DropdownList
        value={getRefDataFromCode(options, itemCodeValue)}
        label={t(label)}
        key={name}
        textField="description"
        readOnly
      >
        {renderItem}
      </DropdownList>
    </div>
  );
};
