import React, { useCallback, useMemo } from 'react';
import { batch, useDispatch } from 'react-redux';
import { FormikProvider, useFormik } from 'formik';

// components
import {
  Button,
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';
import { AddBatch } from './AddBatch';
import ItemsPayment from './ItemsPayment';
import { NoDataGrid } from 'app/components';

// redux
import { batchManagementActions } from 'pages/BatchManagement/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import {
  selectorFilterBy,
  selectorItemPaymentList,
  selectorItemPaymentListFilterSortOrder
} from 'pages/BatchManagement/_redux/financialBatch/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useBatchForm } from '../hooks/useBatchForm';
import { useSelector } from 'app/hooks';

// constants & types
import { I18N_TEXT } from 'app/constants';
import {
  IBatch,
  ItemPaymentList,
  ItemsPaymentFilterBy
} from 'pages/BatchManagement/types';
import {
  AutoFeeOrSpread,
  BATCH_TYPE_PAYMENTS,
  INIT_PAYMENT_ENTRY_FORM_VALUE
} from 'pages/BatchManagement/constants';

// helpers
import { isEmpty, isFunction, isTouchedForm, set } from 'app/helpers';

interface FinancialBatchModalProps {
  isOpen: boolean;
  isAdd?: boolean;
  isSearch?: boolean;
}

export const FinancialBatchModal: React.FC<FinancialBatchModalProps> = ({
  isOpen = false,
  isAdd = true,
  isSearch = true
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const itemList: ItemPaymentList[] = useSelector(selectorItemPaymentList);
  const itemListFilterSortOrder: ItemPaymentList[] = useSelector(
    selectorItemPaymentListFilterSortOrder
  );
  const filterBy: ItemsPaymentFilterBy = useSelector(selectorFilterBy);

  const { initialValues, validationSchema, onSubmit } = useBatchForm();

  const formik = useFormik<IBatch>({
    enableReinitialize: true,
    initialValues,
    validationSchema,
    validateOnBlur: true,
    validateOnMount: false,
    onSubmit: values => {
      // map subitems in item list to values
      const _itemList = [...itemList].filter(
        itemPayment => itemPayment.accNumber?.accountNumber
      );
      const itemsPayment = _itemList.map(_item => {
        const itemPayment = { ..._item };
        if (itemPayment.autoSpread === AutoFeeOrSpread.YES) {
          set(itemPayment, ['totalToPayAmount'], '');
          set(itemPayment, ['currentItemType'], _itemList?.length);
          return itemPayment;
        } else {
          const totalToPayAmount = itemPayment.subitems
            ?.reduce(
              (val, subitem) => val + parseInt(subitem.toPayAmount || '0'),
              0
            )
            .toString();
          set(itemPayment, ['totalToPayAmount'], totalToPayAmount);
          set(itemPayment, ['transactionAmountPaymentEntry'], '');
          set(itemPayment, ['currentItemType'], _itemList?.length);

          return itemPayment;
        }
      });

      isFunction(onSubmit) && onSubmit({ ...values, itemsPayment });
      isAdd && resetForm();
      dispatch(accountManagementActions.onSearchByAccReset());
      dispatch(batchManagementActions.clearItemPayment());
    }
  });

  const { values, isValid, submitForm, resetForm, touched, dirty } = formik;

  const handleCancelForm = useCallback(() => {
    resetForm();
    batch(() => {
      dispatch(accountManagementActions.onSearchByAccReset());
      dispatch(batchManagementActions.onSetModalAddBatch({ isOpen: false }));
      dispatch(batchManagementActions.clearItemPayment());
    });
  }, [dispatch, resetForm]);

  const isValidForm = useMemo(() => {
    if (values.batchType !== 'P') {
      return isValid && dirty;
    } else {
      return (
        isValid &&
        itemList &&
        itemList
          .filter(item => item.accNumber?.accountNumber)
          .every(
            item =>
              // validate for manualAgencyFee
              (item.autoFee === AutoFeeOrSpread.YES ||
                item.manualAgencyFee !== '') &&
              // validate for transaction amount
              ((item.autoSpread === AutoFeeOrSpread.YES &&
                item.transactionAmountPaymentEntry) ||
                (item.autoSpread === AutoFeeOrSpread.NO &&
                  !!item.subitems?.filter(_subitem => _subitem.toPayAmount)
                    .length))
          )
      );
    }
  }, [isValid, itemList, values, dirty]);

  const handleCancel = useCallback(() => {
    if (dirty && isTouchedForm(touched)) {
      dispatch(
        rootModalActions.open({
          title: I18N_TEXT.UNSAVED_CHANGE,
          body: I18N_TEXT.DISCARD_CHANGE_BODY,
          btnCancelText: I18N_TEXT.CONTINUE_EDITING,
          btnConfirmText: I18N_TEXT.DISCARD_CHANGE,
          onConfirm: handleCancelForm
        })
      );
      return;
    }

    handleCancelForm();
  }, [dispatch, touched, dirty, handleCancelForm]);

  const handleAddItem = useCallback(() => {
    const itemNumber = Math.floor(Math.random() * 1000).toString();
    batch(() => {
      dispatch(
        batchManagementActions.setItemsPayment([
          { ...INIT_PAYMENT_ENTRY_FORM_VALUE, itemNumber }
        ])
      );
      dispatch(
        batchManagementActions.onSelectedItem({
          ...INIT_PAYMENT_ENTRY_FORM_VALUE,
          itemNumber
        })
      );
    });
  }, [dispatch]);

  const renderItemsPayment = useMemo(() => {
    if (values.batchType === BATCH_TYPE_PAYMENTS) {
      if (!!itemListFilterSortOrder.length || !isEmpty(filterBy)) {
        return <ItemsPayment />;
      } else {
        return (
          <>
            <NoDataGrid text={t(I18N_TEXT.NO_ITEM_INFORMATION_TO_DISPLAY)} />
            <Button
              variant="outline-primary"
              className="d-block mt-n24 mx-auto"
              size="sm"
              onClick={handleAddItem}
            >
              {t(I18N_TEXT.ADD_ITEM)}
            </Button>
          </>
        );
      }
    }
    return <NoDataGrid text={I18N_TEXT.SELECT_BATCH_TYPE_TO_VIEW} />;
  }, [
    t,
    values.batchType,
    itemListFilterSortOrder.length,
    filterBy,
    handleAddItem
  ]);

  return (
    <FormikProvider value={formik}>
      <Modal full show={isOpen} loading={false} enforceFocus={false}>
        <ModalHeader border onHide={handleCancel} closeButton>
          <ModalTitle>
            <h4>{t(isAdd ? I18N_TEXT.ADD_BATCH : I18N_TEXT.EDIT_BATCH)}</h4>
          </ModalTitle>
        </ModalHeader>
        <ModalBody noPadding className="overflow-hidden">
          <div className="bg-light-l20 border-bottom p-24">
            <AddBatch />
          </div>
          <div className="body-add-batch">{renderItemsPayment}</div>
        </ModalBody>
        <ModalFooter
          border
          cancelButtonText={t(I18N_TEXT.CANCEL)}
          okButtonText={t(isAdd ? I18N_TEXT.SUBMIT : I18N_TEXT.SAVE)}
          onCancel={handleCancel}
          disabledOk={!(isValidForm && isTouchedForm(touched))}
          onOk={submitForm}
        />
      </Modal>
    </FormikProvider>
  );
};
