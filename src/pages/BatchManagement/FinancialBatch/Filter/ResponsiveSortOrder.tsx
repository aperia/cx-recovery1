import React, { Fragment, useEffect, useMemo, useState } from 'react';
import { isEqual } from 'lodash';

// components
import {
  Button,
  DropdownList,
  Icon,
  Popover,
  Radio,
  Tooltip
} from 'app/_libraries/_dls/components';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';

// hooks
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';

// constants & types
import { dropdownSortBatch, I18N_TEXT, ordersDropdown } from 'app/constants';
import {
  selectorOrderBatch,
  selectorSortBatch
} from 'pages/BatchManagement/_redux/financialBatch/selectors';
import { batchManagementActions } from 'pages/BatchManagement/_redux';

let refSortBy: RefDataValue = dropdownSortBatch[0];
let refOrderBy: RefDataValue = ordersDropdown[0];

interface ResponsiveSortOrderProps {}

const ResponsiveSortOrder: React.FC<ResponsiveSortOrderProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const sortBy = useSelector(selectorSortBatch);
  const orderBy = useSelector(selectorOrderBatch);

  const [open, setOpen] = useState<boolean>(false);
  const [valueChecked, setChecked] = useState('desc');
  const [valueDropdown, setDropdown] = useState<RefDataValue>(
    dropdownSortBatch[0] as RefDataValue
  );

  useEffect(() => {
    if (!isEqual(refSortBy, sortBy)) {
      setDropdown(sortBy);
      refSortBy = sortBy;
    }

    if (refOrderBy.value !== orderBy.value) {
      setChecked(orderBy.value);
      refOrderBy = orderBy;
    }
  }, [orderBy, sortBy]);

  const handleOpenPopover = () => {
    setOpen(!open);
  };

  const handleChangeRadio = (event: React.ChangeEvent<HTMLInputElement>) => {
    setChecked(event.target.value);
  };

  const handleChangeDropdown = (event: DropdownBaseChangeEvent) => {
    setDropdown(event.target.value);
  };

  const handleResetToDefault = () => {
    if (!isEqual(refSortBy, valueDropdown)) {
      setDropdown(refSortBy);
    }
    if (refOrderBy.value !== valueChecked) {
      setChecked(refOrderBy.value);
    }
  };

  const handleApply = () => {
    batch(() => {
      if (!isEqual(refSortBy, valueDropdown)) {
        dispatch(
          batchManagementActions.onChangeBatchSort({ data: valueDropdown })
        );
      }
      if (refOrderBy.value?.toLowerCase() !== valueChecked) {
        const findValueOrder = ordersDropdown.find(
          item => item.value?.toLowerCase() === valueChecked
        );
        dispatch(batchManagementActions.onChangeOrderBy(findValueOrder!));
      }
    });
    setOpen(false);
  };
  const renderRadioInput = useMemo(() => {
    return ordersDropdown.map(item => {
      const checked = valueChecked?.toLowerCase() === item.value;
      return (
        <Radio className="ml-24" key={item.value}>
          <Radio.Input
            id={item.value}
            name={item.value}
            value={item.value}
            onChange={handleChangeRadio}
            checked={checked}
          />
          <Radio.Label>{t(item.description)}</Radio.Label>
        </Radio>
      );
    });
  }, [t, valueChecked]);

  return (
    <Tooltip
      element={t('txt_sort_and_order')}
      placement="top"
      variant="primary"
    >
      <Popover
        size="md"
        opened={open}
        onVisibilityChange={handleOpenPopover}
        placement="bottom-end"
        keepMount
        element={
          <Fragment>
            <h4 className="mb-16">{t('txt_sort_and_order')}</h4>
            <DropdownList
              id={'DropdownSortByAccountList'}
              textField="description"
              value={valueDropdown}
              label={t(I18N_TEXT.SORT_BY)}
              onChange={handleChangeDropdown}
            >
              {dropdownSortBatch.map(item => (
                <DropdownList.Item
                  key={item.value}
                  label={t(item.description)}
                  value={item}
                />
              ))}
            </DropdownList>
            <div className="d-flex mt-16">
              <p className="fw-500 color-grey">{t(I18N_TEXT.ORDER_BY)}: </p>
              {renderRadioInput}
            </div>
            <div className="d-flex justify-content-end mt-24">
              <Button
                onClick={handleResetToDefault}
                size="sm"
                variant="secondary"
              >
                {t(I18N_TEXT.RESET_TO_DEFAULT)}
              </Button>
              <Button size="sm" onClick={handleApply} variant="primary">
                {t(I18N_TEXT.APPLY)}
              </Button>
            </div>
          </Fragment>
        }
      >
        <Button variant="icon-secondary" onClick={handleOpenPopover}>
          <Icon name="sort-by" />
        </Button>
      </Popover>
    </Tooltip>
  );
};
export default ResponsiveSortOrder;
