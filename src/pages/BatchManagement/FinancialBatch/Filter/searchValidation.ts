import {
  ATTRIBUTE_SEARCH_FIELD,
  MESSAGE
} from 'pages/BatchManagement/constants';
import { AttributeSearchFieldModel } from 'pages/BatchManagement/types';

export const validateBatchNumber = (
  data: AttributeSearchFieldModel,
  t: any
) => {
  if (data.batchNumber && !data.batchNumber?.value) {
    return { [ATTRIBUTE_SEARCH_FIELD.BATCH_NUMBER]: t(MESSAGE.BATCH_NUMBER) };
  }
};

export const validateBatchDate = (data: AttributeSearchFieldModel, t: any) => {
  if (data.batchDate && !data.batchDate?.value) {
    return { [ATTRIBUTE_SEARCH_FIELD.BATCH_DATE]: t(MESSAGE.BATCH_DATE) };
  }
};

export const validateCreatedBySource = (
  data: AttributeSearchFieldModel,
  t: any
) => {
  if (data.createBySource && !data.createBySource?.value) {
    return {
      [ATTRIBUTE_SEARCH_FIELD.CREATED_BY_SOURCE]: t(MESSAGE.CREATED_BY_SOURCE)
    };
  }
};

export const validateCreatedByOperator = (
  data: AttributeSearchFieldModel,
  t: any
) => {
  if (data.createByOperator && !data.createByOperator?.value) {
    return {
      [ATTRIBUTE_SEARCH_FIELD.CREATED_BY_OPERATOR]: t(
        MESSAGE.CREATED_BY_OPERATOR
      )
    };
  }
};

export const attrValidations = (data: AttributeSearchFieldModel, t: any) => {
  const validateList = [
    validateBatchNumber,
    validateBatchDate,
    validateCreatedBySource,
    validateCreatedByOperator
  ];

  const runAllValidates = validateList.map(fn => {
    return fn(data, t);
  }, []);

  const errors: Record<string, string> = runAllValidates.reduce(
    (accumulator, current) => {
      if (!current) return accumulator;
      return { ...accumulator, ...current };
    },
    {}
  );
  return Object.keys(errors).length ? errors : undefined;
};
