import React, { useMemo } from 'react';
import isEmpty from 'lodash.isempty';
import ComboBox from 'app/_libraries/_dls/components/ComboBox';
import ComboboxControl, {
  ComboBoxControlProps
} from 'app/_libraries/_dls/components/AttributeSearch/controls/ComboboxControl';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';

export interface ComboboxAttributeSearchControlProps
  extends ComboBoxControlProps {
  placeholder?: string;
  type: 'createBySource' | 'createByOperator';
}

const ComboBoxControl: React.FC<ComboboxAttributeSearchControlProps> = ({
  placeholder,
  type,
  ...props
}) => {
  const { t } = useTranslation();

  const { data: createBySource } = useGetRefDataQuery(type);

  const comboboxItems = useMemo<any>(() => {
    return createBySource?.[type]?.map((item: RefDataValue, index: number) => (
      <ComboBox.Item key={index} label={item.description} value={item} />
    ));
  }, [createBySource, type]);

  return (
    <>
      {!isEmpty(createBySource) && (
        <ComboboxControl
          textField="value"
          itemKey="value"
          placeholder={t(placeholder)}
          noResult={t('txt_no_results_found')}
          {...props}
        >
          {comboboxItems}
        </ComboboxControl>
      )}
    </>
  );
};

export default ComboBoxControl;
