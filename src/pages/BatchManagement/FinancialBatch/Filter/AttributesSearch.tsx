import React from 'react';
import {
  AttributeSearchData,
  DatePickerControl,
  InputControl
} from 'app/_libraries/_dls/components';
import {
  I18N_TEXT,
  MAX_LENGTH_8,
  PLACE_HOLDER_MM_DD_YYYY
} from 'app/constants';

import { ATTRIBUTE_SEARCH_FIELD } from '../../constants';
import ComboBoxControl from './CreatedBySource';

export const getInitialAttrData = (t: any) => {
  const batchDateField: AttributeSearchData = {
    groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
    name: t(I18N_TEXT.BATCH_DATE),
    key: ATTRIBUTE_SEARCH_FIELD.BATCH_DATE,
    description: t(PLACE_HOLDER_MM_DD_YYYY),
    component: props => <DatePickerControl {...props} />
  };

  const batchNumberField: AttributeSearchData = {
    groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
    name: t(I18N_TEXT.BATCH_NUMBER),
    key: ATTRIBUTE_SEARCH_FIELD.BATCH_NUMBER,
    description: t(I18N_TEXT.ALPHANUMERIC_8_DIGITS),
    component: props => (
      <InputControl
        placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
        maxLength={MAX_LENGTH_8}
        {...props}
      />
    )
  };

  const batchCreateBySourceField: AttributeSearchData = {
    groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
    name: t(I18N_TEXT.CREATED_BY_SOURCE),
    key: ATTRIBUTE_SEARCH_FIELD.CREATED_BY_SOURCE,
    description: t(I18N_TEXT.SELECT_FROM_THE_LIST),
    component: props => (
      <ComboBoxControl
        placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
        {...props}
        type="createBySource"
      />
    )
  };

  const batchCreateByOperatorField: AttributeSearchData = {
    groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
    name: t(I18N_TEXT.CREATED_BY_OPERATOR),
    key: ATTRIBUTE_SEARCH_FIELD.CREATED_BY_OPERATOR,
    description: t(I18N_TEXT.SELECT_FROM_THE_LIST),
    component: props => (
      <ComboBoxControl
        placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
        {...props}
        type="createByOperator"
      />
    )
  };

  return {
    data: [
      batchNumberField,
      batchDateField,
      batchCreateByOperatorField,
      batchCreateBySourceField
    ]
  };
};
