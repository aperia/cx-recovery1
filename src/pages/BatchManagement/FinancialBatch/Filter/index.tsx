import React from 'react';

// components
import {
  Button,
  DropdownList,
  MultiSelect
} from 'app/_libraries/_dls/components';

// utils
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { I18N_TEXT } from 'app/constants';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import { batchManagementActions } from 'pages/BatchManagement/_redux';
import { useDispatch, useSelector } from 'react-redux';
import {
  selectorBatchStatus,
  selectorBatchType,
  selectorOrderBatch,
  selectorSortBatch
} from 'pages/BatchManagement/_redux/financialBatch/selectors';
import ResponsiveSortOrder from './ResponsiveSortOrder';

const FilterBatch = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { data: batchTypes } = useGetRefDataQuery('batchType');
  const { data: batchStatus } = useGetRefDataQuery('batchStatus');
  const { data: sortByBatch } = useGetRefDataQuery('sortByBatch');
  const { data: orderBy } = useGetRefDataQuery('orderBy');

  const batchTypeValue = useSelector(selectorBatchType);
  const batchStatusValue = useSelector(selectorBatchStatus);
  const sortByBatchValue = useSelector(selectorSortBatch);
  const orderByValue = useSelector(selectorOrderBatch);

  const onChangeBatchType = (event: DropdownBaseChangeEvent) => {
    dispatch(
      batchManagementActions.onChangeBatchType({ data: event.target.value })
    );
  };

  const onChangeBatchStatus = (event: DropdownBaseChangeEvent) => {
    dispatch(
      batchManagementActions.onChangeBatchStatus({ data: event.target.value })
    );
  };

  const onChangeBatchOrder = (event: DropdownBaseChangeEvent) => {
    dispatch(
      batchManagementActions.onChangeBatchOrder({ data: event.target.value })
    );
  };

  const onChangeBatchSort = (event: DropdownBaseChangeEvent) => {
    dispatch(
      batchManagementActions.onChangeBatchSort({ data: event.target.value })
    );
  };

  const handleOpenAddBatchModal = () => {
    dispatch(
      batchManagementActions.onSetModalAddBatch({
        isOpen: true,
        formAction: 'ADD'
      })
    );
  };

  const renderItemMultiSelect = (
    data: Record<string, RefDataValue[]> | undefined,
    type: string
  ) => {
    if (!data) return <></>;
    return data[type].map((item: RefDataValue) => (
      <MultiSelect.Item
        label={item.value + ' - ' + item.description}
        variant="checkbox"
        key={item.value}
        value={item}
      />
    ));
  };

  const renderItemDropdownList = (
    data: Record<string, RefDataValue[]> | undefined,
    type: string
  ) => {
    if (!data) return <></>;
    return data[type].map((item: RefDataValue) => (
      <DropdownList.Item
        label={item.description}
        key={item.value}
        value={item}
      />
    ));
  };

  return (
    <div className="d-flex justify-content-between align-items-center">
      <div className="dropdown-batch">
        <div className="mr-24 d-flex align-items-center">
          <p className="color-grey fw-500 mr-4">{t(I18N_TEXT.BATCH_TYPE)}:</p>
          <MultiSelect
            onChange={onChangeBatchType}
            value={batchTypeValue}
            label=""
            textField="description"
            variant="no-border"
          >
            {renderItemMultiSelect(batchTypes, 'batchType')}
          </MultiSelect>
        </div>
        <div className="mr-24 d-flex align-items-center">
          <p className="color-grey fw-500 mr-4">{t(I18N_TEXT.BATCH_STATUS)}:</p>
          <MultiSelect
            onChange={onChangeBatchStatus}
            label=""
            textField="description"
            variant="no-border"
            value={batchStatusValue}
          >
            {renderItemMultiSelect(batchStatus, 'batchStatus')}
          </MultiSelect>
        </div>
      </div>

      <div className="d-flex align-items-center mr-n8">
        <div className="button-sort-order mr-8">
          <ResponsiveSortOrder />
        </div>

        <div className="dropdown-sort-order">
          <div className="mr-16 d-flex align-items-center">
            <p className="color-grey fw-500 mr-4">{t(I18N_TEXT.SORT_BY)}:</p>
            <DropdownList
              onChange={onChangeBatchSort}
              value={sortByBatchValue}
              textField="description"
              variant={'no-border'}
            >
              {renderItemDropdownList(sortByBatch, 'sortByBatch')}
            </DropdownList>
          </div>
          <div className="mr-16 d-flex align-items-center">
            <p className="color-grey fw-500 mr-4">{t(I18N_TEXT.ORDER_BY)}:</p>
            <DropdownList
              onChange={onChangeBatchOrder}
              value={orderByValue}
              textField="description"
              variant={'no-border'}
            >
              {renderItemDropdownList(orderBy, 'orderBy')}
            </DropdownList>
          </div>
        </div>

        <Button
          size="sm"
          variant="outline-primary"
          onClick={handleOpenAddBatchModal}
        >
          {t(I18N_TEXT.ADD_BATCH)}
        </Button>
      </div>
    </div>
  );
};

export default FilterBatch;
