import React, { useEffect } from 'react';

// components
import {
  AttributeSearch,
  AttributeSearchEvent,
  AttributeSearchRef,
  AttributeSearchValue
} from 'app/_libraries/_dls/components';
import { getInitialAttrData } from './AttributesSearch';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { batchManagementActions } from 'pages/BatchManagement/_redux';
import { selectedBatchSearch } from 'pages/BatchManagement/_redux/financialBatch/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { attrValidations } from './searchValidation';

// helpers
import { isEmpty, isEqual } from 'app/helpers';

// constants & types
import { NO_COMBINE_FIELDS_BATCH_SEARCH } from 'pages/BatchManagement/constants';
import { I18N_TEXT } from 'app/constants';
import { AttributeSearchFieldModel } from 'pages/BatchManagement/types';

export interface SearchBatchProps {}

let keepSearchValues: AttributeSearchValue[] = [];

const SearchBatch: React.FC = ({}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { data } = React.useMemo(() => getInitialAttrData(t), [t]);
  const attributeSearchRef = React.useRef<AttributeSearchRef | null>(null);

  const [searchValues, setSearchValues] = React.useState<
    AttributeSearchValue[]
  >([]);

  const originalSearch = useSelector(selectedBatchSearch);

  useEffect(() => {
    if (!isEqual(originalSearch, keepSearchValues)) {
      setSearchValues(originalSearch);
      keepSearchValues = originalSearch;
    }
  }, [originalSearch]);

  const handleChange = (event: AttributeSearchEvent) => {
    const { value } = event;
    setSearchValues(value);
  };

  const handleClear = () => setSearchValues([]);

  const handleSearch = (
    event: AttributeSearchEvent,
    error?: Record<string, string>
  ) => {
    if (!isEmpty(error) || !event.value.length) return;
    batch(() => {
      dispatch(
        batchManagementActions.onChangeBatchSearch({ data: event.value })
      );
      dispatch(batchManagementActions.searchBatch());
      keepSearchValues = event.value;
    });
  };

  const translateCombineField = () => {
    return NO_COMBINE_FIELDS_BATCH_SEARCH.map(item => ({
      ...item,
      filterMessage: t(item.filterMessage)
    }));
  };

  return (
    <AttributeSearch
      small={true}
      value={searchValues}
      orderBy="asc"
      ref={attributeSearchRef}
      placeholder={t(I18N_TEXT.TYPE_TO_ADD_PARAMETER)}
      noCombineFields={translateCombineField()}
      onClear={handleClear}
      onChange={handleChange}
      onSearch={handleSearch}
      data={data}
      validator={(data: AttributeSearchFieldModel) => attrValidations(data, t)}
      clearTooltipProps={{
        element: t(I18N_TEXT.CLEAR_ALL_CRITERIA)
      }}
    />
  );
};

export default SearchBatch;
