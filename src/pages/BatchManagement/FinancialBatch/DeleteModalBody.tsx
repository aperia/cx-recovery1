import React from 'react';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { I18N_TEXT } from 'app/constants';

interface DeleteBatchModalProps {
  batchNumber: string;
}
const DeleteBatchModal: React.FC<DeleteBatchModalProps> = ({
  batchNumber = ''
}) => {
  const { t } = useTranslation();
  return (
    <>
      <p className="mt-4">
        {t(I18N_TEXT.DELETE_BATCH_MESSAGE)}{' '}
        <span className="fw-500">{batchNumber}?</span>
      </p>
    </>
  );
};

export default DeleteBatchModal;
