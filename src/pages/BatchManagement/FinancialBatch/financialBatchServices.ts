import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { BatchStatus, IBatch } from '../types';
import { financialBatchController } from '../__mock__/batchManagementController';

const financialBatchServices = {
  getFinancialBatch() {
    return financialBatchController.getFinancialBatch();
  },
  deleteBatch: (id: string) => {
    return financialBatchController.deleteBatch(id);
  },
  addBatch: (newBatch: IBatch) => {
    return financialBatchController.addBatch(newBatch);
  },
  editBatch: (id: string, dataChange: IBatch) => {
    return financialBatchController.editBatch(id, dataChange);
  },
  updateBatchStatusById: (id: string, batchStatus: BatchStatus) => {
    return financialBatchController.updateBatchStatusById(id, batchStatus);
  },
  searchBatch: (data: AttributeSearchValue[]) => {
    return financialBatchController.searchBatch(data);
  }
};

export default financialBatchServices;
