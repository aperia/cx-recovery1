import React, { useCallback, useMemo, useState } from 'react';

// components
import { Grid, Popover } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants & types
import { GRID_IDS } from 'pages/BatchManagement/constants';
import { I18N_TEXT } from 'app/constants';
import { Subitems } from 'pages/BatchManagement/types';

// helpers
import { formatCurrency } from 'app/helpers';
import useRefData from 'app/hooks/useRefData';
import { getDescriptionFromCode } from 'pages/__commons/helpers';

export interface ViewFTCProps {
  subitems: Subitems[];
}
export const ViewFTC: React.FC<ViewFTCProps> = ({ subitems }) => {
  const { t } = useTranslation();

  const [showPopover, setshowPopover] = useState<boolean>(false);
  const reserveOptions: RefDataValue[] = useRefData('reserve');

  const openPopoverHandler = useCallback(() => {
    setshowPopover(prev => !prev);
  }, [setshowPopover]);

  const renderAmount = useCallback((item: Subitems) => {
    if (item.toPayAmount || item.transactionAmountPaymentEntry) {
      if (item.toPayAmount) {
        return <span>{formatCurrency(item.toPayAmount)}</span>;
      }
      if (item.transactionAmountPaymentEntry) {
        return (
          <span>{formatCurrency(item.transactionAmountPaymentEntry)}</span>
        );
      }
    }
    return <span className="content-empty"></span>;
  }, []);

  const renderFTCValue = useCallback(
    (item: Subitems) => {
      if (item.ftc) {
        return (
          <>
            <p className="fw-400">{item.ftc}</p>
            <p className="fw-400 color-grey text-capitalize">
              {getDescriptionFromCode(reserveOptions, item.ftc, false)}
            </p>
          </>
        );
      }
      return <span className="content-empty"></span>;
    },
    [reserveOptions]
  );

  const columns = useMemo(
    () => [
      {
        Header: t(I18N_TEXT.FTC),
        accessor: (item: Subitems) => <>{renderFTCValue(item)}</>,
        id: GRID_IDS.FTC,
        width: 190,
        autoWidth: true
      },
      {
        Header: t(I18N_TEXT.POIC),
        accessor: GRID_IDS.POIC,
        id: GRID_IDS.POIC,
        width: 72
      },
      {
        Header: t(I18N_TEXT.BALANCE_AMT),
        accessor: (item: Subitems) => (
          <span>{formatCurrency(item.balanceAmount)}</span>
        ),
        id: GRID_IDS.BALANCE_AMOUNT,
        width: 123,
        cellProps: { className: 'text-right' }
      },
      {
        Header: t(I18N_TEXT.TO_PAY_AMT),
        accessor: (item: Subitems) => <>{renderAmount(item)}</>,
        id: GRID_IDS.TO_PAY_AMOUNT,
        width: 144,
        cellProps: { className: 'text-right' }
      }
    ],
    [t, renderFTCValue, renderAmount]
  );

  return (
    <Popover
      containerClassName={''}
      size="xl"
      element={
        <>
          <h5>{t(I18N_TEXT.FTC_DETAILS)}</h5>
          <div className="mt-16">
            <Grid columns={columns} data={subitems} />
          </div>
        </>
      }
      opened={showPopover}
      onVisibilityChange={openPopoverHandler}
    >
      <span className="link" onClick={openPopoverHandler}>
        {t(I18N_TEXT.VIEW_FTC)}
      </span>
    </Popover>
  );
};

export default ViewFTC;
