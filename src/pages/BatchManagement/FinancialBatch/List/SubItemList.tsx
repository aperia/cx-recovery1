import React from 'react';

// components
import { SubItem } from './SubItem';

// redux
import { selectorFinancialBatchDetails } from 'pages/BatchManagement/_redux/financialBatch/selectors';
import { useSelector } from 'app/hooks';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_TEXT } from 'app/constants';

interface SubItemListProps {
  className?: string;
}
const SubItemList: React.FC<SubItemListProps> = ({ className = '' }) => {
  const { t } = useTranslation();
  const financialBatchDetails = useSelector(selectorFinancialBatchDetails);

  return (
    <div className={className}>
      <h5>{t(I18N_TEXT.ITEM_LIST)}</h5>
      {financialBatchDetails?.itemsPayment?.map((item, idx) => (
        <SubItem
          key={item.itemNumber}
          index={idx + 1}
          isLastItem={financialBatchDetails?.itemsPayment?.length === idx + 1}
          item={item}
        />
      ))}
    </div>
  );
};

export default SubItemList;
