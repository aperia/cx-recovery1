import React from 'react';

// utils
import ItemBatchList from './Item';
import { IBatch } from 'pages/BatchManagement/types';

interface IBatchList {
  batchList: IBatch[];
}

const BatchList: React.FC<IBatchList> = ({ batchList }) => {
  return (
    <div className="mt-16">
      <ItemBatchList data={batchList} />
    </div>
  );
};
export default BatchList;
