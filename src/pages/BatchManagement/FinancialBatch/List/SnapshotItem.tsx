import React from 'react';

// components
import { Icon } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelector } from 'app/hooks';
import useRefData from 'app/hooks/useRefData';

// redux
import { selectorFinancialBatchDetails } from 'pages/BatchManagement/_redux/financialBatch/selectors';

// helpers
import { getDescriptionFromCode } from 'pages/__commons/helpers';
import { sumbArrObjPropertyValue } from '../helpers';
import { formatCurrency } from 'app/helpers';

// constants
import { ColorBadge } from 'app/constants/enums';
import { I18N_TEXT } from 'app/constants';

interface SnapshotItemProps {
  className?: string;
  batchStatus: string;
  isMatch: boolean;
  batchNumber: string;
  batchType: string;
}
const SnapshotItem: React.FC<SnapshotItemProps> = ({
  batchStatus: batchStatusProp = '',
  isMatch = false,
  batchNumber = '',
  batchType: batchTypeProp = '',
  className = ''
}) => {
  const { t } = useTranslation();
  const financialBatchDetails = useSelector(selectorFinancialBatchDetails);
  const batchType = useRefData('batchType');
  const batchStatus = useRefData('batchStatus');

  return (
    <div className={className}>
      <div className="d-flex justify-content-between align-items-center">
        <h5>{batchNumber}</h5>
        <p className={`badge badge-${ColorBadge[batchStatusProp]}`}>
          {getDescriptionFromCode(batchStatus, batchStatusProp, false)}
        </p>
      </div>
      <div className="mt-8 mb-16">
        {isMatch ? (
          <>
            <Icon name="success" className="color-green-d16" />
            <span className="color-green-d16 pl-8">
              {t(I18N_TEXT.BATCH_AND_CURRENT_MATCHED)}
            </span>
          </>
        ) : (
          <>
            <Icon name="warning" className="color-orange-d16" />
            <span className="color-orange-d16 pl-8">
              {t(I18N_TEXT.BATCH_AND_CURRENT_NOT_MATCH)}
            </span>
          </>
        )}
      </div>

      <div className="row">
        <div className="col-5">
          <h6 className="fw-400 color-grey mb-4">{t('txt_batch_type')}</h6>
          <span>{getDescriptionFromCode(batchType, 'P', true)}</span>
        </div>

        <div className="col-7">
          <h6 className="fw-400 color-grey mb-4">
            {t('txt_batch_current_item_dot_amount')}
          </h6>
          {!!financialBatchDetails?.itemsPayment?.length ? (
            `${financialBatchDetails?.itemsPayment.length} • ${formatCurrency(
              (
                parseInt(
                  sumbArrObjPropertyValue(
                    financialBatchDetails?.itemsPayment,
                    'totalToPayAmount'
                  )
                ) +
                parseInt(
                  sumbArrObjPropertyValue(
                    financialBatchDetails?.itemsPayment,
                    'transactionAmountPaymentEntry'
                  )
                )
              ).toString()
            )}`
          ) : (
            <span className="content-empty"></span>
          )}
        </div>
      </div>
    </div>
  );
};

export default SnapshotItem;
