import React, { useCallback } from 'react';
// components
import { Icon } from 'app/_libraries/_dls/components';
import MaskAccNbr from 'app/components/MaskAccNbr';
import ViewFTC from './ViewFTC';

// types
import { ItemPaymentList } from 'pages/BatchManagement/types';

// helpers
import { mapTransactionType } from '../helpers';
import { formatCurrency } from 'app/helpers';

interface SubItemProps {
  index: number;
  isLastItem: boolean;
  item: ItemPaymentList;
}
export const SubItem: React.FC<SubItemProps> = ({
  index = 0,
  isLastItem = false,
  item
}) => {
  const renderValueAmount = useCallback((item: ItemPaymentList) => {
    if (item.transactionAmountPaymentEntry) {
      return formatCurrency(item.transactionAmountPaymentEntry);
    }
    if (item.totalToPayAmount) {
      return formatCurrency(item.totalToPayAmount);
    }
    return <span className="content-empty"></span>;
  }, []);

  return (
    <div className="mt-16">
      <div className="d-flex align-items-center justify-content-between">
        <div className="d-flex">
          <div className="item-index mr-12">{index}</div>
          <MaskAccNbr
            label=""
            value={{
              accountNumberUnMask: item.accNumber?.accountNumberUnMask,
              accountNumber: item.accNumber?.accountNumber
            }}
          />
        </div>
        <div className="d-flex align-items-center">
          <Icon name="collection-information" className="color-grey-l08 mr-8" />
          <span className="mr-8">{renderValueAmount(item)}</span>

          <ViewFTC subitems={item.subitems || []} />
        </div>
      </div>
      <p className="color-grey ml-36 mt-4">
        {item.transactionDate ? (
          item.transactionDate +
          ' • ' +
          mapTransactionType(item.transactionType as string)
        ) : (
          <span className="content-empty"></span>
        )}
      </p>
      {isLastItem ? null : <div className=" divider-dashed mt-16"></div>}
    </div>
  );
};
