import React, { useLayoutEffect, useRef } from 'react';
import { useDispatch } from 'react-redux';

// components
import { Button, SimpleBar } from 'app/_libraries/_dls/components';
import SubItemList from './SubItemList';
import SnapshotItem from './SnapshotItem';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import { batchManagementActions } from 'pages/BatchManagement/_redux';
import { useSelector } from 'app/hooks';

// constants
import { I18N_TEXT } from 'app/constants';
import {
  selectedIsCollapseItem,
  selectorFinancialBatchDetails
} from 'pages/BatchManagement/_redux/financialBatch/selectors';
import { IBatch } from 'pages/BatchManagement/types';

interface ItemDetailProps {}
export const ItemDetail: React.FC<ItemDetailProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const divRef = useRef<HTMLDivElement | null>(null);

  const snapshotItem: IBatch = useSelector(selectorFinancialBatchDetails);

  const isCollapse: boolean = useSelector(selectedIsCollapseItem);

  const handleCancel = () => {
    dispatch(
      batchManagementActions.onChangeCollapseItem({ isCollapse: !isCollapse })
    );
  };

  useLayoutEffect(() => {
    divRef.current && (divRef.current.style.height = `calc(100vh - 309px)`);
  }, []);

  return (
    <>
      <div className="bg-white" ref={divRef}>
        <SimpleBar>
          <SnapshotItem
            className="bg-light-l20 w-100 border-bottom p-24"
            batchNumber={snapshotItem?.batchNumber}
            batchStatus={snapshotItem?.batchStatus}
            batchType={snapshotItem?.batchType}
            isMatch={snapshotItem?.isMatch}
          />
          <SubItemList className="bg-white w-100 p-24" />
        </SimpleBar>
      </div>

      <div className="group-button-footer bg-white text-right">
        <Button variant="secondary" onClick={handleCancel}>
          {t(I18N_TEXT.CLOSE)}
        </Button>
      </div>
    </>
  );
};
