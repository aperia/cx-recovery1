import React from 'react';

// component
import Button from 'app/_libraries/_dls/components/Button';

// utils
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { I18N_TEXT } from 'app/constants';
import { useSelector } from 'react-redux';
import { getFinancialBatchList } from 'pages/BatchManagement/_redux/financialBatch/selectors';

export interface CountItemProps {
  handleClearAndReset: () => void;
}
const CountItem: React.FC<CountItemProps> = ({ handleClearAndReset }) => {
  const { t } = useTranslation();
  const data = useSelector(getFinancialBatchList);

  return (
    <div className="d-flex justify-content-between align-items-center mt-16">
      <p>{t(I18N_TEXT.RESULT_FOUNDED, { count: data.length })}</p>
      <Button
        onClick={handleClearAndReset}
        variant="outline-primary"
        className="mr-n8"
        size="sm"
      >
        {t(I18N_TEXT.CLEAR_AND_RESET)}
      </Button>
    </div>
  );
};
export default CountItem;
