import React, { useCallback } from 'react';
import { batch, useDispatch } from 'react-redux';

// component
import Button from 'app/_libraries/_dls/components/Button';
import {
  Bubble,
  DropdownList,
  Icon,
  Tooltip
} from 'app/_libraries/_dls/components';
import DeleteBatchModal from '../DeleteModalBody';
import UpdateStatusModalBody from '../UpdateStatusModalBody';
import TextShowInformation from 'app/components/_dof_controls/controls/TextViewControl/TextShowInformation';

// redux
import { batchManagementActions } from 'pages/BatchManagement/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { selectedIsCollapseItem } from 'pages/BatchManagement/_redux/financialBatch/selectors';
import { useSelector } from 'app/hooks';

// utils
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';

import { formatCurrency, isArrayHasValue, isEmpty } from 'app/helpers';
import {
  getDescriptionFromCode,
  getRefDataFromCode
} from 'pages/__commons/helpers';
import { sumbArrObjPropertyValue } from '../helpers';

// constants & types
import { ColorBadge } from 'app/constants/enums';
import { I18N_TEXT } from 'app/constants';
import { RootModalOptions } from 'pages/__commons/RootModal/types';
import { IBatch } from '../../types';
import { BATCH_STATUS, CREATED_BY } from 'pages/BatchManagement/constants';

interface ItemBatchListProps {
  data: IBatch[];
}

const ItemBatchList: React.FC<ItemBatchListProps> = ({ data }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { data: batchStatus } = useGetRefDataQuery('batchStatus');
  const { data: batchType } = useGetRefDataQuery('batchType');
  const { data: sourceId } = useGetRefDataQuery('sourceId');

  const isCollapse: boolean = useSelector(selectedIsCollapseItem);

  const handleExpand = (item: IBatch) => {
    batch(() => {
      dispatch(
        batchManagementActions.onChangeCollapseItem({ isCollapse: !isCollapse })
      );
      dispatch(batchManagementActions.setFinancialBatchDetails(item));
    });
  };

  const showModalDelete = useCallback(
    (id = '', batchNumber = '') => {
      const options: RootModalOptions = {
        autoClose: false,
        size: 'xs',
        title: t(I18N_TEXT.CONFIRM_DELETE_BATCH),
        body: <DeleteBatchModal batchNumber={batchNumber} />,
        onConfirm: () => {
          dispatch(batchManagementActions.deleteBatch({ id }));
        },
        btnConfirmText: t(I18N_TEXT.DELETE),
        onCancel: () => dispatch(rootModalActions.close())
      };
      dispatch(rootModalActions.open(options));
    },
    [t, dispatch]
  );

  const showModalUpdateStatus = useCallback(
    (id = '', nextStatus = {}) => {
      const options: RootModalOptions = {
        autoClose: false,
        size: 'xs',
        title: t(I18N_TEXT.CONFIRM_UPDATE_STATUS),
        body: <UpdateStatusModalBody nextStatus={nextStatus.description} />,
        onConfirm: () => {
          dispatch(
            batchManagementActions.updateBatchStatusById({
              id,
              batchStatus: nextStatus.value
            })
          );
        },
        btnConfirmText: t(I18N_TEXT.UPDATE),
        onCancel: () => dispatch(rootModalActions.close())
      };
      dispatch(rootModalActions.open(options));
    },
    [t, dispatch]
  );

  const handleTriggerEdit = (item: IBatch) => {
    dispatch(
      batchManagementActions.triggerEditBatch({
        ...item
      } as IBatch)
    );
  };

  const renderCreateBy = (item: IBatch) => {
    if (item.createdBy === CREATED_BY.OPERATOR) {
      return (
        <div className="d-flex align-items-center">
          <Bubble small name={item?.userId || ''} />
          <span className="ml-4">{item?.userId || ''}</span>
        </div>
      );
    }
    return (
      <TextShowInformation
        value={item?.sourceId || ''}
        id={item?.sourceId || ''}
        description={getDescriptionFromCode(
          sourceId?.sourceId,
          item?.sourceId,
          false
        )}
        onlyTooltip={true}
      />
    );
  };

  const renderBatchStatus = useCallback(
    (itemBatchStatus: string) => {
      if (!batchStatus) return [];
      let batchStatusOptions = batchStatus['batchStatus'];

      if (itemBatchStatus === BATCH_STATUS.ERROR) {
        batchStatusOptions = batchStatusOptions.filter(
          item =>
            item.value === BATCH_STATUS.OPEN ||
            item.value === BATCH_STATUS.ERROR
        );
      }
      if (itemBatchStatus !== BATCH_STATUS.ERROR) {
        batchStatusOptions = batchStatusOptions.filter(
          item => item.value !== BATCH_STATUS.ERROR
        );
      }

      return batchStatusOptions.map((item: RefDataValue) => (
        <DropdownList.Item
          key={item.value}
          variant="badge"
          label={item.description}
          badgeProps={{
            color: ColorBadge[item.value],
            children: item.description
          }}
          value={item}
          hidden={item.value === itemBatchStatus}
        />
      ));
    },
    [batchStatus]
  );

  const renderButton = (item: IBatch) => {
    if (item.batchStatus === BATCH_STATUS.READY_TO_POST) {
      return (
        <>
          <Tooltip placement="top" element={t('txt_cannot_delete_batch_ready')}>
            <Button
              size="sm"
              disabled={true}
              variant="outline-danger"
              onClick={() => showModalDelete(item?.id, item.batchNumber)}
            >
              {t(I18N_TEXT.DELETE)}
            </Button>
          </Tooltip>
          <Tooltip
            triggerClassName="mr-n8 ml-8"
            placement="top"
            element={t('txt_cannot_edit_batch_ready')}
          >
            <Button
              size="sm"
              disabled={true}
              variant="outline-primary"
              onClick={() => handleTriggerEdit(item)}
            >
              {t(I18N_TEXT.EDIT)}
            </Button>
          </Tooltip>
        </>
      );
    }
    return (
      <>
        <Button
          size="sm"
          disabled={item.batchStatus !== BATCH_STATUS.OPEN}
          variant="outline-danger"
          onClick={() => showModalDelete(item?.id, item.batchNumber)}
        >
          {t(I18N_TEXT.DELETE)}
        </Button>
        <Button
          size="sm"
          className="mr-n8"
          disabled={item.batchStatus !== BATCH_STATUS.OPEN}
          variant="outline-primary"
          onClick={() => handleTriggerEdit(item)}
        >
          {t(I18N_TEXT.EDIT)}
        </Button>
      </>
    );
  };

  const handleChangeStatus = (
    id: string,
    currentStatus: string,
    event: DropdownBaseChangeEvent
  ) => {
    const { value, description } = event.target.value;
    if (currentStatus === value) return;
    if (
      currentStatus === BATCH_STATUS.OPEN &&
      value === BATCH_STATUS.READY_TO_POST
    ) {
      // Just show modal when change status from "Open" to "Ready to Post"
      showModalUpdateStatus(id, { value, description });
      return;
    }

    dispatch(
      batchManagementActions.updateBatchStatusById({
        id,
        batchStatus: value
      })
    );
  };

  const renderMainLayout = () => {
    if (!isArrayHasValue(data)) return null;
    return data.map((item, index) => (
      <div key={index} className="mt-16 card br-radius-8 pt-12 px-16 pb-16">
        <div className="d-flex justify-content-between align-items-center">
          <div className="d-flex  align-items-center">
            <p className="mr-16 fw-500">{item.batchNumber}</p>
            {!item.isMatch && (
              <>
                <Icon name="warning" className="color-orange-d16" />
                <span className="d-md-inline-block d-lg-none d-xl-inline-block color-orange-d16 pl-8">
                  {t(I18N_TEXT.BATCH_AND_CURRENT_NOT_MATCH)}
                </span>
              </>
            )}
          </div>
          <div>
            <DropdownList
              value={getRefDataFromCode(
                batchStatus?.batchStatus,
                item.batchStatus
              )}
              variant="no-border"
              textField="description"
              onChange={e =>
                handleChangeStatus(item?.id || '', item.batchStatus, e)
              }
              className="mr-8"
            >
              {renderBatchStatus(item.batchStatus)}
            </DropdownList>
            {renderButton(item)}
          </div>
        </div>
        <div className="row-batch mt-8">
          <div className="col-batch-item">
            <h6 className="fw-400 color-grey mb-4">{t('txt_batch_date')}</h6>
            <span>{item.batchDate}</span>
          </div>
          <div className="col-batch-item">
            <h6 className="fw-400 color-grey mb-4">{t('txt_batch_type')}</h6>
            <span>
              {`${item.batchType} - ${getDescriptionFromCode(
                batchType?.batchType,
                item.batchType,
                false
              )}`}
            </span>
          </div>
          <div className="col-batch-item">
            <h6 className="fw-400 color-grey mb-4">{t('txt_create_by')}</h6>
            {renderCreateBy(item)}
          </div>
          <div className="col-batch-item">
            <h6 className="fw-400 color-grey mb-4">
              {t('txt_batch_item_dot_amount')}
            </h6>
            {`${item.numberOfItems} • ${formatCurrency(
              item.transactionAmount
            )}`}
          </div>
          <div className="col-batch-item">
            <h6 className="fw-400 color-grey mb-4">
              {t('txt_batch_current_item_dot_amount')}
            </h6>
            <div className="d-flex align-items-center">
              {!isEmpty(item?.itemsPayment) ? (
                <>
                  {`${item?.currentItemType} • ${formatCurrency(
                    (
                      parseInt(
                        sumbArrObjPropertyValue(
                          item?.itemsPayment,
                          'totalToPayAmount'
                        )
                      ) +
                      parseInt(
                        sumbArrObjPropertyValue(
                          item?.itemsPayment,
                          'transactionAmountPaymentEntry'
                        )
                      )
                    ).toString()
                  )}`}

                  {!isEmpty(item?.itemsPayment) &&
                    item.currentItemType &&
                    item.currentItemAmount && (
                      <Button
                        size="sm"
                        variant="icon-secondary"
                        className="d-flex justify-content-center align-items-center ml-8"
                        onClick={() => handleExpand(item)}
                      >
                        <Icon
                          name="information"
                          size="4x"
                          className="color-grey-l16"
                        />
                      </Button>
                    )}
                </>
              ) : (
                <span className="content-empty"></span>
              )}
            </div>
          </div>
        </div>
      </div>
    ));
  };
  return <>{renderMainLayout()}</>;
};

export default ItemBatchList;
