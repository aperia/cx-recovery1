import { useDispatch } from 'react-redux';
import * as Yup from 'yup';

// redux
import { selectorSubitemsModal } from 'pages/BatchManagement/_redux/financialBatch/selectors';

// hooks
import { useSelector } from 'app/hooks';

// constants
import {
  ITEM_PAYMENT_ENTRY_FIELD,
  POIC_BALANCE_AMOUNT
} from 'pages/BatchManagement/constants';
import { ModalSubitems } from 'pages/BatchManagement/types';

// helpers
import { get } from 'app/helpers';
import { useFormik } from 'formik';
import { batchManagementActions } from 'pages/BatchManagement/_redux';
import { useCallback, useMemo } from 'react';

export const useAddEditSubItemModal = () => {
  const dispatch = useDispatch();

  const subitemsModal: ModalSubitems = useSelector(selectorSubitemsModal);

  const isAddModal = !subitemsModal.modalProps?.ftc;

  const initialValues = useMemo(
    () => ({
      ftc: subitemsModal?.modalProps?.ftc || '',
      toPayAmount: subitemsModal?.modalProps?.toPayAmount || ''
    }),
    [subitemsModal?.modalProps]
  );

  const validationSchema = Yup.object().shape({
    [ITEM_PAYMENT_ENTRY_FIELD.FTC.name]: Yup.string().required(
      'txt_ftc_is_required'
    ),
    [ITEM_PAYMENT_ENTRY_FIELD.TO_PAY_AMOUNT.name]: Yup.number().when(
      [ITEM_PAYMENT_ENTRY_FIELD.FTC.name],
      (ftc: string) => {
        const balanceAmount = get(
          POIC_BALANCE_AMOUNT,
          [ftc, 'balanceAmount'],
          '0'
        );
        return Yup.number()
          .required('txt_to_pay_amount_is_required')
          .max(
            parseInt(balanceAmount),
            'txt_to_pay_amount_must_be_less_than_or_equal_to_balance_amount'
          );
      }
    )
  });

  const formik = useFormik({
    enableReinitialize: true,
    initialValues,
    validationSchema,
    validateOnMount: true,
    onSubmit: values => {
      const poicBalanceAmount = mapPOICandBalanceAmount(values.ftc);
      if (isAddModal) {
        dispatch(
          batchManagementActions.addSubitemPayment({
            ...Object.assign(poicBalanceAmount, values),
            subitemID: Math.floor(Math.random() * 1000).toString()
          })
        );
      } else {
        dispatch(
          batchManagementActions.editSubitemPayment({
            ...Object.assign(poicBalanceAmount, values),
            subitemID: subitemsModal.modalProps?.subitemID
          })
        );
      }
      onCloseModal();
    }
  });

  const { resetForm } = formik;

  const onCloseModal = useCallback(() => {
    dispatch(batchManagementActions.removeModalSubitemPayment());
    resetForm();
  }, [dispatch, resetForm]);

  const mapPOICandBalanceAmount = useCallback((ftc: string) => {
    const poic = get(POIC_BALANCE_AMOUNT, [ftc, 'POIC'], '');
    const balanceAmount = get(POIC_BALANCE_AMOUNT, [ftc, 'balanceAmount'], '');
    return { poic, balanceAmount };
  }, []);

  return {
    formik,
    subitemsModal,
    isAddModal,
    onCloseModal,
    mapPOICandBalanceAmount
  };
};
