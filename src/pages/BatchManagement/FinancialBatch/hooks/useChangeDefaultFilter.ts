import { useCallback, useEffect } from 'react';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import { useDispatch } from 'react-redux';
import { batchManagementActions } from 'pages/BatchManagement/_redux';

const useSetDefaultFilter = () => {
  const dispatch = useDispatch();
  const { data: batchTypes } = useGetRefDataQuery('batchType');

  const { data: batchStatus } = useGetRefDataQuery('batchStatus');
  const { data: sortByBatch } = useGetRefDataQuery('sortByBatch');
  const { data: orderBy } = useGetRefDataQuery('orderBy');

  const handleResetBatchTypes = useCallback(() => {
    if (!batchTypes) return undefined;
    dispatch(
      batchManagementActions.onChangeBatchType({ data: batchTypes?.batchType })
    );
  }, [dispatch, batchTypes]);

  const handleResetBatchStatus = useCallback(() => {
    if (!batchStatus) return undefined;
    dispatch(
      batchManagementActions.onChangeBatchStatus({
        data: batchStatus?.batchStatus
      })
    );
  }, [dispatch, batchStatus]);

  useEffect(() => {
    handleResetBatchTypes();
  }, [handleResetBatchTypes]);

  useEffect(() => {
    handleResetBatchStatus();
  }, [handleResetBatchStatus]);

  useEffect(() => {
    if (!sortByBatch) return undefined;
    dispatch(
      batchManagementActions.onChangeBatchSort({
        data: sortByBatch?.sortByBatch?.[0]
      })
    );
  }, [dispatch, sortByBatch]);

  useEffect(() => {
    if (!orderBy) return undefined;
    dispatch(
      batchManagementActions.onChangeBatchOrder({
        data: orderBy?.orderBy?.[1]
      })
    );
  }, [dispatch, orderBy]);

  return { handleResetBatchTypes, handleResetBatchStatus };
};

export default useSetDefaultFilter;
