import { useCallback, useMemo } from 'react';
import * as Yup from 'yup';
import { useDispatch } from 'react-redux';

// hooks
import { useSelector } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// redux
import {
  selectedFormActionBatch,
  selectorFormValueBatch
} from 'pages/BatchManagement/_redux/financialBatch/selectors';
import { batchManagementActions } from 'pages/BatchManagement/_redux';

// helpers
import { prepareBatchDataSubmit } from '../helpers';

// constants & types
import {
  AutoFeeOrSpread,
  BATCH_TYPE_PAYMENTS,
  CREATED_BY,
  FINANCIAL_BATCH_FORM,
  INIT_BATCH_FORM_VALUE,
  ITEM_PAYMENT_ENTRY_FIELD
} from 'pages/BatchManagement/constants';
import { I18N_TEXT } from 'app/constants';
import { FormType } from 'app/constants/enums';
import { FormAction } from 'pages/AccountManagement/types';
import { IBatch } from 'pages/BatchManagement/types';

export const useBatchForm = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const formAction: FormAction = useSelector(selectedFormActionBatch);
  const formValue: IBatch = useSelector(selectorFormValueBatch);

  const validationSchema = useMemo(() => {
    return Yup.object().shape({
      [FINANCIAL_BATCH_FORM.ADD_BATCH.batchDate.name]: Yup.string().required(
        t(I18N_TEXT.BATCH_DATE_IS_REQUIRED)
      ),
      [FINANCIAL_BATCH_FORM.ADD_BATCH.createdBy.name]: Yup.string().required(
        t(I18N_TEXT.CREATED_BY_IS_REQUIRED)
      ),

      [FINANCIAL_BATCH_FORM.ADD_BATCH.userId.name]: Yup.string().when(
        [FINANCIAL_BATCH_FORM.ADD_BATCH.createdBy.name],
        (value: string, schema: Yup.AnySchema) =>
          value === CREATED_BY.OPERATOR
            ? schema.required(t(I18N_TEXT.USER_ID_IS_REQUIRED))
            : schema
      ),

      [FINANCIAL_BATCH_FORM.ADD_BATCH.sourceId.name]: Yup.string().when(
        [FINANCIAL_BATCH_FORM.ADD_BATCH.createdBy.name],
        (value: string, schema: Yup.AnySchema) =>
          value === CREATED_BY.SOURCE
            ? schema.required(t(I18N_TEXT.SOURCE_ID_IS_REQUIRED))
            : schema
      ),

      [FINANCIAL_BATCH_FORM.ADD_BATCH.batchType.name]: Yup.string().required(
        t(I18N_TEXT.BATCH_TYPE_IS_REQUIRED)
      ),

      [FINANCIAL_BATCH_FORM.ADD_BATCH.numberOfItems.name]:
        Yup.string().required(t(I18N_TEXT.NUMBER_OF_ITEMS_IS_REQUIRED)),

      [FINANCIAL_BATCH_FORM.ADD_BATCH.transactionAmount.name]:
        Yup.string().required(t(I18N_TEXT.TRANSACTION_AMOUNT_IS_REQUIRED)),

      [ITEM_PAYMENT_ENTRY_FIELD.TRANSACTION_DATE.name]: Yup.string().when(
        [FINANCIAL_BATCH_FORM.ADD_BATCH.batchType.name],
        (value: string, schema: Yup.AnySchema) =>
          value === BATCH_TYPE_PAYMENTS
            ? schema.required(t(I18N_TEXT.TRANSACTION_DATE_IS_REQUIRED))
            : schema
      ),

      [ITEM_PAYMENT_ENTRY_FIELD.TRANSACTION_TYPE.name]: Yup.string().when(
        [FINANCIAL_BATCH_FORM.ADD_BATCH.batchType.name],
        (value: string, schema: Yup.AnySchema) =>
          value === BATCH_TYPE_PAYMENTS
            ? schema.required(t(I18N_TEXT.TRANSACTION_TYPE_IS_REQUIRED))
            : schema
      ),
      [ITEM_PAYMENT_ENTRY_FIELD.MANUAL_AGENCY_FEE.name]: Yup.string().when(
        [
          FINANCIAL_BATCH_FORM.ADD_BATCH.batchType.name,
          ITEM_PAYMENT_ENTRY_FIELD.AUTO_FEE.name
        ],
        {
          is: (batchTypeValue: string, autoFeeValue: string) =>
            batchTypeValue === BATCH_TYPE_PAYMENTS &&
            autoFeeValue === AutoFeeOrSpread.NO,
          then: Yup.string().required(
            t(I18N_TEXT.MANUAL_AGENCY_FEE_IS_REQUIRED)
          )
        }
      ),
      [ITEM_PAYMENT_ENTRY_FIELD.TRANSACTION_AMOUNT.name]: Yup.string().when(
        [
          FINANCIAL_BATCH_FORM.ADD_BATCH.batchType.name,
          ITEM_PAYMENT_ENTRY_FIELD.AUTO_SPREAD.name
        ],
        {
          is: (batchTypeValue: string, autoSpreadValue: string) =>
            batchTypeValue === BATCH_TYPE_PAYMENTS &&
            autoSpreadValue === AutoFeeOrSpread.YES,
          then: Yup.string().required(
            t(I18N_TEXT.TRANSACTION_AMOUNT_IS_REQUIRED)
          )
        }
      )
    });
  }, [t]);

  const onSubmit = useCallback(
    (values: IBatch) => {
      const formattedData = prepareBatchDataSubmit(values);
      if (formAction === FormType[0]) {
        // Handle Add
        dispatch(
          batchManagementActions.addBatch({
            newBatch: formattedData
          })
        );
      }

      if (formAction === FormType[1]) {
        // Handle Edit
        dispatch(
          batchManagementActions.editBatch({
            id: values?.id || '001',
            dataChange: formattedData
          })
        );
      }
    },
    [dispatch, formAction]
  );

  return {
    initialValues:
      formAction === FormType[0]
        ? (INIT_BATCH_FORM_VALUE as IBatch)
        : formValue,
    onSubmit,
    validationSchema
  };
};
