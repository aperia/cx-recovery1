import { useFormikContext } from 'formik';

// redux
import { batch, useDispatch } from 'react-redux';
import {
  selectorItemPaymentList,
  selectorItemPaymentListFilterSortOrder,
  selectorSelectedItem
} from 'pages/BatchManagement/_redux/financialBatch/selectors';
import { batchManagementActions } from 'pages/BatchManagement/_redux';

// hooks
import { useSelector } from 'app/hooks';

// helpers
import { isDate, isObject } from 'app/helpers';

// types & constants
import { IBatch, ItemPaymentList } from 'pages/BatchManagement/types';
import {
  AutoFeeOrSpread,
  ITEM_PAYMENT_ENTRY_FIELD
} from 'pages/BatchManagement/constants';

export const useItemsPaymentForm = () => {
  const dispatch = useDispatch();

  const itemList: ItemPaymentList[] = useSelector(selectorItemPaymentList);
  const itemListFilterSortOrder: ItemPaymentList[] = useSelector(
    selectorItemPaymentListFilterSortOrder
  );
  const selectedItem: ItemPaymentList = useSelector(selectorSelectedItem);

  const { values, setValues, setTouched, handleChange } =
    useFormikContext<IBatch>();

  const handleChangeFormField = (event: React.ChangeEvent<any>) => {
    const { name, value } = event.target;
    const _itemList = [...itemList];
    const indexItem = _itemList?.findIndex(
      _item => _item.itemNumber === selectedItem.itemNumber
    );
    const _item = {
      ..._itemList[indexItem],
      [name]:
        isObject(value) && !isDate(value)
          ? (value as RefDataValue)?.value
          : value,
      ...(name === ITEM_PAYMENT_ENTRY_FIELD.AUTO_SPREAD.name && {
        subitems: [],
        totalToPayAmount: ''
      }),
      ...(name === ITEM_PAYMENT_ENTRY_FIELD.AUTO_SPREAD.name &&
        value === AutoFeeOrSpread.NO && {
          transactionAmountPaymentEntry: ''
        }),
      ...(name === ITEM_PAYMENT_ENTRY_FIELD.AUTO_FEE.name &&
        (value as RefDataValue)?.value === AutoFeeOrSpread.YES && {
          manualAgencyFee: ''
        })
    };
    _itemList[indexItem] = _item;

    batch(() => {
      dispatch(batchManagementActions.onSelectedItem(_item));
      dispatch(batchManagementActions.setItemsPayment([..._itemList]));
    });
  };

  return {
    itemList,
    itemListFilterSortOrder,
    selectedItem,
    values,
    setValues,
    setTouched,
    handleChange,
    handleChangeFormField
  };
};
