import React, { useEffect, useLayoutEffect, useRef } from 'react';
import classNames from 'classnames';
import { useDispatch, useSelector } from 'react-redux';

// components
import {
  AttributeSearchValue,
  Button,
  InlineMessage,
  Pagination,
  SimpleBar
} from 'app/_libraries/_dls/components';
import FilterBatch from './Filter';
import BatchList from './List';
import If from 'pages/__commons/If';
import { FinancialBatchModal } from './Form';
import { ItemDetail } from './List/ItemDetail';
import AddEditSubItemModal from './Form/ItemsPayment/ItemPaymentDetails/AddEditSubItemModal';
import { NoDataGrid, SearchNodata } from 'app/components';
import SearchBatch from './Filter/Search';
import CountItem from './List/CountItem';

// utils
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { usePagination } from 'app/hooks/usePagination';
import { IBatch } from '../types';
import {
  getFinancialBatchList,
  selectedFormActionBatch,
  selectedIsCollapseItem,
  selectorBatchStatus,
  selectorBatchType,
  selectorIsNoDataBatchList,
  selectorIsOpenModalAddBatch,
  selectorIsSearchBatch
} from '../_redux/financialBatch/selectors';
import { batchManagementActions } from '../_redux';

import { FormType } from 'app/constants/enums';
import { FormAction } from 'pages/AccountManagement/types';
import { I18N_TEXT, MAX_LENGTH_4 } from 'app/constants';
import { useGetRefDataQuery } from 'pages/__commons/RefData/refDataQuery';
import useSetDefaultFilter from './hooks/useChangeDefaultFilter';
import { isEmpty } from 'lodash';

export const FinancialBatch = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const { handleResetBatchTypes, handleResetBatchStatus } =
    useSetDefaultFilter();
  useGetRefDataQuery('batchType');
  useGetRefDataQuery('batchStatus');
  useGetRefDataQuery('sortByBatch');
  useGetRefDataQuery('orderBy');

  const data: IBatch[] = useSelector(getFinancialBatchList);
  const isSearch: boolean = useSelector(selectorIsSearchBatch);
  const batchTypeValue = useSelector(selectorBatchType);
  const batchStatusValue = useSelector(selectorBatchStatus);
  const isNoData: boolean = useSelector(selectorIsNoDataBatchList);
  const openAddModal: boolean = useSelector(selectorIsOpenModalAddBatch);
  const formAction: FormAction = useSelector(selectedFormActionBatch);

  const contentRightRef = useRef<HTMLDivElement | null>(null);
  const contentLeftRef = useRef<HTMLDivElement | null>(null);

  const isCollapse: boolean = useSelector(selectedIsCollapseItem);

  useEffect(() => {
    dispatch(batchManagementActions.getFinancialBatch());
  }, [dispatch]);

  const {
    total,
    currentPage,
    pageSize,
    currentPageSize,
    gridData,
    onPageChange,
    onPageSizeChange
  } = usePagination(data);

  const handleOpenAddBatchModal = () => {
    dispatch(
      batchManagementActions.onSetModalAddBatch({
        isOpen: true,
        formAction: 'ADD'
      })
    );
  };

  useLayoutEffect(() => {
    contentLeftRef.current &&
      (contentLeftRef.current.style.height = `calc(100vh - 240px)`);
    contentRightRef.current &&
      (contentRightRef.current.style.height = `calc(100vh - 240px)`);
  }, []);

  const handleClearAndReset = () => {
    if (isSearch) {
      dispatch(
        batchManagementActions.onChangeBatchSearch({
          data: [] as AttributeSearchValue[]
        })
      );
    }
    if (batchTypeValue.length !== MAX_LENGTH_4) {
      handleResetBatchTypes();
    }
    if (batchStatusValue.length !== MAX_LENGTH_4) {
      handleResetBatchStatus();
    }

    dispatch(batchManagementActions.getFinancialBatch());
  };

  const renderBatchList = () => {
    if (
      isSearch ||
      batchTypeValue.length !== MAX_LENGTH_4 ||
      batchStatusValue.length !== MAX_LENGTH_4
    ) {
      if (isEmpty(data)) {
        return (
          <SearchNodata
            text={t(I18N_TEXT.ADJUST_YOUR_SEARCH_FILTER)}
            onClearSearch={handleClearAndReset}
          />
        );
      } else {
        return (
          <>
            <CountItem handleClearAndReset={handleClearAndReset} />
            <BatchList batchList={gridData} />
          </>
        );
      }
    } else {
      if (isEmpty(data)) {
        return <NoDataGrid text={t(I18N_TEXT.NO_DATA_TO_DISPLAY)} />;
      } else {
        return <BatchList batchList={gridData} />;
      }
    }
  };

  const renderLayout = () => {
    if (isNoData) {
      return (
        <NoDataGrid text={t(I18N_TEXT.NO_DATA_TO_DISPLAY)}>
          <Button
            className="mt-24"
            size="sm"
            variant="outline-primary"
            onClick={handleOpenAddBatchModal}
          >
            {t(I18N_TEXT.ADD_BATCH)}
          </Button>
        </NoDataGrid>
      );
    }

    return (
      <>
        <InlineMessage className="mt-16" variant="warning">
          <span>{t('txt_batch_update_to')}</span>
        </InlineMessage>
        <FilterBatch />
        {renderBatchList()}
      </>
    );
  };

  return (
    <>
      <div
        className={classNames('content-block content-batch-list', {
          expand: !isCollapse
        })}
      >
        <div ref={contentLeftRef} className="content-left overflow-hidden">
          <SimpleBar>
            <div className="p-24 max-width-lg mx-auto">
              <div className="d-flex align-items-center justify-content-between">
                <h4>{t('txt_final_batch_list')}</h4>
                <SearchBatch />
              </div>

              {renderLayout()}
              <If condition={data?.length > 10}>
                <div className="pt-16">
                  <Pagination
                    totalItem={total}
                    pageSize={pageSize}
                    pageNumber={currentPage}
                    pageSizeValue={currentPageSize}
                    compact
                    onChangePage={onPageChange}
                    onChangePageSize={onPageSizeChange}
                  />
                </div>
              </If>
              <FinancialBatchModal
                isOpen={openAddModal}
                isAdd={formAction === FormType[0]}
              />
              <AddEditSubItemModal />
            </div>
          </SimpleBar>
        </div>
        <div
          ref={contentRightRef}
          className={classNames('content-right', {
            collapsed: isCollapse
          })}
        >
          <ItemDetail />
        </div>
      </div>
    </>
  );
};
