import { AttributeSearchValue } from 'app/_libraries/_dls/components';

export type FieldType<T> = {
  value?: T;
};

export type AttrCommonModel = FieldType<string>;

export interface AttributeSearchFieldModel {
  queueID?: AttrCommonModel;
  queueName?: AttrCommonModel;
}

export interface QueueListReducer {
  sortBy: RefDataValue;
  orderBy: RefDataValue;
  isLoading: boolean;
  isError: boolean;
  searchValue: AttributeSearchValue[];
  totalItem: number;
  currentPage: number;
  pageSize: number;
  data: QueueAccount[];
}

export interface QueueAccount {
  id: string;
  queueID: string;
  queueName: string;
  lastWorkDate: string;
  totalNumberOfAccounts: number | string;
  totalOutstandingBalanceAmount: number | string;
  daysBefore: number | string;
  daysAfter: number | string;
  assignedTo: string;
  createdDate: string;
  workedAccounts: {
    numberAccounts: string;
    value: string;
  };
  remainingAccounts: {
    numberAccounts: string;
    value: string;
  };
}

export interface IPayloadGetQueueList {
  sortBy?: string;
  orderBy?: string;
  searchValue: AttributeSearchValue[];
  pageSize: number;
  page: number;
}

export interface IPayloadGetQueueListReturn {
  data: {
    data: QueueAccount[];
    totalItem: number;
    currentPage: number;
    pageSize: number;
  };
}

export type WorkQueueType =
  | 'First'
  | 'Last'
  | 'current_account'
  | 'next_account'
  | 'previous_account';
export interface StartOfQueueReducer {
  isOpen: boolean;
  workType?: WorkQueueType;
  calendarCode?: RefDataValue;
  queueActions?: QueueAccount;
}

export interface IAccountActivityReducer {
  queueList: QueueListReducer;
  startQueue: StartOfQueueReducer;
}
