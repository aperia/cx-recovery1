import React from 'react';

// components
import { DropdownList } from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helper
import { I18N_TEXT } from 'app/constants';
import { calendarCodeDropdown } from '../constants';

export interface CalendarCodeDropdownProps {
  value?: RefDataValue;
  handleChange: (e: DropdownBaseChangeEvent) => void;
}
const CalendarCodeDropdown: React.FC<CalendarCodeDropdownProps> = ({
  value,
  handleChange
}) => {
  const { t } = useTranslation();

  return (
    <div className="mt-16">
      <DropdownList
        id={'DropdownSortByAccountList'}
        textField="description"
        onChange={handleChange}
        value={value}
        label={t(I18N_TEXT.CALENDAR_CODE)}
        textFieldRender={itemValue => <>{t(itemValue.description)}</>}
      >
        {calendarCodeDropdown.map(item => (
          <DropdownList.Item
            key={item.value}
            label={t(item.description)}
            value={item}
          />
        ))}
      </DropdownList>
    </div>
  );
};

export default CalendarCodeDropdown;
