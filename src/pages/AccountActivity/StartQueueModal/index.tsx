import React, { useEffect, useCallback } from 'react';
import { useDispatch } from 'react-redux';

// component
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelector } from 'app/hooks';

// redux
import {
  selectIsOpenModal,
  selectCalendarCodeSelected,
  selectQueueAccountSelected,
  selectQueueWorkType
} from '../_redux/startQueue/selectors';
import { accountActivityActions } from 'pages/AccountActivity/_redux';
import { queueDetailActions } from 'pages/QueueDetail/_redux';

// helper
import { I18N_TEXT } from 'app/constants';

import CalendarCodeDropdown from './CalendarCodeDropdown';
import { QueueAccount, WorkQueueType } from '../types';
import { tabActions } from 'pages/__commons/TabBar/_redux';
import { IAccounts } from 'pages/AccountManagement/types';

export interface StartQueueModalProps {}

const StartQueueModal: React.FC<StartQueueModalProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const isOpen = useSelector<boolean>(selectIsOpenModal);
  const calendarSelected = useSelector<RefDataValue | undefined>(
    selectCalendarCodeSelected
  );
  const queueAccount = useSelector<QueueAccount | undefined>(
    selectQueueAccountSelected
  );
  const workType = useSelector<WorkQueueType | undefined>(selectQueueWorkType);

  useEffect(() => {
    if (!isOpen) {
      dispatch(accountActivityActions.onClearAndResetStartQueue());
    }
  }, [isOpen, dispatch]);

  const handleChangeCalendarCode = (e: DropdownBaseChangeEvent) => {
    //
    dispatch(accountActivityActions.onChangeCalendarCode(e.value));
  };

  const handleCancel = () => {
    dispatch(accountActivityActions.onShowModalStartQueue({ open: false }));
  };

  const handleOpenAccountDetail = useCallback(
    (account: IAccounts) => {
      if (!account) return;
      if (!queueAccount) return;

      const { id: storeId, accountNumber, accNumber, debtorName } = account;

      dispatch(
        tabActions.addTab({
          title: `${debtorName} - ${accountNumber}`,
          storeId,
          tabType: 'accountDetail',
          iconName: 'file',
          props: {
            storeId,
            debtorName,
            accountNumber
          }
        })
      );

      dispatch(
        queueDetailActions.onChangeBaseAccount({
          storeId: queueAccount.queueID,
          baseAccount: accNumber?.accountNumberUnMask
        })
      );
      dispatch(
        accountActivityActions.onShowModalStartQueue({
          open: false
        })
      );
    },
    [dispatch, queueAccount]
  );

  const handleSubmitForm = () => {
    if (!queueAccount) return;
    const requestData = {
      params: {
        queueId: queueAccount.queueID,
        workType,
        calendarCode: calendarSelected?.value
      },
      callback: handleOpenAccountDetail
    };
    dispatch(queueDetailActions.getAccountToWork(requestData));
  };

  return (
    <Modal show={isOpen} loading={false} enforceFocus={false} xs>
      <ModalHeader border closeButton onHide={handleCancel}>
        <ModalTitle>{t(I18N_TEXT.TITLE_START_QUEUE_MODAL)}</ModalTitle>
      </ModalHeader>
      <ModalBody className="overflow-hidden">
        <p>{t(I18N_TEXT.MSG_CONFIRM_START_QUEUE_MODAL)}</p>
        <CalendarCodeDropdown
          value={calendarSelected}
          handleChange={handleChangeCalendarCode}
        />
      </ModalBody>
      <ModalFooter
        cancelButtonText={t(I18N_TEXT.CANCEL)}
        okButtonText={t(I18N_TEXT.START_QUEUE)}
        onCancel={handleCancel}
        onOk={handleSubmitForm}
      ></ModalFooter>
    </Modal>
  );
};

export default StartQueueModal;
