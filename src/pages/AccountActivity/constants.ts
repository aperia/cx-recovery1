import { I18N_TEXT, ordersDropdown } from 'app/constants';
import { QueueListReducer, StartOfQueueReducer } from './types';
import { NoCombineField } from 'app/_libraries/_dls/components';

export const STORE_ID_QUEUE_LIST = 'QUEUE_LIST';

export const dropdownSortQueueList = [
  {
    description: 'Queue Name',
    value: 'queueName'
  },
  { description: 'Last Work Date', value: 'lastWorkDate' }
];

export const sortByDefault = dropdownSortQueueList[1];

export const orderByDefault = ordersDropdown[0];

export const PAGE_SIZE = [10, 25, 50];

export const QueueListInitialState: QueueListReducer = {
  sortBy: sortByDefault,
  orderBy: orderByDefault,
  isLoading: false,
  isError: false,
  searchValue: [],
  pageSize: 10,
  currentPage: 1,
  totalItem: 0,
  data: []
};

export const StartQueueInitialState: StartOfQueueReducer = {
  isOpen: false,
  workType: 'First'
};

export enum ATTRIBUTE_SEARCH_FIELD {
  QUEUE_ID = 'queueID',
  QUEUE_NAME = 'queueName'
}

export const MESSAGE = {
  QUEUE_ID_SEARCH: 'txt_queue_id_search',
  QUEUE_NAME_SEARCH: 'txt_queue_name_search'
};

export const FIELD_NOT_CONSTRAINT: string[] = [
  ATTRIBUTE_SEARCH_FIELD.QUEUE_ID,
  ATTRIBUTE_SEARCH_FIELD.QUEUE_NAME
];

export const NO_COMBINE_FIELDS: NoCombineField[] = [
  {
    field: ATTRIBUTE_SEARCH_FIELD.QUEUE_ID,
    filterMessage: I18N_TEXT.QUEUE_ID_CANNOT_COMBINED
  },
  {
    field: ATTRIBUTE_SEARCH_FIELD.QUEUE_NAME,
    filterMessage: I18N_TEXT.QUEUE_NAME_CANNOT_COMBINED
  }
];

export const calendarCodeDropdown = [
  {
    description: 'txt_none',
    value: ''
  },
  {
    description: 'txt_broken_promise',
    value: 'BPP'
  },
  {
    description: 'txt_collector_activity',
    value: 'CAN'
  },
  {
    description: 'txt_check_asset',
    value: 'CAS'
  },
  {
    description: 'txt_check_credit',
    value: 'CBC'
  },
  {
    description: 'txt_disputed_balance',
    value: 'DSP'
  },
  {
    description: 'txt_disputed_resolved',
    value: 'DSR'
  },
  {
    description: 'txt_judgement_filed',
    value: 'JDF'
  },
  {
    description: 'txt_clerical_follow_up',
    value: 'LFU'
  },
  {
    description: 'txt_letter_follow_up',
    value: 'LTR'
  }
];
