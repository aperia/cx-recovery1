import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { checkEmpty } from 'app/helpers';

import QueueListService from 'pages/AccountActivity/QueueList/QueueListService';
import {
  IAccountActivityReducer,
  IPayloadGetQueueListReturn
} from 'pages/AccountActivity/types';

interface IGetQueueListArg {}

export const getQueueList = createAsyncThunk<
  IPayloadGetQueueListReturn,
  IGetQueueListArg,
  ThunkAPIConfig
>('getQueueList', async (args, thunkAPI) => {
  try {
    const { searchValue, pageSize, currentPage, sortBy, orderBy } =
      thunkAPI.getState().accountActivity.queueList;

    const response = await QueueListService.getQueueList({
      searchValue,
      pageSize,
      page: currentPage,
      sortBy: sortBy.value,
      orderBy: orderBy.value
    });

    return { data: response.data };
  } catch (error: any) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getQueueListBuilder = (
  builder: ActionReducerMapBuilder<IAccountActivityReducer>
) => {
  const { pending, fulfilled, rejected } = getQueueList;
  builder
    .addCase(pending, (draftState, action) => {
      draftState.queueList.isLoading = true;
    })
    .addCase(fulfilled, (draftState, action) => {
      const { data } = action.payload;
      const { isError } = draftState.queueList;

      draftState.queueList.isLoading = false;
      draftState.queueList.data = data.data;
      draftState.queueList.totalItem = data.totalItem;
      draftState.queueList.currentPage = data.currentPage;
      draftState.queueList.pageSize = data.pageSize;
      draftState.queueList.isError = checkEmpty([isError, false, true]);
    })
    .addCase(rejected, (draftState, action) => {
      draftState.queueList.isLoading = false;
      draftState.queueList.isError = true;
    });
};
