import { createSelector } from '@reduxjs/toolkit';
import { formatListData } from 'app/helpers';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { QueueAccount } from 'pages/AccountActivity/types';

const getQueueListSortBy = (states: RootState): RefDataValue => {
  return states.accountActivity.queueList.sortBy;
};

const getQueueListOrderBy = (states: RootState): RefDataValue => {
  return states.accountActivity.queueList.orderBy;
};

const getQueueList = (states: RootState): QueueAccount[] => {
  return states.accountActivity.queueList.data;
};

const getValueSearchQueueList = (states: RootState): AttributeSearchValue[] => {
  return states.accountActivity.queueList.searchValue;
};

export const selectorQueueList = createSelector(
  getQueueList,
  (data: QueueAccount[]) => formatListData(data)
);

export const selectorValueSearch = createSelector(
  [getValueSearchQueueList],
  (data: AttributeSearchValue[]) => data
);

export const selectorQueueListSort = createSelector(
  [getQueueListSortBy],
  (data: RefDataValue) => data
);

export const selectorQueueListOrder = createSelector(
  [getQueueListOrderBy],
  (data: RefDataValue) => data
);

export const selectorIsError = createSelector(
  [(states: RootState) => states.accountActivity.queueList.isError],
  (data: boolean) => data
);

export const selectorIsLoading = createSelector(
  [(states: RootState) => states.accountActivity.queueList.isLoading],
  (data: boolean) => data
);

export const selectorTotalPage = createSelector(
  (states: RootState) => states.accountActivity.queueList.totalItem,
  (data: number) => data
);

export const selectorCurrentPage = createSelector(
  (states: RootState) => states.accountActivity.queueList.currentPage,
  (data: number) => data
);

export const selectorPageSize = createSelector(
  (states: RootState) => states.accountActivity.queueList.pageSize,
  (data: number) => data
);
