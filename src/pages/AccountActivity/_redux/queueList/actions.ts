import { PayloadAction } from '@reduxjs/toolkit';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { IAccountActivityReducer } from 'pages/AccountActivity/types';
import { sortByDefault, orderByDefault } from '../../constants';

export const onChangeSortByQueueList = (
  draftState: IAccountActivityReducer,
  action: PayloadAction<RefDataValue>
) => {
  draftState.queueList.sortBy = action.payload;
};

export const onChangeOrderByQueueList = (
  draftState: IAccountActivityReducer,
  action: PayloadAction<RefDataValue>
) => {
  draftState.queueList.orderBy = action.payload;
};

export const onChangeSearchValue = (
  draftState: IAccountActivityReducer,
  action: PayloadAction<AttributeSearchValue[]>
) => {
  draftState.queueList.searchValue = action.payload;
};


export const onChangeCurrentPage = (
  draftState: IAccountActivityReducer,
  action: PayloadAction<number>
) => {
  draftState.queueList.currentPage = action.payload;
};

export const onChangePageSize = (
  draftState: IAccountActivityReducer,
  action: PayloadAction<number>
) => {
  draftState.queueList.pageSize = action.payload;
};

export const onClearAndReset = (draftState: IAccountActivityReducer) => {
  draftState.queueList.searchValue = [];
  draftState.queueList.data = [];
  draftState.queueList.currentPage = 1;
  draftState.queueList.pageSize = 10;
  draftState.queueList.totalItem = 0;
  draftState.queueList.sortBy = sortByDefault;
  draftState.queueList.orderBy = orderByDefault;
};