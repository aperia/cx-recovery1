import { createSelector } from '@reduxjs/toolkit';
import { QueueAccount, WorkQueueType } from 'pages/AccountActivity/types';

export const selectIsOpenModal = createSelector(
  (states: RootState) => states.accountActivity.startQueue.isOpen,
  (data: boolean) => data
);

export const selectCalendarCodeSelected = createSelector(
  (states: RootState) => states.accountActivity.startQueue.calendarCode,
  (data?: RefDataValue) => data
);

export const selectQueueAccountSelected = createSelector(
  (states: RootState) => states.accountActivity.startQueue.queueActions,
  (data?: QueueAccount) => data
);

export const selectQueueWorkType = createSelector(
  (states: RootState) => states.accountActivity.startQueue.workType,
  (data?: WorkQueueType) => data
);
