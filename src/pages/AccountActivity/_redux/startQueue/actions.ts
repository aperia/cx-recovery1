import { PayloadAction } from '@reduxjs/toolkit';
import {
  IAccountActivityReducer,
  QueueAccount,
  WorkQueueType
} from 'pages/AccountActivity/types';

interface IShowModalPayload {
  open: boolean;
  workType?: WorkQueueType;
  queueAccount?: QueueAccount;
}

export const onShowModalStartQueue = (
  draftState: IAccountActivityReducer,
  action: PayloadAction<IShowModalPayload>
) => {
  draftState.startQueue.isOpen = action.payload.open;
  if (action.payload.open === true) {
    draftState.startQueue.queueActions = action.payload?.queueAccount;
    draftState.startQueue.workType = action.payload?.workType;
  } else {
    draftState.startQueue.queueActions = undefined;
  }
};

export const onChangeCalendarCode = (
  draftState: IAccountActivityReducer,
  action: PayloadAction<RefDataValue>
) => {
  draftState.startQueue.calendarCode = action.payload;
};

export const onClearAndResetStartQueue = (
  draftState: IAccountActivityReducer
) => {
  draftState.startQueue.isOpen = false;
  draftState.startQueue.calendarCode = undefined;
};
