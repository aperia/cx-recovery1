import { createSlice } from '@reduxjs/toolkit';

// queue list action
import {
  onChangeSearchValue,
  onChangeOrderByQueueList,
  onChangeSortByQueueList,
  onClearAndReset,
  onChangeCurrentPage,
  onChangePageSize
} from './queueList/actions';

import {
  onChangeCalendarCode,
  onClearAndResetStartQueue,
  onShowModalStartQueue
} from './startQueue/actions';

import { getQueueList, getQueueListBuilder } from './queueList/getQueueList';

// types
import { IAccountActivityReducer } from 'pages/AccountActivity/types';

// constants
import { QueueListInitialState, StartQueueInitialState } from '../constants';

const initialState: IAccountActivityReducer = {
  queueList: QueueListInitialState,
  startQueue: StartQueueInitialState
};

export const { actions, reducer } = createSlice({
  name: 'accountActivity',
  initialState: initialState,
  reducers: {
    // queue list
    onChangeCurrentPage,
    onChangePageSize,
    onChangeSearchValue,
    onChangeOrderByQueueList,
    onChangeSortByQueueList,
    onClearAndReset,
    onChangeCalendarCode,
    onClearAndResetStartQueue,
    onShowModalStartQueue
  },
  extraReducers: builder => {
    getQueueListBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getQueueList
};

export { combineActions as accountActivityActions };
