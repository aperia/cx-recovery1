import { checkEmpty } from 'app/helpers';
import { QueueAccount } from 'pages/AccountActivity/types';

export const filterAndOrderQueueList = (data: QueueAccount[]) => {
  if (!data.length) [];

  if (!checkEmpty([data, false])) return [];

  return data;
};
