import React, { useCallback } from 'react';

// component
import OrderByAccounts from 'pages/__commons/AccountList/OrderByAccounts';

// redux
import { accountActivityActions } from 'pages/AccountActivity/_redux';
import { selectorQueueListOrder } from 'pages/AccountActivity/_redux/queueList/selectors';
import { useDispatch } from 'react-redux';

// hooks
import { useSelector } from 'app/hooks';

interface OrderByQueueListProps {}

const OrderByQueueList: React.FC<OrderByQueueListProps> = () => {
  const dispatch = useDispatch();
  const value: RefDataValue = useSelector(selectorQueueListOrder);

  const handleChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      dispatch(accountActivityActions.onChangeOrderByQueueList(e.value));
    },
    [dispatch]
  );

  return <OrderByAccounts value={value} handleChange={handleChange} />;
};
export default OrderByQueueList;
