import React, { useCallback } from 'react';

// component
import { DropdownList } from 'app/_libraries/_dls/components';

// redux
import { selectorQueueListSort } from 'pages/AccountActivity/_redux/queueList/selectors';
import { useDispatch, useSelector } from 'react-redux';
import { accountActivityActions } from 'pages/AccountActivity/_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { dropdownSortQueueList } from 'pages/AccountActivity/constants';
import { I18N_TEXT } from 'app/constants';

interface SortByQueueList {}

const SortByAccountList: React.FC<SortByQueueList> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const value: RefDataValue = useSelector(selectorQueueListSort);

  const handleChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      dispatch(accountActivityActions.onChangeSortByQueueList(e.value));
    },
    [dispatch]
  );

  return (
    <div className="d-flex align-items-center mr-24">
      <p className="fw-500 color-grey mr-4">{t(I18N_TEXT.SORT_BY)}:</p>
      <DropdownList
        textField="description"
        variant={'no-border'}
        value={value}
        onChange={handleChange}
      >
        {dropdownSortQueueList.map(item => (
          <DropdownList.Item
            key={item.value}
            label={t(item.description)}
            value={item}
          />
        ))}
      </DropdownList>
    </div>
  );
};
export default SortByAccountList;
