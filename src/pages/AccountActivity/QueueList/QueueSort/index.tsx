import React from 'react';

// component
import SortByAccountList from './SortBy';
import OrderByAccountList from './OrderBy';

interface QueueSortProps {}

const QueueSort: React.FC<QueueSortProps> = () => {
  return (
    <>
      <SortByAccountList />
      <OrderByAccountList />
    </>
  );
};
export default QueueSort;
