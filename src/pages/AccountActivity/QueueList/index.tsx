import React, { useCallback, useEffect, useState } from 'react';
import { batch, useDispatch } from 'react-redux';
import { useSelector } from 'app/hooks';
import classNames from 'classnames';

// redux
import { accountActivityActions } from '../_redux';
import {
  selectorIsLoading,
  selectorQueueList
} from '../_redux/queueList/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// component
import { SimpleBar } from 'app/_libraries/_dls/components';
import QueueGrid from './QueueGrid';
import QueueSearch from './QueueSearch';
import QueueSort from './QueueSort';

// constants
import { I18N_TEXT } from 'app/constants';
import { QueueAccount } from '../types';
import { isEqual } from 'lodash';

interface QueueListProps {
  screen?: 'Home' | 'QueueList';
  title?: string;
}

const QueueList: React.FC<QueueListProps> = ({ screen = 'QueueList' }) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();
  const isLoading: boolean = useSelector(selectorIsLoading);
  const data: QueueAccount[] = useSelector(selectorQueueList);
  const [listQueue, setListQueue] = useState<QueueAccount[]>([]);

  const onGetListQueue = useCallback(() => {
    batch(() => {
      dispatch(accountActivityActions.getQueueList({}));
    });
  }, [dispatch]);

  useEffect(() => {
    onGetListQueue();

    return () => {
      dispatch(accountActivityActions.onClearAndReset());
    };
  }, [dispatch, onGetListQueue]);

  useEffect(() => {
    if (!isEqual(listQueue, data)) {
      if (listQueue.length === 0 && screen === 'Home') {
        setListQueue(data.slice(0, 4));
      }
    }
  }, [data, listQueue, screen]);

  return (
    <SimpleBar
      className={classNames({
        loading: isLoading
      })}
    >
      <div
        className={classNames('max-width-lg mx-auto', {
          'p-24': screen === 'QueueList'
        })}
      >
        {screen === 'QueueList' && (
          <>
            <div className="d-flex justify-content-between align-items-center">
              <h3>{t(I18N_TEXT.QUEUE_LIST)}</h3>
              <QueueSearch />
            </div>
            <div className="d-flex align-items-center justify-content-end mt-24 mr-n8">
              <QueueSort />
            </div>
          </>
        )}

        <QueueGrid
          data={screen === 'Home' ? listQueue : data}
          getQueueList={onGetListQueue}
          isPaging={screen === 'QueueList'}
        />
      </div>
    </SimpleBar>
  );
};

export default QueueList;
