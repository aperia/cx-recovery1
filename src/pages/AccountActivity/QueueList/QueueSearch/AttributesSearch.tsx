import React from 'react';
import {
  AttributeSearchData,
  InputControl,
} from 'app/_libraries/_dls/components';
import { I18N_TEXT, MAX_LENGTH_6, MAX_LENGTH_50 } from 'app/constants';

import { ATTRIBUTE_SEARCH_FIELD } from 'pages/AccountActivity/constants';

export const getInitialAttrData = (t: any) => {
  const queueIDField: AttributeSearchData = {
    groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
    name: t(I18N_TEXT.QUEUE_ID),
    key: ATTRIBUTE_SEARCH_FIELD.QUEUE_ID,
    description: t(I18N_TEXT.QUEUE_ID_DESCRIPTION),
    component: props => (
      <InputControl
        placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
        maxLength={MAX_LENGTH_6}
        {...props}
      />
    )
  };
  const queueNameField: AttributeSearchData = {
    groupName: t(I18N_TEXT.SEARCH_PARAMETERS),
    name: t(I18N_TEXT.QUEUE_NAME),
    key: ATTRIBUTE_SEARCH_FIELD.QUEUE_NAME,
    description: t(I18N_TEXT.QUEUE_NAME_DESCRIPTION),
    component: props => (
      <InputControl
        placeholder={t(I18N_TEXT.ENTER_KEY_WORD)}
        maxLength={MAX_LENGTH_50}
        {...props}
      />
    )
  };
  return {
    data: [queueIDField, queueNameField]
  };
};
