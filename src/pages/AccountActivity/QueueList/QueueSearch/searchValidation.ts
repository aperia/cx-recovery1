import { stringValidate } from 'app/helpers/stringValidate';
import {
  ATTRIBUTE_SEARCH_FIELD,
  MESSAGE
} from 'pages/AccountActivity/constants';
import { AttributeSearchFieldModel } from '../../types';

export const validateQueueId = (data: AttributeSearchFieldModel, t: any) => {
  const queueIDValue = data.queueID?.value;
  if (data.queueID) {
    const methodValid = stringValidate(queueIDValue);
    const isValidMinLength =
      queueIDValue &&
      methodValid.isSpecialAlphanumeric() &&
      methodValid.maxLength(6);

    if (!isValidMinLength) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.QUEUE_ID]: t(MESSAGE.QUEUE_ID_SEARCH)
      };
    }
  }
};

export const validateQueueName = (data: AttributeSearchFieldModel, t: any) => {
  const queueNameValue = data.queueName?.value;
  if (data.queueName) {
    const methodValid = stringValidate(queueNameValue);
    const isValidMinLength =
      queueNameValue &&
      methodValid.isAlphanumericAndSpace() &&
      methodValid.maxLength(50);

    if (!isValidMinLength) {
      return {
        [ATTRIBUTE_SEARCH_FIELD.QUEUE_NAME]: t(MESSAGE.QUEUE_NAME_SEARCH)
      };
    }
  }
};

export const attrValidations = (data: AttributeSearchFieldModel, t: any) => {
  const validateList = [validateQueueId, validateQueueName];
  const runAllValidates = validateList.map(fn => {
    return fn(data, t);
  }, []);

  const errors: Record<string, string> = runAllValidates.reduce(
    (accumulator, current) => {
      if (!current) return accumulator;
      return { ...accumulator, ...current };
    },
    {}
  );
  return Object.keys(errors).length ? errors : undefined;
};
