import React, { useEffect } from 'react';

// components
import {
  AttributeSearch,
  AttributeSearchEvent,
  AttributeSearchRef,
  AttributeSearchValue
} from 'app/_libraries/_dls/components';
import { getInitialAttrData } from './AttributesSearch';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { accountActivityActions } from '../../_redux';
import { selectorValueSearch } from '../../_redux/queueList/selectors';

// hooks
import { every, isEmpty, isEqual } from 'app/helpers';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { EMPTY_STRING, I18N_TEXT } from 'app/constants';
import {
  FIELD_NOT_CONSTRAINT,
  NO_COMBINE_FIELDS,
  ATTRIBUTE_SEARCH_FIELD
} from '../../constants';
import { AttributeSearchFieldModel } from 'pages/AccountActivity/types';
import { attrValidations } from 'pages/AccountActivity/QueueList/QueueSearch/searchValidation';

interface QueueSearchProps {}

let keepSearchValues: AttributeSearchValue[] = [];

const QueueSearch: React.FC<QueueSearchProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { data } = React.useMemo(() => getInitialAttrData(t), [t]);

  const attributeSearchRef = React.useRef<AttributeSearchRef | null>(null);

  const [searchValues, setSearchValues] = React.useState<
    AttributeSearchValue[]
  >([]);
  const originalSearch = useSelector(selectorValueSearch);

  useEffect(() => {
    if (!isEqual(originalSearch, keepSearchValues)) {
      setSearchValues(originalSearch);
      keepSearchValues = originalSearch;
    }
  }, [originalSearch]);

  const handleChange = (event: AttributeSearchEvent) => {
    const { value } = event;
    setSearchValues(value);
  };

  const handleClear = () => setSearchValues([]);

  const handleSearch = (
    event: AttributeSearchEvent,
    error?: Record<string, string>
  ) => {
    if (!isEmpty(error) || !event.value.length) return;
    batch(() => {
      dispatch(accountActivityActions.onChangeSearchValue(event.value));
      dispatch(accountActivityActions.onChangeCurrentPage(1));
      keepSearchValues = event.value;
    });
  };

  const noCombineFields = React.useMemo(() => {
    const isEnableNoCombineFeature = every(searchValues, ({ key }) => {
      return FIELD_NOT_CONSTRAINT.includes(key as ATTRIBUTE_SEARCH_FIELD);
    });

    const translationFields = NO_COMBINE_FIELDS.map(r => ({
      ...r,
      filterMessage: t(r.filterMessage)
    }));

    return isEnableNoCombineFeature ? translationFields : [];
  }, [searchValues, t]);

  const renderPlaceHolder = () => {
    if (!searchValues.length) return t(I18N_TEXT.TYPE_TO_ADD_PARAMETER);

    if (FIELD_NOT_CONSTRAINT.includes(searchValues[0]?.key))
      return EMPTY_STRING;

    return t(I18N_TEXT.TYPE_TO_ADD_PARAMETER);
  };

  return (
    <AttributeSearch
      small
      orderBy="asc"
      ref={attributeSearchRef}
      data={data}
      value={searchValues}
      placeholder={renderPlaceHolder()}
      noCombineFields={noCombineFields}
      validator={(input: AttributeSearchFieldModel) =>
        attrValidations(input, t)
      }
      onClear={handleClear}
      onChange={handleChange}
      onSearch={handleSearch}
      clearTooltipProps={{
        element: t(I18N_TEXT.CLEAR_ALL_CRITERIA)
      }}
    />
  );
};
export default QueueSearch;
