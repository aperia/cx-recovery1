import { queueListController } from 'pages/AccountActivity/__mock__/queueList/queueListController';
import { IPayloadGetQueueList } from 'pages/AccountActivity/types';
const QueueListService = {
  getQueueList: (params: IPayloadGetQueueList) => {
    return queueListController.getQueueList(params);
  }
};
export default QueueListService;
