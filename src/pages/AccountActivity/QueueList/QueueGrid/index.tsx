import React, { Fragment, useCallback, useEffect } from 'react';
import { useDispatch } from 'react-redux';
import { useSelector } from 'app/hooks';
import { checkEmpty, isEmpty } from 'app/helpers';
import If from 'pages/__commons/If';

// component
import {
  AttributeSearchValue,
  Button,
  pageSizeInfo,
  Pagination
} from 'app/_libraries/_dls/components';
import { NoDataGrid, SearchNodata } from 'app/components';
import FailedApiReload from 'app/components/FailedApiReload';
import GridItem from './GridItem';

// redux
import {
  selectorIsLoading,
  selectorIsError,
  selectorValueSearch,
  selectorPageSize,
  selectorTotalPage,
  selectorCurrentPage,
  selectorQueueListOrder,
  selectorQueueListSort
} from 'pages/AccountActivity/_redux/queueList/selectors';
import { accountActivityActions } from 'pages/AccountActivity/_redux';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constant
import { I18N_TEXT } from 'app/constants';
import { QueueAccount } from 'pages/AccountActivity/types';
import { PAGE_SIZE } from 'pages/AccountActivity/constants';

interface QueueGridProps {
  getQueueList: () => void;
  data: QueueAccount[];
  isPaging?: boolean;
}

const QueueGrid: React.FC<QueueGridProps> = ({
  getQueueList,
  data,
  isPaging = true
}) => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const isError = useSelector(selectorIsError);
  const valueSearch: AttributeSearchValue[] = useSelector(selectorValueSearch);
  const isLoading: boolean = useSelector(selectorIsLoading);
  const currentPageSize: number = useSelector(selectorPageSize);
  const totalItem: number = useSelector(selectorTotalPage);
  const currentPage: number = useSelector(selectorCurrentPage);
  const orderBy: RefDataValue = useSelector(selectorQueueListOrder);
  const sortBy: RefDataValue = useSelector(selectorQueueListSort);

  useEffect(() => {
    getQueueList();
  }, [
    getQueueList,
    currentPageSize,
    currentPage,
    orderBy.value,
    sortBy.value,
    valueSearch
  ]);

  const onPageChange = useCallback(
    (page: number) => {
      dispatch(accountActivityActions.onChangeCurrentPage(page));
    },
    [dispatch]
  );

  const onPageSizeChange = useCallback(
    (pageInfo: pageSizeInfo) => {
      dispatch(accountActivityActions.onChangePageSize(pageInfo.size));
    },
    [dispatch]
  );

  const onClearSearch = useCallback(() => {
    dispatch(accountActivityActions.onClearAndReset());
  }, [dispatch]);

  if (isError)
    return <FailedApiReload id="getQueueList" onReload={getQueueList} />;

  if (!data.length && !checkEmpty([valueSearch, false, true]) && !isLoading) {
    return <NoDataGrid text={t(I18N_TEXT.NO_QUEUE_TO_DISPLAY)}></NoDataGrid>;
  }

  if (!data.length && !isLoading)
    return (
      <SearchNodata
        text={t(I18N_TEXT.ADJUST_YOUR_SEARCH_FILTER)}
        onClearSearch={onClearSearch}
      />
    );

  return (
    <Fragment>
      <div className={'mt-16'}>
        {!isPaging && (
          <p>{`${data.length} ${t(I18N_TEXT.RECENTLY_WORKED_QUEUE)}`}</p>
        )}

        <If condition={!isEmpty(valueSearch)}>
          <Button
            size="sm"
            variant="outline-primary"
            onClick={() => onClearSearch()}
          >
            {t(I18N_TEXT.CLEAR_AND_RESET)}
          </Button>
        </If>
      </div>
      {data.map((item: QueueAccount, index: number) => (
        <GridItem key={index.toString()} queueItem={item} index={index} />
      ))}

      <If condition={totalItem > 10 && isPaging}>
        <div className="pt-16">
          <Pagination
            totalItem={totalItem}
            pageSize={PAGE_SIZE}
            pageNumber={currentPage}
            pageSizeValue={currentPageSize}
            compact
            onChangePage={onPageChange}
            onChangePageSize={onPageSizeChange}
          />
        </div>
      </If>
    </Fragment>
  );
};

export default QueueGrid;
