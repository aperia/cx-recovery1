import React, { useMemo, useCallback } from 'react';
import { View } from 'app/_libraries/_dof/core';

// component
import { Button, DropdownList } from 'app/_libraries/_dls/components';

// redux
import { useDispatch } from 'react-redux';
import { tabActions } from 'pages/__commons/TabBar/_redux';

// hooks
import { QueueAccount, WorkQueueType } from '../../types';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constant
import { I18N_TEXT } from 'app/constants';
import { accountActivityActions } from 'pages/AccountActivity/_redux';

interface GridItemProps {
  queueItem: QueueAccount;
  index: number;
}

const queueActions = [
  {
    description: I18N_TEXT.WORK_FROM_BEGINNING,
    value: I18N_TEXT.WORK_FROM_BEGINNING
  },
  {
    description: I18N_TEXT.WORK_FROM_LAST_WORKED,
    value: I18N_TEXT.WORK_FROM_LAST_WORKED
  }
];

const GridItem: React.FC<GridItemProps> = ({ queueItem, index }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  // format data for dof view
  const dataFormat: QueueAccount = useMemo(() => {
    return {
      ...queueItem,
      totalNumberOfAccounts: `${queueItem.totalNumberOfAccounts}`,
      totalOutstandingBalanceAmount: `${queueItem.totalOutstandingBalanceAmount}`,
      daysBefore: `${queueItem.daysBefore}`,
      daysAfter: `${queueItem.daysAfter}`
    };
  }, [queueItem]);

  const handleViewQueueDetail = useCallback(() => {
    const { id } = dataFormat;
    const storeId = `QUEUE_DETAIL_${id}`;
    dispatch(
      tabActions.addTab({
        title: `${dataFormat.queueID} - ${dataFormat.queueName}`,
        storeId,
        tabType: 'queueDetail',
        iconName: 'file',
        props: {
          queueDetail: dataFormat
        }
      })
    );
  }, [dispatch, dataFormat]);

  const showStartOfQueue = useCallback((workType: WorkQueueType) => {
    if (!queueItem) return;
    dispatch(
      accountActivityActions.onShowModalStartQueue({
        open: true,
        queueAccount: queueItem,
        workType
      })
    );
  }, [dispatch, queueItem]);

  const handleOpenWorkFromBeginning = useCallback(() => {
    showStartOfQueue('First');
  }, [showStartOfQueue]);

  const handleOpenWorkFromLastWorked = useCallback(() => {
    showStartOfQueue('Last');
  }, [showStartOfQueue]);

  const handleOnchangeQueueActions = useCallback(
    (e: DropdownBaseChangeEvent) => {
      switch (e.value.value) {
        case I18N_TEXT.WORK_FROM_BEGINNING:
          handleOpenWorkFromBeginning();
          break;
        case I18N_TEXT.WORK_FROM_LAST_WORKED:
          handleOpenWorkFromLastWorked();
          break;
        default:
          break;
      }
    },
    [handleOpenWorkFromBeginning, handleOpenWorkFromLastWorked]
  );

  const responsiveActions = useMemo(() => {
    return (
      <DropdownList
        variant={'no-border'}
        value="Work on Queue"
        onChange={e => handleOnchangeQueueActions(e)}
        className="dropdown-primary"
        popupBaseProps={{ placement: 'bottom-end' }}
      >
        {queueActions.map(item => (
          <DropdownList.Item
            key={item.value}
            label={t(item.description)}
            value={item}
          />
        ))}
      </DropdownList>
    );
  }, [t, handleOnchangeQueueActions]);

  return (
    <div
      className="card br-radius-8 px-16 pb-16 pt-12 mt-16"
      key={index.toString()}
    >
      <div className="d-flex justify-content-between align-items-center">
        <p className="fw-500">
          {dataFormat.queueID} - {dataFormat.queueName}
        </p>
        <div className="d-flex mr-n8">
          <Button
            size="sm"
            className="mr-8"
            variant="outline-primary"
            onClick={handleViewQueueDetail}
          >
            {t(I18N_TEXT.VIEW)}
          </Button>
          <div className="d-lg-none d-block">{responsiveActions}</div>
          <div className="d-none d-lg-block">
            <Button
              size="sm"
              variant="outline-primary"
              onClick={() => handleOpenWorkFromBeginning()}
            >
              {t(I18N_TEXT.WORK_FROM_BEGINNING)}
            </Button>
            <Button
              size="sm"
              variant="outline-primary"
              onClick={() => handleOpenWorkFromLastWorked()}
            >
              {t(I18N_TEXT.WORK_FROM_LAST_WORKED)}
            </Button>
          </div>
        </div>
      </div>

      <View
        id={`queueList-${index}`}
        formKey={`queueList-${index}`}
        descriptor="queueList"
        value={dataFormat}
      />
    </div>
  );
};

export default GridItem;
