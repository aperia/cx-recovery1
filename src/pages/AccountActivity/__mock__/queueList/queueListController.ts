import { Repository } from 'app/utils/repositories/concretes/Repository';
import { orderBy } from 'app/helpers';

// Queue list
import queueList from './queueList.json';
import {
  QueueAccount,
  IPayloadGetQueueList
} from 'pages/AccountActivity/types';

export const accountRepository = new Repository<QueueAccount>(
  JSON.parse(JSON.stringify(queueList))
);

const TIME_DELAY = 600;
const isHaveEmpty = window.location.href.includes('empty');

export const queueListController = {
  getQueueList(params: IPayloadGetQueueList) {
    try {
      const { data } = accountRepository.getAll();

      let accountListData: QueueAccount[] = [];

      for (let i = 0; i < 7; i++) {
        accountListData = [
          ...accountListData,
          ...data.map(item => {
            const id = Number(item.id) + 10 * i;

            return {
              ...item,
              id: `${id}`,
              queueName: `${item.queueName} ${id}`
            };
          })
        ];
      }

      let filterData: QueueAccount[] = accountListData;

      if (params.searchValue.length > 0) {
        const ky = params.searchValue[0].key;
        const value = params.searchValue[0].value;

        if (ky === 'queueID') {
          filterData = accountListData.filter((item: QueueAccount) =>
            `${item?.queueID?.toLocaleLowerCase()}`.includes(value.toLocaleLowerCase())
          );
        }

        if (ky === 'queueName') {
          filterData = accountListData.filter((item: QueueAccount) =>
          `${item?.queueName?.toLocaleLowerCase()}`.includes(value.toLocaleLowerCase())
          );
        }
      }
      
      if (params.sortBy) {
        filterData = orderBy(
          filterData,
          params.sortBy,
          params.orderBy as 'asc' | 'desc'
        );
      }

      const ITEMS_PER_PAGE = params.pageSize;
      const totalItem = filterData.length;
      let startIndex = (params.page - 1) * ITEMS_PER_PAGE;
      let endIndex = startIndex + (ITEMS_PER_PAGE - 1);

      let listData = filterData.slice(startIndex, endIndex + 1);

      if (listData.length === 0) {
        startIndex = (params.page - 2) * ITEMS_PER_PAGE;
        endIndex = startIndex + (ITEMS_PER_PAGE - 1);
        listData = filterData.slice(startIndex, endIndex + 1);
      }
      const currentPage = Math.ceil((startIndex - 1) / ITEMS_PER_PAGE + 1);

      return new Promise<any>(function (resolve, reject) {
        setTimeout(() => {
          resolve({
            success: true,
            data: {
              data: isHaveEmpty ? [] : listData,
              totalItem,
              currentPage: currentPage > 0 ? currentPage : 1,
              pageSize: params.pageSize
            }
          });
        }, TIME_DELAY);
      });
    } catch (err) {
      console.error('err', err);
      return new Promise<any>(function (resolve, reject) {
        setTimeout(() => {
          resolve({
            success: true,
            data: {
              data: [],
              totalItem: 0,
              currentPage: params.page,
              pageSize: params.pageSize
            }
          });
        }, TIME_DELAY);
      });
    }
  }
};
