import { IDeleteWorkDateArgs } from './../_redux/deleteWorkDate';
// mocks
import { Repository } from 'app/utils/repositories/concretes/Repository';
import dataJson from './workDates.json';

// types
import { IWorkDate } from '../types';
import { IGetWorkDateArgs } from '../_redux/getWorkDate';

const isHaveError = window.location.href.includes('error');
const isHaveEmpty = window.location.href.includes('empty');

let workDateData = [] as IWorkDate[];

try {
  workDateData = JSON.parse(JSON.stringify(dataJson)).workDates as IWorkDate[];
} catch (error) {}

const workDateRepository = new Repository<IWorkDate>(workDateData);

export const workDateController = {
  saveWorkDate: (args: any) => {
    const { id, values } = args;

    if (isHaveError) {
      return Promise.reject({});
    }

    const { data } = id
      ? workDateRepository.update(id, values)
      : workDateRepository.add(values);

    return Promise.resolve({
      data: { success: true, data }
    });
  },
  getWorkDate(args: IGetWorkDateArgs) {
    const { data } = workDateRepository.getAll();
    const { groupId } = args;

    const result = data.filter(
      workDate =>
        workDate?.groupId === undefined || workDate?.groupId === groupId
    );

    return Promise.resolve({
      data: { success: true, data: isHaveEmpty ? [] : result }
    });
  },
  deleteWorkDate: ({ id }: IDeleteWorkDateArgs) => {
    if (isHaveError) {
      return Promise.reject({});
    }

    const { data } = workDateRepository.delete(id);

    return Promise.resolve({
      data: { success: true, data }
    });
  }
};
