import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import classNames from 'classnames';
import { isFunction } from 'lodash';

export interface InfoRowProps {
  label: string;
  value?: React.ReactNode;
  containerClass?: string;
  labelClass?: string;
  valueClass?: string;
  onValueClick?: Function;
  labelDetail?: boolean;
}
const InfoRow: React.FC<InfoRowProps> = ({
  label,
  value,
  containerClass,
  labelClass,
  valueClass,
  onValueClick,
  labelDetail
}) => {
  const { t } = useTranslation();

  const handleValueClick = React.useCallback(() => {
    isFunction(onValueClick) && onValueClick();
  }, [onValueClick]);

  return (
    <div className={classNames(containerClass)}>
      {labelDetail ? (
        <span className={classNames('mr-8', labelClass)}>{t(label)}:</span>
      ) : (
        <span className={classNames('mr-8', labelClass)}>{t(label)}</span>
      )}

      <span onClick={handleValueClick} className={classNames(valueClass)}>
        {value}
      </span>
    </div>
  );
};

export default InfoRow;
