import { TimePickerValue } from 'app/_libraries/_dls/components/TimePicker';
export interface IWorkDate {
  id?: string;
  calendarCode: string;
  reason: string;
  nextWorkDate: string;
  nextWorkTime?: string;
  groupId?: string;
  noteId?: string;
}

export interface IWorkDateForm extends Omit<IWorkDate, 'nextWorkTime'> {
  postToGroup?: string;
  linkToNote?: string;
  nextWorkTime?: TimePickerValue;
  noteCreatedDate?: string;
}

export interface IWorkDateState {
  data: IWorkDate[];
  isLoading: boolean;
  isLoadMore: boolean;
  isOpen: boolean;
  isEnd: boolean;
  isError: boolean;
  selected?: IWorkDate;
}

export interface IInitialStateWorkDate {
  [storeId: string]: IWorkDateState;
}

export interface ToggleWorkDateFormModalArgs {
  storeId: string;
  isOpen?: boolean;
  selected?: IWorkDate;
}
