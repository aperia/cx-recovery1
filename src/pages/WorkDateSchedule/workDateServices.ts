import { workDateController } from './__mock__/workDateController';

// types
import { IGetWorkDateArgs } from './_redux/getWorkDate';
import { IDeleteWorkDateArgs } from './_redux/deleteWorkDate';

const workDateServices = {
  getWorkDate(args: IGetWorkDateArgs) {
    return workDateController.getWorkDate(args);
  },
  saveWorkDate(workDate: any) {
    return workDateController.saveWorkDate(workDate);
  },
  deleteWorkDate(args: IDeleteWorkDateArgs) {
    return workDateController.deleteWorkDate(args);
  }
};

export default workDateServices;
