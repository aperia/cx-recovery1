import React from 'react';
import { batch, useDispatch } from 'react-redux';

// hooks
import { useSelectorId } from 'app/hooks';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import { Button, SimpleBar } from 'app/_libraries/_dls/components';
import { InfoBarSection } from 'app/components/InfoBar/_redux';
import PinnedHeader from 'pages/__commons/PinnedHeader';
import If from 'pages/__commons/If';
import WorkDateList from './WorkDateList';
import WorkDateForm from './Form';
import { NoDataGrid } from 'app/components';

// actions
import { workDateActions } from './_redux';

// selectors
import { selectWorkDates } from './_redux/selectors';
import { getAccountDetail } from 'pages/AccountDetail/_redux/selectors';

// constants
import { I18N_TEXT } from 'app/constants';
import { IWorkDate } from './types';

// helpers
import classNames from 'classnames';
import { isToday } from 'app/helpers';

export interface WorkDateScheduleProps {
  storeId: string;
}
const WorkDateSchedule: React.FC<WorkDateScheduleProps> = ({ storeId }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const isFirstTime = React.useRef(true);
  const { groupId } = useSelectorId(getAccountDetail) || {};

  const workDates = useSelectorId<IWorkDate[]>(selectWorkDates);

  const handleShowAddForm = React.useCallback(() => {
    dispatch(
      workDateActions.onToggleWorkDateFormModal({
        storeId,
        isOpen: true
      })
    );
  }, [dispatch, storeId]);

  // handle init work date list
  React.useEffect(() => {
    if (isFirstTime.current) {
      isFirstTime.current = false;

      batch(() => {
        dispatch(workDateActions.getWorkDate({ storeId, groupId }));
      });
    }
  }, [dispatch, groupId, storeId]);

  const [newWorkDates, pastWorkDates, workDateDict] = React.useMemo(() => {
    const newItems = [] as IWorkDate[];
    const oldItems = [] as IWorkDate[];
    const dictionary = {} as MagicKeyValue;

    if (!workDates.length) return [newItems, oldItems, dictionary];

    const today = new Date();
    today.setHours(0, 0, 0);

    workDates.forEach((workDate: IWorkDate) => {
      const { calendarCode, nextWorkDate, nextWorkTime = '' } = workDate;
      const comnbinedKey = `${calendarCode}-${nextWorkDate}-${nextWorkTime}`;
      if (dictionary[comnbinedKey] === undefined) {
        dictionary[comnbinedKey] = comnbinedKey;
      }

      const date = new Date(workDate.nextWorkDate);
      date.setHours(0, 0, 0);

      if (date.getTime() >= today.getTime() || isToday(workDate.nextWorkDate)) {
        newItems.push(workDate);
      } else {
        oldItems.push(workDate);
      }
    });

    return [newItems, oldItems, dictionary];
  }, [workDates]);

  return (
    <>
      <SimpleBar>
        <div
          className={classNames(
            'd-flex flex-column px-24 pt-24 overlap-next-action'
          )}
        >
          <PinnedHeader
            storeId={storeId}
            title={I18N_TEXT.WORK_DATE_SCHEDULE}
            itemIdPinned={InfoBarSection.WorkDateSchedule}
            className="mb-18"
            renderProps={
              <If condition={!!newWorkDates.length}>
                <div className="d-flex-inline">
                  <Button
                    size="sm"
                    className="mr-n8"
                    variant="outline-primary"
                    onClick={handleShowAddForm}
                  >
                    {t(I18N_TEXT.ADD_WORK_DATE)}
                  </Button>
                </div>
              </If>
            }
          />

          <WorkDateList
            newWorkDates={newWorkDates}
            pastWorkDates={pastWorkDates}
            renderEmpty={
              <>
                <div className="pt-18 pb-24">
                  <NoDataGrid
                    iconName="calendar"
                    text={I18N_TEXT.NO_WORK_DATES_TO_DISPLAY}
                    className="mb-0"
                  />
                </div>
                <Button
                  size="sm"
                  className="mr-n8"
                  variant="outline-primary"
                  onClick={handleShowAddForm}
                >
                  {t(I18N_TEXT.ADD_WORK_DATE)}
                </Button>
              </>
            }
          />

          <WorkDateForm workDateDict={workDateDict} />
        </div>
      </SimpleBar>
    </>
  );
};

export default WorkDateSchedule;
