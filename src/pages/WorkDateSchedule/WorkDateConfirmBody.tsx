import React from 'react';

// hooks
import useRefData from 'app/hooks/useRefData';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import InfoRow from './InfoRow';

// constants + types
import { I18N_TEXT } from 'app/constants';
import { IWorkDate } from './types';

// helpers
import { getDescriptionFromCode } from 'pages/__commons/helpers';

export interface WorkDateConfirmBodyProps {
  data: IWorkDate;
  noteCode: string;
}

const WorkDateConfirmBody: React.FC<WorkDateConfirmBodyProps> = ({
  data,
  noteCode
}) => {
  const { t } = useTranslation();
  const calendarCodeOptions = useRefData('calendarCode2');

  return (
    <div>
      <p className="mb-16">{t(I18N_TEXT.DELETE_WORK_DATE_CONFIRM)}</p>
      <div className="bg-light-l20 border br-light-l04 br-radius-8 p-16 pt-12">
        <InfoRow
          label={I18N_TEXT.CALENDAR_CODE}
          value={getDescriptionFromCode(
            calendarCodeOptions,
            data.calendarCode,
            true
          )}
          containerClass="d-flex align-item-center justify-content-between"
          labelClass="d-inline-block w-40 mr-8"
          valueClass="w-60 fw-500"
          labelDetail
        />

        <InfoRow
          label={I18N_TEXT.REASON}
          value={data.reason}
          containerClass="d-flex align-item-center justify-content-between mt-8"
          labelClass="d-inline-block w-40 mr-8"
          valueClass="w-60 fw-500"
          labelDetail
        />

        <InfoRow
          label={I18N_TEXT.NEXT_WORK_TIME}
          value={`${data.nextWorkDate} ${data?.nextWorkTime}`}
          containerClass="d-flex align-item-center justify-content-between mt-8"
          labelClass="d-inline-block w-40 mr-8"
          valueClass="w-60 fw-500"
          labelDetail
        />
        <InfoRow
          label={I18N_TEXT.POST_TO_GROUP}
          value={data?.groupId ? t(I18N_TEXT.YES) : t(I18N_TEXT.NO)}
          containerClass="d-flex align-item-center justify-content-between mt-8"
          labelClass="d-inline-block w-40 mr-8"
          valueClass="w-60 fw-500"
          labelDetail
        />
        <InfoRow
          label={I18N_TEXT.LINK_TO_NOTE}
          value={noteCode || t(I18N_TEXT.NO)}
          containerClass="d-flex align-item-center justify-content-between mt-8"
          labelClass="d-inline-block w-40 mr-8"
          valueClass="w-60 fw-500"
          labelDetail
        />
      </div>
    </div>
  );
};

export default WorkDateConfirmBody;
