import React from 'react';

// components
import {
  DatePickerControl,
  DropdownControl,
  TextBoxControl,
  YesNoQuestionControl
} from 'app/components';
import TimePicker, {
  TimePickerValue
} from 'app/_libraries/_dls/components/TimePicker';
import { InlineMessage } from 'app/_libraries/_dls/components';
import If from 'pages/__commons/If';

// hooks
import useRefData from 'app/hooks/useRefData';
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useSelectorId } from 'app/hooks';
import { useFormikContext } from 'formik';

// selectors
import { selectNoteCodes } from 'pages/__commons/NoteCode/_redux/selectors';
import { getAccountDetail } from 'pages/AccountDetail/_redux/selectors';

// constants
import {
  ALPHA_NUMERIC_SPACE_REPLACE_REGEX,
  I18N_TEXT,
  MAX_LENGTH_20
} from 'app/constants';
import { WORK_DATE_FIELD } from '../constants';
import { INoteCode } from 'pages/__commons/NoteCode/types';
import { IWorkDateForm } from '../types';
import { FormatTime } from 'app/constants/enums';
import {
  formatTimeDefault,
  getTimePickerValue,
  isEqual,
  isUndefined
} from 'app/helpers';
import useRenderTime from 'pages/__commons/NoteCode/hooks/useRenderTime';
import { TimePickerChangeEvent } from 'app/_libraries/_dls/components/TimePicker/types';

export interface FormContentProps {
  workDateDict: MagicKeyValue;
}
const FormContent: React.FC<FormContentProps> = ({ workDateDict }) => {
  const { t } = useTranslation();
  const calendarCodeOptions = useRefData('calendarCode2');
  const noteCodes = useSelectorId<INoteCode[]>(selectNoteCodes);
  const { groupId } = useSelectorId(getAccountDetail) || {};
  const {
    handleChange,
    values,
    setValues,
    setErrors,
    errors,
    setFieldTouched
  } = useFormikContext<IWorkDateForm>();

  const [timePickValue, setTimePickValue] = React.useState<TimePickerValue>(
    getTimePickerValue(new Date())
  );

  const {
    linkToNote,
    calendarCode = '',
    nextWorkDate = '',
    nextWorkTime,
    noteCreatedDate
  } = values;

  const formattedNoteCreatedDate = useRenderTime(noteCreatedDate || '');

  const combinedKey = React.useMemo(() => {
    const dateStr = formatTimeDefault(nextWorkDate, FormatTime.Date);
    const { hour = 0, minute = 0, meridiem = 'AM' } = nextWorkTime || {};

    const timeStr = isUndefined(nextWorkTime)
      ? ''
      : `${hour}:${minute} ${meridiem}`;

    return `${calendarCode}-${dateStr}-${timeStr}`;
  }, [calendarCode, nextWorkDate, nextWorkTime]);

  const workDateExists = !!workDateDict[combinedKey];

  const handlePostToGroupChange = (val: string) => {
    const event = {
      target: { name: WORK_DATE_FIELD.POST_TO_GROUP.name, value: val }
    };

    handleChange(event);

    setValues({
      ...values,
      groupId: val === 'Y' ? undefined : groupId
    });
  };

  const handleLinkToNoteChange = (val: string) => {
    const event = {
      target: { name: WORK_DATE_FIELD.LINK_TO_NOTE.name, value: val }
    };

    handleChange(event);
  };

  const handleNoteChange = React.useCallback(
    (e: DropdownBaseChangeEvent) => {
      const { id: noteId, createdDate } = e.value;

      setValues({
        ...values,
        noteId,
        noteCreatedDate: createdDate
      });
    },
    [setValues, values]
  );

  const handleTimePickerChange = React.useCallback(
    (event: TimePickerChangeEvent) => {
      handleChange(event);

      const { value = {} } = event;
      setTimePickValue(value);
    },
    [handleChange, setTimePickValue]
  );

  const renderDropdownNote = React.useMemo(() => {
    if (linkToNote === 'Y') {
      return (
        <>
          <DropdownControl
            name="note"
            label={t(I18N_TEXT.NOTE)}
            className="mt-16"
            options={noteCodes}
            onValueChange={handleNoteChange}
            isCombine
            required
          />

          {formattedNoteCreatedDate && (
            <div className="mt-8 fs-12 color-grey">
              <span className="mr-8">{`${t(I18N_TEXT.SORT_BY_DATE)}:`}</span>
              <span>{formattedNoteCreatedDate}</span>
            </div>
          )}
        </>
      );
    }
  }, [linkToNote, t, noteCodes, handleNoteChange, formattedNoteCreatedDate]);

  React.useEffect(() => {
    if (!nextWorkDate) {
      setFieldTouched(WORK_DATE_FIELD.NEXT_WORK_DATE.name, true);
    }
  }, [nextWorkDate, setFieldTouched]);

  React.useEffect(() => {
    const { nextWorkTime } = values;

    if (!isEqual(nextWorkTime, timePickValue)) {
      setValues({
        ...values,
        nextWorkTime: timePickValue
      });
    }
  }, [setValues, timePickValue, values]);

  React.useEffect(() => {
    if (workDateExists) {
      setErrors({
        ...errors,
        [WORK_DATE_FIELD.CALENDAR_CODE.name]: t(I18N_TEXT.WORK_DATE_EXISTS),
        [WORK_DATE_FIELD.NEXT_WORK_DATE.name]: t(I18N_TEXT.WORK_DATE_EXISTS),
        [WORK_DATE_FIELD.NEXT_WORK_TIME.name]: t(I18N_TEXT.WORK_DATE_EXISTS)
      });
    }
  }, [workDateExists, setErrors, t, errors]);

  return (
    <>
      <If condition={workDateExists}>
        <InlineMessage variant="danger">
          <span>{t(I18N_TEXT.WORK_DATE_EXISTS)}</span>
        </InlineMessage>
      </If>

      <DropdownControl
        name={WORK_DATE_FIELD.CALENDAR_CODE.name}
        label={t(WORK_DATE_FIELD.CALENDAR_CODE.label)}
        options={calendarCodeOptions}
        isCombine
        required
      />

      <TextBoxControl
        name={WORK_DATE_FIELD.REASON.name}
        label={t(WORK_DATE_FIELD.REASON.label)}
        className="mt-16"
        maxLength={MAX_LENGTH_20}
        regex={ALPHA_NUMERIC_SPACE_REPLACE_REGEX}
        required
      />

      <DatePickerControl
        name={WORK_DATE_FIELD.NEXT_WORK_DATE.name}
        label={t(WORK_DATE_FIELD.NEXT_WORK_DATE.label)}
        className="mt-16"
        pastDateDisabledText={t(I18N_TEXT.NEXT_WORK_DATE_NOT_PAST_DAY)}
        minDaysFromToday={0}
        required
      />

      <div className="mt-16">
        <TimePicker
          name={WORK_DATE_FIELD.NEXT_WORK_TIME.name}
          label={t(WORK_DATE_FIELD.NEXT_WORK_TIME.label)}
          value={timePickValue}
          onChange={handleTimePickerChange}
          error={
            errors[WORK_DATE_FIELD.NEXT_WORK_TIME.name as keyof typeof errors]
              ? {
                  message: t(I18N_TEXT.WORK_DATE_EXISTS),
                  status: true
                }
              : undefined
          }
        />
      </div>
      <div className="d-flex mt-16">
        <div className="w-35">
          <p className="fw-500 color-grey">
            {t(WORK_DATE_FIELD.POST_TO_GROUP.label)}:
          </p>
        </div>
        <div className="w-65">
          <YesNoQuestionControl
            name={WORK_DATE_FIELD.POST_TO_GROUP.name}
            onValueChange={handlePostToGroupChange}
          />
        </div>
      </div>
      <p className="mt-4 fs-12 color-grey">{t(I18N_TEXT.POST_TO_GROUP_DESC)}</p>
      <div className="d-flex mt-16">
        <div className="w-35">
          <p className="fw-500 color-grey">
            {t(WORK_DATE_FIELD.LINK_TO_NOTE.label)}:
          </p>
        </div>
        <div className="w-65">
          <YesNoQuestionControl
            name={WORK_DATE_FIELD.LINK_TO_NOTE.name}
            onValueChange={handleLinkToNoteChange}
          />
        </div>
      </div>

      {renderDropdownNote}
    </>
  );
};

export default FormContent;
