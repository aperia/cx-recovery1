import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';

// components
import {
  Modal,
  ModalBody,
  ModalFooter,
  ModalHeader,
  ModalTitle
} from 'app/_libraries/_dls/components';

// actions
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { workDateActions } from '../_redux';

// selectors

import {
  selectSelectedWorkDateForm,
  selectWorkDateFormIsOpen
} from '../_redux/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import useWorkDateForm from '../hooks/useWorkDateForm';

// types
import { IWorkDate } from '../types';

// helpers
import { FormikProvider } from 'formik';
import { isTouchedForm } from 'app/helpers';

// constants
import { I18N_TEXT } from 'app/constants';
import FormContent from './FormContent';

export interface WorkDateFormProps {
  workDateDict: MagicKeyValue;
}
const WorkDateForm: React.FC<WorkDateFormProps> = ({ workDateDict }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();

  const isOpen = useSelectorId<boolean>(selectWorkDateFormIsOpen);
  const selected = useSelectorId<IWorkDate | undefined>(
    selectSelectedWorkDateForm
  );

  const editMode = !!selected?.id;
  const { formik } = useWorkDateForm();

  const {
    submitForm,
    resetForm,
    setSubmitting,
    isSubmitting,
    isValid,
    touched,
    dirty
  } = formik;

  const handleCancelForm = useCallback(() => {
    resetForm();
    setSubmitting(false);

    dispatch(
      workDateActions.onToggleWorkDateFormModal({
        storeId,
        isOpen: false
      })
    );
  }, [dispatch, resetForm, setSubmitting, storeId]);

  const handleCancel = useCallback(() => {
    if (dirty && isTouchedForm(touched)) {
      return dispatch(
        rootModalActions.open({
          title: I18N_TEXT.UNSAVED_CHANGE,
          body: I18N_TEXT.DISCARD_CHANGE_BODY,
          btnCancelText: I18N_TEXT.CONTINUE_EDITING,
          btnConfirmText: I18N_TEXT.DISCARD_CHANGE,
          onConfirm: handleCancelForm
        })
      );
    }

    handleCancelForm();
  }, [dirty, touched, handleCancelForm, dispatch]);

  const handleSubmit = useCallback(() => {
    setSubmitting(true);
    submitForm();
  }, [setSubmitting, submitForm]);

  return (
    <FormikProvider value={formik}>
      <Modal xs show={isOpen} loading={false} enforceFocus={false}>
        <ModalHeader border closeButton onHide={handleCancel}>
          <ModalTitle className="text-capitalize">
            {editMode
              ? t(I18N_TEXT.EDIT_WORK_DATE)
              : t(I18N_TEXT.ADD_WORK_DATE)}
          </ModalTitle>
        </ModalHeader>
        <ModalBody>
          <FormContent workDateDict={workDateDict} />
        </ModalBody>
        <ModalFooter
          cancelButtonText={t(I18N_TEXT.CANCEL)}
          okButtonText={editMode ? t(I18N_TEXT.SAVE) : t(I18N_TEXT.SUBMIT)}
          onCancel={handleCancel}
          onOk={handleSubmit}
          disabledOk={
            !editMode && (isSubmitting || !(isTouchedForm(touched) && isValid))
          }
        ></ModalFooter>
      </Modal>
    </FormikProvider>
  );
};

export default React.memo(WorkDateForm);
