import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

// constants
import { API_ERROR } from 'app/constants';

// service
import workDateServices from '../workDateServices';

// types
import { IInitialStateWorkDate, IWorkDate } from '../types';

export interface IPayloadGetWorkDate {
  data: IWorkDate[];
}
export interface IGetWorkDateArgs {
  storeId: string;
  groupId?: string;
}

export const getWorkDate = createAsyncThunk<
  IPayloadGetWorkDate,
  IGetWorkDateArgs,
  ThunkAPIConfig
>('workDateSchedule/getWorkDate', async (args, thunkAPI) => {
  try {
    const response = await workDateServices.getWorkDate(args);

    return response.data;
  } catch (error) {
    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  }
});

export const getWorkDateBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateWorkDate>
) => {
  const { pending, fulfilled, rejected } = getWorkDate;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true,
        isError: false
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: false,
        data
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: true
      };
    });
};
