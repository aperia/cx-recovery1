import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { workDateActions } from '.';

// constants
import { API_ERROR, I18N_TEXT } from 'app/constants';

// service
import workDateServices from '../workDateServices';

// types
import { IInitialStateWorkDate, IWorkDate } from '../types';

export interface IPayloadDeleteWorkDate {
  data: IWorkDate[];
}
export interface IDeleteWorkDateArgs {
  id: string;
  storeId: string;
  groupId?: string;
}

export const deleteWorkDate = createAsyncThunk<
  IPayloadDeleteWorkDate,
  IDeleteWorkDateArgs,
  ThunkAPIConfig
>('workDateSchedule/deleteWorkDate', async (args, thunkAPI) => {
  const { dispatch } = thunkAPI;
  const { storeId, groupId } = args;

  try {
    const { data } = await workDateServices.deleteWorkDate(args);
    batch(async () => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.DELETE_WORK_DATE_SUCCESS
        })
      );

      await dispatch(workDateActions.getWorkDate({ storeId, groupId }));
    });

    return { data: data.data };
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: I18N_TEXT.DELETE_WORK_DATE_FAIL
      })
    );

    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  } finally {
    dispatch(rootModalActions.close());
  }
});

export const deleteWorkDateBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateWorkDate>
) => {
  const { pending, fulfilled, rejected } = deleteWorkDate;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true,
        isError: false
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: false,
        data: data
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: true
      };
    });
};
