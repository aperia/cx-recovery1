import { createSelector } from '@reduxjs/toolkit';

// types
import { IWorkDate, IWorkDateState } from './../types';

// helpers
import { orderBy } from 'app/helpers';

const getWorkDateState = (state: RootState, storeId: string): IWorkDateState =>
  state.workDateSchedule[storeId] || {};

export const selectWorkDates = createSelector(
  getWorkDateState,
  (data: IWorkDateState): IWorkDate[] => {
    const { data: workDates = [] } = data;
    return orderBy(
      workDates,
      workDate => {
        const { nextWorkDate, nextWorkTime } = workDate;

        const dateTimeStr = `${nextWorkDate}${
          nextWorkTime ? ` ${nextWorkTime}` : ''
        }`;

        return new Date(dateTimeStr);
      },
      'desc'
    );
  }
);

export const selectWorkDateFormIsOpen = createSelector(
  getWorkDateState,
  (data: IWorkDateState): boolean => !!data?.isOpen
);

export const selectSelectedWorkDateForm = createSelector(
  getWorkDateState,
  (data: IWorkDateState): IWorkDate | undefined => data?.selected
);
