import { createSlice, PayloadAction } from '@reduxjs/toolkit';

import { getWorkDate, getWorkDateBuilder } from './getWorkDate';
import { deleteWorkDate, deleteWorkDateBuilder } from './deleteWorkDate';
import { saveWorkDate, saveWorkDateBuilder } from './saveWorkDate';

// types
import { IInitialStateWorkDate, ToggleWorkDateFormModalArgs } from './../types';

// helpers
import { isBoolean } from 'app/helpers';

const initialState: IInitialStateWorkDate = {};

export const { actions, reducer } = createSlice({
  name: 'workDateSchedule',
  initialState: initialState,
  reducers: {
    onToggleWorkDateFormModal: (
      draftState: IInitialStateWorkDate,
      action: PayloadAction<ToggleWorkDateFormModalArgs>
    ) => {
      const {
        storeId,
        isOpen: isOpenProp,
        selected: selectedProp
      } = action.payload;

      const { isOpen = false, selected } = draftState[storeId] || {};

      draftState[storeId] = {
        ...draftState[storeId],
        ...(selectedProp ? selectedProp : selected),
        isOpen: isBoolean(isOpenProp) ? isOpenProp : !isOpen
      };
    }
  },
  extraReducers: builder => {
    getWorkDateBuilder(builder);
    deleteWorkDateBuilder(builder);
    saveWorkDateBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getWorkDate,
  deleteWorkDate,
  saveWorkDate
};

export { combineActions as workDateActions };
