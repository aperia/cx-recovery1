import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';

// actions
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { workDateActions } from '.';

// constants
import { API_ERROR, I18N_TEXT } from 'app/constants';
import { FormikHelpers } from 'formik';

// service
import workDateServices from '../workDateServices';

// types
import { IInitialStateWorkDate, IWorkDate } from '../types';
import { formatTimeDefault, isUndefined } from 'app/helpers';
import { FormatTime } from 'app/constants/enums';

// helpers
import { pick } from 'app/helpers/lodash';
import { pickBy, identity } from 'lodash';

export interface IPayloadWorkDate {
  data: IWorkDate[];
}
export interface IWorkDateArgs {
  id?: string;
  values: any;
  formikBag: FormikHelpers<any>;
  storeId: string;
  accNumber: string;
  groupId?: string;
}

export const saveWorkDate = createAsyncThunk<
  IPayloadWorkDate,
  IWorkDateArgs,
  ThunkAPIConfig
>('workDateSchedule/saveWorkDate', async (args, thunkAPI) => {
  const { storeId, id, values, formikBag, groupId } = args;

  const { resetForm, setSubmitting } = formikBag;
  const { dispatch } = thunkAPI;

  const { nextWorkDate, nextWorkTime, postToGroup } = values;

  const { hour = 0, minute = 0, meridiem = 'AM' } = nextWorkTime || {};

  const workDateFields = pickBy(
    // pick workDate fields
    pick(values, [
      'calendarCode',
      'reason',
      'nextWorkDate',
      'nextWorkTime',
      'noteId'
    ]),
    identity // remove empty fields
  );

  const workDate = {
    ...workDateFields,
    groupId: postToGroup === 'Y' ? undefined : groupId,
    nextWorkDate: formatTimeDefault(nextWorkDate, FormatTime.Date),
    nextWorkTime: isUndefined(nextWorkTime)
      ? ''
      : `${hour}:${minute} ${meridiem}`
  };

  try {
    const response = await workDateServices.saveWorkDate({
      id,
      values: workDate
    });

    batch(async () => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: id
            ? I18N_TEXT.EDIT_WORK_DATE_SUCCESS
            : I18N_TEXT.ADD_WORK_DATE_SUCCESS
        })
      );

      await dispatch(
        workDateActions.getWorkDate({
          storeId,
          groupId
        })
      );

      // close modal

      dispatch(
        workDateActions.onToggleWorkDateFormModal({
          storeId,
          isOpen: false
        })
      );
    });

    resetForm();

    return response.data;
  } catch (error) {
    dispatch(
      actionsToast.addToast({
        show: true,
        type: 'error',
        message: id
          ? I18N_TEXT.EDIT_WORK_DATE_FAIL
          : I18N_TEXT.ADD_WORK_DATE_FAIL
      })
    );

    return thunkAPI.rejectWithValue({
      errorMessage: API_ERROR,
      response: error
    });
  } finally {
    setSubmitting(false);
  }
});

export const saveWorkDateBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateWorkDate>
) => {
  const { pending, fulfilled, rejected } = saveWorkDate;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;

      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: true,
        isError: false
      };
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: false,
        data
      };
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      draftState[storeId] = {
        ...draftState[storeId],
        isLoading: false,
        isError: true
      };
    });
};
