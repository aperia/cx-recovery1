import { useDispatch } from 'react-redux';

// hooks
import useValidation from './useValidation';
import { useAccountDetail, useSelectorId } from 'app/hooks';

// actions
import { workDateActions } from '../_redux';

// selectors
import { selectSelectedWorkDateForm } from '../_redux/selectors';
import { getAccountDetail } from 'pages/AccountDetail/_redux/selectors';

// types
import { IWorkDate } from '../types';

// helpers
import { useFormik } from 'formik';
import { formatTimeDefault } from 'app/helpers';
import { FormatTime } from 'app/constants/enums';
import { WORK_DATE_FIELD } from '../constants';

const useWorkDateForm = () => {
  const dispatch = useDispatch();
  const { storeId, accNumber } = useAccountDetail();
  const { validationSchema } = useValidation();
  const { groupId } = useSelectorId(getAccountDetail) || {};

  const selected = useSelectorId<IWorkDate | undefined>(
    selectSelectedWorkDateForm
  );

  const selectedId = selected?.id;

  const today = new Date();
  const dateStr = formatTimeDefault(today, FormatTime.Date);

  const defaultValues = {
    [WORK_DATE_FIELD.POST_TO_GROUP.name]: 'N',
    [WORK_DATE_FIELD.LINK_TO_NOTE.name]: 'N',
    [WORK_DATE_FIELD.NEXT_WORK_DATE.name]: dateStr
  };

  const formik = useFormik({
    enableReinitialize: true,
    initialValues: {
      ...defaultValues,
      ...(selected ? { ...selected } : {})
    },
    onSubmit: (values, formikBag) => {
      dispatch(
        workDateActions.saveWorkDate({
          storeId,
          accNumber,
          id: selectedId,
          values,
          groupId,
          formikBag
        })
      );
    },
    validationSchema
  });

  return { formik };
};

export default useWorkDateForm;
