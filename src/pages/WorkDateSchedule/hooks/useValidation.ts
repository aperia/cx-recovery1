// constants
import { I18N_TEXT } from 'app/constants';
import { WORK_DATE_FIELD } from './../constants';

// helpers
import * as Yup from 'yup';

const useValidation = () => {
  const validationSchema = Yup.object().shape({
    [WORK_DATE_FIELD.CALENDAR_CODE.name]: Yup.string().required(
      I18N_TEXT.CALENDAR_CODE_IS_REQUIRED
    ),
    [WORK_DATE_FIELD.REASON.name]: Yup.string().required(
      I18N_TEXT.REASON_REQUIRED
    ),
    [WORK_DATE_FIELD.NEXT_WORK_DATE.name]: Yup.string().required(
      I18N_TEXT.NEXT_WORK_DATE_REQUIRED
    ),
    [WORK_DATE_FIELD.NOTE.name]: Yup.string().when(
      [WORK_DATE_FIELD.LINK_TO_NOTE.name],
      (value: string, schema: Yup.AnySchema) =>
        value === 'Y' ? schema.required(I18N_TEXT.NOTE_REQUIRED) : schema
    )
  });

  return {
    validationSchema
  };
};

export default useValidation;
