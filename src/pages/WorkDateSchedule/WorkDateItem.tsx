import React from 'react';
import { useDispatch } from 'react-redux';

// components
import { Button, Icon, Popover, Tooltip } from 'app/_libraries/_dls/components';
import InfoRow from './InfoRow';
import WorkDateConfirmBody from './WorkDateConfirmBody';
import If from 'pages/__commons/If';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { useAccountDetail, useSelectorId } from 'app/hooks';
import useRenderTime from 'pages/__commons/NoteCode/hooks/useRenderTime';
import useRefData from 'app/hooks/useRefData';

// actions
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { workDateActions } from './_redux';

// selector
import { selectNoteCodes } from 'pages/__commons/NoteCode/_redux/selectors';
import { getAccountDetail } from 'pages/AccountDetail/_redux/selectors';

// constants
import { I18N_TEXT } from 'app/constants';
import { IWorkDate } from './types';
import { INoteCode } from 'pages/__commons/NoteCode/types';
import { getDescriptionFromCode } from 'pages/__commons/helpers';

export interface WorkDateItemProps {
  data: IWorkDate;
  isPastWorkDate?: boolean;
}
const WorkDateItem: React.FC<WorkDateItemProps> = ({
  data,
  isPastWorkDate = false
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const { storeId } = useAccountDetail();
  const { groupId } = useSelectorId(getAccountDetail) || {};
  const calendarCodeOptions = useRefData('calendarCode2');
  const noteCodes = useSelectorId<INoteCode[]>(selectNoteCodes);
  const { id = '', noteId = '', nextWorkDate = '', nextWorkTime = '' } = data;

  const noteData =
    noteCodes.find((note: INoteCode) => note.id === noteId) ||
    ({} as INoteCode);
  const { createdDate = '', value: noteCode, description: noteDesc } = noteData;

  const noteTime = useRenderTime(createdDate);

  const nextWorkTimeViewVal = React.useMemo(() => {
    if (!nextWorkTime) return nextWorkDate;

    return `${nextWorkDate} ${nextWorkTime}`;
  }, [nextWorkDate, nextWorkTime]);

  const handleDelete = React.useCallback(() => {
    dispatch(
      rootModalActions.open({
        autoClose: false,
        title: I18N_TEXT.DELETE_WORK_DATE_TITLE,
        body: <WorkDateConfirmBody data={data} noteCode={noteCode} />,
        btnConfirmText: I18N_TEXT.DELETE,
        onConfirm: () => {
          dispatch(workDateActions.deleteWorkDate({ storeId, id, groupId }));
        },
        onCancel: () => {
          dispatch(rootModalActions.close());
        }
      })
    );
  }, [data, dispatch, groupId, id, noteCode, storeId]);

  return (
    <div className="bg-light-l16 br-radius-8 mb-8 p-16 pt-12">
      <InfoRow
        label={getDescriptionFromCode(
          calendarCodeOptions,
          data.calendarCode,
          true
        )}
        value={
          <If condition={!isPastWorkDate}>
            <Tooltip
              triggerClassName="d-flex"
              variant="primary"
              placement="top"
              element={t(I18N_TEXT.DELETE)}
            >
              <Button size="sm" variant="icon-secondary" onClick={handleDelete}>
                <Icon name="delete" className="color-grey-l08" size="4x" />
              </Button>
            </Tooltip>
          </If>
        }
        containerClass="d-flex align-item-center justify-content-between"
        labelClass="fw-500"
      />

      <InfoRow
        label={I18N_TEXT.REASON}
        labelDetail
        value={data.reason}
        containerClass="mt-8"
        labelClass="d-inline-block w-40 color-grey fw-500 mr-8"
        valueClass="w-60"
      />

      <InfoRow
        label={I18N_TEXT.NEXT_WORK_TIME}
        labelDetail
        value={nextWorkTimeViewVal}
        containerClass="mt-8"
        labelClass="d-inline-block w-40 color-grey fw-500 mr-8"
        valueClass="w-60"
      />

      <InfoRow
        label={I18N_TEXT.POST_TO_GROUP}
        labelDetail
        value={data?.groupId ? t(I18N_TEXT.NO) : t(I18N_TEXT.YES)}
        containerClass="mt-8"
        labelClass="d-inline-block w-40 color-grey fw-500 mr-8"
        valueClass="w-60"
      />

      <InfoRow
        label={I18N_TEXT.LINK_TO_NOTE}
        labelDetail
        value={
          <>
            <Popover
              placement="top"
              size="md"
              containerClassName="inside-infobar"
              element={
                <>
                  <div className="d-flex align-item-center justify-content-between mt-16">
                    <span className="fw-500">{noteCode}</span>
                    <span className="fs-12 color-grey">{noteTime}</span>
                  </div>
                  <div className="mt-4">{noteDesc}</div>
                </>
              }
              popoverInnerClassName="pt-0 px-16 pb-16"
            >
              <If condition={!!noteCode}>
                <span className="link text-decoration-none">{noteCode}</span>
              </If>

              <If condition={!noteCode}>
                <span>{t(I18N_TEXT.NO)}</span>
              </If>
            </Popover>
          </>
        }
        containerClass="mt-8"
        labelClass="d-inline-block w-40 color-grey fw-500 mr-8"
        valueClass="w-60"
      />
    </div>
  );
};

export default React.memo(WorkDateItem);
