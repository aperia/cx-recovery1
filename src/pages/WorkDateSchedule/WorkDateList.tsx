import React from 'react';

// components
import WorkDateItem from './WorkDateItem';
import SectionControl from 'app/components/SectionControl';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// constants
import { I18N_TEXT } from 'app/constants';
import { IWorkDate } from './types';

// helpers
import { isEmpty } from 'app/helpers';
import If from 'pages/__commons/If';

export interface WorkDateListProps {
  newWorkDates: IWorkDate[];
  pastWorkDates: IWorkDate[];
  renderEmpty?: React.ReactNode;
}
const WorkDateList: React.FC<WorkDateListProps> = ({
  newWorkDates,
  pastWorkDates,
  renderEmpty
}) => {
  const { t } = useTranslation();

  return (
    <>
      {newWorkDates.map((workDate: IWorkDate) => (
        <WorkDateItem key={workDate.id} data={workDate} />
      ))}

      <If condition={isEmpty(newWorkDates)}>{renderEmpty}</If>

      <div className="border-top mt-8 mb-16" />

      <SectionControl
        title={t(I18N_TEXT.PAST_WORK_DATES)}
        sectionKey={`section-PAST_WORK_DATES`}
        classNames="fs-11 text-uppercase color-grey fw-500"
        classNamesView="fs-14 text-transform-none fw-400 color-grey-d20 py-16"
        isExpand={false}
        isTitleNormal={true}
      >
        {pastWorkDates.map((workDate: IWorkDate) => (
          <WorkDateItem key={workDate.id} data={workDate} isPastWorkDate />
        ))}
      </SectionControl>
    </>
  );
};

export default React.memo(WorkDateList);
