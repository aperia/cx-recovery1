import { I18N_TEXT } from 'app/constants';

export const WORK_DATE_FIELD = {
  CALENDAR_CODE: {
    name: 'calendarCode',
    label: I18N_TEXT.CALENDAR_CODE
  },
  REASON: {
    name: 'reason',
    label: I18N_TEXT.REASON
  },
  NEXT_WORK_DATE: {
    name: 'nextWorkDate',
    label: I18N_TEXT.NEXT_WORK_DATE
  },
  NEXT_WORK_TIME: {
    name: 'nextWorkTime',
    label: I18N_TEXT.NEXT_WORK_TIME
  },
  POST_TO_GROUP: {
    name: 'postToGroup',
    label: I18N_TEXT.POST_TO_GROUP
  },
  LINK_TO_NOTE: {
    name: 'linkToNote',
    label: I18N_TEXT.LINK_TO_NOTE
  },
  NOTE: {
    name: 'note',
    label: I18N_TEXT.NOTE
  }
};
