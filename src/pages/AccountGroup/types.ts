import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import {
  IAccounts,
  orderByDefault,
  sortByDefault,
  statusDefault,
  TypeStatusAccountList
} from 'pages/AccountManagement/types';

export const AccountListInitialState: AccountListReducer = {
  sortBy: sortByDefault,
  orderBy: orderByDefault,
  status: statusDefault,
  isLoading: false,
  isLoadingLoadMore: false,
  isEnd: false,
  isError: false,
  startSequence: 0,
  endSequence: 25,
  searchValue: [],
  data: [],
  selectedAccounts: [] as IAccounts[]
};

export const ModalAccountFromGroupInitialState: ModalAccountFromGroup = {
  isOpenedModal: false,
  modalProps: {}
};

export interface AccountListReducer {
  sortBy: RefDataValue;
  orderBy: RefDataValue;
  status: TypeStatusAccountList;
  isLoading: boolean;
  isError: boolean;
  startSequence: number;
  endSequence: number;
  isLoadingLoadMore: boolean;
  isEnd: boolean;
  searchValue: AttributeSearchValue[];
  data: IAccounts[];
  selectedAccounts: IAccounts[];
}

export interface IDataGroupList {
  id: string;
  accountId: string;
  debtorName: string;
  groupId: string;
  accountNumber: string;
  accNumber: {
    accountNumber: string;
    accountNumberUnMask: string;
  };
  accountType: string;
  accountClass: string;
  responsibilityCenterNumber: string;
  reasonCode: string;
  chargedOffReasonCode: string;
  chargedOffDate: string;
  chargedOffAmount: string;
  outstandingBalanceAmount: string;
  lastPaymentDate: string;
  specialCondition: Array<'B' | 'D' | 'F' | 'L' | 'X'>;
}

export interface IPayloadGetAccountGroupReturn {
  data: IDataGroupList[];
}

export interface IAccountGroup {
  list: {
    data: IDataGroupList[];
    isLoading: boolean;
  };
}

export interface ModalAccountFromGroup {
  isOpenedModal: boolean;
  modalProps: {
    account?: IAccounts;
    groupId?: string;
  };
}

export interface IInitialStateAccountGroupReducer {
  modalAccountFromGroup: ModalAccountFromGroup;
  timeUpdate?: number;
  accountList: AccountListReducer;
  accountGroup: {
    [storeId: string]: IAccountGroup;
  };
  isLoading: boolean;
}
