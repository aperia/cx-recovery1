import React, {
  Fragment,
  useCallback,
  useEffect,
  useMemo,
  useState
} from 'react';

// components
import {
  AttributeSearchEvent,
  AttributeSearchValue,
  SimpleBar
} from 'app/_libraries/_dls/components';
import AccountListSearch from 'pages/__commons/AccountList/SearchAccounts';
import SortAccountList from './Sort';
import Accounts from './Accounts';
import FailedApiReload from 'app/components/FailedApiReload';
import { NoDataGrid, SearchNodata } from 'app/components';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { accountGroupActions } from '../_redux';
import {
  selectorAccountList,
  selectorIsError,
  selectorIsLoading,
  selectorIsSearch
} from '../_redux/accountList/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { checkEmpty, isEmpty, isEqual } from 'app/helpers';
import { I18N_TEXT } from 'app/constants';
import classNames from 'classnames';
import { IAccounts } from 'pages/AccountManagement/types';

interface AccountListProps {}

let keepSearchValues: AttributeSearchValue[] = [];

const AccountList: React.FC<AccountListProps> = ({}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [searchValues, setSearchValues] = useState<AttributeSearchValue[]>([]);

  const accounts: IAccounts[] = useSelector(selectorAccountList);
  const originalSearch: AttributeSearchValue[] = useSelector(selectorIsSearch);
  const isError: boolean = useSelector(selectorIsError);
  const isLoading: boolean = useSelector(selectorIsLoading);

  const getAccounts = useCallback(() => {
    batch(() => {
      dispatch(
        accountGroupActions.getAccountsInAccountGroupModal({
          isLoadMore: false
        })
      );
    });
  }, [dispatch]);

  useEffect(() => {
    return () => {
      dispatch(accountGroupActions.removeAccountList());
    };
  }, [dispatch]);

  useEffect(() => {
    if (!isEqual(originalSearch, keepSearchValues)) {
      setSearchValues(originalSearch);
      keepSearchValues = originalSearch;
    }
  }, [originalSearch]);

  useEffect(() => {
    if (originalSearch.length) {
      getAccounts();
    }
  }, [getAccounts, originalSearch]);

  const handleChange = (event: AttributeSearchEvent) => {
    const { value } = event;
    setSearchValues(value);
  };

  const handleClear = () => setSearchValues([]);

  const handleSearch = (
    event: AttributeSearchEvent,
    error?: Record<string, string>
  ) => {
    if (!isEmpty(error) || !event.value.length) return;
    batch(() => {
      dispatch(accountGroupActions.onChangeSearchValue(event.value));
      keepSearchValues = event.value;
    });
  };

  const onClearSearch = useCallback(() => {
    dispatch(accountGroupActions.onClearAndReset());
  }, [dispatch]);

  const renderAccounts = useMemo(() => {
    if (isError)
      return (
        <FailedApiReload id="failure-api-get-accounts" onReload={getAccounts} />
      );

    if (!checkEmpty([originalSearch, false, true]) && !isLoading) {
      return (
        <>
          <NoDataGrid text={I18N_TEXT.SEARCH_ACCOUNTS_TO_SELECT} />
        </>
      );
    }

    return (
      <Fragment>
        <div className="mt-16 d-flex justify-content-between align-items-center">
          <SortAccountList />
        </div>
        {accounts.length ? (
          <Accounts />
        ) : (
          <SearchNodata
            text={t(I18N_TEXT.ADJUST_YOUR_SEARCH_FILTER)}
            onClearSearch={onClearSearch}
          />
        )}
      </Fragment>
    );
  }, [
    isError,
    getAccounts,
    originalSearch,
    isLoading,
    accounts.length,
    t,
    onClearSearch
  ]);

  const renderMainLayout = () => {
    return (
      <>
        <div className="header-account">
          <h5 className="header-account-title">{t(I18N_TEXT.ACCOUNT_LIST)}</h5>
          <div className="header-account-search">
            <AccountListSearch
              searchValues={searchValues}
              handleClear={handleClear}
              handleChange={handleChange}
              handleSearch={handleSearch}
            />
          </div>
        </div>
        {renderAccounts}
      </>
    );
  };

  return (
    <SimpleBar>
      <div
        className={classNames(
          'account-list-from-group p-24 max-width-lg mx-auto',
          {
            loading: isLoading
          }
        )}
      >
        {renderMainLayout()}
      </div>
    </SimpleBar>
  );
};
export default AccountList;
