import React, { useCallback } from 'react';

// component
import SortByAccounts from 'pages/__commons/AccountList/SortByAccounts';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { selectorSortBy } from 'pages/AccountGroup/_redux/accountList/selectors';
import { accountGroupActions } from 'pages/AccountGroup/_redux';

// helper
import { dropdownSortAccounts } from 'app/constants';

interface SortByAccountListProps {}

const SortByAccountList: React.FC<SortByAccountListProps> = () => {
  const dispatch = useDispatch();
  const value: RefDataValue = useSelector(selectorSortBy);

  const handleChange = useCallback(
    (e: DropdownBaseChangeEvent) => {
      dispatch(accountGroupActions.onChangeSortAccounts(e.value));
    },
    [dispatch]
  );

  return (
    <SortByAccounts
      value={value}
      dropdownSortAccounts={dropdownSortAccounts}
      handleChange={handleChange}
    />
  );
};
export default SortByAccountList;
