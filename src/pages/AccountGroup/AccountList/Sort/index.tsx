import React from 'react';

// component
import StatusAccountList from './Status';
import SortByAccountList from './SortBy';
import OrderByAccountList from './OrderBy';

interface SortAccountListProps {}

const SortAccountList: React.FC<SortAccountListProps> = () => {
  return (
    <div className="flex-1 d-flex justify-content-between align-items-center">
      <StatusAccountList />
      <div className="d-flex align-items-center mr-n8">
        <SortByAccountList />
        <OrderByAccountList />
      </div>
    </div>
  );
};
export default SortAccountList;
