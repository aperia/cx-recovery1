import React, { useCallback } from 'react';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { accountGroupActions } from 'pages/AccountGroup/_redux';
import { selectorStatus } from 'pages/AccountGroup/_redux/accountList/selectors';

// helper
import { TypeStatusAccountList } from 'pages/AccountManagement/types';
import StatusAccounts from 'pages/__commons/AccountList/StatusAccounts';

interface StatusAccountListProps {}

const StatusAccountList: React.FC<StatusAccountListProps> = () => {
  const dispatch = useDispatch();
  const value: TypeStatusAccountList = useSelector(selectorStatus);

  const handleChange = useCallback(
    (value: string) => {
      dispatch(
        accountGroupActions.onChangeStatus(value as TypeStatusAccountList)
      );
    },
    [dispatch]
  );

  return <StatusAccounts value={value} handleChange={handleChange} />;
};
export default StatusAccountList;
