import React from 'react';

// redux
import { accountGroupActions } from 'pages/AccountGroup/_redux';
import { useDispatch } from 'react-redux';

// helper
import ClearAndReset from 'pages/__commons/AccountList/ClearAndReset';

interface ClearAndResetAccountsProps {}

const ClearAndResetAccounts: React.FC<ClearAndResetAccountsProps> = () => {
  const dispatch = useDispatch();

  const onClick = () => {
    dispatch(accountGroupActions.onClearAndReset());
  };

  return <ClearAndReset onClick={onClick} />;
};
export default ClearAndResetAccounts;
