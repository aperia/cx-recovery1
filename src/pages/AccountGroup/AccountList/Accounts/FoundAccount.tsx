import React, { useCallback } from 'react';

// components
import { View } from 'app/_libraries/_dof/core';
import { CheckBox, Tooltip } from 'app/_libraries/_dls/components';
import Input from 'app/_libraries/_dls/components/CheckBox/Input';

// redux
import { accountGroupActions } from 'pages/AccountGroup/_redux';

// helpers
import { useDispatch, useSelector } from 'react-redux';
import {
  selectorAccountList,
  selectorSelectedAccounts
} from '../../_redux/accountList/selectors';
import classNames from 'classnames';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// type
import { IAccounts } from 'pages/AccountManagement/types';
import { I18N_TEXT } from 'app/constants';

export interface FoundAccountProps {}

const FoundAccount: React.FC<FoundAccountProps> = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();
  const data: IAccounts[] = useSelector(selectorAccountList);

  const selectedAccounts: IAccounts[] = useSelector(selectorSelectedAccounts);

  const handleSelectedAccounts = useCallback(
    (item: IAccounts) => {
      const index = selectedAccounts.findIndex(
        (account: IAccounts) => item.accountNumber === account.accountNumber
      );
      const _selectedAccounts = [...selectedAccounts];

      if (index !== -1) {
        _selectedAccounts.splice(index, 1);
      } else {
        _selectedAccounts.push(item);
      }

      dispatch(accountGroupActions.onSelectorAccounts(_selectedAccounts));
    },
    [dispatch, selectedAccounts]
  );

  const renderItemAccount = (item: IAccounts, index: number) => {
    const checked = selectedAccounts.some(
      (account: IAccounts) => item.accountNumber === account.accountNumber
    );
    return (
      <div
        onClick={() => handleSelectedAccounts(item)}
        className={classNames(
          'card card-blue',
          item.groupId && 'disabled',
          checked && 'active'
        )}
      >
        <div className="d-flex">
          <div className="pl-8 pr-16">
            <CheckBox className="account-list__checkbox">
              <Input
                id={`checkbox_${item.id}`}
                checked={checked}
                onChange={() => handleSelectedAccounts(item)}
                className={classNames(item.groupId && 'disabled')}
              />
            </CheckBox>
          </div>
          <div className="flex-1">
            <p className="fw-500 mb-8">{item.debtorName}</p>
            <View
              id={`accountAccountList-${item.accountNumber}-${index}`}
              formKey={`accountAccountList-${item.accountNumber}-${index}`}
              descriptor="accountAccountList"
              value={item}
            />
          </div>
        </div>
      </div>
    );
  };

  const renderLayout = () => {
    return data.map((item, index) => {
      return (
        <div key={index} className="mt-16">
          {item.groupId ? (
            <Tooltip
              element={t(
                I18N_TEXT.CANNOT_SELECT_BECAUSE_THIS_ACCOUNT_HAS_BEEN_IN_A_GROUP
              )}
              placement="top-start"
              triggerClassName="w-100"
              variant="default"
            >
              {renderItemAccount(item, index)}
            </Tooltip>
          ) : (
            renderItemAccount(item, index)
          )}
        </div>
      );
    });
  };

  return <>{renderLayout()}</>;
};

export default FoundAccount;
