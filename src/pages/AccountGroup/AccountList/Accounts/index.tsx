import React from 'react';

// components
import FoundAccount from './FoundAccount';
import InfinityLoad from '../LoadMoreAccounts';
import ClearAndResetAccounts from './ClearAndReset';

// helpers
import useTranslation from 'app/_libraries/_dls/hooks/useTranslation';
import { I18N_TEXT } from 'app/constants';
import { useSelector } from 'react-redux';
import { selectorAccountList } from 'pages/AccountGroup/_redux/accountList/selectors';

// type
import { IAccounts } from 'pages/AccountManagement/types';

export interface ListAccountProps {}

const Accounts: React.FC<ListAccountProps> = () => {
  const { t } = useTranslation();
  const data: IAccounts[] = useSelector(selectorAccountList);

  const renderLayout = () => {
    return <FoundAccount />;
  };

  return (
    <div className="mt-16">
      <div className="d-flex justify-content-between align-items-center mr-n8">
        <p>{t(I18N_TEXT.RESULT_FOUNDED, { count: data.length })}</p>
        <ClearAndResetAccounts />
      </div>
      {renderLayout()}
      <InfinityLoad />
    </div>
  );
};

export default Accounts;
