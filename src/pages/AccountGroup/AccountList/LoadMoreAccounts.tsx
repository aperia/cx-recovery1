import React from 'react';

// components
import InfinityLoad from 'app/components/LoadMore';

// redux
import { useDispatch, useSelector } from 'react-redux';

// helper
import { accountGroupActions } from '../_redux';
import {
  selectorIsEnd,
  selectorIsLoadingLoadMore
} from '../_redux/accountList/selectors';

export interface LoadMoreAgencyProps {}

const LoadMore: React.FC<LoadMoreAgencyProps> = () => {
  const dispatch = useDispatch();
  const isEnd: boolean = useSelector(selectorIsEnd);
  const isLoading: boolean = useSelector(selectorIsLoadingLoadMore);

  const handleOnLoadMore = () => {
    dispatch(
      accountGroupActions.getAccountsInAccountGroupModal({ isLoadMore: true })
    );
  };

  return (
    <InfinityLoad
      dataTestId="accounts-load-more"
      loading={isLoading}
      onLoadMore={handleOnLoadMore}
      isEnd={isEnd}
    />
  );
};

export default LoadMore;
