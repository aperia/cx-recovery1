import React, { useMemo, useEffect } from 'react';
import { View } from 'app/_libraries/_dof/core';
import { useSelectorId } from 'app/hooks';
import { formatCurrency } from 'app/helpers';

// component
import { Button, Tooltip } from 'app/_libraries/_dls/components';

// redux
import { useDispatch } from 'react-redux';
import { accountGroupActions } from '../_redux';
import { getAccountGroup } from 'pages/AccountGroup/_redux/accountGroup/selectors';

// hooks
import { IDataGroupList } from '../types';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helpers
import { formatDataAccounts } from '../helpers';

// constant
import { LIMIT_ACCOUNT_GROUP } from '../constants';
import { I18N_TEXT } from 'app/constants';

interface AccountGroupListProps {
  openModalConfirmRemove: (
    event: React.MouseEvent<HTMLButtonElement>,
    account: IDataGroupList
  ) => void;
  openAccountDetail: (account: IDataGroupList) => void;
  removeCurrentTab: () => void;
  openModalConfirmDisband: (event: React.MouseEvent<HTMLButtonElement>) => void;
}

const AccountGroupList: React.FC<AccountGroupListProps> = ({
  openModalConfirmRemove,
  removeCurrentTab,
  openAccountDetail,
  openModalConfirmDisband
}) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const data: IDataGroupList[] = useSelectorId(getAccountGroup);

  // calculate amount from data
  const accountOverview = useMemo(() => {
    let totalChargedOffAmount = 0;
    let totalAccountBalancerAmount = 0;

    data?.forEach((item: IDataGroupList) => {
      totalChargedOffAmount += Number(item.chargedOffAmount);
      totalAccountBalancerAmount += Number(item.outstandingBalanceAmount);
    });

    return {
      totalAccount: data?.length || 0,
      totalChargedOffAmount: formatCurrency(`${totalChargedOffAmount}`),
      totalAccountBalancerAmount: formatCurrency(
        `${totalAccountBalancerAmount}`
      ),
      groupId: data?.length ? data[0].groupId : ''
    };
  }, [data]);

  // IF number of account in the group is 20 THEN system will disabled Link Account button
  const checkDisableLinkAccount: boolean = useMemo(() => {
    return data?.length >= LIMIT_ACCOUNT_GROUP;
  }, [data]);

  useEffect(() => {
    if (data && accountOverview.totalAccount === 0) {
      removeCurrentTab();
    }
  }, [accountOverview.totalAccount, data, removeCurrentTab]);

  return (
    <div className="p-24 max-width-lg mx-auto">
      {/* HEADING */}
      <div className="d-flex justify-content-between align-items-center">
        <h3>{t(I18N_TEXT.ACCOUNT_GROUP_TITLE)}</h3>
        <div className="mr-n8">
          <Button
            size="sm"
            variant="outline-danger"
            onClick={event => openModalConfirmDisband(event)}
          >
            {t(I18N_TEXT.DISBAND_ACCOUNT)}
          </Button>
          {checkDisableLinkAccount ? (
            <Tooltip
              placement="top"
              variant="default"
              element={t(I18N_TEXT.CAN_NOT_LINK_ACCOUNT)}
            >
              <Button
                size="sm"
                variant="outline-primary"
                disabled={checkDisableLinkAccount}
              >
                {t(I18N_TEXT.LINK_ACCOUNT)}
              </Button>
            </Tooltip>
          ) : (
            <Button
              size="sm"
              variant="outline-primary"
              disabled={checkDisableLinkAccount}
              onClick={() =>
                dispatch(
                  accountGroupActions.setModalCreateAccountGroup({
                    isOpenedModal: true,
                    modalProps: {
                      groupId: accountOverview.groupId
                    }
                  })
                )
              }
            >
              {t(I18N_TEXT.LINK_ACCOUNT)}
            </Button>
          )}
        </div>
      </div>
      {/* SNAPSHOT */}
      <div className="border br-light-l04 bg-light-l20 rounded-lg p-16 mt-24">
        <div className="row">
          <div className="col-12 col-lg-4">
            <span>{t(I18N_TEXT.TOTAL_ACCOUNT)}:</span>
            <strong className="ml-4">{`${accountOverview.totalAccount}/${LIMIT_ACCOUNT_GROUP}`}</strong>
          </div>
          <div className="col-6 col-lg-4 mt-lg-0 mt-md-8">
            <span>{t(I18N_TEXT.TOTAL_CHARGED_OFF_AMOUNT)}:</span>
            <strong className="ml-4">
              {accountOverview.totalChargedOffAmount}
            </strong>
          </div>
          <div className="col-6 col-lg-4 mt-lg-0 mt-md-8">
            <span>{t(I18N_TEXT.TOTAL_OUTSTANDING_BALANCE_AMOUNT)}:</span>
            <strong className="ml-4">
              {accountOverview.totalAccountBalancerAmount}
            </strong>
          </div>
        </div>
      </div>
      {/* LIST ACCOUNT GROUP */}
      <div className="mt-16">
        {data?.map((item: IDataGroupList, index: number) => (
          <div
            key={index.toString()}
            className="card card-radio cursor-pointer br-radius-8 p-16 mt-16"
            onClick={() => openAccountDetail(item)}
          >
            <div className="d-flex justify-content-between align-items-center">
              <h5 className="fw-500">{item.debtorName}</h5>
              <div className="d-flex">
                <Button
                  size="sm"
                  className="mr-n8"
                  variant="outline-danger"
                  onClick={event => {
                    openModalConfirmRemove(event, item);
                  }}
                >
                  {t(I18N_TEXT.REMOVE)}
                </Button>
              </div>
            </div>

            <View
              id={`accountGroup-${index}`}
              formKey={`accountGroup-${index}`}
              descriptor="accountGroup"
              value={formatDataAccounts(t, item)}
            />
          </div>
        ))}
      </div>
    </div>
  );
};

export default AccountGroupList;
