import { accountGroupController } from 'pages/AccountGroup/__mock__/accountGroupController';

// types
import { IAccounts } from 'pages/AccountManagement/types';

const accountGroupService = {
  getAccountGroup: (groupId: string) => {
    return accountGroupController.getAccountGroup(groupId);
  },
  setAccounsGroup(accounts: IAccounts[], groupId: string) {
    return accountGroupController.setAccounsGroup(accounts, groupId);
  },
  removeAccountGroup: (accNumber: string, groupId: string) => {
    return accountGroupController.removeAccountGroup(accNumber, groupId);
  },
  disbandAccountGroup: (groupId: string) => {
    return accountGroupController.disbandAccountGroup(groupId);
  }
};
export default accountGroupService;
