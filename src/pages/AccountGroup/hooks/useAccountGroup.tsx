import React, { useCallback } from 'react';
import { useDispatch } from 'react-redux';

// redux
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { accountGroupActions } from 'pages/AccountGroup/_redux';

// hooks
import { I18N_TEXT } from 'app/constants';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// components
import ModalConfirmRemove from '../ModalConfirmRemove';

export interface RemoveAccGroupPayload {
  accountId: string;
  debtorName: string;
  accountNumber: string; // number account main of group
  groupId: string; // number account link of group
  storeId: string;
}

export interface DisbandAccGroupPayload {
  groupId: string; // number account link of group
  storeId: string;
}

const useAccountGroup = () => {
  const dispatch = useDispatch();
  const { t } = useTranslation();

  const showConfirmRemoveAccGroup = useCallback(
    (input: RemoveAccGroupPayload) => {
      dispatch(
        rootModalActions.open({
          autoClose: false,
          title: I18N_TEXT.TITLE_CONFIRM_REMOVE_ACCOUNT_GROUP,
          size: 'sm',
          body: (
            <ModalConfirmRemove
              debtorName={input.debtorName}
              accNumber={input.accountNumber}
            />
          ),
          btnConfirmText: I18N_TEXT.REMOVE,
          btnCancelText: I18N_TEXT.CANCEL,
          onConfirm: () => {
            dispatch(rootModalActions.loading(true));

            dispatch(
              accountGroupActions.removeAccountGroup({
                accountId: input.accountId,
                accNumber: input.accountNumber,
                groupId: input.groupId,
                storeId: input.storeId
              })
            );
          },
          onCancel: () => {
            dispatch(rootModalActions.close());
          }
        })
      );
    },
    [dispatch]
  );

  const showConfirmDisbandAccGroup = useCallback(
    (input: DisbandAccGroupPayload) => {
      dispatch(
        rootModalActions.open({
          autoClose: false,
          title: I18N_TEXT.TITLE_CONFIRM_DISBAND_GROUP,
          body: <p>{t(I18N_TEXT.BODY_CONFIRM_DISBAND_GROUP)}</p>,
          btnConfirmText: I18N_TEXT.DISBAND,
          btnCancelText: I18N_TEXT.CANCEL,
          onConfirm: () => {
            dispatch(rootModalActions.loading(true));

            dispatch(
              accountGroupActions.disbandAccountGroup({
                groupId: input.groupId,
                storeId: input.storeId
              })
            );
          },
          onCancel: () => {
            dispatch(rootModalActions.close());
          }
        })
      );
    },
    [dispatch, t]
  );

  return { showConfirmRemoveAccGroup, showConfirmDisbandAccGroup };
};

export default useAccountGroup;
