import React, { useEffect, useRef, useCallback } from 'react';
import { useDispatch } from 'react-redux';
import { AccountDetailProvider } from 'app/hooks';

// redux
import { accountGroupActions } from 'pages/AccountGroup/_redux';
import { tabActions } from 'pages/__commons/TabBar/_redux';

// hooks
import useAccountGroup from 'pages/AccountGroup/hooks/useAccountGroup';

// component
import { SimpleBar } from 'app/_libraries/_dls/components';
import AccountGroupList from './AccountGroupList';

// constant
import { STORE_ACCOUNT_GROUP_KEY } from './constants';
import { IDataGroupList } from './types';
interface AccountGroupProps {
  storeId: string;
  groupId: string;
  accountNumber: string;
}

const AccountGroup: React.FC<AccountGroupProps> = ({
  storeId,
  groupId,
  accountNumber
}) => {
  const dispatch = useDispatch();
  const contextValueRef = useRef({
    storeId,
    groupId,
    accNumber: accountNumber
  });

  const { showConfirmRemoveAccGroup, showConfirmDisbandAccGroup } =
    useAccountGroup();

  useEffect(() => {
    dispatch(
      accountGroupActions.getAccountGroup({
        groupId,
        storeId
      })
    );
  }, [dispatch, storeId, groupId]);

  const handleShowConfirmRemove = useCallback(
    (event: React.MouseEvent<HTMLButtonElement>, account: IDataGroupList) => {
      event.stopPropagation();
      const storeGroupId = `${STORE_ACCOUNT_GROUP_KEY}_${groupId}`;
      showConfirmRemoveAccGroup({
        accountId: account.accountId,
        debtorName: account.debtorName,
        groupId,
        accountNumber: account.accNumber.accountNumber,
        storeId: storeGroupId
      });
    },
    [showConfirmRemoveAccGroup, groupId]
  );

  const handleShowConfirmDisband = useCallback(
    (event: React.MouseEvent<HTMLButtonElement>) => {
      event.stopPropagation();
      const storeGroupId = `${STORE_ACCOUNT_GROUP_KEY}_${groupId}`;
      showConfirmDisbandAccGroup({
        groupId,
        storeId: storeGroupId
      });
    },
    [showConfirmDisbandAccGroup, groupId]
  );

  const handleOpenAccountDetail = useCallback(
    (account: IDataGroupList) => {
      const { accountId, accNumber, debtorName } = account;

      dispatch(
        tabActions.addTab({
          title: `${debtorName} - ${accNumber.accountNumber}`,
          storeId: accountId,
          tabType: 'accountDetail',
          iconName: 'file',
          props: {
            storeId: accountId,
            debtorName,
            accountNumber: accNumber.accountNumber
          }
        })
      );
    },
    [dispatch]
  );

  const removeCurrentTab = useCallback(() => {
    dispatch(tabActions.removeTab({ storeId }));
  }, [dispatch, storeId]);

  return (
    <AccountDetailProvider value={contextValueRef.current}>
      <SimpleBar>
        <AccountGroupList
          openModalConfirmRemove={handleShowConfirmRemove}
          removeCurrentTab={removeCurrentTab}
          openAccountDetail={handleOpenAccountDetail}
          openModalConfirmDisband={handleShowConfirmDisband}
        />
      </SimpleBar>
    </AccountDetailProvider>
  );
};

export default AccountGroup;
