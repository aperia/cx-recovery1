import React, { useCallback, useMemo } from 'react';

// components
import { Icon } from 'app/_libraries/_dls/components';
import MaskAccNbr from 'app/components/MaskAccNbr';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { accountGroupActions } from '../_redux';
import { selectorSelectedAccounts } from '../_redux/accountList/selectors';

// types
import { IAccounts } from 'pages/AccountManagement/types';

export interface ItemAccountSelectedProps {
  account: IAccounts;
}
const ItemAccountSelected: React.FC<ItemAccountSelectedProps> = ({
  account
}) => {
  const dispatch = useDispatch();
  const selectedAccounts: IAccounts[] = useSelector(selectorSelectedAccounts);

  const value = useMemo(
    () => ({
      accountNumber: account.accNumber.accountNumber,
      accountNumberUnMask: account.accNumber.accountNumberUnMask
    }),
    [account]
  );

  const handleOnMouseDownIcon = useCallback(() => {
    const index = selectedAccounts.findIndex(
      (_account: IAccounts) => account.accountNumber === _account.accountNumber
    );
    const _selectedAccounts = [...selectedAccounts];

    _selectedAccounts.splice(index, 1);
    dispatch(accountGroupActions.onSelectorAccounts(_selectedAccounts));
  }, [dispatch, selectedAccounts, account.accountNumber]);

  return (
    <div className="pb-16 border-bottom">
      <div className="mt-16 mb-4 d-flex justify-content-between align-items-center">
        <h5>{account.debtorName}</h5>
        <Icon
          name="close"
          onMouseDown={handleOnMouseDownIcon}
          className="link text-decoration-none color-red"
        />
      </div>
      <MaskAccNbr label="" value={value} />
    </div>
  );
};

export default ItemAccountSelected;
