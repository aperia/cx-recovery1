import React, { useCallback } from 'react';

// components
import ItemAccountSelected from './itemAccountSelected';
import {
  Button,
  Icon,
  InlineMessage,
  SimpleBar
} from 'app/_libraries/_dls/components';

// redux
import { useDispatch, useSelector } from 'react-redux';
import { accountGroupActions } from '../_redux';
import { selectorSelectedAccounts } from '../_redux/accountList/selectors';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// types
import { IAccounts } from 'pages/AccountManagement/types';
import { NoDataGrid } from 'app/components';
import { I18N_TEXT, MAX_LENGTH_20 } from 'app/constants';

export interface AccountSelectedProps {
  isCollapse: (collapse: boolean) => void;
}
const AccountSelected: React.FC<AccountSelectedProps> = ({ isCollapse }) => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const selectedAccounts: IAccounts[] = useSelector(selectorSelectedAccounts);

  const renderAccountSelected = useCallback(() => {
    return selectedAccounts.map((account, index) => (
      <ItemAccountSelected key={index} account={account} />
    ));
  }, [selectedAccounts]);

  return (
    <>
      <div className="px-24 py-16 border-bottom d-flex d-lg-none align-items-center justify-content-between">
        <h5>{t(I18N_TEXT.SELECTED_ACCOUNTS)}</h5>
        <Icon
          className="link-primary"
          name="close"
          onClick={() => isCollapse(true)}
          size="5x"
        ></Icon>
      </div>
      <SimpleBar>
        <div className="px-24 pb-24 pt-lg-30 pt-8">
          <h5 className="d-lg-block d-none">
            {t(I18N_TEXT.SELECTED_ACCOUNTS)}
          </h5>
          {selectedAccounts.length > MAX_LENGTH_20 && (
            <InlineMessage className="mt-16 mb-0" variant="danger">
              {t(I18N_TEXT.SELECT_ACCOUNTS_EXCEED_THE_ALLOWED_NUMBER)}
            </InlineMessage>
          )}
          {!!selectedAccounts.length ? (
            <div className="mt-16 d-flex justify-content-between align-items-center">
              <span>
                {selectedAccounts.length}/{MAX_LENGTH_20}{' '}
                {t(I18N_TEXT.ACCOUNTS_SELECTED)}
              </span>
              <Button
                variant="outline-danger"
                size="sm"
                className="mr-n8"
                onClick={() =>
                  dispatch(accountGroupActions.onSelectorAccounts([]))
                }
              >
                {t(I18N_TEXT.REMOVE_ALL)}
              </Button>
            </div>
          ) : (
            <NoDataGrid text={t(I18N_TEXT.NO_SELECTED_ACCOUNTS)}></NoDataGrid>
          )}
          {renderAccountSelected()}
        </div>
      </SimpleBar>
    </>
  );
};
export default AccountSelected;
