import accountGroupList from './accountGroupList.json';
import { IDataGroupList } from '../types';
import { IAccounts } from 'pages/AccountManagement/types';

export let accountGroupListData: IDataGroupList[] = JSON.parse(
  JSON.stringify(accountGroupList.filter(accountGroup => accountGroup.groupId))
);
const isHaveError = window.location.href.includes('error');

export const accountGroupController = {
  getAccountGroup(groupId: string) {
    const data: IDataGroupList[] = accountGroupListData.filter(
      item => item.groupId === groupId
    );

    return Promise.resolve({ data: { success: true, data: data } });
  },
  setAccounsGroup: (accountsToGroup: IAccounts[], groupId: string) => {
    // groupId have value for scenario create accounts group
    // groupId no have value for scenario link accounts group
    const _groupId = groupId || Math.floor(Math.random() * 10).toString();
    if (accountsToGroup[0].accountNumber === '022002••••••1493') {
      return Promise.reject({
        success: false,
        message: 'CREATE ACCOUNT FAILED',
        data: []
      });
    } else {
      accountsToGroup.forEach((accountToGroup: IAccounts) => {
        const itemIndex = (accountGroupList as IDataGroupList[]).findIndex(
          (item: IDataGroupList) =>
            accountToGroup.accountNumber === item.accountNumber
        );
        if (itemIndex !== -1) {
          accountGroupListData.push({
            ...(accountGroupList as IDataGroupList[])[itemIndex],
            groupId: _groupId
          });
        }
      });
      return Promise.resolve({
        success: true,
        data: { accounts: accountsToGroup, groupId: _groupId }
      });
    }
  },
  removeAccountGroup(accNumber: string, groupId: string) {
    try {
      if (isHaveError)
        return Promise.reject({
          success: false,
          message: 'DELETE FAILED'
        });

      const account = accountGroupListData.find(
        item => item.accNumber.accountNumber === accNumber
      );

      if (!account)
        return Promise.reject({ success: false, data: accountGroupListData });

      accountGroupListData = accountGroupListData.filter(
        obj => obj.id !== account.id && obj.groupId === groupId
      );

      return Promise.resolve({ success: true, data: accountGroupListData });
    } catch (err) {
      return Promise.reject({ success: false, data: accountGroupListData });
    }
  },
  disbandAccountGroup(groupId: string) {
    try {
      if (isHaveError)
        return Promise.reject({
          success: false,
          message: 'DISBAND FAILED'
        });

      accountGroupListData = accountGroupListData.filter(
        obj => obj.groupId !== groupId
      );

      return Promise.resolve({ success: true, data: accountGroupListData });
    } catch (err) {
      return Promise.reject({ success: false, data: accountGroupListData });
    }
  }
};
