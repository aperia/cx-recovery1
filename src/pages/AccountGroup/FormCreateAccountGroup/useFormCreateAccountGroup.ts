import { useCallback, useState } from 'react';

// helpers
import { useTranslation } from 'app/_libraries/_dls/hooks';
import { get } from 'app/helpers';

// redux
import { batch, useDispatch, useSelector } from 'react-redux';
import { accountGroupActions } from 'pages/AccountGroup/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { selectorModalAccountFromGroup } from 'pages/AccountGroup/_redux/accountGroup/selectors';
import {
  selectorIsSearch,
  selectorSelectedAccounts
} from '../_redux/accountList/selectors';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';
import { tabActions } from 'pages/__commons/TabBar/_redux';

// types
import { IAccounts } from 'pages/AccountManagement/types';
import { I18N_TEXT, MAX_LENGTH_20 } from 'app/constants';
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { STORE_ACCOUNT_GROUP_KEY } from '../constants';

export const useFormCreateAccountGroup = () => {
  const { t } = useTranslation();
  const dispatch = useDispatch();

  const [collapse, isCollapse] = useState(false);

  const { isOpenedModal, modalProps } = useSelector(
    selectorModalAccountFromGroup
  );
  const originalSearch = useSelector(selectorIsSearch);
  const selectedAccounts: IAccounts[] = useSelector(selectorSelectedAccounts);

  const { groupId } = modalProps;

  const handleCloseModal = useCallback(() => {
    window.innerWidth > 1023 ? isCollapse(false) : isCollapse(true);
    dispatch(accountGroupActions.removeModalCreateAccountGroup());
  }, [dispatch]);

  const handleCancel = useCallback(() => {
    if (originalSearch.length) {
      dispatch(
        rootModalActions.open({
          title: I18N_TEXT.UNSAVED_CHANGE,
          body: I18N_TEXT.DISCARD_CHANGE_BODY,
          btnCancelText: I18N_TEXT.CONTINUE_EDITING,
          btnConfirmText: I18N_TEXT.DISCARD_CHANGE,
          onConfirm: handleCloseModal
        })
      );
      return;
    }
    handleCloseModal();
  }, [dispatch, handleCloseModal, originalSearch]);

  const handleSubmit = useCallback(async () => {
    // groupId no value for scenario create account group
    // groupId have value for scenario link account group
    if (selectedAccounts.length && selectedAccounts.length <= MAX_LENGTH_20) {
      try {
        const response = await dispatch(
          accountGroupActions.setAccountsGroup({ groupId: groupId || '' })
        );
        const isError = get(response, ['meta', 'rejectedWithValue'], false);
        const responseAccountsGroupId = get(
          response,
          ['payload', 'data', 'groupId'],
          ''
        );
        const storeId = `${STORE_ACCOUNT_GROUP_KEY}_${responseAccountsGroupId}`;

        if (isError) {
          throw new Error(I18N_TEXT.ACCOUNT_FAILED_TO_CREATE);
        }
        batch(() => {
          dispatch(
            actionsToast.addToast({
              show: true,
              type: 'success',
              message: groupId
                ? I18N_TEXT.ACCOUNT_LINKED
                : I18N_TEXT.ACCOUNT_GROUP_CREATED
            })
          );
          dispatch(
            tabActions.addTab({
              title: `${t(
                I18N_TEXT.ACCOUNT_GROUP_TITLE
              )} - ${responseAccountsGroupId}`,
              storeId,
              tabType: 'accountGroup',
              iconName: 'file',
              props: {
                storeId,
                groupId: responseAccountsGroupId
              }
            })
          );
          dispatch(
            accountGroupActions.getAccountGroup({
              groupId: responseAccountsGroupId,
              storeId: storeId
            })
          );
          dispatch(accountManagementActions.getAccounts({ isLoadMore: false }));
        });

        handleCloseModal();
      } catch (error) {
        dispatch(
          actionsToast.addToast({
            show: true,
            type: 'error',
            message: groupId
              ? I18N_TEXT.ACCOUNT_FAILED_TO_LINK
              : I18N_TEXT.ACCOUNT_FAILED_TO_CREATE
          })
        );
      }
    }
  }, [t, dispatch, selectedAccounts, handleCloseModal, groupId]);

  const handleResize = useCallback(() => {
    window.innerWidth > 1023 ? isCollapse(false) : isCollapse(true);
  }, []);

  return {
    collapse,
    isCollapse,
    isOpenedModal,
    modalProps,
    selectedAccounts,
    handleCancel,
    handleSubmit,
    handleResize
  };
};
