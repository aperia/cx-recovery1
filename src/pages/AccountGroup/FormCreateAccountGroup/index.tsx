import React, { useLayoutEffect } from 'react';

// components
import {
  Modal,
  ModalHeader,
  ModalTitle,
  ModalBody,
  ModalFooter,
  Icon
} from 'app/_libraries/_dls/components';
import AccountList from 'pages/AccountGroup/AccountList';
import AccountSelected from '../AccountSelected';

// hooks
import { useFormCreateAccountGroup } from './useFormCreateAccountGroup';

// helpers
import classNames from 'classnames';
import { useTranslation } from 'app/_libraries/_dls/hooks';

// types
import { I18N_TEXT, MAX_LENGTH_20 } from 'app/constants';

export interface AccountGroupFormCreateProps {}

const AccountGroupFormCreate: React.FC<AccountGroupFormCreateProps> = () => {
  const { t } = useTranslation();

  const {
    collapse,
    isCollapse,
    isOpenedModal,
    modalProps,
    selectedAccounts,
    handleCancel,
    handleSubmit,
    handleResize
  } = useFormCreateAccountGroup();

  const { account: accountCreateAccountGroup, groupId } = modalProps;

  useLayoutEffect(() => {
    handleResize();
    window.addEventListener('resize', handleResize);
    return () => {
      window.removeEventListener('resize', handleResize);
    };
  }, [handleResize]);

  return (
    <Modal full show={isOpenedModal} loading={false}>
      <ModalHeader border closeButton onHide={handleCancel}>
        <ModalTitle>
          {groupId
            ? t(I18N_TEXT.LINK_ACCOUNT)
            : t(I18N_TEXT.CREATE_ACCOUNT_GROUP)}
        </ModalTitle>
      </ModalHeader>
      <ModalBody noPadding className="overflow-hidden">
        {!groupId && (
          <div className="bg-light-l20 border-bottom group-id">
            <div className="p-24">
              {t(
                I18N_TEXT.SELECT_ACCOUNTS_THAT_WILL_BE_GROUPED_WITH_THE_ACCOUNTS
              )}
              <span className="ml-4 fw-500">
                {accountCreateAccountGroup?.debtorName} -{' '}
                {accountCreateAccountGroup?.accountNumber?.replace(
                  /\•/gi,
                  ' • '
                )}
              </span>
            </div>
          </div>
        )}
        <div className="content-block content-link-account">
          <div className="content-left">
            <AccountList />
            <ModalFooter
              className="d-lg-none d-md-flex"
              border
              cancelButtonText={t(I18N_TEXT.CANCEL)}
              okButtonText={t(I18N_TEXT.SUBMIT)}
              onCancel={handleCancel}
              onOk={handleSubmit}
              disabledOk={
                !selectedAccounts.length ||
                selectedAccounts.length > MAX_LENGTH_20
              }
            >
              {!!selectedAccounts.length && (
                <div className="d-flex align-items-center mr-auto">
                  <Icon name="error" className="color-red" />
                  <span className="ml-4">
                    {selectedAccounts.length}/{MAX_LENGTH_20}{' '}
                    {t(I18N_TEXT.ACCOUNTS_SELECTED)}.
                  </span>
                  <span
                    className="link text-decoration-none ml-4"
                    onClick={() => isCollapse(false)}
                  >
                    {t(I18N_TEXT.VIEW_DETAILS)}
                  </span>
                </div>
              )}
            </ModalFooter>
          </div>
          <div
            className={classNames('content-right', {
              collapsed: collapse
            })}
          >
            <AccountSelected isCollapse={isCollapse} />
          </div>
        </div>
      </ModalBody>
      <ModalFooter
        className="d-none d-lg-block"
        border
        cancelButtonText={t(I18N_TEXT.CANCEL)}
        okButtonText={t(I18N_TEXT.SUBMIT)}
        onCancel={handleCancel}
        onOk={handleSubmit}
        disabledOk={
          !selectedAccounts.length || selectedAccounts.length > MAX_LENGTH_20
        }
      ></ModalFooter>
    </Modal>
  );
};

export default AccountGroupFormCreate;
