import React from 'react';

import { get } from 'app/helpers';
import { I18N_TEXT } from 'app/constants';
import { SPECIAL_CONDITION } from './constants';
import classnames from 'classnames';
import { genMashAccNumber } from 'pages/AccountManagement/helpers';

// format data pass to view
export const formatDataAccounts = (t: any, inputData: MagicKeyValue) => {
  return {
    ...inputData,
    accountNumber: genMashAccNumber(inputData.accNumber.accountNumberUnMask),
    accNumber: {
      ...inputData.accNumber,
      accountNumber: genMashAccNumber(inputData.accNumber.accountNumberUnMask)
    },
    accountType: {
      value: inputData.accountType,
      description: t(I18N_TEXT.DESCRIPTION_ACCOUNT_TYPE)
    },
    accountClass: {
      value: inputData.accountClass,
      description: t(I18N_TEXT.DESCRIPTION_ACCOUNT_CLASS)
    },
    responsibilityCenterNumber: {
      value: inputData.responsibilityCenterNumber,
      description: t(I18N_TEXT.DESCRIPTION_ACCOUNT_RESPONSIBILITY)
    },
    chargedOffReasonCode: {
      value: inputData.chargedOffReasonCode,
      description: t(I18N_TEXT.DESCRIPTION_ACCOUNT_CHARED_OFF_REASON_CODE)
    },
    specialCondition: {
      value: inputData?.specialCondition?.join(' - '),
      description: (
        <>
          {inputData?.specialCondition?.map((key: string, index: number) => (
            <p
              key={index.toString()}
              className={classnames('d-flex', index !== 0 ? 'mt-16' : '')}
            >
              <span>
                <strong>{key}</strong>
                <span className="mx-8">-</span>
              </span>
              <span>{t(get(SPECIAL_CONDITION, key, ''))}</span>
            </p>
          ))}
        </>
      )
    }
  };
};
