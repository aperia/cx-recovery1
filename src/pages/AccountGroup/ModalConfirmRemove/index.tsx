import React from 'react';

// hooks
import { useTranslation } from 'app/_libraries/_dls/hooks';

// helper
import { I18N_TEXT } from 'app/constants';

export interface ModalConfirmRemoveProps {
  accNumber: string;
  debtorName: string;
}

const ModalConfirmRemove: React.FC<ModalConfirmRemoveProps> = ({
  accNumber,
  debtorName
}) => {
  const { t } = useTranslation();

  return (
    <>
      <p>{t(I18N_TEXT.BODY_CONFIRM_REMOVE_ACCOUNT_GROUP)}</p>
      <p className="mt-16">
        {t(I18N_TEXT.ACCOUNT_NAME_GROUP)}:{' '}
        <strong>{`${debtorName} - ${accNumber.replace(/\•/gi, ' • ')}`}</strong>
      </p>
    </>
  );
};

export default ModalConfirmRemove;
