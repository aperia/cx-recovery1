import { createSlice } from '@reduxjs/toolkit';

// accountGroup action
import {
  getAccountGroup,
  getAccountGroupBuilder
} from './accountGroup/getAccountGroup';
import {
  setAccountsGroup,
  setAccountsGroupBuilder
} from './accountGroup/setAccountsGroup';
import {
  setModalCreateAccountGroup,
  removeModalCreateAccountGroup
} from './accountGroup/actions';
import {
  removeAccountGroup,
  removeAccountGroupBuilder
} from './accountGroup/removeAccountGroup';
import {
  disbandAccountGroup,
  disbandAccountGroupBuilder
} from './accountGroup/disbandAccountGroup';

// accountList action
import {
  onChangeOrderBy,
  onChangeSortAccounts,
  onChangeStatus,
  onChangeSearchValue,
  onClearAndReset,
  removeAccountList,
  onSelectorAccounts
} from './accountList/actions';
import {
  getAccountsInAccountGroupModal,
  getAccountsInAccountGroupModalBuilder
} from './accountList/getAccounts';

// types
import {
  AccountListInitialState,
  ModalAccountFromGroupInitialState
} from 'pages/AccountGroup/types';
import { IInitialStateAccountGroupReducer } from '../types';

const initialState: IInitialStateAccountGroupReducer = {
  modalAccountFromGroup: ModalAccountFromGroupInitialState,
  accountList: AccountListInitialState,
  accountGroup: {},
  isLoading: false
};

export const { actions, reducer } = createSlice({
  name: 'accountGroup',
  initialState: initialState,
  reducers: {
    setModalCreateAccountGroup,
    removeModalCreateAccountGroup,
    removeAccountList,
    onSelectorAccounts,
    onClearAndReset,
    onChangeSortAccounts,
    onChangeOrderBy,
    onChangeStatus,
    onChangeSearchValue
  },
  extraReducers: builder => {
    getAccountGroupBuilder(builder);
    setAccountsGroupBuilder(builder);
    getAccountsInAccountGroupModalBuilder(builder);
    removeAccountGroupBuilder(builder);
    disbandAccountGroupBuilder(builder);
  }
});

const combineActions = {
  ...actions,
  getAccountGroup,
  getAccountsInAccountGroupModal,
  setAccountsGroup,
  removeAccountGroup,
  disbandAccountGroup
};

export { combineActions as accountGroupActions };
