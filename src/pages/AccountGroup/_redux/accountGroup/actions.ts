import { PayloadAction } from '@reduxjs/toolkit';
import {
  IInitialStateAccountGroupReducer,
  ModalAccountFromGroup
} from 'pages/AccountGroup/types';

export const setModalCreateAccountGroup = (
  draftState: IInitialStateAccountGroupReducer,
  action: PayloadAction<ModalAccountFromGroup>
) => {
  draftState.modalAccountFromGroup = action.payload;
};

export const removeModalCreateAccountGroup = (
  draftState: IInitialStateAccountGroupReducer
) => {
  draftState.modalAccountFromGroup = {
    isOpenedModal: false,
    modalProps: {}
  };
};