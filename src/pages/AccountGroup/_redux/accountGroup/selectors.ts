import { createSelector } from '@reduxjs/toolkit';
import { ModalAccountFromGroup } from 'pages/AccountGroup/types';
import { IDataGroupList } from 'pages/AccountGroup/types';

const getModalAccountFromGroup = (states: RootState) =>
  states.accountGroup.modalAccountFromGroup;

export const selectorModalAccountFromGroup = createSelector(
  getModalAccountFromGroup,
  (data: ModalAccountFromGroup) => data
);

export const getTimeUpdate = (state: RootState) =>
  state.accountGroup.timeUpdate;

export const getAccountGroup = createSelector(
  (states: RootState, storeId: string) =>
    states.accountGroup.accountGroup[storeId]?.list?.data,
  (data: IDataGroupList[]) => data
);
export const getLoadingSnapShot = createSelector(
  (states: RootState, storeId: string) =>
    states.accountGroup.accountGroup[storeId]?.list?.isLoading,
  (data: boolean) => data
);
