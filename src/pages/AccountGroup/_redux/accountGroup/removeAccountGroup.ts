import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { batch } from 'react-redux';
import { set } from 'app/helpers';

// Redux
import { accountGroupActions } from 'pages/AccountGroup/_redux';
import { accountManagementActions } from 'pages/AccountManagement/_redux';
import { rootModalActions } from 'pages/__commons/RootModal/_redux/reducer';
import { actionsToast } from 'pages/__commons/ToastNotifications/_redux';

import accountGroupService from 'pages/AccountGroup/accountGroupService';

// Constant
import { I18N_TEXT } from 'app/constants';

// Types
import { IInitialStateAccountGroupReducer } from 'pages/AccountGroup/types';

export interface IRemoveAccountGroupArg {
  accountId: string;
  accNumber: string;
  groupId: string;
  storeId: string;
}

export const removeAccountGroup = createAsyncThunk<
  void,
  IRemoveAccountGroupArg,
  ThunkAPIConfig
>('removeAccountGroup', async (args, { dispatch, rejectWithValue }) => {
  try {
    const { success } = await accountGroupService.removeAccountGroup(
      args.accNumber,
      args.groupId
    );

    if (!success) throw new Error(I18N_TEXT.MSG_REMOVE_ACCOUNT_GROUP_ERROR);
    batch(() => {
      // get list accounts
      dispatch(accountManagementActions.getAccounts({ isLoadMore: false }));

      // get list account group
      dispatch(
        accountGroupActions.getAccountGroup({
          groupId: args.groupId,
          storeId: args.storeId
        })
      );

      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'success',
          message: I18N_TEXT.MSG_REMOVE_ACCOUNT_GROUP_SUCCESS
        })
      );
      dispatch(rootModalActions.close());
    });
  } catch (error: any) {
    batch(() => {
      dispatch(
        actionsToast.addToast({
          show: true,
          type: 'error',
          message: I18N_TEXT.MSG_REMOVE_ACCOUNT_GROUP_ERROR
        })
      );
      dispatch(rootModalActions.close());
    });
    return rejectWithValue(error);
  }
});

export const removeAccountGroupBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateAccountGroupReducer>
) => {
  const { pending, fulfilled, rejected } = removeAccountGroup;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `accountGroup.${storeId}.list.isLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `accountGroup.${storeId}.list.isLoading`, false);
      draftState.timeUpdate = new Date().getTime();
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `accountGroup.${storeId}.list.isLoading`, false);
    });
};
