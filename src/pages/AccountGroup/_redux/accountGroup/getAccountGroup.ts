import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';

import accountGroupService from 'pages/AccountGroup/accountGroupService';
import {
  IInitialStateAccountGroupReducer,
  IPayloadGetAccountGroupReturn
} from 'pages/AccountGroup/types';
import { set } from 'app/helpers';

interface IGetAccountGroupArg {
  storeId: string;
  groupId: string;
}

export const getAccountGroup = createAsyncThunk<
  IPayloadGetAccountGroupReturn,
  IGetAccountGroupArg,
  ThunkAPIConfig
>('getAccountGroup', async (args, thunkAPI) => {
  try {
    const { groupId } = args;
    const { data } = await accountGroupService.getAccountGroup(groupId);

    return { data: data.data || [] };
  } catch (error: any) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const getAccountGroupBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateAccountGroupReducer>
) => {
  const { pending, fulfilled, rejected } = getAccountGroup;
  builder
    .addCase(pending, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `accountGroup.${storeId}.list.isLoading`, true);
    })
    .addCase(fulfilled, (draftState, action) => {
      const { storeId } = action.meta.arg;
      const { data } = action.payload;
      set(draftState, `accountGroup.${storeId}.list.isLoading`, false);
      set(draftState, `accountGroup.${storeId}.list.data`, data);
    })
    .addCase(rejected, (draftState, action) => {
      const { storeId } = action.meta.arg;
      set(draftState, `accountGroup.${storeId}.list.isLoading`, false);
    });
};
