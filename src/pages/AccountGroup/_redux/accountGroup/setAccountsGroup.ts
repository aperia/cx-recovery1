import { ActionReducerMapBuilder, createAsyncThunk } from '@reduxjs/toolkit';
import { get, set } from 'app/helpers';
import accountGroupService from 'pages/AccountGroup/accountGroupService';
import { IInitialStateAccountGroupReducer } from 'pages/AccountGroup/types';
import { IAccounts } from 'pages/AccountManagement/types';

export const setAccountsGroup = createAsyncThunk<
  { success: boolean; data: { accounts: IAccounts[]; groupId: string } },
  {
    groupId: string;
  },
  ThunkAPIConfig
>('accountGroup/setAccountsGroup', async (args, thunkAPI) => {
  try {
    const { accountGroup } = thunkAPI.getState();
    const { groupId } = args;
    const selectedAccounts: IAccounts[] = get(
      accountGroup,
      ['accountList', 'selectedAccounts'],
      []
    );

    const data = await accountGroupService.setAccounsGroup(
      selectedAccounts,
      groupId
    );

    return data;
  } catch (error) {
    return thunkAPI.rejectWithValue(error);
  }
});

export const setAccountsGroupBuilder = (
  builder: ActionReducerMapBuilder<IInitialStateAccountGroupReducer>
) => {
  const { pending, fulfilled, rejected } = setAccountsGroup;
  builder
    .addCase(pending, (draftState, action) => {
      set(draftState, ['isLoading'], true);
    })
    .addCase(fulfilled, (draftState, action) => {
      set(draftState, ['isLoading'], false);
      set(draftState, ['timeUpdate'], new Date().getTime());
    })
    .addCase(rejected, (draftState, action) => {
      set(draftState, ['isLoading'], false);
    });
};
