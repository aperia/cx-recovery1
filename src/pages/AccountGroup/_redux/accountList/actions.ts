import { PayloadAction } from '@reduxjs/toolkit';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import {
  AccountListInitialState,
  IInitialStateAccountGroupReducer
} from 'pages/AccountGroup/types';
import {
  IAccounts,
  orderByDefault,
  sortByDefault,
  statusDefault
} from 'pages/AccountManagement/types';

export const onChangeSortAccounts = (
  draftState: IInitialStateAccountGroupReducer,
  action: PayloadAction<RefDataValue>
) => {
  draftState.accountList.sortBy = action.payload;
};

export const onChangeOrderBy = (
  draftState: IInitialStateAccountGroupReducer,
  action: PayloadAction<RefDataValue>
) => {
  draftState.accountList.orderBy = action.payload;
};

export const onChangeStatus = (
  draftState: IInitialStateAccountGroupReducer,
  action: PayloadAction<'active' | 'purged'>
) => {
  draftState.accountList.status = action.payload;
};

export const onChangeSearchValue = (
  draftState: IInitialStateAccountGroupReducer,
  action: PayloadAction<AttributeSearchValue[]>
) => {
  draftState.accountList.searchValue = action.payload;
};

export const onClearAndReset = (
  draftState: IInitialStateAccountGroupReducer
) => {
  draftState.accountList.searchValue = [];
  draftState.accountList.data = [];
  draftState.accountList.orderBy = orderByDefault;
  draftState.accountList.sortBy = sortByDefault;
  draftState.accountList.status = statusDefault;
};

export const removeAccountList = (
  draftState: IInitialStateAccountGroupReducer
) => {
  return { ...draftState, accountList: AccountListInitialState };
};

export const onSelectorAccounts = (
  draftState: IInitialStateAccountGroupReducer,
  action: PayloadAction<IAccounts[]>
) => {
  draftState.accountList.selectedAccounts = action.payload;
};
