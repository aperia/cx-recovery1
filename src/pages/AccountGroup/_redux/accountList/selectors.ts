import { createSelector } from '@reduxjs/toolkit';
import { AttributeSearchValue } from 'app/_libraries/_dls/components';
import { filterAndOrderAccounts } from 'pages/AccountManagement/helpers';
import {
  IAccounts,
  TypeStatusAccountList
} from 'pages/AccountManagement/types';

const getAccounts = (states: RootState): IAccounts[] =>
  states.accountGroup.accountList.data;

const getOrderBy = (states: RootState): RefDataValue =>
  states.accountGroup.accountList.orderBy;

const getSortBy = (states: RootState): RefDataValue =>
  states.accountGroup.accountList.sortBy;

const getStatus = (states: RootState): TypeStatusAccountList =>
  states.accountGroup.accountList.status;

const getSelectedAccounts = (states: RootState): IAccounts[] =>
  states.accountGroup.accountList.selectedAccounts;

export const selectorAccountList = createSelector(
  getAccounts,
  getStatus,
  getSortBy,
  getOrderBy,
  (
    data: IAccounts[],
    status: TypeStatusAccountList,
    sortBy: RefDataValue,
    orderBy: RefDataValue
  ) => filterAndOrderAccounts(data, status, sortBy.value, orderBy.value)
);

export const selectorIsLoading = createSelector(
  [(states: RootState) => states.accountGroup.accountList.isLoading],
  (isLoading: boolean) => isLoading
);

export const selectorIsSearch = createSelector(
  [(states: RootState) => states.accountGroup.accountList.searchValue],
  (data: AttributeSearchValue[]) => data
);

export const selectorStatus = createSelector(
  getStatus,
  (data: TypeStatusAccountList) => data
);

export const selectorSortBy = createSelector(
  getSortBy,
  (data: RefDataValue) => data
);

export const selectorIsError = createSelector(
  [(states: RootState) => states.accountGroup.accountList.isError],
  (data: boolean) => data
);

export const selectorIsEnd = createSelector(
  [(states: RootState) => states.accountGroup.accountList.isEnd],
  (data: boolean) => data
);

export const selectorIsLoadingLoadMore = createSelector(
  [(states: RootState) => states.accountGroup.accountList.isLoadingLoadMore],
  (data: boolean) => data
);

export const selectorOrderBy = createSelector(
  getOrderBy,
  (data: RefDataValue) => data
);

export const selectorSelectedAccounts = createSelector(
  getSelectedAccounts,
  (data: IAccounts[]) => data
);
