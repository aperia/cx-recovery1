// components
export const SPECIAL_CONDITION = {
  B: 'txt_special_condition_b',
  D: 'txt_special_condition_d',
  F: 'txt_special_condition_f',
  L: 'txt_special_condition_l',
  X: 'txt_special_condition_x'
};

export const LIMIT_ACCOUNT_GROUP = 20;

export const STORE_ACCOUNT_GROUP_KEY = 'account_group';
