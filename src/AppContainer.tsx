import React, { Fragment } from 'react';
import Header from 'pages/__commons/Header';
import TabBar from 'pages/__commons/TabBar';
import RootModal from 'pages/__commons/RootModal';
import ToastNotifications from 'pages/__commons/ToastNotifications';
import AgencyForm from 'pages/__commons/Agency/AgencyForm';

export interface AppContainerProps {}

const AppContainer: React.FC<AppContainerProps> = () => {
  return (
    <Fragment>
      <Header />
      <TabBar />
      <ToastNotifications />
      <RootModal />
      <AgencyForm />
    </Fragment>
  );
};

export default AppContainer;
